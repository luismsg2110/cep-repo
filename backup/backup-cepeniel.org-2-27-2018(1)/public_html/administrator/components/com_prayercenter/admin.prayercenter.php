<?php
/**
* PrayerCenter Component for Joomla
* By Mike Leeper
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
$version = new JVersion();
// Make sure the user is authorized to view this page
if( (real)$version->RELEASE == 1.5 ) {
  $auth =& JFactory::getACL();
  $auth->addACL( 'com_prayercenter', 'manage', 'users', 'super administrator' );
  $auth->addACL( 'com_prayercenter', 'manage', 'users', 'administrator' );
  // uncomment following to allow managers to access the PrayerCenter configuration and management pages
  //$auth->addACL( 'com_prayercenter', 'manage', 'users', 'manager' );
  $user = & JFactory::getUser();
  if (!$user->authorize( 'com_prayercenter', 'manage' )) {
    global $mainframe;
  	$mainframe->redirect( 'index.php', JText::_('ALERTNOTAUTH') );
  }
} elseif( (real)$version->RELEASE >= 1.6 ){
  if (!JFactory::getUser()->authorise('core.manage', 'com_prayercenter')) {
  	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
  }
}
JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_prayercenter'.DS.'tables');
// Require the base controller
require_once (JPATH_ADMINISTRATOR.DS.'components'.DS.'com_prayercenter'.DS.'controller.php');
require( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_prayercenter'.DS.'helpers'.DS.'admin_pc_includes.php' );
// Require specific controller if requested
if($controller = JRequest::getWord('controller', '')) {
	$path = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_prayercenter'.DS.'controllers'.DS.$controller.'.php';
	if (file_exists($path)) {
		require_once $path;
	} else {
		$controller = '';
	}
}
// Create the controller
$classname	= 'PrayerCenterController'.ucfirst($controller);
$controller	= new $classname( );
//JResponse::setHeader( 'Expires', 'Mon, 26 Jul 1997 05:00:00 GMT', true );
// Perform the Request task
$controller->execute( JRequest::getCmd( 'task' ) );
$controller->redirect();
