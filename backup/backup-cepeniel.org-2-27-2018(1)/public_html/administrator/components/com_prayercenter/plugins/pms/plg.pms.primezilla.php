<?php
// ******************************************************************************
// Title          PrayerCenter Component for Joomla
// Author         Mike Leeper
// Plugin         Primezilla Private Messaging (requires Primezilla 1.0.5 or above)
// License        This is free software and you may redistribute it under the GPL.
//                PrayerCenter comes with absolutely no warranty. For details, 
//                see the license at http://www.gnu.org/licenses/gpl.txt
//                YOU ARE NOT REQUIRED TO KEEP COPYRIGHT NOTICES IN
//                THE HTML OUTPUT OF THIS SCRIPT. YOU ARE NOT ALLOWED
//                TO REMOVE COPYRIGHT NOTICES FROM THE SOURCE CODE.
// *******************************************************************************
defined( '_JEXEC' ) or die( 'Restricted access' );
class PCPrimezillaPMSPlugin extends PCPluginHelper {
  function pcpmsloadvars($newrequesterid){
    global $now, $senderid, $pmsItemId;
    global $iLoggingIsEnabled,$bSendEmail,$iEmailNotificationIsAllowed,$iEmailNotificationIsChangeable,$bUseEmailNotification,$iEmailNotificationIsDefault,$sEmailHeader,$sEmailFooter,$sEmailBanner,$sEmailSubject;
    DEFINE('MOSLANG','english'); //For backwards compatibility of language file of Primezilla which is not Joomla 1.5 native
    if (file_exists(JPATH_ROOT.DS.'components'.DS.'com_primezilla'.DS.'preferences.php')) {
      include_once(JPATH_ROOT.DS.'components'.DS.'com_primezilla'.DS.'preferences.php');
    }
    if (file_exists(JPATH_ROOT.DS.'components'.DS.'com_primezilla'.DS.'language'.DS.MOSLANG.'.php')) {
      include_once(JPATH_ROOT.DS.'components'.DS.'com_primezilla'.DS.'language'.DS.MOSLANG.'.php');
    } else {
      include_once(JPATH_ROOT.DS.'components'.DS.'com_primezilla'.DS.'language'.DS.'english.php');
    }
    $pmsItemId = $this->getPMSItemID();
    $senderid = 62;
    return true;
  }
  function pcpmsloaddb($senderid, $recipid, $message, $now, $config, $prayerrequest=null, $subject=null, $time=null){
    global $iLoggingIsEnabled;
    $message = $this->removeEvilTags(addslashes(JText::_($message)));
    $db	=& JFactory::getDBO();
    $sql = "INSERT INTO #__primezilla_inbox ( userid, userid_from, username_from, msg_date, msg_time, subject, message ) VALUES ( '".(int)$recipid->id."', '".(int)$senderid."', ".$db->quote($db->getEscaped($sender),false).", CURRENT_DATE(), CURRENT_TIME(), ".$db->quote($db->getEscaped($subject),false).", ".$db->quote($db->getEscaped($message),false)." )";
    $db->setQuery( $sql );
      if ($db->getErrorNum()) {
         echo $db->stderr();
        } else {
         $db->query();
         }
      $msgid = mysql_insert_id();
      if ( $iLoggingIsEnabled == 1) {
      $sql = "INSERT INTO #__primezilla_log ( userid_from, userid_to, msg_date, msg_time ) VALUES ( '".(int)$senderid."', '".(int)$recipid->id."', CURRENT_DATE(), CURRENT_TIME()	)";
      $db->setQuery( $sql );
        if ($db->getErrorNum()) {
           echo $db->stderr();
          } else {
           $db->query();
           }
        }
    return $msgid;
  }
  function pcpmsloadsmail($insID, $var_fromid, $var_toid, $var_message, $emn_option, $config){
    global $bSendEmail,$iEmailNotificationIsAllowed,$iEmailNotificationIsChangeable,$bUseEmailNotification,$iEmailNotificationIsDefault,$sEmailHeader,$sEmailFooter,$sEmailBanner,$sEmailSubject;
    $db	=& JFactory::getDBO();
    jimport( 'joomla.mail.helper' );
    $app =& JFactory::getApplication();
    $sitename = $app->getCfg( 'sitename' );
    $mailfrom = $app->getCfg('mailfrom');
    $var_message = $this->removeEvilTags(addslashes(JText::_($var_message)));
    $db->setQuery( "SELECT email FROM #__users WHERE id='".(int)$var_toid->id."'");
    $sRecipientEmail = $db->loadResult();
    $bSendEmail = 0;
    if ( $iEmailNotificationIsAllowed == 1 ) {
        if ( $iEmailNotificationIsChangeable == 1 ) {
            $bSendEmail = $bUseEmailNotification;
        }
        else {
            if ( $iEmailNotificationIsDefault == 1 ) {
                $bSendEmail = 1;
            }
        }
    }
    if ( $bSendEmail == 1 ) {
        $sLink = JRoute::_("index.php?option=com_primezilla&page=read&box=in&msgid=".(int)$insID."&Itemid=".(int)$pmsItemId);
        $sEmailBody  = $sEmailHeader;
        $sEmailBody .= $sLink;
        $sEmailBody .= $sEmailFooter;
        if( $iEmailUseBanner == 1) {
            $sEmailBody .= $sEmailBanner;
        }
      JUtility::sendMail($mail_from, $sitename, $sRecipientEmail->email, $sEmailSubject, $sEmailBody);
    }
    return true;
  }
  function removeEvilTags($source)
  {
    $allowedTags = '<br>';
    $source = strip_tags($source, $allowedTags);
    return preg_replace('/<(.*?)>/ie', "'<'.removeEvilAttributes('\\1').'>'", $source);
  }
  function removeEvilAttributes($tagSource)
  {
    $stripAttrib = 'javascript:|onclick|ondblclick|onmousedown|onmouseup|onmouseover|onmousemove|onmouseout|onkeypress|onkeydown|onkeyup';
    return stripslashes(preg_replace("/$stripAttrib/i", 'forbidden', $tagSource));
  }
  function getPMSItemID() {
    $component	= &JComponentHelper::getComponent('com_primezilla');
  	$menu		= &JSite::getMenu();
  	$items		= $menu->getItems('componentid', $component->id);
    $pmsItemId = $items[0]->id;
    return $pmsItemId;
  }
}
?>