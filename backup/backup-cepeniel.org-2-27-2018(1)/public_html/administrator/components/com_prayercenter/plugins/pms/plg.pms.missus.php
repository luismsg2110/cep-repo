<?php
// ******************************************************************************
// Title          PrayerCenter Component for Joomla
// Author         Mike Leeper
// Plugin         Missus Private Messaging (requires Missus 1.0 or above)
// License        This is free software and you may redistribute it under the GPL.
//                PrayerCenter comes with absolutely no warranty. For details, 
//                see the license at http://www.gnu.org/licenses/gpl.txt
//                YOU ARE NOT REQUIRED TO KEEP COPYRIGHT NOTICES IN
//                THE HTML OUTPUT OF THIS SCRIPT. YOU ARE NOT ALLOWED
//                TO REMOVE COPYRIGHT NOTICES FROM THE SOURCE CODE.
// *******************************************************************************
defined( '_JEXEC' ) or die( 'Restricted access' );
class PCMissusPMSPlugin extends PCPluginHelper {
  function pcpmsloadvars($newrequesterid){
    global $now, $senderid, $Send_Mail, $Send_Mail_Msg, $Send_Mail_Data;
    $app =& JFactory::getApplication();
    $offset = $app->getCfg( 'config.offset' );
    if (file_exists(JPATH_ROOT.DS.'components'.DS.'com_missus'.DS.'com_missus_settings.php')) {
      include_once(JPATH_ROOT.DS.'components'.DS.'com_missus'.DS.'com_missus_settings.php');
    }
    if (file_exists(JPATH_ROOT.DS.'components'.DS.'com_missus'.DS.'language'.DS.MOSLANG.'.php')) {
      include_once(JPATH_ROOT.DS.'components'.DS.'com_missus'.DS.'language'.DS.MOSLANG.'.php');
    } else {
      include_once(JPATH_ROOT.DS.'components'.DS.'com_missus'.DS.'language'..DS'english.php');
    }
    if (file_exists(JPATH_COMPONENT.DS.'pc_config.php')) {
      include_once(JPATH_COMPONENT.DS.'pc_config.php');
    }
   	$now = date( "Y-m-d H:i:s", time() + $offset + date("I") );
    $senderid = 62;
    return true;
  }
  function pcpmsloaddb($senderid, $recipid, $message, $now, $config, $prayerrequest=null, $subject=null, $time=null){
  	$message = substr( $this->my_specialchars( $message, array() ), 0 );
    $db	=& JFactory::getDBO();
		$sql = "INSERT INTO #__missus (senderid,sendername,sendermail,datesended,subject,message,broadcast) VALUES ($senderid,".$db->quote($db->getEscaped($prayerrequest),false).", '', ".$db->quote($db->getEscaped($now),false).", ".$db->quote($db->getEscaped($subject),false).", ".$db->quote($db->getEscaped($message),false).", 0)"; 
		$db->setQuery($sql);
		if (!$db->query()) {
			die("SQL error" . $db->stderr(true));
		}	
		$msgid = $db->insertid();
		$db->setQuery( "INSERT INTO #__missus_receipt (id,receptorid) VALUES (".(int)$msgid.", ".(int)$recipid->id.")" );
		if (!$db->query()) {
			die("SQL error" . $db->stderr(true));
		}	
    return true;
  }
  function pcpmsloadsmail($insID, $var_fromid, $var_toid, $var_message, $emn_option, $config){
    global $Send_Mail, $Send_Mail_Msg, $Send_Mail_Data;
  	$var_message = substr( $this->my_specialchars( $var_message, array() ), 0 );
    jimport( 'joomla.mail.helper' );
    $app =& JFactory::getApplication();
    $sitename = $app->getCfg( 'sitename' );
    $mailfrom = $app->getCfg('mailfrom');
    if ( $Send_Mail ){
  		$body = _MISSUS_MAIL_NEWMSSG_BODY1;
  		if ( $Send_Mail_Data ) {
  			$body .= _MISSUS_MAIL_NEWMSSG_BODY2;
    		$body = preg_replace('/<a(.*?)>/ie',"",$body);
    		$body = preg_replace('/<\/a>/ie',"",$body);
  			$body .= _MISSUS_FROM_SHOW.' '.$prayerrequest.'<br>';
  			$body .= _MISSUS_DATE_SHOW.' '.$now.'<br>';
  			$body .= _MISSUS_SUBJECT_SHOW.' '.$subject.'<br>';
  			if ( $Send_Mail_Msg ) $body .= stripslashes($var_message).'<br>';
  		}
  		$body .= _MISSUS_MAIL_NEWMSSG_BODY_LAST;
   		$body = str_replace("<br>","\n",$body);
 			$mail_to = $var_toid->email;
 			$mail_user_name = $var_toid->username;
      $pms_subject = _MISSUS_MAIL_NEWMSSG_SUB;
 			$head= "MIME-Version: 1.0\n";
 			$head .= "Content-type: multipart/mixed; charset=iso-8859-1\n";
 			$head .= "X-Priority: 1\n";
 			$head .= "X-MSMail-Priority: High\n";
 			$head .= "X-Mailer: php\n";
 			$head .= "From: \"".$sitename."\" <".$mailfrom.">\n";
 			@mail($mail_to, $pms_subject, $body, $head);
    }
    return true;
  }
  function my_specialchars( $content, $format ) 
  {	$ret = $content;
  	//PREVENT EVIL HTML TAGS FOR DESTROYING LAYOUT
  	$ret = preg_replace("/(\<script)(.*?)(script>)/si", "", $ret);
  	$ret = preg_replace("/(\<style)(.*?)(style>)/si", "", $ret);
  	$ret = preg_replace("/(\<head)(.*?)(head>)/si", "", $ret);
  	$ret = preg_replace("/(\<table)(.*?)(table>)/si", "", $ret);
  	$ret = preg_replace("/(\<span)(.*?)(span>)/si", "", $ret);
  	$ret = preg_replace("/(\<input)(.*?)(>)/si", "", $ret);
  	//STRIP HTML TAGS AND RESPECT ADDITIONAL LINE BREAKS	
  	$ret = trim(strip_tags($ret, '<'.implode('><', $format).'>'));
  	//REPLACE SPECIAL CHARS		 
  	$ret = str_replace("<!--", "&lt;!--", $ret);
  	$ret = preg_replace("/(\<!--)(.*?)(-->)/mi", "", $ret);
  	$ret = preg_replace("/(\<)(.*?)(--\>)/mi", "", $ret); 
  	return $ret;
  }
}
?>