<?php
// ******************************************************************************
// Title          PrayerCenter Component for Joomla
// Author         Mike Leeper
// Plugin         JIM Private Messaging (requires JIM 1.0.1 or above)
// License        This is free software and you may redistribute it under the GPL.
//                PrayerCenter comes with absolutely no warranty. For details, 
//                see the license at http://www.gnu.org/licenses/gpl.txt
//                YOU ARE NOT REQUIRED TO KEEP COPYRIGHT NOTICES IN
//                THE HTML OUTPUT OF THIS SCRIPT. YOU ARE NOT ALLOWED
//                TO REMOVE COPYRIGHT NOTICES FROM THE SOURCE CODE.
// *******************************************************************************
defined( '_JEXEC' ) or die( 'Restricted access' );
class PCJimPMSPlugin extends PCPluginHelper {
  function pcpmsloadvars($newrequesterid){
    global $now, $senderid, $JimConfig;
    DEFINE('MOSLANG','english'); //For backwards compatibility of language file of Primezilla which is not Joomla 1.5 native
    $app =& JFactory::getApplication();
    $offset = $app->getCfg( 'config.offset' );
    if (file_exists(JPATH_ROOT.DS.'components'.DS.'com_jim'.DS.'config.jim.php')) {
      include_once(JPATH_ROOT.DS.'components'.DS.'com_jim'.DS.'config.jim.php');
    }
    if (file_exists(JPATH_ROOT.DS.'components'.DS.'com_jim'.DS.'language'.DS.MOSLANG.'php')) {
      include_once(JPATH_ROOT.DS.'components'.DS.'com_jim'.DS.'language'.DS.MOSLANG.'php');
    } else {
      include_once(JPATH_ROOT.DS.'components'.DS.'com_jim'.DS.'language'.DS.'english.php');
    }
    if (file_exists(JPATH_COMPONENT.DS.'pc_config.php')) {
      include_once(JPATH_COMPONENT.DS.'pc_config.php');
    }
   	$now = date( "Y-m-d H:i:s", time() + $offset + date('I') );
    $senderid = null;
    return true;
  }
  function pcpmsloaddb($senderid, $recipid, $message, $now, $config, $prayerrequest=null, $subject=null, $time=null){
    $db	=& JFactory::getDBO();
    $message = $this->removeEvilTags(addslashes(JText::_($message)));
    $sql="INSERT INTO #__jim ( id, username, whofrom, date, readstate, subject, message) VALUES ('',".$db->quote($db->getEscaped($recipid->username),false).",".$db->quote($db->getEscaped($prayerrequest),false).",".$db->quote($db->getEscaped($now),false).",0,".$db->quote($db->getEscaped($subject),false).",".$db->quote($db->getEscaped($message),false).")";
  	$db->setQuery($sql);
  	if (!$db->query()) {
  		die("SQL error" . $db->stderr(true));
  	}	
    return true;
  }
  function pcpmsloadsmail($insID, $var_fromid, $var_toid, $var_message, $emn_option, $config){
    global $JimConfig;
    jimport( 'joomla.mail.helper' );
    $livesite = JURI::base();
    $app =& JFactory::getApplication();
    $sitename = $app->getCfg( 'sitename' );
    $mailfrom = $app->getCfg('mailfrom');
    $var_message = $this->removeEvilTags(addslashes(JText::_($var_message)));
    $sender = JText::_('PCTITLE').'@';
    if ($JimConfig["emailnotify"]) {
 			$mail_to = $var_toid->email;
 			$mail_user_name = $var_toid->username;
      $mail_from = preg_replace('/(.*?)\@/i', $sender, $mailfrom); 
 			$m_sub = sprintf( _JIM_MAILSUB, $sitename);
 			$m_msg = sprintf( _JIM_MAILMSG,  $mail_user_name, $sender, $sitename, $livesite, $sitename );
 			$head= "MIME-Version: 1.0\n";
 			$head .= "Content-type: text/html; charset=iso-8859-1\n";
 			$head .= "X-Priority: 1\n";
 			$head .= "X-MSMail-Priority: High\n";
 			$head .= "X-Mailer: php\n";
 			$head .= "From: \"".$sitename."\" <".$mail_from.">\n";
 			@mail($mail_to, $m_sub, $m_msg, $head);
 		}
    return true;
  }
  function removeEvilTags($source)
  {
    $allowedTags = '<br>';
    $source = strip_tags($source, $allowedTags);
    return preg_replace('/<(.*?)>/ie', "'<'.removeEvilAttributes('\\1').'>'", $source);
  }
  function removeEvilAttributes($tagSource)
  {
    $stripAttrib = 'javascript:|onclick|ondblclick|onmousedown|onmouseup|onmouseover|onmousemove|onmouseout|onkeypress|onkeydown|onkeyup';
    return stripslashes(preg_replace("/$stripAttrib/i", 'forbidden', $tagSource));
  }
}
?>