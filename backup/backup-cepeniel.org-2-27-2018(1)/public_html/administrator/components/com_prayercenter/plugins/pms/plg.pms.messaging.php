<?php
// ******************************************************************************
// Title          PrayerCenter Component for Joomla
// Author         Mike Leeper
// Plugin         Messaging Private Messaging (requires Messaging 1.5 or above)
// License        This is free software and you may redistribute it under the GPL.
//                PrayerCenter comes with absolutely no warranty. For details, 
//                see the license at http://www.gnu.org/licenses/gpl.txt
//                YOU ARE NOT REQUIRED TO KEEP COPYRIGHT NOTICES IN
//                THE HTML OUTPUT OF THIS SCRIPT. YOU ARE NOT ALLOWED
//                TO REMOVE COPYRIGHT NOTICES FROM THE SOURCE CODE.
// *******************************************************************************
defined( '_JEXEC' ) or die( 'Restricted access' );
class PCMessagingPMSPlugin extends PCPluginHelper {
  function pcpmsloadvars($newrequesterid){
    global $now, $senderid;
    jimport('joomla.utilities.date');
    $lang =& Jfactory::getLanguage();
    $lang->load( 'com_messaging', JPATH_SITE); 
    $app =& JFactory::getApplication();
    $senderid = 62;
    $dateset = new JDate(gmdate('Y-m-d H:i:s'));
    $dateset->setOffset($app->getCfg( 'config.offset' ) + date('I'));
    $now = $dateset->toFormat('%Y-%m-%d %H:%M:%S');
    return true;
  }
  function pcpmsloaddb($senderid, $recipid, $message, $now, $config, $prayerrequest=null, $subject=null, $time=null){
    $db	=& JFactory::getDBO();
    $sql = "INSERT INTO #__messaging (n,idFrom,idTo,subject,message,seen,date) VALUES ('','".(int)$senderid."',".$db->quote($db->getEscaped($recipid->id),false).",".$db->quote($db->getEscaped($subject),false).",".$db->quote($db->getEscaped($message),false).",0,".$db->quote($db->getEscaped($now),false).")";
		$db->setQuery($sql);
		if (!$db->query()) {
			die("SQL error" . $db->stderr(true));
		}	
		$query = "SELECT messageLimit FROM #__messaging_groups WHERE n=8";
		$db->setQuery($query);
		$rows = $db->loadObjectList();
    return $rows[0]->messageLimit;
  }
  function pcpmsloadsmail($insID, $var_fromid, $var_toid, $var_message, $emn_option, $config){
    $app =& JFactory::getApplication();
		if($insID)//send notify
		{
		   $mailer =& JFactory::getMailer();
		   $mailer->setSender(array($app->getCfg('mailfrom'), 'Messaging - '.$app->getCfg('fromname')));
		   $mailer->setSubject(JText::_('YOURECEIVEDMESSAGE'));
		   $body = JText::_('HELLO').' '.$var_toid->name.','."\n"."\n";
		   $body .= JText::_('YOURECEIVEDMESSAGEFROM').' '.$sender."\n"."\n";
		   $body .= $app->getCfg('sitename').' ('.JURI::root().')';
		   $mailer->setBody($body);
		   $mailer->addRecipient($var_toid->email);
		   $mailer->IsHTML(0);
		   $mailer->Send();
		}
    return true;
  }
}
?>