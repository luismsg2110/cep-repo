<?php
// ******************************************************************************
// Title          PrayerCenter Component for Joomla
// Author         Mike Leeper
// Plugin         MyPMS II Private Messaging (requires MyPMS II 2.0)
// License        This is free software and you may redistribute it under the GPL.
//                PrayerCenter comes with absolutely no warranty. For details, 
//                see the license at http://www.gnu.org/licenses/gpl.txt
//                YOU ARE NOT REQUIRED TO KEEP COPYRIGHT NOTICES IN
//                THE HTML OUTPUT OF THIS SCRIPT. YOU ARE NOT ALLOWED
//                TO REMOVE COPYRIGHT NOTICES FROM THE SOURCE CODE.
// *******************************************************************************
defined( '_JEXEC' ) or die( 'Restricted access' );
class PCMypms2PMSPlugin extends PCPluginHelper {
  function pcpmsloadvars($newrequesterid){
    global $now, $senderid, $time;
    DEFINE('MOSLANG','english'); //For backwards compatibility of language file of Primezilla which is not Joomla 1.5 native
    if (file_exists(JPATH_ROOT.DS.'components'.DS.'com_pms'.DS.'language'.DS.MOSLANG.'.php')) {
      include_once(JPATH_ROOT.DS.'components'.DS.'com_pms'.DS.'language'.DS.MOSLANG.'.php');
    } else {
      include_once(JPATH_ROOT.DS.'components'.DS.'com_pms'.DS.'language'.DS.'english.php');
    }
    if (file_exists(JPATH_COMPONENT.DS.'pc_config.php')) {
      include_once(JPATH_COMPONENT.DS.'pc_config.php');
    }
		$time = date('H:i:s');
		$now = date('Y-m-d');
		$senderid = null;
    return true;
  }
  function pcpmsloaddb($senderid, $recipid, $message, $now, $config, $prayerrequest=null, $subject=null, $time=null){
    $message = $this->removeBadTags(addslashes(JText::_($message)));
    $db	=& JFactory::getDBO();
    $sql="INSERT INTO #__pms ( id, groupname, username, whofrom, date, time, readstate, subject, message) VALUES ('','',".$db->quote($db->getEscaped($recip->username),false).",".$db->quote($db->getEscaped($sender),false).",".$db->quote($db->getEscaped($now),false).",".$db->quote($db->getEscaped($time),false).",'',".$db->quote($db->getEscaped($subject),false).",".$db->quote($db->getEscaped($message),false).")";
    $db->setQuery($sql);
		if (!$db->query()) {
			die("SQL error" . $db->stderr(true));
  		}	
    return true;
  }
  function pcpmsloadsmail($insID, $var_fromid, $var_toid, $var_message, $emn_option, $config){
    return true;
  }
}
?>