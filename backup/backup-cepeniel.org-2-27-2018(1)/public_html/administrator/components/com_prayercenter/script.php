<?php
/**
* @version $Id: script.php,v 1.6x - 2.5x
* @package PrayerCenter
*/
defined ( '_JEXEC' ) or die ( 'Restricted Access' );
class com_prayercenterInstallerScript
{
  private $release = '2.5.2';
  private $params;
  function install($parent)
  {
    $this->params = $this->getParams();
//    $this->setParams($params);
//    $parent->getParent()->setRedirectURL('index.php?option=com_prayercenter');
  }
  function uninstall($parent)
  {
//    echo '<p>'.JText::_('COM_PRAYERCENTER_UNINSTALL_TEXT').'</p>';
  }
  function update($parent)
  {
//    echo '<p>'.JText::_('COM_PRAYERCENTER_UPDATE_TEXT').'</p>';
  }
  function preflight($type,$parent)
  {
    $JVersion = new JVersion();
    if(version_compare($JVersion->getShortVersion(), '1.6', 'lt')){
      JError::raiseWarning(null, 'Cannot install com_prayercenter in a Joomla release prior to 1.6');
      return false;
    }
    if($type == 'update'){
      include_once('components/com_prayercenter/helpers/pc_version.php');
      $pcversion = & PCVersion::getInstance();
      $oldrelease = $pcversion->getShortVersion();
      $rel = $oldrelease.' to '.$this->release;
      if(version_compare($this->release, $oldrelease, 'le')){
        JError::raiseWarning(null, 'Incorrect version sequence.  Cannot upgrade '.$rel);
        return false;
      } else {
        $rel = $this->release;
      }
    }
//    echo '<p>'.JText::_('COM_PRAYERCENTER_PREFLIGHT_'.$type.'_TEXT').'</p>';
  }
  function getParams(){
    $xml = JFactory::getXML(JPATH_ROOT.'/administrator/components/com_prayercenter/config.xml');
    $ini = array();
    $fieldsets = $xml->fields->fieldset;
    $fieldscount = count($fieldsets);
    for($i=0;$i<$fieldscount;$i++){
    	if( ! count($fieldsets[$i]->children())) {
    		return null;
    	}
    	foreach ($fieldsets[$i] as $field)
    	{
				if (($name = $field->attributes()->name) === null) {
					continue;
				}
				if (($value = $field->attributes()->default) === null) {
					continue;
				}
    		if ($name != '@spacer') {
      		$ini[(string) $name] = (string) $value;
    		}
      }
    }
    return $ini;
  }
  function setParams($param_array){
    if(count($param_array) > 0){
      $db = JFactory::getDbo();
      foreach($param_array as $name => $value){
        $params['params'][(string)$name] = (string)$value;
      }
      $paramString = json_encode($params);
      $db->setQuery('UPDATE #__extensions SET params='.$db->quote($paramString).' WHERE element="com_prayercenter"');
      $db->query();
    }
  }
  function setRules($param_array){
    if(count($param_array) > 0){
      $db = JFactory::getDbo();
      $query = $db->getQuery(true);
      $query->select('a.rules');
      $query->from('#__assets AS a');
      $query->group('a.id, a.rules, a.lft');
      $query->where('(a.name = ' . $db->quote('com_prayercenter')  . ')');
      $db->setQuery($query);
      $result = $db->loadColumn();
      if($result[0] == '{}'){
        $rules = json_encode(array('prayercenter.view'=>array($param_array['config_user_view']=>1),'prayercenter.post'=>array($param_array['config_user_post']=>1),'prayercenter.publish'=>array($param_array['config_user_publish']=>1),'prayercenter.subscribe'=>array($param_array['config_user_subscribe']=>1),'prayercenter.devotional'=>array($param_array['config_user_view_devotional']=>1),'prayercenter.links'=>array($param_array['config_user_view_links']=>1)));
        $db->setQuery("UPDATE #__assets SET rules=".$db->quote($rules)." WHERE name='com_prayercenter'");
        $db->query();
      }
    }
  }
  function postflight($type,$parent)
  {
//    echo '<p>'.JText::_('COM_PRAYERCENTER_POSTFLIGHT_'.$type.'_TEXT').'</p>';
    if($type == 'update'){
      $pcParams = JComponentHelper::getParams('com_prayercenter');
      $pcParamsArray = $pcParams->toArray();
      foreach($pcParamsArray['params'] as $name => $value){
        $pcConfig[(string)$name] = (string)$value;
      }
      $this->setRules($pcConfig);
      $parent->getParent()->setRedirectURL('index.php?option=com_installer&view=update');
    } elseif($type == 'install'){
      $this->setParams($this->params);
      $this->setRules($this->params);
      $parent->getParent()->setRedirectURL('index.php?option=com_prayercenter');
    }
  }
}
?>