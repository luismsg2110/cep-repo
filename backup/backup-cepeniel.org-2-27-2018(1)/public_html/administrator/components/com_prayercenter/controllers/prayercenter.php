<?php
/**
* PrayerCenter Component
* 
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
*/
defined('_JEXEC') or die( 'Restricted access' );
jimport('joomla.application.component.controller');
class PrayerCenterControllerPrayerCenter extends PrayerCenterController
{
	function __construct( $default = array() )
	{
		parent::__construct( $default );
		$this->registerTask( 'apply', 'save' );
		$this->registerTask( 'unapprove',	'approve' );
		$this->registerTask( 'unarchive',	'archive' );
		$this->registerTask( 'unpublish',	'publish' );
		$this->registerTask( 'unpublishlink',	'publishlink' );
		$this->registerTask( 'unpublishdevotion',	'publishdevotion' );
		$this->registerTask( 'prayer',	'praise' );
		$this->registerTask( 'hidereq',	'displayreq' );
	}
  function uploadfile( $option='com_prayercenter' ) {
    global $prayercenteradmin;
    if((!empty($_FILES['uploadedfile'])) && ($_FILES['uploadedfile']['error'] == 0)) {
      $filename = basename($_FILES['uploadedfile']['name']);
      $ext = substr($filename, strrpos($filename, '.') + 1);
      if (($ext == "php") && ($_FILES['uploadedfile']['size'] < 350000)) {
          $newname = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_prayercenter'.DS.'plugins'.DS.$filename;
          if (!file_exists($newname)) {
            if ((move_uploaded_file($_FILES['uploadedfile']['tmp_name'],$newname))) {
            	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_files", "The file has been saved as: ".$newname );
            } else {
             	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_files", "Error: A problem occurred during file upload!" );
            }
          } else {
          	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_files", "Error: File ".$_FILES['uploadedfile']['name']." already exists" );
          }
      } else {
         $prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_files","Error: Only .php files under 350Kb are accepted for upload");
      }
    } else {
    	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_files", "Error: No file uploaded" );
    }
  }
  function deletefile( $option='com_prayercenter' )
  {
    global $prayercenteradmin;
    $file = JRequest::getVar( 'file', null, 'method', '' );
    $plugin_path = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_prayercenter'.DS.'plugins'.DS;
  	if (count( $file )) {
  		unlink($plugin_path.$file);
  	}
  	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_files", "File has been deleted." );
  }
  function uploadLangfile( $option='com_prayercenter' ) {
    global $prayercenteradmin;
    if((!empty($_FILES['uploadedlangfile'])) && ($_FILES['uploadedlangfile']['error'] == 0)) {
      $filename = basename($_FILES['uploadedlangfile']['name']);
      $foldername = substr($filename,0,-21);
      $ext = substr($filename, strrpos($filename, '.') + 1);
      if (($ext == "ini") && ($_FILES['uploadedlangfile']['size'] < 350000)) {
          $newname = JPATH_ROOT.DS.'language'.DS.$foldername.DS.$filename;
          if (!file_exists($newname)) {
            if ((move_uploaded_file($_FILES['uploadedlangfile']['tmp_name'],$newname))) {
            	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_files", "The file has been saved as: ".$newname );
            } else {
             	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_files", "Error: A problem occurred during file upload!" );
            }
          } else {
          	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_files", "Error: File ".$_FILES['uploadedlangfile']['name']." already exists" );
          }
      } else {
         $prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_files","Error: Only .ini files under 350Kb are accepted for upload");
      }
    } else {
    	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_files", "Error: No file uploaded" );
    }
  }
  function deleteLangfile( $option='com_prayercenter' )
  {
    global $prayercenteradmin;
    $file = JRequest::getVar( 'file', null, 'method', '' );
    $lang_folder = substr($file,0,-21);
    $lang_path = JPATH_ROOT.DS.'language'.DS.$lang_folder.DS;
  	if (count( $file )) {
  		unlink($lang_path.$file);
  	}
  	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_files", "File has been deleted." );
  }
  function uploadimage( $option='com_prayercenter' ) {
    global $prayercenteradmin;
    if((!empty($_FILES['uploadedimage'])) && ($_FILES['uploadedimage']['error'] == 0)) {
      $imagename = basename($_FILES['uploadedimage']['name']);
      $ext = substr($imagename, strrpos($imagename, '.') + 1);
      if (($ext == "jpg" | $ext == "png" | $ext == "gif") && ($_FILES['uploadedimage']['size'] < 350000)) {
          $newname = JPATH_ROOT.DS.'components'.DS.'com_prayercenter'.DS.'assets'.DS.'images'.DS.$imagename;
          if (!file_exists($newname)) {
            if ((move_uploaded_file($_FILES['uploadedimage']['tmp_name'],$newname))) {
            	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_files", "The image has been saved as: ".$newname );
            } else {
             	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_files", "Error: A problem occurred during image upload!" );
            }
          } else {
          	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_files", "Error: Image ".$_FILES['uploadedimage']['name']." already exists" );
          }
      } else {
         $prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_files","Error: Only image files under 350Kb are accepted for upload");
      }
    } else {
    	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_files", "Error: No image uploaded" );
    }
  }
  function deleteimage( $option='com_prayercenter' )
  {
    global $prayercenteradmin;
    $image = JRequest::getVar( 'image', null, 'method', 'string' );
    $image_path = JPATH_ROOT.DS.'components'.DS.'com_prayercenter'.DS.'assets'.DS.'images'.DS;
  	if (count( $image )) {
  		unlink($image_path.$image);
  	}
  	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_files", "Image file has been deleted." );
  }
  function uploadssimage( $option='com_prayercenter' ) {
    global $prayercenteradmin;
    if((!empty($_FILES['uploadedssimage'])) && ($_FILES['uploadedssimage']['error'] == 0)) {
      $imagename = basename($_FILES['uploadedssimage']['name']);
      $ext = substr($imagename, strrpos($imagename, '.') + 1);
      if (($ext == "jpg" | $ext == "png" | $ext == "gif") && ($_FILES['uploadedssimage']['size'] < 350000)) {
          $newname = JPATH_ROOT.DS.'components'.DS.'com_prayercenter'.DS.'assets'.DS.'images'.DS.'slideshow'.DS.$imagename;
          if (!file_exists($newname)) {
            if ((move_uploaded_file($_FILES['uploadedssimage']['tmp_name'],$newname))) {
            	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_files", "The image has been saved as: ".$newname );
            } else {
             	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_files", "Error: A problem occurred during image upload!" );
            }
          } else {
          	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_files", "Error: Image ".$_FILES['uploadedssimage']['name']." already exists" );
          }
      } else {
         $prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_files","Error: Only image files under 350Kb are accepted for upload");
      }
    } else {
    	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_files", "Error: No image uploaded" );
    }
  }
  function deletessimage( $option='com_prayercenter' )
  {
    global $prayercenteradmin;
    $image = JRequest::getVar( 'image', null, 'method', 'string' );
    $image_path = JPATH_ROOT.DS.'components'.DS.'com_prayercenter'.DS.'assets'.DS.'images'.DS.'slideshow'.DS;
  	if (count( $image )) {
  		unlink($image_path.$image);
  	}
  	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_files", "Image file has been deleted." );
  }
  function doPRUpgrade( $option='com_prayercenter' ) {
    global $prayercenteradmin, $db;
  	$db		=& JFactory::getDBO();
  	$filePath = JPATH_COMPONENT;
  	$checkfileName = 'prayerrequest_copied_checkfile';
   	touch($filePath.DS.$checkfileName);
      $db->setQuery( "SELECT * FROM #__prayerrequests");
      $prdata = $db->loadObjectList();
        foreach($prdata as $prrequests)
        {
        $date = date('Y-m-d',strtotime($prrequests->date));
        $time = date('h:i:s',strtotime($prrequests->date));
        $db->setQuery( "INSERT INTO #__prayercenter (id,requester,date,time,request,publishstate,archivestate,displaystate,sendto,email,praise) VALUES ('',".$db->quote($db->getEscaped($prrequests->requester),false).",".$db->quote($db->getEscaped($date),false).",".$db->quote($db->getEscaped($time),false).",".$db->quote($db->getEscaped($prrequests->request),false).",'".(int)$prrequests->approved."','".(int)$prrequests->archived."','".(int)$prrequests->display."','".(int)$prrequests->distribute."',".$db->quote($db->getEscaped($prrequests->email),false).",'0')");
      	$db->query();
        }
  		if (!$db->query()) {
			return JError::raiseWarning( 500, $db->stderr() );
  		}
  		else {
        $prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=support", "Prayer Requests Data Migrated Into PrayerCenter" );
      }
  }
  function syncsub( $option='com_prayercenter' ){
    global $prayercenteradmin, $db;
 		$app = JFactory::getApplication();
    jimport('joomla.utilities.date');
  	$db		=& JFactory::getDBO();
    $db->setQuery("SELECT email FROM #__users WHERE block='0'");
    $uemail = $db->loadObjectList();
    $db->setQuery("SELECT email FROM #__prayercenter_subscribe");
    $pcemail = $db->loadObjectList();
    $dateset = new JDate(gmdate('Y-m-d H:i:s'));
    $dateset->setOffset($app->getCfg( 'config.offset' ) + date('I'));
    $date = $dateset->toFormat('%Y-%m-%d');
    $count = 0;
    foreach($uemail as $ue){
      if(!$this->pca_recursive_in_array($ue->email,$pcemail))
      {
        $db->setQuery( "INSERT INTO #__prayercenter_subscribe (id,email,date,approved) VALUES ('',".$db->quote($db->getEscaped($ue->email),false).",".$db->quote($db->getEscaped($date),false).",'0')");
    		if (!$db->query()) {
  			return JError::raiseWarning( 500, $db->stderr() );
    		}
        $count++;
       }
    }
     $prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_sub", $count." Joomla registered users email addresses syncronized with PrayerCenter subscriber list." );
  }
  function pca_recursive_in_array($needle, $haystack) {
      foreach ($haystack as $stalk) {
          if ($needle == $stalk->email || (is_array($stalk) && $this->pca_recursive_in_array($needle, $stalk))) {
              return true;
          }
      }
      return false;
  }
  function remove_req( $option='com_prayercenter' ) {
    global $pcConfig, $prayercenteradmin, $db;
		$cid	= JRequest::getVar( 'cid', array(0), 'post', 'array' );
  	$db		=& JFactory::getDBO();
  	if (count( $cid )) {
  		$cids = implode( ',', $cid );
  		$db->setQuery( "DELETE FROM #__prayercenter WHERE id IN ($cids)" );
  		if (!$db->query()) {
			return JError::raiseWarning( 500, $db->getError() );
  		}
    }
    if($pcConfig['config_comments'] == 1) {
      $jcomments = JPATH_SITE . DS .'components' . DS . 'com_jcomments' . DS . 'jcomments.php';
      if (file_exists($jcomments)) {
        require_once($jcomments);
        foreach($cid as $delid){  
          JCommentsModel::deleteComments($delid, 'com_prayercenter');
        }
      }
    } elseif($pcConfig['config_comments'] == 2) {
      $jsc = JPATH_SITE.DS.'components'.DS.'com_jsitecomments'.DS.'helpers'.DS.'jsc_class.php';
      if (file_exists($jsc)) {
        require_once($jsc);
        foreach($cid as $delid){  
          jsitecomments::JSCdelComment('com_prayercenter', $delid);
        }
      }
    }
  	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_req" );
  }
  function remove_link( $option='com_prayercenter' ) {
    global $prayercenteradmin, $db;
		$cid	= JRequest::getVar( 'cid', array(0), 'post', 'array' );
  	$db		=& JFactory::getDBO();
  	if (count( $cid )) {
  		$cids = implode( ',', $cid );
  		$db->setQuery( "DELETE FROM #__prayercenter_links WHERE id IN ($cids)" );
  		if (!$db->query()) {
			return JError::raiseWarning( 500, $db->getError() );
  		}
  	}
  	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_link" );
  }
  function remove_devotion( $option='com_prayercenter' ) {
    global $prayercenteradmin, $db;
		$cid	= JRequest::getVar( 'cid', array(0), 'post', 'array' );
  	$db		=& JFactory::getDBO();
  	if (count( $cid )) {
  		$cids = implode( ',', $cid );
  		$db->setQuery( "DELETE FROM #__prayercenter_devotions WHERE id IN ($cids)" );
  		if (!$db->query()) {
			return JError::raiseWarning( 500, $db->getError() );
  		}
  	}
  	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_dev" );
  }
  function remove_sub( $option='com_prayercenter' ) {
    global $prayercenteradmin, $db;
		$cid	= JRequest::getVar( 'cid', array(0), 'post', 'array' );
  	$db		=& JFactory::getDBO();
  	if (count( $cid )) {
  		$cids = implode( ',', $cid );
  		$db->setQuery( "DELETE FROM #__prayercenter_subscribe WHERE id IN ($cids)" );
  		if (!$db->query()) {
			return JError::raiseWarning( 500, $db->getError() );
  		}
  	}
  	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_sub" );
  }
  function cancelSettings( $option='com_prayercenter'){
    global $prayercenteradmin;
  	$prayercenteradmin->PCRedirect( 'index.php?option='.$option );
  }
  function canceledit( $option='com_prayercenter'){
    global $prayercenteradmin;
		$cid	= JRequest::getVar( 'id', null, 'post', 'int' );
		$model =& $this->getModel('prayercenter');
		$model->checkin();
  	$prayercenteradmin->PCRedirect( 'index.php?option='.$option.'&task=manage_req' );
  }
  function canceleditlink( $option='com_prayercenter'){
    global $prayercenteradmin;
		$cid	= JRequest::getVar( 'id', null, 'post', 'int' );
		$model =& $this->getModel('prayercenter');
		$model->checkin();
  	$prayercenteradmin->PCRedirect( 'index.php?option='.$option.'&task=manage_link' );
  }
  function canceleditlang( $option='com_prayercenter'){
    global $prayercenteradmin;
  	$prayercenteradmin->PCRedirect( 'index.php?option='.$option.'&task=manage_lang' );
  }
  function canceleditdevotion( $option='com_prayercenter'){
    global $prayercenteradmin;
		$cid	= JRequest::getVar( 'id', null, 'post', 'int' );
		$model =& $this->getModel('prayercenter');
		$model->checkin();
  	$prayercenteradmin->PCRedirect( 'index.php?option='.$option.'&task=manage_dev' );
  }
  function publish( $option='com_prayercenter' ) {
  	global $pcConfig, $db, $prayercenteradmin;
 		$db	= & JFactory::getDBO();
		$cid	= JRequest::getVar( 'cid', array(0), 'post', 'array' );
 		$action	= JRequest::getVar( 'task', null, 'method' );
    if($action == 'publish'){
      $publish = true;
      } else {
      $publish = false;
      }
    $count = count($cid);
  	if (!is_array( $cid ) || $count < 1 || $cid[0] == 0) {
    	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_req", 'Select an item to '.$action );
  	}
    for ($i = 0; $i < $count; $i++) {
  	$db->setQuery( "UPDATE #__prayercenter SET publishstate='".(int)$publish."'"
  	. "\nWHERE id='".$cid[$i]."'" );
  	if (!$db->query()) {
			return JError::raiseWarning( 500, $db->stderr() );
  	}
  	if (count( $cid ) == 1) {
  		$model =& $this->getModel('prayercenter');
  		$model->checkin();
  	}
    if($action == 'publish'){
    $lang =& Jfactory::getLanguage();
    $lang->load( 'com_prayercenter', JPATH_SITE); 
    $db->setQuery("SELECT * FROM #__prayercenter WHERE id='".$cid[$i]."'");
    $publishedq = $db->loadObjectList();
    $published = $publishedq[0];
    $newrequest = $published->request;
    $newrequester = $published->requester;
    $newemail = $published->email;
    $sendpriv = $published->displaystate;
    $sessionid = $published->sessionid;
    if($sendpriv){
      $prayercenteradmin->PCAemail_notification($newrequest,$newrequester,$newemail,0,$sendpriv);
      $prayercenteradmin->PCAemail_prayer_chain($newrequest,$newrequester);
      if($pcConfig['config_distrib_type'] > 1 && !empty($pcConfig['config_pms_plugin'])){
        $prayercenteradmin->PCAsendPM($newrequesterid,$newrequester,$newrequest,$newemail,$sendpriv);
      }
    } 
    elseif(!$sendpriv){
      $prayercenteradmin->PCAemail_notification($newrequest,$newrequester,$newemail,0,$sendpriv,$cid[$i],$sessionid);
      if($pcConfig['config_distrib_type'] > 1 && !empty($pcConfig['config_pms_plugin'])){
        $prayercenteradmin->PCAsendPM($newrequesterid,$newrequester,$newrequest,$newemail,$sendpriv,$cid[$i],$sessionid);
        }
      }
     }
    }
  	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_req" );
  }
  function publishlink( $option='com_prayercenter' ) {
    global $prayercenteradmin, $db;
 		$db	= & JFactory::getDBO();
		$cid	= JRequest::getVar( 'cid', array(0), 'post', 'array' );
 		$action	= JRequest::getVar( 'task', null, 'method' );
    if($action == 'publishlink'){
      $publish = true;
      } else {
      $publish = false;
      }
    $count = count($cid);
  	if (!is_array( $cid ) || $count < 1 || $cid[0] == 0) {
    	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_link", 'Select an item to '.$action );
  	}
    for ($i = 0; $i < $count; $i++) {
  	$db->setQuery( "UPDATE #__prayercenter_links SET published='".(int)$publish."'"
  	. "\nWHERE id='".$cid[$i]."'" );
  	if (!$db->query()) {
			return JError::raiseWarning( 500, $db->stderr() );
  	}
  	if (count( $cid ) == 1) {
  		$model =& $this->getModel('prayercenter');
  		$model->checkin();
  	 }
  	}
  	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_link" );
  	}
  function publishdevotion( $option='com_prayercenter' ) {
    global $prayercenteradmin, $db;
 		$db	= & JFactory::getDBO();
		$cid	= JRequest::getVar( 'cid', array(0), 'post', 'array' );
 		$action	= JRequest::getVar( 'task', null, 'method' );
    if($action == 'publishdevotion'){
      $publish = true;
      } else {
      $publish = false;
      }
    $count = count($cid);
  	if (!is_array( $cid ) || $count < 1 || $cid[0] == 0) {
    	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_dev", 'Select an item to '.$action );
  	}
    for ($i = 0; $i < $count; $i++) {
  	$db->setQuery( "UPDATE #__prayercenter_devotions SET published='".(int)$publish."'"
  	. "\nWHERE id='".$cid[$i]."'" );
  	if (!$db->query()) {
			return JError::raiseWarning( 500, $db->stderr() );
  	}
  	if (count( $cid ) == 1) {
  		$model =& $this->getModel('prayercenter');
  		$model->checkin();
  	 }
  	}
  	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_dev" );
  	}
  function displayreq( $option='com_prayercenter' ) {
    global $prayercenteradmin, $db;
		$cid	= JRequest::getVar( 'cid', array(0), 'post', 'array' );
 		$action	= JRequest::getVar( 'task', null, 'method' );
    if($action == 'displayreq'){
      $display = true;
      } else {
      $display = false;
      }
    $count = count($cid);
  	if (!is_array( $cid ) || $count < 1 || $cid[0] == 0) {
    	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_req", 'Select an item to '.rtrim( "req", $action ));
  	}
    for ($i = 0; $i < $count; $i++) {
    $db	=& JFactory::getDBO();
  	$db->setQuery( "UPDATE #__prayercenter SET displaystate='".(int)$display."'"
  	. "\nWHERE id='".$cid[$i]."'" );
  	if (!$db->query()) {
			return JError::raiseWarning( 500, $db->stderr() );
  	}
  	if (count( $cid ) == 1) {
  		$model =& $this->getModel('prayercenter');
  		$model->checkin();
  	}
     }
  	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_req" );
  }
  function praise( $cid=null, $praise, $option='com_prayercenter' ) {
    global $prayercenteradmin, $db;
		$cid	= JRequest::getVar( 'cid', array(0), 'post', 'array' );
 		$action	= JRequest::getVar( 'task', null, 'method' );
    if($action == 'praise'){
      $praise = true;
      } else {
      $praise = false;
      }
    $count = count($cid);
  	if (!is_array( $cid ) || $count < 1 || $cid[0] == 0) {
    	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_req", 'Select an item to '.$action );
  	}
    for ($i = 0; $i < $count; $i++) {
    $db	=& JFactory::getDBO();
  	$db->setQuery( "UPDATE #__prayercenter SET praise='".(int)$praise."'"
  	. "\nWHERE id='".$cid[$i]."'" );
  	if (!$db->query()) {
			return JError::raiseWarning( 500, $db->stderr() );
  	}
  	if (count( $cid ) == 1) {
  		$model =& $this->getModel('prayercenter');
  		$model->checkin();
  	}
     }
  	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_req" );
  }
  function approve( $option='com_prayercenter' ) {
    global $db, $prayercenteradmin;
		$cid	= JRequest::getVar( 'cid', array(0), 'post', 'array' );
 		$action	= JRequest::getVar( 'task', null, 'method' );
    if($action == 'approve'){
      $approve = true;
      } else {
      $approve = false;
      }
    $count = count($cid);
  	if (!is_array( $cid ) || $count < 1 || $cid[0] == 0) {
    	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_sub", 'Select an item to '.$action );
  	}
    for ($i = 0; $i < $count; $i++) {
    $db	=& JFactory::getDBO();
  	$db->setQuery( "UPDATE #__prayercenter_subscribe SET approved='".(int)$approve."'"
  	. "\nWHERE id=$cid[$i]" );
  	if (!$db->query()) {
			return JError::raiseWarning( 500, $db->stderr() );
  	}
  	$db->setQuery( "SELECT email FROM #__prayercenter_subscribe"
  	. "\nWHERE id='".$cid[$i]."'" );
    $email = $db->loadObjectList();
  	if (!$db->query()) {
			return JError::raiseWarning( 500, $db->stderr() );
  	}
  	if (count( $cid ) == 1) {
  		$model =& $this->getModel('prayercenter');
  		$model->checkin();
  	}
    if($action == 'approve'){
     $prayercenteradmin->PCAemail_subscribe($email[0]->email);
     }
    }
  	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_sub" );
  }
  function archive( $option='com_prayercenter' ) {
    global $prayercenteradmin, $db;
		$cid	= JRequest::getVar( 'cid', array(0), 'post', 'array' );
 		$action	= JRequest::getVar( 'task', null, 'method' );
    if($action == 'archive'){
      $archive = true;
      } else {
      $archive = false;
      }
    $count = count($cid);
  	if (!is_array( $cid ) || $count < 1 || $cid[0] == 0) {
    	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_req", 'Select an item to '.$action );
  	}
    for ($i = 0; $i < $count; $i++) {
    $db	=& JFactory::getDBO();
  	$db->setQuery( "UPDATE #__prayercenter SET archivestate='".(int)$archive."'"
  	. "\nWHERE id='".$cid[$i]."'"	);
  	if (!$db->query()) {
			return JError::raiseWarning( 500, $db->stderr() );
  	}
  	if (count( $cid ) == 1) {
  		$model =& $this->getModel('prayercenter');
  		$model->checkin();
  	}
      }
  	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_req" );
  }
  function purge( $option='com_prayercenter' )
  {
    global $pcConfig, $db, $prayercenteradmin;
    if($pcConfig['config_comments'] == 1) {
      $jcomments = JPATH_SITE . DS .'components' . DS . 'com_jcomments' . DS . 'jcomments.php';
      if (file_exists($jcomments)) {
        require_once($jcomments);
      }
    } elseif($pcConfig['config_comments'] == 2) {
      $jsc = JPATH_SITE.DS.'components'.DS.'com_jsitecomments'.DS.'helpers'.DS.'jsc_class.php';
      if (file_exists($jsc)) {
        require_once($jsc);
      }
    }
    $config_request_retention = $pcConfig['config_request_retention'];
    $config_archive_retention = $pcConfig['config_archive_retention'];
    $count = 0;
    $totalcount = 0;
    $db	=& JFactory::getDBO();
    $db->setQuery("SELECT * FROM #__prayercenter WHERE DATEDIFF(CURDATE(),date) >= ".$config_request_retention." AND archivestate='0'");
    $result = $db->loadObjectList();
    $db->setQuery("SELECT * FROM #__prayercenter WHERE DATEDIFF(CURDATE(),date) >= ".$config_archive_retention." AND archivestate='1'");
    $archiveresult = $db->loadObjectList();
    $db->setQuery("SELECT count(*) FROM #__prayercenter");
    $totalreqcount = $db->loadResult();
    $requestcount = count($result);
    $archivecount = count($archiveresult);
    $totalcount = ($requestcount + $archivecount);
    if ($totalcount < 1)
      {
        $msg = 'There are no requests to purge';
      } else {
        foreach($result as $results)
        {
           $db->setQuery("DELETE FROM #__prayercenter WHERE id='".(int)$results->id."'");
           $db->Query();
           $count++;
            if($pcConfig['config_comments'] > 0) {
              if (file_exists($jcomments)) {
                  JCommentsModel::deleteComments($results->id, 'com_prayercenter');
              } elseif (file_exists($jsc)) {
                  jsitecomments::JSCdelComment('com_prayercenter', $results->id);
              }
            }
          }
        foreach($archiveresult as $archiveresults)
        {
           $db	=& JFactory::getDBO();
           $db->setQuery("DELETE FROM #__prayercenter WHERE id='".(int)$archiveresults->id."'");
           $db->Query();
           $count++;
            if($pcConfig['config_comments'] > 0) {
              if (file_exists($jcomments)) {
                  JCommentsModel::deleteComments($archiveresults->id, 'com_prayercenter');
              } elseif (file_exists($jsc)) {
                  jsitecomments::JSCdelComment('com_prayercenter', $archiveresults->id);
              }
            }
        }
        if ($count > 0)
          {
            $msg = $count.' of '.$totalreqcount.' Requests Purged';
          } else {
            $msg = 'No Requests Purged';
          }
      }
    $prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_req", $msg );
  }
  function saveCss( $option='com_prayercenter' ) {
    global $prayercenteradmin;
		JRequest::checkToken() or jexit( 'Invalid Token' );
  	$config_css = JRequest::getVar( 'config_css', null, 'post', 'string' );
  		$configcss = str_replace("[CR][NL]","\n",$config_css);
  		$configcss = str_replace("[ES][SQ]","'",$configcss);
  		$configcss = nl2br($configcss);
  		$configcss = str_replace("<br />"," ",$configcss);
  		$filename = JPATH_ROOT.DS.'components'.DS.'com_prayercenter'.DS.'assets'.DS.'css'.DS.'prayercenter.css';
      file_put_contents($filename, $configcss);
    $prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_css", "Changes in CSS have been saved." );
  }
  function saveLang( $option='com_prayercenter' ) {
    global $prayercenteradmin;
		JRequest::checkToken() or jexit( 'Invalid Token' );
    $foldername = JRequest::getVar( 'config_langfolder', null, 'post', 'string' );
    $filename = JRequest::getVar( 'config_langfile', null, 'post', 'string' );
  	$config_lang = JRequest::getVar( 'config_lang', null, 'post', 'string' );
  		$configlang = str_replace("[CR][NL]","\n",$config_lang);
  		$configlang = str_replace("[ES][SQ]","'",$configlang);
  		$configlang = nl2br($configlang);
  		$configlang = str_replace("<br />"," ",$configlang);
  		$langfilepath = JPATH_ROOT.DS.'language'.DS.$foldername;
      if(!is_dir($langfilepath) || !file_exists($langfilepath.DS.$foldername.'.xml')) $prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_lang", "Please install the corresponding Joomla language extension for ".$foldername.".com_prayercenter.ini" );
 			$ldata = JApplicationHelper::parseXMLLangMetaFile($langfilepath.DS.$foldername.'.xml');
  			if (!is_array($ldata)) {
  				$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_lang", "Please install the valid Joomla language extension for ".$foldername.".com_prayercenter.ini" );
  			}
      $langfilename = $langfilepath.DS.$foldername.".com_prayercenter.ini";
  		file_put_contents($langfilename, $configlang);
    $prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_lang", "Changes in the language file have been saved." );
  }
  function resetCss( $option='com_prayercenter' ) {
    global $prayercenteradmin;
  		$savfilename = JPATH_ROOT.DS.'components'.DS.'com_prayercenter'.DS.'assets'.DS.'css'.DS.'prayercenter.sav';
  		$savfilecontent = file_get_contents($savfilename);
  		$replacecss = str_replace("[CR][NL]","\n",$savfilecontent);
  		$replacecss = str_replace("[ES][SQ]","'",$replacecss);
  		$replacecss = nl2br($replacecss);
  		$replacecss = str_replace("<br />"," ",$replacecss);
  		$filename = JPATH_ROOT.DS.'components'.DS.'com_prayercenter'.DS.'assets'.DS.'css'.DS.'prayercenter.css';
  		file_put_contents($filename, $replacecss);
    $prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_css", "CSS has been reset to default settings." );
  }
  function resetLang( $option='com_prayercenter' ) {
    global $prayercenteradmin;
      $JVersion = new JVersion();
      if( (real)$JVersion->RELEASE == 1.5 ) {
    		$savfilename = JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_prayercenter'.DS.'langsource'.DS.'j15'.DS.'en-GB.com_prayercenter.ini';
      } elseif( (real)$JVersion->RELEASE >= 1.6 ){
      	$savfilename = JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_prayercenter'.DS.'langsource'.DS.'j17'.DS.'en-GB.com_prayercenter.ini';
      }
  		$savfilecontent = file_get_contents($savfilename);
  		$replacelang = str_replace("[CR][NL]","\n",$savfilecontent);
  		$replacelang = str_replace("[ES][SQ]","'",$replacelang);
  		$replacelang = nl2br($replacelang);
  		$replacelang = str_replace("<br />"," ",$replacelang);
  		$filename = JPATH_ROOT.DS.'language'.DS.'en-GB'.DS.'en-GB.com_prayercenter.ini';
  		file_put_contents($filename, $replacelang);
    $prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_lang", "The PrayerCenter English language file has been reset to default settings." );
  }
  function savereq( $option='com_prayercenter' ) {
    global $prayercenteradmin, $db;
		JRequest::checkToken() or jexit( 'Invalid Token' );
  	$db =& JFactory::getDBO();
    $app =& JFactory::getApplication();
    jimport('joomla.utilities.date');
    $dateset = new JDate(gmdate('Y-m-d H:i:s'));
    $dateset->setOffset($app->getCfg( 'config.offset' ) + date('I'));
    $time = $dateset->toFormat('%H:%M:%S');
    $date = $dateset->toFormat('%Y-%m-%d');
    $postrequest = nl2br(addslashes(JText::_(JRequest::getVar( 'postrequest', null, 'post', 'string' ))));
    $posttopic = JRequest::getVar( 'posttopic', null, 'post', 'int' );
    $posttitle = nl2br(addslashes(JText::_(JRequest::getVar( 'posttitle', null, 'post', 'string' ))));
    $id = JRequest::getVar( 'id', null, 'post', 'int' );
   	$save = "UPDATE #__prayercenter SET request=".$db->quote($db->getEscaped($postrequest),false).",title=".$db->quote($db->getEscaped($posttitle),false).",topic=".(int)$posttopic.", date=".$db->quote($db->getEscaped($date),false).", time=".$db->quote($db->getEscaped($time),false)." WHERE id='".(int)$id."'";
    $db->setQuery($save);
  	if (!$db->query()) {
			return JError::raiseWarning( 500, $db->getError() );
  	}
		$model =& $this->getModel('prayercenter');
		$model->checkin();
  	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_req" );
  }
  function savelink( $option='com_prayercenter' ) {
    global $prayercenteradmin, $db;
		JRequest::checkToken() or jexit( 'Invalid Token' );
  	$db =& JFactory::getDBO();
		$post	= JRequest::get('post');
		$id	= JRequest::getVar( 'id', null, 'post', 'int' );
  	$row =& JTable::getInstance('prayercenterlinks', 'Table');
		$post['ordering'] = $row->getNextOrder( '' );
    if($id > 0){
   	$save = "UPDATE #__prayercenter_links SET name='".addslashes(JText::_($post['title']))."', url='".$post['url']."', alias='".addslashes(JText::_($post['alias']))."', descrip='".addslashes(JText::_($post['description']))."', published='".$post['published']."', ordering='".$post['ordering']."' WHERE id='".(int)$id."'";
    } else {
    $save = "INSERT INTO #__prayercenter_links (id,name,url,alias,descrip,published,checked_out,checked_out_time,ordering) VALUES ('','".addslashes(JText::_($post['title']))."','".$post['url']."','".addslashes(JText::_($post['alias']))."','".addslashes(JText::_($post['description']))."','".$post['published']."','','','".$post['ordering']."')";
    }
    $db->setQuery($save);
  	if (!$db->query()) {
			return JError::raiseWarning( 500, $db->getError() );
  	}
		$row->reorder();
		$model =& $this->getModel('prayercenter');
		$model->checkin();
  	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_link" );
  }
  function savedevotion( $option='com_prayercenter' ) {
    global $prayercenteradmin, $db;
		JRequest::checkToken() or jexit( 'Invalid Token' );
  	$db =& JFactory::getDBO();
		$post	= JRequest::get('post');
		$id	= JRequest::getVar( 'id', null, 'post', 'int' );
  	$row =& JTable::getInstance('prayercenterdevotions', 'Table');
		$post['ordering'] = $row->getNextOrder( '' );
    if($id > 0){
   	$save = "UPDATE #__prayercenter_devotions SET name='".addslashes(JText::_($post['title']))."', feed='".$post['feed']."', published='".$post['published']."', ordering='".$post['ordering']."' WHERE id='".(int)$id."'";
    } else {
    $save = "INSERT INTO #__prayercenter_devotions (id,name,feed,published,checked_out,checked_out_time,ordering) VALUES ('','".addslashes(JText::_($post['title']))."','".$post['feed']."','".$post['published']."','','','".$post['ordering']."')";
    }
    $db->setQuery($save);
  	if (!$db->query()) {
			return JError::raiseWarning( 500, $db->getError() );
  	}
		$row->reorder();
		$model =& $this->getModel('prayercenter');
		$model->checkin();
  	$prayercenteradmin->PCRedirect( "index.php?option=".$option."&task=manage_dev" );
  }
  function orderup()
	{
    global $prayercenteradmin;
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$model =& JModel::getInstance('links', 'prayercentermodel');
		$model->move(-1);
		$prayercenteradmin->PCRedirect( 'index.php?option=com_prayercenter&task=manage_link');
	}
	function orderdown()
	{
    global $prayercenteradmin;
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$model =& JModel::getInstance('links', 'prayercentermodel');
		$model->move(1);
		$prayercenteradmin->PCRedirect( 'index.php?option=com_prayercenter&task=manage_link');
	}
	function orderupdev()
	{
    global $prayercenteradmin;
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$model =& JModel::getInstance('devotions', 'prayercentermodel');
		$model->move(-1);
		$prayercenteradmin->PCRedirect( 'index.php?option=com_prayercenter&task=manage_dev');
	}
	function orderdowndev()
	{
    global $prayercenteradmin;
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$model =& JModel::getInstance('devotions', 'prayercentermodel');
		$model->move(1);
		$prayercenteradmin->PCRedirect( 'index.php?option=com_prayercenter&task=manage_dev');
	}
	function saveorderdevotions()
	{
    global $prayercenteradmin;
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$cid 	= JRequest::getVar( 'cid', array(), 'post', 'array' );
    $order 	= JRequest::getVar( 'order', array(), 'post', 'array' );
		JArrayHelper::toInteger($cid);
		JArrayHelper::toInteger($order);
		$model =& JModel::getInstance('devotions', 'prayercentermodel');
		$model->saveorderdevotions($cid, $order);
		$msg = JText::_( 'New ordering saved' );
		$prayercenteradmin->PCRedirect( 'index.php?option=com_prayercenter&task=manage_dev', $msg );
	}
	function saveorderlinks()
	{
    global $prayercenteradmin;
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$cid 	= JRequest::getVar( 'cid', array(), 'post', 'array' );
    $order 	= JRequest::getVar( 'order', array(), 'post', 'array' );
		JArrayHelper::toInteger($cid);
		JArrayHelper::toInteger($order);
		$model =& JModel::getInstance('links', 'prayercentermodel');
		$model->saveorderlinks($cid, $order);
		$msg = JText::_( 'New ordering saved' );
		$prayercenteradmin->PCRedirect( 'index.php?option=com_prayercenter&task=manage_link', $msg );
	}
  function save( $option='com_prayercenter' ) {
    global $prayercenteradmin;
  		JRequest::checkToken() or jexit( 'Invalid Token' );
  		$component = JRequest::getCmd( 'option' );
  		$table =& JTable::getInstance('component');
  		$post['params'] = JRequest::getVar('params', array(0), '', 'array');

// Save the rules.
      if (isset($post['params']) && isset($post['params']['rules'])) {
      	$rules	= new JAccessRules($post['params']['rules']);
      	$asset	= JTable::getInstance('asset');
      	$asset->rules = (string) $rules;
      	if (!$asset->check() || !$asset->store()) {
      		$this->setError($asset->getError());
      		return false;
      	}
      	unset($post['params']['rules']);
      }

  		if (!$table->loadByOption( $component ))
  		{
  			JError::raiseWarning( 500, 'Not a valid component' );
  			return false;
  		}
  		$table->bind( $post );
  		if (!$table->check()) {
  			JError::raiseWarning( 500, $table->getError() );
  			return false;
  		}
  		if (!$table->store()) {
  			JError::raiseWarning( 500, $table->getError() );
  			return false;
  		}
  		$task = JRequest::getCmd( 'task' );
  		switch ($task)
  		{
  			case 'apply':
  				$link = 'index.php?option='.$option.'&task=config';
  				break;
  			case 'save':
  			default:
  				$link = 'index.php?option='.$option;
  				break;
  		}
  	$msg	= JText::_( 'Settings successfully Saved' );
  	$prayercenteradmin->PCRedirect( $link, $msg );
  }
}
?>