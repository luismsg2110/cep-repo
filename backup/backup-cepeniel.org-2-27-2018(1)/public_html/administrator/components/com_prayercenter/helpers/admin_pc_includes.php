<?php
/**
* PrayerCenter Component
* 
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
*/
defined( '_JEXEC' ) or die( 'Restricted access' );
global $pcConfig,$prayercenteradmin;
$lang =& Jfactory::getLanguage();
$lang->load( 'com_prayercenter', JPATH_SITE); 
require( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_prayercenter'.DS.'helpers'.DS.'admin_pc_class.php' );
$prayercenteradmin = new prayercenteradmin();
$JVersion = new JVersion();
$pcParams = JComponentHelper::getParams('com_prayercenter');
if( (real)$JVersion->RELEASE == 1.5 ) {
  $pcConfig = $pcParams->toArray();
} elseif( (real)$JVersion->RELEASE >= 1.6 ) {
  $pcParamsArray = $pcParams->toArray();
  foreach($pcParamsArray['params'] as $name => $value){
    $pcConfig[(string)$name] = (string)$value;
  }
}
?>