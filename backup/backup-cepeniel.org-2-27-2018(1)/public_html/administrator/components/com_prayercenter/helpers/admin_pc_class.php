<?php
/**
* PrayerCenter Component
* 
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
*/
defined( '_JEXEC' ) or die( 'Restricted access' );
class prayercenteradmin 
{
	public static function addSubmenu($vName = 'prayercenter')
	{
		JSubMenuHelper::addEntry(
			JText::_('CPanel'),
			'index.php?option=com_prayercenter',
			$vName == 'prayercenter'
		);
		JSubMenuHelper::addEntry(
			JText::_('Requests'),
			'index.php?option=com_prayercenter&task=manage_req',
			$vName == 'managereq'
		);
		JSubMenuHelper::addEntry(
			JText::_('Subscribers'),
			'index.php?option=com_prayercenter&task=manage_sub',
			$vName == 'managesub'
		);
		JSubMenuHelper::addEntry(
			JText::_('CSS'),
			'index.php?option=com_prayercenter&task=manage_css',
			$vName == 'managecss'
		);
		JSubMenuHelper::addEntry(
			JText::_('Files'),
			'index.php?option=com_prayercenter&task=manage_files',
			$vName == 'managefiles'
		);
		JSubMenuHelper::addEntry(
			JText::_('Devotionals'),
			'index.php?option=com_prayercenter&task=manage_dev',
			$vName == 'managedev'
		);
		JSubMenuHelper::addEntry(
			JText::_('Links'),
			'index.php?option=com_prayercenter&task=manage_link',
			$vName == 'managelink'
		);
		JSubMenuHelper::addEntry(
			JText::_('Language Files'),
			'index.php?option=com_prayercenter&task=manage_lang',
			$vName == 'managelang'
		);
	}
	public static function getActions()
	{
		$user	= JFactory::getUser();
		$result	= new JObject;
		$assetName = 'com_prayercenter';
		$actions = array('core.admin', 'core.manage');
		foreach ($actions as $action) {
			$result->set($action,	$user->authorise($action, $assetName));
		}
		return $result;
	}
  function fullfileperms($perms)
  {
    if (($perms & 0xC000) == 0xC000) {
      // Socket
      $info = 's';
    } elseif (($perms & 0xA000) == 0xA000) {
      // Symbolic Link
      $info = 'l';
    } elseif (($perms & 0x8000) == 0x8000) {
      // Regular
      $info = '-';
    } elseif (($perms & 0x6000) == 0x6000) {
      // Block special
      $info = 'b';
    } elseif (($perms & 0x4000) == 0x4000) {
      // Directory
      $info = 'd';
    } elseif (($perms & 0x2000) == 0x2000) {
      // Character special
      $info = 'c';
    } elseif (($perms & 0x1000) == 0x1000) {
      // FIFO pipe
      $info = 'p';
    } else {
      // Unknown
      $info = 'u';
    }
    // Owner
    $info .= (($perms & 0x0100) ? 'r' : '-');
    $info .= (($perms & 0x0080) ? 'w' : '-');
    $info .= (($perms & 0x0040) ?
            (($perms & 0x0800) ? 's' : 'x' ) :
            (($perms & 0x0800) ? 'S' : '-'));
    // Group
    $info .= (($perms & 0x0020) ? 'r' : '-');
    $info .= (($perms & 0x0010) ? 'w' : '-');
    $info .= (($perms & 0x0008) ?
            (($perms & 0x0400) ? 's' : 'x' ) :
            (($perms & 0x0400) ? 'S' : '-'));
    // World
    $info .= (($perms & 0x0004) ? 'r' : '-');
    $info .= (($perms & 0x0002) ? 'w' : '-');
    $info .= (($perms & 0x0001) ?
            (($perms & 0x0200) ? 't' : 'x' ) :
            (($perms & 0x0200) ? 'T' : '-'));
    echo $info;
  }
  function findext($filename)
  {
    $filename = strtoupper($filename);
    $ext = preg_split("[\.]",$filename,-1,PREG_SPLIT_NO_EMPTY);
    $ext = $ext[1];
    return $ext;
  }
  function findimageext($filename)
  {
    $filename = strtoupper($filename);
    $ext = preg_split("[\.]",$filename,-1,PREG_SPLIT_NO_EMPTY);
    $n = count($ext)-1;
    $ext = $ext[$n];
    return $ext;
  }
  function FileSel( $name, &$active, $javascript=NULL, $directory=NULL ) {
    $pmsrefarray = array(
          1 => array ('val' => 'jim', 'desc' => ''.JText::_(' - (Requires JIM 1.0.1 or above)').''),
          2 => array ('val' => 'joomla', 'desc' => ''.JText::_(' - (Built-in Joomla Messaging Component. Requires Joomla 1.6 or above)').''),
          3 => array ('val' => 'messaging', 'desc' => ''.JText::_(' - (Requires Messaging 1.5 or above)').''),
          4 => array ('val' => 'missus', 'desc' => ''.JText::_(' - (Requires Missus 1.0 or above)').''),
          5 => array ('val' => 'mypms2', 'desc' => ''.JText::_(' - (Requires MyPMS II 2.0)').''),
          6 => array ('val' => 'primezilla', 'desc' => ''.JText::_(' - (Requires Primezilla 1.0.5 or above)').''),
          7 => array ('val' => 'privmsg', 'desc' => ''.JText::_(' - (Requires PrivMSG 2.1.0 or above)').''),
          8 => array ('val' => 'uddeim', 'desc' => ''.JText::_(' - (Requires uddeIM 1.8 or above)').'')
          );
		$SelFiles = JFolder::files( JPATH_COMPONENT.DS.$directory );
		$files 	= array(  JHTML::_( 'select.option', 0, '- Select -' ) );
		foreach ( $SelFiles as $file ) {
			if ( eregi( "php", $file ) ) {
			  preg_match('/^plg\.pms\.(.*)\.php$/',$file,$match);
				if($match){
          $keyarr = $this->pc_array_search_recursive($match[1], $pmsrefarray);
          $key = $keyarr[0];
          $pmsfile = $pmsrefarray[$key]['desc'];
          $files[] = JHTML::_( 'select.option', $match[1], ucfirst($match[1].$pmsfile) );
        }
			}
		}
		$files = JHTML::_( 'select.genericlist', $files, $name, 'class="inputbox" size="1" '. $javascript, 'value', 'text', $active );
		return $files;
  }
	function pc_get_php_setting($val, $colour=0, $yn=1) {
		$r =  (ini_get($val) == '1' ? 1 : 0);
		if ($colour) {
			if ($yn) {
				$r = $r ? '<span style="color: green;">ON</span>' : '<span style="color: red;">OFF</span>';
			} else {
				$r = $r ? '<span style="color: red;">ON</span>' : '<span style="color: green;">OFF</span>';
			}
			return $r;
		} else {
			return $r ? 'ON' : 'OFF';
		}
	}
	function pc_get_server_software() {
		if (isset($_SERVER['SERVER_SOFTWARE'])) {
			return $_SERVER['SERVER_SOFTWARE'];
		} else if (($sf = phpversion() <= '4.2.1' ? getenv('SERVER_SOFTWARE') : $_SERVER['SERVER_SOFTWARE'])) {
			return $sf;
		} else {
			return 'n/a';
		}
	}
	function PCfooter() {
    include_once('components/com_prayercenter/helpers/pc_version.php');
    $pcversion = & PCVersion::getInstance();
	  ?>
		<div align="center" class="small">
		  <i>"Lift up holy hands in prayer,and praise the Lord."</i>&nbsp;<a href="http://www.biblegateway.com/passage/?search=Psalm%20134:2;&version=51;" target="_blank"><b>Psalm 134:2</b></a>
		<br /><br />
		  <a href="<?php echo $pcversion->getUrl()?>" target="_blank">PrayerCenter Component - <?php echo $pcversion->getShortCopyright();?></a>
		</div>
	  <?php
	}
  function PCAerrorLog($msg){
  	jimport('joomla.error.log');
    jimport('joomla.utilities.date');
    $dateset = new JDate(gmdate('Y-m-d H:i:s'));
		$options['format'] = "{DATE} {TIME} {MESSAGE}";
		$log = JLog::getInstance('pcerrorlog.php', $options, JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_prayercenter'.DS.'logs');
		$pcerrorlog = array();
		$pcerrorlog['message'] = $msg;
		$pcerrorlog['time'] = $dateset->toFormat("%H:%M:%S(GMT)");
		$log->addEntry($pcerrorlog);
  }
  function PCAsendPM($newrequester,$newrequest,$newemail,$sendpriv,$lastid=null,$sessionid=null)
  {
    global $pcConfig;
    $pcpmsclassname = 'PC'.ucfirst($pcConfig['config_pms_plugin']).'PMSPlugin';
    if (!empty($pcConfig['config_pms_plugin']) && file_exists(JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_prayercenter'.DS.'plugins'.DS.'pms'.DS.'plg.pms.'.$pcConfig['config_pms_plugin'].'.php')) {
      require_once(JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_prayercenter'.DS.'helpers'.DS.'pc_plugin_class.php');
      $PCPluginHelper = new PCPluginHelper();
      $pluginfile = 'plg.pms.'.$pcConfig['config_pms_plugin'].'.php';
      $PCPluginHelper->importPlugin('pms',$pluginfile);
      $PCPMSPlugin = new $pcpmsclassname();
    } else {
      return;
    }
    $PCPMSPlugin->send_private_messaging($newrequester,$newrequest,$newemail,$sendpriv,$lastid,$sessionid);
  }
  function PCAsendmail($mail_from, $config_sender_name, $toemail, $email_subject, $body, $config_email_mode)
  {
    global $pcConfig;
    $config_email_bcc = $pcConfig['config_email_bcc'];
  	$mailer =& JFactory::getMailer();
  	$mailer->setSender(array($mail_from, $config_sender_name));
  	$mailer->setSubject($email_subject);
  	$mailer->setBody($body);
  	$mailer->IsHTML($config_email_mode);
    if ( $config_email_bcc && count($toemail) > 1 ) {
   		$mailer->addBCC($toemail);
    	$mailer->addRecipient($mail_from);
    } else {
   		$mailer->addRecipient($toemail);
    }
    $count = count($toemail);
  	$rs	= $mailer->Send();
  	if($pcConfig['config_error_logging']){
    	if ( JError::isError($rs) && $pcConfig['config_error_logging'] > 0 ) {
    		$msg	= $rs->getError();
    	} elseif(!$rs && $pcConfig['config_error_logging'] > 0) {
    		$msg = JText::_('The mail could not be sent. Cause undetermined.');
    	} elseif($pcConfig['config_error_logging'] == 2) {
    		$msg = JText::_( 'E-mail sent to '.$count.' recipients.' );
    	}
      if(isset($msg)) $this->PCAerrorLog($msg);
    }
  }
  function PCAemail_notification($message,$from_name,$from_email,$pms=false,$sendpriv,$lastid=null,$sessionid=null)
  {
    global $db, $pcConfig, $mainframe;
    jimport( 'joomla.mail.helper' );
    $lang =& Jfactory::getLanguage();
    $lang->load( 'com_prayercenter', JPATH_SITE); 
    $db		=& JFactory::getDBO();
    $JVersion = new JVersion();
    if($pms){
    $config_sender_name = $pms;
    }
    else {
    $config_sender_name = JText::_('PCEMAILSENDER');
    }
    $livesite = JURI::root();
    $app =& JFactory::getApplication();
    $sitename = $app->getCfg( 'sitename' );
    $config_email_mode = $pcConfig['config_email_mode'];
    $config_email_inc_req = $pcConfig['config_email_inc_req'];
    $config_use_admin_alert = $pcConfig['config_use_admin_alert'];
    $config_distrib_type = $pcConfig['config_distrib_type'];
    $config_email_list = $pcConfig['config_email_list'];
    $config_email_list = strip_tags($config_email_list);
    $emailArray = preg_split('/[,]/',$config_email_list, -1, PREG_SPLIT_NO_EMPTY);
    $config_email_request = $pcConfig['config_email_request'];
    $email_message = htmlentities(JText::_('PCEMAILMSG'),ENT_COMPAT,'UTF-8');
    $sendpriv ? $email_nomessage = htmlentities(JText::_('PCEMAILNOMSG'),ENT_COMPAT,'UTF-8') : $email_nomessage = htmlentities(JText::_('PCEMAILNOMSGPRIV'),ENT_COMPAT,'UTF-8');
    $email_subject = htmlentities(JText::_('PCEMAILSUBJECT'),ENT_COMPAT,'UTF-8');
    $viewer_name = htmlentities(JText::_('PCVIEWERNAME'),ENT_COMPAT,'UTF-8');
    $mail_from = $this->getPCReturnAddress();
    $message = stripslashes(JText::_($message));
  	$message = wordwrap($message,60,"\t\r\n");
    $sendpriv ? $link = "" : $link = 'index.php?option=com_prayercenter&task=view_request&id='.$lastid.'&&prv=1&pop=1&tmpl=component&sessionid='.$sessionid;
  	$slink = '<a href="'.$livesite.'" target="_blank">'.$sitename.'</a>';
    $mail_to = array();
      if($config_email_request == '0') {
          $resultusers = $this->PCgetAdminData();
          if(!empty($resultusers)){
            foreach($resultusers as $results) {
              $mail_to[] = $results->email;
            }
          }
      }
      if($config_email_request == '1'){
          $db->setQuery("SELECT name,email FROM #__users");
          $resultusers = $db->loadObjectList();
          if(!empty($resultusers)){
            foreach($resultusers as $results) {
              $mail_to[] = $results->email;
            }
          }
      }
      if($config_email_request == '2'){
          if(!empty($emailArray)){
            foreach($emailArray as $email) {
              $mail_to[] = trim($email);
            }
          }
      }
			if(!empty($from_email) && $config_email_mode == true) $from_id = '<a href="mailto:'.$from_email.'">'.$from_name.'</a>';
      if(!empty($from_email) && $config_email_mode == false)
      {
      $from_id = $from_name.' ('.$from_email.')';
      }elseif(empty($from_email)) {
        $from_id = $from_name;
      }
      if($config_email_mode == true){
        if($config_email_inc_req == true){
          $body = sprintf( $email_message, $viewer_name, $from_id, $slink, $livesite, $message );
        } else {
          $body = sprintf( $email_nomessage, $viewer_name, $from_id, $slink, $livesite, $link );
        }
        $body = str_replace("\n","<br />",$body);
      } else {
        if($config_email_inc_req == true){
          $body = sprintf( $email_message, $viewer_name, $from_id, $sitename, $livesite, $message );
        } else {
          $body = sprintf( $email_nomessage, $viewer_name, $from_id, $sitename, $livesite, $link );
        }
      }
    if(count($mail_to)>0) $this->PCAsendmail($mail_from, $config_sender_name, $mail_to, $email_subject, $body, $config_email_mode);
  }
  function PCAemail_prayer_chain($msg,$fromid)
  {
    global $db, $pcConfig, $mainframe;
    jimport( 'joomla.mail.helper' );
    $lang =& Jfactory::getLanguage();
    $lang->load( 'com_prayercenter', JPATH_SITE); 
    $db		=& JFactory::getDBO();
    $livesite = JURI::root();
    $app =& JFactory::getApplication();
    $sitename = $app->getCfg( 'sitename' );
    $config_email_mode = $pcConfig['config_email_mode'];
    $config_email_inc_req = $pcConfig['config_email_inc_req'];
    $config_sender_name = htmlentities(JText::_('PCEMAILSENDER'),ENT_COMPAT,'UTF-8');
    $config_email_message = htmlentities(JText::_('PCCHAINEMAILMSG'),ENT_COMPAT,'UTF-8');
    $config_email_nomessage = htmlentities(JText::_('PCCHAINEMAILNOMSG'),ENT_COMPAT,'UTF-8');
    $subject = htmlentities(JText::_('PCEMAILSUBJECT'),ENT_COMPAT,'UTF-8');
    $db->setQuery("SELECT * FROM #__prayercenter_subscribe WHERE approved='1'");
    $resultsubscribers = $db->loadObjectList();
    $subscriber_name = htmlentities(JText::_('PCSUBSCRIBERNAME'),ENT_COMPAT,'UTF-8');
    $message = stripslashes(JText::_($msg));
    $slink = '<a href="'.$livesite.'" target="_blank">'.$sitename.'</a>';
    $mail_from = $this->getPCReturnAddress();
    $mail_to = array();
    foreach($resultsubscribers as $subscribers){
      $mail_to[] = $subscribers->email;
    }
    if($config_email_mode == true){
      if($config_email_inc_req == true){
        $body = sprintf( $config_email_message, $subscriber_name, $fromid, $slink, $message );
      } else {
        $body = sprintf( $config_email_nomessage, $subscriber_name, $fromid, $slink, $slink );
      }
      $body = str_replace("\n","<br />",$body);
    } else {
      if($config_email_inc_req == true){
        $body = sprintf( $config_email_message, $subscriber_name, $fromid, $sitename, $message );
      } else {
        $body = sprintf( $config_email_nomessage, $subscriber_name, $fromid, $sitename, $livesite );
      }
    }
   if(count($mail_to)>0) $this->PCAsendmail($mail_from, $config_sender_name, $mail_to, $subject, $body, $config_email_mode);
  }
  function PCAemail_subscribe($email)
  {
    global $mainframe, $pcConfig;
    jimport( 'joomla.mail.helper' );
    $lang =& Jfactory::getLanguage();
    $lang->load( 'com_prayercenter', JPATH_SITE); 
    $livesite = JURI::root();
    $app = JFactory::getApplication();
    $sitename = $app->getCfg( 'sitename' );
    $config_email_mode = $pcConfig['config_email_mode'];
    $config_sender_name = JText::_('PCEMAILSENDER');
    $config_subscribe_email_message = JText::_('SUBSCRIBEMSG');
    $subject = JText::_('SUBSCRIBESUBJECT');
    $link = $livesite.'index.php?option=com_prayercenter&task=subscribe';
    $slink = '<a href="'.$livesite.'index.php?option=com_prayercenter&task=subscribe" target="_blank">'.$livesite.'index.php?option=com_prayercenter&task=subscribe</a>';
    if($config_email_mode == true) {
      $body = sprintf( $config_subscribe_email_message, $email, $sitename, $slink );
      $body = str_replace("\n","<br />",$body);
    }else{
      $body = sprintf( $config_subscribe_email_message, $email, $sitename, $link );
		}
    $mail_from = $this->getPCReturnAddress();
    $mail_to[] = $email;
    $this->PCAsendmail($mail_from, $config_sender_name, $mail_to, $subject, $body, $config_email_mode);
  }
  function getPCReturnAddress(){
    global $mainframe, $pcConfig;
    $lang =& Jfactory::getLanguage();
    $lang->load( 'com_prayercenter', JPATH_SITE); 
    $config_custom_ret_addr = $pcConfig['config_custom_ret_addr'];
    $config_return_addr = $pcConfig['config_return_addr'];
    $app =& JFactory::getApplication();
    $mailfrom = $app->getCfg('mailfrom');
    if( $config_return_addr == 0 || $config_return_addr == 2 ){
      $pc_mf = $mailfrom;
    } elseif( $config_return_addr == 1 ) {
      $pc_mf = $config_custom_ret_addr;
    } 
    $valid = preg_match( '/^[\w\.\-]+@\w+[\w\.\-]*?\.\w{1,4}$/', $pc_mf );
    if(!$valid){
      return JText::_('PCMAILFROM');
    } else {
      return $pc_mf; 
    }     
  }
  function PCgetTopics(){
    $topicArray = array (
         1 => array ('val' => '0', 'text' => ''.JText::_('PCSELECTTOPIC0').''),         
         2 => array ('val' => '1', 'text' => ''.JText::_('PCSELECTTOPIC1').''),         
         3 => array ('val' => '2', 'text' => ''.JText::_('PCSELECTTOPIC2').''),         
         4 => array ('val' => '3', 'text' => ''.JText::_('PCSELECTTOPIC3').''),         
         5 => array ('val' => '4', 'text' => ''.JText::_('PCSELECTTOPIC4').''),         
         6 => array ('val' => '5', 'text' => ''.JText::_('PCSELECTTOPIC5').''),         
         7 => array ('val' => '6', 'text' => ''.JText::_('PCSELECTTOPIC6').''),         
         8 => array ('val' => '7', 'text' => ''.JText::_('PCSELECTTOPIC7').''),         
         9 => array ('val' => '8', 'text' => ''.JText::_('PCSELECTTOPIC8').''),         
         10 => array ('val' => '9', 'text' => ''.JText::_('PCSELECTTOPIC9').''),         
         11 => array ('val' => '10', 'text' => ''.JText::_('PCSELECTTOPIC10').''),         
         12 => array ('val' => '11', 'text' => ''.JText::_('PCSELECTTOPIC11').''),         
         13 => array ('val' => '12', 'text' => ''.JText::_('PCSELECTTOPIC12').''),         
         14 => array ('val' => '13', 'text' => ''.JText::_('PCSELECTTOPIC13').''),         
         15 => array ('val' => '14', 'text' => ''.JText::_('PCSELECTTOPIC14').''),         
         16 => array ('val' => '15', 'text' => ''.JText::_('PCSELECTTOPIC15').''),         
         17 => array ('val' => '16', 'text' => ''.JText::_('PCSELECTTOPIC16').''),         
         18 => array ('val' => '17', 'text' => ''.JText::_('PCSELECTTOPIC17').''),         
         19 => array ('val' => '18', 'text' => ''.JText::_('PCSELECTTOPIC18').''),         
         20 => array ('val' => '19', 'text' => ''.JText::_('PCSELECTTOPIC19').''),         
         21 => array ('val' => '20', 'text' => ''.JText::_('PCSELECTTOPIC20').''), 
         22 => array ('val' => '21', 'text' => ''.JText::_('PCSELECTTOPIC21').''),       
         23 => array ('val' => '22', 'text' => ''.JText::_('PCSELECTTOPIC22').'')       
          );
    return $topicArray;
  }
	function PCquickiconButton( $link, $image, $text, $attrib="" )
	{
		global $mainframe;
		$lang		=& JFactory::getLanguage();
    $version = new JVersion();
    if( (real)$version->RELEASE == 1.5 ) {
  		$template	= $mainframe->getTemplate();
    } elseif( (real)$version->RELEASE >= 1.6 ){
      $template	= JFactory::getApplication()->getTemplate();
    }
		?>
			<div class="icon-wrapper"><div class="icon">
    <?php
			$image = JHTML::_('image.site',  $image, '/templates/'. $template .'/images/header/', NULL, NULL, strip_tags($text) ); 
			$image .=	'<span>'.$text.'</span>';
  		echo JHTML::_('link', JRoute::_($link), $image, $attrib);
    ?>
			</div>
		</div>
		<?php
	}
  function PCparseXml($xmlfile){
      $data = "";
			if (file_exists($xmlfile)){
			$data = JApplicationHelper::parseXMLInstallFile($xmlfile);
			}
    return $data;
	}
	 function PCChangeLog() {
	 	$output = '';
		$options = array();
		$options['rssUrl']		= 'http://www.mlwebtechnologies.com/index.php?option=com_content&view=category&id=53&format=feed';
		$options['cache_time']	= 86400;
		$rssDoc =& JFactory::getXMLparser('RSS', $options);
		if ( $rssDoc == false ) {
			$output = JText::_('Error: Feed not retrieved');
		} else {	
			$title 	= $rssDoc->get_title();
			$link	= $rssDoc->get_link();
			$output = '<table class="adminlist">';
			$items = array_slice($rssDoc->get_items(), 0, 3);
			$numItems = count($items);
      if($numItems == 0) {
      	$output .= '<tr><th>' .JText::_('PrayerCenter change log not available at this time'). '</th></tr>';
      } else {
       	$output .= '<tr><td><textarea cols="70" rows="40">';
       	$k = 0;
        for( $j = 0; $j < $numItems; $j++ ) {
          $item = $items[$j];
  				if($item->get_description()) {
           	$output .= ltrim($this->PCp2nl($item->get_description()),"pcnews\npcconfig\npclang\n");
					}
					$k = 1 - $k;
      }
    	$output .= '</textarea></td></tr>';
    }
		$output .= '</table>';
	 }	 	
	 	return $output;
	}
	function PClimitText($text, $wordcount)
	{
		if(!$wordcount) {
			return $text;
		}
		$texts = explode( ' ', $text );
		$count = count( $texts );
		if ( $count > $wordcount )
		{
			$text = '';
			for( $i=0; $i < $wordcount; $i++ ) {
				$text .= ' '. $texts[$i];
			}
			$text .= '...';
		}
		return $text;
	}
  function pc_array_search_recursive( $needle, $haystack )
  {
     $path = NULL;
     $keys = array_keys($haystack);
     while (!$path && (list($toss,$k)=each($keys))) {
        $v = $haystack[$k];
        if (is_scalar($v)) {
           if (strtolower($v)===strtolower($needle)) {
              $path = array($k);
           }
        } elseif (is_array($v)) {
           if ($path=$this->pc_array_search_recursive( $needle, $v )) {
              array_unshift($path,$k);
           }
        }
     }
     return $path;
  }
  function PCkeephtml($string){
    $res = htmlentities($string,ENT_COMPAT,'UTF-8');
    $res = str_replace("&lt;","<",$res);
    $res = str_replace("&gt;",">",$res);
    $res = str_replace("&quot;",'"',$res);
    $res = str_replace("&amp;",'&',$res);
    return $res;
  }
  function PCp2nl ($str) {
    return preg_replace(array("/<p[^>]*>/iU","/<\/p[^>]*>/iU","/<br[^>]*>/iU"),array("\n","","\n"),$str);
  }
  function PCRedirect($str,$msg=null) {
    $JVersion = new JVersion();
    if( (real)$JVersion->RELEASE == 1.5 ) {
      global $mainframe;
      $mainframe->redirect($str,$msg);
    } elseif( (real)$JVersion->RELEASE >= 1.6 ) {
			$app = JFactory::getApplication();
			$app->redirect($str,$msg);
    }    
  }
  function PCarray_flatten($array) { 
    if (!is_array($array)) { 
      return FALSE; 
    } 
    $result = array(); 
    foreach ($array as $key => $value) { 
      if (is_array($value)) { 
        $result = array_merge($result, $this->PCarray_flatten($value)); 
      } 
      else { 
        $result[$key] = $value; 
      } 
    } 
    return $result; 
  } 
  function PCgetAdminData(){
    $db = JFactory::getDBO();
    $JVersion = new JVersion();
    if( (real)$JVersion->RELEASE == 1.5 ) {
      $db->setQuery("SELECT name,email FROM #__users WHERE usertype='Administrator' OR usertype='Super Administrator'");
      $resultusers = $db->loadObjectList();
    } elseif( (real)$JVersion->RELEASE >= 1.6 ) {
      $access = JFactory::getACL();
      $db->setQuery("SELECT id FROM #__usergroups");
      $groups = $db->loadObjectList();
      foreach($groups as $group){
        if($access->checkGroup($group->id, 'core.manage') || $access->checkGroup($group->id, 'core.admin')){
          $adminusers[] = $access->getUsersByGroup($group->id);
        }
      }
      $result = $this->PCarray_flatten($adminusers);
   		$result = implode(',', $result);
      $db->setQuery("SELECT name,email FROM #__users WHERE id IN (".$result.")");
      $resultusers = $db->loadObjectList();
    }
    return $resultusers;
  }
}
?>