<?php
/**
 * @version		$Id: pcpmslist.php 20196 2012-06-24 02:40:25Z ian $
 * @package		PrayerCenter
 * @copyright	Copyright (C) 2006 - 2012 MLWebTechnologies. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('JPATH_BASE') or die;
jimport('joomla.html.html');
jimport('joomla.form.formfield');
jimport('joomla.form.helper');
JFormHelper::loadFieldClass('filelist');
class JFormFieldPCPMSList extends JFormField
{
	protected $type = 'PCPMSList';
	protected static $initialised = false;
	protected function getInput()
	{
    jimport( 'joomla.application.component.helper');
    $comParams = &JComponentHelper::getParams('com_prayercenter');
    $pcParamsArray = $comParams->toArray();
    $pmsrefarray = array(
          1 => array ('val' => 'jim', 'desc' => ''.JText::_(' - (Requires JIM 1.0.1 or above)').''),
          2 => array ('val' => 'joomla', 'desc' => ''.JText::_(' - (Built-in Joomla Messaging Component. Requires Joomla 1.6 or above)').''),
          3 => array ('val' => 'messaging', 'desc' => ''.JText::_(' - (Requires Messaging 1.5 or above)').''),
          4 => array ('val' => 'missus', 'desc' => ''.JText::_(' - (Requires Missus 1.0 or above)').''),
          5 => array ('val' => 'mypms2', 'desc' => ''.JText::_(' - (Requires MyPMS II 2.0)').''),
          6 => array ('val' => 'primezilla', 'desc' => ''.JText::_(' - (Requires Primezilla 1.0.5 or above)').''),
          7 => array ('val' => 'privmsg', 'desc' => ''.JText::_(' - (Requires PrivMSG 2.1.0 or above)').''),
          8 => array ('val' => 'uddeim', 'desc' => ''.JText::_(' - (Requires uddeIM 1.8 or above)').'')
          );
		$SelFiles = JFolder::files( JPATH_ROOT.DS.'/administrator/components/com_prayercenter/plugins/pms/' );
		$files 	= array(  JHTML::_( 'select.option', 0, '- Select -' ) );
		foreach ( $SelFiles as $file ) {
			if ( eregi( "php", $file ) ) {
			  preg_match('/^plg\.pms\.(.*)\.php$/',$file,$match);
				if($match){
          $keyarr = $this->pc_array_search_recursive($match[1], $pmsrefarray);
          $key = $keyarr[0];
          $pmsfile = $pmsrefarray[$key]['desc'];
          $files[] = JHTML::_( 'select.option', $match[1], ucfirst($match[1].$pmsfile) );
        }
			}
		}
		$files = JHTML::_( 'select.genericlist', $files, "jform[params][config_pms_plugin]", 'class="inputbox" size="1" '. '', 'value', 'text', $pcParamsArray['params']['config_pms_plugin'] );
		return $files;
  }
  function pc_array_search_recursive( $needle, $haystack )
  {
     $path = NULL;
     $keys = array_keys($haystack);
     while (!$path && (list($toss,$k)=each($keys))) {
        $v = $haystack[$k];
        if (is_scalar($v)) {
           if (strtolower($v)===strtolower($needle)) {
              $path = array($k);
           }
        } elseif (is_array($v)) {
           if ($path=$this->pc_array_search_recursive( $needle, $v )) {
              array_unshift($path,$k);
           }
        }
     }
     return $path;
  }
}