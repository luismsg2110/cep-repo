<?php
/**
* PrayerCenter Component
* 
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
*/
// ensure this file is being included by a parent file
defined( '_JEXEC' ) or die( 'Restricted Access' );
require_once( JApplicationHelper::getPath( 'toolbar_html' ) );
switch ( $task ) {
	case "manage_css":
		MENU_prayercenter::MANAGE_CSS_MENU();
		break;
	case "manage_req":
		MENU_prayercenter::MANAGE_REQ_MENU();
		break;
	case "manage_sub":
		MENU_prayercenter::MANAGE_SUB_MENU();
		break;
	case "manage_files":
		MENU_prayercenter::MANAGE_FILES_MENU();
		break;
	case "manage_dev":
		MENU_prayercenter::MANAGE_DEV_MENU();
		break;
	case "manage_link":
		MENU_prayercenter::MANAGE_LINK_MENU();
		break;
	case "manage_lang":
		MENU_prayercenter::MANAGE_LANG_MENU();
		break;
	case 'edit' :
    MENU_prayercenter::EDIT_MENU();
    break;
	case 'editlang' :
    MENU_prayercenter::EDIT_LANG_MENU();
    break;
	case 'editlink' :
    MENU_prayercenter::EDIT_LINK_MENU();
    break;
	case 'editdevotion' :
    MENU_prayercenter::EDIT_DEVOTION_MENU();
    break;
	case 'addlang' :
    MENU_prayercenter::EDIT_LANG_MENU();
    break;
	case 'addlink' :
    MENU_prayercenter::EDIT_LINK_MENU();
    break;
	case 'adddevotion' :
    MENU_prayercenter::EDIT_DEVOTION_MENU();
    break;
	case 'support' :
    MENU_prayercenter::DEFAULT_MENU();
    break;
	default:
		MENU_prayercenter::DEFAULT_MENU();
		break;
}
?>
