<?php
/**
* PrayerCenter Component
* 
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
*/
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view');
class PrayerCenterViewEditDev extends JView
{
	function display( $tpl = null)
	{
	global $db;
	$lists = array();
	$edit		= JRequest::getVar('edit',true);
  $cid 		= JRequest::getVar( 'cid', array(0), '', 'array' );
  $id = $cid[0];
  if($edit){
  $db =& JFactory::getDBO();
  $user = &JFactory::getUser();
	$query = "SELECT *"
	. "\n FROM #__prayercenter_devotions"
	. "\n WHERE id = '".$id."'"
	;
  $db->setQuery($query);
  $row = $db->loadObject();
	$crow =& JTable::getInstance('prayercenterdevotions', 'Table');
	$crow->load( (int)$id );
  $query = "SELECT name FROM #__users WHERE id='".$row->checked_out."'";
  $db->setQuery($query);
  $moduser = $db->loadObjectList();
	if ($crow->isCheckedOut( $user->get('id'))) {
		$error = "This feed is currently being edited by ".$moduser[0]->name.".";
			JError::raiseWarning(0, $error );
	   }
      else {
	   $crow->checkout( $user->get('id') );
    }
    }  else {
			$row = new stdClass();
			$row->id = 0;
			$row->name = "";
			$row->feed = "";
			$row->published = 1;
			$row->ordering 	= 0;
    }
		$oquery = 'SELECT ordering AS value, feed AS text'
			. ' FROM #__prayercenter_devotions'
			. ' ORDER BY ordering';
  	$lists['ordering'] 			= JHTML::_('list.specificordering',  $row, $row->id, $oquery );
  	$lists['published'] 		= JHTML::_('select.booleanlist',  'published', 'class="inputbox"', $row->published );
    $this->assignRef('rows', $row);
		$this->assignRef('option', $option);
		$this->assignRef('lists',		$lists);
		$this->assignRef('edit',		$edit);
		parent::display($tpl);
	}
}
?>
