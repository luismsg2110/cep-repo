<?php 
defined('_JEXEC') or die('Restricted access'); 
		global $prayercenteradmin;
  	JRequest::setVar( 'hidemainmenu', 1 );
    $edit = $this->edit;
  	$text = !$edit ? JText::_( 'New' ) : JText::_( 'Edit' );
  	JToolBarHelper::title(   JText::_( 'Edit Devotional' ).': <small><small>[ ' . $text.' ]</small></small>' , 'searchtext' );
  		JToolBarHelper::save('savedevotion', 'Save' );
  	if (!$edit)  {
  		JToolBarHelper::cancel('canceleditdevotion');
  	} else {
  		JToolBarHelper::cancel( 'canceleditdevotion', 'Close' );
  	}
JHTML::_('behavior.tooltip'); 
?>
<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		var form = document.adminForm;
		if (pressbutton == 'canceleditdevotion') {
			submitform( pressbutton );
			return;
		}
		// do field validation
		if (form.title.value == ""){
			alert( "<?php echo JText::_( 'Feed item must have a name', true ); ?>" );
		} else if (form.feed.value == ""){
			alert( "<?php echo JText::_( 'You must have a feed.', true ); ?>" );
		} else {
			submitform( pressbutton );
		}
	}
</script>
<style type="text/css">
	table.paramlist td.paramlist_key {
		width: 92px;
		text-align: left;
		height: 30px;
	}
</style>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<div class="col width-60">
	<fieldset class="adminform">
		<legend><?php echo JText::_( 'Details' ); ?></legend>
		<table class="admintable">
		<tr>
			<td width="100" align="right" class="key">
				<label for="title">
					<?php echo JText::_( 'Name' ); ?>:
				</label>
			</td>
			<td>
				<input class="text_area" type="text" name="title" id="title" size="60" maxlength="250" value="<?php echo $this->rows->name;?>" />
			</td>
		</tr>
		<tr>
			<td width="100" align="right" class="key">
				<label for="alias">
					<?php echo JText::_( 'Feed' ); ?>:
				</label>
			</td>
			<td>
				<input class="text_area" type="text" name="feed" id="feed" size="60" maxlength="250" value="<?php echo $this->rows->feed;?>" />
			</td>
		</tr>
		<tr>
			<td valign="top" align="right" class="key">
				<?php echo JText::_( 'Published' ); ?>:
			</td>
			<td>
				<?php echo $this->lists['published']; ?>
			</td>
		</tr>
		<tr>
			<td valign="top" align="right" class="key">
				<label for="ordering">
					<?php echo JText::_( 'Ordering' ); ?>:
				</label>
			</td>
			<td>
				<?php echo $this->lists['ordering']; ?>
			</td>
		</tr>
	</table>
	</fieldset>
</div>
<div class="clr"></div>
	<input type="hidden" name="option" value="com_prayercenter" />
	<input type="hidden" name="controller" value="prayercenter" />
	<input type="hidden" name="id" value="<?php echo $this->rows->id; ?>" />
	<input type="hidden" name="task" value="" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form><br />
	<?php $prayercenteradmin->PCfooter(); ?>
