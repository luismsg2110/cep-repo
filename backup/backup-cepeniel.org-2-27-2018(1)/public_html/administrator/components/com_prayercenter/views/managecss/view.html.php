<?php
/**
* PrayerCenter Component
* 
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
*/
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view');
class PrayerCenterViewManageCss extends JView
{
	function display( $tpl = null)
	{
    $version = new JVersion();
		$this->assignRef('JVersion', $version);
		parent::display($tpl);
	}
}
?>
