<?php
/**
* PrayerCenter Component for Joomla
* By Mike Leeper
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
    global $prayercenteradmin, $mainframe;
    $editor = &JFactory::getEditor('none');
    ?>
    <script language="javascript" type="text/javascript">
		function submitbutton(pressbutton) {
			var form = document.adminForm;
			if (pressbutton == 'cancel') {
				submitform( pressbutton );
				return;
				}
				submitform( pressbutton );
			}
    </script>
 	<form action="index.php?option=com_prayercenter" method="post" name="adminForm">
  <div>
  <fieldset style="padding-left:20px;"><legend><?php echo JText::_('PrayerCenter Frontend Cascading Style Sheet'); ?></legend>
    <table width="100%">
		<tr style="text-align:center;">
			<td><br />
  		<?php
  		$filename = JPATH_ROOT.DS.'components'.DS.'com_prayercenter'.DS.'assets'.DS.'css'.DS.'prayercenter.css';
  		$initstring = file_get_contents($filename);
      echo $editor->display( 'config_css',  $initstring , '90%', '250', '70', '15', false ) ;
  		?>
			</td>
		</tr>
    <tr style="text-align:center;"><td>
    <script language="javascript">
    function printcss()
    { 
      var disp_setting = "toolbar=yes,location=no,directories=yes,menubar=yes,"; 
          disp_setting += "scrollbars=yes,width=650, height=300, left=100, top=25"; 
      var content_vlue = document.getElementById("config_css").value.replace(/(\r\n|\n\r|\r|\n)/g,"<br>"); 
      
      var docprint=window.open("","",disp_setting); 
       docprint.document.open(); 
       docprint.document.write('<html><head><title>PrayerCenter CSS File</title>'); 
       docprint.document.write('</head><body onLoad="self.print()"><center><table border=1><tr><td>');          
       docprint.document.write(content_vlue);
       docprint.document.write('</td></tr></table><br /><a href="javascript:self.close();">Close Window</a>');          
       docprint.document.write('</center></body></html>'); 
       docprint.document.close(); 
       docprint.focus(); 
    }
    </script>
    <?php
    if( (real)$this->JVersion->RELEASE == 1.5 ) {
  		$template	= $mainframe->getTemplate();
    } elseif( (real)$this->JVersion->RELEASE >= 1.6 ){
      $template	= JFactory::getApplication()->getTemplate();
    }
    $imagedir = 'templates/'.$template.'/images/menu/';
 		$pattribs['onclick'] = "javascript:printcss();";
 		$prtimage = JHTML::_('image.site',  'icon-16-print.png', $imagedir, NULL, NULL, htmlentities(JText::_( 'Print CSS' ),ENT_COMPAT,'UTF-8'),'style=vertical-align:middle;float:none;');
  	$pattribs['title']	= htmlentities(JText::_( 'Print CSS' ),ENT_COMPAT,'UTF-8');
  	$pattribs['rel']     = 'nofollow';
 		echo '<span style="background-color:white;">&nbsp;'.JHTML::_('link', '#', $prtimage.'&nbsp;<small>'.htmlentities(JText::_('Print CSS'),ENT_COMPAT,'UTF-8').'</small>',$pattribs);
 		$w3link = "http://www.w3schools.com/css/default.asp";
 		$image = JHTML::_('image.site',  'icon-16-info.png', $imagedir, NULL, NULL, NULL,'style=vertical-align:middle;float:none;');
  	$attribs['rel']     = 'nofollow';
 		echo '&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.JHTML::_('link', JRoute::_($w3link), $image.'&nbsp;<small>'.JText::_('CSS Tutorial and Reference').'</small>',$attribs).'&nbsp;</span>';
    ?>
    </td></tr>
   </table>
  </fieldset>
  </div>
		<input type="hidden" name="option" value="com_prayercenter" />
 		<input type="hidden" name="task" value="manage_css" />
 		<input type="hidden" name="controller" value="prayercenter" />
  	<?php echo JHTML::_( 'form.token' ); ?>
		</form><br /><br />
	<?php
  $prayercenteradmin->PCfooter();
?>
