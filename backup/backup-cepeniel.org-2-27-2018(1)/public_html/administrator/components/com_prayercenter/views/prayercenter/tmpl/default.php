<?php
/**
* PrayerCenter Component for Joomla
* By Mike Leeper
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
  global $mainframe,$pcConfig,$prayercenteradmin;
  $livesite = JURI::base();
  $lang =& Jfactory::getLanguage();
  $lang->load( 'com_prayercenter', JPATH_SITE); 
  jimport('joomla.html.pane');
  include_once('components/com_prayercenter/helpers/pc_version.php');
  JHTML::_('script','administrator/components/com_prayercenter/assets/js/admin_pc.js');
  $pcversion = & PCVersion::getInstance();
  $db	=& JFactory::getDBO();
	$version = new JVersion();
  $supportinfo = "";
  $supportinfo .= 'System Information%0D%0A%0D%0A';
  $supportinfo .= 'Database Version:%20'.$db->getVersion().'%0D%0A';
  $supportinfo .= 'PHP Version:%20'.phpversion().'%0D%0A';
  $supportinfo .= 'Web Server:%20'.$prayercenteradmin->pc_get_server_software().'%0D%0A';
  $supportinfo .= 'Joomla! Version:%20'.$version->getLongVersion().'%0D%0A%0D%0A';
  $supportinfo .= 'Relevant PHP Settings%0D%0A';
  $supportinfo .= 'Magic Quotes GPC:%20'.$prayercenteradmin->pc_get_php_setting('magic_quotes_gpc').'%0D%0A';
  $supportinfo .= 'Short Open Tags:%20'.$prayercenteradmin->pc_get_php_setting('short_open_tag').'%0D%0A';
  $supportinfo .= 'Disabled Functions:%20'.(($df=ini_get('disable_functions'))?$df:'none').'%0D%0A';
  if( (real)$version->RELEASE == 1.5 ) {
		$template	= $mainframe->getTemplate();
  } elseif( (real)$version->RELEASE >= 1.6 ){
    $template	= JFactory::getApplication()->getTemplate();
  }
  $imagedir = 'templates/'.$template.'/images/menu/';
  ?>
	<div class="adminform">
	<div class="cpanel-left">
	<div id="cpanel" class="cpanel">
		<?php
		$prayercenteradmin->PCquickiconButton( 'index.php?option=com_prayercenter&task=manage_req', 'icon-48-article.png', JText::_( 'Manage Requests' ) );
		$prayercenteradmin->PCquickiconButton( 'index.php?option=com_prayercenter&task=manage_sub', 'icon-48-user.png', JText::_( 'Manage Subscribers' ) );
		$prayercenteradmin->PCquickiconButton( 'index.php?option=com_prayercenter&task=manage_css', 'icon-48-themes.png', JText::_( 'Manage CSS' ) );
		$prayercenteradmin->PCquickiconButton( 'index.php?option=com_prayercenter&task=manage_files', 'icon-48-category.png', JText::_( 'Manage Files' ) );
		$prayercenteradmin->PCquickiconButton( 'index.php?option=com_prayercenter&task=manage_dev', 'icon-48-stats.png', JText::_( 'Manage Devotions' ) );
		$prayercenteradmin->PCquickiconButton( 'index.php?option=com_prayercenter&task=manage_link', 'icon-48-component.png', JText::_( 'Manage Links' ) );
		$prayercenteradmin->PCquickiconButton( 'index.php?option=com_prayercenter&task=manage_lang', 'icon-48-language.png', JText::_( 'Manage Languages' ) );
		$prayercenteradmin->PCquickiconButton( 'index.php?option=com_prayercenter&task=utilities', 'icon-48-module.png', JText::_( 'Utilities' ) );
    if( (real)$version->RELEASE >= 1.6 ){
  		$prayercenteradmin->PCquickiconButton( "http://www.joomlacode.org/gf/project/prayercenter/tracker/", 'icon-48-help-this.png', JText::_( 'Support BugTracker' ) );
  		$prayercenteradmin->PCquickiconButton( "http://www.mlwebtechnologies.com/index.php?option=com_kunena", 'icon-48-help-forum.png', JText::_( 'Support Forum' ) );
  		$prayercenteradmin->PCquickiconButton( "mailto:web@mlwebtechnologies.com?subject=PrayerCenter%20Support%20Inquiry&body=".$supportinfo, 'icon-48-writemess.png', JText::_( 'Support Email' ) );
  		$prayercenteradmin->PCquickiconButton( "http://www.mlwebtechnologies.com", 'icon-48-links.png', JText::_( 'Support Website' ) );
      $url = "http://www.mlwebtechnologies.com/update/prayercenter-update.xml";
      $data = file_get_contents($url);
      $xmlObj = simplexml_load_string($data);
      $xmlObj->update->version;
      $upglink = $xmlObj->update->downloads->downloadurl;
      if(trim($pcversion->getShortVersion()) < trim($xmlObj->update->version)){
        $message = JText::_('Component Update ['.$xmlObj->update->version.' Available]');
      }else{
        $message = JText::_( 'Component Update [None Available]');
      }
  		$prayercenteradmin->PCquickiconButton( "index.php?option=com_installer&view=update", 'icon-48-extension.png', $message );
    } else {
  		$prayercenteradmin->PCquickiconButton( "http://www.joomlacode.org/gf/project/prayercenter/tracker/", 'icon-48-help.png', JText::_( 'Support BugTracker' ) );
  		$prayercenteradmin->PCquickiconButton( "http://www.mlwebtechnologies.com/index.php?option=com_kunena", 'icon-48-info.png', JText::_( 'Support Forum' ) );
  		$prayercenteradmin->PCquickiconButton( "mailto:web@mlwebtechnologies.com?subject=PrayerCenter%20Support%20Inquiry&body=".$supportinfo, 'icon-48-inbox.png', JText::_( 'Support Email' ) );
  		$prayercenteradmin->PCquickiconButton( "http://www.mlwebtechnologies.com", 'icon-48-frontpage.png', JText::_( 'Support Website' ) );
		}
    ?>
	</div>
	</div>
	<div class="cpanel-right">
  <?php
	$pane = &JPane::getInstance('sliders', array('allowAllClose' => false));
	echo $pane->startPane("info-pane");
	$title = JText :: _('Version Information') ;
	echo $pane->startPanel( $title, 'panel1' );
  $version = new JVersion();
  if( (real)$version->RELEASE == 1.5 ) {
		$template	= $mainframe->getTemplate();
  } elseif( (real)$version->RELEASE >= 1.6 ){
    $template	= JFactory::getApplication()->getTemplate();
  }
  ?>
    <table class="adminlist">
       <tr>
          <th><b><?php echo JText :: _('Installed version');?></b></th>
          <td style="border: 1px solid;">
          <?php
          $url = "http://www.mlwebtechnologies.com/update/prayercenter-update.xml";
          $data = file_get_contents($url);
          $xmlObj = simplexml_load_string($data);
          $xmlObj->update->version;
          $upglink = $xmlObj->update->downloads->downloadurl;
          if(trim($pcversion->getShortVersion()) < trim($xmlObj->update->version)){
         		$image = JHTML::_('image.site',  'icon-16-install.png', '/templates/'.$template.'/images/menu/', NULL, NULL, NULL,'style=vertical-align:bottom;');
          	$attribs['rel']     = 'nofollow';
          	$attribs['class'] = 'upgrade';
          	?><style type="text/css">
            a:link.upgrade, a:visited.upgrade {
              display:inline;
              font-weight:bold;
              color:#FFFFFF;
              background-color:#98bf21;
              width:120px;
              text-align:center;
              padding:4px;
              text-decoration:none;
              font-size:x-small;
            }
            a:hover.upgrade,a:active.upgrade {
              background-color:#7A991A;
            }
            </style>
            <?php
         		echo $pcversion->getLongVersion().'&nbsp;&nbsp;'.JHTML::_('link', JRoute::_($upglink), JText::_($xmlObj->update->version.' Update Available'),$attribs);
          } else {
            echo $pcversion->getLongVersion().'&nbsp;<span style="color:green;font-size:x-small;">[Latest version installed]</span>';          
          }   
          ?>
          </td>
       </tr>
       <tr>
          <th><b><?php echo JText :: _('Copyright</b></td>');?></th>
          <td style="border: 1px solid;"><?php echo $pcversion->getShortCopyright();?></td>
       </tr>
       <tr>
          <th><b><?php echo JText :: _('License');?></b></th>
          <td style="border: 1px solid;"><a href="http://www.gnu.org/copyleft/gpl.html" target="_blank"><img src="<?php echo JURI::base();?>components/com_prayercenter/assets/images/gplv3-88x31.png" border="0" title="<?php echo JText::_('GNU General Public License v3');?>" /></a>&nbsp;<?php echo JText::_('GNU/GPL3');?></td>
       </tr>
       <tr>
          <th><b><?php echo JText :: _('Developer');?></b></th>
          <td style="border: 1px solid;"><?php echo JText :: _('Mike Leeper');?></td>
       </tr>
       <tr>
          <th><b><?php echo JText :: _('Website');?></b></th>
          <td style="border: 1px solid;">
          <?php
       		$image = JHTML::_('image.site',  'icon-16-frontpage.png', '/templates/'.$template.'/images/menu/', NULL, NULL, NULL,'style=vertical-align:bottom;');
        	$attribs['rel']     = 'nofollow';
          $attribs['class'] = '';
       		echo JHTML::_('link', JRoute::_($pcversion->getUrl()), $image.'&nbsp;'.JText::_('http://www.mlwebtechnologies.com'),$attribs);
          ?>
          </td>
       </tr>
       <tr>
       <th><b><?php echo JText::_('Donation');?></b></th>
     <td style="border: 1px solid;">
		<script language="javascript" type="text/javascript">
		function submitbutton(pressbutton) {
			var form = document.adminForm;
			if (pressbutton == 'cancel') {
				submitform( pressbutton );
				return;
				}
				submitform( pressbutton );
			}
    </script>
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
    <input type="hidden" name="cmd" value="_s-xclick">
    <input type="hidden" name="hosted_button_id" value="8VDCTFRZPPQNW">
    <input type="hidden" name="business" value="web@mlwebtechnologies.com">
    <input type="hidden" name="item_name" value="MLWebTechnologies - PrayerCenter Donations">
    <input type="hidden" name="item_number" value="MSD1">
    <input type="hidden" name="no_shipping" value="1">
    <input type="hidden" name="return" value="">
    <input type="hidden" name="no_note" value="1">
    <input type="hidden" name="currency_code" value="USD">
    <input type="hidden" name="tax" value="0">
    <input type="hidden" name="bn" value="MLWebTechnologies">
    <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" style="border:none !important;" border="0" name="submit" title="PayPal - The safer, easier way to pay online!">
    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1" />
    </form>
    </td></tr>
   </table>
  <?php
	echo $pane->endPanel();
	echo $pane->startPanel( 'System Information', 'panel3' );
  ?>
    <table class="adminlist">
		 <tr>
			<th><b><?php echo JText::_('PHP built On');?></b></th>
			<td style="border: 1px solid;"><?php echo php_uname(); ?></td>
		 </tr>
		 <tr>
			<th><b><?php echo JText::_('Database Version');?></b></th>
			<td style="border: 1px solid;"><?php echo $db->getVersion(); ?></td>
		 </tr>
		 <tr>
			<th><b><?php echo JText::_('PHP Version');?></b></th>
			<td style="border: 1px solid;"><?php echo phpversion(); ?></td>
		 </tr>
		 <tr>
			<th><b><?php echo JText::_('Web Server');?></b></th>
			<td style="border: 1px solid;"><?php echo $prayercenteradmin->pc_get_server_software(); ?></td>
		 </tr>
		 <tr>
			<th><b><?php echo JText::_('Joomla! Version');?></b></th>
			<td style="border: 1px solid;"><?php echo $version->getLongVersion(); ?></td>
		 </tr>
		 <tr>
			<th><b><?php echo JText::_('User Agent');?></b></th>
			<td style="border: 1px solid;"><?php echo phpversion() <= '4.2.1' ? getenv( 'HTTP_USER_AGENT' ) : $_SERVER['HTTP_USER_AGENT'];?></td>
		 </tr>
		 <tr>
			<th><b><?php echo JText::_('Relevant PHP Settings');?></b></th>
			<td style="border: 1px solid;"><table cellspacing="1" cellpadding="1" border="0" width="100%">
        <tr>
					<td class="key"><?php echo JText::_('Magic Quotes GPC');?></td>
					<td style="font-weight: bold;"><?php echo $prayercenteradmin->pc_get_php_setting('magic_quotes_gpc', true, false); ?></td>
				</tr>
				<tr>
					<td class="key"><?php echo JText::_('Short Open Tags');?></td>
					<td style="font-weight: bold;"><?php echo $prayercenteradmin->pc_get_php_setting('short_open_tag', true, false); ?></td>
				</tr>
				<tr>
					<td class="key"><?php echo JText::_('Disabled Functions');?></td>
					<td style="font-weight: bold;" colspan="2"><?php echo (($df=ini_get('disable_functions'))?$df:'None'); ?></td>
				</tr>
			</table>
		</td>
	</tr></table>
  <?php
	echo $pane->endPanel();
	echo $pane->startPanel( JText::_('Optional Modules - Status'), 'panel4' );
  ?>
    <table border="0" width="100%" class="adminlist">
		<thead>
			<tr>
				<th class="title" width="40%" align="center"><?php echo JText::_( 'Name' ); ?></th>
				<th class="title" width="30%"><?php echo JText::_( 'Date' ); ?></th>
				<th class="title" width="30%" align="center"><?php echo JText::_( 'Version' ); ?></th>
			</tr>
		</thead>
    <tbody>
    <?php 
    $modArray = array(1 => array ('module' => 'mod_pc_submit_request', 'name' => 'PrayerCenter Submit Request'),
                      2 => array ('module' => 'mod_pc_subscribe', 'name' => 'PrayerCenter Subscribe'),
                      3 => array ('module' => 'mod_pc_menu', 'name' => 'PrayerCenter Menu'),
                      4 => array ('module' => 'mod_pc_latest', 'name' => 'PrayerCenter Latest'));
    foreach($modArray as $module){
      $moduledir = JPATH_ROOT.DS.'modules';
      $xmlfile = $moduledir.DS.$module['module'].DS.$module['module'].'.xml';
      $xmldata = $prayercenteradmin->PCparseXml($xmlfile);
      if($xmldata){
        echo '<tr><td class="key" width="40%">'.$module['name'].'</td>';
        echo '<td class="key" width="30%"><font color="green"><center>'.$xmldata['creationDate'].'</font></center></td>';
        echo '<td class="key" width="30%"><font color="green"><center>'.$xmldata['version'].'</font></center></td>';
      } else {
        echo '<tr><td class="key" width="40%">'.$module['name'].'</td>';
        echo '<td colspan="2"><font color="red"><center>'.JText :: _('Not Installed').'</center></font></td>';
      }
    echo '</tr>';
    }
    ?>
    </tbody>
   </table>
  <?php
	echo $pane->endPanel();
	echo $pane->startPanel( JText::_('PrayerCenter Change Log'), 'panel2' );
  ?>
  <table border="0" width="100%" class="adminlist">
  <tbody><tr class="row0">
  <td><?php echo $prayercenteradmin->PCChangeLog();?></td>
  </tr></tbody>
  </table>
  <?php
	echo $pane->endPanel();
	echo $pane->startPanel( 'PrayerCenter Configuration File', 'panel5' );
  ?>
    <table border="0" width="100%" class="admintable">
			<tr>
				<td><center>
        <?php
        $cstring = "";
        $JVersion = new JVersion();
        $pcParams = JComponentHelper::getParams('com_prayercenter');
        if( (real)$JVersion->RELEASE == 1.5 ) {
          $pcConfig = $pcParams->toArray();
        } elseif( (real)$JVersion->RELEASE >= 1.6 ) {
          $pcParamsArray = $pcParams->toArray();
          foreach($pcParamsArray['params'] as $name => $value){
            $pcConfig[(string)$name] = (string)$value;
          }
        }
    		foreach ($pcConfig as $name => $value) {
          $cstring .= $name.' = '.$value.'\n';
        }
  			?>
  			<textarea name="pcconfigstr" id="config_content" cols="68" rows="15" readonly><?php echo str_replace('\n',"\n",$cstring);?></textarea>
				</center></td>
		 </tr>
    <tr><td>
    <script language="javascript">
    function printconfig()
    { 
      var disp_setting = "toolbar=yes,location=no,directories=yes,menubar=yes,"; 
          disp_setting += "scrollbars=yes,width=650, height=300, left=100, top=25"; 
      var content_vlue = document.getElementById("config_content").value.replace(/(\r\n|\n\r|\r|\n)/g,"<br>"); 
      var docprint=window.open("","",disp_setting); 
       docprint.document.open(); 
       docprint.document.write('<html><head><title>PrayerCenter Configuration File</title>'); 
       docprint.document.write('</head><body onLoad="self.print()"><center><table border=1><tr><td>');          
       docprint.document.write(content_vlue);          
       docprint.document.write('</td></tr></table><br /><a href="javascript:self.close();">Close Window</a>');          
       docprint.document.write('</center></body></html>'); 
       docprint.document.close(); 
       docprint.focus(); 
    }
    function savePCConfig2file( fcontent ) {
     var w = window.frames.w;
     if( !w ) {
      w = document.createElement( 'iframe' );
      w.id = 'w';
      w.style.display = 'none';
      document.body.insertBefore( w, null );
      w = window.frames.w;
      if( !w ) {
       w = window.open( '', '_temp', 'width=100,height=100' );
       if( !w ) {
        window.alert( 'Sorry, the file could not be created.' ); return false;
       }
      }
     }
     var d = w.document, ext = 'utf-8', name = 'PCConfig.txt';
     d.open( 'text/plain', 'replace' );
     d.charset = 'utf-8';
     d.write( fcontent );
     d.close();
     if( d.execCommand( 'SaveAs', null, name ) ){
      window.alert( 'The file has been saved.' );
     }
     w.close();
     return false;
    }
    </script>
    <?php
 		$printconfiglink = "#";
 		$prtimage = JHTML::_('image.site',  'icon-16-print.png', $imagedir, NULL, NULL, htmlentities(JText::_( 'Print Config' ),ENT_COMPAT,'UTF-8'),'style=vertical-align:top;');
  	$attribs['title']	= htmlentities(JText::_( 'Print Configuration' ),ENT_COMPAT,'UTF-8');
  	$attribs['rel']     = 'nofollow';
    $attribs['onclick'] = "javascript:printconfig();";
 		echo '&nbsp;&nbsp'.JHTML::_('link', JRoute::_($printconfiglink), $prtimage.'&nbsp;<small>'.htmlentities(JText::_('Print Config'),ENT_COMPAT,'UTF-8').'</small>',$attribs);
    $savelink = '#';
    $saveimage = JHTML::_('image.site',  'icon-16-install.png', $imagedir, NULL, NULL, NULL,'style=vertical-align:bottom;');
  	$saveattribs['title']	= htmlentities(JText::_( 'Save Configuration as File' ),ENT_COMPAT,'UTF-8');
    $saveattribs['rel']     = 'nofollow';
    ?><script language="javascript">var cstring = '<?php echo $cstring;?>';</script><?php
    $saveattribs['onclick'] = "javascript:savePCConfig2file(cstring);";
    echo '&nbsp;&nbsp;|&nbsp;&nbsp;'.JHTML::_('link', JRoute::_($savelink), $saveimage.'&nbsp;<small>'.JText::_('Save as file').'</small>',$saveattribs);
    ?>
    </td></tr>
   </table>
  <?php
	echo $pane->endPanel();
	echo $pane->endPane();
  ?>
  <input type="hidden" name="task" value="" />
	</div></div><div class="clr"></div><br /><br /><div>
  <?php
  $prayercenteradmin->PCfooter();
?></div>