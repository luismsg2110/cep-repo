<?php
/**
* PrayerCenter Component for Joomla
* By Mike Leeper
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
    global $prayercenteradmin;
		$ordering = ($this->lists['order'] == 'ordering');
		JHTML::_('behavior.tooltip');
    JHtml::_('script','system/multiselect.js', false, true);
		  ?>
		<script language="javascript" type="text/javascript">
		function submitbutton(pressbutton) {
			var form = document.adminForm;
			if (pressbutton == 'cancel') {
				submitform( pressbutton );
				return;
				}
				submitform( pressbutton );
			}
    </script>
	<form action="index.php?option=com_prayercenter" method="post" name="adminForm">
	<fieldset id="filter-bar">
		<div class="filter-search fltlft">
			<label class="filter-search-lbl" for="filter_search"><?php echo JText::_('Filter').'&nbsp;'; ?></label>
			<input type="text" name="search" id="search" value="<?php echo $this->lists['search'];?>" class="inputbox" onChange="document.adminForm.submit();" />
			<button type="submit"><?php echo JText::_('Go'); ?></button>
			<button type="button" onclick="document.id('filter_search').value='';this.form.submit();"><?php echo JText::_('Reset'); ?></button>
		</div>
	</fieldset>
	<div class="clr"> </div>
  <table class="adminlist">
  	<thead>
    		<tr>
					<th width="5" class="title">
						<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->rows); ?>);" />
					</th>
					<th class="title">
						<?php echo JHTML::_('grid.sort',   'Name', 'name', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
					</th>
					<th class="title">
						<?php echo JHTML::_('grid.sort',   'Url', 'url', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
					</th>
					<th class="title">
						<?php echo JHTML::_('grid.sort',   'Alias', 'alias', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
					</th>
					<th class="title">
						<?php echo JHTML::_('grid.sort',   'Description', 'descrip', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
					</th>
					<th width="5%" class="title" nowrap="nowrap">
						<?php echo JHTML::_('grid.sort',   'Published', 'published', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
					</th>
					<th width="12%" class="title" nowrap="nowrap">
						<?php echo JHTML::_('grid.sort',   'Ordering', 'ordering', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
    				<?php echo JHTML::_('grid.order',  $this->rows, 'filesave.png', "saveorderlinks" ); ?>
					</th>
    			<th width="1%" nowrap="nowrap">
    				<?php echo JHTML::_('grid.sort',  'ID', 'id', $this->lists['order_Dir'], $this->lists['order'] ); ?>
    			</th>
  		</tr></thead>
		<?php
		$k = 0;
		$i = 0;
		for ($i=0, $n=count( $this->rows ); $i < $n; $i++) {
			$row = &$this->rows[$i];
            $description = $row->descrip;
            $description = strip_tags($description,"<i><strong><u><em><strike>");
            $description = stripslashes($description);
            if (strlen($description) > 80) $description = substr($description, 0 , 78) . " ...";
			?>
			<tbody><tr class="row<?php echo $k; ?>">
				<td align="center" width="5"><input type="checkbox" id="cb<?php echo $i;?>" name="cid[]" value="<?php echo $row->id; ?>" onClick="isChecked(this.checked);"></td>
				<td align="center"><span class="editlinktip hasTip" title="<?php echo JText::_( 'Edit Link' );?>::<?php echo $this->escape($row->name); ?>">
					<a href="#edit" onclick="return listItemTask('cb<?php echo $i; ?>','editlink')">
						<?php echo $this->escape($row->name); ?></a></span></td>
				<td align="center"><?php echo $row->url; ?></td>
        <td align="center"><?php echo $row->alias; ?></td>
        <td align="center"><?php echo $description; ?></td>
      <?php
      if( (real)$this->JVersion->RELEASE == 1.5 ) {
        $imagedir = 'images';
      } elseif( (real)$this->JVersion->RELEASE >= 1.6 ){
        $template	= JFactory::getApplication()->getTemplate();
        $imagedir = 'templates/'.$template.'/images/admin';
      }
			if ($row->published) {
      	$publishimg = "<a href=\"javascript: void(0);\" ><img src=\"".$imagedir."/tick.png\" border=\"0\" onclick=\"return listItemTask('cb$i','unpublishlink');\" /></a>";
			} else {
	      $publishimg = "<a href=\"javascript: void(0);\" ><img src=\"".$imagedir."/publish_r.png\" border=\"0\" onclick=\"return listItemTask('cb$i','publishlink');\" /></a>";
			} 
    ?>           		
    <td align="center"><?php echo $publishimg; ?></td>
			<td class="order">
			<?php if ($this->lists['order_Dir'] == 'asc') : ?>
  			<span><?php echo $this->pageNav->orderUpIcon( $i, true,'orderupdev', 'JLIB_HTML_MOVE_UP', $ordering ); ?></span>
  			<span><?php echo $this->pageNav->orderDownIcon( $i, $n, true, 'orderdowndev', 'JLIB_HTML_MOVE_DOWN', $ordering ); ?></span>
			<?php elseif ($this->lists['order_Dir'] == 'desc') : ?>
  			<span><?php echo $this->pageNav->orderUpIcon( $i, true,'orderdowndev', 'JLIB_HTML_MOVE_UP', $ordering ); ?></span>
  			<span><?php echo $this->pageNav->orderDownIcon( $i, $n, true, 'orderupdev', 'JLIB_HTML_MOVE_DOWN', $ordering ); ?></span>
			<?php endif; ?>
				<?php $disabled = $ordering ?  '' : 'disabled="disabled"'; ?>
				<input type="text" name="order[]" size="5" value="<?php echo $row->ordering;?>" <?php echo $disabled ?> class="text_area" style="text-align: center" />
			</td>
			<td align="center">
				<?php echo $row->id; ?>
			</td>
		</tr></tbody>
  <?php
  }
  ?>
		<tfoot><tr>
			<th align="center" colspan="10">
				<?php echo $this->pageNav->getListFooter(); ?></th>
		</tr>
    </tfoot>
		</table>
			<input type="hidden" name="option" value="com_prayercenter" />
			<input type="hidden" name="task" value="manage_link" />
			<input type="hidden" name="controller" value="prayercenter" />
			<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
	<?php echo JHTML::_( 'form.token' ); ?>
	</form><br />
	<?php
  $prayercenteradmin->PCfooter();
?>