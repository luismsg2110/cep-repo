<?php
/**
* PrayerCenter Component
* 
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
*/
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view');
class PrayerCenterViewSettings extends JView
{
	function display( $tpl = null)
	{
		global $mainframe, $pcConfig;
    jimport( 'joomla.application.component.helper');
    $comParams = &JComponentHelper::getParams('com_prayercenter');
    $popup = JRequest::getVar('pop',null,'get','int');
		// Set pathway information
		$this->assignRef('config_user_view', $comParams->get('config_user_view'));
		$this->assignRef('config_view_template', $comParams->get('config_view_template'));
		$this->assignRef('config_show_requester', $comParams->get('config_show_requester'));
		$this->assignRef('config_show_date', $comParams->get('config_show_date'));
		$this->assignRef('config_show_print', $comParams->get('config_show_print'));
		$this->assignRef('config_show_pdf', $comParams->get('config_show_pdf'));
		$this->assignRef('config_show_email', $comParams->get('config_show_email'));
		$this->assignRef('config_show_rss', $comParams->get('config_show_rss'));
		$this->assignRef('config_bmrss_service', $comParams->get('config_bmrss_service'));
		$this->assignRef('config_show_credit', $comParams->get('config_show_credit'));
		$this->assignRef('config_show_dwprint', $comParams->get('config_show_dwprint'));
		$this->assignRef('config_show_bookmarks', $comParams->get('config_show_bookmarks'));
		$this->assignRef('config_bm_service', $comParams->get('config_bm_service'));
		$this->assignRef('config_bm_service_id', $comParams->get('config_bm_service_id'));
		$this->assignRef('config_use_gcode', $comParams->get('config_use_gcode'));
		$this->assignRef('config_google_id', $comParams->get('config_google_id'));
		$this->assignRef('config_allow_purge', $comParams->get('config_allow_purge'));
		$this->assignRef('config_request_retention', $comParams->get('config_request_retention'));
		$this->assignRef('config_archive_retention', $comParams->get('config_archive_retention'));
		$this->assignRef('config_rows', $comParams->get('config_rows'));
		$this->assignRef('config_user_post', $comParams->get('config_user_post'));
		$this->assignRef('config_user_publish', $comParams->get('config_user_publish'));
		$this->assignRef('config_email_option', $comParams->get('config_email_option'));
		$this->assignRef('config_show_priv_option', $comParams->get('config_show_priv_option'));
		$this->assignRef('config_req_length', $comParams->get('config_req_length'));
		$this->assignRef('config_date_format', $comParams->get('config_date_format'));
		$this->assignRef('config_time_format', $comParams->get('config_time_format'));
		$this->assignRef('config_show_tz', $comParams->get('config_show_tz'));
		$this->assignRef('config_use_admin_alert', $comParams->get('config_use_admin_alert'));
		$this->assignRef('config_admin_distrib_type', $comParams->get('config_admin_distrib_type'));
		$this->assignRef('config_distrib_type', $comParams->get('config_distrib_type'));
		$this->assignRef('config_email_request', $comParams->get('config_email_request'));
		$this->assignRef('config_email_list', $comParams->get('config_email_list'));
		$this->assignRef('config_return_addr', $comParams->get('config_return_addr'));
		$this->assignRef('config_custom_ret_addr', $comParams->get('config_custom_ret_addr'));
		$this->assignRef('config_pms_plugin', $comParams->get('config_pms_plugin'));
		$this->assignRef('config_pms_user_list', $comParams->get('config_pms_user_list'));
		$this->assignRef('config_show_subscribe', $comParams->get('config_show_subscribe'));
		$this->assignRef('config_user_subscribe', $comParams->get('config_user_subscribe'));
		$this->assignRef('config_admin_approve_subscribe', $comParams->get('config_admin_approve_subscribe'));
		$this->assignRef('config_email_subscribe', $comParams->get('config_email_subscribe'));
		$this->assignRef('config_moderator_user_list', $comParams->get('config_moderator_user_list'));
		$this->assignRef('config_use_wordfilter', $comParams->get('config_use_wordfilter'));
		$this->assignRef('config_bad_words', $comParams->get('config_bad_words'));
		$this->assignRef('config_replace_word', $comParams->get('config_replace_word'));
		$this->assignRef('config_enable_rss_cache', $comParams->get('config_enable_rss_cache'));
		$this->assignRef('config_rss_update_time', $comParams->get('config_rss_update_time'));
		$this->assignRef('config_rss_num', $comParams->get('config_rss_num'));
		$this->assignRef('config_rss_limit_text', $comParams->get('config_rss_limit_text'));
		$this->assignRef('config_rss_text_length', $comParams->get('config_rss_text_length'));
		$this->assignRef('config_rss_authkey', $comParams->get('config_rss_authkey'));
		$this->assignRef('config_use_spamcheck', $comParams->get('config_use_spamcheck'));
		$this->assignRef('config_use_slideshow', $comParams->get('config_use_slideshow'));
		$this->assignRef('config_slideshow_speed', $comParams->get('config_slideshow_speed'));
		$this->assignRef('config_slideshow_duration', $comParams->get('config_slideshow_duration'));
		$this->assignRef('config_imagefile', $comParams->get('config_imagefile'));
		$this->assignRef('config_show_image', $comParams->get('config_show_image'));
		$this->assignRef('config_show_page_headers', $comParams->get('config_show_page_headers'));
		$this->assignRef('config_show_menu', $comParams->get('config_show_menu'));
		$this->assignRef('config_show_header_text', $comParams->get('config_show_header_text'));
		$this->assignRef('config_show_devotion', $comParams->get('config_show_devotion'));
		$this->assignRef('config_user_view_devotional', $comParams->get('config_user_view_devotional'));
		$this->assignRef('config_update_time', $comParams->get('config_update_time'));
		$this->assignRef('config_enable_cache', $comParams->get('config_enable_cache'));
		$this->assignRef('config_feed_descr', $comParams->get('config_feed_descr'));
		$this->assignRef('config_feed_image', $comParams->get('config_feed_image'));
		$this->assignRef('config_item_descr', $comParams->get('config_item_descr'));
		$this->assignRef('config_word_count', $comParams->get('config_word_count'));
		$this->assignRef('config_item_limit', $comParams->get('config_item_limit'));
		$this->assignRef('config_show_links', $comParams->get('config_show_links'));
		$this->assignRef('config_user_view_links', $comParams->get('config_user_view_links'));
		$this->assignRef('config_two_column', $comParams->get('config_two_column'));
		$this->assignRef('config_use_gb', $comParams->get('config_use_gb'));
		$this->assignRef('config_domain_list', $comParams->get('config_domain_list'));
		$this->assignRef('config_emailblock_list', $comParams->get('config_emailblock_list'));
		$this->assignRef('config_captcha', $comParams->get('config_captcha'));
		$this->assignRef('config_captcha_bypass_4member', $comParams->get('config_captcha_bypass_4member'));
		$this->assignRef('config_captcha_maxattempts', $comParams->get('config_captcha_maxattempts'));
		$this->assignRef('config_recap_pubkey', $comParams->get('config_recap_pubkey'));
		$this->assignRef('config_recap_privkey', $comParams->get('config_recap_privkey'));
		$this->assignRef('config_recap_theme', $comParams->get('config_recap_theme'));
		$this->assignRef('config_moduleclass_sfx', $comParams->get('config_moduleclass_sfx'));
		$this->assignRef('config_enable_plugins', $comParams->get('config_enable_plugins'));
		$this->assignRef('config_allowed_plugins', $comParams->get('config_allowed_plugins'));
		$this->assignRef('config_email_mode', $comParams->get('config_email_mode'));
		$this->assignRef('config_email_inc_req', $comParams->get('config_email_inc_req'));
		$this->assignRef('config_email_bcc', $comParams->get('config_email_bcc'));
		$this->assignRef('config_error_logging', $comParams->get('config_error_logging'));
		$this->assignRef('config_editor', $comParams->get('config_editor'));
		$this->assignRef('config_editor_mode', $comParams->get('config_editor_mode'));
		$this->assignRef('config_editor_width', $comParams->get('config_editor_width'));
		$this->assignRef('config_editor_height', $comParams->get('config_editor_height'));
		$this->assignRef('config_show_xtd_buttons', $comParams->get('config_show_xtd_buttons'));
		$this->assignRef('config_community', $comParams->get('config_community'));
		$this->assignRef('config_comments', $comParams->get('config_comments'));
		$this->assignRef('config_show_translate', $comParams->get('config_show_translate'));
		$this->assignRef('config_show_comprofile', $comParams->get('config_show_comprofile'));
		$this->assignRef('config_show_viewed', $comParams->get('config_show_viewed'));
		$this->assignRef('config_show_commentlink', $comParams->get('config_show_commentlink'));
		$this->assignRef('popup', $popup);
		parent::display($tpl);
	}
}
?>