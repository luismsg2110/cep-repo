<?php
/**
* PrayerCenter Component for Joomla
* By Mike Leeper
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
 	global $mainframe, $db,$prayercenteradmin;
	JRequest::setVar( 'hidemainmenu', 1 );
	$db =& JFactory::getDBO();
  $lang =& Jfactory::getLanguage();
  $lang->load( 'com_prayercenter', JPATH_SITE); 
  $version = new JVersion();
  if( (real)$version->RELEASE == 1.5 ) {
    $mailfrom = $mainframe->getCfg('mailfrom');
  } elseif( (real)$version->RELEASE >= 1.6 ){
  	$config = &JFactory::getConfig();
    $mailfrom = $config->get('mailfrom');
  }
  $agrparray=array();
  $agrparray[] = JHTML::_( 'select.option', '-1', 'Public' );
  $agrparray[] = JHTML::_( 'select.option', '18' , 'Registered' );
  $agrparray[] = JHTML::_( 'select.option', '23' , 'Manager' );
  $agrparray[] = JHTML::_( 'select.option', '24' , 'Administrator' );
  $agrparray[] = JHTML::_( 'select.option', '25' , 'Super Administrator' );
  $grparray=array();
  $grparray[] = JHTML::_( 'select.option', '-1', 'Public' );
  $grparray[] = JHTML::_( 'select.option', '18' , 'Registered' );
  $grparray[] = JHTML::_( 'select.option', '23' , 'Special' );
  $forwardarray = array();
  $forwardarray[] = JHTML::_( 'select.option', '0', 'Site Admins');
  $forwardarray[] = JHTML::_( 'select.option', '1', 'All Registered Users');
  $forwardarray[] = JHTML::_( 'select.option', '2', 'Distribution Listing');
  $filterarray = array();
  $filterarray[] = JHTML::_( 'select.option', '0', 'None');
  $filterarray[] = JHTML::_( 'select.option', '1', 'PrayerCenter Word Filter');
  $filterarray[] = JHTML::_( 'select.option', '2', 'WordCensor Plugin');
  $filterarray[] = JHTML::_( 'select.option', '3', 'Bad Word Filter Plugin');
  $filterarray[] = JHTML::_( 'select.option', '4', 'JBehave Plugin');
  $translatearray = array();
  $translatearray[] = JHTML::_( 'select.option', '0', 'No');
  $translatearray[] = JHTML::_( 'select.option', '1', 'Yes - Google Translate v2 - Popup');
  $translatearray[] = JHTML::_( 'select.option', '2', 'Yes - Microsoft Bing Translator - Popup');
  $translatearray[] = JHTML::_( 'select.option', '3', 'Yes - Yahoo! Babel Fish - Popup');
  $translatearray[] = JHTML::_( 'select.option', '4', 'Yes - Google Translate v2 - Inline');
  $translatearray[] = JHTML::_( 'select.option', '5', 'Yes - Microsoft Bing Translator - Inline');
  $captchaarray = array();
  $captchaarray[] = JHTML::_( 'select.option', '0', 'No');
  $captchaarray[] = JHTML::_( 'select.option', '1', 'Yes - PrayerCenter Captcha');
  $captchaarray[] = JHTML::_( 'select.option', '2', 'Yes - Alikonweb SecureForm Plugin');
  $captchaarray[] = JHTML::_( 'select.option', '3', 'Yes - CrossCheck Component');
  $captchaarray[] = JHTML::_( 'select.option', '4', 'Yes - TinCaptcha Component');
  $captchaarray[] = JHTML::_( 'select.option', '5', 'Yes - Moovur Component');
  $captchaarray[] = JHTML::_( 'select.option', '6', 'Yes - ReCaptcha Service');
  $recapthemearray = array();
  $recapthemearray[] = JHTML::_( 'select.option', 'red', 'Red');
  $recapthemearray[] = JHTML::_( 'select.option', 'white', 'White');
  $recapthemearray[] = JHTML::_( 'select.option', 'blackglass', 'Black Glass');
  $recapthemearray[] = JHTML::_( 'select.option', 'clean', 'Clean');
  $commentsarray = array();
  $commentsarray[] = JHTML::_( 'select.option', '0', 'No');
  $commentsarray[] = JHTML::_( 'select.option', '1', 'Yes - JComments Component');
  $approvalarray = array();
  $approvalarray[] = JHTML::_( 'select.option', '0', 'None');
  $approvalarray[] = JHTML::_( 'select.option', '1', 'User Email Confirmation');
  $approvalarray[] = JHTML::_( 'select.option', '2', 'Site Administrators');
  $approvalarray[] = JHTML::_( 'select.option', '3', 'Prayer Moderators');
  $approvalarray[] = JHTML::_( 'select.option', '4', 'Both Admins & Moderators');
  $subapprovalarray = array();
  $subapprovalarray[] = JHTML::_( 'select.option', '0', 'None');
  $subapprovalarray[] = JHTML::_( 'select.option', '1', 'Admin Approval');
  $subapprovalarray[] = JHTML::_( 'select.option', '2', 'Email Confirmation');
  $distribarray = array();
  $distribarray[] = JHTML::_( 'select.option', '1', 'Email');
  $distribarray[] = JHTML::_( 'select.option', '2', 'Private Messaging');
  $distribarray[] = JHTML::_( 'select.option', '3', 'Both');
  $columnarray = array();
  $columnarray[] = JHTML::_( 'select.option', '0', 'One Column');
  $columnarray[] = JHTML::_( 'select.option', '1', 'Two Column');
  $templatearray = array();
  $templatearray[] = JHTML::_( 'select.option', '0', 'Default');
  $templatearray[] = JHTML::_( 'select.option', '1', 'Rounded');
  $templatearray[] = JHTML::_( 'select.option', '2', 'Basic');
  $dateformatarray = array();
  $dateformatarray[] = JHTML::_( 'select.option', 'm-d-Y', 'MM-DD-YYYY');
  $dateformatarray[] = JHTML::_( 'select.option', 'd-m-Y', 'DD-MM-YYYY');
  $dateformatarray[] = JHTML::_( 'select.option', 'Y-m-d', 'YYYY-MM-DD');
  $timeformatarray = array();
  $timeformatarray[] = JHTML::_( 'select.option', 'h:i:s A', '12 Hour');
  $timeformatarray[] = JHTML::_( 'select.option', 'H:i:s', '24 Hour');
  $returnaddressarray = array();
  $returnaddressarray[] = JHTML::_( 'select.option', '0', 'Default Joomla Mail From Return Address');
  $returnaddressarray[] = JHTML::_( 'select.option', '1', 'Custom Mail From Return Address');
  $returnaddressarray[] = JHTML::_( 'select.option', '2', 'Prayer Requester Email As Return Address');
  $modearray = array();
  $modearray[] = JHTML::_( 'select.option', '0', 'Plain Text');
  $modearray[] = JHTML::_( 'select.option', '1', 'HTML');
  if( (real)$version->RELEASE == 1.5 ) {
  	$query = 'SELECT element AS value, name AS text'
			.' FROM #__plugins'
			.' WHERE folder = "editors"'
			.' AND published = 1'
			.' ORDER BY ordering, name'
			;
  } elseif( (real)$version->RELEASE >= 1.6 ){
  	$query = 'SELECT element AS value, name AS text'
			.' FROM #__extensions'
			.' WHERE type = "plugin"'
			.' AND folder = "editors"'
			.' AND enabled = 1'
			.' ORDER BY ordering, name'
			;
  }
	$db->setQuery($query);
	$editorarr = $db->loadObjectList();
  $editorarr2 = array(array('value' => 'default', 'text' => 'Editor - Default (Joomla Global Configuration)'));
  $editorarray = array_merge($editorarr,$editorarr2);
  $editormodearray = array();
  $editormodearray[] = JHTML::_( 'select.option', 'simple', 'Simple');
  $editormodearray[] = JHTML::_( 'select.option', 'advanced', 'Advanced');
  $editormodearray[] = JHTML::_( 'select.option', 'extended', 'Extended');
  $bmservicearray = array();
  $bmservicearray[] = JHTML::_( 'select.option', '1', 'AddThis.com');
  $bmservicearray[] = JHTML::_( 'select.option', '2', 'AddToAny.com');
  $bmservicearray[] = JHTML::_( 'select.option', '3', 'ShareThis.com');
  $bmservicearray[] = JHTML::_( 'select.option', '4', 'SocialTwist.com');
  $bmrssservicearray = array();
  $bmrssservicearray[] = JHTML::_( 'select.option', '0', 'None');
  $bmrssservicearray[] = JHTML::_( 'select.option', '1', 'AddThis.com');
  $bmrssservicearray[] = JHTML::_( 'select.option', '2', 'AddToAny.com');
  $communityarray = array();
  $communityarray[] = JHTML::_( 'select.option', '0', 'None');
  $communityarray[] = JHTML::_( 'select.option', '1', 'Community Builder');
  $communityarray[] = JHTML::_( 'select.option', '2', 'JomSocial');
  $errorlogarray = array();
  $errorlogarray[] = JHTML::_( 'select.option', '0', 'None');
  $errorlogarray[] = JHTML::_( 'select.option', '1', 'Log Failed Only');
  $errorlogarray[] = JHTML::_( 'select.option', '2', 'Log All');
	$query = "SELECT id AS value, CONCAT_WS('-',id,name) AS text"
	. ' FROM #__users'
	. ' WHERE block = 0'
	. ' ORDER BY name'
	;
	$db->setQuery($query);
	$users[] = JHTML::_('select.option',  '0', JText::_( 'Select User' ));
	$usersarray = array_merge( $users, $db->loadObjectList() );
  $script = "javascript:if(config_pms_select.options[config_pms_select.selectedIndex].text!='Select User'){this.form.elements['config_pms_user_list'].value+=(',')+config_pms_select.options[config_pms_select.selectedIndex].text;}dedupe_list('config_pms_user_list');";
  $onblur_script = "javascript:document.adminForm.config_pms_select.selectedIndex=0;";
  $list_script = "javascript:addChr('config_pms_user_list');";
  $moderator_script = "javascript:if(config_moderator_select.options[config_moderator_select.selectedIndex].text!='Select User'){this.form.elements['config_moderator_user_list'].value+=(',')+config_moderator_select.options[config_moderator_select.selectedIndex].text;}dedupe_list('config_moderator_user_list');";
  $moderatoronblur_script = "javascript:document.adminForm.config_moderator_select.selectedIndex=0;";
  $modlist_script = "javascript:addChr('config_moderator_user_list');";
  $preview_script = "javascript:showImage(this.value);";
  $livesite = JURI::root();
  ?>
  <SCRIPT LANGUAGE="Javascript" type="text/javascript">
  function ltrim(str, chars) {
  	chars = chars || "\\s";
  	return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
  }
  function rtrim(str, chars) {
  	chars = chars || "\\s";
  	return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
  }
  function dedupe_list(sel)
  {
  	var count = 0;
  	var mainlist = document.getElementById(sel).value;
  	mainlist = mainlist.replace(/\r/gi, "");
  	mainlist = mainlist.replace(/\n+/gi, "");
    mainlist = ltrim(mainlist,",");
  	var listvalues = new Array();
  	var newlist = new Array();
  	listvalues = mainlist.split(",");
  	var hash = new Object();
  	for (var i=0; i<listvalues.length; i++)
  	{
  		if (hash[listvalues[i].toLowerCase()] != 1)
  		{
        if(listvalues[i] != 'No User'){
  			newlist = newlist.concat(listvalues[i]);
        hash[listvalues[i].toLowerCase()] = 1
  		  }
      }
  		else { count++; }
  	}
  	document.getElementById(sel).value = newlist.join(",");
  }
  function showImage(img)
  {
    var site = "<?php echo $livesite; ?>";
    var imgObj = document.images['config_preview'];
    imgObj.src = site + 'components/com_prayercenter/assets/images/' + img;
  }
  function addChr(sel){
    var strlist = document.getElementById(sel).value;
    if(strlist.length == 0) return false;
    strlist = rtrim(strlist,",");
    document.getElementById(sel).value = strlist;
  }
  function submitbutton(pressbutton) {
    var form = document.adminForm;
    if (pressbutton == 'cancel') {
      submitform( pressbutton );
      return;
      }
      submitform( pressbutton );
  }
  </script>
  <?php
  jimport('joomla.html.pane');
	JHTML::_('behavior.tooltip');
	?>
 		<form action="index.php?option=com_prayercenter&task=config" method="post" name="adminForm">
    <?php if($this->popup){ ?>
		<fieldset>
			<div style="float: right">
				<button type="button" onclick="submitbutton('save');window.top.setTimeout('window.parent.document.getElementById(\'sbox-window\').close()', 700);">
					<?php echo JText::_( 'Save' );?></button>
				<button type="button" onclick="window.parent.document.getElementById('sbox-window').close();">
					<?php echo JText::_( 'Cancel' );?></button>&nbsp;&nbsp;&nbsp;&nbsp;
			</div>
      <div class="configuration" >
				<?php echo JText::_('PrayerCenter Parameters') ?>
			</div>
		</fieldset>
		<?php }?>
		<div>
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Component Wide Settings' ); ?></legend>
				<table class="admintable">
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Show Page Headers::Choose yes to display the component title as page headers on component frontend.">Show Page Headers:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_show_page_headers]", '1', $this->config_show_page_headers); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Show Image::Choose yes to display image or image slideshow on component frontend.">Show Image:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_show_image]", '1', $this->config_show_image); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Use Image Slideshow::Choose yes to use a randomly generated image slideshow (image files stored in the components\com_prayercenter\assets\images\slideshow directory) or no to use a specific image selected on the Image to Display configuration item.">Use Image Slideshow:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_use_slideshow]", '1', $this->config_use_slideshow); ?></td>
		</tr>
		<tr>
    	<td class="key"><span class="editlinktip hasTip" title="Image Slideshow Speed::The amount of time it takes for the image to change">Image Slideshow Speed:</span></td>
			<td><input type="text" name="params[config_slideshow_speed]" value="<?php echo $this->config_slideshow_speed; ?>" size="10" /></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Image Slideshow Duration::The amount of time between image changes">Image Slideshow Duration:</span></td>
			<td><input type="text" name="params[config_slideshow_duration]" value="<?php echo $this->config_slideshow_duration; ?>" size="10" /></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Image to Display::Image displayed on component frontend">Image to Display:</span></td>
			<td><img name="config_preview" src="<?php echo JURI::root().'components/com_prayercenter/assets/images/'.$this->config_imagefile;?>" height="50" width="50" />
      <div align="left" valign="middle"><?php echo JHTML::_( 'list.images', "params[config_imagefile]", $this->config_imagefile, 'onchange="'.$preview_script.'"', '/components/com_prayercenter/assets/images' ); ?></div></td>
    </tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Show Menu::Choose yes to display menu on component frontend.">Show Menu:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_show_menu]", '1', $this->config_show_menu); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Show Heading Text::Choose yes to display text at top of component frontend.">Show Heading Text:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_show_header_text]", '1', $this->config_show_header_text); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Use Squeezebox Popup Effect::Select to use Squeezebox popup effect or standard browser">Use Squeezebox Popup Effect:</td>
			<td><?php echo JHTML::_('select.booleanlist', "params[config_use_gb]", '1', $this->config_use_gb); ?></td>
		</tr>
		</table>
		</fieldset>
		<fieldset class="adminform">
		<legend><?php echo JText::_( 'Prayer Listing Page' ); ?></legend>
		<table class="admintable">
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Users Allowed to View::Users that are allowed to view prayer requests">Users Allowed to View:</span></td>
			<td><?php echo JHTML::_('select.genericlist', $grparray, 'params[config_user_view]', 'class="inputbox" size="1"', 'value', 'text', intval( $this->config_user_view ) ); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="View Prayer Listing Template::Select the template for the View Prayer Listing page">View Prayer Listing Template:</span></td>
			<td><?php echo JHTML::_( 'select.genericlist', $templatearray, "params[config_view_template]", 'size="1" class="inputbox"', 'value', 'text', $this->config_view_template ); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Show Name of Requester::Show name of requester on prayer listing">Show Name of Requester:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',  'params[config_show_requester]', 'class="inputbox"', $this->config_show_requester ); ?></td>
			</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Show Date of Request::Show date of request on prayer listing">Show Date of Request:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_show_date]", '1', $this->config_show_date); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Date Format::Date format of prayer requests">Prayer Request Date Format:</span></td>
			<td><?php echo JHTML::_( 'select.genericlist', $dateformatarray, "params[config_date_format]", 'size="1" class="inputbox"', 'value', 'text', $this->config_date_format ); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Time Format::Time format of prayer requests">Prayer Request Time Format:</span></td>
			<td><?php echo JHTML::_( 'select.genericlist', $timeformatarray, "params[config_time_format]", 'size="1" class="inputbox"', 'value', 'text', $this->config_time_format ); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Show Timezone::Show timezone with date of request on prayer listing">Show Timezone:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_show_tz]", '1', $this->config_show_tz); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Show Print Icon::Show print icon on view prayer listing page of component frontend">Show Print Icon:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_show_print]", '1', $this->config_show_print); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Show PDF Icon::Show pdf icon on view prayer listing page of component frontend">Show PDF Icon:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_show_pdf]", '1', $this->config_show_pdf); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Show Email Icon::Show email icon on view prayer listing page of component frontend">Show Email Icon:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_show_email]", '1', $this->config_show_email); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Show Social Bookmarks::Show social bookmarks on view prayer listing and show request pages of component frontend.  Select service under the 3rd Party Component/Plugin/Service Integration section of this configuration page.">Show Social Bookmarks:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_show_bookmarks]", '1', $this->config_show_bookmarks); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Show Daily/Weekly Print Icon::Show daily and weekly print icons on view prayer listing page of component frontend">Show Daily/Weekly Print Icons:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_show_dwprint]", '1', $this->config_show_dwprint); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Requests Per Page::Number of requests per page displayed on prayer listing">Requests Per Page:</span></td>
			<td><input type="text" name="params[config_rows]" value="<?php echo $this->config_rows; ?>" size="10" /></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Request Text Limit::Length of request text to display on prayer listing. Set at 0 for unlimited length (This will also disable the READMORE link).">Request Text Limit:</span></td>
			<td><input type="text" name="params[config_req_length]" value="<?php echo $this->config_req_length; ?>" size="10" /></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Show Viewed Count::Show number of times request has been viewed on view prayer listing page of component frontend">Show Viewed Count:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_show_viewed]", '1', $this->config_show_viewed); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Show Number of Comments::Show number of comments made for request on view prayer listing page of component frontend. Requires Commenting component.">Show Number of Comments:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_show_commentlink]", '1', $this->config_show_commentlink); ?></td>
		</tr>
		</table>
		</fieldset>
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'View Request Page' ); ?></legend>
				<table class="admintable">
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Show Language Translator::Show Language Translator option. This will allow the viewer to translate the prayer request temporarily into one of several supported languages. All popup translator options are free services but may be affected by popup blockers.  As of December 1, 2011, Google Translate v2 - Inline is a paid service and requires an API key.  Visit the link, http://code.google.com/intl/en/ and click on API Console and then API Access to create one.  Click on Billing to register for service.  Edit the components/com_prayercenter/assets/js/gtranslate.js file to add the API Key.  Microsoft Bing Translator - Inline is free for the first 2000 transactions/month, afterwards it is a paid service.  It requires an Application ID.  First, Subscribe to the Microsoft Translator API by clicking this link https://datamarket.azure.com/dataset/1899a118-d202-492c-aa16-ba21c33c06cb and choose the subscription option for your site.  Then, register the PrayerCenter application by visiting this link https://datamarket.azure.com/developer/applications/.  Then, obtain the Application ID by visiting this link https://ssl.bing.com/webmaster/developers/appids.aspx.   Edit the components/com_prayercenter/assets/js/mstranslate.js file to add the Application ID.  Inline services will make translations within the prayer request box on the View Prayer Request page.">Show Language Translator:</span></td>
			<td><?php echo JHTML::_('select.genericlist', $translatearray, 'params[config_show_translate]', 'class="inputbox" size="1"', 'value', 'text', intval( $this->config_show_translate ) ); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Show Coummnity Profile::Show user profile of requester. (Requires Community Builder or JomSocial component)">Show Community Profile:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_show_comprofile]", '0', $this->config_show_comprofile); ?></td>
		</tr>
		</table>
		</fieldset>
		<fieldset class="adminform">
		<legend><?php echo JText::_( 'Request Form Page' ); ?></legend>
		<table class="admintable">
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Users Allowed to Post::Users that are allowed to post new prayer requests">Users Allowed to Post:</span></td>
			<td><?php echo JHTML::_('select.genericlist', $grparray, 'params[config_user_post]', 'class="inputbox" size="1"', 'value', 'text', intval( $this->config_user_post ) ); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Show Email Address Option::Display requester email address option on request submission page of component frontend">Show Email Address Option:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_email_option]", '1', $this->config_email_option); ?></td>
		</tr>
    <tr>
			<td class="key"><span class="editlinktip hasTip" title="Show Private Submission Option::Show submit private option checkbox on request submission page of component frontend">Show Private Submission Option:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_show_priv_option]", '1', $this->config_show_priv_option); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Editor to Use For Requests::Editor to Use For Requests. Choose No Editor for normal textbox display">Editor to Use For Requests:</span></td>
			<td><?php echo JHTML::_( 'select.genericlist', $editorarray, "params[config_editor]", 'size="1" class="inputbox"', 'value', 'text', $this->config_editor ); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Editor Mode::Select editor mode">Editor Mode:</span></td>
			<td><?php echo JHTML::_('select.genericlist', $editormodearray, 'params[config_editor_mode]', 'class="inputbox" size="1"', 'value', 'text', intval( $this->config_editor_mode ) ); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Show XTD Editor Buttons Option::Show XTD Editor buttons below the editor text area">Show XTD Editor Buttons Option:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_show_xtd_buttons]", '1', $this->config_show_xtd_buttons); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Editor Width::Width of editor/request box displayed on prayer listing. Set as percentage (%) or px.">Editor Width:</span></td>
			<td><input type="text" name="params[config_editor_width]" value="<?php echo $this->config_editor_width; ?>" size="10" /></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Editor Height::Height of editor/request box displayed on prayer listing. Set as percentage (%) or px.">Editor Height:</span></td>
			<td><input type="text" name="params[config_editor_height]" value="<?php echo $this->config_editor_height; ?>" size="10" /></td>
		</tr>
		</table>
		</fieldset>
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Subscription Page' ); ?></legend>
				<table class="admintable">
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Show Subscription Page::Select to show Prayer Chain Subscription page">Show Subscription Page:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_show_subscribe]", '0', $this->config_show_subscribe); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Users Allowed to Subscribe::Users that are allowed to subscribe to the Prayer Chain">Users Allowed to Subscribe:</span></td>
			<td><?php echo JHTML::_('select.genericlist', $grparray, 'params[config_user_subscribe]', 'class="inputbox" size="1"', 'value', 'text', intval( $this->config_user_subscribe ) ); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="New Subscriber Approval Method::Select approval type of new Prayer Chain subscribers">New Subscriber Approval Method:</span></td>
			<td><?php echo JHTML::_( 'select.genericlist', $subapprovalarray, "params[config_admin_approve_subscribe]", 'size="1" class="inputbox"', 'value', 'text', $this->config_admin_approve_subscribe ); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Email Subscription Response::Select to send an email response to new subscribers or unsubscribers">Email Subscription Response:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_email_subscribe]", '0', $this->config_email_subscribe); ?></td>
		</tr>
		</table>
		</fieldset>
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Web Links Page' ); ?></legend>
				<table class="admintable">
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Show Links Page::Select to show links page">Show Links Page:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_show_links]", '0', $this->config_show_links); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Users Allowed to View Links::Users that are allowed to view links">Users Allowed to View Links:</span></td>
			<td><?php echo JHTML::_('select.genericlist', $grparray, 'params[config_user_view_links]', 'class="inputbox" size="1"', 'value', 'text', intval( $this->config_user_view_links ) ); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Display Type::Number of columns to display">Display Type:</span></td>
			<td><?php echo JHTML::_( 'select.genericlist', $columnarray, "params[config_two_column]", 'size="1" class="inputbox"', 'value', 'text', $this->config_two_column ); ?></td>
		</tr>
		</table>
		</fieldset>
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Devotionals Page' ); ?></legend>
				<table class="admintable">
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Show Devotional Page::Select to show devotional page">Show Devotional Page:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_show_devotion]", '0', $this->config_show_devotion); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Users Allowed to View Devotionals::Users that are allowed to view devotionals">Users Allowed to View Devotionals:</span></td>
			<td><?php echo JHTML::_('select.genericlist', $grparray, 'params[config_user_view_devotional]', 'class="inputbox" size="1"', 'value', 'text', intval( $this->config_user_view_devotional ) ); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Enable Devotional Feed Cache::Enable caching of RSS feed data">Enable Devotional Feed Cache:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_enable_cache]", '0', $this->config_enable_cache); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Feed Update Frequency::Feed update frequency (in seconds)">Feed Update Frequency (in seconds):</span></td>
			<td><input type="text" name="params[config_update_time]" value="<?php echo $this->config_update_time; ?>" size="10" /></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Show Feed Images::Show images included with feed">Show Feed Images:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_feed_image]", '0', $this->config_feed_image); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Show Feed Description::Show feed description">Show Feed Description:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_feed_descr]", '0', $this->config_feed_descr); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Feed Item List Limit::Limit the number of RSS feed items displayed (-1 = unlimited)">Feed Item List Limit <br />(-1 = unlimited):</span></td>
			<td><input type="text" name="params[config_item_limit]" value="<?php echo $this->config_item_limit; ?>" size="10" /></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Show Feed Item Description::Show feed item description">Show Feed Item Description:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_item_descr]", '0', $this->config_item_descr); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Feed Item Word Limit::Limit the number of words in feed item description (-1 = unlimited)">Feed Item Word Limit <br />(-1 = unlimited):</span></td>
			<td><input type="text" name="params[config_word_count]" value="<?php echo $this->config_word_count; ?>" size="10" /></td>
		</tr>
		</table>
		</fieldset>
    <fieldset class="adminform">
		<legend><?php echo JText::_( 'Request Approval' ); ?></legend>
		<table class="admintable">
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Users Allowed to Publish::Users that are allowed to publish prayer requests for viewing">Users Allowed to Publish:</span></td>
			<td><?php echo JHTML::_( 'select.genericlist', $agrparray, "params[config_user_publish]", 'size="1" class="inputbox"', 'value', 'text', $this->config_user_publish ); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Approval Distribution::Approvers or approval method of new prayer requests">Approval Distribution:</span></td>
			<td><?php echo JHTML::_( 'select.genericlist', $approvalarray, "params[config_use_admin_alert]", 'size="1" class="inputbox"', 'value', 'text', $this->config_use_admin_alert ); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Distribution Type::Type of approval prayer request distribution">Distribution Type:</span></td>
			<td><?php echo JHTML::_( 'select.genericlist', $distribarray, "params[config_admin_distrib_type]", 'size="1" class="inputbox"', 'value', 'text', $this->config_admin_distrib_type ); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Prayer Request Moderators List::Users allowed to approve new prayer requests">Prayer Request Moderators List:</span></td>
		<td><?php echo JHTML::_('select.genericlist', $usersarray, 'config_moderator_select', 'class="inputbox" size="1" '. 'onclick="'.$moderator_script.'" onblur="'.$moderatoronblur_script.'"', 'value', 'text', ''); ?>&nbsp;&nbsp;Select Moderator
			<br /><textarea name="params[config_moderator_user_list]" id="config_moderator_user_list" cols="30" rows="5" onBlur="<?php echo $modlist_script;?>"><?php echo $this->config_moderator_user_list; ?></textarea></td>
		</tr>
		</table>
		</fieldset>
		<fieldset class="adminform">
		<legend><?php echo JText::_( 'Request Distribution' ); ?></legend>
		<table class="admintable">
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Distribution Type::Type of approved prayer request distribution">Distribution Type:</span></td>
			<td><?php echo JHTML::_( 'select.genericlist', $distribarray, "params[config_distrib_type]", 'size="1" class="inputbox"', 'value', 'text', $this->config_distrib_type ); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Forward Prayer Requests To::Approved prayer requests are forwarded to these users">Forward Prayer Requests To:</span></td>
			<td><?php echo JHTML::_( 'select.genericlist', $forwardarray, "params[config_email_request]", 'size="1" class="inputbox"', 'value', 'text', $this->config_email_request ); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Email Distribution List::Email distribution list (separate by comma)">Email Distribution List (separate by comma):</span></td>
			<td><textarea name="params[config_email_list]" cols="30" rows="5"><?php echo strip_tags($this->config_email_list); ?></textarea></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Prayer Request Email Return Address::Choose whether to use the Mail From return address in the Joomla Global Configuration or use a custom return address for PrayerCenter emails">Prayer Request Email Return Address:</span></td>
			<td><?php echo JHTML::_( 'select.genericlist', $returnaddressarray, "params[config_return_addr]", 'size="1" class="inputbox"', 'value', 'text', $this->config_return_addr ); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Custom Email Return Address::Custom Email return address to use on all PrayerCenter emails">Custom Email Return Address:</span></td>
			<td><input type="text" name="params[config_custom_ret_addr]" value="<?php echo $this->config_custom_ret_addr; ?>" size="50" /></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Private Messaging System Distribution List::List of users that will receive approved new prayer requests via PMS">Private Messaging System Distribution List:</span></td>
			<td><?php echo JHTML::_('select.genericlist', $usersarray, 'config_pms_select', 'class="inputbox" size="1" '. 'onclick="'.$script.'" onblur="'.$onblur_script.'"', 'value', 'text', ''); ?>&nbsp;&nbsp;Select PMS User
			<br /><textarea name="params[config_pms_user_list]" id="config_pms_user_list" cols="30" rows="5" onBlur="<?php echo $list_script;?>"><?php echo str_replace('<br />', "\n", $this->config_pms_user_list); ?></textarea></td>
		</tr>
 		</table>
		</fieldset>
		<fieldset class="adminform">
		<legend><?php echo JText::_( '3rd Party Component/Plugin/Service Integration' ); ?></legend>
		<table class="admintable">
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Enable User Comments Component Integration::Show user comments links on prayer listing. Show user comments and form on prayer detail page.  Requires the JComments component (v2.2.0.2 or higher)">Enable User Comments Component Integration:</span></td>
			<td><?php echo JHTML::_( 'select.genericlist', $commentsarray, "params[config_comments]", 'size="1" class="inputbox"', 'value', 'text', $this->config_comments ); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Enable User Community Component Integration::Show user community component avatars and user information on prayer listing. Requires the Community Builder component (v1.2.1 or higher) or JomSocial component (1.6.290 or higher)">Enable User Community Component Integration:</span></td>
			<td><?php echo JHTML::_( 'select.genericlist', $communityarray, "params[config_community]", 'size="1" class="inputbox"', 'value', 'text', $this->config_community ); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Enable Content Plugins::Enable Joomla content plugins to be applied to viewing prayer requests">Enable Content Plugins:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_enable_plugins]", '0', $this->config_enable_plugins); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Content Plugins Allowed::Joomla content plugins allowed to apply to viewing prayer requests on prayer listing. (Separate by comma)">Content Plugins Allowed (Separate by comma):</span></td>
			<td><textarea name="params[config_allowed_plugins]" id="config_allowed_plugins" cols="30" rows="3"><?php echo $this->config_allowed_plugins; ?></textarea></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Select Private Messaging System::Private messaging system used to distibute approved prayer request.">Select Private Messaging System:</span></td>
			<td><?php echo $prayercenteradmin->FileSel( "params[config_pms_plugin]", $this->config_pms_plugin, "", 'plugins/pms' ); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Social Bookmark Service::Service used to generate the social bookmark links enabled in the Prayer Listing Page section of this configuration page.">Social Bookmark Service:</span></td>
			<td><?php echo JHTML::_( 'select.genericlist', $bmservicearray, "params[config_bm_service]", 'size="1" class="inputbox"', 'value', 'text', $this->config_bm_service ); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Social Bookmark Service ID::Account ID used in association with the selected Social Bookmarking service.  This is needed only if you are wanting to track bookmark usage information.  Requires registration with the respective service provider.">Social Bookmark Service ID:</span></td>
			<td><input type="text" name="params[config_bm_service_id]" value="<?php echo $this->config_bm_service_id; ?>" size="30" /></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Enable Google Analytics Code::Enable code to generate Google Analytics usage data.  Enable only if you have not already added this code to your site.  Requires registration on Google Analytics (http://www.google.com/analytics/)">Enable Google Analytics Code:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_use_gcode]", '1', $this->config_use_gcode); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Google Analytics ID::Google ID used in association with Google Analytics service (UA-xxxxxx-x).">Google Analytics ID:</span></td>
			<td><input type="text" name="params[config_google_id]" value="<?php echo $this->config_google_id; ?>" size="20" /></td>
		</tr>
		</table>
		</fieldset>
		<fieldset class="adminform">
		<legend><?php echo JText::_( 'Word Filtering' ); ?></legend>
		<table class="admintable">
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Select Word Filter::Select word filter to use">Select Word Filter:</span></td>
			<td><?php echo JHTML::_( 'select.genericlist', $filterarray, "params[config_use_wordfilter]", 'size="1" class="inputbox"', 'value', 'text', $this->config_use_wordfilter ); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Bad Words to be Filtered::Bad words to be replaced (separate by comma)">Bad Words to be Filtered (Separate by comma):</span></td>
			<td><textarea name="params[config_bad_words]" value="<?php echo $this->config_bad_words; ?>" cols="30" rows="5" /></textarea></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Replace Bad Words With::Bad words are replaced with this string">Replace Bad Words With:</span></td>
			<td><input type="text" name="params[config_replace_word]" value="<?php echo $this->config_replace_word; ?>" size="50" /></td>
		</tr>
		</table>
		</fieldset>
		<fieldset class="adminform">
		<legend><?php echo JText::_( 'RSS Feeds' ); ?></legend>
		<table class="admintable">
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Show RSS Feed Icon::Show latest prayer request RSS feeds icon on the bottom of each page of component frontend">Show RSS Feed Icon:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_show_rss]", '1', $this->config_show_rss); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Social Bookmark RSS Feed Service::Service used to generate the social bookmark RSS feed links">Social Bookmark RSS Feed Service:</span></td>
			<td><?php echo JHTML::_( 'select.genericlist', $bmrssservicearray, "params[config_bmrss_service]", 'size="1" class="inputbox"', 'value', 'text', $this->config_bmrss_service ); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Enable RSS Feed Cache::Enable caching of RSS feed data">Enable RSS Feed Cache:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_enable_rss_cache]", '0', $this->config_enable_rss_cache); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="RSS Feed Update Frequency::RSS feed update frequency (in seconds)">RSS Feed Update Frequency (in seconds):</span></td>
			<td><input type="text" name="params[config_rss_update_time]" value="<?php echo $this->config_rss_update_time; ?>" size="10" /></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Number of RSS Feeds Listed::Number of RSS feeds listed">Number of RSS Feeds Listed:</span></td>
			<td><input type="text" name="params[config_rss_num]" value="<?php echo $this->config_rss_num; ?>" size="10" /></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="Limit RSS Feed Text Length::Limit RSS Feed Text Length">Limit RSS Feed Text Length:</span></td>
			<td><?php echo JHTML::_('select.booleanlist',"params[config_rss_limit_text]", '0', $this->config_rss_limit_text); ?></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="RSS Feed Text Length::Length of RSS Feed Text">RSS Feed Text Length:</span></td>
			<td><input type="text" name="params[config_rss_text_length]" value="<?php echo $this->config_rss_text_length; ?>" size="10" /></td>
		</tr>
		<tr>
			<td class="key"><span class="editlinktip hasTip" title="RSS Feed Authentication Key::Authentication key to help secure RSS Feed text. Used for added security when prayer listing is restricted to registered users. Warning: This does not totally secure the prayer listing feed. Anyone with the feed URL will be able to view the listing but should be sufficient in a controlled environment.">RSS Feed Authentication Key:</span></td>
			<td><input type="text" name="params[config_rss_authkey]" value="<?php echo $this->config_rss_authkey; ?>" size="30" /></td>
		</tr>
    </table>
		</fieldset>
		<fieldset class="adminform">
		<legend><?php echo JText::_( 'Automatic Purging' ); ?></legend>
		<table class="admintable">
    		<tr>
    			<td class="key"><span class="editlinktip hasTip" title="Enable Auto Purge of Requests::Select to enable auto purging of old prayer requests">Enable Auto Purge of Requests:</span></td>
    			<td><?php echo JHTML::_('select.booleanlist',"params[config_allow_purge]", '0', $this->config_allow_purge); ?></td>
    		</tr>
    		<tr>
    			<td class="key"><span class="editlinktip hasTip" title="Global Request Retention::Length of time requests will be kept (in days)">Global Request Retention (days):</span></td>
    			<td><input type="text" name="params[config_request_retention]" value="<?php echo $this->config_request_retention; ?>" size="10" /></td>
    		</tr>
    		<tr>
    			<td class="key"><span class="editlinktip hasTip" title="Global Archived Request Retention::Length of time archived requests will be kept (in days)">Global Archived Request Retention (days):</span></td>
    			<td><input type="text" name="params[config_archive_retention]" value="<?php echo $this->config_archive_retention; ?>" size="10" /></td>
    		</tr>
				<tr>
				</table>
			</fieldset>
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Spam Prevention' ); ?></legend>
				<table class="admintable">
    		<tr>
    			<td class="key"><span class="editlinktip hasTip" title="Use Spam Prevention::Enable spam protection for prayer requests">Use Spam Prevention:</span></td>
    			<td><?php echo JHTML::_('select.booleanlist',"params[config_use_spamcheck]", '0', $this->config_use_spamcheck); ?></td>
    		</tr>
    		<tr>
    			<td class="key"><span class="editlinktip hasTip" title="Use CAPTCHA Security Images::Use CAPTCHA security images, on request submission page of component frontend, to reduce spam.  Requires the php_gd2 PHP extension to be installed/enabled on your server.  ReCaptcha is only supported to display 1 ReCaptcha image on the page at 1 time.  If using the PC submit or subscription modules, make sure Joomla is configured to only display 1 per page, including the new request page of the PC component.">Use CAPTCHA Security Images:</span></td>
    			<td><?php echo JHTML::_( 'select.genericlist', $captchaarray, "params[config_captcha]", 'size="1" class="inputbox"', 'value', 'text', $this->config_captcha ); ?></td>
    		</tr>
    		<tr>
    			<td class="key"><span class="editlinktip hasTip" title="ReCaptcha Public Key::Public key used in conjuction with the ReCaptcha CAPTCHA service.  This requires that you register your site domain at https://www.google.com/recaptcha/admin/create.">ReCaptcha Public Key (Required if choosing ReCaptcha option above):</span></td>
    			<td><input type="text" name="params[config_recap_pubkey]" value="<?php echo $this->config_recap_pubkey; ?>" size="60" /></td>
    		</tr>
    		<tr>
    			<td class="key"><span class="editlinktip hasTip" title="ReCaptcha Private Key::Private key used in conjuction with the ReCaptcha CAPTCHA service.  This requires that you register your site domain at https://www.google.com/recaptcha/admin/create.">ReCaptcha Private Key (Required if choosing ReCaptcha option above):</span></td>
    			<td><input type="text" name="params[config_recap_privkey]" value="<?php echo $this->config_recap_privkey; ?>" size="60" /></td>
    		</tr>
    		<tr>
    			<td class="key"><span class="editlinktip hasTip" title="ReCaptcha Theme::Color theme used in conjuction with the ReCaptcha CAPTCHA service.  This requires that you register your site domain at https://www.google.com/recaptcha/admin/create.">ReCaptcha Theme (Optional when choosing ReCaptcha option above):</span></td>
    			<td><?php echo JHTML::_( 'select.genericlist', $recapthemearray, "params[config_recap_theme]", 'size="1" class="inputbox"', 'value', 'text', $this->config_recap_theme ); ?></td>
    		</tr>
    		<tr>
    			<td class="key"><span class="editlinktip hasTip" title="Maximum CAPTCHA Attempts Allowed::Maximum number of CAPTCHA attempts allowed per session">Maximum CAPTCHA Attempts Allowed:</span></td>
    			<td><input type="text" name="params[config_captcha_maxattempts]" value="<?php echo $this->config_captcha_maxattempts; ?>" size="10" /></td>
    		</tr>
    		<tr>
    			<td class="key"><span class="editlinktip hasTip" title="Bypass Captcha for Registered Users::Choose to bypass Captcha entry for registered users and require it for non-registered users">Bypass Captcha for Registered Users:</span></td>
    			<td><?php echo JHTML::_('select.booleanlist',"params[config_captcha_bypass_4member]", '0', $this->config_captcha_bypass_4member); ?></td>
    		</tr>
    		<tr>
    			<td class="key"><span class="editlinktip hasTip" title="Email domains to be Blocked::Email domains that will be blocked from being used on the PrayerCenter request form and subscription form (ie. test.com) (separate by comma)">Email domains to be Blocked (Separate by comma):</span></td>
    			<td><textarea name="params[config_domain_list]" value="<?php echo $this->config_domain_list; ?>"  cols="30" rows="5" /></textarea></td>
    		</tr>
    		<tr>
    			<td class="key"><span class="editlinktip hasTip" title="Email Addresses to be Blocked::Email addresses that will be blocked from being used on the PrayerCenter request form and subscription form (ie. user@test.com) (separate by comma)">Email addresses to be Blocked (Separate by comma):</span></td>
    			<td><textarea name="params[config_emailblock_list]" value="<?php echo $this->config_emailblock_list; ?>"  cols="30" rows="5" /></textarea></td>
    		</tr>
				</table>
			</fieldset>
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Miscellaneous' ); ?></legend>
				<table class="admintable">
    		<tr>
    			<td class="key"><span class="editlinktip hasTip" title="Use Alternate Menu Module Class::By default PrayerCenter uses your templates MAINLEVEL CSS menu module class. To define an alternative class the PrayerCenter CSS contains a MAINLEVELALT definition to use">Use Alternate Menu Module Class:</span></td>
    			<td><?php echo JHTML::_('select.booleanlist',"params[config_moduleclass_sfx]", '0', $this->config_moduleclass_sfx); ?></td>
    		</tr>
    		<tr>
    			<td class="key"><span class="editlinktip hasTip" title="Email Mode::Select the mode type for outgoing email messages">Email Mode:</span></td>
    			<td><?php echo JHTML::_( 'select.genericlist', $modearray, "params[config_email_mode]", 'size="1" class="inputbox"', 'value', 'text', $this->config_email_mode ); ?></td>
    		</tr>
    		<tr>
    			<td class="key"><span class="editlinktip hasTip" title="Include request in email message::Choose whether to include the prayer request in the outgoing email messages.">Include request in email message:</span></td>
    			<td><?php echo JHTML::_('select.booleanlist',"params[config_email_inc_req]", '1', $this->config_email_inc_req); ?></td>
    		</tr>
    		<tr>
    			<td class="key"><span class="editlinktip hasTip" title="Send Recipients as BCC. Adds copy to site e-mail::Choose whether to hide recipient list in the outgoing email messages.">Send Recipients as BCC. Adds copy to site e-mail:</span></td>
    			<td><?php echo JHTML::_('select.booleanlist',"params[config_email_bcc]", '1', $this->config_email_bcc); ?></td>
    		</tr>
    		<tr>
    			<td class="key"><span class="editlinktip hasTip" title="Email Error Logging::Select to enable simple system error logging for outgoing email messages.  The log is viewable on the Utilities page accessed from the PrayerCenter CPanel.  Warning:  (The 'Log All' setting may cause the log to grow quite large.  Use with caution.)">Email Error Logging:</span></td>
    			<td><?php echo JHTML::_( 'select.genericlist', $errorlogarray, "params[config_error_logging]", 'size="1" class="inputbox"', 'value', 'text', $this->config_error_logging ); ?></td>
    		</tr>
    		<tr>
    			<td class="key"><span class="editlinktip hasTip" title="Show Developer Credit::Show developer credit on the bottom of each page of component frontend">Show Developer Credit:</span></td>
    			<td><?php echo JHTML::_('select.booleanlist',"params[config_show_credit]", '1', $this->config_show_credit ); ?></td>
    		</tr>
				</table>
			</fieldset><br />
		</div>
		<div class="clr"></div>
		<input type="hidden" name="option" value="com_prayercenter" />
		<input type="hidden" name="controller" value="prayercenter" />
		<input type="hidden" name="task" value="" />
		<?php echo JHTML::_( 'form.token' ); ?>
		</form>
		<?php
    if(!$this->popup){
    footer();
    }
?>