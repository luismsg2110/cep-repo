<?php
/**
* PrayerCenter Component
* 
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
*/
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view');
class PrayerCenterViewEditReq extends JView
{
	function display( $tpl = null)
	{
	global $db;
  $cid 		= JRequest::getVar( 'cid', array(0), '', 'array' );
  $id = $cid[0];
  $db =& JFactory::getDBO();
  $user = &JFactory::getUser();
  $errorlevel=error_reporting();
  error_reporting($errorlevel & ~E_NOTICE);
	$query = "SELECT *"
	. "\n FROM #__prayercenter"
	. "\n WHERE id = '".$id."'"
	;
  $db->setQuery($query);
  $row = $db->loadObjectList();
	$crow =& JTable::getInstance('prayercenter', 'Table');
	$crow->load( (int)$id );
  $query = "SELECT name FROM #__users WHERE id='".$row->checked_out."'";
  $db->setQuery($query);
  $moduser = $db->loadObjectList();
	if ($crow->isCheckedOut( $user->get('id'))) {
		$error = "This prayer request is currently being edited by ".$moduser[0]->name.".";
			JError::raiseWarning(0, $error );
	}
	else {
  if ( $id ) {
		$crow->checkout( $user->get('id') );
    }
  }
  error_reporting($errorlevel);
		$this->assignRef('rows', $row[0]);
		$this->assignRef('option', $option);
		parent::display($tpl);
	}
}
?>
