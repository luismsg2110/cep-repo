<?php
/**
* PrayerCenter Component for Joomla
* By Mike Leeper
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
	global $mainframe,$prayercenteradmin;
  $app = JFactory::getApplication('administrator');
  $version = new JVersion();
	JRequest::setVar( 'hidemainmenu', 1 );
	$row = $this->rows;
  $lang =& Jfactory::getLanguage();
  $lang->load( 'com_prayercenter', JPATH_SITE);
  $config_editor = 'tinymce'; 
  jimport( 'joomla.plugin.plugin' );
  $enabled = JPluginHelper::isEnabled('editors','tinymce');
  if(!$enabled) $config_editor = 'none';
  $editor =& JFactory::getEditor($config_editor);
  $eparams = array('mode'=>'simple');
  if( (real)$version->RELEASE == 1.5 ) {
 		$template	= $mainframe->getTemplate();
    ?>
    <style type="text/css">
    .icon-32-print 	{ background-image: url('templates/khepri/images/toolbar/icon-32-print.png'); }
    </style>
    <?php
  } elseif( (real)$version->RELEASE >= 1.6 ){
 		$template	= $app->getTemplate();
    ?>
    <style type="text/css">
    .icon-32-print 	{ background-image: url('templates/bluestork/images/toolbar/icon-32-print.png'); }
    </style>
    <?php
  }
	$print_link = "index.php?option=com_prayercenter&amp;task=view_req&amp;id=".$row->id."&amp;tmpl=component";
  $newtopicarray = $prayercenteradmin->PCgetTopics();
	$sb = & JToolBar::getInstance('toolbar');
	$sb->appendButton( 'Popup', 'print', 'Print', $print_link, 600, 380 );
  JHTML::_('behavior.tooltip');
  ?>
	<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		var form = document.adminForm;
		if (pressbutton == 'cancel') {
			submitform( pressbutton );
			return;
			}
			submitform( pressbutton );
		}
  </script>
	<form action="index.php?option=com_prayercenter" method="post" name="adminForm">
	<input type="hidden" name="requester" value="<?php echo $row->requester; ?>" />
	<input type="hidden" name="date" value="<?php echo $row->date;?>" />
	<input type="hidden" name="time" value="<?php echo $row->time;?>" />
	<input type="hidden" name="email" value="<?php echo $row->email; ?>" />
  <div class="col width-50" style="float:left;">
  	<fieldset class="adminform">
		<legend><?php echo JText::_( 'Details' ); ?>:</legend>
		<table class="admintable">
		<tr>
			<td width="100" align="right" class="key">
				<label for="title">
					<?php echo JText::_( 'USRLPRAYERREQUESTER' ); ?>
				</label>
			</td>
      <td><?php echo $row->requester;?></td>
      </tr>
    <?php if(empty($row->email)) $row->email = 'None';?>
		<tr>
			<td width="100" align="right" class="key">
				<label for="title">
					<?php echo JText::_( 'USRLPRAYERREQUESTEREMAIL' ); ?>
				</label>
			</td>
			<td><?php echo $row->email;?></td>
		</tr>
		<tr>
			<td width="100" align="right" class="key">
				<label for="title">
					<?php echo JText::_( 'USRLDATE' ); ?>
				</label>
			</td>
			<td><?php echo $row->date;?></td>
		</tr>
		<tr>
			<td width="100" align="right" class="key">
				<label for="title">
					<?php echo JText::_( 'USRLTIME' ); ?>
				</label>
			</td>
			<td><?php echo date("h:i:s A",strtotime($row->time));?></td>
		</tr>
		<tr>
			<td width="100" align="right" class="key">
				<label for="posttopic">
					<?php echo JText::_( 'PCREQTOPIC' ); ?>
				</label>
			</td>
			<td><select name="posttopic">
    <?php
    $topics = '<option value="">'.JText::_('PCSELECTTOPIC').'</option>';
    $selected = "";
    foreach($newtopicarray as $nt){
    if($row->topic == $nt['val']) $selected = ' selected';
    $topics .= '<option value="'.$nt['val'].'"'.$selected.'>'.$nt['text'].'</option>';
    $selected = "";
    }
    echo $topics;
    ?>
    </select></td></tr>
		<tr>
			<td width="100" align="right" class="key">
				<label for="posttitle">
					<?php echo JText::_( 'PCREQTITLE' ); ?>
				</label>
			</td>
			<td><input type="text" name="posttitle" value="<?php echo JText::_($row->title); ?>" />
    </td>
		</tr>
	</table>
	</fieldset>
  </div>
  <div class="col width-50" style="float:right;">
	<fieldset class="adminform">
		<legend><?php echo JText::_( 'USRLPRAYERREQUEST' ); ?></legend>
		<table class="admintable">
		<tr>
			<td colspan="2">
      <?php
      echo $editor->display( 'postrequest',  rtrim(stripslashes(JText::_($row->request))) , '420', '150', '70', '15', false, $eparams ) ;
      ?>
			</td>
		</tr>
		</table>
	</fieldset>
  </div>
  <div class="clr"></div>
	<input type="hidden" name="option" value="com_prayercenter" />
 	<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
  <input type="hidden" name="task" value="" />
  <input type="hidden" name="controller" value="prayercenter" />
	<?php echo JHTML::_( 'form.token' ); ?>
  </form>
	<?php
	echo '<br /><br />';
  $prayercenteradmin->PCfooter();
?>
