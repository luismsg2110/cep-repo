<?php
/**
* PrayerCenter Component for Joomla
* By Mike Leeper
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
  global $prayercenteradmin;
		$ordering = ($this->lists['order'] == 'ordering');
    $newtopicarray = $prayercenteradmin->PCgetTopics();
		JHTML::_('behavior.tooltip');
    JHtml::_('script','system/multiselect.js',false,true);
		  ?>
		<script language="javascript" type="text/javascript">
		function submitbutton(pressbutton) {
			var form = document.adminForm;
			if (pressbutton == 'cancel') {
				submitform( pressbutton );
				return;
				}
				submitform( pressbutton );
			}
    </script>
		<form action="index.php?option=com_prayercenter" method="post" name="adminForm">
	<fieldset id="filter-bar">
		<div class="filter-search fltlft">
			<label class="filter-search-lbl" for="filter_search"><?php echo JText::_('Filter').'&nbsp;'; ?></label>
			<input type="text" name="search" id="search" value="<?php echo $this->lists['search'];?>" class="inputbox" onChange="document.adminForm.submit();" />
				<button onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>
				<button onclick="document.getElementById('search').value='';document.getElementById('filter_order').value='id';document.getElementById('filter_order_Dir').value='DESC';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button>
		</div>
	</fieldset>
	<div class="clr"> </div>
  	<table class="adminlist">
	<thead><tr>
					<th width="5" class="title">
						<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->rows); ?>);" />
					</th>
					<th class="title">
						<?php echo JHTML::_('grid.sort',   'Requester', 'requester', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
					</th>
					<th class="title">
						<?php echo JHTML::_('grid.sort',   'Email Address', 'email', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
					</th>
					<th class="title">
						<?php echo JHTML::_('grid.sort',   'Topic', 'topic', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
					</th>
					<th class="title">
						<?php echo JHTML::_('grid.sort',   'Title', 'title', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
					</th>
					<th class="title">
						<?php echo JHTML::_('grid.sort',   'Request', 'request', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
					</th>
					<th class="title">
						<?php echo JHTML::_('grid.sort',   'Date / Time', 'datetime', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
					</th>
					<th class="title">
						<?php echo JHTML::_('grid.sort',   'Display', 'displaystate', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
					</th>
					<th width="5%" class="title" nowrap="nowrap">
						<?php echo JHTML::_('grid.sort',   'Published', 'publishstate', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
					</th>
					<th width="5%" class="title" nowrap="nowrap">
						<?php echo JHTML::_('grid.sort',   'Archived', 'archivestate', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
					</th>
  		</tr></thead>
		<?php
		$k = 0;
		$i = 0;
		for ($i=0, $n=count( $this->rows ); $i < $n; $i++) {
			$row = &$this->rows[$i];
            $request = $row->request;
            $request = strip_tags($request,"<i><strong><u><em><strike>");
            $request = stripslashes($request);
            if (strlen($request) > 50) $request = substr($request, 0 , 48) . " ...";
			?>
			<tbody><tr class="row<?php echo $k; ?>">
				<td align="center" width="5"><input type="checkbox" id="cb<?php echo $i;?>" name="cid[]" value="<?php echo $row->id; ?>" onClick="isChecked(this.checked);"></td>
        <td align="center"><?php echo $row->requester; ?></td>
        <td align="center"><?php echo $row->email; ?></td>
        <td align="center"><?php echo $newtopicarray[$row->topic+1]['text']; ?></td>
        <td align="center"><?php echo JText::_($row->title); ?></td>
				<td align="center"><span class="editlinktip hasTip" title="<?php echo JText::_( 'Edit Request' );?>::<?php echo $this->escape($request); ?>">
					<a href="#edit" onclick="return listItemTask('cb<?php echo $i; ?>','edit')">
						<?php echo $this->escape(JText::_($request)); ?></a></span></td>
        <td align="center"><?php echo $row->datetime; ?></td>
      <?php
      if( (real)$this->JVersion->RELEASE == 1.5 ) {
        $imagedir = 'images';
      } elseif( (real)$this->JVersion->RELEASE >= 1.6 ){
        $template	= JFactory::getApplication()->getTemplate();
        $imagedir = 'templates/'.$template.'/images/admin';
      }
			if ($row->displaystate) {
      	$displayimg = "<a href=\"javascript: void(0);\" ><img src=\"".$imagedir."/tick.png\" border=\"0\" onclick=\"return listItemTask('cb$i','hidereq');\" /></a>";
			} else {
	      $displayimg = "<a href=\"javascript: void(0);\" ><img src=\"".$imagedir."/publish_r.png\" border=\"0\" onclick=\"return listItemTask('cb$i','displayreq');\" /></a>";
			} 
			if ($row->publishstate) {
      	$publishimg = "<a href=\"javascript: void(0);\" ><img src=\"".$imagedir."/tick.png\" border=\"0\" onclick=\"return listItemTask('cb$i','unpublish');\" /></a>";
			} else {
	      $publishimg = "<a href=\"javascript: void(0);\" ><img src=\"".$imagedir."/publish_r.png\" border=\"0\" onclick=\"return listItemTask('cb$i','publish');\" /></a>";
			} 
      if ($row->archivestate) {
      	$archiveimg = "<a href=\"javascript: void(0);\" ><img src=\"".$imagedir."/tick.png\" border=\"0\" onclick=\"return listItemTask('cb$i','unarchive');\" /></a>";
			} else {
      	$archiveimg = "<a href=\"javascript: void(0);\" ><img src=\"".$imagedir."/publish_r.png\" border=\"0\" onclick=\"return listItemTask('cb$i','archive');\" /></a>";
			}
    ?>           		
    <td align="center"><?php echo $displayimg; ?></td>
    <td align="center"><?php echo $publishimg; ?></td>
    <td align="center"><?php echo $archiveimg; ?></td>
		</tr></tbody>
  <?php
  }
  ?>
		<tfoot><tr>
			<th align="center" colspan="10">
				<?php echo $this->pageNav->getListFooter(); ?></th>
		</tr>
    </tfoot>
		</table>
			<input type="hidden" name="option" value="com_prayercenter" />
			<input type="hidden" name="task" value="manage_req" />
			<input type="hidden" name="controller" value="prayercenter" />
			<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
	</form><br />
	<?php
  $prayercenteradmin->PCfooter();
?>