<?php 
defined('_JEXEC') or die('Restricted access');
    global $prayercenteradmin;
  	JRequest::setVar( 'hidemainmenu', 1 );
    $edit = $this->edit;
  	$text = !$edit ? JText::_( 'New' ) : JText::_( 'Edit' );
    if( (real)$this->JVersion->RELEASE == 1.5 ) {
      global $mainframe;
  		$template	= $mainframe->getTemplate();
      $imagedir = 'templates/'.$template.'/images/header';
      ?>
      <style type="text/css">
      .icon-48-component 	{ background-image: url($imagedir.'/icon-48-component.png'); }
      </style>
      <?php
    	JToolBarHelper::title(   JText::_( 'Edit Language' ).': <small><small>[ ' . $text.' ]</small></small>' , 'component' );
    } elseif( (real)$this->JVersion->RELEASE >= 1.6 ){
  		$app = JFactory::getApplication();
      $template	= JFactory::getApplication()->getTemplate();
      $imagedir = 'templates/'.$template.'/images/header';
  		$html  = "<div class=\"pagetitle\" style=\"background-image: url($imagedir'/icon-48-component.png');\">\n";
  		$html .= "<h2>Manage Links</h2>";
  		$html .= "</div>";
  		$app->set('JComponentTitle', $html);
    }
		JToolBarHelper::save('savelink', 'Save' );
  	if (!$edit)  {
  		JToolBarHelper::cancel('canceleditlink');
  	} else {
  		JToolBarHelper::cancel( 'canceleditlink', 'Close' );
  	}
JHTML::_('behavior.tooltip'); 
?>
<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		var form = document.adminForm;
		if (pressbutton == 'canceleditlink') {
			submitform( pressbutton );
			return;
		}
		// do field validation
		if (form.title.value == ""){
			alert( "<?php echo JText::_( 'Link item must have a name', true ); ?>" );
		} else if (form.url.value == ""){
			alert( "<?php echo JText::_( 'You must have a url.', true ); ?>" );
		} else {
			submitform( pressbutton );
		}
	}
</script>
<style type="text/css">
	table.paramlist td.paramlist_key {
		width: 92px;
		text-align: left;
		height: 30px;
	}
</style>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<div class="col width-50">
	<fieldset class="adminform">
		<legend><?php echo JText::_( 'Details' ); ?></legend>
		<table class="admintable">
		<tr>
			<td width="100" align="right" class="key">
				<label for="title">
					<?php echo JText::_( 'Name' ); ?>:
				</label>
			</td>
			<td>
				<input class="text_area" type="text" name="title" id="title" size="32" maxlength="250" value="<?php echo $this->rows->name;?>" />
			</td>
		</tr>
		<tr>
			<td width="100" align="right" class="key">
				<label for="alias">
					<?php echo JText::_( 'Alias' ); ?>:
				</label>
			</td>
			<td>
				<input class="text_area" type="text" name="alias" id="alias" size="32" maxlength="250" value="<?php echo $this->rows->alias;?>" />
			</td>
		</tr>
		<tr>
			<td valign="top" align="right" class="key">
				<?php echo JText::_( 'Published' ); ?>:
			</td>
			<td>
				<?php echo $this->lists['published']; ?>
			</td>
		</tr>
		<tr>
			<td valign="top" align="right" class="key">
				<label for="url">
					<?php echo JText::_( 'URL' ); ?>:
				</label>
			</td>
			<td>
				<input class="text_area" type="text" name="url" id="url" value="<?php echo $this->rows->url; ?>" size="32" maxlength="250" />
			</td>
		</tr>
		<tr>
			<td valign="top" align="right" class="key">
				<label for="ordering">
					<?php echo JText::_( 'Ordering' ); ?>:
				</label>
			</td>
			<td>
				<?php echo $this->lists['ordering']; ?>
			</td>
		</tr>
	</table>
	</fieldset>
</div>
<div class="col width-50">
	<fieldset class="adminform">
		<legend><?php echo JText::_( 'Description' ); ?></legend>
		<table class="admintable">
		<tr>
			<td>
				<textarea class="text_area" cols="58" rows="8" name="description" id="description"><?php echo $this->rows->descrip; ?></textarea>
			</td>
		</tr>
		</table>
	</fieldset>
</div>
<div class="clr"></div>
	<input type="hidden" name="option" value="com_prayercenter" />
	<input type="hidden" name="controller" value="prayercenter" />
	<input type="hidden" name="id" value="<?php echo $this->rows->id; ?>" />
	<input type="hidden" name="task" value="" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form><br />
	<?php $prayercenteradmin->PCfooter(); ?>
