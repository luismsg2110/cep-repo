<?php
/**
* PrayerCenter Component for Joomla
* By Mike Leeper
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
    global $prayercenteradmin, $mainframe;
    $lang =& Jfactory::getLanguage();
    $lang->load( 'com_prayercenter', JPATH_SITE); 
  	$version = new JVersion();
    $livesite = JURI::base();
    $lang_path = JPATH_ROOT.DS.'language'.DS;
    $langfolderarray = JFolder::folders($lang_path, '.', false, false, array('pdf_fonts'));
    $langarray = array();
    foreach($langfolderarray as $langfolder){
    $langfilesarray = JFolder::files($lang_path.$langfolder,'com_prayercenter.ini',false,true);
    $langarray = array_merge_recursive($langarray,$langfilesarray);
     }
    $lfsubmit = "";
    $lfsubmit .= 'PrayerCenter Language File Submission%0D%0A%0D%0A';
    $lfsubmit .= "PrayerCenter Version:%20".JText::_('PCVERSION')."%0D%0A%0D%0A";
    $lfsubmit .= 'Joomla! Version:%20'.$version->getLongVersion().'%0D%0A%0D%0A%0D%0A%0D%0A';
    $lfsubmit .= 'Do you wish to have your name and/or email address listed as the author of this language file?%0D%0A%0D%0A';
    $lfsubmit .= 'Would you agree to be contacted when language file changes are made in future version of PrayerCenter?%0D%0A%0D%0A%0D%0A%0D%0A';
    $lfsubmit .= 'Please attach the language file to this message. It will then be made available for others to download.%0D%0A%0D%0A';

    $client	=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));
    $params = JComponentHelper::getParams('com_languages');
    $defaultlang = $params->get($client->name, 'en-GB');
    if( (real)$this->JVersion->RELEASE == 1.5 ) {
  		$template	= $mainframe->getTemplate();
    } elseif( (real)$this->JVersion->RELEASE >= 1.6 ){
      $template	= JFactory::getApplication()->getTemplate();
    }
    $imagedir = 'templates/'.$template.'/images/menu/';
$this->lists = "";
    ?>
    <script language="javascript" type="text/javascript">
		function submitbutton(pressbutton) {
			var form = document.adminForm;
			if (pressbutton == 'cancel') {
				submitform( pressbutton );
				return;
				}
				submitform( pressbutton );
			}
    </script>
  	<form action="index.php?option=com_prayercenter" method="post" name="adminForm">
  	<fieldset style="float:none;"><legend><?php echo 'Installed Language Files' ?></legend>
    <table class="adminlist">
		<thead><tr>
			<th class="title" width="5">&nbsp;</th>
			<th class="title" width="200"><?php echo JText::_( 'File');?></th>
			<th class="title" width="100"><?php echo JText::_( 'Folder');?></th>
			<th class="title" width="25%"><?php echo JText::_( 'Joomla Language Extension' );?></th>
			<th class="title" width="10%"><?php echo JText::_( 'Version' ); ?></th>
			<th class="title" width="10%"><?php echo JText::_( 'Date' ); ?></th>
			<th class="title" width="20%"><?php echo JText::_( 'Author' ); ?></th>
			<th class="title" width="25%"><?php echo JText::_( 'Author Email' ); ?></th>
		</tr></thead>
    <?php
    for($j=0;$j<count($langarray);$j++)
    {
      $d = "#[\\\/]#";
      $lf = preg_split($d,$langarray[$j],-1,PREG_SPLIT_NO_EMPTY);
      $lffile = count($lf)-1;
      $lffolder = $lffile-1;
			$ldata = JApplicationHelper::parseXMLLangMetaFile($lang_path.$lf[$lffolder].DS.$lf[$lffolder].'.xml');
			$row = new StdClass();
			$row->id = $j;
			$row->language = substr($lf[$lffolder].'.xml',0,-4);
			if (!is_array($ldata)) {
				continue;
			}
			foreach($ldata as $key => $value) {
				$row->$key = $value;
			}
    $content = file($lang_path.$lf[$lffolder].DS.$lf[$lffile]);
		if (strpos($content[0],'.ini')) {
			$line = preg_replace('/^.*[.]ini[ ]+/','',$content[0]);
			list( $file['version'], $file['date'], $file['time'], $file['owner'], $file['complete'] ) = explode( ' ', $line . '   ', 6 );
			$file['headertype'] = 1;
		}
		$file['author'] 	= preg_replace('/^.*author[ ]+/i', '', trim($content[1],'# ') );
		$file['author email'] 	= preg_replace('/^.*author email[ ]+/i', '', trim($content[2],'# ') );
    ?>
		<tbody><tr class="row0">
       <td align="center" width="5px"><input type="checkbox" id="cb<?php echo $j;?>" name="fid[]" value="<?php echo $lf[$lffolder].DS.$lf[$lffile]; ?>" onClick="isChecked(this.checked);"></td>
				<td align="center" nowrap><span class="editlangtip hasTip" title="<?php echo JText::_( 'Edit Language File' );?>::<?php echo $lf[$lffile]; ?>">
					<a href="#edit" onclick="return listItemTask('cb<?php echo $j; ?>','editlang')">
						<?php echo $lf[$lffile]; ?></a></span></td>
       <td width="10px" align="center" nowrap><?php echo $lf[$lffolder]; ?></td>
			<td align="center">
 			<?php
			if ($defaultlang == $lf[$lffolder]) {	 ?>
							<img src="<?php echo $imagedir;?>/icon-16-default.png" alt="<?php echo JText::_( 'Default Joomla Language Extension' ); ?>" style="vertical-align:middle;float:none;" />&nbsp;<?php echo $row->name;?>
							<?php
						} else {
							echo $row->name;
						}
					?>
      </td>
			<td align="center"><?php echo (!empty($file['version'])) ? $file['version'] : ""; ?></td>
			<td align="center"><?php echo (!empty($file['date'])) ? $file['date'] : ""; ?></td>
			<td align="center"><?php echo (!empty($file['author'])) ? $file['author'] : ""; ?></td>
			<td align="center"><?php echo (!empty($file['author email'])) ? $file['author email'] : ""; ?></td>
    </tr>
    <?php
    }
    ?>    
    </tbody><tfoot><tr><td colspan="8"></td></tr></tfoot></table><table width="100%">
    <tr>
    <td colspan="8" style="text-align:center;"><span style="padding:20px;"><img src="<?php echo $imagedir;?>/icon-16-install.png" style="vertical-align:middle;float:none;" /><a href="http://www.joomlacode.org/gf/project/prayercenter/frs/" target="_blank">Download available language files</a></span>
    <span style="padding:20px;"><img src="<?php echo $imagedir;?>/icon-16-language.png" style="vertical-align:middle;float:none;" /><a href="index.php?option=com_languages" target="_self">Joomla Language Manager</a></span>
    <span style="padding:20px;"><img src="<?php echo $imagedir;?>/icon-16-messages.png" style="vertical-align:middle;float:none;" /><a href="mailto:web@mlwebtechnologies.com?subject=PrayerCenter%20Language%20File%20Submission&body=<?php echo $lfsubmit; ?>">Submit a language file</a></span></td>
    </tr></table>
		<input type="hidden" name="option" value="com_prayercenter" />
		<input type="hidden" name="boxchecked" value="0" />
 		<input type="hidden" name="task" value="manage_lang" />
 		<input type="hidden" name="controller" value="prayercenter" />
		</fieldset></form><br />
	<?php
  $prayercenteradmin->PCfooter();
?>
