<?php
/**
* PrayerCenter Component for Joomla
* By Mike Leeper
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
  global $db, $mainframe, $prayercenteradmin;
  jimport('joomla.html.pane');
  jimport('joomla.filesystem.file');
  $livesite = JURI::base();
  $db	=& JFactory::getDBO();
	$version = new JVersion();
  $supportinfo = "";
  $supportinfo .= 'System Information%0D%0A%0D%0A';
  $supportinfo .= 'Database Version:%20'.$db->getVersion().'%0D%0A';
  $supportinfo .= 'PHP Version:%20'.phpversion().'%0D%0A';
  $supportinfo .= 'Web Server:%20'.$prayercenteradmin->pc_get_server_software().'%0D%0A';
  $supportinfo .= 'Joomla! Version:%20'.$version->getLongVersion().'%0D%0A%0D%0A';
  $supportinfo .= 'Relevant PHP Settings%0D%0A';
  $supportinfo .= 'Magic Quotes GPC:%20'.$prayercenteradmin->pc_get_php_setting('magic_quotes_gpc').'%0D%0A';
  $supportinfo .= 'Short Open Tags:%20'.$prayercenteradmin->pc_get_php_setting('short_open_tag').'%0D%0A';
  $supportinfo .= 'Disabled Functions:%20'.(($df=ini_get('disable_functions'))?$df:'none').'%0D%0A';
  if( (real)$this->JVersion->RELEASE == 1.5 ) {
		$template	= $mainframe->getTemplate();
  } elseif( (real)$this->JVersion->RELEASE >= 1.6 ){
    $template	= JFactory::getApplication()->getTemplate();
  }
  $imagedir = 'templates/'.$template.'/images/menu/';
  ?>
   <div class="adminform">
   <?php 
    if( (real)$this->JVersion->RELEASE >= 1.6 ){
   ?>
   <div class="cpanel-left"><div class="adminlist" style="font-size:18px;padding:12px 0px 8px 0px;color:#999;"><?php echo JText::_('Support References');?></div>
	 <div class="cpanel">
		<?php
		$prayercenteradmin->PCquickiconButton( "http://www.joomlacode.org/gf/project/prayercenter/tracker/", 'icon-48-help-this.png', JText::_( 'Support BugTracker' ) );
		$prayercenteradmin->PCquickiconButton( "http://www.mlwebtechnologies.com/index.php?option=com_kunena", 'icon-48-help-forum.png', JText::_( 'Support Forum' ) );
		$prayercenteradmin->PCquickiconButton( "mailto:web@mlwebtechnologies.com?subject=PrayerCenter%20Support%20Inquiry&body=".$supportinfo, 'icon-48-writemess.png', JText::_( 'Support Email' ) );
		$prayercenteradmin->PCquickiconButton( "http://www.mlwebtechnologies.com", 'icon-48-links.png', JText::_( 'Support Website' ) );
		?>
	</div>
       <?php 
      } else {
       ?>
        <table border="1" width="100%" class="admintable">
		      <tr>
            <th colspan="2" style="text-align:left;" nowrap><font size="2"><b><?php echo JText::_('Support References');?></font></th>
		      </tr>
		      <tr>
  		      <th nowrap><b><?php echo JText::_('Support BugTracker')?></b></th>
  		      <td>
            <?php
         		$trackerlink = "http://www.joomlacode.org/gf/project/prayercenter/tracker/";
         		$image = JHTML::_('image.site',  'icon-16-help.png', $imagedir, NULL, NULL, NULL,'style=vertical-align:bottom;');
          	$tattribs['rel']     = 'nofollow';
         		echo '&nbsp;&nbsp;'.JHTML::_('link', JRoute::_($trackerlink), $image.'&nbsp;'.JText::_('PrayerCenter BugTracker'),$tattribs);
            ?>
            </td>
         </tr>
         <tr>
            <th nowrap><b><?php echo JText::_('Support Forum');?></b></th>
            <td>
            <?php
         		$forumlink = "http://www.mlwebtechnologies.com/index.php?option=com_kunena";
         		$image = JHTML::_('image.site',  'icon-16-user.png', $imagedir, NULL, NULL, NULL,'style=vertical-align:bottom;');
          	$fattribs['rel']     = 'nofollow';
         		echo '&nbsp;&nbsp;'.JHTML::_('link', JRoute::_($forumlink), $image.'&nbsp;'.JText::_('MLWebTechnologies Support Forum'),$fattribs);
            ?>
            </td>
         </tr>
         <tr>
            <th><b><?php echo JText::_('Support Email');?></b></th>
            <td>
            <?php
         		$smaillink = "mailto:web@mlwebtechnologies.com?subject=PrayerCenter%20Support%20Inquiry&body=".$supportinfo;
         		$image = JHTML::_('image.site',  'icon-16-messages.png', $imagedir, NULL, NULL, NULL,'style=vertical-align:bottom;');
          	$eattribs['rel']     = 'nofollow';
         		echo '&nbsp;&nbsp;'.JHTML::_('link', JRoute::_($smaillink), $image.'&nbsp;'.JText::_('MLWebTechnologies Support Email'),$eattribs);
            ?>
            </td>
         </tr>
         <tr>
            <th><b><?php echo JText::_('Website');?></b></th>
            <td>
            <?php
         		$mlwlink = "http://www.mlwebtechnologies.com";
         		$image = JHTML::_('image.site',  'icon-16-frontpage.png', $imagedir, NULL, NULL, NULL,'style=vertical-align:bottom;');
          	$sattribs['rel']     = 'nofollow';
         		echo '&nbsp;&nbsp;'.JHTML::_('link', JRoute::_($mlwlink), $image.'&nbsp;'.JText::_('http://www.mlwebtechnologies.com'),$sattribs);
            ?>
            </td>
         </tr>
       </table>
       <?php 
      }
       ?>
       </div>
    <div class="cpanel-right">
  <?php
	$pane = &JPane::getInstance('sliders', array('allowAllClose' => true));
	echo $pane->startPane("support-pane");
	echo $pane->startPanel( 'System Information', 'panel1' );
  ?>
    <table border="1" width="100%" class="admintable">
		 <tr>
			<th><?php echo JText::_('PHP built On');?></th>
			<td><?php echo php_uname(); ?></td>
		 </tr>
		 <tr>
			<th><?php echo JText::_('Database Version');?></th>
			<td><?php echo $db->getVersion(); ?></td>
		 </tr>
		 <tr>
			<th><?php echo JText::_('PHP Version');?></th>
			<td><?php echo phpversion(); ?></td>
		 </tr>
		 <tr>
			<th><?php echo JText::_('Web Server');?></th>
			<td><?php echo $prayercenteradmin->pc_get_server_software(); ?></td>
		 </tr>
		 <tr>
			<th><?php echo JText::_('Joomla! Version');?></th>
			<td><?php echo $version->getLongVersion(); ?></td>
		 </tr>
		 <tr>
			<td class="key" valign="top"><strong><?php echo JText::_('User Agent');?></strong></td>
			<td><?php echo phpversion() <= '4.2.1' ? getenv( 'HTTP_USER_AGENT' ) : $_SERVER['HTTP_USER_AGENT'];?></td>
		 </tr>
		 <tr>
			<td colspan="2" style="height: 10px;"></td>
		 </tr>
		 <tr>
			<td valign="top" class="key"><strong><?php echo JText::_('Relevant PHP Settings');?></strong></td>
			<td><table cellspacing="1" cellpadding="1" border="0" width="100%">
        <tr>
					<td class="key"><?php echo JText::_('Magic Quotes GPC');?></td>
					<td style="font-weight: bold;"><?php echo $prayercenteradmin->pc_get_php_setting('magic_quotes_gpc', true, false); ?></td>
				</tr>
				<tr>
					<td class="key"><?php echo JText::_('Short Open Tags');?></td>
					<td style="font-weight: bold;"><?php echo $prayercenteradmin->pc_get_php_setting('short_open_tag', true, false); ?></td>
				</tr>
				<tr>
					<td class="key"><?php echo JText::_('Disabled Functions');?></td>
					<td style="font-weight: bold;" colspan="2"><?php echo (($df=ini_get('disable_functions'))?$df:'None'); ?></td>
				</tr>
			</table>
		</td>
	</tr></table>
  <?php
	echo $pane->endPanel();
	echo $pane->startPanel( JText::_('Optional Modules - Status'), 'panel2' );
  ?>
    <table border="0" width="100%" class="adminlist">
		<thead>
			<tr>
				<th class="title" width="40%" align="center"><?php echo JText::_( 'Name' ); ?></th>
				<th class="title" width="30%"><?php echo JText::_( 'Date' ); ?></th>
				<th class="title" width="30%" align="center"><?php echo JText::_( 'Version' ); ?></th>
			</tr>
		</thead>
    <tbody>
    <?php 
    $modArray = array(1 => array ('module' => 'mod_pc_submit_request', 'name' => 'PrayerCenter Submit Request'),
                      2 => array ('module' => 'mod_pc_subscribe', 'name' => 'PrayerCenter Subscribe'),
                      3 => array ('module' => 'mod_pc_menu', 'name' => 'PrayerCenter Menu'),
                      4 => array ('module' => 'mod_pc_latest', 'name' => 'PrayerCenter Latest'));
    foreach($modArray as $module){
      $moduledir = JPATH_ROOT.DS.'modules';
      $xmlfile = $moduledir.DS.$module['module'].DS.$module['module'].'.xml';
      $xmldata = $prayercenteradmin->PCparseXml($xmlfile);
      if($xmldata){
        echo '<tr><td class="key" width="40%">'.$module['name'].'</td>';
        echo '<td class="key" width="30%"><font color="green"><center>'.$xmldata['creationdate'].'</font></center></td>';
        echo '<td class="key" width="30%"><font color="green"><center>'.$xmldata['version'].'</font></center></td>';
      } else {
        echo '<tr><td class="key" width="40%">'.$module['name'].'</td>';
        echo '<td colspan="2"><font color="red"><center>'.JText :: _('Not Installed').'</center></font></td>';
      }
    echo '</tr>';
    }
    ?>
    </tbody>
   </table>
  <?php
	echo $pane->endPanel();
	echo $pane->startPanel( 'PrayerCenter Configuration File', 'panel3' );
  ?>
    <table border="0" width="100%" class="admintable">
			<tr>
				<td><center>
        <?php
        $cstring = "";
        $JVersion = new JVersion();
        $pcParams = JComponentHelper::getParams('com_prayercenter');
        if( (real)$JVersion->RELEASE == 1.5 ) {
          $pcConfig = $pcParams->toArray();
        } elseif( (real)$JVersion->RELEASE >= 1.6 ) {
          $pcParamsArray = $pcParams->toArray();
          foreach($pcParamsArray['params'] as $name => $value){
            $pcConfig[(string)$name] = (string)$value;
          }
        }
    		foreach ($pcConfig as $name => $value) {
          $cstring .= $name.' = '.$value.'\n';
        }
  			?>
  			<textarea name="pcconfigstr" id="config_content" cols="72" rows="15" readonly><?php echo str_replace('\n',"\n",$cstring);?></textarea>
				</center></td>
		 </tr>
    <tr><td>
    <script language="javascript">
    function printconfig()
    { 
      var disp_setting = "toolbar=yes,location=no,directories=yes,menubar=yes,"; 
          disp_setting += "scrollbars=yes,width=650, height=300, left=100, top=25"; 
      var content_vlue = document.getElementById("config_content").value.replace(/(\r\n|\n\r|\r|\n)/g,"<br>"); 
      var docprint=window.open("","",disp_setting); 
       docprint.document.open(); 
       docprint.document.write('<html><head><title>PrayerCenter Configuration File</title>'); 
       docprint.document.write('</head><body onLoad="self.print()"><center><table border=1><tr><td>');          
       docprint.document.write(content_vlue);          
       docprint.document.write('</td></tr></table><br /><a href="javascript:self.close();">Close Window</a>');          
       docprint.document.write('</center></body></html>'); 
       docprint.document.close(); 
       docprint.focus(); 
    }
    function savePCConfig2file( fcontent ) {
     var w = window.frames.w;
     if( !w ) {
      w = document.createElement( 'iframe' );
      w.id = 'w';
      w.style.display = 'none';
      document.body.insertBefore( w, null );
      w = window.frames.w;
      if( !w ) {
       w = window.open( '', '_temp', 'width=100,height=100' );
       if( !w ) {
        window.alert( 'Sorry, the file could not be created.' ); return false;
       }
      }
     }
     var d = w.document, ext = 'utf-8', name = 'PCConfig.txt';
     d.open( 'text/plain', 'replace' );
     d.charset = 'utf-8';
     d.write( fcontent );
     d.close();
     if( d.execCommand( 'SaveAs', null, name ) ){
      window.alert( 'The file has been saved.' );
     }
     w.close();
     return false;
    }
    </script>
    <?php
 		$printconfiglink = "#";
 		$prtimage = JHTML::_('image.site',  'icon-16-print.png', $imagedir, NULL, NULL, htmlentities(JText::_( 'Print Config' ),ENT_COMPAT,'UTF-8'),'style=vertical-align:top;');
  	$attribs['title']	= htmlentities(JText::_( 'Print Configuration' ),ENT_COMPAT,'UTF-8');
  	$attribs['rel']     = 'nofollow';
    $attribs['onclick'] = "javascript:printconfig();";
 		echo '&nbsp;&nbsp'.JHTML::_('link', JRoute::_($printconfiglink), $prtimage.'&nbsp;<small>'.htmlentities(JText::_('Print Config'),ENT_COMPAT,'UTF-8').'</small>',$attribs);
    $savelink = '#';
    $saveimage = JHTML::_('image.site',  'icon-16-install.png', $imagedir, NULL, NULL, NULL,'style=vertical-align:bottom;');
  	$saveattribs['title']	= htmlentities(JText::_( 'Save Configuration as File' ),ENT_COMPAT,'UTF-8');
    $saveattribs['rel']     = 'nofollow';
    ?><script language="javascript">var cstring = '<?php echo $cstring;?>';</script><?php
    $saveattribs['onclick'] = "javascript:savePCConfig2file(cstring);";
    echo '&nbsp;&nbsp;|&nbsp;&nbsp;'.JHTML::_('link', JRoute::_($savelink), $saveimage.'&nbsp;<small>'.JText::_('Save as file').'</small>',$saveattribs);
    ?>
    </td></tr>
   </table>
  <?php
	echo $pane->endPanel();
	echo $pane->endPane();
  echo '</div></form><div class="clr"></div><br /><br /><br /><div>';
  $prayercenteradmin->PCfooter();
?></div>