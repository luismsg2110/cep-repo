<?php
/**
* PrayerCenter Component for Joomla
* By Mike Leeper
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
    global $prayercenteradmin;
		$ordering = ($this->lists['order'] == 'ordering');
		JHTML::_('behavior.tooltip');
    JHtml::_('script','system/multiselect.js', false, true);
		  ?>
		<script language="javascript" type="text/javascript">
		function submitbutton(pressbutton) {
			var form = document.adminForm;
			if (pressbutton == 'cancel') {
				submitform( pressbutton );
				return;
				}
				submitform( pressbutton );
			}
    </script>
		<form action="index.php?option=com_prayercenter" method="post" name="adminForm">
	<fieldset id="filter-bar">
		<div class="filter-search fltlft">
			<label class="filter-search-lbl" for="filter_search"><?php echo JText::_('Filter').'&nbsp;'; ?></label>
			<input type="text" name="search" id="search" value="<?php echo $this->lists['search'];?>" class="inputbox" onChange="document.adminForm.submit();" />
				<button onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>
				<button onclick="document.getElementById('search').value='';document.getElementById('filter_order').value='id';document.getElementById('filter_order_Dir').value='DESC';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button>
		</div>
	</fieldset>
	<div class="clr"> </div>
  <table class="adminlist">
  	<thead>
    		<tr>
					<th width="5" class="title">
						<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->rows); ?>);" />
					</th>
					<th class="title">
						<?php echo JHTML::_('grid.sort',   'Name', 'name', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
					</th>
					<th class="title">
						<?php echo JHTML::_('grid.sort',   'Feed', 'feed', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
					</th>
					<th width="5%" class="title" nowrap="nowrap">
						<?php echo JHTML::_('grid.sort',   'Published', 'publishstate', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
					</th>
					<th width="12%" class="title" nowrap="nowrap">
						<?php echo JHTML::_('grid.sort',   'Ordering', 'ordering', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
    				<?php echo JHTML::_('grid.order',  $this->rows, 'filesave.png', "saveorderdevotions" ); ?>
					</th>
    			<th width="1%" nowrap="nowrap">
    				<?php echo JHTML::_('grid.sort',  'ID', 'id', $this->lists['order_Dir'], $this->lists['order'] ); ?>
    			</th>
  		</tr></thead>
		<?php
		$k = 0;
		$i = 0;
		for ($i=0, $n=count( $this->rows ); $i < $n; $i++) {
			$row = &$this->rows[$i];
			?>
			<tbody><tr class="row<?php echo $k; ?>">
				<td align="center" width="5"><input type="checkbox" id="cb<?php echo $i;?>" name="cid[]" value="<?php echo $row->id; ?>" onClick="isChecked(this.checked);"></td>
				<td align="center"><span class="editlinktip hasTip" title="<?php echo JText::_( 'Edit Devotional' );?>::<?php echo $this->escape($row->name); ?>">
					<a href="#edit" onclick="return listItemTask('cb<?php echo $i; ?>','editdevotion')">
						<?php echo $this->escape($row->name); ?></a></span></td>
        <td align="center"><?php echo $row->feed; ?></td>
      <?php
      if( (real)$this->JVersion->RELEASE == 1.5 ) {
        $imagedir = 'images';
      } elseif( (real)$this->JVersion->RELEASE >= 1.6 ){
        $template	= JFactory::getApplication()->getTemplate();
        $imagedir = 'templates/'.$template.'/images/admin';
      }
			if ($row->published == "1") {
      	$publishimg = "<a href=\"javascript: void(0);\" ><img src=\"".$imagedir."/tick.png\" border=\"0\" onclick=\"return listItemTask('cb$i','unpublishdevotion');\" /></a>";
			} else if ($row->published != "1") {
	      $publishimg = "<a href=\"javascript: void(0);\" ><img src=\"".$imagedir."/publish_x.png\" border=\"0\" onclick=\"return listItemTask('cb$i','publishdevotion');\" /></a>";
			} 
    ?>           		
    <td align="center"><?php echo $publishimg; ?></td>
		<td class="order">
			<?php if ($this->lists['order_Dir'] == 'asc') : ?>
  			<span><?php echo $this->pageNav->orderUpIcon( $i, true,'orderupdev', 'JLIB_HTML_MOVE_UP', $ordering ); ?></span>
  			<span><?php echo $this->pageNav->orderDownIcon( $i, $n, true, 'orderdowndev', 'JLIB_HTML_MOVE_DOWN', $ordering ); ?></span>
			<?php elseif ($this->lists['order_Dir'] == 'desc') : ?>
  			<span><?php echo $this->pageNav->orderUpIcon( $i, true,'orderdowndev', 'JLIB_HTML_MOVE_UP', $ordering ); ?></span>
  			<span><?php echo $this->pageNav->orderDownIcon( $i, $n, true, 'orderupdev', 'JLIB_HTML_MOVE_DOWN', $ordering ); ?></span>
			<?php endif; ?>
			<?php $disabled = $ordering ?  '' : 'disabled="disabled"'; ?>
			<input type="text" name="order[]" size="5" value="<?php echo $row->ordering;?>" <?php echo $disabled; ?> class="text-area-order" style="text-align: center" />
		</td>
		<td align="center">
			<?php echo $row->id; ?>
		</td>
		</tr></tbody>
  <?php
  }
  ?>
		<tfoot><tr>
			<th align="center" colspan="10">
				<?php echo $this->pageNav->getListFooter(); ?></th>
		</tr>
    </tfoot>
		</table>
			<input type="hidden" name="option" value="com_prayercenter" />
			<input type="hidden" name="task" value="manage_dev" />
			<input type="hidden" name="controller" value="prayercenter" />
			<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
	<?php echo JHTML::_( 'form.token' ); ?>
	</form><br />
	<?php
  $prayercenteradmin->PCfooter();
?>