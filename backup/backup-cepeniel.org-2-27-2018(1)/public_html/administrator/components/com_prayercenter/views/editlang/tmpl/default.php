<?php
/**
* PrayerCenter Component for Joomla
* By Mike Leeper
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
		global $prayercenteradmin;
  	JRequest::setVar( 'hidemainmenu', 1 );
    $edit = $this->edit;
  	$text = !$edit ? JText::_( 'New' ) : JText::_( 'Edit' );
  	JToolBarHelper::title(   JText::_( 'Edit Language' ).': <small><small>[ ' . $text.' ]</small></small>' , 'langmanager' );
  		JToolBarHelper::save('savelang', 'Save' );
  	if (!$edit)  {
  		JToolBarHelper::cancel('canceleditlang');
  	} else {
  		JToolBarHelper::cancel( 'canceleditlang', 'Close' );
  	}
    if(empty($edit)) $edit = 0;
		if(!$this->file){
      $lffile = 'en-GB.com_prayercenter.ini';
      $lffolder = 'en-GB';
     } else {
    $d = "#[\\\/]#";
    $lf = preg_split($d,$this->file,-1,PREG_SPLIT_NO_EMPTY);
    $lffile = $lf[count($lf)-1];
    $lffolder = $lf[count($lf)-2];
    }
    ?>
		<script language="javascript" type="text/javascript">
  	function submitbutton(pressbutton) {
      var edit = <?php echo $edit;?>;
  		var form = document.adminForm;
  		if (pressbutton == 'canceleditlang') {
  			submitform( pressbutton );
  			return;
  		}
      if (edit > 0) {
        submitform(pressbutton);
        return;
        }
  		// do field validation
      var q = new RegExp('[a-z]+\-[A-Z]');
  		if (form.config_langfolder.value == "" || q.test(form.config_langfolder.value) != true || form.config_langfolder.value == 'en-GB'){
  			alert( "Please enter a valid language code. \n(Example: en-GB)" );
  		} else {
  			submitform( pressbutton );
  		}
  	}
    </script>
  	<form action="index.php?option=com_prayercenter" method="post" name="adminForm">
    <div class="col width-50">
  	<fieldset class="adminform">
		<legend><?php echo JText::_( 'Details' ); ?></legend>
		<table class="admintable">
    <?php
    if($this->file){
    ?>
		<tr>
			<td width="100" align="right" class="key">
				<label for="title">
					<?php echo JText::_( 'File Name' ); ?>:
				</label>
			</td>
      <td>
			<input class="text_area" type="text" name="config_langfile" id="config_langfile" size="32" maxlength="250" value="<?php echo $lffile;?>" readonly="readonly" />
      </td>
      </tr>
    <?php  
    }
    ?>
		<tr>
			<td width="100" align="right" class="key">
				<label for="title">
					<?php 
          if(!$this->file){
          echo JText::_( 'Language Code:<br />(Example en-GB)<br />(See Help for details)' );
          } else {
          echo JText::_( 'Language Code:' );
          }
          ?>
				</label>
			</td>
			<td>
      <?php
      if(!$this->file){
      ?>
			<input class="text_area" type="text" name="config_langfolder" id="config_langfolder" size="6" maxlength="250" value="" />
      <?php
      } else {
      ?>
			<input class="text_area" type="text" name="config_langfolder" id="config_langfolder" size="6" maxlength="250" value="<?php echo $lffolder;?>" readonly="readonly" />
			<?php
      }
      ?>
      </td>
		</tr>
	</table>
	</fieldset>
  </div>
  <div>
		<table style="text-align:left;width:100%;padding-left:6px;">
		<tr>
			<td><br />
      <?php
      $filename = JPATH_ROOT.DS.'language'.DS.$lffolder.DS.$lffile;
  		$initstring = file_get_contents($filename);
      echo $this->editor->display( 'config_lang',  $initstring , '90%', '250', '70', '15', false ) ;
      ?>
			<br /><br /></td>
		</tr>
		</table>
  </div>
  <div class="clr"></div>
	<input type="hidden" name="option" value="com_prayercenter" />
  <input type="hidden" name="task" value="" />
  <input type="hidden" name="controller" value="prayercenter" />
	<?php echo JHTML::_( 'form.token' ); ?>
	 </form>
	<?php
	echo '<br /><br />';
  $prayercenteradmin->PCfooter();
?>
