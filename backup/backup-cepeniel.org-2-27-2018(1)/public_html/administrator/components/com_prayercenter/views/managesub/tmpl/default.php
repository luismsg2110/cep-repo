<?php
/**
* PrayerCenter Component for Joomla
* By Mike Leeper
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
    global $prayercenteradmin;
		$sub_ordering = ($this->lists['order'] == 'ordering');
		JHTML::_('behavior.tooltip');
		  ?>
		<script language="javascript" type="text/javascript">
		function submitbutton(pressbutton) {
			var form = document.adminForm;
			if (pressbutton == 'cancel') {
				submitform( pressbutton );
				return;
				}
				submitform( pressbutton );
			}
    </script>
		<form action="index.php?option=com_prayercenter" method="post" name="adminForm">
	<fieldset id="filter-bar">
		<div class="filter-search fltlft">
			<label class="filter-search-lbl" for="filter_search"><?php echo JText::_('Filter').'&nbsp;'; ?></label>
			<input type="text" name="search" id="search" value="<?php echo $this->lists['search'];?>" class="inputbox" onChange="document.adminForm.submit();" />
				<button onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>
				<button onclick="document.getElementById('search').value='';document.getElementById('filter_order').value='id';document.getElementById('filter_order_Dir').value='DESC';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button>
		</div>
	</fieldset>
	<div class="clr"> </div>
  	<table class="adminlist">
  	<thead>
    <tr>
			<th width="10" class="title">
				<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->rows); ?>);" />
			</th>
			<th class="title">
				<?php echo JHTML::_('grid.sort',   'Email Address', 'email', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
			</th>
      <th class="title" width="50">&nbsp;</th>
			<th class="title">
				<?php echo JHTML::_('grid.sort',   'Date', 'date', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
			</th>
      <th class="title" width="50">&nbsp;</th>
			<th width="5%" class="title" nowrap="nowrap">
				<?php echo JHTML::_('grid.sort',   'Approved', 'approved', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
			</th>
      <th class="title" width="50">&nbsp;</th>
		</tr></thead>
		<?php
		$k = 0;
		$i = 0;
		for ($i=0, $n=count( $this->rows ); $i < $n; $i++) {
			$row = &$this->rows[$i];
			?>
			<tbody><tr class="row<?php echo $k; ?>">
				<td width="5"><input type="checkbox" id="cb<?php echo $i;?>" name="cid[]" value="<?php echo $row->id; ?>" onClick="isChecked(this.checked);"></td>
                <td align="center"><?php echo $row->email; ?></td>
                <td align="center">&nbsp;</td>
                <td align="center"><?php echo $row->date; ?></td>
                <td align="center">&nbsp;</td>
  <?php
    if( (real)$this->JVersion->RELEASE == 1.5 ) {
      $imagedir = 'images';
    } elseif( (real)$this->JVersion->RELEASE >= 1.6 ){
      $template	= JFactory::getApplication()->getTemplate();
      $imagedir = 'templates/'.$template.'/images/admin';
    }
			if ($row->approved) {
      	$approvedimg = "<a href=\"javascript: void(0);\" ><img src=\"".$imagedir."/tick.png\" border=\"0\" onclick=\"return listItemTask('cb$i','unapprove');\" /></a>";
			} else {
      	$approvedimg = "<a href=\"javascript: void(0);\" ><img src=\"".$imagedir."/publish_r.png\" border=\"0\" onclick=\"return listItemTask('cb$i','approve');\" /></a>";
			} 
  ?>
                <td align="center"><?php echo $approvedimg; ?></td>
                <td align="center">&nbsp;</td>
				<?php
					$k = 1 - $k;
			}?>
		</tr></tbody>
		<tfoot><tr>
			<th align="center" colspan="7">
				<?php echo $this->pageNav->getListFooter(); ?></th>
		</tr>
    </tfoot>
		</table>
			<input type="hidden" name="option" value="com_prayercenter" />
			<input type="hidden" name="task" value="manage_sub" />
			<input type="hidden" name="controller" value="prayercenter" />
			<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
	</form><br />
	<?php
  $prayercenteradmin->PCfooter();
?>