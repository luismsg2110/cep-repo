<?php
/**
* PrayerCenter Component
* 
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
*/
defined( '_JEXEC' ) or die( 'Restricted Access' );
/**
* Provides access to the #__prayercenter_devotions table
*/
class TablePrayerCenterDevotions extends JTable {
	/** @var int Unique id*/
	var $id=null;
	/** @var int */
	var $checked_out=null;
	/** @var datetime */
	var $checked_out_time=null;
	var $ordering=null;
	/**
	* @param database A database connector object
	*/
	function __construct( &$db ) {
		parent::__construct( '#__prayercenter_devotions', 'id', $db );
	}
	function check() {
		if( empty( $this->created ) ) {
			$this->created = date('Y-m-d H:i:s');
		}
		return true;
	}
}
?>
