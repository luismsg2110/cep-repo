<?php
/**
* PrayerCenter Component
* 
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
*/
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );
jimport('joomla.application.component.model');
class PrayerCenterModelLinks extends JModel
{
	/**
	 * @var int
	 */
	var $_id = null;
	/**
	 * @var array
	 */
	var $_data = null;
	/**
	 * Constructor
	 *
	 * @since 1.5
	 */
	function __construct()
	{
		parent::__construct();
		$array = JRequest::getVar('cid', array(0), '', 'array');
		$edit	= JRequest::getVar('edit',true);
		if($edit)
			$this->setId((int)$array[0]);
	}
	function setId($id)
	{
		$this->_id		= $id;
		$this->_data	= null;
	}
	function move($direction)
	{
  	$row =& JTable::getInstance('PrayerCenterlinks', 'Table');
		if (!$row->load($this->_id)) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}
		if (!$row->move( $direction, ' published >= 0 ' )) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}
		return true;
	}
	function saveorderlinks($cid = array(), $order)
	{
    $row =& JTable::getInstance('PrayerCenterlinks', 'Table');
    $this->_db->setQuery("SELECT * FROM #__prayercenter_links");
    $results = $this->_db->loadObjectList();
    if(!empty($cid)){
		for( $i=0; $i < count($cid); $i++ )
		{
      if($results[$i]->ordering != $order[$i])
			{
       	$save = "UPDATE #__prayercenter_links SET ordering='".$order[$i]."' WHERE id='".$cid[$i]."'";
        $this->_db->setQuery($save);
      	if (!$this->_db->query()) {
					$this->setError($this->_db->getErrorMsg());
					return false;
				}
	     }
  		}
    }
		$row->reorder();
		return true;
	}
	function &getData()
	{
		// Load the weblink data
		if ($this->_loadData())
		{
			// Initialize some variables
			$user = &JFactory::getUser();
			// Check to see if the category is published
			if (!$this->_data->cat_pub) {
				JError::raiseError( 404, JText::_("Resource Not Found") );
				return;
			}
			// Check whether category access level allows access
			if ($this->_data->cat_access > $user->get('aid', 0)) {
				JError::raiseError( 403, JText::_('ALERTNOTAUTH') );
				return;
			}
		}
		else  $this->_initData();
		return $this->_data;
	}
}
