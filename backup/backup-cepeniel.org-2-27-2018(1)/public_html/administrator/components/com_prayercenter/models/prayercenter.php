<?php
/**
* PrayerCenter Component
* 
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
*/
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );
jimport('joomla.application.component.model');

class PrayerCenterModelPrayerCenter extends JModel
{
	/**
	 * @var int
	 */
	var $_id = null;
	/**
	 * @var array
	 */
	var $_data = null;
	var $_forms = null;
	/**
	 * Constructor
	 *
	 * @since 1.5
	 */
	function __construct()
	{
		parent::__construct();
		$id = JRequest::getVar('id', 0, '', 'int');
		$this->setId((int)$id);
	}
	function setId($id)
	{
		$this->_id		= $id;
		$this->_data	= null;
	}
	function isCheckedOut( $uid=0 )
	{
		if ($this->_loadData())
		{
			if ($uid) {
				return ($this->_data[0]->checked_out && $this->_data[0]->checked_out != $uid);
			} else {
				return $this->_data[0]->checked_out;
			}
		}
	}
	function checkin()
	{
		if ($this->_id)
		{
			$prayercenter = & $this->getTable();
			if(! $prayercenter->checkin($this->_id)) {
				$this->setError($this->_db->getErrorMsg());
				return false;
			}
		}
		return false;
	}
	function checkout($uid = null)
	{
		if ($this->_id)
		{
			if (is_null($uid)) {
				$user	=& JFactory::getUser();
				$uid	= $user->get('id');
			}
			$prayercenter = & $this->getTable();
			if(!$prayercenter->checkout($uid, $this->_id)) {
				$this->setError($this->_db->getErrorMsg());
				return false;
			}

			return true;
		}
		return false;
	}
	function _loadData()
	{
		if (empty($this->_data))
		{
			$query = 'SELECT checked_out'.
					' FROM #__prayercenter' .
					' WHERE id = '. (int) $this->_id;
			$this->_db->setQuery($query);
			$this->_data = $this->_db->loadObject();
			return (boolean) $this->_data;
		}
		return true;
	}
}
?>
