<?php
/**
* PrayerCenter Component
* 
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
*/
// ensure this file is being included by a parent file
defined( '_JEXEC' ) or die( 'Restricted Access' );
class MENU_prayercenter {
	/**
	* Draws the menu
	*/
	function MANAGE_REQ_MENU() {
    JToolBarHelper::title( 'Manage Requests', 'article' );
		JToolBarHelper::publishList("publish", 'Publish');
		JToolBarHelper::unpublishList("unpublish", 'Unpublish');
		JToolBarHelper::divider();
  	JToolBarHelper::archiveList("archive", 'Archive');
		JToolBarHelper::unarchiveList("unarchive", 'Unarchive');
		JToolBarHelper::divider();
		JToolBarHelper::editListX("edit", 'Edit');
		JToolBarHelper::deleteList( "Remove Request(s)?", "remove_req", 'Remove' );
		JToolBarHelper::divider();
		$cb = & JToolBar::getInstance('toolbar');
		$cb->appendButton( 'confirm', 'Do you wish to proceed with the purging of old prayer requests?  Requests that are older than the request/archive retention settings will be removed from the PrayerCenter database table.', 'apply', 'Purge', 'purge', false);
	}
	function MANAGE_DEV_MENU() {
    JToolBarHelper::title( 'Manage Devotionals', 'searchtext' );
		JToolBarHelper::publishList("publishdevotion", 'Publish');
		JToolBarHelper::unpublishList("unpublishdevotion", 'Unpublish');
		JToolBarHelper::divider();
		JToolBarHelper::editListX( "editdevotion", 'Edit');
		JToolBarHelper::addNewX( "adddevotion", 'New');
		JToolBarHelper::deleteList( "Remove Devotional(s)?", "remove_devotion", 'Remove' );
	}
	function MANAGE_LINK_MENU() {
    $version = new JVersion();
    if( (real)$version->RELEASE == 1.5 ) {
      global $mainframe;
  		$template	= $mainframe->getTemplate();
  		$html  = "<div class=\"header\" style=\"background-image: url('templates/".$template."/images/header/icon-48-component.png');\">\n";
  		$html .= "Manage Links\n";
  		$html .= "</div>\n";
  		$mainframe->set('JComponentTitle', $html);
    } elseif( (real)$version->RELEASE >= 1.6 ){
  		$app = JFactory::getApplication();
      $template	= JFactory::getApplication()->getTemplate();
  		$html  = "<div class=\"pagetitle\" style=\"background-image: url('templates/".$template."/images/header/icon-48-component.png');\">\n";
  		$html .= "<h2>Manage Links</h2>";
  		$html .= "</div>";
  		$app->set('JComponentTitle', $html);
    }
		JToolBarHelper::publishList("publishlink", 'Publish');
		JToolBarHelper::unpublishList("unpublishlink", 'Unpublish');
		JToolBarHelper::divider();
		JToolBarHelper::editListX( "editlink", 'Edit');
		JToolBarHelper::addNewX( "addlink", 'New');
		JToolBarHelper::deleteList( "Remove Link(s)?", "remove_link", 'Remove' );
	}
	function MANAGE_SUB_MENU() {
    JToolBarHelper::title( 'Manage Subscribers', 'user' );
		JToolBarHelper::publishList('approve', 'Approve');
		JToolBarHelper::unpublishList('unapprove', 'Unapprove');
		JToolBarHelper::divider();
		$cb = & JToolBar::getInstance('toolbar');
		$cb->appendButton( 'confirm', 'Do you wish to proceed with the sychronizing of Joomla user email addresses into the PrayerCenter Subscriber listing?  This may take awhile depending on the number or users.', 'apply', 'Sync', 'syncsub', false);
		JToolBarHelper::deleteList( "Remove Subscriber(s)?", "remove_sub", 'Remove' );
	}
  function MANAGE_CSS_MENU() {
    JToolBarHelper::title( 'Manage CSS', 'thememanager' );
		JToolBarHelper::save('savecss', 'Save');
		$cb = & JToolBar::getInstance('toolbar');
		$cb->appendButton( 'confirm', 'Do you wish to reset the PrayerCenter CSS file to default settings?', 'restore', 'Reset', 'resetcss', false);
		JToolBarHelper::cancel('cancelsettings', 'Cancel' );
  }
  function MANAGE_LANG_MENU() {
    JToolBarHelper::title( 'Manage Languages', 'langmanager' );
		JToolBarHelper::save('savelang', 'Save');
		$cb = & JToolBar::getInstance('toolbar');
		$cb->appendButton( 'confirm', 'Do you wish to reset the English PrayerCenter language file to default settings?', 'restore', 'Reset', 'resetlang', false);
		JToolBarHelper::divider();
		JToolBarHelper::editListX( "editlang", 'Edit');
		JToolBarHelper::addNewX( "addlang", 'New');
		JToolBarHelper::divider();
		$cb->appendButton( 'Popup', 'help', 'Help', 'index.php?option=com_prayercenter&amp;task=pchelp&amp;pop=1&amp;tmpl=component', 800, 570 );
  }
	function MANAGE_FILES_MENU() {
    JToolBarHelper::title( 'Manage Files', 'categories' );
		$fb = & JToolBar::getInstance('toolbar');
		$fb->appendButton( 'Popup', 'help', 'Help', 'index.php?option=com_prayercenter&amp;task=pchelp&amp;pop=1&amp;tmpl=component', 800, 570 );
	}
	function EDIT_MENU() {
    JToolBarHelper::title( 'Edit Request', 'article' );
		JToolBarHelper::save('savereq', 'Save' );
		JToolBarHelper::cancel( 'canceledit', 'Cancel' );
	}
	function EDIT_LINK_MENU() {
	}
	function EDIT_LANG_MENU() {
		$lb = & JToolBar::getInstance('toolbar');
		$lb->appendButton( 'Popup', 'help', 'Help', 'index.php?option=com_prayercenter&amp;task=pchelp&amp;pop=1&amp;tmpl=component', 800, 570 );
	}
	function EDIT_DEVOTION_MENU() {
	}
	function DEFAULT_MENU() {
		require_once JPATH_COMPONENT.'/helpers/admin_pc_class.php';
    $version = new JVersion();
		if( (real)$version->RELEASE >= 1.6 ) $canDo	= prayercenteradmin::getActions();
    JToolBarHelper::title( 'PrayerCenter Control Panel', 'cpanel' );
		$sb = & JToolBar::getInstance('toolbar');
    if( (real)$version->RELEASE == 1.5 ) {
  		$sb->appendButton( 'Popup', 'config', 'Parameters', 'index.php?option=com_prayercenter&amp;task=config&amp;pop=1&amp;tmpl=component', 550, 570 );
    } elseif( (real)$version->RELEASE >= 1.6 && $canDo->get('core.admin') ){
      JToolBarHelper::preferences('com_prayercenter', '550', '900', 'Parameters');
    }
		JToolBarHelper::divider();
		$sb->appendButton( 'Popup', 'help', 'Help', 'index.php?option=com_prayercenter&amp;task=pchelp&amp;pop=1&amp;tmpl=component', 800, 570 );
	}
}
?>