<?php
/**
* @version $Id: install.prayercenter.php,v 1.5x
* @package PrayerCenter
*/
defined ( '_JEXEC' ) or die ( 'Restricted Access' );
  function com_install() {
    $version = new JVersion();
    if ( (real)$version->RELEASE < 1.5 ) {
        echo '<h1 style="color: red;">This PrayerCenter component will only work on Joomla version 1.5 or later!</h1>';
        return false;
    } elseif ( (real)$version->RELEASE == 1.5 ) {
        $db =& JFactory::getDBO();
        $query = "SELECT id FROM #__components WHERE admin_menu_link = 'option=com_prayercenter&task=config'";
        $db->setQuery($query);
        $id = $db->loadResult();
        $query = "UPDATE #__components SET admin_menu_img  = 'js/ThemeOffice/config.png' WHERE id='$id'";
        $db->setQuery($query);
        $db->query();
        $params = getPCParams();
        setPCParams($params);
    } else {
      return true;
    }
  }
  function getPCParams(){
    $xml = JFactory::getXMLParser('Simple');
    $xml->loadFile(JPATH_ROOT.'/administrator/components/com_prayercenter/config.xml');
    $ini = array();
    $root = $xml->document;
    $element =& $root->getElementByPath('params');
		$params = $element->children();
		if (count($params) == 0) {
			return null;
		}
		$ini = null;
		foreach ($params as $param) {
			if (!$name = $param->attributes('name')) {
				continue;
			}
      $value = $param->attributes('default');
  		if ((string)$name != '@spacer') {
  			$ini .= $name."=".$value."\n";
  		}
		}
    return $ini;
  }
  function setPCParams($param_array){
    if(count($param_array) > 0){
      $db = JFactory::getDbo();
      $db->setQuery('UPDATE #__components SET params='.$db->quote($param_array).' WHERE link="option=com_prayercenter" AND parent="0"');
      $db->query();
    }
  }
?>