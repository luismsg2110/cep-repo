<?php
/**
* @package		Komento
* @copyright	Copyright (C) 2012 Stack Ideas Private Limited. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Komento is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Restricted access');
?>
<table class="noshow">
	<tr>
		<td width="50%" valign="top">
			<fieldset class="adminform">
			<legend><?php echo JText::_( 'COM_KOMENTO_SETTINGS_PROFILE_TAB' ); ?></legend>
			<table class="admintable" cellspacing="1">
				<tbody>

					<!-- Show Activities Tab -->
					<?php echo $this->renderSetting( 'COM_KOMENTO_SETTINGS_PROFILE_TAB_ACTIVITIES', 'profile_tab_activities' ); ?>

					<!-- Show Popular Comments Tab -->
					<?php echo $this->renderSetting( 'COM_KOMENTO_SETTINGS_PROFILE_TAB_POPULAR', 'profile_tab_popular' ); ?>

					<!-- Show Sticked Comments Tab -->
					<?php echo $this->renderSetting( 'COM_KOMENTO_SETTINGS_PROFILE_TAB_STICKED', 'profile_tab_sticked' ); ?>

				</tbody>
			</table>
			</fieldset>
		</td>
		<td valign="top">
			<fieldset class="adminform">
			<legend><?php echo JText::_( 'COM_KOMENTO_SETTINGS_PROFILE_ACTIVITIES' ); ?></legend>
			<table class="admintable" cellspacing="1">
				<tbody>

					<!-- Show Comments -->
					<?php echo $this->renderSetting( 'COM_KOMENTO_SETTINGS_PROFILE_ACTIVITIES_COMMENTS', 'profile_activities_comments' ); ?>

					<!-- Show Replies -->
					<?php echo $this->renderSetting( 'COM_KOMENTO_SETTINGS_PROFILE_ACTIVITIES_REPLIES', 'profile_activities_replies' ); ?>

					<!-- Show Likes -->
					<?php echo $this->renderSetting( 'COM_KOMENTO_SETTINGS_PROFILE_ACTIVITIES_LIKES', 'profile_activities_likes' ); ?>

				</tbody>
			</table>
			</fieldset>
		</td>
	</tr>
</table>
