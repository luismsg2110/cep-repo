<?php
/**
 * @package		GAnalytics
 * @author		Digital Peak http://www.digital-peak.com
 * @copyright	Copyright (C) 2012 Digital Peak, All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL
 */

defined('_JEXEC') or die();

jimport( 'joomla.application.component.model' );

class GAnalyticsModelDashboard extends JModel {

	private $statsViews;
	private $profiles;
	private $groups;

	public function getStatsViews()	{
		if (empty( $this->statsViews )) {
			$this->statsViews = $this->_getList('SELECT * FROM #__ganalytics_stats order by `group_id` asc, `column` asc, `position` asc');
		}
		return $this->statsViews;
	}

	public function getGroups()	{
		if (empty($this->groups)) {
			$this->groups = $this->_getList('SELECT * FROM #__ganalytics_stats_groups order by `position` asc');
		}
		return $this->groups;
	}

	public function getProfiles() {
		if (empty( $this->profiles )) {
			$this->profiles = $this->_getList('SELECT * FROM #__ganalytics_profiles');
		}
		return $this->profiles;
	}

	public function saveStructure(array $structure){
		foreach ($structure as $index => $column) {
			foreach ($column as $row => $id) {
				$table = JTable::getInstance('StatsView', 'GAnalyticsTable');
				$table->save(array('id' => $id, 'column' => $index, 'position' => $row));
			}
		}
		return true;
	}

	public function addgroup($name){
		$table = $this->getTable('StatsGroup', 'GAnalyticsTable');
		$this->getDbo()->setQuery('select max(position) as max from #__ganalytics_stats_groups');
		$max = $this->getDbo()->loadObjectList();
		$table->save(array('name' => $this->getDbo()->escape($name), 'position' => (int)$max[0]->max + 1));

		return $table;
	}

	public function deletegroup($id){
		$table = $this->getTable('StatsGroup', 'GAnalyticsTable');
		$table->load($id);
		$success = $table->delete($id);
		if(!$success){
			return $success;
		}
		$this->getDbo()->setQuery('update #__ganalytics_stats_groups set `position` = (`position` -1) where `position`> '.$table->position);
		$this->getDbo()->query();

		$this->getDbo()->setQuery('delete from #__ganalytics_stats where `group_id` = '.$id);
		$this->getDbo()->query();

		return $success;
	}

	public function addcolumn($id, $column){
		$table = $this->getTable('StatsGroup', 'GAnalyticsTable');
		$table->load($id);
		$table->column_count = $table->column_count + 1;
		$success = $table->store();
		if(!$success){
			return $success;
		}
		$this->getDbo()->setQuery('update #__ganalytics_stats set `column` = (`column` +1) where `group_id`='.$table->id.' and `column`>'.$column);
		$this->getDbo()->query();

		return $success;
	}

	public function deletecolumn($id, $column){
		$table = $this->getTable('StatsGroup', 'GAnalyticsTable');
		$table->load($id);
		$table->column_count = $table->column_count - 1;
		$success = $table->store();
		if(!$success){
			return $success;
		}
		$this->getDbo()->setQuery('delete from #__ganalytics_stats where `group_id`='.$table->id.' and `column`='.$column);
		$this->getDbo()->query();

		$this->getDbo()->setQuery('update #__ganalytics_stats set `column` = (`column` -1) where `group_id`='.$table->id.' and `column`>'.$column);
		$this->getDbo()->query();

		return $success;
	}

	public function savewidget(){
		$table = $this->getTable('StatsView', 'GAnalyticsTable');
		return $table->save(JRequest::get());
	}

	public function addwidget(){
		$table = $this->getTable('StatsView', 'GAnalyticsTable');
		$success = $table->save(JRequest::get());
		if(!$success){
			return $success;
		}

		$this->getDbo()->setQuery('select max(position) as max from #__ganalytics_stats where group_id='.$table->group_id);
		$max = $this->getDbo()->loadObjectList();
		$table->save(array('position' => (int)$max[0]->max + 1));

		return $success;
	}

	public function deletewidget($id){
		$table = $this->getTable('StatsView', 'GAnalyticsTable');
		$table->load($id);
		$success = $table->delete($id);

		if(!$success){
			return $success;
		}

		$this->getDbo()->setQuery('update #__ganalytics_stats set `position` = (`position` -1) where `group_id`='.$table->group_id.' and `column`='.$table->column.' and `position`>'.$table->position);
		$this->getDbo()->query();
		return $success;
	}


	public function getStatsView() {
		$view = JTable::getInstance('StatsView', 'GAnalyticsTable');
		$view->load(JRequest::getInt('id'));
		return $view;
	}

	public function getStatsData() {
		$view = JTable::getInstance('StatsView', 'GAnalyticsTable');
		$view->load(JRequest::getInt('id'));

		$profile = JTable::getInstance('Profile', 'GAnalyticsTable');
		$profile->load(JRequest::getInt('gaid', 0));

		$sort = str_replace('ga:', '', $view->sort);
		if(!empty($sort)){
			$sort = array($sort);
		} else {
			$sort = null;
		}

		$s = JRequest::getVar('start', null);
		$start = new JDate();
		$start = $s == null ? $start = null : $start->setDate(substr($s, 0,4), substr($s, 5,2), substr($s, 8,2));

		$e = JRequest::getVar('end', null);
		$end = new JDate();
		$end = $e == null ? $end = null: $end->setDate(substr($e, 0,4), substr($e, 5,2), substr($e, 8,2));

		return GAnalyticsDataHelper::getData($profile, array(str_replace('ga:', '', $view->dimensions)), array(str_replace('ga:', '', $view->metrics)), $start, $end, $sort, $view->max_result, $view->filter);
	}

	public function reset()	{
		$this->getDbo()->setQuery('delete from #__ganalytics_stats_groups');
		$this->getDbo()->query();
		$this->getDbo()->setQuery("
			INSERT INTO `#__ganalytics_stats_groups` (`id`, `name`, `position`, `column_count`) VALUES
			(1, 'Overview', 0, 2),
			(2, 'Visitors', 0, 2),
			(3, 'Pages', 0, 2),
			(4, 'Sources', 0, 2),
			(5, 'Demographics', 0, 2),
			(6, 'System', 0, 2);
		");
		$this->getDbo()->query();

		$this->getDbo()->setQuery('delete from #__ganalytics_stats');
		$this->getDbo()->query();

		$this->getDbo()->setQuery("
			INSERT INTO `#__ganalytics_stats` (`id`, `group_id`, `column`, `position`, `type`, `name`, `metrics`, `dimensions`, `sort`, `filter`, `max_result`) VALUES
			(1, 2, 0, 0, 'chart', 'Visitors per day', 'ga:visitors', 'ga:date', 'ga:date', '', 1000),
			(2, 3, 0, 0, 'list', 'Top pages', 'ga:visits', 'ga:pagePath', '-ga:visits', '', 10),
			(3, 4, 0, 0, 'chart', 'Referring Sites', 'ga:visits', 'ga:source', '-ga:visits', '', 10),
			(4, 5, 0, 1, 'list', 'Countrys', 'ga:visits', 'ga:country', '-ga:visits', '', 300),
			(5, 6, 0, 0, 'chart', 'Browsers', 'ga:visits', 'ga:browser', '-ga:visits', '', 10),
			(7, 2, 0, 1, 'chart', 'Unique visitors', 'ga:newVisits', 'ga:date', 'ga:date', '', 1000),
			(8, 2, 1, 1, 'chart', 'Page views per day', 'ga:pageviews', 'ga:date', 'ga:date', '', 1000),
			(9, 2, 1, 0, 'chart', 'New vs returning', 'ga:visitors', 'ga:visitorType', '', '', 1000),
			(10, 3, 1, 1, 'list', 'Page load time', 'ga:pageLoadTime', 'ga:pagePath', '-ga:pageLoadTime', '', 10),
			(11, 4, 1, 1, 'chart', 'Referring medium', 'ga:visits', 'ga:medium', '-ga:visits', '', 1000),
			(12, 4, 0, 2, 'list', 'Search keywords', 'ga:visits', 'ga:keyword', '-ga:visits', '', 10),
			(13, 4, 1, 3, 'chart', 'Campaign', 'ga:visits', 'ga:campaign', '-ga:visits', '', 10),
			(14, 5, 0, 0, 'chart', 'Country', 'ga:visits', 'ga:country', '-ga:visits', '', 100),
			(15, 5, 1, 2, 'chart', 'Continent', 'ga:visits', 'ga:continent', '-ga:visits', '', 10),
			(16, 5, 1, 3, 'chart', 'Network', 'ga:visits', 'ga:networkDomain', '-ga:visits', '', 10),
			(17, 6, 1, 1, 'chart', 'OS', 'ga:visits', 'ga:operatingSystem', '-ga:visits', '', 10),
			(18, 6, 0, 2, 'chart', 'Screen resolution', 'ga:visits', 'ga:screenResolution', '-ga:visits', '', 10),
			(19, 6, 1, 3, 'chart', 'Mobile', 'ga:visits', 'ga:isMobile', '', '', 10),
			(20, 3, 0, 2, 'chart', 'Language', 'ga:visits', 'ga:language', '-ga:visits', '', 10),
			(21, 3, 1, 3, 'list', 'Landing page', 'ga:visits', 'ga:landingPagePath', '-ga:visits', '', 10),
			(23, 1, 0, 1, 'chart', 'Visitors per day', 'ga:visitors', 'ga:date', 'ga:date', '', 1000),
			(24, 1, 1, 2, 'chart', 'Country', 'ga:visits', 'ga:country', '-ga:visits', '', 10),
			(25, 1, 0, 3, 'chart', 'Source', 'ga:visits', 'ga:source', '-ga:visits', '', 10),
			(26, 1, 1, 4, 'list', 'Top pages', 'ga:visits', 'ga:pagePath', '-ga:visits', '', 10);
		");
		$this->getDbo()->query();

		return true;
	}
}