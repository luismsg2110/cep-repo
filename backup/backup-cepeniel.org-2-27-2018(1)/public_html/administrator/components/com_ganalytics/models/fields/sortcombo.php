<?php
/**
 * @package		GAnalytics
 * @author		Digital Peak http://www.digital-peak.com
 * @copyright	Copyright (C) 2012 Digital Peak, All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL
 */

defined('_JEXEC') or die();

JFormHelper::loadFieldClass('list');

class JFormFieldSortcombo extends JFormFieldList
{
	protected $type  = 'Sortcombo';

	protected function getInput() {
		$params = '';
		if(isset($this->element['params-field'])){
			$params = 'params_';
		}
		$prefix = $this->form->getFormControl();
		if(!empty($prefix)){
			$prefix .= '_';
		}
		$id = $prefix.$params.$this->element['name'];

		$document = &JFactory::getDocument();
 		$document->addScript(JURI::base().'components/com_ganalytics/libraries/jquery/jquery.min.js');
 		$document->addScript(JURI::base().'components/com_ganalytics/models/fields/sortcombo.js');
		return parent::getInput();
	}

	protected function getOptions(){
		return array(JHtml::_('select.option', $this->value));
	}
}