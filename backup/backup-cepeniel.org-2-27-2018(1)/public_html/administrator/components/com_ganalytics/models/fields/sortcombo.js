function updateSortCombo(form){
	if(form == null){
		return;
	}
	var combo = form.find('.sortcombo');
	var dimCombo = form.find('.dimensionscombo :selected');
	var metrCombo = form.find('.metricscombo :selected');
	
	var oldValue = combo.val();
	combo.html('');
	combo.append(jQuery('<option></option>').val('').html(''));
	combo.append(jQuery('<option></option>').val(dimCombo.val()).html(dimCombo.html()));
	combo.append(jQuery('<option></option>').val('-'+dimCombo.val()).html('-'+dimCombo.html()));
	combo.append(jQuery('<option></option>').val(metrCombo.val()).html(metrCombo.html()));
	combo.append(jQuery('<option></option>').val('-'+metrCombo.val()).html('-'+metrCombo.html()));
	combo.val(oldValue);
}
jQuery(document).ready(function(){
	jQuery('.dimensionscombo').live('change', function() {
		updateSortCombo(jQuery(this).parents('form'));
	});
	jQuery('.metricscombo').live('change', function() {
		updateSortCombo(jQuery(this).parents('form'));
	});
	jQuery('.dimensionscombo').each(function() {
		updateSortCombo(jQuery(this).parents('form'));
	});
});