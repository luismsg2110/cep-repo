<?php
/**
 * @package		GAnalytics
 * @author		Digital Peak http://www.digital-peak.com
 * @copyright	Copyright (C) 2012 Digital Peak, All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL
 */

defined('_JEXEC') or die();

require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ganalytics'.DS.'helper'.DS.'ganalytics.php');

JFormHelper::loadFieldClass('groupedlist');

class JFormFieldMetricsCombo extends JFormFieldGroupedList{

	protected $type = 'MetricsCombo';

	private $notLoad = false;

	protected function getGroups(){
		JFactory::getLanguage()->load('com_ganalytics');

		if($this->notLoad){
			return array(array(JHtml::_('select.option', $this->value,  GAnalyticsHelper::translate($this->value), 'value', 'text')));
		}

		$xml = new SimpleXMLElement(JFile::read(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ganalytics'.DS.'helper'.DS.'metrics.xml'));
		$groups = array();
		foreach ($xml->div as $group) {
			$label = GAnalyticsHelper::translate($group->div);

			if (!isset($groups[$label]))
			{
				$groups[$label] = array();
			}

			foreach ($group->label as $value) {
				$v = (string)$value->attributes()->for;

				$string = str_replace(' ', '_', $v);
				$string = str_replace('(', '_', $string);
				$string = str_replace(')', '_', $string);
				//echo strtoupper('COM_GANALYTICS_'.$string).'="'.str_replace('ga:', '', $v).' ['.$v.']"'."\n";
				$groups[$label][] = JHtml::_('select.option', $v, GAnalyticsHelper::translate($v), 'value', 'text');
			}

		}
		return $groups;
	}

	public function setNotLoad(){
		$this->notLoad = true;
	}
}