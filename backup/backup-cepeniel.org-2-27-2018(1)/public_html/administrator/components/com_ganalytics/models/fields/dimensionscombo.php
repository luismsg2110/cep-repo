<?php
/**
 * @package		GAnalytics
 * @author		Digital Peak http://www.digital-peak.com
 * @copyright	Copyright (C) 2012 Digital Peak, All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL
 */

defined('_JEXEC') or die();

require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ganalytics'.DS.'helper'.DS.'ganalytics.php');

JFormHelper::loadFieldClass('groupedlist');

class JFormFieldDimensionsCombo extends JFormFieldGroupedList{

	protected $type = 'DimensionsCombo';

	private $notLoad = false;

	protected function getGroups(){
		JFactory::getLanguage()->load('com_ganalytics');

		if($this->notLoad){
			return array(array(JHtml::_('select.option', $this->value,  GAnalyticsHelper::translate($this->value), 'value', 'text')));
		}

		$xml = new SimpleXMLElement(JFile::read(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ganalytics'.DS.'helper'.DS.'dimensions.xml'));
		$groups = array();
		foreach ($xml->div as $group) {
			$label = GAnalyticsHelper::translate($group->div);

			if (!isset($groups[$label]))
			{
				$groups[$label] = array();
			}

			foreach ($group->label as $value) {
				$v = (string)$value->attributes()->for;
				$groups[$label][] = JHtml::_('select.option', $v,  GAnalyticsHelper::translate($v), 'value', 'text');
			}

		}
		return $groups;
	}

	public function setNotLoad(){
		$this->notLoad = true;
	}
}