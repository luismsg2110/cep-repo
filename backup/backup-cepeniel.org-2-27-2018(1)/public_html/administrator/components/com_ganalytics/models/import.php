<?php
/**
 * @package		GAnalytics
 * @author		Digital Peak http://www.digital-peak.com
 * @copyright	Copyright (C) 2012 Digital Peak, All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL
 */

defined('_JEXEC') or die();

jimport('joomla.application.component.model');

class GAnalyticsModelImport extends JModel
{

	function getItems() {
		$profiles = GAnalyticsDataHelper::getAccounts(JRequest::getVar('user'), JRequest::getVar('pass'));

		if($profiles === null){
			return $profiles;
		}

		$tmp = array();
		foreach($profiles as $profile){
			$table = & $this->getTable('Profile', 'GAnalyticsTable');
			$table->id = 0;
			$table->accountID = $profile->getAccountId();
			$table->accountName = $profile->getAccountName();
			$table->profileID = $profile->getProfileId();
			$table->profileName = $profile->getTitle();
			$table->webPropertyId = $profile->getWebPropertyId();
			$tmp[] = $table;
		}
		return $tmp;
	}

	function store()	{
		$cids = JRequest::getVar( 'cid', array(0));
		if(empty($cids)){
			return true;
		}
		$accounts = JRequest::getVar( 'accounts', array(0));
		foreach ($cids as $cid) {
			$row = & $this->getTable('Profile', 'GAnalyticsTable');
			$row->id = 0;
			$row->accountID = $accounts[$cid]['id'];
			$row->profileID = $accounts[$cid]['profile'];
			$row->webPropertyId = $accounts[$cid]['property'];
			$row->accountName = $this->getDbo()->escape(base64_decode($accounts[$cid]['name']));
			$row->profileName = $this->getDbo()->escape(base64_decode($accounts[$cid]['profileName']));
			$row->username = JRequest::getVar('user');
			$row->password = base64_decode(JRequest::getVar('pass'));

			$data = GAnalyticsDataHelper::getData($row, array('year', 'month'), array('visits'), new JDate('2005-1-1'), null, array('year'), 1000);
			if($data !== null){
				foreach ($data as $item){
					if($item->getVisits() > 0){
						$startDate = new JDate();
						$startDate->setDate((int)$item->getYear(), (int)$item->getMonth(), 1);
						$startDate->setTime(0, 0);

						$endDate = new JDate();
						$endDate->setDate((int)$item->getYear(), (int)$item->getMonth(), 1);
						$endDate->setTime(0, 0);
						$endDate->modify('+1 month');

						if($endDate->format('U') > time()){
							$endDate = new JDate();
						}
						$data = GAnalyticsDataHelper::getData($row, array('date'), array('visits'), $startDate, $endDate, array('date'), 100);
						break;
					}
				}
				$text = date('Y-m-d');
				foreach ($data as $item){
					if($item->getVisits() > 0){
						$text = $item->getDate();
						$text = substr($text, 0, 4).'-'.substr($text, 4, 2).'-'.substr($text, 6, 2);
						break;
					}
				}
				$row->startDate = $text;
			}
			if (!$row->check()) {
				JError::raiseWarning( 500, $row->getError() );
				return false;
			}

			if (!$row->store()) {
				JError::raiseWarning( 500, $row->getError() );
				return false;
			}
		}
		return true;
	}
}