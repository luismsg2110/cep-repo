<?php
/**
 * @package		GAnalytics
 * @author		Digital Peak http://www.digital-peak.com
 * @copyright	Copyright (C) 2012 Digital Peak, All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL
 */

defined('_JEXEC') or die();

require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ganalytics'.DS.'libraries'.DS.'gapi'.DS.'gapi.class.php');

class GAnalyticsDataHelper{

	public static function getAccounts($username, $password){
		try{
			$ga = new gapi($username,$password);

			return $ga->requestAccountData(1, 1000);
		}catch(Exception $e){
			JError::raiseWarning( 500, JText::_('COM_GANALYTICS_IMPORT_VIEW_MODEL_FEED_ERROR').' '.$e->getMessage());
			return null;
		}
	}

	public static function getData($profile, array $dimensions, array $metrics, JDate $startDate = null, JDate $endDate = null, array $sort = null, $max = 1000, $filter = null){
		if($startDate == null){
			$startDate = new JDate();
			$startDate->modify('-1 month');
		}
		if($endDate == null){
			$endDate = new JDate();
			$endDate->modify('-1 day');
		}

		try{
			if(GAnalyticsHelper::isPROMode()){
				return GAnalyticsProUtil::getFromCache($profile, $dimensions, $metrics, $startDate, $endDate, $sort, $max, $filter);
			} else {
				$ga = new gapi($profile->username,$profile->password);
				return $ga->requestReportData($profile->profileID, $dimensions, $metrics, $sort, $filter, $startDate->format('Y-m-d'), $endDate->format('Y-m-d'), 1, $max);
			}
		}catch(Exception $e){
			JError::raiseWarning( 500, JText::_('COM_GANALYTICS_IMPORT_VIEW_MODEL_FEED_ERROR').' '.$e->getMessage());
			return null;
		}
	}
}