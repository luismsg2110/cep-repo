<?php
/**
 * @package		GAnalytics
 * @author		Digital Peak http://www.digital-peak.com
 * @copyright	Copyright (C) 2012 Digital Peak, All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL
 */

defined('_JEXEC') or die();

jimport('joomla.database.table');
jimport('joomla.utilities.simplecrypt');

class GAnalyticsTableProfile extends JTable
{
	public function __construct(&$db)
	{
		parent::__construct('#__ganalytics_profiles', 'id', $db);
	}

	public function load($keys = null, $reset = true)
	{
		$result = parent::load($keys, $reset);

		if(isset($this->password) && !empty($this->password)){
			$cryptor = new JSimpleCrypt();
			$this->password = $cryptor->decrypt($this->password);
		}

		return $result;
	}

	public function store($updateNulls = false)
	{
		$oldPassword = $this->password;
		if(!empty($oldPassword)){
			$cryptor = new JSimpleCrypt();
			$this->password = $cryptor->encrypt($oldPassword);
		}
		$result = parent::store($updateNulls);

		$this->password = $oldPassword;

		return $result;
	}
}