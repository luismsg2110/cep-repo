<?php
/**
 * @package		GAnalytics
 * @author		Digital Peak http://www.digital-peak.com
 * @copyright	Copyright (C) 2012 Digital Peak, All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL
 */

defined('_JEXEC') or die();

jimport( 'joomla.application.component.view' );

class GAnalyticsViewDashboard extends JView{

	public function display($tpl = null){
		$this->assignRef('statsView', $this->get('StatsView'));
		if(JRequest::getVar('layout') == 'data'){
			$this->assignRef('data', $this->get('StatsData'));
		}
		parent::display($tpl);
	}
}