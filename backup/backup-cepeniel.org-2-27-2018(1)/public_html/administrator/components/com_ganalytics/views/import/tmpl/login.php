<?php
/**
 * @package		GAnalytics
 * @author		Digital Peak http://www.digital-peak.com
 * @copyright	Copyright (C) 2012 Digital Peak, All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL
 */

defined('_JEXEC') or die();
?>
<fieldset><legend><?php echo JText::_('COM_GANALYTICS_IMPORT_VIEW_CREDENTIALS');?></legend>
<form action="<?php echo JRoute::_( 'index.php?option=com_ganalytics&view=import');?>" method="post">
	<table>
	<tr><td><?php echo JText::_('COM_GANALYTICS_IMPORT_VIEW_USERNAME');?>:</td><td><input type="text" name="user" size="100"/></td></tr>
	<tr><td><?php echo JText::_('COM_GANALYTICS_IMPORT_VIEW_PASSWORD');?>:</td><td><input type="password"name="pass" size="100"/></td></tr>
	<tr><td><input type="submit" value="Login"/></td><td></td></tr>
	</table>
</form>
</fieldset>
<div align="center" style="clear: both">
	<?php echo sprintf(JText::_('COM_GANALYTICS_FOOTER'), JRequest::getVar('GANALYTICS_VERSION'));?>
</div>