<?php
/*------------------------------------------------------------------------
# com_authorlist - Author List
# ------------------------------------------------------------------------
# author    Joomla!Vargas
# copyright Copyright (C) 2012 joomla.vargas.co.cr. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://joomla.vargas.co.cr
# Technical Support:  Forum - http://joomla.vargas.co.cr/forum
-------------------------------------------------------------------------*/

// No direct access
defined('_JEXEC') or die;

jimport('joomla.form.formfield');

class JFormFieldModal_Authors extends JFormField
{
	protected $type = 'Modal_Authors';

	protected function getInput()
	{
		// Load the javascript and css
		JHtml::_('behavior.framework');
		JHTML::_('script','system/modal.js', false, true);
		JHTML::_('stylesheet','system/modal.css', array(), true);

		// Build the script.
		$script = array();
		$script[] = '	function jSelectChart_'.$this->id.'(id, name, object) {';
		$script[] = '		document.id("'.$this->id.'_id").value = id;';
		$script[] = '		document.id("'.$this->id.'_name").value = name;';
		$script[] = '		SqueezeBox.close();';
		$script[] = '	}';

		// Add the script to the document head.
		JFactory::getDocument()->addScriptDeclaration(implode("\n", $script));

		// Build the script.
		$script = array();
		$script[] = '	window.addEvent("domready", function() {';
		$script[] = '		var div = new Element("div").setStyle("display", "none").injectBefore(document.id("menu-types"));';
		$script[] = '		document.id("menu-types").injectInside(div);';
		$script[] = '		SqueezeBox.initialize();';
		$script[] = '		SqueezeBox.assign($$("input.modal"), {parse:"rel"});';
		$script[] = '	});';

		// Add the script to the document head.
		JFactory::getDocument()->addScriptDeclaration(implode("\n", $script));

		if ($this->value == 0) :
			$title = JText::_('COM_AUTHORLIST_OPTION_NO_AUTHOR_LABEL');	
		else :
			$db	= JFactory::getDBO();
			$db->setQuery(
				'SELECT u.name' .
				' FROM #__users u' .
				' LEFT JOIN #__authorlist a ON a.userid = u.id' .
				' WHERE u.id = '.(int) $this->value
			);
			$title = $db->loadResult();

			if ($error = $db->getErrorMsg()) {
				JError::raiseWarning(500, $error);
			}
		endif;

		if (empty($title)) {
			$title = JText::_('COM_AUTHORLIST_FIELD_SELECT_AUTHOR_LABEL');
		}

		$link = 'index.php?option=com_authorlist&amp;view=authors&amp;layout=modal&amp;tmpl=component&amp;function=jSelectChart_'.$this->id;

		JHTML::_('behavior.modal', 'a.modal');
		$html = "\n".'<div class="fltlft"><input type="text" id="'.$this->id.'_name" value="'.htmlspecialchars($title, ENT_QUOTES, 'UTF-8').'" disabled="disabled" /></div>';
		$html .= '<div class="button2-left"><div class="blank"><a class="modal" title="'.JText::_('COM_AUTHORLIST_CHANGE_AUTHOR_BUTTON').'"  href="'.$link.'" rel="{handler: \'iframe\', size: {x: 800, y: 450}}">'.JText::_('COM_AUTHORLIST_CHANGE_AUTHOR_BUTTON').'</a></div></div>'."\n";
		// The active contact id field.
		if (0 == (int)$this->value) {
			$value = '';
		} else {
			$value = (int)$this->value;
		}

		// class='required' for client side validation
		$class = '';
		if ($this->required) {
			$class = ' class="required modal-value"';
		}

		$html .= '<input type="hidden" id="'.$this->id.'_id"'.$class.' name="'.$this->name.'" value="'.$value.'" />';

		return $html;
	}
}
