<?php
/*------------------------------------------------------------------------
# com_authorlist - Author List
# ------------------------------------------------------------------------
# author    Joomla!Vargas
# copyright Copyright (C) 2012 joomla.vargas.co.cr. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://joomla.vargas.co.cr
# Technical Support:  Forum - http://joomla.vargas.co.cr/forum
-------------------------------------------------------------------------*/

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

class AuthorListController extends JController
{
	protected $default_view = 'authors';

	public function display($cachable = false, $urlparams = false)
	{
		require_once JPATH_COMPONENT.'/helpers/authorlist.php';

		// Load the submenu.
		AuthorListHelper::addSubmenu(JRequest::getCmd('view', 'authors'));

		$view	= JRequest::getCmd('view', 'authors');
		$layout = JRequest::getCmd('layout', 'default');
		$id		= JRequest::getInt('id');

		// Check for edit form.
		if ($view == 'author' && $layout == 'edit' && !$this->checkEditId('com_authorlist.edit.author', $id)) {

			// Somehow the person just went to the form - we don't allow that.
			$this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_UNHELD_ID', $id));
			$this->setMessage($this->getError(), 'error');
			$this->setRedirect(JRoute::_('index.php?option=com_authorlist&view=authors', false));

			return false;
		}

		parent::display();

		return $this;
	}
}
