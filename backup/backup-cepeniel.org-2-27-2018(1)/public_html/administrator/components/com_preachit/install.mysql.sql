CREATE TABLE IF NOT EXISTS `#__pibooks` (
  `id` int(11) NOT NULL auto_increment,
  `book_name` varchar(50) NOT NULL,
  `published` TINYINT( 3 ) NOT NULL DEFAULT '1',
  `display_name` varchar(50) NOT NULL,
  `shortform` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
);

INSERT IGNORE INTO `#__pibooks` (`id`, `book_name`, `display_name`, `shortform`)
VALUES ('1', 'Genesis', 'Genesis', 'Gen'),
('2', 'Exodus', 'Exodus', 'Exod'),
('3', 'Leviticus', 'Leviticus', 'Lev'),
('4', 'Numbers', 'Numbers', 'Num'),
('5', 'Deuteronomy', 'Deuteronomy', 'Deut'),
('6', 'Joshua', 'Joshua', 'Josh'),
('7', 'Judges', 'Judges', 'Judg'),
('8', 'Ruth', 'Ruth', 'Ruth'),
('9', '1 Samuel', '1 Samuel', '1Sam'),
('10', '2 Samuel', '2 Samuel', '2Sam'),
('11', '1 Kings', '1 Kings', '1Kgs'),
('12', '2 Kings', '2 Kings', '2Kgs'),
('13', '1 Chronicles', '1 Chronicles', '1Chr'),
('14', '2 Chronicles', '2 Chronicles', '2Chr'),
('15', 'Ezra', 'Ezra', 'Ezra'),
('16', 'Nehemiah', 'Nehemiah', 'Neh'),
('17', 'Esther', 'Esther', 'Esth'),
('18', 'Job', 'Job', 'Job'),
('19', 'Psalm', 'Psalm', 'Ps'),
('20', 'Proverbs',' Proverbs', 'Prov'),
('21', 'Ecclesiastes', 'Ecclesiastes', 'Eccl'),
('22', 'Song of Songs', 'Song of Songs', 'Song'),
('23', 'Isaiah', 'Isaiah', 'Isa'),
('24', 'Jeremiah', 'Jeremiah', 'Jer'),
('25', 'Lamentations', 'Lamentations', 'Lam'),
('26', 'Ezekiel', 'Ezekiel', 'Ezek'),
('27', 'Daniel', 'Daniel', 'Dan'),
('28', 'Hosea', 'Hosea', 'Hos'),
('29', 'Joel', 'Joel', 'Joel'),
('30', 'Amos', 'Amos', 'Amos'),
('31', 'Obadiah', 'Obadiah', 'Obad'),
('32', 'Jonah', 'Jonah', 'Jonah'),
('33', 'Micah', 'Micah', 'Mic'),
('34', 'Nahum', 'Nahum', 'Nah'),
('35', 'Habakkuk', 'Habakkuk', 'Hab'),
('36', 'Zephaniah', 'Zephaniah', 'Zeph'),
('37', 'Haggai', 'Haggai', 'Hag'),
('38', 'Zechariah', 'Zechariah', 'Zech'),
('39', 'Malachi', 'Malachi', 'Mal'),
('40', 'Matthew', 'Matthew', 'Matt'),
('41', 'Mark', 'Mark', 'Mark'),
('42', 'Luke', 'Luke', 'Luke'),
('43', 'John', 'John', 'John'),
('44', 'Acts', 'Acts', 'Acts'),
('45', 'Romans', 'Romans', 'Rom'),
('46', '1 Corinthians', '1 Corinthians', '1Cor'),
('47', '2 Corinthians', '2 Corinthians', '2Cor'),
('48', 'Galatians', 'Galatians', 'Gal'),
('49', 'Ephesians', 'Ephesians', 'Eph'),
('50', 'Philippians', 'Philippians', 'Phil'),
('51', 'Colossians', 'Colossians', 'Col'),
('52', '1 Thessalonians', '1 Thessalonians', '1Thess'),
('53', '2 Thessalonians', '2 Thessalonians', '2Thess'),
('54', '1 Timothy', '1 Timothy', '1Tim'),
('55', '2 Timothy', '2 Timothy', '2Tim'),
('56', 'Titus', 'Titus', 'Titus'),
('57', 'Philemon', 'Philemon', 'Phlm'),
('58', 'Hebrews', 'Hebrews', 'Heb'),
('59', 'James', 'James', 'Jas'),
('60', '1 Peter', '1 Peter', '1Pet'),
('61', '2 Peter', '2 Peter', '2Pet'),
('62', '1 John', '1 John', '1John'),
('63', '2 John', '2 John', '2John'),
('64', '3 John', '3 John', '3John'),
('65', 'Jude', 'Jude', 'Jude'),
('66', 'Revelation', 'Revelation', 'Rev'),
('67', 'Topical', 'Topical', 'top')
;

CREATE TABLE IF NOT EXISTS `#__picomments` (
  `id` int(11) NOT NULL auto_increment,
  `study_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `full_name` varchar(50) NOT NULL,
  `comment_date` datetime NOT NULL,
  `comment_text` text NOT NULL,
  `published` TINYINT( 3 ) NOT NULL DEFAULT '1',
  `email` varchar(250) NOT NULL,
  PRIMARY KEY  (`id`)
);

CREATE TABLE IF NOT EXISTS `#__pifilepath` (
  `id` int(11) NOT NULL auto_increment,
  `server` varchar(100) NOT NULL,
  `folder` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  `published` tinyint(3) NOT NULL,
  `name` varchar(40) NOT NULL,
  `type` TINYINT(3) NOT NULL,
  `ftphost` VARCHAR(100) NOT NULL,
  `ftpuser` VARCHAR(250) NOT NULL,
  `ftppassword` VARCHAR(250) NOT NULL,
  `ftpport` VARCHAR(10) NOT NULL,
  `aws_key` VARCHAR(100) NOT NULL,
  `aws_secret` VARCHAR(100) NOT NULL,
  PRIMARY KEY  (`id`)
);

CREATE TABLE IF NOT EXISTS `#__pimime` (
  `id` int(11) NOT NULL auto_increment,
  `extension` varchar(10) NOT NULL,
  `mediatype` varchar(20) NOT NULL,
  `published` TINYINT( 3 ) NOT NULL DEFAULT '1',
  PRIMARY KEY  (`id`)
);


INSERT IGNORE INTO `#__pimime` (`id`, `extension`, `mediatype`)
VALUES ('1', 'mp3', 'audio/mpeg'),
('2', 'mp4', 'video/mpeg'),
('3', 'ra', 'audio/x-pn-realaudio'),
('4', 'm4v', 'video/x-m4v'),
('5', 'rm', 'application/vnd.rn-r'),
('6', 'wma', 'audio/x-ms-wma'),
('7', 'wav', 'audio/x-wav'),
('8', 'rpm', 'audio/x-pn-realaudio'),
('9', 'rm', 'audio/x-pn-realaudio'),
('10', 'ram', 'audio/x-pn-realaudio'),
('11', 'mpg', 'video/mpeg'),
('12', 'mp2', 'video/mpeg'),
('13', 'avi', 'video/x-msvideo'),
('14', 'flv', 'video/x-flv .flv');


CREATE TABLE IF NOT EXISTS `#__pipodcast` (
  `id` int(11) NOT NULL auto_increment,
  `published` tinyint(3) NOT NULL,
  `name` varchar(50) NOT NULL,
  `records` int(3) NOT NULL,
  `website` varchar(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `image` varchar(100) NOT NULL,
  `imagehgt` int(4) NOT NULL,
  `imagewth` int(4) NOT NULL,
  `author` varchar(50) NOT NULL,
  `search` varchar(200) NOT NULL,
  `filename` varchar(50) NOT NULL,
  `menu_item` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `editor` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `ordering` int(11) NOT NULL,
  `podpub` datetime NOT NULL,
  `itunestitle` int(11) NOT NULL,
  `itunessub` int(11) NOT NULL,
  `itunesdesc` int(11) NOT NULL,
  `series` tinyint(3) NOT NULL,
  `series_list` text NOT NULL,
  `ministry` tinyint(3) NOT NULL,
  `ministry_list` text NOT NULL,
  `teacher` tinyint(3) NOT NULL,
  `teacher_list` text NOT NULL,
  `media` tinyint(3) NOT NULL,
  `media_list` text NOT NULL,
  `languagesel` char(7) NOT NULL,
  PRIMARY KEY  (`id`)
);

CREATE TABLE IF NOT EXISTS `#__piseries` (
  `id` int(11) NOT NULL auto_increment,
  `series_name` varchar(250) NOT NULL,
  `series_alias` varchar(250) NOT NULL,
  `image_folderlrg` int(11) NOT NULL,
  `series_image_lrg` varchar(250) NOT NULL,
  `series_description` text NOT NULL,
  `ministry` text NOT NULL,
  `ordering` int(11) NOT NULL,
  `published` tinyint(3) NOT NULL,
  `featured` tinyint(3) NOT NULL,
  `introvideo` tinyint(3) NOT NULL,
  `videoplayer` int(11) NOT NULL,
  `videofolder` int(11) NOT NULL,
  `videolink` varchar(250) NOT NULL,
  `vheight` int(4) NOT NULL,
  `vwidth` int(4) NOT NULL,
  `checked_out` INT(10),
  `checked_out_time` datetime,
  `user` int(11) NOT NULL,
  `language` char(7) NOT NULL,
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `part` tinyint(3) NOT NULL,
  `access` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY  (`id`)
);

CREATE TABLE IF NOT EXISTS `#__piministry` (
  `id` int(11) NOT NULL auto_increment,
  `ministry_name` varchar(250) NOT NULL,
  `ministry_alias` varchar(250) NOT NULL,
  `image_folderlrg` int(11) NOT NULL,
  `ministry_image_lrg` varchar(250) NOT NULL,
  `ministry_description` text NOT NULL,
  `ordering` int(11) NOT NULL,
  `published` tinyint(3) NOT NULL,
  `featured` tinyint(3) NOT NULL,
  `access` int(11) NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL,
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  PRIMARY KEY  (`id`)
);

CREATE TABLE IF NOT EXISTS `#__piteachers` (
  `id` int(11) NOT NULL auto_increment,
  `teacher_name` varchar(250) NOT NULL,
  `lastname` varchar (250) NOT NULL,
  `teacher_alias` varchar(250) NOT NULL,
  `teacher_role` varchar(50) NOT NULL,
  `image_folderlrg` int(11) NOT NULL,
  `teacher_image_lrg` varchar(250) NOT NULL,
  `teacher_email` varchar(100) NOT NULL,
  `teacher_website` varchar(250) NOT NULL,
  `teacher_description` text NOT NULL,
  `published` tinyint(3) NOT NULL,
  `featured` tinyint(3) NOT NULL,
  `ordering` int(11) NOT NULL,
  `teacher_view` tinyint(3) NOT NULL,
  `checked_out` INT(10),
  `checked_out_time` datetime,
  `user` int(11) NOT NULL,
  `language` char(7) NOT NULL,
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  PRIMARY KEY  (`id`)
);

CREATE TABLE IF NOT EXISTS `#__pistudies` (
  `id` int(11) NOT NULL auto_increment,
  `study_date` datetime NOT NULL,
  `study_name` varchar(255) NOT NULL,
  `study_alias` varchar(250) NOT NULL,
  `study_description` text NOT NULL,
  `study_book` int(11) NOT NULL,
  `ref_ch_beg` int(4) NOT NULL,
  `ref_ch_end` int(4) NOT NULL,
  `ref_vs_beg` int(4) NOT NULL,
  `ref_vs_end` int(4) NOT NULL,
  `study_book2` int(11) NOT NULL,
  `ref_ch_beg2` int(4) NOT NULL,
  `ref_ch_end2` int(4) NOT NULL,
  `ref_vs_beg2` int(4) NOT NULL,
  `ref_vs_end2` int(4) NOT NULL,
  `series` int(11) NOT NULL,
  `ministry` text NOT NULL,
  `dur_hrs` int(4) NOT NULL,
  `dur_mins` int(4) NOT NULL,
  `dur_secs` int(4) NOT NULL,
  `video_type` int(11) NOT NULL,
  `video_link` varchar(250) NOT NULL,
  `vclip1_type` int(11) NOT NULL,
  `vclip1_link` varchar(250) NOT NULL,
  `vclip2_type` int(11) NOT NULL,
  `vclip2_link` varchar(250) NOT NULL,
  `vclip3_type` int(11) NOT NULL,
  `vclip3_link` varchar(250) NOT NULL,
  `video_download` tinyint(3) NOT NULL,
  `audio_type` int(11) NOT NULL,
  `audio_link` varchar(250) NOT NULL,
  `audio_download` tinyint(3) NOT NULL,
  `published` tinyint(3) NOT NULL,
  `featured` tinyint(3) NOT NULL,
  `comments` tinyint(3) NOT NULL,
  `study_text` mediumtext NOT NULL,
  `teacher` text NOT NULL,
  `audio_folder` int(11) NOT NULL,
  `video_folder` int(11) NOT NULL,
  `vclip1_folder` int(11) NOT NULL,
  `vclip2_folder` int(11) NOT NULL,
  `vclip3_folder` int(11) NOT NULL,
  `hits` int(11) NOT NULL DEFAULT '0',
  `downloads` int(11) NOT NULL DEFAULT '0',
  `asmedia` int(11) NOT NULL,
  `studylist` tinyint(3) NOT NULL,
  `publish_up` DATETIME NOT NULL ,
  `publish_down` DATETIME NOT NULL ,
  `image_folderlrg` int(11) NOT NULL,
  `imagelrg` VARCHAR( 250 ) NOT NULL ,
  `notes_folder` INT( 11 ) NOT NULL ,
  `notes` TINYINT( 3 ) NOT NULL ,
  `notes_link` VARCHAR( 250 ) NOT NULL ,
  `add_downloadvid` TINYINT( 3 ) NOT NULL ,
  `downloadvid_link` VARCHAR( 250 ) NOT NULL,
  `downloadvid_folder` INT(11) NOT NULL,
  `checked_out` INT(10),
  `checked_out_time` datetime,
  `user` int(11) NOT NULL,
  `access` int(11) NOT NULL DEFAULT '0',
  `saccess` int(11) NOT NULL DEFAULT '0',
  `minaccess` int(11) NOT NULL DEFAULT '0',
  `audpurchase` tinyint(3) NOT NULL,
  `audpurchase_folder` int(11) NOT NULL,
  `audpurchase_link` varchar(250) NOT NULL,
  `vidpurchase` tinyint(3) NOT NULL,
  `vidpurchase_folder` int(11) NOT NULL,
  `vidpurchase_link` varchar(250) NOT NULL,
  `tags` text NOT NULL,
  `audiofs` int(24) NOT NULL,
  `videofs` int(24) NOT NULL,
  `notesfs` int(24) NOT NULL,
  `slidesfs` int(24) NOT NULL,
  `advideofs` int(24) NOT NULL,
  `slides_folder` INT( 11 ) NOT NULL ,
  `slides` TINYINT( 3 ) NOT NULL ,
  `slides_link` VARCHAR( 250 ) NOT NULL ,
  `slides_type` TINYINT (3) NOT NULL,
  `language` char(7) NOT NULL,
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `podpublish_up` datetime NOT NULL,
  `podpublish_down` datetime NOT NULL,
  `audioprice` VARCHAR(250) NOT NULL,
  `videoprice` VARCHAR(250) NOT NULL,
  PRIMARY KEY  (`id`)
);

CREATE TABLE IF NOT EXISTS `#__pimediaplayers` (
  `id` int(11) NOT NULL auto_increment,
  `playername` varchar(250) NOT NULL,
  `playertype` int(11) NOT NULL,
  `playercode` mediumtext NOT NULL,
  `published` TINYINT( 3 ) NOT NULL DEFAULT '1',
  `vers` DECIMAL (5,2) NOT NULL,
  `playerscript` VARCHAR(250) NOT NULL,
  `html5` tinyint(3) NOT NULL,
  `facebook` tinyint(3) NOT NULL,
  `image` int(11) NOT NULL,
  `playerurl` VARCHAR(250) NOT NULL,
  `runplugins` tinyint(3) NOT NULL,
  PRIMARY KEY  (`id`)
);

INSERT IGNORE INTO `#__pimediaplayers` (`id`, `playername`, `playertype`, `playercode`, `vers`, `published`, `playerscript`, `html5`, `facebook`, `image`, `playerurl`, `runplugins`) VALUES
(1, 'JWplayer Video', 2, '<div class="localvideoplayer" id="player-div"><p id="localplayer[unique_id]"></p><script type="text/javascript">jwplayer("localplayer[unique_id]").setup({flashplayer: "[playerurl]",file: "[fileurl]",height: [height],width: [width],skin: "[skin]", controlbar: "bottom"});</script></div>', '3.55', 1, 'components/com_preachit/assets/mediaplayers/jwplayer.js', 1, 0, 0, '[root]/components/com_preachit/assets/mediaplayers/player.swf', 0),
(2, 'Vimeo', 2, '<div class="vimeoplayer">\r\n{ccconsent}<iframe src="http://player.vimeo.com/video/[fileid]?title=0&amp;byline=0&amp;portrait=0&amp;color=d11b24" width="[width]" height="[height]" frameborder="0"></iframe>{/ccconsent}\r\n</div>', '3.55', 1, '', 0, 1, 0, 'http://vimeo.com/moogaloop.swf?clip_id=[fileid]&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=1&amp;color=00ADEF&amp;fullscreen=1&amp;autoplay=0&amp;loop=0', 0),
(3, 'Youtube', 2, '<div class="youtubeplayer"><iframe width="[width]" height="[height]" src="http://www.youtube.com/embed/[fileid]?rel=0" frameborder="0" allowfullscreen></iframe></div>', '3.55', 1, '', 0, 0, 0, 'http://www.youtube.com/watch?v=[fileid]', 0),
(4, 'Blip tv', 2, '<div class="blipplayer">\r\n<embed src="[fileid]" type="application/x-shockwave-flash" width="[width]" height="[height]" allowscriptaccess="always" allowfullscreen="true"></embed>\r\n</div>', '3.55', 1, '', 0, 0, 0, '', 0),
(5, 'One pixel out', 1, '<div class="localaudioplayer"><script type="text/javascript">AudioPlayer.setup("[playerurl]", { width: [width],initialvolume: 100,transparentpagebg: "yes",left: "000000",lefticon: "FFFFFF"}); </script> <p id="audioplayer_[unique_id]">Alternative content</p><script type="text/javascript">  AudioPlayer.embed("audioplayer_[unique_id]", { soundFile: "[fileurl]",autostart: "no"});</script></div>', '3.55', 1, 'components/com_preachit/assets/mediaplayers/audio-player.js', 1, 0, 0, '[root]/components/com_preachit/assets/mediaplayers/pixelplayer.swf', 0),
(6, 'JWplayer Audio', 1, '<div class="localaudioplayer" id="player-div"><p id="localplayer[unique_id]"></p><script type="text/javascript">jwplayer("localplayer[unique_id]").setup({flashplayer: "[playerurl]",file: "[fileurl]",height: [height],width: [width],skin: "[skin]", controlbar: "bottom"});</script></div>', '3.55', 1, 'components/com_preachit/assets/mediaplayers/jwplayer.js', 1, 1, 0, '[root]/components/com_preachit/assets/mediaplayers/player.swf', 0),
(7, 'Flowplayer', 2, '<div class="localvideoplayer" id="player-div"><div style="width:[width]px;height:[height]px;" id="player[unique_id]"></div><!-- this script block will install Flowplayer inside previous DIV tag --><script language="JavaScript">flowplayer("player[unique_id]","[playerurl]","[fileurl]");</script></div>', '3.55', 1, 'components/com_preachit/assets/mediaplayers/flowplayer-3.2.2.min.js', 1, 0, 0, '[root]/components/com_preachit/assets/mediaplayers/flowplayer-3.2.2.swf', 0);

CREATE TABLE IF NOT EXISTS `#__pibckadmin` (
  `id` int(11) NOT NULL,
  `tableversion` text NOT NULL,
  `curversion` text NOT NULL,
  `varsioncheck` datetime NOT NULL,
  PRIMARY KEY  (`id`)
);

CREATE TABLE IF NOT EXISTS `#__pitemplates` (
  `id` int(11) NOT NULL auto_increment,
  `messortlists` text NOT NULL,
  `messagelist` text NOT NULL,
  `audio` text NOT NULL,
  `audiopopup` text NOT NULL,
  `video` text NOT NULL,
  `videopopup` text NOT NULL,
  `text` text NOT NULL,
  `seriesheader` text NOT NULL,
  `serieslist` text NOT NULL,
  `series` text NOT NULL,
  `seriessermons` text NOT NULL,
  `teacherheader` text NOT NULL,
  `teacherlist` text NOT NULL,
  `teacher` text NOT NULL,
  `teachersermons` text NOT NULL,
  `ministryheader` text NOT NULL,
  `ministrylist` text NOT NULL,
  `ministry` text NOT NULL,
  `ministryseries` text NOT NULL,
  `taglist` text NOT NULL,
  PRIMARY KEY  (`id`)
); 

INSERT IGNORE INTO `#__pitemplates` (`id`, `messortlists`, `messagelist`, 
`audio`, `audiopopup`, `video`, `videopopup`, `text`, `serieslist`, `series`, `seriessermons`, `teacherlist`, `teacher`, `teachersermons`, `ministrylist`, `ministry`, `ministryseries`)
VALUES (
'1',
'<div class="headblock">
<div class="sortlistblock">
<span class="filtertext">Filter Media by:</span>
<span class="sortlists">[booklist]</span>
<span class="sortlists">[teacherlist]</span>
<span class="sortlists">[serieslist]</span>
<span class="sortlists">[yearlist]</span>
</div>
</div>',
'<div class="listblock">
<div class="date">[date]</div>
<div class="medialinks">[medialinks]</div>
<div class="study_name">[name]</div>
<div class="scripture">Passage: [scripture][scripture2]</div>
<div class="additional"><span class="teacher">Teacher: [teacher]</span><span class="series">Series: [series]</span><span class="duration">Duration: [duration]</span></div>
</div>', '<div class="topbar"><div class="study_name"><div class="date">[date]</div>
[name]</div>
<div class="subtitle">[scripture][scripture2] by [teacher]</div>
<div class="study_description">[description]</div>
<div class="series">[series]</div>
[audio]
<div class="medialinks">[medialinks]</div>
<div class="share">[share]</div>
</div>
[backlink]', '<div class="detailscontainer"><div class="topbar"><div class="study_name"><div class="date">[date]</div>
[name]</div>
<div class="subtitle">[scripture][scripture2] by [teacher]
Series: [series]
</div></div>
[audio]
div class="study_description">[description]</div>
<div class="links"
<div class="medialinks">[medialinks]</div>
<div class="share">[share]</div>
 </div></div>', '<div class="topbar"><div class="study_name"><div class="date">[date]</div>
[name]</div>
<div class="subtitle">[scripture][scripture2] by [teacher]</div></div>
[video]
<div class="study_description">[description]</div>
<div class="series">Series: [series]</div>
<div class="medialinks">
<div class="share">[share]</div>
[medialinks]</div>
[backlink]',
'<div class="detailscontainer">
<div class="topbar"><div class="study_name">[date]</div>
[name]</div>
<div class="subtitle"><div class="series">Series: [series]</div>[scripture][scripture2] by [teacher]</div></div>
[video]
<div class="study_description">[description]</div>
<div class="links"><div class="medialinks">[medialinks]</div>
<div class="share">[share]</div>
</div>
</div>',
'<div class="topbar"><div class="study_name"><div class="date">[date]</div>
[name]</div>
<div class="subtitle">[scripture][scripture2] by [teacher]</div></div>
<div class="study_description">[description]</div>
<div class="series">Series: [series]</div>
<div class="medialinks">
<div class="share">[share]</div>
[medialinks]</div>

<div class="study_text">[text]</div>

<div class="medialinks">
<div class="share">[share]</div>
[medialinks]</div>

[backlink]',
'<div class="listblock">
<table width="100%">
<tr>
<td>[simagesm]
<div class="seriesname">[series]</div>
<div class="seriesdescription">[seriesdescription]</div>
</td>
</tr>
</table>
</div>',
'<table width="100%">
<tr>
<td>
[simagelrg]
<div class="seriesname">[series]</div>
<div class="seriesdescription">[seriesdescription]</div>
</td>
</tr>
</table>',
'<div class="listblock">
<div class="medialinks">[medialinks]</div>
<div class="study_name">[name]</div>
<div class="scripture">Passage: [scripture][scripture2]<span class="date">[date]</span></div>
<div class="teacher">Teacher: [teacher]</div>
</div>',
'<div class="listblock">
<table width="100%">
<tr>
<td>[timagesm]
<div class="teachername">[teacher]</div>
<div class="teacherrole">[role]</div>
<div class="teacherdescription">[teacherdescription]</div>
</td>
</tr>
</table>
</div>',
'<table width="100%">
<tr>
<td>
[timagelrg]
<div class="teachername">[teacher]</div>
<div class="teacherrole">Role: [role]</div>
<div class="teacherweb">Web: [web]</div></td>
</tr>
</table>
<div class="teacherdescription">[teacherdescription]</div>',
'<div class="listblock">
<div class="medialinks">[medialinks]</div>
<div class="study_name">[name]</div>
<div class="date">[date]<span class="scripture">Passage: [scripture][scripture2]</span></div>
</div>',
'<div class="listblock">
<table width="100%">
<tr>
<td>[mimagesm]
<div class="ministryname">[ministry]</div>
<div class="ministrydescription">[ministrydescription]</div>
</td>
</tr>
</table>
</div>',
'<table width="100%">
<tr>
<td>[mimagelrg]
<div class="ministryname">[ministry]</div>
<div class="ministrydescription">[ministrydescription]</div>
</td>
</tr>
</table>',
'<div class="listblock">
<table width="100%">
<tr>
<td>[simagesm]
<div class="seriesname">[series]</div>
<div class="seriesdescription">[seriesdescription]</div>
</td>
</tr>
</table>
</div>');

CREATE TABLE IF NOT EXISTS `#__pitemplate` (
  `id` int(11) NOT NULL auto_increment,
  `template` varchar(50) NOT NULL,
  `client_id` tinyint(3) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `params` text NOT NULL,
  `def` tinyint(3) NOT NULL,
  `cssoverride` text NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `#__pishare` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(250) NOT NULL,
  `code` text NOT NULL,
  `published` tinyint(3) NOT NULL DEFAULT '1',
  `script` varchar(250) NOT NULL,
  `ordering` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
);

INSERT IGNORE INTO `#__pishare` (`id`, `name`, `code`, `published`, `ordering`) VALUES
(1, 'Addthis', '<!-- AddThis Button begin --> <div class="addthis_toolbox addthis_default_style"><a class="addthis_button_compact at300m" href="http://www.addthis.com/bookmark.php" addthis:description="[sharedescription]" addthis:title="[pagetitle]" addthis:url="[shareurl]" addthis:ui_click="true">[sharetext]</a><div class="atclear"></div></div> {ccconsent}<script src="http://s7.addthis.com/js/250/addthis_widget.js#username=xa-4bf7f9c3283266f1" type="text/javascript"></script>{/ccconsent} <!-- AddThis Button END -->', 1, 1);

CREATE TABLE IF NOT EXISTS `#__pibiblevers` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  `code` varchar(20) NOT NULL,
  `published` TINYINT( 3 ) NOT NULL DEFAULT '1',
  PRIMARY KEY  (`id`)
);

INSERT IGNORE INTO `#__pibiblevers` (`id`, `name`, `code`)
VALUES ('1', 'New International Version', 'NIV'),
('2', 'New International Version UK', 'NIVUK'),
('3', 'Contemporary English Version', 'CEV'),
('4', 'English Standard Version', 'ESV'),
('5', 'New King James Version', 'NKJV'),
('6', 'New American Standard Version', 'NASB'),
('7', 'New Living Translation', 'NLT'),
('8', 'The Message', 'MSG'),
('9', 'Todays New International Version', 'TNIV'),
('10', 'Louis Segond', 'LSG'),
('11', 'La Bible du Semeur', 'BDS'),
('12', 'Nueva Version Internacional', 'NVI');
