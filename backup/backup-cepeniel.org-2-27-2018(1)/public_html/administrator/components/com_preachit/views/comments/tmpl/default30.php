<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$option = JRequest::getCmd('option');
$user	= JFactory::getUser();
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');
$listOrder    = $this->escape($this->state->get('list.ordering'));
$listDirn    = $this->escape($this->state->get('list.direction'));
$trashed    = $this->state->get('filter.state') == -2 ? true : false;
?>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<?php if(!empty( $this->sidebar)): ?>
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
    <div id="j-main-container" class="span10">
<?php else : ?>
    <div id="j-main-container">
<?php endif;?>
    <div id="filter-bar" class="btn-toolbar">
        <div class="btn-group pull-right hidden-phone">
                    <label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?></label>
                    <?php echo $this->pagination->getLimitBox(); ?>
        </div>
    </div>
<table class="adminlist table table-striped" id="articleList">

    <thead>
        <tr>
            <th width="1%" class="hidden-phone">
                <input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
            </th>
            <th width="1%" class="nowrap center">
                <?php echo JHtml::_('grid.sort', 'JSTATUS', 'published', $listDirn, $listOrder); ?>
            </th>
            <th>
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_DATELIST', 'comment_date', $listDirn, $listOrder); ?>
            </th>
            <th width="300px">
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_STUDYTITLELIST', 'b.study_name', $listDirn, $listOrder); ?>
            </th>
            <th width="15%" class="hidden-phone">
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_USERLIST', 'full_name', $listDirn, $listOrder); ?>
            </th>
            <th width="30%" class="hidden-phone">
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_COMMENTLIST', 'comment_text', $listDirn, $listOrder); ?>
            </th>
            <th width="1%" class="nowrap center hidden-phone">
                        <?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'id', $listDirn, $listOrder); ?>
            </th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <td colspan="4"> <?php echo $this->pagination->getListFooter(); ?> </td>
        </tr>
    </tfoot>
    <tbody>
        <?php
        foreach ($this->rows as $i => $item)
        {
        $ordering  = ($listOrder == 'ordering');
        $canCreate  = $user->authorise('core.create', 'com_preachit');
        $canEdit    = $user->authorise('core.edit', 'com_preachit');
        $canCheckin = $user->authorise('core.manage',     'com_checkin') || $item->checked_out == $userId || $item->checked_out == 0;
        $canChange  = $user->authorise('core.edit.state', 'com_preachit') && $canCheckin;
        //get study name
            $db = JFactory::getDBO();
            $query = "
              SELECT ".$db->quoteName('study_name')."
                FROM ".$db->quoteName('#__pistudies')."
                WHERE ".$db->quoteName('id')." = ".$db->quote($item->study_id).";
              ";
            $db->setQuery($query);
            $studyname = $db->loadResult();
            // get checked out name

	$query = "SELECT ".$db->quoteName('name')."
    FROM ".$db->quoteName('#__users')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($item->checked_out).";
  ";
	$db->setQuery($query);
	$editor = $db->loadResult();
        ?>
            <tr class="row<?php echo $i % 2; ?>" sortable-group-id="0">
                <td class="center hidden-phone">
                    <?php echo JHtml::_('grid.id', $i, $item->id); ?>
                </td>
                <td class="center">
                    <?php echo JHtml::_('jgrid.published', $item->published, $i, 'comments.', $canChange, 'cb'); ?>
                </td>
                <td class="nowrap has-context">
                    <div class="pull-left">
                        <?php if ($item->checked_out) : ?>
                                <?php echo JHtml::_('jgrid.checkedout', $i, $editor, $item->checked_out_time, 'comments.', $canCheckin); ?>
                            <?php endif; ?>
                        <?php if ($canEdit) : ?>
                            <a href="<?php echo JRoute::_('index.php?option=com_preachit&task=comment.edit&id='.(int) $item->id); ?>">
                                <?php echo $this->escape($item->comment_date); ?></a>
                        <?php else : ?>
                            <?php echo $this->escape($item->comment_date); ?>
                        <?php endif; ?>
                    </div>
                    <div class="pull-left">
                        <?php
                            // Create dropdown items
                            JHtml::_('dropdown.edit', $item->id, 'biblever.');
                            JHtml::_('dropdown.divider');
                            if ($item->published) :
                                JHtml::_('dropdown.unpublish', 'cb' . $i, 'biblevers.');
                            else :
                                JHtml::_('dropdown.publish', 'cb' . $i, 'biblevers.');
                            endif;
                                JHtml::_('dropdown.divider');
                            if ($trashed) :
                                JHtml::_('dropdown.untrash', 'cb' . $i, 'biblevers.');
                            else :
                                JHtml::_('dropdown.trash', 'cb' . $i, 'biblevers.');
                            endif;

                            // render dropdown list
                            echo JHtml::_('dropdown.render');
                        ?>
                    </div>
                </td>
                <td>
                    <?php echo $studyname ?>
                </td>
                <td class="hidden-phone">
                    <?php echo $item->full_name ?>
                </td>
                <td class="hidden-phone">
                    <?php echo JString::substr($item->comment_text, 0, 149); ?>
                </td>
                <td class="center hidden-phone">
                        <?php echo $item->id; ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<!-- /Footer -->
<?php echo JHTML::_( 'form.token' ); ?>
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="view" value="comments" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
</div>
<div class="clearfix"> </div>
<div style="text-align: center; padding-top: 5px;">
<?php $abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/lib/preachit.footer.php');?>
<?php echo PIfooter::footer();?></div>
</form>