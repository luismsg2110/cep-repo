<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
class PreachitViewMinistries extends JViewLegacy
{
function display($tpl = null)
{
$document = JFactory::getDocument();
$document->addStyleSheet('../media/preachit/css/preachit.css');

$app = JFactory::getApplication();
$option = JRequest::getCmd('option');

// get Joomla version to decide which form and method

$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/additional.php');
$this->state  = $this->get('State');
$this->rows = $this->get('data');
$this->pagination = $this->get('Pagination');

// load menu
PreachitHelper::addSubmenu('ministries');

$this->addToolbar();

if (Tewebcheck::getJversion() >= 3.0)
{
    $layout = JRequest::getVar('layout', '');
    if ($layout == 'modal')
    {$this->setLayout('modal30');}
    else {$this->setLayout('default30');}
    $this->sidebar = JHtmlSidebar::render();   
}

parent::display($tpl);
}

/**
     * Add the page title and toolbar.
     *
     * @since    1.6
     */
protected function addToolbar()
{
    $user    = JFactory::getUser();    
    JToolBarHelper::title( JText::_( 'COM_PREACHIT_ADMIN_TITLE_MINISTRY' ), 'ministry.png');
    if ($user->authorise('core.create', 'com_preachit'))  {
    JToolBarHelper::addNew('ministry.add','JTOOLBAR_NEW');}
    if ($user->authorise('core.edit', 'com_preachit'))  {
    JToolBarHelper::editList('ministry.edit','JTOOLBAR_EDIT');
    }
    JToolBarHelper::divider();
    if ($user->authorise('core.edit.state', 'com_preachit')) 
    {
    JToolBarHelper::custom('ministries.publish', 'publish.png', 'publish_f2.png','JTOOLBAR_PUBLISH', true);
    JToolBarHelper::custom('ministries.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
    JToolbarHelper::custom('ministries.featured', 'featured.png', 'featured_f2.png', 'JFEATURED', true);
    }
    JToolBarHelper::divider();
    if ($this->state->get('filter.state') == -2 && $user->authorise('core.delete', 'com_preachit')) {
                JToolBarHelper::deleteList('', 'ministries.delete','TE_TOOLBAR_EMPTY_TRASH');
            } else if ($user->authorise('core.edit.state', 'com_preachit')) {
                JToolBarHelper::trash('ministries.trash','TE_TOOLBAR_TRASH');
            }
    JToolBarHelper::divider();
    if ($user->authorise('core.admin', 'com_preachit'))  {
    JToolBarHelper::preferences('com_preachit', '550', '900');
    }
    JToolBarHelper::help('pihelp', 'com_preachit');
    
    if (Tewebcheck::getJversion() >= 3.0)
    {
        $selectstate = Tewebdetails::stateselector();
        
        JHtmlSidebar::setAction('index.php?option=com_preachit&view=ministries');

        JHtmlSidebar::addFilter(
            JText::_('JOPTION_SELECT_PUBLISHED'),
            'filter_statemin',
            JHtml::_('select.options', $selectstate, 'value', 'text', $this->state->get('filter.state'), true)
        );
        
        JHtmlSidebar::addFilter(
            JText::_('JOPTION_SELECT_LANGUAGE'),
            'filter_language',
            JHtml::_('select.options', JHtml::_('contentlanguage.existing', true, true), 'value', 'text', $this->state->get('filter.language'))
        );
    }
    else {
        $selectstate = Tewebdetails::stateselector();
        $state_list = JHTML::_('select.genericList', $selectstate, 'filter_statemin', 'class="inputbox" size="1" onchange="this.form.submit()"', 'value', 'text', $this->state->get('filter.state') );
        $this->assignRef('state_list', $state_list);
        // language list
        jimport( 'joomla.language.helper' );
        $langlist = JHtml::_('contentlanguage.existing', true, true);
        $defaultlang = array(
        array('value' => '', 'text' => JText::_('JOPTION_SELECT_LANGUAGE')),
        );
        $langlist = array_merge( $defaultlang, $langlist );
        $lang_list = JHTML::_('select.genericList', $langlist, 'filter_language', 'class="inputbox" size="1" onchange="this.form.submit()"', 'value', 'text', $this->state->get('filter.language') );
        $this->assignRef('lang_list', $lang_list);
    }
}

/**
     * Returns an array of fields the table can be sorted by
     *
     * @return  array  Array containing the field name to sort by as the key and display text as value
     *
     * @since   3.0
     */
    protected function getSortFields()
    {
        return array(
            'ordering' => JText::_('JGRID_HEADING_ORDERING'),
            'published' => JText::_('JSTATUS'),
            'id' => JText::_('JGRID_HEADING_ID'),
            'ministry_name' => JText::_('COM_PREACHIT_ADMIN_NAMELIST'),
            'featured' => JText::_('JFEATURED')
        );
    }

}