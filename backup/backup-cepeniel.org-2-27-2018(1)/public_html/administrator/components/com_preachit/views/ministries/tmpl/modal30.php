<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$option = JRequest::getCmd('option');
$function = JRequest::getVar('function', 'jSelectChart');
JHTML::_( 'behavior.framework' );
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');
$user        = JFactory::getUser();
$userId        = $user->get('id');
$listOrder    = $this->escape($this->state->get('list.ordering'));
$listDirn    = $this->escape($this->state->get('list.direction'));
?>
<form action="index.php" method="post" name="adminForm" id="adminForm">
    <fieldset class="filter clearfix">
        <div class="filters">
            <select name="filter_published" class="input-medium" onchange="this.form.submit()">
                <option value=""><?php echo JText::_('JOPTION_SELECT_PUBLISHED');?></option>
                <?php echo JHtml::_('select.options', Tewebdetails::stateselector(), 'value', 'text', $this->state->get('filter.state'), true);?>
            </select>
            <select name="filter_language" class="input-medium" onchange="this.form.submit()">
                <option value=""><?php echo JText::_('JOPTION_SELECT_LANGUAGE');?></option>
                <?php echo JHtml::_('select.options', JHtml::_('contentlanguage.existing', true, true), 'value', 'text', $this->state->get('filter.language'));?>
            </select>
        </div>
    </fieldset>
    <table class="adminlist table table-striped" id="articleList">
        <thead>
            <tr>
                <th width="1%" class="nowrap center">
                    <?php echo JHtml::_('grid.sort', 'JSTATUS', 'published', $listDirn, $listOrder); ?>
                </th>
                <th>
                    <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_NAMELIST', 'ministry_name', $listDirn, $listOrder); ?>
                </th>
                <th width="45%" class="hidden-phone">
                    <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_DESCRIPTIONLIST', 'ministry_description', $listDirn, $listOrder); ?>
                </th>
                <th width="10%" class="nowrap hidden-phone">
                    <?php echo JHtml::_('grid.sort', 'JGRID_HEADING_LANGUAGE', 'language', $listDirn, $listOrder); ?>
                </th>
                <th width="1%" class="nowrap center hidden-phone">
                            <?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'id', $listDirn, $listOrder); ?>
                </th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td colspan="6"> <?php echo $this->pagination->getListFooter(); ?> </td>
            </tr>
        </tfoot>
        <tbody>
            <?php
            foreach ($this->rows as $i => $item)
            {
            $ordering  = ($listOrder == 'ordering');
            $canCreate  = $user->authorise('core.create', 'com_preachit');
            $canEdit    = $user->authorise('core.edit', 'com_preachit');
            $canCheckin = true; //$user->authorise('core.manage',     'com_checkin') || $item->checked_out == $userId || $item->checked_out == 0;
            $canChange  = $user->authorise('core.edit.state', 'com_preachit') && $canCheckin;

            // get language title

            $db = JFactory::getDBO();
                $query = "SELECT ".$db->quoteName('title')."
                FROM ".$db->quoteName('#__languages')."
                WHERE ".$db->quoteName('lang_code')." = ".$db->quote($item->language).";
              ";
                $db->setQuery($query);
                $language_title = $db->loadResult();

            ?>
                <tr class="row<?php echo $i % 2; ?>" sortable-group-id="0">
                    <td class="center">
                        <?php echo JHtml::_('jgrid.published', $item->published, $i, 'ministries.', $canChange, 'cb'); ?>
                    </td>
                    <td class="nowrap has-context">
                        <div class="pull-left">
                                <a class="pointer" href="#" onclick="if (window.parent) window.parent.<?php echo $function;?>('<?php echo (int) $item->id; ?>', '<?php echo addslashes($item->ministry_name); ?>');"> 
                                    <?php echo $this->escape($item->ministry_name); ?></a>
                        </div>
                    </td>
                    <td class="hidden-phone">
                        <?php echo JString::substr($item->ministry_description, 0, 149); ?>
                    </td>
                    <td class="small center hidden-phone">
                        <?php if ($item->language == '*'):?>
                            <?php echo JText::alt('JALL', 'language'); ?>
                        <?php else:?>
                            <?php echo $language_title ? $this->escape($language_title) : JText::_('JUNDEFINED'); ?>
                        <?php endif;?>
                    </td>
                    <td class="center hidden-phone">
                            <?php echo $item->id; ?>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
<?php echo JHTML::_( 'form.token' ); ?>
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="view" value="ministries" />
<input type="hidden" name="tmpl" value="component" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="layout" value="modal" />
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
</form>