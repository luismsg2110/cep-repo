<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$user	= JFactory::getUser();
$option = JRequest::getCmd('option');
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');
$user        = JFactory::getUser();
$userId        = $user->get('id');
$listOrder    = $this->escape($this->state->get('list.ordering'));
$listDirn    = $this->escape($this->state->get('list.direction'));
$canOrder    = $user->authorise('core.edit.state', 'com_preachit');
$saveOrder    = $listOrder == 'ordering';
if ($saveOrder)
{
    $saveOrderingUrl = 'index.php?option=com_preachit&task=sery.saveOrderAjax&tmpl=component';
    JHtml::_('sortablelist.sortable', 'articleList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
$trashed    = $this->state->get('filter.state') == -2 ? true : false;
$sortFields = $this->getSortFields();
?>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<?php if(!empty( $this->sidebar)): ?>
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
    <div id="j-main-container" class="span10">
<?php else : ?>
    <div id="j-main-container">
<?php endif;?>
    <div id="filter-bar" class="btn-toolbar">
        <div class="btn-group pull-right hidden-phone">
                    <label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?></label>
                    <?php echo $this->pagination->getLimitBox(); ?>
        </div>
    </div>
<table class="adminlist table table-striped" id="articleList">

    <thead>
        <tr>
            <th width="1%" class="nowrap center hidden-phone">
                <?php echo JHtml::_('grid.sort', '<i class="icon-menu-2"></i>', 'ordering', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING'); ?>
            </th>
            <th width="1%" class="hidden-phone">
                <input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
            </th>
            <th width="1%" style="min-width:55px" class="nowrap center">
                <?php echo JHtml::_('grid.sort', 'JSTATUS', 'published', $listDirn, $listOrder); ?>
            </th>
            <th>
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_NAMELIST', 'series_name', $listDirn, $listOrder); ?>
            </th>
            <th width="45%" class="hidden-phone">
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_DESCRIPTIONLIST', 'series_description', $listDirn, $listOrder); ?>
            </th>
            <th width="10%" class="nowrap hidden-phone">
                <?php echo JHtml::_('grid.sort', 'JGRID_HEADING_LANGUAGE', 'language', $listDirn, $listOrder); ?>
            </th>
            <th width="1%" class="nowrap center hidden-phone">
                        <?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'id', $listDirn, $listOrder); ?>
            </th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <td colspan="6"> <?php echo $this->pagination->getListFooter(); ?> </td>
        </tr>
    </tfoot>
    <tbody>
        <?php
        foreach ($this->rows as $i => $item)
        {
        $ordering  = ($listOrder == 'ordering');
        $canCreate  = $user->authorise('core.create', 'com_preachit');
        $canEdit    = $user->authorise('core.edit', 'com_preachit');
        $canCheckin = $user->authorise('core.manage',     'com_checkin') || $item->checked_out == $userId || $item->checked_out == 0;
        $canChange  = $user->authorise('core.edit.state', 'com_preachit') && $canCheckin;
        
        // get checked out name
$db = JFactory::getDBO();
	$query = "SELECT ".$db->quoteName('name')."
    FROM ".$db->quoteName('#__users')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($item->checked_out).";
  ";
	$db->setQuery($query);
	$editor = $db->loadResult();

        // get language title

            $query = "SELECT ".$db->quoteName('title')."
            FROM ".$db->quoteName('#__languages')."
            WHERE ".$db->quoteName('lang_code')." = ".$db->quote($item->language).";
          ";
            $db->setQuery($query);
            $language_title = $db->loadResult();

        ?>
            <tr class="row<?php echo $i % 2; ?>" sortable-group-id="0">
                <td class="order nowrap center hidden-phone">
                    <?php if ($canChange) :
                        $disableClassName = '';
                        $disabledLabel      = '';
                        if (!$saveOrder) :
                            $disabledLabel    = JText::_('JORDERINGDISABLED');
                            $disableClassName = 'inactive tip-top';
                        endif; ?>
                        <span class="sortable-handler hasTooltip <?php echo $disableClassName?>" title="<?php echo $disabledLabel?>">
                            <i class="icon-menu"></i>
                        </span>
                        <input type="text" style="display:none" name="order[]" size="5"
                            value="<?php echo $item->ordering;?>" class="width-20 text-area-order " />
                    <?php else : ?>
                        <span class="sortable-handler inactive" >
                            <i class="icon-menu"></i>
                        </span>
                    <?php endif; ?>
                </td>
                <td class="center hidden-phone">
                    <?php echo JHtml::_('grid.id', $i, $item->id); ?>
                </td>
                <td class="center">
                    <?php echo JHtml::_('jgrid.published', $item->published, $i, 'sery.', $canChange, 'cb'); ?>
                    <?php echo JHtml::_('preachitadministrator.featured', $item->featured, $i, 'sery.', $canChange); ?>
                </td>
                <td class="nowrap has-context">
                    <div class="pull-left">
                    <?php if ($item->checked_out) : ?>
                                <?php echo JHtml::_('jgrid.checkedout', $i, $editor, $item->checked_out_time, 'sery.', $canCheckin); ?>
                            <?php endif; ?>
                            <?php if ($canEdit) : ?>
                                <a href="<?php echo JRoute::_('index.php?option=com_preachit&task=series.edit&id='.(int) $item->id); ?>">
                                    <?php echo $this->escape($item->series_name); ?></a>
                            <?php else : ?>
                                <?php echo $this->escape($item->series_name); ?>
                            <?php endif; ?>
                    </div>
                    <div class="pull-left">
                        <?php
                            // Create dropdown items
                            JHtml::_('dropdown.edit', $item->id, 'series.');
                            JHtml::_('dropdown.divider');
                            if ($item->published) :
                                JHtml::_('dropdown.unpublish', 'cb' . $i, 'sery.');
                            else :
                                JHtml::_('dropdown.publish', 'cb' . $i, 'sery.');
                            endif;
                                JHtml::_('dropdown.divider');
                            if ($trashed) :
                                JHtml::_('dropdown.untrash', 'cb' . $i, 'sery.');
                            else :
                                JHtml::_('dropdown.trash', 'cb' . $i, 'sery.');
                            endif;

                            // render dropdown list
                            echo JHtml::_('dropdown.render');
                        ?>
                    </div>
                </td>
                <td class="hidden-phone">
                    <?php $item->series_description = strip_tags($item->series_description);
                    echo JString::substr($item->series_description, 0, 149); ?>
                </td>
                <td class="small center hidden-phone">
                    <?php if ($item->language == '*'):?>
                        <?php echo JText::alt('JALL', 'language'); ?>
                    <?php else:?>
                        <?php echo $language_title ? $this->escape($language_title) : JText::_('JUNDEFINED'); ?>
                    <?php endif;?>
                </td>
                <td class="center hidden-phone">
                        <?php echo $item->id; ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<!-- /Footer -->
<?php echo JHTML::_( 'form.token' ); ?>
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="view" value="sery" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
</div>
<div class="clearfix"> </div>
<div style="text-align: center; padding-top: 5px;">
<?php $abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/lib/preachit.footer.php');?>
<?php echo PIfooter::footer();?></div>
</form>