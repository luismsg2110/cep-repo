<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
class PreachitViewTemplateinstall extends JViewLegacy
{
function display($tpl = null)
{
$document = JFactory::getDocument();
$document->addStyleSheet('../media/preachit/css/preachit.css');
$user    = JFactory::getUser();
if (!$user->authorise('core.admin', 'com_preachit'))
{Tewebcheck::check403();}

// load menu
PreachitHelper::addSubmenu('templates');

$this->addToolbar();

if (Tewebcheck::getJversion() >= 3.0)
{
    $this->sidebar = JHtmlSidebar::render();   
}
parent::display($tpl);
}

/**
     * Add the page title and toolbar.
     *
     * @since    1.6
     */
protected function addToolbar()
{
    $user    = JFactory::getUser();   
    JToolBarHelper::title( JText::_( 'COM_PREACHIT_TEMPLATE_MANAGER_TITLE' ), 'template.png');
    JToolBarHelper::preferences('com_preachit', '600', '640');
    JToolBarHelper::cancel( 'template.cancel', 'JTOOLBAR_CLOSE' );
    JToolBarHelper::help('pihelp', 'com_preachit');
}
}