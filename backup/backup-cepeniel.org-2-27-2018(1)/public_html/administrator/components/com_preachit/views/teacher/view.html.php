<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
class PreachitViewTeacher extends JViewLegacy
{
    protected $form;

    protected $item;

    protected $state; 
    
function display($tpl = null)
{
//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();
$document = JFactory::getDocument();
$document->addStyleSheet('../media/preachit/css/preachit.css');
JRequest::setVar('hidemainmenu', 1);
// get Joomla version to decide which form and method

$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/teacherimage.php');
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/forms.php');
jimport('teweb.details.standard');

$user	= JFactory::getUser();
$this->assignRef('user', $user);

// hide elements if chosen in admin

$hide = PIHelperforms::hideteacheredit();
$this->assignRef('hide', $hide);
$validate = PIHelperforms::validateteacheredit();
$this->assignRef('validate', $validate);

// get the Form
$this->form = $this->get('Form');
$this->item = $this->get('Item');

// Check for errors.
if (count($errors = $this->get('Errors'))) {
    JError::raiseError(500, implode("\n", $errors));
    return false;
}

if ($this->item->id > 0)
{
if (!$user->authorise('core.edit', 'com_preachit') && !$user->authorise('core.edit.own', 'com_preachit')) 
{Tewebcheck::check403();}
elseif (!$user->authorise('core.edit', 'com_preachit') && $user->id != $row->user)
{Tewebcheck::check403();}
}	
else 
{
if (!$user->authorise('core.create', 'com_preachit')) 
{Tewebcheck::check403();}
}
if (Tewebcheck::getJversion() >= 3.0)
{
    $this->setLayout('form30');
}
else {
    $this->setLayout('form16');
}
//get preview images

$image = PIHelpertimage::teacherimage($this->item->id, 0, '', 'large');

$this->assignRef('image', $image);
$this->addToolbar();
parent::display($tpl);
}

/**
     * Add the page title and toolbar.
     *
     * @since    1.6
     */
protected function addToolbar()
{
    $user    = JFactory::getUser();   
    if ($this->item->id > 0)
    {
    JToolBarHelper::title( JText::_( 'COM_PREACHIT_ADMIN_TEACHERS_EDIT' ), 'teacher.png' );
    }
    else
    {
    JToolBarHelper::title( JText::_( 'COM_PREACHIT_ADMIN_TEACHERS_DETAILS' ), 'teacher.png' );
    }
    JToolBarHelper::apply('teacher.apply', 'JTOOLBAR_APPLY');
    JToolBarHelper::save('teacher.save', 'JTOOLBAR_SAVE');
    JToolBarHelper::divider();
    $user    = JFactory::getUser();
    if ($user->authorise('core.admin', 'com_preachit'))  {
    JToolBarHelper::preferences('com_preachit', '550', '900');
    }
    if ($this->item->id > 0)
    {
    JToolBarHelper::cancel( 'teacher.cancel', 'JTOOLBAR_CLOSE' );
    }
    else
    {
    JToolBarHelper::cancel( 'teacher.cancel', 'JTOOLBAR_CANCEL' );
    }
    JToolBarHelper::divider();
    JToolBarHelper::help('pihelp', 'com_preachit');
}

}