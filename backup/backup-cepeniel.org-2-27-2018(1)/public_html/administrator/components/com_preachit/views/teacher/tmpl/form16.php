<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
JHTML::_('behavior.tooltip');
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/tooltips.php');
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/additional.php');
$option = JRequest::getCmd('option');
JText::script('COM_PREACHIT_FIELDS_INVALID');
$user	= JFactory::getUser();
JHTML::_('behavior.tooltip');
JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
$document = JFactory::getDocument();      
$document->addScript(JURI::root() . 'media/preachit/js/pisubmitbutton.js');
jimport('joomla.html.pane');
$pane = JPane::getInstance( 'tabs' );  
?>
<script language="javascript" type="text/javascript">
function setDisabled(filter, setting)
{
    if (filter)
    {filter.disabled = setting;}    
    return
}

function setDisabledall(setting)
{
    var filter5 = document.getElementById('jform_image_folderlrg');
    setDisabled(filter5, setting);
}

window.addEvent('domready', function(){
                setDisabledall(true);
            })
</script>
<form action="index.php" class ="form-validate" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
<?php
echo $pane->startPane( 'content-pane' );
echo $pane->startPanel( JText::_( 'COM_PREACHIT_ADMIN_TEACHER_LEGEND' ), 'MAIN' );
?>
<div class="width-100">
<fieldset class="panelform">
<ul class="adminformlist">
<?php
foreach ($this->form->getFieldset("maininfo") as $field):
if (!$user->authorise('core.edit.state', 'com_preachit') && $field->name == 'jform[published]')
{continue;}
if (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
{
        ?> 
          <?php 
                if ($field->hidden):
                    echo $field->input;
                else:
                ?>
                <li>
                    <?php echo $field->label; ?>       
                    <?php echo $field->input; ?>
                </li>
                <?php
                endif;
           
            ?>
        <?php }
        endforeach;?>
</ul>
<?php if (Tewebcheck::showtab($this->form, 'metaoptions', $this->hide))
{ ?>
<div class="clr"></div>
<h2><?php echo JText::_('COM_PREACHIT_SUB_META_OPTIONS');?></h2>
<ul class="adminformlist">
<?php
foreach ($this->form->getFieldset("metaoptions") as $field):
if (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
{
        ?>
          <?php 
                if ($field->hidden):
                    echo $field->input;
                else:
                ?>
                <li>
                    <?php echo $field->label; ?>       
                    <?php echo $field->input; ?>
                </li>
                <?php
                endif;
           
            ?>
        <?php    }
        endforeach;?>
</ul>
<?php } ?>
</fieldset>
</div>
<?php
echo $pane->endPanel();
if (Tewebcheck::showtab($this->form, 'images', $this->hide))
{
echo $pane->startPanel( JText::_( 'COM_PREACHIT_ADMIN_IMAGEHEAD' ), 'MESIMAGES' );
?>
<div class="width-100">
<fieldset class="panelform">
<ul class="adminformlist">
<?php
foreach ($this->form->getFieldset("images") as $field):
if ($field->name == 'jform[image_folderlrg]' && in_array('image_folderlrg', $this->hide) && !in_array('teacher_image_lrg', $this->hide)) 
{?>
    <input type="hidden" id="<?php echo $field->id;?>" "name="<?php echo $field->name;?>" value="<?php $value = Tewebcheck::gettvalue($field->name); echo $this->row->$value;?>" />
<?php }   
elseif (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
{
        ?> 
          <?php 
                if ($field->hidden):
                    echo $field->input;
                else:
                ?>
                <li>
                    <?php echo $field->label; ?>  
                    <?php echo $field->input; ?>
                </li>
                <?php if ($field->name == 'jform[teacher_image_lrg]')
                    {?>
                    <li>
                        <label>
                            <?php echo JText::_('COM_PREACHIT_ADMIN_LRGIMAGE_PREVIEW');?>
                        </label>
                            <?php echo $this->image; ?>
                    </li> <?php } ?>
                <?php
                endif;
           
            ?>
        <?php
}
endforeach;?>
</ul>
</fieldset>
</div>
<?php
echo $pane->endPanel();
}
if (!in_array('teacher_description', $this->hide))
{
echo $pane->startPanel( JText::_( 'COM_PREACHIT_ADMIN_DESCHEAD' ), 'DESC' );
?>
<div class="width-100">
<fieldset class="panelform">
<?php echo $this->form->getInput('teacher_description'); ?>
</fieldset>
</div>
<?php
echo $pane->endPanel();
}
echo $pane->endPane();
?>
<!-- Footer -->
<div style="text-align: center">
<?php $abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/lib/preachit.footer.php');?>
<?php echo PIfooter::footer();?></div>
<!-- /Footer -->
<input type="hidden" name="id" value="<?php echo $this->item->id; ?>" />
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="controller" value="teacher" />
<input type="hidden" name="task" value ="" />
<?php if (!$user->authorise('core.edit.state', 'com_preachit') && !in_array(Tewebcheck::gettvalue('jform[published]'), $this->hide)) {?>
<input type="hidden" name="jform[published]" id="jform_published" value="<?php echo $this->item->published;?>" />
<?php }?>
<?php foreach ($this->validate AS $validate)
{
    if (in_array($validate, $this->hide))
    {?>
        <input type="hidden" name="jform[<?php echo $validate;?>]" id="jform_<?php echo $validate;?>" value="<?php echo $this->item->$validate;?>" />
    <?php }    
}?>
<?php echo JHTML::_( 'form.token' ); ?>
</form>