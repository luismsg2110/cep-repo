<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$option = JRequest::getCmd('option');
JText::script('COM_PREACHIT_FIELDS_INVALID');
JText::script('COM_PREACHIT_TAG_NO_CAHNGE_WARNING');
$user	= JFactory::getUser();
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.chosen', 'select');
$document = JFactory::getDocument();      
$document->addScript(JURI::root() . 'media/preachit/js/pisubmitbutton.js');
?>
<form class ="form-validate" action="index.php" method="post" name="adminForm" id="adminForm">
    <fieldset>
        <legend><?php echo JText::_('COM_PREACHIT_ADMIN_TAG_LEGEND');?></legend>
        <div class="form-horizontal">
            <div class="control-group">
                <div class="control-label">
                    <?php echo JText::_('COM_PREACHIT_ADMIN_TAG_LABEL');?><span class="star">&nbsp;*</span>
                </div>
                <div class="controls">       
                    <input id="tag" class="inputbox required" type="text" maxlength="40" size="40" value="<?php echo $this->tag;?>" name="tag" aria-required="true" required="required">
                </div>
            </div>
        </div>
    </fieldset>
<!-- Footer -->
<div style="text-align: center">
<?php $abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/lib/preachit.footer.php');?>
<?php echo PIfooter::footer();?></div>
<!-- /Footer -->
<input id="controller" type="hidden" name="controller" value="taglist" />
<input id="original" type="hidden" name="original" value="<?php echo $this->tag; ?>" />
<input id="option "type="hidden" name="option" value="<?php echo $option;?>" />
<input id="task" type="hidden" name="task" value ="" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>






