<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
class PreachitViewTag extends JViewLegacy
{
function display($tpl = null)
{
$document = JFactory::getDocument();
$document->addStyleSheet('../media/preachit/css/preachit.css');
$user    = JFactory::getUser();   
JRequest::setVar('hidemainmenu', 1);

// get Joomla version to decide which form and method

$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/additional.php');

$tag = JRequest::getVar( 'tagstring', '' );
$this->assignRef('tag', $tag);

if (!$user->authorise('core.create', 'com_preachit')) 
{Tewebcheck::check403();}
if (Tewebcheck::getJversion() >= 3.0)
{
    $this->setLayout('form30');
}
else {
    $this->setLayout('form16');
}
$this->addToolbar();
parent::display($tpl);
}

/**
     * Add the page title and toolbar.
     *
     * @since    1.6
     */
protected function addToolbar()
{
    $user    = JFactory::getUser();   
    JToolBarHelper::title( JText::_( 'COM_PREACHIT_TAG_TITLE_EDIT' ), 'tag.png' );
    JToolBarHelper::apply('tag.apply', 'JTOOLBAR_APPLY');
    JToolBarHelper::save('tag.save', 'JTOOLBAR_SAVE');
    JToolBarHelper::divider();
    if ($user->authorise('core.admin', 'com_preachit'))  {
    JToolBarHelper::preferences('com_preachit', '550', '900');
    }
    JToolBarHelper::cancel( 'tag.cancel', 'JTOOLBAR_CLOSE' );
    JToolBarHelper::divider();
    JToolBarHelper::help('pihelp', 'com_preachit');
}

}