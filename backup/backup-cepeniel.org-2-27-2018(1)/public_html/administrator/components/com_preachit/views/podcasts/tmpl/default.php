<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$user	= JFactory::getUser();
$option = JRequest::getCmd('option');
$listOrder    = $this->escape($this->state->get('list.ordering'));
$listDirn    = $this->escape($this->state->get('list.direction'));
?>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<table width="100%">
<tr>
<td> <?php echo $this->state_list; ?></td>
</tr></table>
<table class="adminlist table table-striped">
<thead>
<tr>
<th width="20">
<input type="checkbox" name="toggle"
value="" onclick="checkAll(<?php echo count( $this->rows ); ?>);" />
</th>
<th width="5%" nowrap="nowrap"><?php echo JText::_('COM_PREACHIT_ADMIN_STATELIST');?></th>
<th width ="7%"><?php echo JText::_('COM_PREACHIT_ADMIN_ORDERLIST');?></th>
<th width="40%"><?php echo JText::_('COM_PREACHIT_ADMIN_NAMELIST');?></th>
<th width="48%"><?php echo JText::_('COM_PREACHIT_ADMIN_DESCRIPTIONLIST');?></th>
</tr>
</thead>
<?php
jimport('joomla.filter.output');
$k = 0;
for ($i=0, $n=count( $this->rows ); $i < $n; $i++)
{
$row = &$this->rows[$i];
$checked = JHTML::_('grid.id', $i, $row->id );
$canCheckin    = $user->authorise('core.manage',        'com_checkin') || $row->checked_out == $user->id || $row->checked_out == 0;
// determine state column
$published = JHtml::_('jgrid.published', $row->published, $i, 'podcasts.', $user->authorise('core.edit.state', 'com_preachit'), 'cb');

$link = JFilterOutput::ampReplace( 'index.php?option=' . $option . '&task=podcast.edit&id='. $row->id );

// get checked out name
$db = JFactory::getDBO();
	$query = "SELECT ".$db->quoteName('name')."
    FROM ".$db->quoteName('#__users')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($row->checked_out).";
  ";
	$db->setQuery($query);
	$editor = $db->loadResult();

//determine name column

if ($user->authorise('core.edit', 'com_preachit')) 
{
$name = '<a href="'.$link.'">'.$row->name.'</a>';}
else {$name = $row->name;}

?>
<tr class="<?php echo "row$k"; ?>">
<td>
<?php echo $checked; ?>
</td>
<td align="center">
<?php echo $published;?>
</td>
<td class="order" style="min-width: 85px;">
<?php if ($listOrder == 'ordering')
{?>
<span><?php echo $this->pagination->orderUpIcon($i, true, 'podcasts.orderup', 'JLIB_HTML_MOVE_UP', true); ?></span>
<span><?php echo $this->pagination->orderDownIcon($i, $this->pagination->total, true, 'podcasts.orderdown', 'JLIB_HTML_MOVE_DOWN', true); ?></span>
<?php } ?>
<input type="text" style="text-align: center" class="text_area" value="<?php echo $row->ordering;?>" size="5" name="order[]">
</td>
<td>
<?php if ($row->checked_out) : ?>
    <?php echo JHtml::_('jgrid.checkedout', $i, $editor, $row->checked_out_time, 'podcasts.', $canCheckin); ?>
<?php endif; ?>
<a href="<?php echo $link; ?>"><?php echo $name; ?></a>
</td>
<td>
<?php echo JString::substr($row->description, 0, 149); ?>
</td>
</tr>
<?php
$k = 1 - $k;
}
?>
	      <tfoot><tr>
	      <td colspan="5"> <?php echo $this->pagination->getListFooter(); ?> </td></tr>
	      </tfoot>
</table>
<div style="text-align: center; padding-top: 5px;">
<?php $abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/lib/preachit.footer.php');?>
<?php echo PIfooter::footer();?></div>
<!-- /Footer -->
<?php echo JHTML::_( 'form.token' ); ?>
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="view" value="podcasts" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
</form>