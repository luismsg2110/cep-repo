<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 * This control panel uses styling that has been adapted from the Kunena Control Panel
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$abspath = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/scripture.php');
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/lib/preachit.stats.class.php');
$option = JRequest::getCmd('option');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');
?>
<script language="javascript" type="text/javascript">
Joomla.submitbutton = function(task)
    {
    if (task == "")
                {
                return false;
                }
   else if (task == "cpanel.resethits") 
    {
        if(confirm("<?php echo JText::_('COM_PREACHIT_TEMPLATE_MANAGER_RESET_WARNING');?>"))
        {Joomla.submitform(task);}
        } 
    else if (task == "cpanel.resetdownloads") 
    {
        if(confirm("<?php echo JText::_('COM_PREACHIT_TEMPLATE_MANAGER_DOWNLOADS_WARNING');?>"))
        {Joomla.submitform(task);}
        } 
    
 else {
            Joomla.submitform(task);
        }
}
</script>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<?php if(!empty( $this->sidebar)): ?>
    <div id="j-sidebar-container" class="span2">
        <div class="center" style="padding: 20px 20px 10px 20px; margin-bottom: 30px; border-bottom: 1px solid #f1f1f1;">
            <p><img class="img-polaroid" src = "../media/preachit/images/picpanel.png" border="0" alt = "preachit"/></p>
            <p class="lead"><em>"<?php echo JText::_('COM_PREACHIT_VS');?>"</em></br />
            <?php echo JText::_('COM_PREACHIT_VS_REF');?></p>
        </div>
        <?php echo $this->sidebar; ?>
    </div>
    <div id="j-main-container" class="span10">
<?php else : ?>
    <div id="j-main-container">
<?php endif;?>

<div class="fbwelcome">
  <h2><?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_WELCOME');?></h2>
  <p><?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_WELCOME_MES');?></p>
</div>

<!-- BEGIN: STATS -->
<div class="span4">
    <h3><?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_GENERAL_STATS'); ?></h3>
        <table class="adminlist table table-striped">
          <thead>
            <tr>
              <th><?php echo JText::_('COM_PREACHIT_ADMIN_NAMELIST');?></th>
              <th></th>
              <th><?php echo JText::_('COM_PREACHIT_ADMIN_NUMBERSLIST');?></th>
            </tr>
          </thead>
          <tbody>
		    <tr>
			    <td><?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_MESSAGE_NO')?></td>
				<td></td>
				<td><?php echo piStats::get_no_messages();?></td>
			</tr>
			<tr>
			    <td><?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_SERIES_NO')?></td>
				<td></td>
				<td><?php echo piStats::get_no_series();?></td>
			</tr>
			<tr>
			    <td><?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_TEACHERS_NO')?></td>
				<td></td>
				<td><?php echo piStats::get_no_teachers();?></td>
			</tr>
			<tr>
			    <td><?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_MINISTRIES_NO')?></td>
				<td></td>
				<td><?php echo piStats::get_no_ministry();?></td>
			</tr>
			<tr>
			    <td><?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_PODCASTS_NO')?></td>
				<td></td>
				<td><?php echo piStats::get_no_podcast();?></td>
			</tr>
			<tr>
			    <td><?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_HITS')?></td>
				<td></td>
				<?php
					$submith = "Joomla.submitbutton('cpanel.resethits')";?>
					<td><?php echo piStats::totalhits();?><span style="padding-left: 20px;"><button class="btn btn-small btn-warning" type="button" onclick="<?php echo $submith;?>">
 <?php echo JText::_('COM_PREACHIT_RESET');?> </button></span></td>
            </tr>
			<tr>
			    <td><?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_DOWNLOADS')?></td>
				<td></td>
					<?php
					$submitd = "Joomla.submitbutton('cpanel.resetdownloads')";?>
					<td><?php echo piStats::totaldownloads();?><span style="padding-left: 20px;"><button class="btn btn-small btn-warning" type="button" onclick="<?php echo $submitd;?>">
 <?php echo JText::_('COM_PREACHIT_RESET');?> </button></span></td>
            </tr>
          </tbody>
        </table>
</div>
<div class="span1"></div>
<div class="span7">
    <h3><?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_LATMES'); ?></h3>
        <table class="adminlist table table-striped">
          <thead>
            <tr>
              <th width="32%"><?php echo JText::_('COM_PREACHIT_ADMIN_TITLELIST');?></th>
              <th width="2%"></th>
              <th width="32%"><?php echo Jtext::_('COM_PREACHIT_ADMIN_SCRIPTURELIST');?></th>
              <th width="2%"></th>
              <th width="32%"><?php echo Jtext::_('COM_PREACHIT_ADMIN_DATELIST');?></th>
            </tr>
          </thead>
          <tbody>
		        <?php foreach($this->latests as $i => $latest)
		        { 
	        
		            $link = JFilterOutput::ampReplace( 'index.php?option=' . $option . '&task=study.edit&id='. $latest->id );	
	            
		            //build scripture ref
	            
		            //get scripture
                    $bibleref = new PIHelperscripture($latest);
                    $scripture = $bibleref->ref1;
		            ?>
		            <tr class="row<?php echo $i % 2; ?>">
		                <td><a href="<?php echo $link; ?>"><?php echo $latest->study_name;?></a></td>
		                <td></td>
		                <td><?php echo $scripture;?></td>
		                <td></td>
		                <td><?php echo JHTML::Date($latest->study_date, $this->dateformat); ?></td>
		            </tr>
		        <?php } ?>
          </tbody>
        </table>
</div>
<div class="clearfix"> </div>
<div class="span4">  
    <h3><?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_LATPOD'); ?></h3>
        <table class="adminlist table table-striped">
          <thead>
            <tr>
              <th><?php echo Jtext::_('COM_PREACHIT_ADMIN_NAMELIST');?></th>
              <th></th>
              <th><?php echo Jtext::_('COM_PREACHIT_ADMIN_CPANEL_PODPUB');?></th>
            </tr>
          </thead>
          <tbody>
		    <?php foreach($this->pods as $i => $pod)
		    { 
	    
		        $link2 = JFilterOutput::ampReplace( 'index.php?option=' . $option . '&task=podcast.edit&id='. $pod->id );	
	        
		        ?>
		        <tr class="row<?php echo $i % 2; ?>">
		            <td><a href="<?php echo $link2; ?>"><?php echo $pod->name;?></a></td>
		            <td></td>
		            <td><?php echo JHTML::Date($pod->podpub, $this->dateformat); ?></td>
		        </tr>
		    <?php } ?>
	
          </tbody>
        </table>
</div>       
<div class="span1"></div>
<div class="span7">
    <h3><?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_POPMES'); ?></h3>
        <table class="adminlist table table-striped">
          <thead>
            <tr>
              <th width="32%"><?php echo JText::_('COM_PREACHIT_ADMIN_TITLELIST');?></th>
              <th width="2%"></th>
              <th width="32%"><?php echo Jtext::_('COM_PREACHIT_ADMIN_SCRIPTURELIST');?></th>
              <th width="2%"></th>
              <th width="32%"><?php echo Jtext::_('COM_PREACHIT_ADMIN_HITSLIST');?></th>
            </tr>
          </thead>
          <tbody>
                <?php foreach($this->pops as $i => $pop)
		        { 
	        
		            $link1 = JFilterOutput::ampReplace( 'index.php?option=' . $option . '&task=study.edit&id='. $pop->id );	
	            
		            //build scripture ref
	            
		            //get scripture
                    $bibleref = new PIHelperscripture($pop);
                    $scripture1 = $bibleref->ref1;
		            ?>
		            <tr class="row<?php echo $i % 2; ?>">
		                <td><a href="<?php echo $link1; ?>"><?php echo $pop->study_name;?></a></td>
		                <td></td>
		                <td><?php echo $scripture1;?></td>
		                <td></td>
		                <td><?php echo $pop->hits; ?></td>
		            </tr>
		        <?php } ?>
          </tbody>
        </table>
</div>

 <!-- Footer -->
 <input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="controller" value="cpanel" />
<?php echo JHTML::_( 'form.token' ); ?>
</div>
<div class="clearfix"> </div>
<div style="text-align: center; padding-top: 5px;">
<?php $abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/lib/preachit.footer.php');?>
<?php echo PIfooter::footer();?></div>
</div>
</form>