<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
class PreachitViewCpanel extends JViewLegacy
{
function display($tpl = null)
{
$app = JFactory::getApplication();
$option = JRequest::getCmd('option');
$document = JFactory::getDocument();
$document->addStyleSheet('../media/preachit/css/preachit.css');
$latests = $this->get('datalat');
$pops = $this->get('datapop');
$pods= $this->get('datapod');

$this->assignRef('latests', $latests);
$this->assignRef('pops', $pops);
$this->assignRef('pods', $pods);

// load menu
PreachitHelper::addSubmenu('cpanel');

$this->addToolbar();

if (Tewebcheck::getJversion() >= 3.0)
{
    $this->setLayout('default30');
    $this->sidebar = JHtmlSidebar::render();   
}

$dateformat = 'd F, Y';
$this->assignRef('dateformat', $dateformat);

parent::display($tpl);
}

/**
     * Add the page title and toolbar.
     *
     * @since    1.6
     */
protected function addToolbar()
{
    $user    = JFactory::getUser();
    JToolBarHelper::title( JText::_( 'COM_PREACHIT_ADMIN_CPANEL_TITLE' ), 'picpanel.png');
    if ($user->authorise('core.admin', 'com_preachit'))  {
        JToolBarHelper::preferences('com_preachit', '550', '900');
    }
}


}