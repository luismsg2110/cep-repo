<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
class PreachitViewMinistry extends JViewLegacy
{
    protected $form;

    protected $item;

    protected $state; 
    
function display($tpl = null)
{
//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();
$document = JFactory::getDocument();
$document->addStyleSheet('../media/preachit/css/preachit.css');
JRequest::setVar('hidemainmenu', 1);

// get Joomla version to decide which form and method

$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/ministryimage.php');

// get the Form
$this->form = $this->get('Form');
$this->item = $this->get('Item');  
// Check for errors.
if (count($errors = $this->get('Errors'))) {
    JError::raiseError(500, implode("\n", $errors));
    return false;
}
$user	= JFactory::getUser();
if ($this->item->id > 0)
{
if (!$user->authorise('core.edit', 'com_preachit')) 
{Tewebcheck::check403();}
if (!$user->authorise('core.edit.own', 'com_preachit') && $user->id != $row->user)
{Tewebcheck::check403();}
}
else 
{
if (!$user->authorise('core.create', 'com_preachit')) 
{Tewebcheck::check403();}
}

if (Tewebcheck::getJversion() >= 3.0)
{
    $this->setLayout('form30');
}
else {
    $this->setLayout('form16');
}
//get preview images

$image = PIHelperminimage::ministryimage($this->item->id, 0, '', 'large');

$this->assignRef('image', $image);
$this->addToolbar();
parent::display($tpl);
}

/**
     * Add the page title and toolbar.
     *
     * @since    1.6
     */
protected function addToolbar()
{
    $user    = JFactory::getUser();   
    if ($this->item->id > 0)
    {
    JToolBarHelper::title( JText::_( 'COM_PREACHIT_ADMIN_MINISTRY_EDIT' ), 'ministry.png' );
    }
    else
    {
    JToolBarHelper::title( JText::_( 'COM_PREACHIT_ADMIN_MINISTRY_DETAILS' ), 'ministry.png' );
    }
    JToolBarHelper::apply('ministry.apply', 'JTOOLBAR_APPLY');
    JToolBarHelper::save('ministry.save', 'JTOOLBAR_SAVE');
    JToolBarHelper::divider();
    $user    = JFactory::getUser();
    if ($user->authorise('core.admin', 'com_preachit'))  {
    JToolBarHelper::preferences('com_preachit', '550', '900');
    }
    if ($this->item->id > 0)
    {
    JToolBarHelper::cancel( 'ministry.cancel', 'JTOOLBAR_CLOSE' );
    }
    else
    {
    JToolBarHelper::cancel( 'ministry.cancel', 'JTOOLBAR_CANCEL' );
    }
    JToolBarHelper::divider();
    JToolBarHelper::help('pihelp', 'com_preachit');
}

}