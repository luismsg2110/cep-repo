<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/tooltips.php');
$option = JRequest::getCmd('option');
JText::script('COM_PREACHIT_FIELDS_INVALID');
$user	= JFactory::getUser();
JHtml::_('behavior.tooltip');
JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
$document = JFactory::getDocument();      
$document->addScript(JURI::root() . 'media/preachit/js/pisubmitbutton.js');
?>
<form class ="form-validate" action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
    <div class="form-horizontal">
        <fieldset>
            <ul class="nav nav-tabs">
                <li class="active"><a href="#main" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_ADMIN_PODCAST_LEGEND');?></a></li>
                <li><a href="#podepisodes" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_ADMIN_EPISODEHEAD');?></a></li>
                <li><a href="#podcontent" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_ADMIN_CONTENTHEAD');?></a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="main">
                    <?php
                        foreach ($this->form->getFieldset("maininfo") as $field):
                                ?>
                                  <?php 
                                        if ($field->hidden):
                                            echo $field->input;
                                        elseif (!$user->authorise('core.edit.state', 'com_preachit') && $field->name == 'jform[published]'):
                                                    continue;
                                        else:
                                        ?>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php if ($field->name != 'jform[podpub]' ) {?> 
                                                <?php echo $field->input; ?>
                                                <?php if ($field->name == 'jform[website]' || $field->name == 'jform[image]')
                                                {echo PIHelpertooltips::nohttp();}?>
                                                 <?php if ($field->name == 'jform[description]')
                                                {echo PIHelpertooltips::poddescription();}?>
                                                <?php if ($field->name == 'jform[filename]')
                                                {echo PIHelpertooltips::podfilename();}?>
                                                <?php if ($field->name == 'jform[search]')
                                                {echo PIHelpertooltips::separate();}?>
                                                <?php if ($field->name == 'jform[menu_item]')
                                                {echo PIHelpertooltips::podmenuitem();}?>
                                                <?php if ($field->name == 'jform[language]')
                                                {echo PIHelpertooltips::lang();}?>
                                                <?php }
                                                else {?>
                                                <input id="podpub" type="text" readonly="readonly" size="100" style="border: 0px" value="<?php echo JHTML::Date($this->item->podpub, 'D, d M, Y');?>" name="podpub">
                                                <?php }?>
                                            </div>
                                        </div>
                                        <?php
                                        endif;
                                   
                                    ?>
                                <?php
                                endforeach;?>
                </div>
                <div class="tab-pane" id="podepisodes">
                    <?php
                        foreach ($this->form->getFieldset("episodes") as $field):
                                ?>
                                  <?php 
                                        if ($field->hidden):
                                            echo $field->input;
                                        else:
                                        ?>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                                <?php if ($field->name == 'jform[menu_item]')
                                                {echo PIHelpertooltips::podmenuitem();}?>
                                                <?php if ($field->name == 'jform[itunestitle]' || $field->name == 'jform[itunessub]' || $field->name == 'jform[itunesdesc]')
                                                {echo PIHelpertooltips::podcastdef();}?>
                                            </div>
                                        </div>
                                        <?php
                                        endif;
                                   
                                    ?>
                                <?php
                                endforeach;?>
                </div>
                <div class="tab-pane" id="podcontent">
                    <?php
                        foreach ($this->form->getFieldset("content") as $field):
                                ?>
                                  <?php 
                                        if ($field->hidden):
                                            echo $field->input;
                                        else:
                                        ?>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                        <?php
                                        endif;
                                   
                                    ?>
                                <?php
                                endforeach;?>
                </div>
            </div>
        </fieldset>
    </div>
<!-- Footer -->
<div style="text-align: center">
<?php $abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/lib/preachit.footer.php');?>
<?php echo PIfooter::footer();?></div>
<!-- /Footer -->
<input type="hidden" name="controller" value="podcast" />
<input type="hidden" name="id" value="<?php echo $this->item->id; ?>" />
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="task" value ="" />
<?php if (!$user->authorise('core.edit.state', 'com_preachit')) {?>
<input type="hidden" name="jform[published]" id="jform_published" value="<?php echo $this->item->published;?>" />
<?php }?>
<?php echo JHTML::_( 'form.token' ); ?>
</form>