<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
class PreachitViewPodcast extends JViewLegacy
{
    protected $form;

    protected $item;

    protected $state; 
    
function display($tpl = null)
{
$document = JFactory::getDocument();
$document->addStyleSheet('../media/preachit/css/preachit.css');
JRequest::setVar('hidemainmenu', 1);

// get Joomla version to decide which form and method

$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/additional.php');

$id = JRequest::getInt( 'id', 0 );
$this->assignRef('id', $id);

// get the Form
$this->form = $this->get('Form');
$this->item = $this->get('Item');
// Check for errors.
if (count($errors = $this->get('Errors'))) {
    JError::raiseError(500, implode("\n", $errors));
    return false;
}

$user	= JFactory::getUser();
if ($id > 0)
{
if (!$user->authorise('core.edit', 'com_preachit')) 
{Tewebcheck::check403();}
}
else 
{
if (!$user->authorise('core.create', 'com_preachit')) 
{Tewebcheck::check403();}
}

if (Tewebcheck::getJversion() >= 3.0)
{
    $this->setLayout('form30');
}
else {
    $this->setLayout('form16');
}
$this->addToolbar();
parent::display($tpl);
}

/**
     * Add the page title and toolbar.
     *
     * @since    1.6
     */
protected function addToolbar()
{
    $user    = JFactory::getUser();   
    if ($this->id > 0)
    {
    JToolBarHelper::title( JText::_( 'COM_PREACHIT_ADMIN_PODCASTS_EDIT' ), 'podcast.png' );
    }
    else
    {
    JToolBarHelper::title( JText::_( 'COM_PREACHIT_ADMIN_PODCASTS_DETAILS' ), 'podcast.png' );
    }
    JToolBarHelper::apply('podcast.apply', 'JTOOLBAR_APPLY');
    JToolBarHelper::save('podcast.save', 'JTOOLBAR_SAVE');
    JToolBarHelper::divider();
    JToolBarHelper::custom('podcast.publishxml', 'publishxml.png', 'publishxml.png', 'COM_PREACHIT_ADMIN_BUTTON_WRITE_XML', false);
    JToolBarHelper::divider();
    $user    = JFactory::getUser();
    if ($user->authorise('core.admin', 'com_preachit'))  {
    JToolBarHelper::preferences('com_preachit', '550', '900');
    }
    if ($this->id > 0)
    {
    JToolBarHelper::cancel( 'podcast.cancel', 'JTOOLBAR_CLOSE' );
    }
    else
    {
    JToolBarHelper::cancel( 'podcast.cancel', 'JTOOLBAR_CANCEL' );
    }
    JToolBarHelper::divider();
    JToolBarHelper::help('pihelp', 'com_preachit');
}

}