<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
class PreachitViewTemplates extends JViewLegacy
{
function display($tpl = null)
{
$document = JFactory::getDocument();
$document->addStyleSheet('../media/preachit/css/preachit.css');

$dateformat = 'F, Y';
$this->assignRef('dateformat', $dateformat);

$this->state  = $this->get('State');
$this->rows = $this->get('data');
$this->pagination = $this->get('Pagination');

// load menu
PreachitHelper::addSubmenu('templates');

$this->addToolbar();

if (Tewebcheck::getJversion() >= 3.0)
{
    $this->setLayout('default30');
    $this->sidebar = JHtmlSidebar::render();   
}       

parent::display($tpl);
}

/**
     * Add the page title and toolbar.
     *
     * @since    1.6
     */
protected function addToolbar()
{
$user    = JFactory::getUser();      
JToolBarHelper::title( JText::_( 'COM_PREACHIT_TEMPLATE_MANAGER_TITLE' ), 'template.png');
if ($user->authorise('core.admin', 'com_preachit')) {
 JToolBarHelper::addNew('templates.addPITemplate', JText::_('COM_PREACHIT_TOOLBAR_ADD'));
JToolBarHelper::makeDefault('templates.deftemp', 'Default');
JToolBarHelper::divider();
JToolBarHelper::custom('templates.copy', 'copy', '', JText::_('Copy'));
JToolBarHelper::deleteList('LIB_TEWEB_TEMPLATE_MANAGER_DELETE_WARNING', 'templates.remove');
JToolBarHelper::divider();
JToolBarHelper::preferences('com_preachit', '550', '900');
}
JToolBarHelper::help('pihelp', 'com_preachit');
}

/**
     * Returns an array of fields the table can be sorted by
     *
     * @return  array  Array containing the field name to sort by as the key and display text as value
     *
     * @since   3.0
     */
    protected function getSortFields()
    {
        return array(
            'published' => JText::_('JSTATUS'),
            'id' => JText::_('JGRID_HEADING_ID'),
            'def' => JText::_('COM_PREACHIT_ADMIN_DEFAULTLIST'),
            'title' => JText::_('COM_PREACHIT_ADMIN_NAMELIST'),
            'template' => JText::_('COM_PREACHIT_ADMIN_TEMPLATELIST'),
            'version' => JText::_('COM_PREACHIT_ADMIN_VERSIONLIST'),
            'create_date' => JText::_('COM_PREACHIT_ADMIN_CREATION_DATELIST'),
            'author' => JText::_('COM_PREACHIT_ADMIN_AUTHORLIST'),
            'cssfiles' => JText::_('COM_PREACHIT_ADMIN_CSS_FILELIST')
        );
    }

}