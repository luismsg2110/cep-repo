<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$option = JRequest::getCmd('option');
$user    = JFactory::getUser(); 	
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');
$listOrder    = $this->escape($this->state->get('list.ordering'));
$listDirn    = $this->escape($this->state->get('list.direction'));
$trashed    = $this->state->get('filter.published') == -2 ? true : false;
?>
<script language="javascript" type="text/javascript">
<?php 
echo 'Joomla.submitbutton = function(task)
    {
    if (task == "")
                {
                return false;
                }
   else if (task == "template.resettemp") 
    {
        if(confirm("'.JText::_('COM_PREACHIT_TEMPLATE_MANAGER_RESET_TEMP_WARN').'"))
        {Joomla.submitform(task);}
        } 
 else {
            Joomla.submitform(task);
        }
}';?>
</script>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<?php if(!empty( $this->sidebar)): ?>
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
    <div id="j-main-container" class="span10">
<?php else : ?>
    <div id="j-main-container">
<?php endif;?>
<table class="adminlist table table-striped" id="articleList">

    <thead>
        <tr>
            <th width="1%" class="hidden-phone">
                <input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
            </th>
            <th>
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_NAMELIST', 'title', $listDirn, $listOrder); ?>
            </th>
            <th width="5%" class="center hidden-phone">
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_DEFAULTLIST', 'def', $listDirn, $listOrder); ?>
            </th>
            <th width="19%" class="center hidden-phone">
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_TEMPLATELIST', 'template', $listDirn, $listOrder); ?>
            </th>
            <th width="5%" class="center hidden-phone">
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_VERSIONLIST', 'version', $listDirn, $listOrder); ?>
            </th>
            <th width="15%" class="center hidden-phone">
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_CREATION_DATELIST', 'create_date', $listDirn, $listOrder); ?>
            </th>
            <th width="15%" class="center hidden-phone">
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_AUTHORLIST', 'author', $listDirn, $listOrder); ?>
            </th>
            <th width="15%">
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_CSS_FILELIST', 'cssfile', $listDirn, $listOrder); ?>
            </th>
            <th width="1%" class="nowrap center hidden-phone">
                        <?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'id', $listDirn, $listOrder); ?>
            </th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <td colspan="9"> <?php echo $this->pagination->getListFooter(); ?> </td>
        </tr>
    </tfoot>
    <tbody>
        <?php
        foreach ($this->rows as $i => $item)
        {
        $ordering  = ($listOrder == 'ordering');
        $canCreate  = $user->authorise('core.create', 'com_preachit');
        $canEdit    = $user->authorise('core.admin', 'com_preachit');
        $canCheckin = $user->authorise('core.manage',     'com_checkin') || $item->checked_out == $userId || $item->checked_out == 0;
        $canChange  = $user->authorise('core.edit.state', 'com_preachit') && $canCheckin;
        // get checked out name
$db = JFactory::getDBO();
	$query = "SELECT ".$db->quoteName('name')."
    FROM ".$db->quoteName('#__users')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($item->checked_out).";
  ";
	$db->setQuery($query);
	$editor = $db->loadResult();
        if ($item->client_id > 0)
        {$master = null;}
        else {$master = '<span class="master">'.JText::_('LIB_TEWEB_TEMP_MASTER').'</span>';}
        ?>
            <tr class="row<?php echo $i % 2; ?>" sortable-group-id="0">
                <td class="center hidden-phone">
                    <?php echo JHtml::_('grid.id', $i, $item->id); ?>
                </td>
                <td class="nowrap has-context">
                    <div class="pull-left">
                        <?php if ($item->checked_out) : ?>
                                <?php echo JHtml::_('jgrid.checkedout', $i, $editor, $item->checked_out_time, 'templates.', $canCheckin); ?>
                            <?php endif; ?>
                        <?php if ($canEdit) : ?>
                            <a href="<?php echo JRoute::_('index.php?option=com_preachit&view=template&template='.(int) $item->id); ?>">
                                <?php echo $this->escape($item->name).$master; ?></a>
                        <?php else : ?>
                            <?php echo $this->escape($item->name).$master; ?>
                        <?php endif; ?>
                    </div>
                </td>
                <td class="center hidden-phone">
                    <?php if ($item->def == 1)
                    {?> 
                        <img alt="Default" src="../media/preachit/images/icon-16-default.png"> 
                    <?php }?>
                </td>
                <td class="center hidden-phone">
                    <?php echo $item->template;?>
                </td>
                <td class="center hidden-phone">
                    <?php if ($item->version)
                    { 
                        echo $item->version; 
                    } 
                    else {
                        echo '<div style="text-align: center;"> - </div>';
                    } ?>
                </td>
                <td class="center hidden-phone">
                    <?php if ($item->date)
                    { 
                        echo JHTML::Date($item->date, $this->dateformat); 
                    } 
                    else {
                        echo '<div style="text-align: center;"> - </div>';
                    } ?>
                </td>
                <td class="center hidden-phone">
                    <?php if ($item->author)
                    { 
                        echo $item->author; 
                    } 
                    else {
                        echo '<div style="text-align: center;"> - </div>';
                    } ?>
                </td>
                <td class="center hidden-phone">
                    <?php echo $item->css;?>
                </td>
                <td class="center hidden-phone">
                        <?php echo $item->id; ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<!-- /Footer -->
<?php echo JHTML::_( 'form.token' ); ?>
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="view" value="templates" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
</div>
<div class="clearfix"> </div>
<div style="text-align: center; padding-top: 5px;">
<?php $abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/lib/preachit.footer.php');?>
<?php echo PIfooter::footer();?></div>
</form>