<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
class PreachitViewBiblevers extends JViewLegacy
{
function display($tpl = null)
{
$document = JFactory::getDocument();
$document->addStyleSheet('../media/preachit/css/preachit.css');
$app = JFactory::getApplication();
$option = JRequest::getCmd('option');
$this->state  = $this->get('State');
$this->rows = $this->get('data');
$this->pagination = $this->get('Pagination');

// load menu
PreachitHelper::addSubmenu('admin');

$this->addToolbar();

if (Tewebcheck::getJversion() >= 3.0)
{
    $this->setLayout('default30');
    $this->sidebar = JHtmlSidebar::render();   
}

parent::display($tpl);
}

/**
     * Add the page title and toolbar.
     *
     * @since    1.6
     */
protected function addToolbar()
{
    $user    = JFactory::getUser();    
    JToolBarHelper::title( JText::_( 'COM_PREACHIT_ADMIN_TITLE_BIBLEVERS' ), 'version.png');
    if ($user->authorise('core.create', 'com_preachit'))  {
    JToolBarHelper::addNew('biblever.add','JTOOLBAR_NEW');}
    if ($user->authorise('core.edit', 'com_preachit'))  {
    JToolBarHelper::editList('biblever.edit','JTOOLBAR_EDIT');
    }
    JToolBarHelper::divider();
    if ($user->authorise('core.edit.state', 'com_preachit')) 
    {
    JToolBarHelper::custom('biblevers.publish', 'publish.png', 'publish_f2.png','JTOOLBAR_PUBLISH', true);
    JToolBarHelper::custom('biblevers.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
    }
    JToolBarHelper::divider();
    if ($this->state->get('filter.state') == -2 && $user->authorise('core.delete', 'com_preachit')) {
                JToolBarHelper::deleteList('', 'biblevers.delete','TE_TOOLBAR_EMPTY_TRASH');
            } else if ($user->authorise('core.edit.state', 'com_preachit')) {
                JToolBarHelper::trash('biblevers.trash','TE_TOOLBAR_TRASH');
            }
    JToolBarHelper::divider();
    if ($user->authorise('core.admin', 'com_preachit'))  {
    JToolBarHelper::preferences('com_preachit', '550', '900');
    }
    JToolBarHelper::help('pihelp', 'com_preachit');
    
    if (Tewebcheck::getJversion() >= 3.0)
    {
        $selectstate = Tewebdetails::stateselector();
        
        JHtmlSidebar::setAction('index.php?option=com_preachit&view=biblevers');

        JHtmlSidebar::addFilter(
            JText::_('JOPTION_SELECT_PUBLISHED'),
            'filter_statebv',
            JHtml::_('select.options', $selectstate, 'value', 'text', $this->state->get('filter.state'), true)
        );
    }
    else {
        $selectstate = Tewebdetails::stateselector();
        $state_list = JHTML::_('select.genericList', $selectstate, 'filter_statebv', 'class="inputbox" size="1" onchange="this.form.submit()"', 'value', 'text', $this->state->get('filter.state') );
        $this->assignRef('state_list', $state_list);
    }
}

/**
     * Returns an array of fields the table can be sorted by
     *
     * @return  array  Array containing the field name to sort by as the key and display text as value
     *
     * @since   3.0
     */
    protected function getSortFields()
    {
        return array(
            'published' => JText::_('JSTATUS'),
            'id' => JText::_('JGRID_HEADING_ID'),
            'name' => JText::_('COM_PREACHIT_ADMIN_NAMELIST'),
            'code' => JText::_('COM_PREACHIT_ADMIN_CODELIST')
        );
    }

}