<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
$user	= JFactory::getUser();
$option = JRequest::getCmd('option');
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/additional.php');	
$translate = PIHelperadditional::translate();
$listOrder    = $this->escape($this->state->get('list.ordering'));
$listDirn    = $this->escape($this->state->get('list.direction'));

//headings in JText
 
$head_id = Jtext::_('COM_PREACHIT_ADMIN_IDLIST');
 $head_published = JText::_('COM_PREACHIT_ADMIN_STATELIST');
 $head_order = JText::_('COM_PREACHIT_ADMIN_ORDERLIST');
 $head_name = JText::_('COM_PREACHIT_ADMIN_NAMELIST');
 $head_role = JText::_('COM_PREACHIT_ADMIN_ROLELIST');

?>
<script type="text/javascript" >

function submitbutton(pressbutton) {
	if (pressbutton == 'teachers.saveorder') 
	{
		var n = <?php echo count( $this->rows ); ?>;
		var fldName = 'cb';
		var f = document.adminForm;
		var c = 1;
		var n2 = 0;
		for (i=0; i < n; i++) {
		cb = eval( 'f.' + fldName + '' + i );
		if (cb) {
		cb.checked = c;
		n2++;
		}
		}
		if (c) {
			document.adminForm.boxchecked.value = n2;
		} else {
			document.adminForm.boxchecked.value = 0;
		} 
		Joomla.submitform(pressbutton);
	}
	else {Joomla.submitform(pressbutton);}
}
</script>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<table width="100%">
<tr>
<td><?php echo $this->lang_list; ?>
<?php echo $this->state_list; ?></td>
</tr></table>
<table class="adminlist table table-striped">
<thead>
<tr>
<th width="20">
<input type="checkbox" name="toggle"
value="" onclick="checkAll(<?php echo count( $this->rows ); ?>);" />
</th>
<th width="5%" nowrap="nowrap"><?php echo JHTML::_( 'grid.sort',$head_id,'id', $listDirn, $listOrder ); ?></th>
<th width="5%" nowrap="nowrap"><?php echo JHTML::_('grid.sort',$head_published,'published',$listDirn,$listOrder); ?></th>
<th width="5%"><?php echo JHtml::_('grid.sort', 'JFEATURED', 'featured', $listDirn, $listOrder, NULL, 'desc'); ?></th>
<th width ="80"><?php echo JHTML::_('grid.sort',$head_order,'ordering',$listDirn,$listOrder); ?></th>
<th width ="1%"><a title="Save Order" href="#" onclick="submitbutton('teachers.saveorder')">
<img alt="Save Order" src="../media/preachit/images/filesave.png">
</a></th>
<th width="18%"><?php echo JHTML::_('grid.sort',$head_name,'lastname, teacher_name',$listDirn,$listOrder); ?></th>
<th width="14%"><?php echo JHTML::_('grid.sort',$head_role,'teacher_role',$listDirn,$listOrder); ?></th>
<th width="40%"><?php echo JText::_('COM_PREACHIT_ADMIN_DESCRIPTIONLIST');?></th>
<th width="8%"><?php echo JText::_('JGRID_HEADING_LANGUAGE'); ?></th>
</tr>
</thead>
<?php
jimport('joomla.filter.output');
$k = 0;
for ($i=0, $n=count( $this->rows ); $i < $n; $i++)
{
$row = &$this->rows[$i];
$checked = JHTML::_('grid.id', $i, $row->id );
$canCheckin    = $user->authorise('core.manage',        'com_checkin') || $row->checked_out == $user->id || $row->checked_out == 0;
$canChange  = $user->authorise('core.edit.state', 'com_preachit') && $canCheckin;
// add language code if multilanguage mode
if ($translate && $row->language != '*')
{$row->lastname = $row->lastname .'-'. $row->language;}

// determine state column
$published = JHtml::_('jgrid.published', $row->published, $i, 'teachers.', $user->authorise('core.edit.state', 'com_preachit'), 'cb');

$link = JFilterOutput::ampReplace( 'index.php?option=' . $option . '&task=teacher.edit&id='. $row->id );

//determine name column
if ($user->authorise('core.edit', 'com_preachit') || $user->authorise('core.edit.own', 'com_preachit') && $user->id == $row->user) 
{
	$name = '<a href="'.$link.'">'.$row->teacher_name.' '.$row->lastname.'</a>';}
else {$name = $row->teacher_name.' '.$row->lastname;}

// get language title

$db = JFactory::getDBO();
	$query = "SELECT ".$db->quoteName('title')."
    FROM ".$db->quoteName('#__languages')."
    WHERE ".$db->quoteName('lang_code')." = ".$db->quote($row->language).";
  ";
	$db->setQuery($query);
	$language_title = $db->loadResult();
	
	// get checked out name

	$query = "SELECT ".$db->quoteName('name')."
    FROM ".$db->quoteName('#__users')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($row->checked_out).";
  ";
	$db->setQuery($query);
	$editor = $db->loadResult();

?>
<tr class="<?php echo "row$k"; ?>">
<td>
<?php echo $checked; ?>
</td>
<td align="center">
<?php echo $row->id; ?></td>
<td align="center">
<?php echo $published;?>
</td>
<td class="center">
                    <?php echo JHtml::_('preachitadministrator.featured25', $row->featured, $i, 'teachers.', $canChange); ?>
                </td>
<td class="order" colspan="2" style="min-width: 85px;">
<?php if ($listOrder == 'ordering')
{?><span><?php echo $this->pagination->orderUpIcon($i, true, 'teachers.orderup', 'JLIB_HTML_MOVE_UP', true); ?></span>
<span><?php echo $this->pagination->orderDownIcon($i, $this->pagination->total, true, 'teachers.orderdown', 'JLIB_HTML_MOVE_DOWN', true); ?></span><?php } ?>
<input type="text" style="text-align: center" class="text_area" value="<?php echo $row->ordering;?>" size="5" name="order[]">
</td>
<td>
<?php if ($row->checked_out) : ?>
    <?php echo JHtml::_('jgrid.checkedout', $i, $editor, $row->checked_out_time, 'teachers.', $canCheckin); ?>
<?php endif; ?>
<?php echo $name; ?>
</td>
<td>
<?php echo $row->teacher_role; ?>
</td>
<td>
<?php 
$row->teacher_description = strip_tags($row->teacher_description);
echo JString::substr($row->teacher_description, 0, 149); ?>
</td>
<td class="center">
<?php if ($row->language=='*'):?>
<?php echo JText::alt('JALL','language'); ?>
<?php else:?>
<?php echo $language_title ? $this->escape($language_title) : JText::_('JUNDEFINED'); ?>
<?php endif;?>
</td>
</tr>
<?php
$k = 1 - $k;
}
$col = 9;
?>
	      <tfoot><tr>
	      <td colspan="<?php echo $col;?>"> <?php echo $this->pagination->getListFooter(); ?> </td></tr>
	      </tfoot>
</table>
<div style="text-align: center; padding-top: 5px;">
<?php $abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/lib/preachit.footer.php');?>
<?php echo PIfooter::footer();?></div>
<!-- /Footer -->
<?php echo JHTML::_( 'form.token' ); ?>
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="view" value="teachers" />
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
</form>