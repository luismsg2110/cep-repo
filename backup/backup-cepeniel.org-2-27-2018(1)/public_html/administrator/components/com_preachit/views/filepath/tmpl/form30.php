<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$option = JRequest::getCmd('option');
JText::script('COM_PREACHIT_FIELDS_INVALID');
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/tooltips.php');
$user	= JFactory::getUser();
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.chosen', 'select');
$document = JFactory::getDocument();      
$document->addScript(JURI::root() . 'media/preachit/js/pisubmitbutton.js');
?>
<form class ="form-validate" action="index.php" method="post" name="adminForm" id="adminForm">
    <div class="form-horizontal">
        <fieldset>
            <ul class="nav nav-tabs">
                <li class="active"><a href="#main" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_ADMIN_FOLDERS_LEGEND');?></a></li>
                <li><a href="#ftp" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_ADMIN_FTPHEAD');?></a></li>
                <li><a href="#aws" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_ADMIN_AWS');?></a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="main">
                <?php
                    foreach ($this->form->getFieldset("maininfo") as $field):
                            ?>
                              <?php 
                                    if ($field->hidden):
                                        echo $field->input;
                                    elseif (!$user->authorise('core.edit.state', 'com_preachit') && $field->name == 'jform[published]'):
                                                continue;
                                    else:
                                    ?>
                                    <div class="control-group">
                                        <div class="control-label">
                                            <?php echo $field->label; ?>   
                                        </div>
                                        <div class="controls">     
                                            <?php echo $field->input; ?>
                                            <?php if ($field->name == 'jform[server]')
                                            {echo PIHelpertooltips::server();}?>
                                            <?php if ($field->name == 'jform[folder]')
                                            {echo PIHelpertooltips::folder();}?>
                                        </div>
                                    </div>
                                    <?php
                                    endif;
                               
                                ?>
                            <?php
                            endforeach;?>  
                  </div>  
                  <div class="tab-pane" id="ftp">
                  <?php
                    foreach ($this->form->getFieldset("ftptab") as $field):
                            ?>
                              <?php 
                                    if ($field->hidden):
                                        echo $field->input;
                                    else:
                                    ?>
                                    <div class="control-group">
                                        <div class="control-label">
                                            <?php echo $field->label; ?>
                                        </div>
                                        <div class="controls">       
                                            <?php echo $field->input; ?>
                                        </div>
                                  </div>
                                    <?php
                                    endif;
                               
                                ?>
                            <?php
                            endforeach;?>
                    </div>
                    <div class="tab-pane" id="aws">
                    <?php
                    foreach ($this->form->getFieldset("awstab") as $field):
                            ?>
                              <?php 
                                    if ($field->hidden):
                                        echo $field->input;
                                    else:
                                    ?>
                                    <div class="control-group">
                                        <div class="control-label">
                                            <?php echo $field->label; ?>
                                        </div>
                                        <div class="controls">       
                                            <?php echo $field->input; ?>
                                        </div>
                                  </div>
                                    <?php
                                    endif;
                               
                                ?>
                            <?php
                            endforeach;?>
                    </div>
                </div>
            </fieldset>
        </div>
<!-- Footer -->
<div style="text-align: center">
<?php $abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/lib/preachit.footer.php');?>
<?php echo PIfooter::footer();?></div>
<!-- /Footer -->
<input type="hidden" name="controller" value="filepath" />
<input type="hidden" name="id" value="<?php echo $this->item->id; ?>" />
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="task" value ="" />
<?php if (!$user->authorise('core.edit.state', 'com_preachit')) {?>
<input type="hidden" name="jform[published]" id="jform_published" value="<?php echo $this->item->published;?>" />
<?php }?>
<?php echo JHTML::_( 'form.token' ); ?>
</form>


