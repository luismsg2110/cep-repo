<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
JText::script('COM_PREACHIT_FIELDS_INVALID');
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/tooltips.php');
$option = JRequest::getCmd('option');
$user	= JFactory::getUser();
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.chosen', 'select');
$document = JFactory::getDocument();      
$document->addScript(JURI::root() . 'media/preachit/js/pisubmitbutton.js');
?>
<form class ="form-validate" action="index.php" method="post" name="adminForm" id="adminForm">
    <fieldset>
        <legend><?php echo JText::_('COM_PREACHIT_ADMIN_SHARE_LEGEND');?></legend>
        <div class="form-horizontal">
        <?php
            foreach ($this->form->getFieldset("maininfo") as $field):
                    ?>
                      <?php 
                            if ($field->hidden):
                                echo $field->input;
                            elseif (!$user->authorise('core.edit.state', 'com_preachit') && $field->name == 'jform[published]'):
                                        continue;
                            else:
                            ?>
                            <div class="control-group">
                                    <div class="control-label">
                                        <?php echo $field->label; ?>
                                    </div>
                                    <div class="controls">       
                                        <?php echo $field->input; ?>
                                        <?php if ($field->name == 'jform[code]')
                                        {echo PIHelpertooltips::sharedef();}?>
                                        <?php if ($field->name == 'jform[script]')
                                        {echo PIHelpertooltips::sharescript();}?>
                                    </div>
                              </div>
                            <?php
                            endif;
                       
                        ?>
                    <?php
                    endforeach;?>
        </div>
    </fieldset>
<!-- Footer -->
<div style="text-align: center">
<?php $abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/lib/preachit.footer.php');?>
<?php echo PIfooter::footer();?></div>
<!-- /Footer -->
<input type="hidden" name="task" value="" />
<input type="hidden" name="id" value="<?php echo $this->item->id; ?>" />
<input type="hidden" name="controller" value="share" />
<input type="hidden" name="option" value="<?php echo $option;?>" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>




