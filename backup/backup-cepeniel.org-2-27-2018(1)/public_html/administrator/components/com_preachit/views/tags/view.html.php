<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport ('teweb.details.tags');
class PreachitViewTags extends JViewLegacy
{
function display($tpl = null)
{
$document = JFactory::getDocument();
$document->addStyleSheet('../media/preachit/css/preachit.css');
$app = JFactory::getApplication();
$option = JRequest::getCmd('option');

// get data
$this->state  = $this->get('State');
$this->rows = $this->get('data');
$this->pagination = $this->get('Pagination');

// load menu
PreachitHelper::addSubmenu('tags');

$this->addToolbar();

if (Tewebcheck::getJversion() >= 3.0)
{
    $this->setLayout('default30');
    $this->sidebar = JHtmlSidebar::render();   
}

parent::display($tpl);
}

/**
     * Add the page title and toolbar.
     *
     * @since    1.6
     */
protected function addToolbar()
{
    $user    = JFactory::getUser();    
    JToolBarHelper::title( JText::_( 'COM_PREACHIT_ADMIN_TITLE_TAGS' ), 'tag.png');
    if ($user->authorise('core.edit', 'com_preachit'))  {
    JToolBarHelper::editList('tag.edit','JTOOLBAR_EDIT');
    }
    JToolBarHelper::divider();
    if ($user->authorise('core.delete', 'com_preachit')) {
    JToolBarHelper::deleteList('', 'tags.remove','TE_TOOLBAR_DELETE');
    }
    JToolBarHelper::divider();
    if ($user->authorise('core.admin', 'com_preachit'))  {
    JToolBarHelper::preferences('com_preachit', '550', '900');
    }
    JToolBarHelper::help('pihelp', 'com_preachit');
}

/**
     * Returns an array of fields the table can be sorted by
     *
     * @return  array  Array containing the field name to sort by as the key and display text as value
     *
     * @since   3.0
     */
    protected function getSortFields()
    {
        return array(
            'published' => JText::_('JSTATUS'),
            'id' => JText::_('JGRID_HEADING_ID'),
            'name' => JText::_('COM_PREACHIT_ADMIN_NAMELIST'),
            'count' => JText::_('COM_PREACHIT_ADMIN_PLAYER_COUNTLIST')
        );
    }

}