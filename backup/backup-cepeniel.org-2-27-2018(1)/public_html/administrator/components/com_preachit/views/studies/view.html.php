<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
class PreachitViewStudies extends JViewLegacy
{
function display($tpl = null)
{
$app = JFactory::getApplication();
 	$option = JRequest::getCmd('option');
$document = JFactory::getDocument();
$document->addStyleSheet('../media/preachit/css/preachit.css');

// get Joomla version to decide which form and method

$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/additional.php');
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/scripture.php');
$params = Tewebdetails::getparams('com_preachit');
$checkin = $params->get('checkin', 0);
$this->assignRef('checkin', $checkin);

$this->state  = $this->get('State');
$this->rows = $this->get('data');
$this->pagination = $this->get('Pagination');

// load menu
PreachitHelper::addSubmenu('messages');

$this->addToolbar();

if (Tewebcheck::getJversion() >= 3.0)
{
    $layout = JRequest::getVar('layout', '');
    if ($layout == 'modal')
    {$this->setLayout('modal30');}
    else {$this->setLayout('default30');}
    $this->sidebar = JHtmlSidebar::render();   
}

$dateformat = 'd F, Y';
$this->assignRef('dateformat', $dateformat);
parent::display($tpl);
}

/**
     * Add the page title and toolbar.
     *
     * @since    1.6
     */
protected function addToolbar()
{
    $user    = JFactory::getUser();     
    JToolBarHelper::title( JText::_( 'COM_PREACHIT_ADMIN_TITLE_MESSAGES' ), 'message.png');
    if ($user->authorise('core.create', 'com_preachit'))  {
    JToolBarHelper::addNew('study.add','JTOOLBAR_NEW');}
    if ($user->authorise('core.edit', 'com_preachit'))  {
    JToolBarHelper::editList('study.edit','JTOOLBAR_EDIT');
    }
    JToolBarHelper::divider();
    if ($user->authorise('core.edit.state', 'com_preachit')) 
    {
    JToolBarHelper::custom('studies.publish', 'publish.png', 'publish_f2.png','JTOOLBAR_PUBLISH', true);
    JToolBarHelper::custom('studies.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
    JToolbarHelper::custom('studies.featured', 'featured.png', 'featured_f2.png', 'JFEATURED', true);
    }
    JToolBarHelper::divider();
    if ($this->state->get('filter.state') == -2 && $user->authorise('core.delete', 'com_preachit')) {
                JToolBarHelper::deleteList('', 'studies.delete','TE_TOOLBAR_EMPTY_TRASH');
            } else if ($user->authorise('core.edit.state', 'com_preachit')) {
                JToolBarHelper::trash('studies.trash','TE_TOOLBAR_TRASH');
            }
    JToolBarHelper::divider();
    if ($user->authorise('core.admin', 'com_preachit'))  {
        if (Tewebcheck::getJversion() >= 3.0)
        {
            JToolBarHelper::custom('studies.resetall', 'loop.png', 'loop.png', 'COM_PREACHIT_RESET_HITS', false);
            JToolBarHelper::custom('studies.resetdownloads', 'loop.png', 'loop.png', 'COM_PREACHIT_RESET_DOWNLOADS', false);
        }
        else {
            JToolBarHelper::custom('studies.resetall', 'hits32.png', 'hits32.png', 'COM_PREACHIT_RESET_HITS', false);
            JToolBarHelper::custom('studies.resetdownloads', 'down32.png', 'down32.png', 'COM_PREACHIT_RESET_DOWNLOADS', false);
        }
    JToolBarHelper::divider();
    JToolBarHelper::preferences('com_preachit', '550', '900');
    }
    JToolBarHelper::help('pihelp', 'com_preachit');
    
    // get lists from db
    
    $db = JFactory::getDBO();
    $this->booklist = PIHelperscripture::getbooklist();
    
    // teacher list
    $db->setQuery('SELECT id AS value, teacher_name AS text, lastname AS text2, language AS lang FROM #__piteachers');
    $tlists = $db->loadObjectList();
    $this->teacherlist = array();
    $i=0;
    foreach($tlists as $tlist) 
    {
        $translate = PIHelperadditional::translate();
        if ($tlist->text != '')
        {
            $name = $tlist->text.' '.$tlist->text2;
        }
        else {$name = $tlist->text2;}
        if ($translate)
        {
            if ($tlist->lang != '*') 
            {
                $name = $name.' - '.$tlist->lang;
            }
        }
        $this->teacherlist[$i] = new stdClass();
        $this->teacherlist[$i]->value = $tlist->value;
        $this->teacherlist[$i]->text = $name; $i++;
    }
    
    // series list

    $db->setQuery('SELECT id AS value, series_name AS text FROM #__piseries ORDER by series_name');
    $this->serieslist = $db->loadObjectList();
    
    //ministry list

    $db->setQuery('SELECT id AS value, ministry_name AS text FROM #__piministry ORDER by ministry_name');
    $this->ministrylist = $db->loadObjectList();
    
    // year list
    
    $query = " SELECT DISTINCT date_format(study_date, '%Y') AS value, date_format(study_date, '%Y') AS text "
        . ' FROM #__pistudies '
        . ' ORDER BY value DESC';
        $db->setQuery( $query );
        $this->yearlist = $db->loadObjectList();

    if (Tewebcheck::getJversion() >= 3.0)
    {
        $selectstate = Tewebdetails::stateselector();
        
        JHtmlSidebar::setAction('index.php?option=com_preachit&view=studies');

        JHtmlSidebar::addFilter(
            JText::_('JOPTION_SELECT_PUBLISHED'),
            'filter_statemes',
            JHtml::_('select.options', $selectstate, 'value', 'text', $this->state->get('filter.state'), true)
        );
        
        JHtmlSidebar::addFilter(
            JText::_('JOPTION_SELECT_LANGUAGE'),
            'filter_language',
            JHtml::_('select.options', JHtml::_('contentlanguage.existing', true, true), 'value', 'text', $this->state->get('filter.language'))
        );
        
        JHtmlSidebar::addFilter(
            JText::_('COM_PREACHIT_ADMIN_BOOK_SELECT'),
            'filter_book',
            JHtml::_('select.options', $this->booklist, 'value', 'text', $this->state->get('filter.book'), true)
        );
        
        JHtmlSidebar::addFilter(
            JText::_('COM_PREACHIT_ADMIN_TEACHER_SELECT'),
            'filter_teacher',
            JHtml::_('select.options', $this->teacherlist, 'value', 'text', $this->state->get('filter.teacher'), true)
        );
        
        JHtmlSidebar::addFilter(
            JText::_('COM_PREACHIT_ADMIN_SERIES_SELECT'),
            'filter_series',
            JHtml::_('select.options', $this->serieslist, 'value', 'text', $this->state->get('filter.series'), true)
        );
        
        JHtmlSidebar::addFilter(
            JText::_('COM_PREACHIT_ADMIN_MINISTRY_SELECT'),
            'filter_ministry',
            JHtml::_('select.options', $this->ministrylist, 'value', 'text', $this->state->get('filter.ministry'), true)
        );
        
        JHtmlSidebar::addFilter(
            JText::_('COM_PREACHIT_ADMIN_YEAR_SELECT'),
            'filter_year',
            JHtml::_('select.options', $this->yearlist, 'value', 'text', $this->state->get('filter.year'), true)
        );
    }
    else {
        $selectstate = Tewebdetails::stateselector();
        $state_list = JHTML::_('select.genericList', $selectstate, 'filter_statemes', 'class="inputbox" size="1" onchange="this.form.submit()"', 'value', 'text', $this->state->get('filter.state') );
        $this->assignRef('state_list', $state_list);
        // language list
        jimport( 'joomla.language.helper' );
        $langlist = JHtml::_('contentlanguage.existing', true, true);
        $defaultlang = array(
        array('value' => '', 'text' => JText::_('JOPTION_SELECT_LANGUAGE')),
        );
        $langlist = array_merge( $defaultlang, $langlist );
        $lang_list = JHTML::_('select.genericList', $langlist, 'filter_language', 'class="inputbox" size="1" onchange="this.form.submit()"', 'value', 'text', $this->state->get('filter.language') );
        $this->assignRef('lang_list', $lang_list);
        
        //Book list

        $selectbook = array(
        array('value' => '', 'text' => JText::_('COM_PREACHIT_ADMIN_BOOK_SELECT')),
        );
        $booklist         = array_merge( $selectbook, $this->booklist );
        $study_book = JHTML::_('select.genericList', $booklist, 'filter_book', 'class="inputbox" size="1" onchange="this.form.submit()"', 'value', 'text', $this->state->get('filter.book') );
        $this->assignRef('study_book', $study_book);
        
        // teacher list
        $selectteach = array(
        array('value' => '', 'text' => JText::_('COM_PREACHIT_ADMIN_TEACHER_SELECT'), 'lang' => ''),
        );
        $teacherlist = array_merge( $selectteach, $this->teacherlist );
        $teacher_list = JHTML::_('select.genericList', $teacherlist, 'filter_teacher', 'class="inputbox" size="1" onchange="this.form.submit()"', 'value', 'text', $this->state->get('filter.teacher') );
        $this->assignRef('teacher_list',$teacher_list );
        
        // series list
        $selectseries = array(
        array('value' => '', 'text' => JText::_('COM_PREACHIT_ADMIN_SERIES_SELECT')),
        );
        $serieslist = array_merge( $selectseries,  $this->serieslist);
        $series_list = JHTML::_('select.genericList', $serieslist, 'filter_series', 'class="inputbox" size="1" onchange="this.form.submit()"', 'value', 'text', $this->state->get('filter.series') );
        $this->assignRef('series_list', $series_list);
        
        // ministry list
        $selectseries = array(
        array('value' => '', 'text' => JText::_('COM_PREACHIT_ADMIN_MINISTRY_SELECT')),
        );
        $ministrylist = array_merge( $selectseries, $this->ministrylist );
        $ministry_list = JHTML::_('select.genericList', $ministrylist, 'filter_ministry', 'class="inputbox" size="1" onchange="this.form.submit()"', 'value', 'text', $this->state->get('filter.ministry') );
        $this->assignRef('ministry_list', $ministry_list);
        
        $selectyears = array(
        array('value' => '', 'text' => JText::_('COM_PREACHIT_ADMIN_YEAR_SELECT')),
        );
        $yearslist = array_merge( $selectyears, $this->yearlist );
        $years_list = JHTML::_('select.genericList', $yearslist, 'filter_year', 'class="inputbox" size="1" onchange="this.form.submit()"', 'value', 'text', $this->state->get('filter.year') );
        $this->assignRef('years_list', $years_list);
    }
}

/**
     * Returns an array of fields the table can be sorted by
     *
     * @return  array  Array containing the field name to sort by as the key and display text as value
     *
     * @since   3.0
     */
    protected function getSortFields()
    {
        return array(
            'ordering' => JText::_('JGRID_HEADING_ORDERING'),
            'published' => JText::_('JSTATUS'),
            'id' => JText::_('JGRID_HEADING_ID'),
            'study_name' => JText::_('COM_PREACHIT_ADMIN_NAMELIST'),
            'study_date' => JText::_('COM_PREACHIT_ADMIN_DATELIST'),
            'teacher' => JText::_('COM_PREACHIT_ADMIN_TEACHERLIST'),
            'hits' => JText::_('COM_PREACHIT_ADMIN_HITLIST'),
            'downloads' => JText::_('COM_PREACHIT_ADMIN_DOWNLOADSLIST'),
            'series' => JText::_('COM_PREACHIT_ADMIN_SERIESLIST'),
            'featured' => JText::_('JFEATURED')
        );
    }

}