<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
$user	= JFactory::getUser();
$option = JRequest::getCmd('option');
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/message-info.php');
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/scripture.php');
$listOrder    = $this->escape($this->state->get('list.ordering'));
$listDirn    = $this->escape($this->state->get('list.direction'));
?>
<script language="javascript" type="text/javascript">
	Joomla.submitbutton = function(task)
	{
	if (task == "")
        		{
                return false;
        		}
   else if (task == "studies.resetall") 
	{
		if(confirm("<?php echo JText::_('COM_PREACHIT_TEMPLATE_MANAGER_RESET_WARNING');?>"))
		{Joomla.submitform(task);}
		} 
	else if (task == "studies.resetdownloads") 
	{
		if(confirm("<?php echo JText::_('COM_PREACHIT_TEMPLATE_MANAGER_DOWNLOADS_WARNING');?>"))
		{Joomla.submitform(task);}
		} 
	
 else {
			Joomla.submitform(task);
		}
}
</script>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<table width="100%">
<tr>
<td> <?php echo $this->study_book;?>
	<?php echo $this->teacher_list; ?>
	<?php echo $this->series_list; ?>
	<?php echo $this->ministry_list; ?>
	<?php echo $this->years_list; ?> 
	<?php echo $this->lang_list; ?>
	<?php echo $this->state_list; ?></td>
</tr></table>
 <?php
 //headings in JText
 
 $head_id = Jtext::_('COM_PREACHIT_ADMIN_IDLIST');
 $head_published = JText::_('COM_PREACHIT_ADMIN_STATELIST');
 $head_date = JText::_('COM_PREACHIT_ADMIN_DATELIST');
 $head_title = JText::_('COM_PREACHIT_ADMIN_NAMELIST');
 $head_script = JText::_('COM_PREACHIT_ADMIN_SCRIPTURELIST');
 $head_teacher = JText::_('COM_PREACHIT_ADMIN_TEACHERLIST');
 $head_series = JText::_('COM_PREACHIT_ADMIN_SERIESLIST');
 $head_hits = JText::_('COM_PREACHIT_ADMIN_HITLIST');
 $head_downloads = JText::_('COM_PREACHIT_ADMIN_DOWNLOADSLIST');
 $head_podpub = JText::_('COM_PREACHIT_ADMIN_PODPUBLIST');
 ?>
<table class="adminlist table table-striped">
<thead>
<tr>
<th width="20">
<input type="checkbox" name="toggle"
value="" onclick="checkAll(<?php echo count( $this->rows ); ?>);" />
</th>
<th width="40"><?php echo JHTML::_( 'grid.sort',$head_id,'id', $listDirn, $listOrder); ?></th>
<th width="8%" nowrap="nowrap"><?php echo JHTML::_('grid.sort',$head_published,'published',$listDirn,$listOrder); ?></th>
<th width="5%"><?php echo JHtml::_('grid.sort', 'JFEATURED', 'featured', $listDirn, $listOrder, NULL, 'desc'); ?></th>
<th width="12%"><?php echo JHTML::_('grid.sort',$head_date,'study_date',$listDirn,$listOrder); ?></th>
<th width="20%"><?php echo JHTML::_( 'grid.sort', $head_title, 'study_name', $listDirn, $listOrder); ?></th>
<th width="14%"><?php echo JHTML::_( 'grid.sort',$head_script, 'study_book', $listDirn, $listOrder); ?></th>
<th width="14%"><?php echo JHTML::_( 'grid.sort',$head_teacher , 'teacher', $listDirn, $listOrder); ?></th>
<th width="14%"><?php echo JHTML::_( 'grid.sort', $head_series, 'series', $listDirn, $listOrder); ?></th>
<th width="8%"><?php echo JHtml::_('grid.sort', 'JGRID_HEADING_LANGUAGE', 'language', $listDirn, $listOrder); ?></th>
<th width="5%"><?php echo JHTML::_( 'grid.sort', $head_hits, 'hits', $listDirn, $listOrder); ?></th>
<th width="5%"><?php echo JHTML::_( 'grid.sort', $head_downloads, 'downloads', $listDirn, $listOrder); ?></th>
<th><?php echo $head_podpub;?></th>
</tr>
</thead>
<?php
jimport('joomla.filter.output');
$k = 0;
for ($i=0, $n=count( $this->rows ); $i < $n; $i++)
{
$row = &$this->rows[$i];
$checked = JHTML::_('grid.id', $i, $row->id );
$canCheckin    = $user->authorise('core.manage',        'com_checkin') || $row->checked_out == $user->id || $row->checked_out == 0;
$canChange  = $user->authorise('core.edit.state', 'com_preachit') && $canCheckin;
// determine state column
if ($row->publish_down == '0000-00-00 00:00:00' || $row->publish_down == '')
{$publishdown = '0000-00-00 00:00:00';}
else {$publishdown = $row->publish_down;}
$published = JHtml::_('jgrid.published', $row->published, $i, 'studies.', $user->authorise('core.edit.state', 'com_preachit'), 'cb', $row->publish_up, $publishdown);

$link = JFilterOutput::ampReplace( 'index.php?option=' . $option . '&task=study.edit&id='. $row->id );

//determine name column

if ($user->authorise('core.edit', 'com_preachit') || $user->authorise('core.edit.own', 'com_preachit') && $user->id == $row->user) 
{
$date = '<a href="'.$link.'">'.JHTML::Date($row->study_date, $this->dateformat).'</a>';}
else {$date = JHTML::Date($row->study_date, $this->dateformat);}

// determin podpub column

if ($row->podpublish_down == '0000-00-00 00:00:00' || $row->podpublish_down == '')
{$podpublishdown = '0000-00-00 00:00:00';}
else {$podpublishdown = $row->podpublish_down;}

$now = gmdate ( 'Y-m-d H:i:s' );
$nullDate = '0000-00-00 00:00:00';

if (($row->published == 1 && ($row->publish_up == $nullDate || $row->publish_up == '' || $row->publish_up <= $now) && ($row->publish_down == $nullDate || $row->publish_down == '' || $row->publish_down >= $now)) && (($row->podpublish_up == $nullDate || $row->podpublish_up == '' || $row->podpublish_up <= $now) && ($row->podpublish_down == $nullDate || $row->podpublish_down == '' || $row->podpublish_down >= $now)))
{
    $podpub = '<img style="width: 16px; height: 16px;" src="../media/preachit/images/success.png">';
}
else {$podpub = '<img style="width: 16px; height: 16px;" src="../media/preachit/images/failure.png">';}

//get teacher name

$teacher = PIHelpermessageinfo::teacher($row->teacher, '', 2);

//get series name

$series = PIHelpermessageinfo::series($row->series, '', 2);

//get scripture

$bibleref = new PIHelperscripture($row);
$scripture = $bibleref->ref1;

// get language title

$db = JFactory::getDBO();
	$query = "SELECT ".$db->quoteName('title')."
    FROM ".$db->quoteName('#__languages')."
    WHERE ".$db->quoteName('lang_code')." = ".$db->quote($row->language).";
  ";
	$db->setQuery($query);
	$language_title = $db->loadResult();

// get checked out name

	$query = "SELECT ".$db->quoteName('name')."
    FROM ".$db->quoteName('#__users')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($row->checked_out).";
  ";
	$db->setQuery($query);
	$editor = $db->loadResult();
?>

<tr class="<?php echo "row$k"; ?>">
<td>
<?php echo $checked; ?>
</td>
<td>
<?php echo $row->id; ?></td>
<td align="center">
<?php echo $published;?>
</td>
<td class="center">
                    <?php echo JHtml::_('preachitadministrator.featured25', $row->featured, $i, 'studies.', $canChange); ?>
                </td>
<td>
<?php if ($row->checked_out) : ?>
    <?php echo JHtml::_('jgrid.checkedout', $i, $editor, $row->checked_out_time, 'studies.', $canCheckin); ?>
<?php endif; ?>
<?php echo $date; ?>
</td>
<td>
<?php echo $row->study_name; ?>
</td>
<td>
<?php echo $scripture; ?>
</td>
<td>
<?php echo $teacher; ?>
</td>
<td>
<?php echo $series; ?>
</td>
<td class="center">
<?php if ($row->language=='*'):?>
<?php echo JText::alt('JALL','language'); ?>
<?php else:?>
<?php echo $language_title ? $this->escape($language_title) : JText::_('JUNDEFINED'); ?>
<?php endif;?>
</td>
<td align="center">
<?php echo $row->hits; ?>
</td>
<td align="center">
<?php echo $row->downloads; ?>
</td>
<td align="center">
<?php echo $podpub; ?>
</td>
</tr>
<?php
$k = 1 - $k;
}
$col = 12;
?>
	      <tfoot><tr>
	      <td colspan="<?php echo $col;?>"> <?php echo $this->pagination->getListFooter(); ?> </td></tr>
 </tfoot>
</table>
<div style="text-align: center; padding-top: 5px;">
<?php $abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/lib/preachit.footer.php');?>
<?php echo PIfooter::footer();?></div>
<!-- /Footer -->
<?php echo JHTML::_( 'form.token' ); ?>
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="view" value="studies" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>
