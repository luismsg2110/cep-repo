<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$option = JRequest::getCmd('option');
$function = JRequest::getVar('function', 'jSelectChart');
JHTML::_( 'behavior.framework' );
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/message-info.php');
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/scripture.php');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');
$user        = JFactory::getUser();
$userId        = $user->get('id');
$listOrder    = $this->escape($this->state->get('list.ordering'));
$listDirn    = $this->escape($this->state->get('list.direction'));
$trashed    = $this->state->get('filter.published') == -2 ? true : false;
?>

<form action="index.php" method="post" id="adminForm" name="adminForm">
    <fieldset class="filter clearfix">
        <div class="filters">
            <select name="filter_published" class="input-medium" onchange="this.form.submit()">
                <option value=""><?php echo JText::_('JOPTION_SELECT_PUBLISHED');?></option>
                <?php echo JHtml::_('select.options', Tewebdetails::stateselector(), 'value', 'text', $this->state->get('filter.state'), true);?>
            </select>
            <select name="filter_year" class="input-medium" onchange="this.form.submit()">
                <option value=""><?php echo JText::_('COM_PREACHIT_ADMIN_YEAR_SELECT');?></option>
                <?php echo JHtml::_('select.options', $this->yearlist, 'value', 'text', $this->state->get('filter.year'));?>
            </select>
            <select name="filter_book" class="input-medium" onchange="this.form.submit()">
                <option value=""><?php echo JText::_('COM_PREACHIT_ADMIN_BOOK_SELECT');?></option>
                <?php echo JHtml::_('select.options', $this->booklist, 'value', 'text', $this->state->get('filter.book'));?>
            </select>
            <select name="filter_teacher" class="input-medium" onchange="this.form.submit()">
                <option value=""><?php echo JText::_('COM_PREACHIT_ADMIN_TEACHER_SELECT');?></option>
                <?php echo JHtml::_('select.options', $this->teacherlist, 'value', 'text', $this->state->get('filter.teacher'));?>
            </select>
            <select name="filter_series" class="input-medium" onchange="this.form.submit()">
                <option value=""><?php echo JText::_('COM_PREACHIT_ADMIN_SERIES_SELECT');?></option>
                <?php echo JHtml::_('select.options', $this->serieslist, 'value', 'text', $this->state->get('filter.series'));?>
            </select>
            <select name="filter_language" class="input-medium" onchange="this.form.submit()">
                <option value=""><?php echo JText::_('JOPTION_SELECT_LANGUAGE');?></option>
                <?php echo JHtml::_('select.options', JHtml::_('contentlanguage.existing', true, true), 'value', 'text', $this->state->get('filter.language'));?>
            </select>
        </div>
    </fieldset>
 <table class="adminlist table table-striped">
    <thead>
        <tr>
            <th width="1%" class="nowrap center">
                <?php echo JHtml::_('grid.sort', 'JSTATUS', 'published', $listDirn, $listOrder); ?>
            </th>
            <th>
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_DATELIST', 'study_date', $listDirn, $listOrder); ?>
            </th>
            <th width="20%">
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_NAMELIST', 'study_name', $listDirn, $listOrder); ?>
            </th>
            <th width="15%" class="hidden-phone">
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_SCRIPTURELIST', 'study_book', $listDirn, $listOrder); ?>
            </th>
            <th width="15%" class="hidden-phone">
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_TEACHERLIST', 'teacher', $listDirn, $listOrder); ?>
            </th>
            <th width="10%" class="hidden-phone">
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_SERIESLIST', 'series', $listDirn, $listOrder); ?>
            </th>
            <th width="8%" class="center nowrap hidden-phone">
                <?php echo JHtml::_('grid.sort', 'JGRID_HEADING_LANGUAGE', 'language', $listDirn, $listOrder); ?>
            </th>
            <th width="1%" class="nowrap center hidden-phone">
                        <?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'id', $listDirn, $listOrder); ?>
            </th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <td colspan="12"> <?php echo $this->pagination->getListFooter(); ?> </td>
        </tr>
    </tfoot>
    <tbody>
        <?php
        foreach ($this->rows as $i => $item)
        {
        $ordering  = ($listOrder == 'ordering');
        $canCreate  = $user->authorise('core.create', 'com_preachit');
        $canEdit    = $user->authorise('core.edit', 'com_preachit');
        $canCheckin = $user->authorise('core.manage',     'com_checkin') || $item->checked_out == $userId || $item->checked_out == 0;
        $canChange  = $user->authorise('core.edit.state', 'com_preachit') && $canCheckin;
        
        //get teacher name

        $teacher = PIHelpermessageinfo::teacher($item->teacher, '', 2);

        //get series name

        $series = PIHelpermessageinfo::series($item->series, '', 2);

        //get scripture

        $bibleref = new PIHelperscripture($item);
        $scripture = $bibleref->ref1;
        
        // determin podpub column

        if ($item->podpublish_down == '0000-00-00 00:00:00' || $item->podpublish_down == '')
        {$podpublishdown = '0000-00-00 00:00:00';}
        else {$podpublishdown = $item->podpublish_down;}

        $now = gmdate ( 'Y-m-d H:i:s' );
        $nullDate = '0000-00-00 00:00:00';

        if (($item->published == 1 && ($item->publish_up == $nullDate || $item->publish_up == '' || $item->publish_up <= $now) && ($item->publish_down == $nullDate || $item->publish_down == '' || $item->publish_down >= $now)) && (($item->podpublish_up == $nullDate || $item->podpublish_up == '' || $item->podpublish_up <= $now) && ($item->podpublish_down == $nullDate || $item->podpublish_down == '' || $item->podpublish_down >= $now)))
        {
            $podpub = '<img style="width: 16px; height: 16px;" src="../media/preachit/images/success.png">';
        }
        else {$podpub = '<img style="width: 16px; height: 16px;" src="../media/preachit/images/failure.png">';}
        
        // get language title

        $db = JFactory::getDBO();
            $query = "SELECT ".$db->quoteName('title')."
            FROM ".$db->quoteName('#__languages')."
            WHERE ".$db->quoteName('lang_code')." = ".$db->quote($item->language).";
          ";
            $db->setQuery($query);
            $language_title = $db->loadResult();
        
        ?>
            <tr class="row<?php echo $i % 2; ?>" sortable-group-id="0">
                <td class="center">
                    <?php echo JHtml::_('jgrid.published', $item->published, $i, 'studies.', $canChange, 'cb'); ?>
                </td>
                <td class="nowrap has-context">
                    <div class="pull-left">
                            <a class="pointer" href="#" onclick="if (window.parent) window.parent.<?php echo $function;?>('<?php echo $item->id; ?>', '<?php echo addslashes($item->study_name); ?>');">
                                <?php echo $this->escape(JHTML::Date($item->study_date, $this->dateformat)); ?></a>
                    </div>
                </td>
                <td>
                    <a class="pointer" href="#" onclick="if (window.parent) window.parent.<?php echo $function;?>('<?php echo $item->id; ?>', '<?php echo addslashes($item->study_name); ?>');">
                        <?php echo $item->study_name ?>
                    </a>
                </td>
                <td class="hidden-phone">
                    <?php echo $scripture ?>
                </td>
                <td class="hidden-phone">
                    <?php echo $teacher ?>
                </td>
                <td class="hidden-phone">
                    <?php echo $series ?>
                </td>
                <td class="small center hidden-phone">
                    <?php if ($item->language == '*'):?>
                        <?php echo JText::alt('JALL', 'language'); ?>
                    <?php else:?>
                        <?php echo $language_title ? $this->escape($language_title) : JText::_('JUNDEFINED'); ?>
                    <?php endif;?>
                </td>
                <td class="center hidden-phone">
                        <?php echo $item->id; ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<?php echo JHTML::_( 'form.token' ); ?>
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="view" value="studies" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="tmpl" value="component" />
<input type="hidden" name="layout" value="modal" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
</form>