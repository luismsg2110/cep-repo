<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$user	= JFactory::getUser();
$option = JRequest::getCmd('option');
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/message-info.php');
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/scripture.php');
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');
$user        = JFactory::getUser();
$userId        = $user->get('id');
$listOrder    = $this->escape($this->state->get('list.ordering'));
$listDirn    = $this->escape($this->state->get('list.direction'));
$trashed    = $this->state->get('filter.state') == -2 ? true : false;
?>
<script language="javascript" type="text/javascript">
	Joomla.submitbutton = function(task)
	{
	if (task == "")
        		{
                return false;
        		}
   else if (task == "studies.resetall") 
	{
		if(confirm("<?php echo JText::_('COM_PREACHIT_TEMPLATE_MANAGER_RESET_WARNING');?>"))
		{Joomla.submitform(task);}
		} 
	else if (task == "studies.resetdownloads") 
	{
		if(confirm("<?php echo JText::_('COM_PREACHIT_TEMPLATE_MANAGER_DOWNLOADS_WARNING');?>"))
		{Joomla.submitform(task);}
		} 
	
 else {
			Joomla.submitform(task);
		}
}
</script>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<?php if(!empty( $this->sidebar)): ?>
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
    <div id="j-main-container" class="span10">
<?php else : ?>
    <div id="j-main-container">
<?php endif;?>
    <div id="filter-bar" class="btn-toolbar">
        <div class="btn-group pull-right hidden-phone">
                    <label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?></label>
                    <?php echo $this->pagination->getLimitBox(); ?>
        </div>
    </div>
<table class="adminlist table table-striped" id="articleList">

    <thead>
        <tr>
            <th width="1%" class="hidden-phone">
                <input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
            </th>
            <th width="1%" style="min-width:55px" class="nowrap center">
                <?php echo JHtml::_('grid.sort', 'JSTATUS', 'published', $listDirn, $listOrder); ?>
            </th>
            <th>
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_DATELIST', 'study_date', $listDirn, $listOrder); ?>
            </th>
            <th width="15%">
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_NAMELIST', 'study_name', $listDirn, $listOrder); ?>
            </th>
            <th width="10%" class="hidden-phone">
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_SCRIPTURELIST', 'study_book', $listDirn, $listOrder); ?>
            </th>
            <th width="10%" class="hidden-phone">
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_TEACHERLIST', 'teacher', $listDirn, $listOrder); ?>
            </th>
            <th width="10%" class="hidden-phone">
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_SERIESLIST', 'series', $listDirn, $listOrder); ?>
            </th>
            <th width="8%" class="center nowrap hidden-phone">
                <?php echo JHtml::_('grid.sort', 'JGRID_HEADING_LANGUAGE', 'language', $listDirn, $listOrder); ?>
            </th>
            <th width="5%" class="center hidden-phone">
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_HITSLIST', 'hits', $listDirn, $listOrder); ?>
            </th>
            <th width="5%" class="center hidden-phone">
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_DOWNLOADSLIST', 'downloads', $listDirn, $listOrder); ?>
            </th>
            <th class="center hidden-phone">
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_PODPUBLIST', 'podpub', $listDirn, $listOrder); ?>
            </th>
            <th width="1%" class="nowrap center hidden-phone">
                        <?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'id', $listDirn, $listOrder); ?>
            </th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <td colspan="12"> <?php echo $this->pagination->getListFooter(); ?> </td>
        </tr>
    </tfoot>
    <tbody>
        <?php
        foreach ($this->rows as $i => $item)
        {
        $ordering  = ($listOrder == 'ordering');
        $canCreate  = $user->authorise('core.create', 'com_preachit');
        $canEdit    = $user->authorise('core.edit', 'com_preachit');
        $canCheckin = $user->authorise('core.manage',     'com_checkin') || $item->checked_out == $userId || $item->checked_out == 0;
        $canChange  = $user->authorise('core.edit.state', 'com_preachit') && $canCheckin;
        
        // get checked out name
$db = JFactory::getDBO();
	$query = "SELECT ".$db->quoteName('name')."
    FROM ".$db->quoteName('#__users')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($item->checked_out).";
  ";
	$db->setQuery($query);
	$editor = $db->loadResult();
        
        //get teacher name

        $teacher = PIHelpermessageinfo::teacher($item->teacher, '', 2);

        //get series name

        $series = PIHelpermessageinfo::series($item->series, '', 2);

        //get scripture

        $bibleref = new PIHelperscripture($item);
        $scripture = $bibleref->ref1;
        
        // determin podpub column

        if ($item->podpublish_down == '0000-00-00 00:00:00' || $item->podpublish_down == '')
        {$podpublishdown = '0000-00-00 00:00:00';}
        else {$podpublishdown = $item->podpublish_down;}

        $now = gmdate ( 'Y-m-d H:i:s' );
        $nullDate = '0000-00-00 00:00:00';

        if (($item->published == 1 && ($item->publish_up == $nullDate || $item->publish_up == '' || $item->publish_up <= $now) && ($item->publish_down == $nullDate || $item->publish_down == '' || $item->publish_down >= $now)) && (($item->podpublish_up == $nullDate || $item->podpublish_up == '' || $item->podpublish_up <= $now) && ($item->podpublish_down == $nullDate || $item->podpublish_down == '' || $item->podpublish_down >= $now)))
        {
            $podpub = '<img style="width: 16px; height: 16px;" src="../media/preachit/images/success.png">';
        }
        else {$podpub = '<img style="width: 16px; height: 16px;" src="../media/preachit/images/failure.png">';}
        
        // get language title
            $query = "SELECT ".$db->quoteName('title')."
            FROM ".$db->quoteName('#__languages')."
            WHERE ".$db->quoteName('lang_code')." = ".$db->quote($item->language).";
          ";
            $db->setQuery($query);
            $language_title = $db->loadResult();
        
        ?>
            <tr class="row<?php echo $i % 2; ?>" sortable-group-id="0">
                <td class="center hidden-phone">
                    <?php echo JHtml::_('grid.id', $i, $item->id); ?>
                </td>
                <td class="center">
                    <?php echo JHtml::_('jgrid.published', $item->published, $i, 'studies.', $canChange, 'cb'); ?>
                    <?php echo JHtml::_('preachitadministrator.featured', $item->featured, $i, 'studies.', $canChange); ?>
                </td>
                <td class="nowrap has-context">
                    <div class="pull-left">
                        <?php if ($item->checked_out) : ?>
                                <?php echo JHtml::_('jgrid.checkedout', $i, $editor, $item->checked_out_time, 'studies.', $canCheckin); ?>
                            <?php endif; ?>
                        <?php if ($canEdit) : ?>
                            <a href="<?php echo JRoute::_('index.php?option=com_preachit&task=study.edit&id='.(int) $item->id); ?>">
                                <?php echo $this->escape(JHTML::Date($item->study_date, $this->dateformat)); ?></a>
                        <?php else : ?>
                            <?php echo $this->escape(JHTML::Date($item->study_date, $this->dateformat)); ?>
                        <?php endif; ?>
                    </div>
                    <div class="pull-left">
                        <?php
                            // Create dropdown items
                            JHtml::_('dropdown.edit', $item->id, 'study.');
                            JHtml::_('dropdown.divider');
                            if ($item->published) :
                                JHtml::_('dropdown.unpublish', 'cb' . $i, 'studies.');
                            else :
                                JHtml::_('dropdown.publish', 'cb' . $i, 'studies.');
                            endif;
                                JHtml::_('dropdown.divider');
                            if ($trashed) :
                                JHtml::_('dropdown.untrash', 'cb' . $i, 'studies.');
                            else :
                                JHtml::_('dropdown.trash', 'cb' . $i, 'studies.');
                            endif;

                            // render dropdown list
                            echo JHtml::_('dropdown.render');
                        ?>
                    </div>
                </td>
                <td>
                    <?php echo $item->study_name ?>
                </td>
                <td class="hidden-phone">
                    <?php echo $scripture ?>
                </td>
                <td class="hidden-phone">
                    <?php echo $teacher ?>
                </td>
                <td class="hidden-phone">
                    <?php echo $series ?>
                </td>
                <td class="small center hidden-phone">
                    <?php if ($item->language == '*'):?>
                        <?php echo JText::alt('JALL', 'language'); ?>
                    <?php else:?>
                        <?php echo $language_title ? $this->escape($language_title) : JText::_('JUNDEFINED'); ?>
                    <?php endif;?>
                </td>
                <td class="center hidden-phone">
                    <?php echo $item->hits; ?>
                </td>
                <td class="center hidden-phone">
                    <?php echo $item->downloads; ?>
                </td>
                <td class="center hidden-phone">
                        <?php echo $podpub; ?>
                </td>
                <td class="center hidden-phone">
                        <?php echo $item->id; ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<!-- /Footer -->
<?php echo JHTML::_( 'form.token' ); ?>
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="view" value="studies" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
</div>
<div class="clearfix"> </div>
<div style="text-align: center; padding-top: 5px;">
<?php $abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/lib/preachit.footer.php');?>
<?php echo PIfooter::footer();?></div>
</form>
