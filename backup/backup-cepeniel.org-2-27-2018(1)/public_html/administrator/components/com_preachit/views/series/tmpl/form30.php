<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.liuthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/tooltips.php');
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/additional.php');
$option = JRequest::getCmd('option');
JText::script('COM_PREACHIT_FIELDS_INVALID');
$user	= JFactory::getUser();
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.chosen', 'select');
$document = JFactory::getDocument();      
$document->addScript(JURI::root() . 'media/preachit/js/pisubmitbutton.js');
$document->addScript(JURI::root() . 'media/preachit/js/piseriesinsert.js');
?>
<script language="javascript" type="text/javascript">
function setDisabled(filter, setting)
{
    if (filter)
    {filter.disabled = setting;}    
    return
}

function setDisabledall(setting)
{
    var filter0 = document.getElementById('jform_videofolder');
    var filter5 = document.getElementById('jform_image_folderlrg');
    setDisabled(filter0, setting);
    setDisabled(filter5, setting);
}

window.addEvent('domready', function(){
                setDisabledall(true);
            })
</script>
<form class ="form-validate" action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
    <div class="form-horizontal">
        <fieldset>
            <ul class="nav nav-tabs">
                <li class="active"><a href="#main" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_ADMIN_SERIES_LEGEND');?></a></li>
                <?php if (Tewebcheck::showtab($this->form, 'images', $this->hide))
                {?>
                <li><a href="#serimage" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_ADMIN_IMAGEHEAD');?></a></li>
                <?php } ?>
                <?php if (Tewebcheck::showtab($this->form, 'videotab', $this->hide))
                {?>
                <li><a href="#servideo" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_ADMIN_VIDEOHEAD');?></a></li>
                <?php } ?>
                <?php if (Tewebcheck::showtab($this->form, 'metaoptions', $this->hide))
                { ?>
                <li><a href="#metadata" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_SUB_META_OPTIONS');?></a></li>
                <?php } ?>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="main">
                    <?php
                        foreach ($this->form->getFieldset("maininfo") as $field):
                        if (!$user->authorise('core.edit.state', 'com_preachit') && $field->name == 'jform[published]')
                        {continue;}
                        if (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
                        {
                                ?>
                                  <?php 
                                        if ($field->hidden):
                                            echo $field->input;
                                        elseif (!$user->authorise('core.edit.state', 'com_preachit') && $field->name == 'jform[published]'):
                                                    continue;
                                        else:
                                        ?>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                        <?php
                                        endif;
                                   
                                    ?>
                                <?php }
                                endforeach;?>
                        <?php if (!in_array(Tewebcheck::gettvalue('jform_series_description'), $this->hide))
                        { ?>
                            <div class="control-group">
                                <div class="control-label">
                                    <?php echo $this->form->getLabel('series_description'); ?>   
                                </div>
                                <div class="controls">     
                                    <?php echo $this->form->getInput('series_description'); ?>
                                </div>
                            </div>
                        <?php } ?>
                </div>
                <div class="tab-pane" id="serimage">
                    <?php
                        foreach ($this->form->getFieldset("images") as $field):
                        if ($field->name == 'jform[image_folderlrg]' && in_array('image_folderlrg', $this->hide) && !in_array('series_image_lrg', $this->hide)) 
                        {?>
                            <input type="hidden" id="<?php echo $field->id;?>" "name="<?php echo $field->name;?>" value="<?php $value = Tewebcheck::gettvalue($field->name); echo $this->row->$value;?>" />
                        <?php }   
                        elseif (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
                        {
                                ?>
                                  <?php 
                                        if ($field->hidden):
                                            echo $field->input;
                                        else:
                                        ?>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                        <?php if ($field->name == 'jform[series_image_lrg]')
                                        {?>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <label>
                                                    <?php echo JText::_('COM_PREACHIT_ADMIN_LRGIMAGE_PREVIEW');?>
                                                </label> 
                                            </div>
                                            <div class="controls">     
                                                <?php echo $this->image; ?>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <?php
                                        endif;
                                   
                                    ?>
                                <?php }
                                endforeach;?>
                </div>
                <div class="tab-pane" id="servideo">
                    <?php
                        foreach ($this->form->getFieldset("videotab") as $field):
                        if ($field->name == 'jform[videofolder]' && in_array('videofolder', $this->hide) && !in_array('videolink', $this->hide)) 
                        {?>
                            <input type="hidden" id="<?php echo $field->id;?>" "name="<?php echo $field->name;?>" value="<?php $value = Tewebcheck::gettvalue($field->name); echo $this->row->$value;?>" />
                        <?php }   
                        elseif (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
                        {
                                ?>
                                  <?php 
                                        if ($field->hidden):
                                            echo $field->input;
                                        else:
                                        ?>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                        <?php
                                        endif;
                                   
                                    ?>
                                <?php }
                                endforeach;?>
                </div>
                <div class="tab-pane" id="metadata">
                    <?php
                        foreach ($this->form->getFieldset("metaoptions") as $field):
                        if (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
                        {
                                ?>
                                  <?php 
                                        if ($field->hidden):
                                            echo $field->input;
                                        else:
                                        ?>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                        <?php
                                        endif;
                                   
                                    ?>
                                <?php }
                                endforeach;?>
                </div>
            </div>
        </fieldset>
    </div>
<!-- Footer -->
<div style="text-align: center">
<?php $abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/lib/preachit.footer.php');?>
<?php echo PIfooter::footer();?></div>
<!-- /Footer -->
<input type="hidden" name="id" value="<?php echo $this->item->id; ?>" />
<input type="hidden" name="controller" value="series" />
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="task" value ="" />
<?php if (!$user->authorise('core.edit.state', 'com_preachit') && !in_array(Tewebcheck::gettvalue('jform[published]'), $this->hide)) {?>
<input type="hidden" name="jform[published]" id="jform_published" value="<?php echo $this->item->published;?>" />
<?php }?>
<?php foreach ($this->validate AS $validate)
{
    if (in_array($validate, $this->hide))
    {?>
        <input type="hidden" name="jform[<?php echo $validate;?>]" id="jform_<?php echo $validate;?>" value="<?php echo $this->item->$validate;?>" />
    <?php }    
}?>
<div id="extrainputs" style="display: none;"></div>
<?php echo JHTML::_( 'form.token' ); ?>
</form>