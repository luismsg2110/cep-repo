<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
class PreachitViewSeries extends JViewLegacy
{
    protected $form;

    protected $item;

    protected $state; 
    
function display($tpl = null)
{
JRequest::setVar('hidemainmenu', 1);	
//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();
$document = JFactory::getDocument();
$document->addStyleSheet('../media/preachit/css/preachit.css');

// get Joomla version to decide which form and method

$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/seriesimage.php');
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/additional.php');
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/forms.php');
jimport('teweb.details.standard');

// get the Form
$this->form = $this->get('Form');
$this->item = $this->get('Item');

// Check for errors.
if (count($errors = $this->get('Errors'))) {
    JError::raiseError(500, implode("\n", $errors));
    return false;
}

$user	= JFactory::getUser();
$this->assignRef('user', $user);

// hide elements if chosen in admin

$hide = PIHelperforms::hideseriesedit();
$this->assignRef('hide', $hide);
$validate = PIHelperforms::validateseriesedit();
$this->assignRef('validate', $validate);

if ($this->item->id > 0)
{
if (!$user->authorise('core.edit', 'com_preachit') && !$user->authorise('core.edit.own', 'com_preachit')) 
{Tewebcheck::check403();}
elseif (!$user->authorise('core.edit', 'com_preachit') && $user->id != $row->user)
{Tewebcheck::check403();}
}	
else 
{
if (!$user->authorise('core.create', 'com_preachit')) 
{Tewebcheck::check403();}
}

if (Tewebcheck::getJversion() >= 3.0)
{
    $this->setLayout('form30');
}
else {
    $this->setLayout('form16');
}

//get preview images

$image = PIHelpersimage::seriesimage($this->item->id, 0, '', 'large');

$this->assignRef('image', $image);
$this->addToolbar();
parent::display($tpl);
}

/**
     * Add the page title and toolbar.
     *
     * @since    1.6
     */
protected function addToolbar()
{
    $user    = JFactory::getUser();   
    if ($this->item->id > 0)
    {
    JToolBarHelper::title( JText::_( 'COM_PREACHIT_ADMIN_SERIES_EDIT' ), 'series.png' );
    }
    else
    {
    JToolBarHelper::title( JText::_( 'COM_PREACHIT_ADMIN_SERIES_DETAILS' ), 'series.png' );
    }
    JToolBarHelper::apply('series.apply', 'JTOOLBAR_APPLY');
    JToolBarHelper::save('series.save', 'JTOOLBAR_SAVE');
    JToolBarHelper::divider();
    $user    = JFactory::getUser();
    if ($user->authorise('core.admin', 'com_preachit'))  {
    JToolBarHelper::preferences('com_preachit', '550', '900');
    }
    if ($this->item->id > 0)
    {
    JToolBarHelper::cancel( 'series.cancel', 'JTOOLBAR_CLOSE' );
    }
    else
    {
    JToolBarHelper::cancel( 'series.cancel', 'JTOOLBAR_CANCEL' );
    }
    JToolBarHelper::divider();
    JToolBarHelper::help('pihelp', 'com_preachit');
}

}