<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('teweb.file.temediaview');
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/additional.php');
class PreachitViewTemedia extends ViewTemedia
{
    public function __construct ( $config = array() ) 
    {
        $this->params = Tewebdetails::getparams('com_preachit');
        $this->media = JRequest::getVar('media', 'image');
        $this->folderchange = JRequest::getVar('foldchange', '');
        $this->folder = $this->returnFolder();
        $this->allowView = $this->allowView();
        $this->thirdparty = $this->allowthirdparty();
        $this->id3data = $this->allowid3data();
        $this->mediavalue = $this->getMediavalue();
        $this->uploadstyle = $this->getUploadstyle();
        $this->upload_folder = $this->getFolderlist('upload');
        $this->lt_folder = $this->getFolderlist('linkto');
        $this->f_folder = $this->getFolderlist('normal', true);
        $this->artistlist = $this->getArtistlist();
        $this->albumlist = $this->getAlbumlist();
        $this->title_title = JText::_('COM_PREACHIT_ADMIN_NAME');
        $this->album_title = JText::_('COM_PREACHIT_ADMIN_SERIES');
        $this->artist_title = JText::_('COM_PREACHIT_ADMIN_TEACHER');
        $this->image_title = JText::_('COM_PREACHIT_ADMIN_IMAGE_LARGE');
        $this->comment_title = JText::_('COM_PREACHIT_ADMIN_DESCRIPTION');
        $this->duration_title = JText::_('COM_PREACHIT_ADMIN_DURATION');
        $this->tags_title = JText::_('COM_PREACHIT_ADMIN_TAGS');
        $this->canMediaAdmin = $this->canMediaAdmin();
        $this->canMediaCreate = $this->canMediaCreate();
        $this->JTPREFIX = 'COM_PREACHIT';
        parent::__construct($config);
    }
    
/**
     * indicates whether the user can view this resource
     *
     * @var    boolean
     */

    public function allowView()
    {
        $user    = JFactory::getUser();
        if ($this->folderchange == '' || (!$user->authorise('core.edit', 'com_preachit') && !$user->authorise('core.create', 'com_preachit') && $user->authorise('core.edit.own', 'com_preachit')))
        {return false;}
        else {return true;}
    }

/**
     * indicates whether the user can managed resource
     *
     * @var    boolean
     */

    public function canMediaAdmin()
    {
        $user    = JFactory::getUser();
        if (!$user->authorise('core.admin', 'com_preachit'))
        {return false;}
        else {return true;}
    }
    
/**
     * indicates whether the user can add resources
     *
     * @var    boolean
     */

    public function canMediaCreate()
    {
        $user    = JFactory::getUser();
        if (!$user->authorise('core.create', 'com_preachit'))
        {return false;}
        else {return true;}
    }
    
/**
     * indicates whether the thirdparty option should be displayed
     *
     * @var    boolean
     */

    public function allowthirdparty()
    {
        if ($this->media == 'video' || $this->media == 'videoclip')
        {
            return true;
        }
        else
        {
            return false;
        }   
    }
    
/**
     * indicates whether the id3data should be displayed
     *
     * @var    boolean
     */

    public function allowid3data()
    {
        if ($this->media == 'video' || $this->media == 'audio' || $this->media == 'dvideo')
        {
            return true;
        }
        else
        {
            return false;
        }   
    }
    
/**
     * gets the media value
     *
     * @var    boolean
     */

    public function getMediavalue()
    {
        if ($this->media == 'audio')
        {
            return 1;
        }
        elseif ($this->media == 'video')
        {
            return 2;
        }
        elseif ($this->media == 'dvideo')
        {
            return 7;
        }
        elseif ($this->media == 'notes')
        {
            return 3;
        }
        elseif ($this->media == 'slides')
        {
            return 8;
        }
        else
        {
            return 4;
        }
    }

/**
     * gets the upload style - used to switch this off for 3rd party non uploadables
     *
     * @var    string
     */

    public function getUploadstyle()
    {
        $folders = JTable::getInstance('Filepath', 'Table');
        $folders->load($this->folder);
        if ($this->folder == 0 || ($folders->type != 0 && $folders->type != 2 && $folders->type != 3))
        {return  "none";}
        else {return  "block";}  
    }

/**
     * gets the folder list
     *
     * @var    string
     */

    public function getFolderlist($type, $disabled = false)
    {
        $db = JFactory::getDBO();
        $params = $this->params;
        if ($disabled)
        {$extrafields = ' disabled="true"';}
        else {$extrafields = '';}
        //folder lists
        if ($params->get('default_folder_only', 0) == 1)
        {$whereext = ' AND id = '.$this->folder;}
        else {$whereext = '';}
        $db->setQuery('SELECT id AS value, name AS text FROM #__pifilepath WHERE published = 1'.$whereext.' ORDER by name');
        $folderlist =  $db->loadObjectList();
        $folder = array(array('value' => '-1', 'text' => JText::_('LIB_TEWEB_JOOMLA_IMAGES')),);
        $nofolder = array(array('value' => '', 'text' => JText::_('LIB_TEWEB_NO_FOLDER')),);
        if ($params->get('default_folder_only', 0) == 1 && $this->folder > -1)
        {$folderlist = array_merge($folderlist);}
        elseif ($params->get('default_folder_only', 0) == 1 && ($this->folder == -1 || $this->folder == ''))
        {$folderlist = $folder;}
        else {{$folderlist = array_merge( $folder, $folderlist );}}
        if ($type == 'upload')
        {return JHTML::_('select.genericList', $folderlist, 'view_folder', 'class="inputbox" onchange="javascript:temediaManager.setFolder(\'\')"'.$extrafields. '', 'value', 'text', $this->folder );}
        elseif ($type == 'linkto')
        {
            $folderlist = array_merge( $nofolder, $folderlist );
            if ($this->folder > 0)
            {$setfolder = $this->folder;}
            else {$setfolder = 0;}
            return JHTML::_('select.genericList', $folderlist, 'lt_folder', 'class="inputbox" '.$extrafields. '', 'value', 'text', $setfolder );}
        else {
            $folderlist = array_merge( $nofolder, $folderlist );
            return JHTML::_('select.genericList', $folderlist, 'f_folder', 'class="inputbox"'.$extrafields. '', 'value', 'text', 0 );}
    }

/**
     * gets the artist list
     *
     * @var    string
     */

    public function getArtistlist()
    {
        $db = JFactory::getDBO();
        $db->setQuery('SELECT id, teacher_name, lastname, language FROM #__piteachers WHERE published = 1 ORDER by lastname');
        $tlists =  $db->loadObjectList();
        $options = array();
        $options[] = JHtml::_('select.option', '', JText::_('COM_PREACHIT_ADMIN_TEACHER_SELECT'));
        $translate = PIHelperadditional::translate();
        foreach($tlists as $tlist) 
                {
                    if ($tlist->lastname != '')
                        {$name = $tlist->teacher_name.' '.$tlist->lastname;}
                    if ($translate)
                            {if ($tlist->language != '*') {$name = $tlist->teacher_name.' '.$tlist->lastname.' - '.$tlist->language;}}
                        $options[] = JHtml::_('select.option', $tlist->id, $name);
                }
        return JHTML::_('select.genericList', $options, 'artist', 'class="inputbox" '. '', 'value', 'text', '' );
    }
    
/**
     * gets the folder list
     *
     * @var    string
     */

    public function getAlbumlist()
    {
        $db = JFactory::getDBO();
        $db->setQuery('SELECT id AS value, series_name AS text FROM #__piseries WHERE published = 1 ORDER by series_name');
        $slists =  $db->loadObjectList();
        $options = array();
        $options[] = JHtml::_('select.option', '', JText::_('COM_PREACHIT_ADMIN_SERIES_SELECT'));
        foreach($slists as $slist) 
        {
            $options[] = JHtml::_('select.option', $slist->value, $slist->text);
        }
        return JHTML::_('select.genericList', $options, 'album', 'class="inputbox"'. '', 'value', 'text', '' );
    }

/**
     * gets the folder setting
     *
     * @var    string
     */

    
    function returnFolder()
    {
        $folder = JRequest::getInt('folder', 0);
        $params = Tewebdetails::getparams('com_preachit');
        if ($folder == 0 || $params->get('default_folder_only', 0) == 1)
        {
            $media = JRequest::getVar('media', 'image');
            $folder = PIHelperadditional::getdefaultfolders($params, $media, -1);
        }
        return $folder;
    }
    
    
    
}
