<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('teweb.file.temedialistview');
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/additional.php');
class PreachitViewTemedialist extends ViewTemedialist
{
    public function __construct ( $config = array() ) 
    {
        $this->allowView = $this->allowView();
        parent::__construct($config);
    }
    
/**
     * indicates whether the user can view this resource
     *
     * @var    boolean
     */

    public function allowView()
    {
        $user    = JFactory::getUser();
        if (!$user->authorise('core.edit', 'com_preachit') && !$user->authorise('core.create', 'com_preachit') && !$user->authorise('core.edit.own', 'com_preachit'))
        {return false;}
        else {return true;}
    }

}
