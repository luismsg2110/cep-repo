<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
class PreachitViewComment extends JViewLegacy
{
    protected $form;

    protected $item;

    protected $state; 
    
function display($tpl = null)
{
$document = JFactory::getDocument();
$document->addStyleSheet('../media/preachit/css/preachit.css');

// get the Form
$this->form = $this->get('Form');
$this->item = $this->get('Item');

// Check for errors.
if (count($errors = $this->get('Errors'))) {
    JError::raiseError(500, implode("\n", $errors));
    return false;
}

$user	= JFactory::getUser();
if ($this->item->id > 0)
{
if (!$user->authorise('core.edit', 'com_preachit')) 
{Tewebcheck::check403();}
}
else 
{
if (!$user->authorise('core.create', 'com_preachit')) 
{Tewebcheck::check403();}
}

if (Tewebcheck::getJversion() >= 3.0)
{
    $this->setLayout('form30');
}
else {
    $this->setLayout('form16');
}
$this->addToolbar();

//get study name
$db = JFactory::getDBO();
$query1 = "
  SELECT ".$db->quoteName('study_id')."
    FROM ".$db->quoteName('#__picomments')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($this->item->id).";
  ";
$db->setQuery($query1);
$study_id = $db->loadResult();


$query = "
  SELECT ".$db->quoteName('study_name')."
    FROM ".$db->quoteName('#__pistudies')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($study_id).";
  ";
$db->setQuery($query);
$studyname = $db->loadResult();
$this->assignRef('studyname', $studyname);
parent::display($tpl);
}

/**
     * Add the page title and toolbar.
     *
     * @since    1.6
     */
protected function addToolbar()
{
    $user    = JFactory::getUser();   
    JFactory::getApplication()->input->set('hidemainmenu', true); 
    JToolBarHelper::title( JText::_( 'COM_PREACHIT_COMMENTS_TITLE_EDIT' ), 'comment.png' );
    JToolBarHelper::apply('comment.apply', 'JTOOLBAR_APPLY');
    JToolBarHelper::save('comment.save', 'JTOOLBAR_SAVE');
    JToolBarHelper::divider();
    if ($user->authorise('core.admin', 'com_preachit'))  {
    JToolBarHelper::preferences('com_preachit', '550', '900');
    }
    if ($this->item->id > 0)
    {
    JToolBarHelper::cancel( 'comment.cancel', 'JTOOLBAR_CLOSE' );
    }
    else
    {
    JToolBarHelper::cancel( 'comment.cancel', 'JTOOLBAR_CANCEL' );
    }
    JToolBarHelper::divider();
    JToolBarHelper::help('pihelp', 'com_preachit');
}

}