<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$option = JRequest::getCmd('option');
JText::script('COM_PREACHIT_FIELDS_INVALID');
$user    = JFactory::getUser();
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.chosen', 'select');
$document = JFactory::getDocument();      
$document->addScript(JURI::root() . 'media/preachit/js/pisubmitbutton.js');
?>
<form class ="form-validate" action="index.php" method="post" name="adminForm" id="adminForm">
<fieldset>
<legend><?php echo JText::_('COM_PREACHIT_ADMIN_COMMENTS_LEGEND');?></legend>
<div class="form-horizontal">
    <div class="control-group">
        <div class="control-label">
            <?php echo $this->form->getLabel('id'); ?>
        </div>
        <div class="controls">
            <?php echo $this->form->getInput('id'); ?>
        </div>
    </div>
    <?php
    if ($user->authorise('core.edit.state', 'com_preachit')) 
    {?>
    <div class="control-group">
        <div class="control-label">
            <?php echo $this->form->getLabel('published'); ?>
        </div>
        <div class="controls">
            <?php echo $this->form->getInput('published'); ?>
        </div>
    </div>
    <?php  }?>
    <div class="control-group">
        <div class="control-label">
            <?php echo $this->form->getLabel('full_name'); ?>
        </div>
        <div class="controls">
            <?php echo $this->form->getInput('full_name'); ?>
        </div>
    </div>
    <div class="control-group">
        <div class="control-label">
            <?php echo $this->form->getLabel('comment_date'); ?>
        </div>
        <div class="controls">
            <?php echo $this->form->getInput('comment_date'); ?>
        </div>
    </div>
    <div class="control-group">
        <div class="control-label">
            <label>
                <?php echo JText::_('COM_PREACHIT_ADMIN_STUDY');?>
            </label>
        </div>
        <div class="controls">
            <input type="text" size="50" readonly="readonly" class="readonly" value="<?php echo $this->studyname;?>" />
        </div>
    </div>
</div>
</fieldset>
<!-- Footer -->
<div style="text-align: center">
<?php $abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/lib/preachit.footer.php');?>
<?php echo PIfooter::footer();?></div>
<!-- /Footer -->
<input type="hidden" name="controller" value="comment" />
<input type="hidden" name="id" value="<?php echo $this->item->id; ?>" />
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="task" value ="" />
<?php if (!$user->authorise('core.edit.state', 'com_preachit')) {?>
<input type="hidden" name="jform[published]" id="jform_published" value="<?php echo $this->item->published;?>" />
<?php }?>
<?php echo JHTML::_( 'form.token' ); ?>
</form>






