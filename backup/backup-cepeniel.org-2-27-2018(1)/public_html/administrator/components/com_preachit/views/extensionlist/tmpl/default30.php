<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$dateformat = '';
$dateformat = 'F, Y';
$option = JRequest::getCmd('option');
$user    = JFactory::getUser();
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');
$listOrder    = $this->escape($this->state->get('list.ordering'));
$listDirn    = $this->escape($this->state->get('list.direction'));
$trashed    = $this->state->get('filter.published') == -2 ? true : false;
?>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<?php if(!empty( $this->sidebar)): ?>
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
    <div id="j-main-container" class="span10">
<?php else : ?>
    <div id="j-main-container">
<?php endif;?>
<table class="adminlist table table-striped">
    <thead>
        <tr>
            <th><?php echo JText::_('COM_PREACHIT_ADMIN_NAMELIST');?></th>
            <th width="10%" class="center hidden-phone"><?php echo JText::_('COM_PREACHIT_ADMIN_VERSIONLIST');?></th>
            <th width="10%" class="center hidden-phone"><?php echo JText::_('COM_PREACHIT_ADMIN_UPGRADELIST');?></th>
            <th width="20%" class="center hidden-phone"><?php echo JText::_('COM_PREACHIT_ADMIN_CREATION_DATELIST');?></th>
            <th width="30%" class="hidden-phone"><?php echo JText::_('COM_PREACHIT_ADMIN_AUTHORLIST');?></th>
            <th width="10%" class="center"><?php echo JText::_('COM_PREACHIT_ADMIN_CSS_FILELIST');?></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($this->rows as $i => $item)
        {
        $ordering  = ($listOrder == 'ordering');
        $canCreate  = $user->authorise('core.create', 'com_preachit');
        $canEdit    = $user->authorise('core.edit', 'com_preachit');
        $canCheckin = true; //$user->authorise('core.manage',     'com_checkin') || $item->checked_out == $userId || $item->checked_out == 0;
        $canChange  = $user->authorise('core.edit.state', 'com_preachit') && $canCheckin;
        ?>
            <tr class="row<?php echo $i % 2; ?>" sortable-group-id="0">
                <td class="nowrap has-context">
                    <div class="pull-left">
                            <?php if ($item->name)
                            { 
                                echo JText::_($item->name); 
                            } 
                            else {echo '<div style="text-align: center;">'.$item->dir.'</div>';} ?>
                    </div>
                </td>
                <td class="center hidden-phone">
                    <?php if ($item->version)
                    { 
                        echo $item->version; 
                    } 
                    else {echo '<div style="text-align: center;"> - </div>';} ?>
                </td>
                <td class="center hidden-phone">
                        <?php if ($item->upgrade)
                        { 
                            echo '<a class="piupgrade" href="http://te-webdesign.org.uk">['.$item->upgrade.']</a>'; 
                        } 
                        else {echo '<div style="text-align: center;"> - </div>';} ?>
                </td>
                <td class="center hidden-phone">
                        <?php if ($item->date)
                        { 
                            echo JHTML::Date($item->date, $dateformat); 
                        } 
                        else {echo '<div style="text-align: center;"> - </div>';} ?>
                </td>
                <td class="hidden-phone">
                        <?php if ($item->author)
                        { 
                            echo $item->author; 
                        } 
                        else {echo '<div style="text-align: center;"> - </div>';} ?>
                </td>
                <td class="center hidden-phone">
                        <?php if ($item->link)
                        {?>
                            <a href="<?php echo $item->link; ?>">[<?php echo JText::_('COM_PREACHIT_EXTENSION_CSS_EDIT');?>]</a>
                        <?php }
                        else { echo ' - ';}?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<!-- /Footer -->
<?php echo JHTML::_( 'form.token' ); ?>
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="view" value="extensionlist" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
</div>
<div class="clearfix"> </div>
<div style="text-align: center; padding-top: 5px;">
<?php $abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/lib/preachit.footer.php');?>
<?php echo PIfooter::footer();?></div>
</form>