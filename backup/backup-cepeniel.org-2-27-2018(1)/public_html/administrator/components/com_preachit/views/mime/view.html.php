<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
class PreachitViewMime extends JViewLegacy
{
    protected $form;

    protected $item;

    protected $state; 
    
function display($tpl = null)
{
JRequest::setVar('hidemainmenu', 1);
$document = JFactory::getDocument();
$document->addStyleSheet('../media/preachit/css/preachit.css');

// get Joomla version to decide which form and method

$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/additional.php');

// get the Form
$this->form = $this->get('Form');
$this->item = $this->get('Item');
// Check for errors.
if (count($errors = $this->get('Errors'))) {
    JError::raiseError(500, implode("\n", $errors));
    return false;
}
$user    = JFactory::getUser();
if (!$user->authorise('core.admin', 'com_preachit'))
{Tewebcheck::check403();}

if (Tewebcheck::getJversion() >= 3.0)
{
    $this->setLayout('form30');
}
else {
    $this->setLayout('form16');
}
$this->addToolbar();
parent::display($tpl);
}

/**
     * Add the page title and toolbar.
     *
     * @since    1.6
     */
protected function addToolbar()
{
    $user    = JFactory::getUser();   
    if ($this->item->id > 0)
    {JToolBarHelper::title( JText::_( 'COM_PREACHIT_MIME_TITLE_EDIT' ), 'mime.png' );}
    else
    {JToolBarHelper::title( JText::_( 'COM_PREACHIT_MIME_TITLE_DETAILS' ), 'mime.png' );}
    JToolBarHelper::apply('mime.apply', 'JTOOLBAR_APPLY');
    JToolBarHelper::save('mime.save', 'JTOOLBAR_SAVE');
    JToolBarHelper::divider();
    $user    = JFactory::getUser();
    if ($user->authorise('core.admin', 'com_preachit'))  {
    JToolBarHelper::preferences('com_preachit', '550', '900');
    }
    if ($this->item->id > 0)
    {
    JToolBarHelper::cancel( 'mime.cancel', 'JTOOLBAR_CLOSE' );
    }
    else
    {
    JToolBarHelper::cancel( 'mime.cancel', 'JTOOLBAR_CANCEL' );
    }
    JToolBarHelper::divider();
    JToolBarHelper::help('pihelp', 'com_preachit');
}

}