<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$option = JRequest::getCmd('option');
JText::script('COM_PREACHIT_FIELDS_INVALID');
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/tooltips.php');
$user	= JFactory::getUser();
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
$id = '';
JHtml::_('behavior.keepalive');
$document = JFactory::getDocument();      
$document->addScript(JURI::root() . 'media/preachit/js/pisubmitbutton.js');
?>

<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
    <div class="form-horizontal">
        <fieldset>
            <div class="control-group form-inline">
                <?php
                    foreach ($this->tform->getFieldset("general") as $field):
                            ?>
                              <?php 
                                    if ($field->hidden):
                                        echo $field->input;
                                    else:
                                    ?>
                                            <?php echo $field->label; ?>     
                                            <?php echo $field->input; ?>
                                    <?php
                                    endif;
                                ?>
                            <?php
                            endforeach;?>
            </div>
            <div class="clearfix"></div>
            <ul class="nav nav-tabs">
                <?php if ($this->paramform->getFieldset("general"))
                {?>
                <li class="active"><a href="#tempgeneral" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_TEMPLATE_PARAMS_GENERAL');?></a></li>
                <?php } ?>
                <?php if ($this->paramform->getFieldset("messagelist"))
                { ?>
                <li><a href="#tempmeslist" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_TEMPLATE_PARAMS_MESSAGELIST');?></a></li>
                <?php } ?>
                <?php if ($this->paramform->getFieldset("video"))
                {?>
                <li><a href="#tempvideo" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_TEMPLATE_PARAMS_VIDEO');?></a></li>
                <?php } ?>
                <?php if ($this->paramform->getFieldset("audio"))
                {?>
                <li><a href="#tempaudio" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_TEMPLATE_PARAMS_AUDIO');?></a></li>
                <?php } ?>
                <?php if ($this->paramform->getFieldset("studypopup"))
                {?>
                <li><a href="#temppopup" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_TEMPLATE_PARAMS_POPUP');?></a></li>
                <?php } ?>
                <?php if ($this->paramform->getFieldset("videopopup"))
                {?>
                <li><a href="#tempvideopopup" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_TEMPLATE_PARAMS_VIDEOPOPUP');?></a></li>
                <?php } ?>
                <?php if ($this->paramform->getFieldset("audiopopup"))
                {?>
                <li><a href="#tempaudiopopup" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_TEMPLATE_PARAMS_AUDIOPOPUP');?></a></li>
                <?php } ?>
                <?php if ($this->paramform->getFieldset("text"))
                {?>
                <li><a href="#temptext" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_TEMPLATE_PARAMS_TEXT');?></a></li>
                <?php } ?>
                <?php if ($this->paramform->getFieldset("generalmedia"))
                {?>
                <li><a href="#tempgeneralmedia" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_TEMPLATE_PARAMS_GENMEDIA');?></a></li>
                <?php } ?>
                <?php if ($this->paramform->getFieldset("serieslist"))
                {?>
                <li><a href="#tempserlist" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_TEMPLATE_PARAMS_SERIESLIST');?></a></li>
                <?php } ?>
                <?php if ($this->paramform->getFieldset("series"))
                {?>
                <li><a href="#tempseries" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_TEMPLATE_PARAMS_SERIES');?></a></li>
                <?php } ?>
                <?php if ($this->paramform->getFieldset("teacherlist"))
                {?>
                <li><a href="#temptealist" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_TEMPLATE_PARAMS_TEACHERLIST');?></a></li>
                <?php } ?>
                <?php if ($this->paramform->getFieldset("teacher"))
                {?>
                <li><a href="#tempteacher" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_TEMPLATE_PARAMS_TEACHER');?></a></li>
                <?php } ?>
                <?php if ($this->paramform->getFieldset("ministrylist"))
                {?>
                <li><a href="#tempminlist" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_TEMPLATE_PARAMS_MINISTRYLIST');?></a></li>
                <?php } ?>
                <?php if ($this->paramform->getFieldset("ministry"))
                {?>
                <li><a href="#tempministry" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_TEMPLATE_PARAMS_MINISTRY');?></a></li>
                <?php } ?>
                <?php if ($this->paramform->getFieldset("asmedia"))
                {?>
                <li><a href="#tempasmedia" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_TEMPLATE_PARAMS_ASMEDIA');?></a></li>
                <?php } ?>
                <?php if ($this->paramform->getFieldset("taglist"))
                {?>
                <li><a href="#temptaglist" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_TEMPLATE_PARAMS_TAGLIST');?></a></li>
                <?php } ?>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tempgeneral">
                    <?php
                        if ($this->paramform->getFieldset("general"))
                        {
                        foreach ($this->paramform->getFieldset("general") as $field):
                                ?>
                                  <?php 
                                        if ($field->hidden):
                                            echo $field->input;
                                        else:
                                        ?>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                        <?php
                                        endif;
                                   
                                    ?>
                                <?php
                                endforeach;?>   
                        <?php } ?> 
                </div>
                <div class="tab-pane" id="tempmeslist">
                    <?php
                        if ($this->paramform->getFieldset("messagelist"))
                        {
                        foreach ($this->paramform->getFieldset("messagelist") as $field):
                                ?>
                                  <?php 
                                        if ($field->hidden):
                                            echo $field->input;
                                        else:
                                        ?>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                        <?php
                                        endif;
                                   
                                    ?>
                                <?php
                                endforeach;?>   
                        <?php } ?>    
                </div>
                <div class="tab-pane" id="tempvideo">
                    <?php
                        if ($this->paramform->getFieldset("video"))
                        {
                        foreach ($this->paramform->getFieldset("video") as $field):
                                ?>
                                  <?php 
                                        if ($field->hidden):
                                            echo $field->input;
                                        else:
                                        ?>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                        <?php
                                        endif;
                                   
                                    ?>
                                <?php
                                endforeach;?>   
                        <?php } ?>    
                </div>
                <div class="tab-pane" id="tempaudio">
                    <?php
                        if ($this->paramform->getFieldset("audio"))
                        {
                        foreach ($this->paramform->getFieldset("audio") as $field):
                                ?>
                                  <?php 
                                        if ($field->hidden):
                                            echo $field->input;
                                        else:
                                        ?>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                        <?php
                                        endif;
                                   
                                    ?>
                                <?php
                                endforeach;?>   
                        <?php } ?>    
                </div>
                <div class="tab-pane" id="temppopup">
                    <?php
                        if ($this->paramform->getFieldset("studypopup"))
                        {
                        foreach ($this->paramform->getFieldset("studypopup") as $field):
                                ?>
                                  <?php 
                                        if ($field->hidden):
                                            echo $field->input;
                                        else:
                                        ?>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                        <?php
                                        endif;
                                   
                                    ?>
                                <?php
                                endforeach;?>   
                        <?php } ?>    
                </div>
                <div class="tab-pane" id="tempvideopopup">
                    <?php
                        if ($this->paramform->getFieldset("videopopup"))
                        {
                        foreach ($this->paramform->getFieldset("videopopup") as $field):
                                ?>
                                  <?php 
                                        if ($field->hidden):
                                            echo $field->input;
                                        else:
                                        ?>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                        <?php
                                        endif;
                                   
                                    ?>
                                <?php
                                endforeach;?>   
                        <?php } ?>    
                </div>
                <div class="tab-pane" id="tempaudiopopup">
                    <?php
                        if ($this->paramform->getFieldset("audiopopup"))
                        {
                        foreach ($this->paramform->getFieldset("audiopopup") as $field):
                                ?>
                                  <?php 
                                        if ($field->hidden):
                                            echo $field->input;
                                        else:
                                        ?>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                        <?php
                                        endif;
                                   
                                    ?>
                                <?php
                                endforeach;?>   
                        <?php } ?>    
                </div>
                <div class="tab-pane" id="temptext">
                    <?php
                        if ($this->paramform->getFieldset("text"))
                        {
                        foreach ($this->paramform->getFieldset("text") as $field):
                                ?>
                                  <?php 
                                        if ($field->hidden):
                                            echo $field->input;
                                        else:
                                        ?>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                        <?php
                                        endif;
                                   
                                    ?>
                                <?php
                                endforeach;?>   
                        <?php } ?>    
                </div>
                <div class="tab-pane" id="tempgeneralmedia">
                    <?php
                        if ($this->paramform->getFieldset("generalmedia"))
                        {
                        foreach ($this->paramform->getFieldset("generalmedia") as $field):
                                ?>
                                  <?php 
                                        if ($field->hidden):
                                            echo $field->input;
                                        else:
                                        ?>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                        <?php
                                        endif;
                                   
                                    ?>
                                <?php
                                endforeach;?>   
                        <?php } ?>    
                </div>
                <div class="tab-pane" id="tempserlist">
                    <?php
                        if ($this->paramform->getFieldset("serieslist"))
                        {
                        foreach ($this->paramform->getFieldset("serieslist") as $field):
                                ?>
                                  <?php 
                                        if ($field->hidden):
                                            echo $field->input;
                                        else:
                                        ?>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                        <?php
                                        endif;
                                   
                                    ?>
                                <?php
                                endforeach;?>   
                        <?php } ?>    
                </div>
                <div class="tab-pane" id="tempseries">
                    <?php
                        if ($this->paramform->getFieldset("series"))
                        {
                        foreach ($this->paramform->getFieldset("series") as $field):
                                ?>
                                  <?php 
                                        if ($field->hidden):
                                            echo $field->input;
                                        else:
                                        ?>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                        <?php
                                        endif;
                                   
                                    ?>
                                <?php
                                endforeach;?>   
                        <?php } ?>    
                </div>
                <div class="tab-pane" id="temptealist">
                    <?php
                        if ($this->paramform->getFieldset("teacherlist"))
                        {
                        foreach ($this->paramform->getFieldset("teacherlist") as $field):
                                ?>
                                  <?php 
                                        if ($field->hidden):
                                            echo $field->input;
                                        else:
                                        ?>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                        <?php
                                        endif;
                                   
                                    ?>
                                <?php
                                endforeach;?>   
                        <?php } ?>    
                </div>
                <div class="tab-pane" id="tempteacher">
                    <?php
                        if ($this->paramform->getFieldset("teacher"))
                        {
                        foreach ($this->paramform->getFieldset("teacher") as $field):
                                ?>
                                  <?php 
                                        if ($field->hidden):
                                            echo $field->input;
                                        else:
                                        ?>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                        <?php
                                        endif;
                                   
                                    ?>
                                <?php
                                endforeach;?>   
                        <?php } ?>    
                </div>
                <div class="tab-pane" id="tempminlist">
                    <?php
                        if ($this->paramform->getFieldset("ministrylist"))
                        {
                        foreach ($this->paramform->getFieldset("ministrylist") as $field):
                                ?>
                                  <?php 
                                        if ($field->hidden):
                                            echo $field->input;
                                        else:
                                        ?>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                        <?php
                                        endif;
                                   
                                    ?>
                                <?php
                                endforeach;?>   
                        <?php } ?>    
                </div>
                <div class="tab-pane" id="tempministry">
                    <?php
                        if ($this->paramform->getFieldset("ministry"))
                        {
                        foreach ($this->paramform->getFieldset("ministry") as $field):
                                ?>
                                  <?php 
                                        if ($field->hidden):
                                            echo $field->input;
                                        else:
                                        ?>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                        <?php
                                        endif;
                                   
                                    ?>
                                <?php
                                endforeach;?>   
                        <?php } ?>    
                </div>
                <div class="tab-pane" id="tempasmedia">
                    <?php
                        if ($this->paramform->getFieldset("asmedia"))
                        {
                        foreach ($this->paramform->getFieldset("asmedia") as $field):
                                ?>
                                  <?php 
                                        if ($field->hidden):
                                            echo $field->input;
                                        else:
                                        ?>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                        <?php
                                        endif;
                                   
                                    ?>
                                <?php
                                endforeach;?>   
                        <?php } ?>    
                </div>
                <div class="tab-pane" id="temptaglist">
                    <?php
                        if ($this->paramform->getFieldset("taglist"))
                        {
                        foreach ($this->paramform->getFieldset("taglist") as $field):
                                ?>
                                  <?php 
                                        if ($field->hidden):
                                            echo $field->input;
                                        else:
                                        ?>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                        <?php
                                        endif;
                                   
                                    ?>
                                <?php
                                endforeach;?>   
                        <?php } ?>    
                </div>
            </div>
        </fieldset>
    </div>
<div style="text-align: center">
<?php $abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/lib/preachit.footer.php');?>
<?php echo PIfooter::footer();?></div>
<!-- /Footer -->
<input type="hidden" name="id" value="<?php echo $this->temp; ?>" />
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="controller" value="template" />
<input type="hidden" name="task" value ="" />
<?php echo JHTML::_( 'form.token' ); ?>

</form>