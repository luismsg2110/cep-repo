<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$user	= JFactory::getUser();
$option = JRequest::getCmd('option');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');
$user        = JFactory::getUser();
$userId        = $user->get('id');
$listOrder    = $this->escape($this->state->get('list.ordering'));
$listDirn    = $this->escape($this->state->get('list.direction'));
$canOrder    = $user->authorise('core.edit.state', 'com_preachit');
$saveOrder    = $listOrder == 'ordering';
if ($saveOrder)
{
    $saveOrderingUrl = 'index.php?option=com_preachit&task=shares.saveOrderAjax&tmpl=component';
    JHtml::_('sortablelist.sortable', 'articleList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
$trashed    = $this->state->get('filter.state') == -2 ? true : false;
$sortFields = $this->getSortFields();
?>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<?php if(!empty( $this->sidebar)): ?>
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
    <div id="j-main-container" class="span10">
<?php else : ?>
    <div id="j-main-container">
<?php endif;?>
    <div id="filter-bar" class="btn-toolbar">
        <div class="btn-group pull-right hidden-phone">
                    <label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?></label>
                    <?php echo $this->pagination->getLimitBox(); ?>
        </div>
    </div>
<table class="adminlist table table-striped" id="articleList">

    <thead>
        <tr>
            <th width="1%" class="nowrap center hidden-phone">
                <?php echo JHtml::_('grid.sort', '<i class="icon-menu-2"></i>', 'ordering', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING'); ?>
            </th>
            <th width="1%" class="hidden-phone">
                <input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
            </th>
            <th width="1%" class="nowrap center">
                <?php echo JHtml::_('grid.sort', 'JSTATUS', 'published', $listDirn, $listOrder); ?>
            </th>
            <th>
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_NAMELIST', 'ministry_name', $listDirn, $listOrder); ?>
            </th>
            <th width="60%">
                <?php echo JHtml::_('grid.sort', 'COM_PREACHIT_ADMIN_CODELIST', 'ministry_description', $listDirn, $listOrder); ?>
            </th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <td colspan="6"> <?php echo $this->pagination->getListFooter(); ?> </td>
        </tr>
    </tfoot>
        <tbody>
        <?php
        foreach ($this->rows as $i => $item)
        {
        $ordering  = ($listOrder == 'ordering');
        $canCreate  = $user->authorise('core.create', 'com_preachit');
        $canEdit    = $user->authorise('core.edit', 'com_preachit');
        $canCheckin = $user->authorise('core.manage',     'com_checkin') || $item->checked_out == $userId || $item->checked_out == 0;
        $canChange  = $user->authorise('core.edit.state', 'com_preachit') && $canCheckin;
        
        // get checked out name
$db = JFactory::getDBO();
	$query = "SELECT ".$db->quoteName('name')."
    FROM ".$db->quoteName('#__users')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($item->checked_out).";
  ";
	$db->setQuery($query);
	$editor = $db->loadResult();

        ?>
            <tr class="row<?php echo $i % 2; ?>" sortable-group-id="0">
                <td class="order nowrap center hidden-phone">
                    <?php if ($canChange) :
                        $disableClassName = '';
                        $disabledLabel      = '';
                        if (!$saveOrder) :
                            $disabledLabel    = JText::_('JORDERINGDISABLED');
                            $disableClassName = 'inactive tip-top';
                        endif; ?>
                        <span class="sortable-handler hasTooltip <?php echo $disableClassName?>" title="<?php echo $disabledLabel?>">
                            <i class="icon-menu"></i>
                        </span>
                        <input type="text" style="display:none" name="order[]" size="5"
                            value="<?php echo $item->ordering;?>" class="width-20 text-area-order " />
                    <?php else : ?>
                        <span class="sortable-handler inactive" >
                            <i class="icon-menu"></i>
                        </span>
                    <?php endif; ?>
                </td>
                <td class="center hidden-phone">
                    <?php echo JHtml::_('grid.id', $i, $item->id); ?>
                </td>
                <td class="center">
                    <?php echo JHtml::_('jgrid.published', $item->published, $i, 'shares.', $canChange, 'cb'); ?>
                </td>
                <td class="nowrap has-context">
                    <div class="pull-left">
                        <?php if ($item->checked_out) : ?>
                                <?php echo JHtml::_('jgrid.checkedout', $i, $editor, $item->checked_out_time, 'shares.', $canCheckin); ?>
                            <?php endif; ?>
                        <?php if ($canEdit) : ?>
                            <a href="<?php echo JRoute::_('index.php?option=com_preachit&task=share.edit&id='.(int) $item->id); ?>">
                                <?php echo $this->escape($item->name); ?></a>
                        <?php else : ?>
                            <?php echo $this->escape($item->name); ?>
                        <?php endif; ?>
                    </div>
                    <div class="pull-left">
                        <?php
                            // Create dropdown items
                            JHtml::_('dropdown.edit', $item->id, 'share.');
                            JHtml::_('dropdown.divider');
                            if ($item->published) :
                                JHtml::_('dropdown.unpublish', 'cb' . $i, 'shares.');
                            else :
                                JHtml::_('dropdown.publish', 'cb' . $i, 'shares.');
                            endif;
                                JHtml::_('dropdown.divider');
                            if ($trashed) :
                                JHtml::_('dropdown.untrash', 'cb' . $i, 'shares.');
                            else :
                                JHtml::_('dropdown.trash', 'cb' . $i, 'shares.');
                            endif;

                            // render dropdown list
                            echo JHtml::_('dropdown.render');
                        ?>
                    </div>
                </td>
                <td class="hidden-phone">
                    <?php $code = htmlspecialchars($item->code);
                    echo JString::substr($code, 0, 149);?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<!-- /Footer -->
<?php echo JHTML::_( 'form.token' ); ?>
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="view" value="shares" />
<input type="hidden" name="boxchecked" value="0" />
</div>
<div class="clearfix"> </div>
<div style="text-align: center; padding-top: 5px;">
<?php $abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/lib/preachit.footer.php');?>
<?php echo PIfooter::footer();?></div>
</form>