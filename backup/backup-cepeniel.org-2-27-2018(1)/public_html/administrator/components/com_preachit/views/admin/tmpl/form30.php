<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
JHTML::_('behavior.tooltip');
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/tooltips.php');
$user    = JFactory::getUser();
$app = JFactory::getApplication();
$option = JRequest::getCmd('option');
$token = JSession::getFormToken() ."=1";
$db    = JFactory::getDBO();  
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select'); 
?>

<script language="javascript" type="text/javascript">
submitbutton = function(task)
    {
    if (task == "")
                {
                return false;
                }
   else if (task == "admin.migratess") 
    {
        if(confirm("<?php echo JText::_('COM_PREACHIT_BCKADMIN_IMPORTSS_WARNING');?>"))
        {Joomla.submitform(task);}
        } 
   else if (task == "admin.migratejbs") 
    {
        if(confirm("<?php echo JText::_('COM_PREACHIT_BCKADMIN_IMPORTJBS_WARNING');?>"))
        {Joomla.submitform(task);}
        } 
    else if (task == "admin.migratesm") 
    {
        if(confirm("<?php echo JText::_('COM_PREACHIT_BCKADMIN_IMPORTSM_WARNING');?>"))
        {Joomla.submitform(task);}
        } 
    else if (task == "admin.restore") 
    {
        if(confirm("<?php echo JText::_('COM_PREACHIT_BCKADMIN_OVERWRITE_WARNING1');?>"))
        {Joomla.submitform(task);}
    }
    else if (task == "admin.import") 
    {
        if(confirm("<?php echo JText::_('COM_PREACHIT_BCKADMIN_OVERWRITE_WARNING2');?>"))
        {Joomla.submitform(task);}
        } 
    else if (task == "admin.resizeimages") 
    {
        if(confirm("<?php echo JText::_('COM_PREACHIT_BCKADMIN_RESIZE_WARNING');?>"))
        {Joomla.submitform(task);}
        } 
    else {Joomla.submitform(task);}
} 

function submiturl(url)
{
    window.open(url);
}
</script>
<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
<?php if(!empty( $this->sidebar)): ?>
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
    <div id="j-main-container" class="span10">
<?php else : ?>
    <div id="j-main-container">
<?php endif;?>
    <fieldset>
        <legend><?php echo JText::_( 'COM_PREACHIT_BCKADMIN_ADMIN_ADVANCED_SETTINGS' );?></legend>
        <div class="row-striped">
            <?php if ($this->allow)   {?> 
                <div class="row-fluid">
                    <div class="span12">
                        <a class="span-marg-rgt" href="index.php?option=com_preachit&amp;view=filepaths" title="<?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_FOLDERS');?>">
                            <i class="icon-folder"></i> 
                            <?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_FOLDERS');?>
                        </a>
                    </div>
                </div>
            <?php }?> 
            <?php if ($this->allow)   {?> 
                <div class="row-fluid">
                    <div class="span12">
                        <a class="span-marg-rgt" href="index.php?option=com_preachit&amp;view=books" title="<?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_BOOKS');?>">
                            <i class="icon-bookmark"></i> 
                            <?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_BOOKS');?>
                        </a>
                    </div>
                </div>
            <?php }?> 
            <?php if ($this->allow)   {?> 
                <div class="row-fluid">
                    <div class="span12">
                        <a class="span-marg-rgt" href="index.php?option=com_preachit&amp;view=biblevers" title="<?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_BIBLEVERS');?>">
                            <i class="icon-briefcase"></i> 
                            <?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_BIBLEVERS');?>
                        </a>
                    </div>
                </div>
            <?php }?> 
            <?php if ($this->allow)   {?> 
                <div class="row-fluid">
                    <div class="span12">
                        <a class="span-marg-rgt" href="index.php?option=com_preachit&amp;view=mimes" title="<?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_MIME');?>">
                            <i class="icon-asterisk"></i> 
                            <?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_MIME');?>
                        </a>
                    </div>
                </div>
            <?php }?> 
            <?php if ($this->allow)   {?> 
                <div class="row-fluid">
                    <div class="span12">
                        <a class="span-marg-rgt" href="index.php?option=com_preachit&amp;view=mediaplayers" title="<?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_MPLAYERS');?>">
                            <i class="icon-mobile"></i> 
                            <?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_MPLAYERS');?>
                        </a>
                    </div>
                </div>
            <?php }?> 
            <?php if ($this->allow)   {?> 
                <div class="row-fluid">
                    <div class="span12">
                        <a class="span-marg-rgt" href="index.php?option=com_preachit&amp;view=shares" title="<?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_SHARE');?>">
                            <i class="icon-share"></i> 
                            <?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_SHARE');?>
                        </a>
                    </div>
                </div>
            <?php }?> 
            <?php if ($this->allow)   {?> 
                <div class="row-fluid">
                    <div class="span12">
                        <a class="span-marg-rgt" href="index.php?option=com_preachit&amp;view=extensionlist" title="<?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_EXTENSIONS');?>">
                            <i class="icon-puzzle"></i> 
                            <?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_EXTENSIONS');?>
                        </a>
                    </div>
                </div>
            <?php }?> 
        </div>
</fieldset>
<div style="margin: 20px;"></div>
<fieldset>
        <legend><?php echo JText::_( 'COM_PREACHIT_BCKADMIN_ADMIN_TASKS' );?></legend>
        <div class="row-striped">
            <?php if (!Tewebcheck::checkgd()) { $app->enqueueMessage ( JText::_('COM_PREACHIT_BCKADMIN_NO_GD_LIBRARY'), 'error' );}
            elseif ($this->allow)   {?>
                <div class="row-fluid">
                    <div class="span12">
                        <a class="span-marg-rgt" href="#" onclick="submitbutton('admin.resizeimages'); return false;" title="<?php echo JText::_('COM_PREACHIT_BCKADMIN_IMAGE_RESIZE');?>">
                            <i class="icon-pictures"></i> 
                            <?php echo JText::_('COM_PREACHIT_BCKADMIN_IMAGE_RESIZE');?>
                        </a>
                    </div>
                </div>
            <?php }?>
            <?php if ($this->allow) {?>
                <div class="row-fluid">
                    <div class="span12">
                        <a class="span-marg-rgt" href="#" onclick="submitbutton('admin.updatetable'); return false;" title="<?php echo JText::_('COM_PREACHIT_BCKADMIN_UPDATETABLE');?>">
                            <i class="icon-cogs"></i> 
                            <?php echo JText::_('COM_PREACHIT_BCKADMIN_UPDATETABLE');?>
                            <span class="small"><?php echo $this->tableversion;?></span>
                        </a>
                    </div>
                </div>
            <?php }?>
            <?php 
            $tables = $db->getTableList();
            $prefix = $db->getPrefix();
            $table = $prefix.'sermon_sermons';
            if (in_array($table, $tables) && $this->allow) {?>
                <div class="row-fluid">
                    <div class="span12">
                        <a class="span-marg-rgt" href="#" onclick="submitbutton('admin.migratess'); return false;" title="<?php echo JText::_('COM_PREACHIT_BCKADMIN_MIGRATE_SERMONSPEAKER');?>">
                            <i class="icon-unarchive"></i> 
                            <?php echo JText::_('COM_PREACHIT_BCKADMIN_MIGRATE_SERMONSPEAKER');?>
                        </a>
                    </div>
                </div>
            <?php }?>
            <?php 
            $tables = $db->getTableList();
            $prefix = $db->getPrefix();
            $table = $prefix.'submitsermon';
            if (in_array($table, $tables) && $this->allow) {?>
                <div class="row-fluid">
                    <div class="span12">
                        <a class="span-marg-rgt" href="#" onclick="submitbutton('admin.migratesm'); return false;" title="<?php echo JText::_('COM_PREACHIT_BCKADMIN_MIGRATE_SERMON_MANAGER');?>">
                            <i class="icon-unarchive"></i> 
                            <?php echo JText::_('COM_PREACHIT_BCKADMIN_MIGRATE_SERMON_MANAGER');?>
                        </a>
                    </div>
                </div>
            <?php }?>
            <?php 
            $tables = $db->getTableList();
            $prefix = $db->getPrefix();
            $table = $prefix.'bsms_studies';
            if (in_array($table, $tables) && $this->allow) {?>
                <div class="row-fluid">
                    <div class="span12">
                        <a class="span-marg-rgt" href="#" onclick="submitbutton('admin.migratejbs'); return false;" title="<?php echo JText::_('COM_PREACHIT_BCKADMIN_MIGRATE_JOOMLABS');?>">
                            <i class="icon-unarchive"></i> 
                            <?php echo JText::_('COM_PREACHIT_BCKADMIN_MIGRATE_JOOMLABS');?>
                        </a>
                    </div>
                </div>
            <?php }?>
            <?php if ($this->allow) {?>
                <div class="row-fluid">
                    <div class="span12">
                        <a class="span-marg-rgt" href="#" onclick="submiturl('index.php?option=com_preachit&task=admin.backupall&<?php echo $token;?>'); return false;" title="<?php echo JText::_('COM_PREACHIT_BCKADMIN_BACKUP');?>">
                            <i class="icon-contract"></i> 
                            <?php echo JText::_('COM_PREACHIT_BCKADMIN_BACKUP');?>
                        </a>
                    </div>
                </div>
            <?php }?>
            <?php if ($this->allow) {?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="span-marg-rgt">
                            <i class="icon-database"></i> 
                            <?php echo JText::_('COM_PREACHIT_BCKADMIN_CHARACTERSET');?>
                        <?php echo $this->codeset;?><span style="padding: 0 0 0 10px;"><button type="button" onclick="submitbutton('admin.codeset')">
 <?php echo JText::_('COM_PREACHIT_BCKADMIN_CHANGE_DATABASE');?> </button></span>
                        </div>
                    </div>
                </div>
            <?php }?>
            <?php if ($this->allow) {?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="span-marg-rgt">
                            <i class="icon-expand"></i> 
                            <?php echo JText::_('COM_PREACHIT_BCKADMIN_RESTORE');?>
                        <input type="file" name ="uploadfile" value="" /><button type="button" onclick="submitbutton('admin.restore')">
 <?php echo JText::_('COM_PREACHIT_BCKADMIN_RESTORE_BUTTON');?> </button><button type="button" onclick="submitbutton('admin.import')">
 <?php echo JText::_('COM_PREACHIT_BCKADMIN_IMPORT_BUTTON');?> </button>
 <?php echo PIHelpertooltips::restore();?>
                        </div>
                    </div>
                </div>
            <?php }?>
       </div>
 </fieldset>

</div>

<!-- Footer -->
<div style="text-align: center">
<?php $abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/lib/preachit.footer.php');?>
<?php echo PIfooter::footer();?></div>
<!-- /Footer -->
<input type="hidden" name="char" value="codeset" />
<input type="hidden" name="overwrite" value="0" />
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="view" value="admin" />
<input type="hidden" name="task" value ="" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>