<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
JHTML::_('behavior.tooltip');
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/tooltips.php');
$user    = JFactory::getUser();
$app = JFactory::getApplication();
$option = JRequest::getCmd('option');
$token = JSession::getFormToken() ."=1";
?>

<script language="javascript" type="text/javascript">
submitbutton = function(task)
    {
    if (task == "")
                {
                return false;
                }
   else if (task == "admin.migratess") 
    {
        if(confirm("<?php echo JText::_('COM_PREACHIT_BCKADMIN_IMPORTSS_WARNING');?>"))
        {Joomla.submitform(task);}
        } 
   else if (task == "admin.migratejbs") 
    {
        if(confirm("<?php echo JText::_('COM_PREACHIT_BCKADMIN_IMPORTJBS_WARNING');?>"))
        {Joomla.submitform(task);}
        } 
    else if (task == "admin.migratesm") 
    {
        if(confirm("<?php echo JText::_('COM_PREACHIT_BCKADMIN_IMPORTSM_WARNING');?>"))
        {Joomla.submitform(task);}
        } 
    else if (task == "admin.restore") 
    {
        if(confirm("<?php echo JText::_('COM_PREACHIT_BCKADMIN_OVERWRITE_WARNING1');?>"))
        {Joomla.submitform(task);}
    }
    else if (task == "admin.import") 
    {
        if(confirm("<?php echo JText::_('COM_PREACHIT_BCKADMIN_OVERWRITE_WARNING2');?>"))
        {Joomla.submitform(task);}
        } 
    else if (task == "admin.resizeimages") 
    {
        if(confirm("<?php echo JText::_('COM_PREACHIT_BCKADMIN_RESIZE_WARNING');?>"))
        {Joomla.submitform(task);}
        } 
    else {Joomla.submitform(task);}
} 

function submiturl(url)
{
    window.open(url);
}
</script>
<style>
#cpanel img {float: none;}

</style>
<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
<div class="width-100">
<fieldset class="adminform">
<legend><?php echo JText::_( 'COM_PREACHIT_BCKADMIN_ADMIN_ADVANCED_SETTINGS' );?></legend>
  <table class="thisform">
    <tr class="thisform">
      <td width = "100%" valign = "top" class = "thisform"><div id="cpanel">
       <?php if ($this->allow)   {?>  
          <div style = "float:left;">
            <div class = "icon"> <a href = "index.php?option=com_preachit&amp;view=filepaths" style = "text-decoration:none;" title = "<?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_FOLDERS');?>"> <img src = "../media/preachit/images/folder.png"  align = "middle" border = "0"/> <span> <?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_FOLDERS'); ?> </span></a> </div>
          </div>
        <?php }?> 
         <?php if ($this->allow)   {?>  
          <div style = "float:left;">
            <div class = "icon"> <a href = "index.php?option=com_preachit&amp;view=books" style = "text-decoration:none;" title = "<?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_BOOKS');?>"> <img src = "../media/preachit/images/book.png"  align = "middle" border = "0"/> <span> <?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_BOOKS'); ?> </a> </div>
          </div>
        <?php }?> 
         <?php if ($this->allow)   {?>  
          <div style = "float:left;">
            <div class = "icon"> <a href = "index.php?option=com_preachit&amp;view=biblevers" style = "text-decoration:none;" title = "<?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_BIBLEVERS');?>"> <img src = "../media/preachit/images/version.png"  align = "middle" border = "0"/> <span> <?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_BIBLEVERS'); ?> </a> </div>
          </div>
        <?php }?> 
         <?php if ($this->allow)   {?>  
          <div style = "float:left;">
            <div class = "icon"> <a href = "index.php?option=com_preachit&amp;view=mimes" style = "text-decoration:none;" title = "<?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_MIME');?>"> <img src = "../media/preachit/images/mime.png"  align = "middle" border = "0"/> <span> <?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_MIME');?> </a> </div>
          </div>
        <?php }?> 
         <?php if ($this->allow)   {?>  
          <div style = "float:left;">
            <div class = "icon"> <a href = "index.php?option=com_preachit&amp;view=mediaplayers" style = "text-decoration:none;" title = "<?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_MPLAYERS');?>"> <img src = "../media/preachit/images/player.png" align = "middle" border = "0"/> <span> <?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_MPLAYERS'); ?> </a> </div>
          </div>
        <?php }?> 
         <?php if ($this->allow)   {?>  
          <div style = "float:left;">
            <div class = "icon"> <a href = "index.php?option=com_preachit&amp;view=shares" style = "text-decoration:none;" title = "<?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_SHARE');?>"> <img src = "../media/preachit/images/share.png" align = "middle" border = "0"/> <span> <?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_SHARE'); ?> </a> </div>
          </div>
         <?php }?> 
         <?php if ($this->allow)   {?>  
          <div style = "float:left;">
            <div class = "icon"> <a href = "index.php?option=com_preachit&amp;view=extensionlist" style = "text-decoration:none;" title = "<?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_EXTENSIONS');?>"> <img src = "../media/preachit/images/extension.png" align = "middle" border = "0"/> <span> <?php echo JText::_('COM_PREACHIT_ADMIN_CPANEL_EXTENSIONS'); ?> </a> </div>
          </div>
         <?php }?> 
        </div></td>
    </tr>
  </table>
</fieldset>
</div>
<div class="width-100">
<fieldset class="adminform">
<legend><?php echo JText::_( 'COM_PREACHIT_BCKADMIN_ADMIN_TASKS' );?></legend>
  <table class="thisform">
    <tr class="thisform">
        <td width = "100%" valign = "top" class = "thisform"><div id="cpanel">
<?php if (!Tewebcheck::checkgd()) { $app->enqueueMessage ( JText::_('COM_PREACHIT_BCKADMIN_NO_GD_LIBRARY'), 'error' );}
elseif ($this->allow)   {?>
        <div style = "float:left;">
            <div class = "icon"> <a href="#" onclick="submitbutton('admin.resizeimages'); return false;" style = "text-decoration:none;" title = "<?php echo JText::_('COM_PREACHIT_BCKADMIN_IMAGE_RESIZE');?>"> <img src = "../media/preachit/images/Map.png"  align = "middle" border = "0"/> <span> <?php echo JText::_('COM_PREACHIT_BCKADMIN_IMAGE_RESIZE');?> </span></a> </div>
          </div>
        <?php }?>        
<?php if ($this->allow)   {?>
        <div style = "float:left;">
            <div class = "icon"> <a href="#" onclick="submitbutton('admin.updatetable'); return false;" style = "text-decoration:none;" title = "<?php echo JText::_('COM_PREACHIT_BCKADMIN_UPDATETABLE');?>"> <img src = "../media/preachit/images/tables.png"  align = "middle" border = "0"/><span> <?php echo JText::_('COM_PREACHIT_BCKADMIN_UPDATETABLE');?> <?php echo $this->tableversion;?></span></a> </div>
          </div>
<?php }?>  
<?php 
$db    =  JFactory::getDBO();   
$tables = $db->getTableList();
$prefix = $db->getPrefix();
$table = $prefix.'sermon_sermons';
if (in_array($table, $tables) && $this->allow)
   {?>
        <div style = "float:left;">
            <div class = "icon"> <a href="#" onclick="submitbutton('admin.migratess'); return false;" style = "text-decoration:none;" title = "<?php echo JText::_('COM_PREACHIT_BCKADMIN_MIGRATE_SERMONSPEAKER');?>"> <img src = "../media/preachit/images/sspeakers.png"  align = "middle" border = "0"/> <span> <?php echo JText::_('COM_PREACHIT_BCKADMIN_MIGRATE_SERMONSPEAKER');?> </span></a> </div>
          </div>
<?php }?> 
<?php 
$db    =  JFactory::getDBO();   
$tables = $db->getTableList();
$prefix = $db->getPrefix();
$table = $prefix.'submitsermon';
if (in_array($table, $tables) && $this->allow)
{?>
        <div style = "float:left;">
            <div class = "icon"> <a href="#" onclick="submitbutton('admin.migratesm'); return false;" style = "text-decoration:none;" title = "<?php echo JText::_('COM_PREACHIT_BCKADMIN_MIGRATE_SERMON_MANAGER');?>"> <img src = "../media/preachit/images/genericmigrate.png"  align = "middle" border = "0"/> <span> <?php echo JText::_('COM_PREACHIT_BCKADMIN_MIGRATE_SERMON_MANAGER');?> </span></a> </div>
          </div>
<?php }?> 
<?php 
$db    =  JFactory::getDBO();   
$tables = $db->getTableList();
$prefix = $db->getPrefix();
$table = $prefix.'bsms_studies';
if (in_array($table, $tables) && $this->allow)
{?>
        <div style = "float:left;">
            <div class = "icon"> <a href="#" onclick="submitbutton('admin.migratejbs'); return false;" style = "text-decoration:none;" title = "<?php echo JText::_('COM_PREACHIT_BCKADMIN_MIGRATE_JOOMLABS');?>"> <img src = "../media/preachit/images/jbslogo.png"  align = "middle" border = "0"/> <span> <?php echo JText::_('COM_PREACHIT_BCKADMIN_MIGRATE_JOOMLABS');?> </span></a> </div>
          </div>
<?php }?>   
<?php if ($this->allow)   {?>
        <div style = "float:left;">
            <div class = "icon"> <a href="#" onclick="submiturl('index.php?option=com_preachit&task=admin.backupall&<?php echo $token;?>'); return false;" style = "text-decoration:none;" title = "<?php echo JText::_('COM_PREACHIT_BCKADMIN_BACKUP');?>"> <img src = "../media/preachit/images/Disquette.png"  align = "middle" border = "0"/> <span> <?php echo JText::_('COM_PREACHIT_BCKADMIN_BACKUP');?> </span></a> </div>
          </div>
<?php }?>    
</td>
</tr>
</table  
<?php if ($this->allow)
{?>
<fieldset class="panelform">
<ul>     
<li>
<label style="width: 220px;">
<?php echo JText::_('COM_PREACHIT_BCKADMIN_CHARACTERSET');?>
</label>
<?php echo $this->codeset;?><span style="padding: 0 0 0 10px;"><button type="button" onclick="submitbutton('admin.codeset')">
 <?php echo JText::_('COM_PREACHIT_BCKADMIN_CHANGE_DATABASE');?> </button></span>
</li>
<li> 
<label style="width: 220px;">
<?php echo JText::_('COM_PREACHIT_BCKADMIN_RESTORE');?>
</label>
<input type="file" name ="uploadfile" value="" /><button type="button" onclick="submitbutton('admin.restore')">
 <?php echo JText::_('COM_PREACHIT_BCKADMIN_RESTORE_BUTTON');?> </button><button type="button" onclick="submitbutton('admin.import')">
 <?php echo JText::_('COM_PREACHIT_BCKADMIN_IMPORT_BUTTON');?> </button>
 <?php echo PIHelpertooltips::restore();?>
</li>
</ul>
</fieldset>
<?php } ?>
</fieldset>
</div>
<!-- Footer -->
<div style="text-align: center">
<?php $abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/lib/preachit.footer.php');?>
<?php echo PIfooter::footer();?></div>
<!-- /Footer -->
<input type="hidden" name="char" value="codeset" />
<input type="hidden" name="overwrite" value="0" />
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="view" value="admin" />
<input type="hidden" name="task" value ="" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>