<?php

/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */

// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' );

class PreachitViewAdmin extends JViewLegacy

{

function display($tpl = null)

{
$document = JFactory::getDocument();
$document->addStyleSheet('../media/preachit/css/preachit.css');
$db = JFactory::getDBO();
// get Joomla version to decide which form and method

$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/forms.php');
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/lib/preachit.version.php');
$user	= JFactory::getUser();

//codeset list

$codeset = array(
array('value' => 'general', 'text' => JText::_('general')),
array('value' => 'unicode', 'text' => JText::_('unicode')),
array('value' => 'czech', 'text' => JText::_('czech')),
array('value' => 'danish', 'text' => JText::_('danish')),
array('value' => 'esperato', 'text' => JText::_('esperato')),
array('value' => 'estonian', 'text' => JText::_('estonian')),
array('value' => 'icelandic', 'text' => JText::_('icelandic')),
array('value' => 'latvian', 'text' => JText::_('latvian')),
array('value' => 'lithuanian', 'text' => JText::_('lithuanian')),
array('value' => 'persian', 'text' => JText::_('persian')),
array('value' => 'polish', 'text' => JText::_('polish')),
array('value' => 'roman', 'text' => JText::_('roman')),
array('value' => 'romanian', 'text' => JText::_('romanian')),
array('value' => 'slovak', 'text' => JText::_('slovak')),
array('value' => 'slovenian', 'text' => JText::_('slovenian')),
array('value' => 'spanish2', 'text' => JText::_('spanish2')),
array('value' => 'spanish', 'text' => JText::_('spanish')),
array('value' => 'swedish', 'text' => JText::_('swedish')),
array('value' => 'turkish', 'text' => JText::_('turkish')),
array('value' => 'bin', 'text' => JText::_('bin')),
);
$codesetlist = JHTML::_('select.genericList', $codeset, 'codeset', 'class="inputbox" '. '', 'value', 'text', 'general' );
$this->assignRef('codeset', $codesetlist);

// get table version

$ad = '#__pibckadmin';
$fields = $db->getTableColumns(  $ad  );
$entry = false;
$entry	= isset( $fields['tableversion'] );
$check = $entry;	
if ($entry)
{
$query = "
  SELECT ".$db->quoteName('tableversion')."
    FROM ".$db->quoteName('#__pibckadmin')."
    WHERE ".$db->quoteName('id')." = ".$db->quote(1).";
  ";
$db->setQuery($query);
$currentversion = $db->loadResult();

}
else $currentversion = '';
$tableversion = ' '.$currentversion;
$latestversion = PIVersion::versionXML();
if ($currentversion != $latestversion)
{
    $tableversion .= '<span style="display: inline;" class="piupgrade"> '.$latestversion.'</span>';
}
$this->assignRef('tableversion', $tableversion);

// get permission for certain admin tasks
$allow = false;
$user = JFactory::getUser();
if ($user->authorise('core.admin', 'com_preachit'))
{$allow = true;}

$this->assignRef('allow', $allow);

// load menu
PreachitHelper::addSubmenu('admin');

$this->addToolbar();

if (Tewebcheck::getJversion() >= 3.0)
{
    $this->setLayout('form30');
    $this->sidebar = JHtmlSidebar::render();   
}
else {$this->setLayout('form16');}

parent::display($tpl);

}

/**
     * Add the page title and toolbar.
     *
     * @since    1.6
     */
protected function addToolbar()
{
    $user    = JFactory::getUser();   
    JToolBarHelper::title( JText::_( 'COM_PREACHIT_BCKADMIN_TITLE' ), 'admin.png' );
    if ($user->authorise('core.admin', 'com_preachit'))  {
    JToolBarHelper::preferences('com_preachit', '550', '900');
    JToolBarHelper::divider();
    JToolBarHelper::help('pihelp', 'com_preachit');
    }
}
}