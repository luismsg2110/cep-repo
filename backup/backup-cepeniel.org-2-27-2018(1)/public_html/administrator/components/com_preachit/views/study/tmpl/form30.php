<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/tooltips.php');
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/additional.php');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');
JText::script('COM_PREACHIT_FIELDS_INVALID');
$document = JFactory::getDocument();      
$document->addScript(JURI::root() . 'media/preachit/js/pisubmitbutton.js');
$document->addScript(JURI::root() . 'media/preachit/js/pistudyinsert.js');
$document->addScript('https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js');
$document->addScript(JURI::root() . 'media/lib_teweb/autocomplete/tag-it.js');
$document->addStyleSheet('http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/flick/jquery-ui.css');
$document->addStyleSheet(JURI::root().'media/lib_teweb/autocomplete/jquery.tagit.css');
$user	= JFactory::getUser();
$option = JRequest::getCmd('option');
?>
<script language="javascript" type="text/javascript">

document.addEvent('domready', function() {
 
    // Test source, list of tags from http://del.icio.us/tag/
    var tokens = [<?php echo $this->taglist;?>];
 
    jQuery('#jform_tags').tagit({
                availableTags: tokens
            });
 
});

function setDisabled(filter, setting)
{
    if (filter)
    {filter.disabled = setting;} 
    return
}

function setDisabledall(setting)
{
    var filter0 = document.getElementById('jform_audio_folder');
    var filter1 = document.getElementById('jform_video_folder');
    var filter2 = document.getElementById('jform_downloadvid_folder');
    var filter3 = document.getElementById('jform_notes_folder');
    var filter4 = document.getElementById('jform_slides_folder');
    var filter5 = document.getElementById('jform_image_folderlrg');
    var filter6 = document.getElementById('jform_vclip1_folder');
    var filter7 = document.getElementById('jform_vclip2_folder');
    var filter8 = document.getElementById('jform_vclip3_folder');
    setDisabled(filter0, setting);
    setDisabled(filter1, setting);
    setDisabled(filter2, setting);
    setDisabled(filter3, setting);
    setDisabled(filter4, setting);
    setDisabled(filter5, setting);
    setDisabled(filter6, setting);
    setDisabled(filter7, setting);
    setDisabled(filter8, setting);
}

window.addEvent('domready', function(){
                setDisabledall(true);
            })

function tefadeelement(id, input, fade)
{
    var container = $(id);
    if (input != '')
    {input.value = '';}
    container.set('tween',{'duration' : 500}).fade(fade);
    if (fade == 'out')
    {
    (function() {
    {container.set('class', 'control-group tehidden');}
    }).delay(500);
    }
    else {container.set('class', 'control-group');}
    
    return false;
}

</script>
<style>
.tehidden, .tehidden .control-label, .tehidden .controls, .tehidden input, .tehidden label {
    height: 0 !important;
    margin: 0 !important;   
    padding: 0 !important; 
}
</style>
<form class ="form-validate" action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
    <div class="form-horizontal">
        <fieldset>
            <ul class="nav nav-tabs">
                <li class="active"><a href="#main" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_ADMIN_MESSAGES_LEGEND');?></a></li>
                <?php
                if (Tewebcheck::showtab($this->form, 'displayoptions', $this->hide) || Tewebcheck::showtab($this->form, 'puboptions', $this->hide) || Tewebcheck::showtab($this->form, 'podoptions', $this->hide) || Tewebcheck::showtab($this->form, 'metaoptions', $this->hide))
                {?>
                <li><a href="#addinfo" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_ADMIN_LINKHEAD');?></a></li>
                <?php } ?>
                <?php if (Tewebcheck::showtab($this->form, 'audtab', $this->hide) || Tewebcheck::showtab($this->form, 'audpurtab', $this->hide))
                {?>
                <li><a href="#mesaudio" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_ADMIN_AUDIOHEAD');?></a></li>
                <?php } ?>
                <?php if (Tewebcheck::showtab($this->form, 'vidtab', $this->hide) || Tewebcheck::showtab($this->form, 'addvidtab', $this->hide) || Tewebcheck::showtab($this->form, 'vidpurtab', $this->hide))
                {?>
                <li><a href="#mesvideo" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_ADMIN_VIDEOHEAD');?></a></li>
                <?php } ?>
                <?php if (!in_array('study_text', $this->hide))
                {?>
                <li><a href="#mestext" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_ADMIN_TEXTHEAD');?></a></li>
                <?php } ?>
                <?php if (Tewebcheck::showtab($this->form, 'notestab', $this->hide))
                {?>
                <li><a href="#mesnotes" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_ADMIN_NOTESHEAD');?></a></li>
                <?php }?>
                <?php if (Tewebcheck::showtab($this->form, 'slidestab', $this->hide))
                {?>
                <li><a href="#messlides" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_ADMIN_SLIDESHEAD');?></a></li>   
                <?php }?>
                <?php if (Tewebcheck::showtab($this->form, 'imagetab', $this->hide))
                {?>
                <li><a href="#mesimage" data-toggle="tab"><?php echo JText::_('COM_PREACHIT_ADMIN_IMAGEHEAD');?></a></li>
                <?php }?>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="main">
                    <?php
                        foreach ($this->form->getFieldset("maininfo") as $field):
                        if (!$user->authorise('core.edit.state', 'com_preachit') && $field->name == 'jform[published]')
                        {continue;}
                        if (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
                        {
                                ?>
                                  <?php 
                                        if ($field->hidden):
                                            echo $field->input;
                                        elseif (!$user->authorise('core.edit.state', 'com_preachit') && $field->name == 'jform[published]'):
                                                    continue;
                                        else:
                                        ?>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php if ($field->name != 'jform[dur_hrs]') {echo $field->input;} ?>
                                                <?php if ($field->name == 'jform[study_book]')
                                                {?>
                                                <label class="small"><?php echo JText::_('COM_PREACHIT_ADMIN_CH_BEG');?></label><?php echo $this->form->getInput('ref_ch_beg'); ?>
                                                <label class="small"><?php echo JText::_('COM_PREACHIT_ADMIN_VS_BEG');?></label><?php echo $this->form->getInput('ref_vs_beg'); ?>
                                                <label class="small"><?php echo JText::_('COM_PREACHIT_ADMIN_CH_END');?></label><?php echo $this->form->getInput('ref_ch_end'); ?>
                                                <label class="small"><?php echo JText::_('COM_PREACHIT_ADMIN_VS_END');?></label><?php echo $this->form->getInput('ref_vs_end'); ?> <?php } ?>
                                                <?php if ($field->name == 'jform[study_book2]')
                                                {?>
                                                <label class="small"><?php echo JText::_('COM_PREACHIT_ADMIN_CH_BEG');?></label><?php echo $this->form->getInput('ref_ch_beg2'); ?>
                                                <label class="small"><?php echo JText::_('COM_PREACHIT_ADMIN_VS_BEG');?></label><?php echo $this->form->getInput('ref_vs_beg2'); ?>
                                                <label class="small"><?php echo JText::_('COM_PREACHIT_ADMIN_CH_END');?></label><?php echo $this->form->getInput('ref_ch_end2'); ?>
                                                <label class="small"><?php echo JText::_('COM_PREACHIT_ADMIN_VS_END');?></label><?php echo $this->form->getInput('ref_vs_end2'); ?> <?php } ?>
                                                <?php if ($field->name == 'jform[dur_hrs]')
                                                {?>
                                                <label class="small"><?php echo JText::_('COM_PREACHIT_ADMIN_HRS');?></label>
                                                <?php echo $this->form->getInput('dur_hrs'); ?>
                                                <label class="small"><?php echo JText::_('COM_PREACHIT_ADMIN_MINS');?></label>
                                                <?php echo $this->form->getInput('dur_mins'); ?>
                                                <label class="small"><?php echo JText::_('COM_PREACHIT_ADMIN_SECS');?></label>
                                                <?php echo $this->form->getInput('dur_secs'); ?><?php } ?>
                                                <?php if ($field->name == 'jform[teacher][]' || $field->name == 'jform[series]' || $field->name == 'jform[ministry][]')
                                                {?><button class="btn btn-info btn-small" type="button" style="vertical-align: top; margin-top: 2px;" onclick="tefadeelement('pinew_<?php echo str_replace('jform_','',$field->id);?>', '', 'in'); return false;">
                                                 <?php echo JText::_('COM_PREACHIT_ADD_NEW_RECORD');?> </button><?php } ?>
                                                 <?php if ($field->name == 'jform[hits]')
                                                    {?><button class="btn btn-small btn-warning" type="button" style="margin-top: 0;" onclick="Joomla.submitbutton('study.reset')">
                                                        <?php echo JText::_('COM_PREACHIT_ADMIN_BUTTON_RESET');?> </button><?php } ?>
                                            </div>
                                        </div>
                                        <?php if ($field->name == 'jform[teacher][]' || $field->name == 'jform[series]' || $field->name == 'jform[ministry][]')
                                        {   $name = str_replace('jform_','',$field->id);
                                            $groupid = 'pinew_'. $name;
                                            ?>
                                                <div class="control-group tehidden" id="<?php echo $groupid;?>" style="opacity: 0; visibility: hidden;">
                                                    <div class="control-label">
                                                        <?php echo $this->form->getLabel('new'.$name); ?>
                                                    </div>
                                                    <div class="controls">
                                                        <?php echo $this->form->getInput('new'.$name); ?>
                                                        <button type="button" onclick="tefadeelement('<?php echo $groupid;?>', 'jform_new<?php echo $name;?>', 'out'); return false;"><?php echo JText::_('JCANCEL') ?></button>
                                                    </div>
                                                </div>
                                        <?php } ?>  
                                        <?php
                                        endif;
                                   
                                    ?>
                                <?php }
                                endforeach;?>
                </div>
                <div class="tab-pane" id="addinfo">
                    <?php
                        if ($user->authorise('core.edit.state', 'com_preachit') && Tewebcheck::showtab($this->form, 'puboptions', $this->hide)) { ?>
                        <h2><?php echo JText::_('COM_PREACHIT_SUB_PUBLISH_OPTIONS');?></h2>
                        <?php
                        foreach ($this->form->getFieldset("puboptions") as $field):
                        if (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
                        {
                                ?>
                                  <?php 
                                        if ($field->hidden):
                                            echo $field->input;
                                        else:
                                        ?>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                        <?php
                                        endif;
                                   
                                    ?>
                                <?php   }
                                endforeach;?>
                    <?php } ?>
                    <?php if (Tewebcheck::showtab($this->form, 'podoptions', $this->hide))
                    { ?>
                    <div class="clearfix"></div>
                    <?php
                    foreach ($this->form->getFieldset("podoptions") as $field):
                    if (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
                    {
                            ?>
                              <?php 
                                    if ($field->hidden):
                                        echo $field->input;
                                    else:
                                    ?>
                                    <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                    <?php
                                    endif;
                               
                                ?>
                            <?php   }
                            endforeach;?>
                    <?php } ?>
                    <?php if (Tewebcheck::showtab($this->form, 'displayoptions', $this->hide))
                    { ?>
                    <div class="clearfix"></div>
                    <h2><?php echo JText::_('COM_PREACHIT_SUB_DISPLAY_OPTIONS');?></h2>
                    <?php
                    foreach ($this->form->getFieldset("displayoptions") as $field):
                    if (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
                    {
                            ?>
                              <?php 
                                    if ($field->hidden):
                                        echo $field->input;
                                    else:
                                    ?>
                                    <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                    <?php
                                    endif;
                               
                                ?>
                            <?php }
                            endforeach;?>
                    <?php } ?>
                    <?php if (Tewebcheck::showtab($this->form, 'metaoptions', $this->hide))
                    { ?>
                    <div class="clearfix"></div>
                    <h2><?php echo JText::_('JGLOBAL_FIELDSET_METADATA_OPTIONS');?></h2>
                    <?php
                    foreach ($this->form->getFieldset("metaoptions") as $field):
                    if (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
                    {
                            ?>
                              <?php 
                                    if ($field->hidden):
                                        echo $field->input;
                                    else:
                                    ?>
                                    <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                    <?php 
                                    endif;
                               
                                ?>
                            <?php }
                            endforeach;?>
                    <?php } ?>
                </div>
                <div class="tab-pane" id="mesaudio">
                    <?php if (Tewebcheck::showtab($this->form, 'audtab', $this->hide))
                    { ?>
                    <div class="control-group">
                        <div class="control-label">
                            <label class="pitablesubhead"><?php echo JText::_('COM_PREACHIT_ADMIN_SUB_FILE');?></label>
                        </div>
                    </div>
                    <?php
                    foreach ($this->form->getFieldset("audtab") as $field) {
                    if ($field->name == 'jform[audio_folder]' && in_array('audio_folder', $this->hide) && !in_array('audio_link', $this->hide)) 
                    {?>
                        <input type="hidden" id="<?php echo $field->id;?>" "name="<?php echo $field->name;?>" value="<?php $value = Tewebcheck::gettvalue($field->name); echo $this->item->$value;?>" />
                    <?php }   
                    elseif (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
                    {
                            ?> 
                              <?php 
                                    if ($field->hidden):
                                        echo $field->input;
                                    else:
                                    ?>
                                    <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                                <?php if ($field->name == 'jform[audiofs]')
                                                {echo PIHelpertooltips::filesize(0);}?>
                                            </div>
                                        </div>
                                    <?php
                                    endif;
                               
                                ?>
                            <?php
                    }
                    }
                    }?>
                    <?php if (Tewebcheck::showtab($this->form, 'audpurtab', $this->hide))
                    {?>
                    <div class="control-group">
                        <div class="control-label">
                            <label class="pitablesubhead"><?php echo JText::_('COM_PREACHIT_ADMIN_SUB_PUR');?></label>
                        </div>
                    </div>
                    <?php
                    foreach ($this->form->getFieldset("audpurtab") as $field) {
                    if (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
                    {
                            ?> 
                              <?php 
                                    if ($field->hidden):
                                        echo $field->input;
                                    else:
                                    ?>
                                    <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                    <?php
                                    endif;
                               
                                ?>
                            <?php
                    }
                    }
                    }?>
            </div>
            <div class="tab-pane" id="mesvideo">
                <?php if (Tewebcheck::showtab($this->form, 'vidtab', $this->hide))
                {?>
                <div class="control-group">
                        <div class="control-label">
                            <label class="pitablesubhead"><?php echo JText::_('COM_PREACHIT_ADMIN_SUB_FILE');?></label>
                        </div>
                    </div>
                <?php
                foreach ($this->form->getFieldset("vidtab") as $field) {
                if ($field->name == 'jform[video_folder]' && in_array('video_folder', $this->hide) && !in_array('video_link', $this->hide)) 
                {?>
                    <input type="hidden" id="<?php echo $field->id;?>" "name="<?php echo $field->name;?>" value="<?php $value = Tewebcheck::gettvalue($field->name); echo $this->item->$value;?>" />
                <?php }   
                elseif (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
                {
                        ?> 
                          <?php 
                                if ($field->hidden):
                                    echo $field->input;
                                else:
                                ?>
                                <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                                <?php if ($field->name == 'jform[videofs]')
                                                {echo PIHelpertooltips::filesize(0);}?>
                                            </div>
                                        </div>
                                <?php
                                endif;
                           
                            ?>
                        <?php
                }
                }
                }?>
                <?php if (Tewebcheck::showtab($this->form, 'addvidtab', $this->hide))
                {?>
                <div class="control-group">
                        <div class="control-label">
                            <label class="pitablesubhead"><?php echo JText::_('COM_PREACHIT_ADMIN_SUB_VIDEO_ADDFILE');?></label>
                        </div>
                    </div>
                <?php
                foreach ($this->form->getFieldset("addvidtab") as $field) {
                if ($field->name == 'jform[downloadvid_folder]' && in_array('downloadvid_folder', $this->hide) && !in_array('downloadvid_link', $this->hide)) 
                {?>
                    <input type="hidden" id="<?php echo $field->id;?>" "name="<?php echo $field->name;?>" value="<?php $value = Tewebcheck::gettvalue($field->name); echo $this->item->$value;?>" />
                <?php }   
                elseif (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
                {
                        ?> 
                          <?php 
                                if ($field->hidden):
                                    echo $field->input;
                                else:
                                ?>
                                <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                                <?php if ($field->name == 'jform[advideofs]')
                                                {echo PIHelpertooltips::filesize(0);}?>
                                            </div>
                                        </div>
                                <?php
                                endif;       
                            ?>
                        <?php
                }
                }
                }?>
                <?php if (Tewebcheck::showtab($this->form, 'vidpurtab', $this->hide))
                {?>
                <div class="control-group">
                        <div class="control-label">
                            <label class="pitablesubhead"><?php echo JText::_('COM_PREACHIT_ADMIN_SUB_PUR');?></label>
                        </div>
                    </div>
                <?php
                foreach ($this->form->getFieldset("vidpurtab") as $field) {
                if (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
                {
                        ?> 
                          <?php 
                                if ($field->hidden):
                                    echo $field->input;
                                else:
                                ?>
                                <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                <?php
                                endif;
                           
                            ?>
                        <?php
                }
                }
                }?>
                <?php if (Tewebcheck::showtab($this->form, 'vcliptab', $this->hide))
                {?>
                <div class="control-group">
                        <div class="control-label">
                            <label class="pitablesubhead"><?php echo JText::_('COM_PREACHIT_ADMIN_SUB_VCLIP');?></label>
                        </div>
                    </div>
                <?php
                foreach ($this->form->getFieldset("vcliptab") as $field) {
                if (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
                {
                        ?> 
                          <?php 
                                if ($field->hidden):
                                    echo $field->input;
                                else:
                                ?>
                                <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                            </div>
                                        </div>
                                <?php
                                endif;
                           
                            ?>
                        <?php
                }
                }
                }?>
            </div>
            <div class="tab-pane" id="mestext">
                <?php if (!in_array('study_text', $this->hide))
                {?>
                    <div class="control-group">   
                            <?php echo $this->form->getInput('study_text'); ?>
                    </div>
                <?php
                }?>
            </div>
            <div class="tab-pane" id="mesnotes">
                <?php
                    foreach ($this->form->getFieldset("notestab") as $field) {
                    if ($field->name == 'jform[notes_folder]' && in_array('notes_folder', $this->hide) && !in_array('notes_link', $this->hide)) 
                    {?>
                        <input type="hidden" id="<?php echo $field->id;?>" "name="<?php echo $field->name;?>" value="<?php $value = Tewebcheck::gettvalue($field->name); echo $this->item->$value;?>" />
                    <?php }   
                    elseif (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
                    {
                            ?> 
                              <?php 
                                    if ($field->hidden):
                                        echo $field->input;
                                    else:
                                    ?>
                                    <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                                <?php if ($field->name == 'jform[notesfs]')
                                                {echo PIHelpertooltips::filesize(0);}?>
                                            </div>
                                        </div>
                                    <?php
                                    endif;
                               
                                ?>
                            <?php
                    }
                    }?>
            </div>
            <div class="tab-pane" id="messlides">
                <?php
                    foreach ($this->form->getFieldset("slidestab") as $field) {
                    if ($field->name == 'jform[slides_folder]' && in_array('slides_folder', $this->hide) && !in_array('slides_link', $this->hide)) 
                    {?>
                        <input type="hidden" id="<?php echo $field->id;?>" "name="<?php echo $field->name;?>" value="<?php $value = Tewebcheck::gettvalue($field->name); echo $this->item->$value;?>" />
                    <?php }   
                    elseif (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
                    {
                            ?> 
                              <?php 
                                    if ($field->hidden):
                                        echo $field->input;
                                    else:
                                    ?>
                                    <div class="control-group">
                                            <div class="control-label">
                                                <?php echo $field->label; ?>   
                                            </div>
                                            <div class="controls">     
                                                <?php echo $field->input; ?>
                                                <?php if ($field->name == 'jform[slidesfs]')
                                                {echo PIHelpertooltips::filesize(0);}?>
                                            </div>
                                        </div>
                                    <?php
                                    endif;
                               
                                ?>
                            <?php
                    }
                    }?>
            </div>
            <div class="tab-pane" id="mesimage">
                <?php 
                foreach ($this->form->getFieldset("imagetab") as $field) {
                if ($field->name == 'jform[image_folderlrg]' && in_array('image_folderlrg', $this->hide) && !in_array('imagelrg', $this->hide)) 
                {?>
                    <input type="hidden" id="<?php echo $field->id;?>" "name="<?php echo $field->name;?>" value="<?php $value = Tewebcheck::gettvalue($field->name); echo $this->item->$value;?>" />
                <?php }   
                elseif (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
                {
                        ?> 
                          <?php 
                                if ($field->hidden):
                                    echo $field->input;
                                else:
                                ?>
                                <div class="control-group">
                                    <div class="control-label">
                                        <?php echo $field->label; ?>   
                                    </div>
                                    <div class="controls">     
                                        <?php echo $field->input; ?>
                                    </div>
                                </div>
                                <?php if ($field->name == 'jform[imagelrg]')
                                {?>
                                <div class="control-group">
                                    <div class="control-label">
                                        <label><?php echo JText::_('COM_PREACHIT_ADMIN_LRGIMAGE_PREVIEW');?></label>
                                    </div>
                                    <div class="controls"> 
                                        <?php echo $this->image; ?>
                                    </div> 
                                </div>
                                <?php } ?>
                                <?php
                                endif;
                           
                            ?>
                        <?php
                }
                }?>
            </div>
        </fieldset>
    </div>
    
<!-- Footer -->
<div style="text-align: center">
<?php $abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/lib/preachit.footer.php');?>
<?php echo PIfooter::footer();?></div>
<!-- /Footer -->
<input type="hidden" name="id" value="<?php echo $this->item->id; ?>" />
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="controller" value="study" />
<input type="hidden" name="task" value="" />
<?php if (!$user->authorise('core.edit.state', 'com_preachit') && !in_array(Tewebcheck::gettvalue('jform[published]'), $this->hide)) {?>
<input type="hidden" name="jform[published]" id="jform_published" value="<?php echo $this->item->published;?>" />
<?php }?>
<?php foreach ($this->validate AS $validate)
{
    if (in_array($validate, $this->hide))
    {?>
        <input type="hidden" name="jform[<?php echo $validate;?>]" id="jform_<?php echo $validate;?>" value="<?php echo $this->item->$validate;?>" />
    <?php }    
}?>
<div id="extrainputs" style="display: none;"></div>
<?php echo JHTML::_( 'form.token' ); ?>
</form>