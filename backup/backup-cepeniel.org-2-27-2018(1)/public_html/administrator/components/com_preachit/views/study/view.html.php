<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
class PreachitViewStudy extends JViewLegacy
{
    protected $form;

    protected $item;

    protected $state; 
    
function display($tpl = null)
{
JHTML::_( 'behavior.framework' );
JHTML::_('behavior.modal');
JRequest::setVar('hidemainmenu', 1);
//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();
 
$document = JFactory::getDocument();
if (Tewebcheck::getJversion() <= 2.5)
{
    $document->addScript($host.'media/lib_teweb/autocomplete/Autocompleter.js');
    $document->addScript($host.'media/lib_teweb/autocomplete/Autocompleter.Local.js');
    $document->addScript($host.'media/lib_teweb/autocomplete/Observer.js');
    $document->addStyleSheet($host.'media/lib_teweb/autocomplete/Autocompleter.css');
}
$document->addStyleSheet('../media/preachit/css/preachit.css');
$db = JFactory::getDBO();

// get Joomla version to decide which form and method

$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/additional.php');
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/scripture.php');
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/messageimage.php');
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/forms.php');
jimport('teweb.details.standard');

$user	= JFactory::getUser();
$this->assignRef('user', $user);

// get the Form
$this->form = $this->get('Form');
$this->item = $this->get('Item');
// Check for errors.
if (count($errors = $this->get('Errors'))) {
    JError::raiseError(500, implode("\n", $errors));
    return false;
}

// hide elements if chosen in admin

$hide = PIHelperforms::hidemessageedit();
$this->assignRef('hide', $hide);
$validate = PIHelperforms::validatemessageedit();
$this->assignRef('validate', $validate);

if ($this->item->id > 0)
{
if (!$user->authorise('core.edit', 'com_preachit') && !$user->authorise('core.edit.own', 'com_preachit')) 
{Tewebcheck::check403();}
elseif (!$user->authorise('core.edit', 'com_preachit') && $user->id != $row->user)
{Tewebcheck::check403();}
}	
else 
{
if (!$user->authorise('core.create', 'com_preachit')) 
{Tewebcheck::check403();}
}

if (Tewebcheck::getJversion() >= 3.0)
{
    $this->setLayout('form30');
}
else {
    $this->setLayout('form16');
}

//get preview images
$image = PIHelpermimage::messageimage($this->item->id, 0, '', 0, 'large');
$this->assignRef('image', $image);

// get tag list for auto complete
jimport ('teweb.details.tags');
$tags = Tewebtags::gettags('#__pistudies', 'ASC', 'name');
$taglist = null;
$i = 0;
foreach ($tags AS $tag)
{
    if ($i != 0)
    {$comma = ',';} else {$comma = null;}
    $taglist .= $comma."'".addslashes($tag->name)."'";
    $i++;
}
$this->assignRef('taglist', $taglist);
$this->addToolbar();
parent::display($tpl);
}

/**
     * Add the page title and toolbar.
     *
     * @since    1.6
     */
protected function addToolbar()
{
    $user    = JFactory::getUser();   
    if ($this->item->id > 0)
    {
    JToolBarHelper::title( JText::_( 'COM_PREACHIT_ADMIN_MESSAGES_EDIT' ), 'message.png' );
    }
    else
    {
    JToolBarHelper::title( JText::_( 'COM_PREACHIT_ADMIN_MESSAGES_DETAILS' ), 'message.png' );
    }
    JToolBarHelper::apply('study.apply', 'JTOOLBAR_APPLY');
    JToolBarHelper::save('study.save', 'JTOOLBAR_SAVE');
    JToolBarHelper::custom('study.save2new', 'save-new.png', 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);
    JToolBarHelper::divider();
    if ($user->authorise('core.admin', 'com_preachit'))  {
    JToolBarHelper::preferences('com_preachit', '550', '900');
    }
    if ($this->item->id > 0)
    {
    JToolBarHelper::cancel( 'study.cancel', 'JTOOLBAR_CLOSE' );
    }
    else
    {
    JToolBarHelper::cancel('study.cancel','JTOOLBAR_CANCEL');
    }
    JToolBarHelper::divider();
    JToolBarHelper::help('pihelp', 'com_preachit');
}

}