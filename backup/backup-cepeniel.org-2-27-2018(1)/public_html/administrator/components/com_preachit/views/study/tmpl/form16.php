<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/tooltips.php');
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/additional.php');
JHTML::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
JText::script('COM_PREACHIT_FIELDS_INVALID');
$document = JFactory::getDocument();      
$document->addScript(JURI::root() . 'media/preachit/js/pisubmitbutton.js');
$document->addScript(JURI::root() . 'media/preachit/js/pistudyinsert.js');
$user	= JFactory::getUser();
$option = JRequest::getCmd('option');
jimport('joomla.html.pane');
$pane = JPane::getInstance( 'tabs' );
?>
<script language="javascript" type="text/javascript">
function addrecord(record) {
	SqueezeBox.setContent( 'iframe', 'index.php?option=com_preachit&view=addrecord&tmpl=component&record='+record );
}

document.addEvent('domready', function() {
 
    // Test source, list of tags from http://del.icio.us/tag/
    var tokens = [<?php echo $this->taglist;?>];
 
    // Our instance for the element with id "demo-local"
    if (typeof Autocompleter != 'undefined' && document.getElementById('jform_tags'))
    {
        new Autocompleter.Local('jform_tags', tokens, {
            'minLength': 1, // We need at least 1 character
            'selectMode': 'type-ahead', // Instant completion
            'multiple': true // Tag support, by default comma separated
        });
    }
 
});

function setDisabled(filter, setting)
{
    if (filter)
    {filter.disabled = setting;} 
    return
}

function setDisabledall(setting)
{
    var filter0 = document.getElementById('jform_audio_folder');
    var filter1 = document.getElementById('jform_video_folder');
    var filter2 = document.getElementById('jform_downloadvid_folder');
    var filter3 = document.getElementById('jform_notes_folder');
    var filter4 = document.getElementById('jform_slides_folder');
    var filter5 = document.getElementById('jform_image_folderlrg');
    var filter6 = document.getElementById('jform_vclip1_folder');
    var filter7 = document.getElementById('jform_vclip2_folder');
    var filter8 = document.getElementById('jform_vclip3_folder');
    setDisabled(filter0, setting);
    setDisabled(filter1, setting);
    setDisabled(filter2, setting);
    setDisabled(filter3, setting);
    setDisabled(filter4, setting);
    setDisabled(filter5, setting);
    setDisabled(filter6, setting);
    setDisabled(filter7, setting);
    setDisabled(filter8, setting);
}

window.addEvent('domready', function(){
                setDisabledall(true);
            })

function tefadeelement(id, input, fade)
{
    var container = $(id);
    if (input != '')
    {input.value = '';}
    container.set('tween',{'duration' : 500}).fade(fade);
    if (fade == 'out')
    {
    (function() {
    {container.set('class', 'tehidden');}
    }).delay(500);
    }
    else {container.set('class', '');}
    
    return false;
}

</script>
<style>
.tehidden, .tehidden .control-label, .tehidden .controls, .tehidden input, .tehidden label, .tehidden button {
    height: 0 !important;
    margin: 0 !important;   
    padding: 0 !important; 
}
</style>
<form class="form-validate" action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
<?php
echo $pane->startPane( 'content-pane' );
echo $pane->startPanel( JText::_( 'COM_PREACHIT_ADMIN_MESSAGES_LEGEND' ), 'MAIN' );
?>
<div class="width-100">
<fieldset class="panelform ">
<ul class="adminformlist">
<?php
foreach ($this->form->getFieldset("maininfo") as $field) {
if (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
{ 
        ?>
          <?php 
                if ($field->hidden)
                    {echo $field->input;}
                elseif (!$user->authorise('core.edit.state', 'com_preachit') && $field->name == 'jform[published]')
                            {continue;}
                else {
                ?>
                <li>
                    <?php echo $field->label; ?>       
                    <?php if ($field->name != 'jform[dur_hrs]') {echo $field->input;} ?>
                    <?php if ($field->name == 'jform[study_book]')
                    {?>
                    <input type="text" size="7" readonly="readonly" value="<?php echo JText::_('COM_PREACHIT_ADMIN_CH_BEG');?>" style="border: 0pt none; font-size: 10px;"><?php echo $this->form->getInput('ref_ch_beg'); ?>
                    <input type="text" size="7" readonly="readonly" value="<?php echo JText::_('COM_PREACHIT_ADMIN_VS_BEG');?>" style="border: 0pt none; font-size: 10px;"><?php echo $this->form->getInput('ref_vs_beg'); ?>
                    <input type="text" size="7" readonly="readonly" value="<?php echo JText::_('COM_PREACHIT_ADMIN_CH_END');?>" style="border: 0pt none; font-size: 10px;"><?php echo $this->form->getInput('ref_ch_end'); ?>
                    <input type="text" size="7" readonly="readonly" value="<?php echo JText::_('COM_PREACHIT_ADMIN_VS_END');?>" style="border: 0pt none; font-size: 10px;"><?php echo $this->form->getInput('ref_vs_end'); ?> <?php } ?>
                    <?php if ($field->name == 'jform[study_book2]')
                    {?>
                    <input type="text" size="7" readonly="readonly" value="<?php echo JText::_('COM_PREACHIT_ADMIN_CH_BEG');?>" style="border: 0pt none; font-size: 10px;"><?php echo $this->form->getInput('ref_ch_beg2'); ?>
                    <input type="text" size="7" readonly="readonly" value="<?php echo JText::_('COM_PREACHIT_ADMIN_VS_BEG');?>" style="border: 0pt none; font-size: 10px;"><?php echo $this->form->getInput('ref_vs_beg2'); ?>
                    <input type="text" size="7" readonly="readonly" value="<?php echo JText::_('COM_PREACHIT_ADMIN_CH_END');?>" style="border: 0pt none; font-size: 10px;"><?php echo $this->form->getInput('ref_ch_end2'); ?>
                    <input type="text" size="7" readonly="readonly" value="<?php echo JText::_('COM_PREACHIT_ADMIN_VS_END');?>" style="border: 0pt none; font-size: 10px;"><?php echo $this->form->getInput('ref_vs_end2'); ?> <?php } ?>
                    <?php if ($field->name == 'jform[dur_hrs]')
                    {?>
                    <input type="text" size="3" readonly="readonly" value="<?php echo JText::_('COM_PREACHIT_ADMIN_HRS');?>" style="border: 0pt none; font-size: 10px;">
                    <?php echo $this->form->getInput('dur_hrs'); ?>
                    <input type="text" size="3" readonly="readonly" value="<?php echo JText::_('COM_PREACHIT_ADMIN_MINS');?>" style="border: 0pt none; font-size: 10px;">
                    <?php echo $this->form->getInput('dur_mins'); ?>
                    <input type="text" size="3" readonly="readonly" value="<?php echo JText::_('COM_PREACHIT_ADMIN_SECS');?>" style="border: 0pt none; font-size: 10px;">
                    <?php echo $this->form->getInput('dur_secs'); ?><?php } ?>
                    <?php if ($field->name == 'jform[teacher][]' || $field->name == 'jform[series]' || $field->name == 'jform[ministry][]')
                    {?><button type="button" style="margin-top: 0; margin-left: 10px;" onclick="tefadeelement('pinew_<?php echo str_replace('jform_','',$field->id);?>', '', 'in'); return false;">
                     <?php echo JText::_('COM_PREACHIT_ADD_NEW_RECORD');?> </button><?php } ?>
                </li>
                <?php if ($field->name == 'jform[teacher][]' || $field->name == 'jform[series]' || $field->name == 'jform[ministry][]')
                {   $name = str_replace('jform_','',$field->id);
                    $groupid = 'pinew_'. $name;
                    ?>
                        <li class="tehidden" id="<?php echo $groupid;?>" style="opacity: 0; visibility: hidden;">
                            <?php echo $this->form->getLabel('new'.$name); ?>
                            <?php echo $this->form->getInput('new'.$name); ?>
                            <button type="button" onclick="tefadeelement('<?php echo $groupid;?>', 'jform_new<?php echo $name;?>', 'out'); return false;"><?php echo JText::_('JCANCEL') ?></button>
                        </li>
                <?php } ?>
                <?php if ($field->name == 'jform[tags]')
                    {?><li><label></label>
                    <input type="text" size="100%" readonly="readonly" value="<?php echo JText::_('COM_PREACHIT_ADMIN_TAG_HELP');?>" 
                        style="border: 0pt none; font-size: 10px; font-style: italic;">
                        </li><?php } ?>
                <?php if ($field->name == 'jform[hits]')
                    {?><button type="button" style="margin-top: 0;" onclick="Joomla.submitbutton('study.reset')">
                        <?php echo JText::_('COM_PREACHIT_ADMIN_BUTTON_RESET');?> </button><?php } ?>
                <?php
                }
           
            ?>
        <?php }
}?>
</ul>
</fieldset>
</div>
<?php
echo $pane->endPanel();
if (Tewebcheck::showtab($this->form, 'displayoptions', $this->hide) || Tewebcheck::showtab($this->form, 'puboptions', $this->hide) || Tewebcheck::showtab($this->form, 'podoptions', $this->hide) || Tewebcheck::showtab($this->form, 'metaoptions', $this->hide))
{
echo $pane->startPanel( JText::_( 'COM_PREACHIT_ADMIN_LINKHEAD' ), 'LINKS' );
?>
<div class="width-100">
<fieldset class="panelform">
<?php
if ($user->authorise('core.edit.state', 'com_preachit') && Tewebcheck::showtab($this->form, 'puboptions', $this->hide)) { ?>
<h2><?php echo JText::_('COM_PREACHIT_SUB_PUBLISH_OPTIONS');?></h2>
<ul class="adminformlist">
<?php
foreach ($this->form->getFieldset("puboptions") as $field):
if (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
{
        ?>
          <?php 
                if ($field->hidden):
                    echo $field->input;
                else:
                ?>
                <li>
                    <?php echo $field->label; ?>       
                    <?php echo $field->input; ?>
                </li>
                <?php
                endif;
           
            ?>
        <?php   }
        endforeach;?>
</ul>
<?php } ?>
<?php if (Tewebcheck::showtab($this->form, 'podoptions', $this->hide))
{ ?>
<div class="clr"></div>
<h2><?php echo JText::_('COM_PREACHIT_SUB_PODPUBLISH_OPTIONS');?></h2>
<ul class="adminformlist">
<?php
foreach ($this->form->getFieldset("podoptions") as $field):
if (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
{
        ?>
          <?php 
                if ($field->hidden):
                    echo $field->input;
                else:
                ?>
                <li>
                    <?php echo $field->label; ?>       
                    <?php echo $field->input; ?>
                </li>
                <?php
                endif;
           
            ?>
        <?php   }
        endforeach;?>
</ul>
<?php } ?>
<?php if (Tewebcheck::showtab($this->form, 'displayoptions', $this->hide))
{ ?>
<div class="clr"></div>
<h2><?php echo JText::_('COM_PREACHIT_SUB_DISPLAY_OPTIONS');?></h2>
<ul class="adminformlist">
<?php
foreach ($this->form->getFieldset("displayoptions") as $field):
if (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
{
        ?>
          <?php 
                if ($field->hidden):
                    echo $field->input;
                else:
                ?>
                <li>
                    <?php echo $field->label; ?>       
                    <?php echo $field->input; ?>
                </li>
                <?php
                endif;
           
            ?>
        <?php }
        endforeach;?>
</ul>
<?php } ?>
<?php if (Tewebcheck::showtab($this->form, 'metaoptions', $this->hide))
{ ?>
<div class="clr"></div>
<h2><?php echo JText::_('JGLOBAL_FIELDSET_METADATA_OPTIONS');?></h2>
<ul class="adminformlist">
<?php
foreach ($this->form->getFieldset("metaoptions") as $field):
if (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
{
        ?>
          <?php 
                if ($field->hidden):
                    echo $field->input;
                else:
                ?>
                <li>
                    <?php echo $field->label; ?>       
                    <?php echo $field->input; ?>
                </li>
                <?php 
                endif;
           
            ?>
        <?php }
        endforeach;?>
</ul>
<?php } ?>
</fieldset>
</div>
<?php
echo $pane->endPanel();
}
if (Tewebcheck::showtab($this->form, 'audtab', $this->hide) || Tewebcheck::showtab($this->form, 'audpurtab', $this->hide))
{
echo $pane->startPanel( JText::_( 'COM_PREACHIT_ADMIN_AUDIOHEAD' ), 'AUDIO' );
?>
<div class="width-100">
<fieldset class="panelform">
<ul class="adminformlist">
<?php if (Tewebcheck::showtab($this->form, 'audtab', $this->hide))
{ ?>
<li id="pimform_afile_head"><label class="pitablesubhead"><?php echo JText::_('COM_PREACHIT_ADMIN_SUB_FILE');?></label></li>
<?php
foreach ($this->form->getFieldset("audtab") as $field) {
if ($field->name == 'jform[audio_folder]' && in_array('audio_folder', $this->hide) && !in_array('audio_link', $this->hide)) 
{?>
    <input type="hidden" id="<?php echo $field->id;?>" "name="<?php echo $field->name;?>" value="<?php $value = Tewebcheck::gettvalue($field->name); echo $this->item->$value;?>" />
<?php }   
elseif (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
{
        ?> 
          <?php 
                if ($field->hidden):
                    echo $field->input;
                else:
                ?>
                <li>
                    <?php echo $field->label; ?>  
                    <?php echo $field->input; ?>
                    <?php if ($field->name == 'jform[audiofs]')
                    {echo PIHelpertooltips::filesize(0);}?>
                </li>
                <?php
                endif;
           
            ?>
        <?php
}
}
}?>
<?php if (Tewebcheck::showtab($this->form, 'audpurtab', $this->hide))
{?>
<li id="pimform_apur_head"><label class="pitablesubhead"><?php echo JText::_('COM_PREACHIT_ADMIN_SUB_PUR');?></label</li>
<?php
foreach ($this->form->getFieldset("audpurtab") as $field) {
if (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
{
        ?> 
          <?php 
                if ($field->hidden):
                    echo $field->input;
                else:
                ?>
                <li>
                    <?php echo $field->label; ?>  
                    <?php echo $field->input; ?>
                </li>
                <?php
                endif;
           
            ?>
        <?php
}
}
}?>
</ul>
</fieldset></div>
<?php
echo $pane->endPanel();
}
if (Tewebcheck::showtab($this->form, 'vidtab', $this->hide) || Tewebcheck::showtab($this->form, 'addvidtab', $this->hide) || Tewebcheck::showtab($this->form, 'vidpurtab', $this->hide))
{
echo $pane->startPanel( JText::_( 'COM_PREACHIT_ADMIN_VIDEOHEAD' ), 'VIDEO' );
?>
<div class="width-100">
<fieldset class="panelform">
<ul class="adminformlist">
<?php if (Tewebcheck::showtab($this->form, 'vidtab', $this->hide))
{?>
<li id="pimform_vfile_head"><label class="pitablesubhead"><?php echo JText::_('COM_PREACHIT_ADMIN_SUB_FILE');?></label></li>
<?php
foreach ($this->form->getFieldset("vidtab") as $field) {
if ($field->name == 'jform[video_folder]' && in_array('video_folder', $this->hide) && !in_array('video_link', $this->hide)) 
{?>
    <input type="hidden" id="<?php echo $field->id;?>" "name="<?php echo $field->name;?>" value="<?php $value = Tewebcheck::gettvalue($field->name); echo $this->item->$value;?>" />
<?php }   
elseif (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
{
        ?> 
          <?php 
                if ($field->hidden):
                    echo $field->input;
                else:
                ?>
                <li>
                    <?php echo $field->label; ?>   
                    <?php echo $field->input; ?>
                    <?php if ($field->name == 'jform[videofs]')
                    {echo PIHelpertooltips::filesize(0);}?>
                </li>
                <?php
                endif;
           
            ?>
        <?php
}
}
}?>
<?php if (Tewebcheck::showtab($this->form, 'addvidtab', $this->hide))
{?>
<li id="pimform_dvidfile_head"><label class="pitablesubhead"><?php echo JText::_('COM_PREACHIT_ADMIN_SUB_VIDEO_ADDFILE');?></label></li>
<?php
foreach ($this->form->getFieldset("addvidtab") as $field) {
if ($field->name == 'jform[downloadvid_folder]' && in_array('downloadvid_folder', $this->hide) && !in_array('downloadvid_link', $this->hide)) 
{?>
    <input type="hidden" id="<?php echo $field->id;?>" "name="<?php echo $field->name;?>" value="<?php $value = Tewebcheck::gettvalue($field->name); echo $this->item->$value;?>" />
<?php }   
elseif (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
{
        ?> 
          <?php 
                if ($field->hidden):
                    echo $field->input;
                else:
                ?>
                <li>
                    <?php echo $field->label; ?>   
                    <?php echo $field->input; ?>
                    <?php if ($field->name == 'jform[advideofs]')
                    {echo PIHelpertooltips::filesize(0);}?>
                </li>
                <?php
                endif;       
            ?>
        <?php
}
}
}?>
<?php if (Tewebcheck::showtab($this->form, 'vidpurtab', $this->hide))
{?>
<li id="pimform_vpur_head"><label class="pitablesubhead"><?php echo JText::_('COM_PREACHIT_ADMIN_SUB_PUR');?></label></li>
<?php
foreach ($this->form->getFieldset("vidpurtab") as $field) {
if (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
{
        ?> 
          <?php 
                if ($field->hidden):
                    echo $field->input;
                else:
                ?>
                <li>
                    <?php echo $field->label; ?>   
                    <?php echo $field->input; ?>
                </li>
                <?php
                endif;
           
            ?>
        <?php
}
}
}?>
<?php if (Tewebcheck::showtab($this->form, 'vcliptab', $this->hide))
{?>
<li id="pimform_vpur_head"><label class="pitablesubhead"><?php echo JText::_('COM_PREACHIT_ADMIN_SUB_VCLIP');?></label></li>
<?php
foreach ($this->form->getFieldset("vcliptab") as $field) {
if (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
{
        ?> 
          <?php 
                if ($field->hidden):
                    echo $field->input;
                else:
                ?>
                <li>
                    <?php echo $field->label; ?>   
                    <?php echo $field->input; ?>
                </li>
                <?php
                endif;
           
            ?>
        <?php
}
}
}?>
</ul>
</fieldset></div>
<?php
echo $pane->endPanel();
}
if (!in_array('study_text', $this->hide))
{
echo $pane->startPanel( JText::_( 'COM_PREACHIT_ADMIN_TEXTHEAD' ), 'STUDYTEXT' );
?>
<div class="width-100">
<fieldset class="panelform">
<?php echo $this->form->getInput('study_text'); ?>
</fieldset></div>
<?php
echo $pane->endPanel();
}
if (Tewebcheck::showtab($this->form, 'notestab', $this->hide))
{
echo $pane->startPanel( JText::_( 'COM_PREACHIT_ADMIN_NOTESHEAD' ), 'MESSAGENOTES' );
?>
<div class="width-100">
<fieldset class="panelform">
<ul class="adminformlist">
<?php
foreach ($this->form->getFieldset("notestab") as $field) {
if ($field->name == 'jform[notes_folder]' && in_array('notes_folder', $this->hide) && !in_array('notes_link', $this->hide)) 
{?>
    <input type="hidden" id="<?php echo $field->id;?>" "name="<?php echo $field->name;?>" value="<?php $value = Tewebcheck::gettvalue($field->name); echo $this->item->$value;?>" />
<?php }   
elseif (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
{
        ?> 
          <?php 
                if ($field->hidden):
                    echo $field->input;
                else:
                ?>
                <li>
                    <?php echo $field->label; ?> 
                    <?php echo $field->input; ?>
                    <?php if ($field->name == 'jform[notesfs]')
                    {echo PIHelpertooltips::filesize(0);}?>
                </li>
                <?php
                endif;
           
            ?>
        <?php
}
}?>
</ul>
</fieldset>
</div>
<?php
echo $pane->endPanel();
}
if (Tewebcheck::showtab($this->form, 'slidestab', $this->hide))
{
echo $pane->startPanel( JText::_( 'COM_PREACHIT_ADMIN_SLIDESHEAD' ), 'MESSAGESLIDES' );
?>
<div class="width-100">
<fieldset class="panelform">
<ul class="adminformlist">
<?php
foreach ($this->form->getFieldset("slidestab") as $field) {
if ($field->name == 'jform[slides_folder]' && in_array('slides_folder', $this->hide) && !in_array('slides_link', $this->hide)) 
{?>
    <input type="hidden" id="<?php echo $field->id;?>" "name="<?php echo $field->name;?>" value="<?php $value = Tewebcheck::gettvalue($field->name); echo $this->item->$value;?>" />
<?php }   
elseif (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
{
        ?> 
          <?php 
                if ($field->hidden):
                    echo $field->input;
                else:
                ?>
                <li>
                    <?php echo $field->label; ?> 
                    <?php echo $field->input; ?>
                    <?php if ($field->name == 'jform[slidesfs]')
                    {echo PIHelpertooltips::filesize(0);}?>
                </li>
                <?php
                endif;
           
            ?>
        <?php
}
}?>
</ul>
</fieldset>
</div>
<?php
echo $pane->endPanel();
}
if (Tewebcheck::showtab($this->form, 'imagetab', $this->hide))
{
echo $pane->startPanel( JText::_( 'COM_PREACHIT_ADMIN_IMAGEHEAD' ), 'MESIMAGES' );
?>
<div class="width-100">
<fieldset class="panelform">
<ul class="adminformlist">
<?php
foreach ($this->form->getFieldset("imagetab") as $field) {
if ($field->name == 'jform[image_folderlrg]' && in_array('image_folderlrg', $this->hide) && !in_array('imagelrg', $this->hide)) 
{?>
    <input type="hidden" id="<?php echo $field->id;?>" "name="<?php echo $field->name;?>" value="<?php $value = Tewebcheck::gettvalue($field->name); echo $this->item->$value;?>" />
<?php }   
elseif (!in_array(Tewebcheck::gettvalue($field->name), $this->hide))
{
        ?> 
          <?php 
                if ($field->hidden):
                    echo $field->input;
                else:
                ?>
                <li>
                    <?php echo $field->label; ?>  
                    <?php echo $field->input; ?>
                </li>
                <?php if ($field->name == 'jform[imagelrg]')
                    {?>
                    <li>
                        <label>
                            <?php echo JText::_('COM_PREACHIT_ADMIN_LRGIMAGE_PREVIEW');?>
                        </label>
                            <?php echo $this->image; ?>
                    </li> <?php } ?>
                <?php
                endif;
           
            ?>
        <?php
}
}?>
</ul>
</fieldset></div>
<?php
echo $pane->endPanel();
}
echo $pane->endPane();
?>
<!-- Footer -->
<div style="text-align: center">
<?php $abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/lib/preachit.footer.php');?>
<?php echo PIfooter::footer();?></div>
<!-- /Footer -->
<input type="hidden" name="id" value="<?php echo $this->item->id; ?>" />
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="controller" value="study" />
<input type="hidden" name="task" value="" />
<?php if (!$user->authorise('core.edit.state', 'com_preachit') && !in_array(Tewebcheck::gettvalue('jform[published]'), $this->hide)) {?>
<input type="hidden" name="jform[published]" id="jform_published" value="<?php echo $this->item->published;?>" />
<?php }?>
<?php foreach ($this->validate AS $validate)
{
    if (in_array($validate, $this->hide))
    {?>
        <input type="hidden" name="jform[<?php echo $validate;?>]" id="jform_<?php echo $validate;?>" value="<?php echo $this->item->$validate;?>" />
    <?php }  
}?>
<div id="extrainputs" style="display: none;"></div>
<?php echo JHTML::_( 'form.token' ); ?>
</form>