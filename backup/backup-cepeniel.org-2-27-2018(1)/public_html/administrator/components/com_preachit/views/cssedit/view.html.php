<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
class PreachitViewCSSedit extends JViewLegacy
{
function display($tpl = null)
{
JRequest::setVar('hidemainmenu', 1);
$app = JFactory::getApplication();
$option = JRequest::getCmd('option');
$document = JFactory::getDocument();
$document->addStyleSheet('../media/preachit/css/preachit.css');
$user    = JFactory::getUser();
if (!$user->authorise('core.admin', 'com_preachit'))
{Tewebcheck::check403();}

//get filename for save process

$temp = JRequest::getVar('template', '');
$cssfile = JRequest::getVar('file', '');
$mod = JRequest::getVar('module', '');
$plug = JRequest::getVar('plugin', '');
$override = JRequest::getVar('override', '');

// get the data

$form = $this->get('form');
// get the Data
$source = $this->get('data');
$data['csssource'] = $source->filecontent;
// Bind the Data
$form->bind($data);    
$this->form = $form;

$this->assignRef('temp', $temp);
$this->assignRef('cssfile', $cssfile);
$this->assignRef('override', $override);
$this->assignRef('mod', $mod);
$this->assignRef('plug', $plug);
$this->addToolbar();
parent::display($tpl);
}

/**
     * Add the page title and toolbar.
     *
     * @since    1.6
     */
protected function addToolbar()
{
$user = JFactory::getUser();    
JToolBarHelper::title( JText::_( 'COM_PREACHIT_ADMIN_CSS_TITLE' ), 'css.png');
JToolBarHelper::apply('cssedit.apply', 'JTOOLBAR_APPLY');
JToolBarHelper::save('cssedit.save', 'JTOOLBAR_SAVE');
JToolBarHelper::divider();
$user    = JFactory::getUser();
if ($user->authorise('core.admin', 'com_preachit'))  {
JToolBarHelper::preferences('com_preachit', '550', '900');
}
JToolBarHelper::cancel( 'cssedit.cancel', 'JTOOLBAR_CLOSE' );
JToolBarHelper::divider();
JToolBarHelper::help('pihelp', 'com_preachit');    
}
}