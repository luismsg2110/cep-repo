<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$option = JRequest::getCmd('option');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
?>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<fieldset>
    <legend><?php echo JText::_( 'COM_PREACHIT_ADMIN_CSS_LEGEND' ); ?></legend>
    <?php echo $this->form->getLabel('csssource'); ?>
    <div class="clr"></div>
    <div class="editor-border">
    <?php echo $this->form->getInput('csssource'); ?>
    </div>
</fieldset>
<!-- /Footer -->
<?php echo JHTML::_( 'form.token' ); ?>
<input type="hidden" name="option" value="<?php echo $option;?>" />
<input type="hidden" name="controller" value="cssedit" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="override" value="<?php echo $this->override; ?>" />
<input type="hidden" name="cssfile" value="<?php echo $this->cssfile; ?>" />
<input type="hidden" name="temp" value="<?php echo $this->temp; ?>" />
<input type="hidden" name="mod" value="<?php echo $this->mod; ?>" />
<input type="hidden" name="plug" value="<?php echo $this->plug; ?>" />
<div class="clearfix"> </div>
<div style="text-align: center; padding-top: 5px;">
<?php $abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/lib/preachit.footer.php');?>
<?php echo PIfooter::footer();?></div>
</form>