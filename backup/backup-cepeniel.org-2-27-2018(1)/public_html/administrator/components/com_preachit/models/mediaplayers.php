<?php
/**
 * @Component - Preachit
 * @version 1.0.0 May, 2010
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha, LLC
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 *
 *
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.modellist');
class PreachitModelMediaplayers extends JModelList
{
var $_data = null;
var $_pagination = null;
var $_total = null;
var $_search = null;
var $_query = null;

function __construct($config = array())
{
    if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'id', 'id',
                'playername', 'playername',
                'playertype', 'playertype',
                'published', 'published'
            );
        }
        parent::__construct($config);
}

function _buildQuery()
	{
		$where		= $this->_buildContentWhere();
		$orderby	= $this->_buildContentOrderBy();
		$query = "SELECT * FROM #__pimediaplayers" 
			. $where
			. $orderby
			;
		return $query;
	}

function getData() 
  {
        // if data hasn't already been obtained, load it
        if (empty($this->_data)) 
{
            $query = $this->_buildQuery();
            $this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit')); 
        }
        return $this->_data;
  }

function getTotal()
  {
        // Load the content if it doesn't already exist
        if (empty($this->_total)) {
            $query = $this->_buildQuery();
            $this->_total = $this->_getListCount($query);    
        }
        return $this->_total;
  }
  
  function _buildContentWhere()
	{

		$where = array();

		// Filter by published state
        $published = $this->getState('filter.state');
        if (is_numeric($published)) {
            $where[] = " #__pimediaplayers.published =".(int) $published;
        } elseif ($published === '') {
            $where[] = " #__pimediaplayers.published IN (0, 1)";
        }
		$where 		= ( count( $where ) ? ' WHERE '. implode( ' AND ', $where ) : '' );
		
		return $where;
	}
	function _buildContentOrderBy()
	{
		$filter_order    = $this->state->get('list.ordering', 'ordering');
        $filter_order_Dir    = $this->state->get('list.direction', 'ASC');
		
		$orderby = ' ORDER BY '.$filter_order.' '.$filter_order_Dir;
		
		return $orderby;
	}
       /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @since    1.6
     */
    protected function populateState($ordering = null, $direction = null)
    {
        $app = JFactory::getApplication('administrator');

        // Load the filter state.
        $search = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
        $this->setState('filter.search', $search);

        $state = $this->getUserStateFromRequest($this->context.'.filter.statemp', 'filter_statemp', '', 'string');
        $this->setState('filter.state', $state);

        // Load the parameters.
        $params = JComponentHelper::getParams('com_preachit');
        $this->setState('params', $params);
        
        // set the pagination variables
        $limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'), 'int');
        $limitstart = $app->getUserStateFromRequest( $this->context.'&view=mediaplayers.limitstart', 'limitstart', 0);
        $this->setState('limit', $limit);
        $this->setState('limitstart', $limitstart);

        // List state information.
        parent::populateState('playername', 'asc');
    }
}