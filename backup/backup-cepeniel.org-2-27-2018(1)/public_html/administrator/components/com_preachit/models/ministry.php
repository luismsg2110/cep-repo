<?php
/**
 * @Component - Preachit
 * @version 1.0.0 May, 2010
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha, LLC
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 *
 *
 */
// no direct access
defined('_JEXEC') or die;
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/admin.php');
jimport('joomla.application.component.modeladmin');

class PreachitModelMinistry extends JModelAdmin
{
	
	public function getForm($data = array(), $loadData = true) 
        {
        			jimport('joomla.form.form');
        			$abspath    = JPATH_SITE;
					$models_path = $abspath.DIRECTORY_SEPARATOR.'administrator'.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_preachit'.DIRECTORY_SEPARATOR.'models';
 
                JForm::addFormPath ($models_path . DIRECTORY_SEPARATOR . 'forms');
                JForm::addFieldPath($models_path . DIRECTORY_SEPARATOR . 'fields');
 
                $form = $this->loadForm('com_preachit.ministryedit', 'ministryedit', array('control' => 'jform', 'load_data' => $loadData));
                return $form;
        }
        
   
	/**
     * Method to get the data that should be injected in the form.
     *
     * @return  mixed  The data for the form.
     *
     * @since   1.6
     */
    protected function loadFormData()
    {
        // Check the session for previously entered form data.
        $app  = JFactory::getApplication();
        $data = $app->getUserState('com_preachit.edit.ministry.data', array());

        if (empty($data))
        {
            $data = $this->getItem();
        }

        return $data;
    }
    
    /**
     * Method to save the form data.
     *
     * @param    array    The form data.
     *
     * @return    boolean    True on success.
     * @since    1.6
     */
    public function save($data)
    {
        
        $data['ministry_alias'] = Tewebadmin::uniquealias('#__piministry',$data['ministry_alias'], $data['id'], 'ministry_alias');
        // change folder entries if only default allowed
        $params = Tewebdetails::getparams('com_preachit');
        if ($params->get('default_folder_only', 0) == 1)
        {
            if (isset($data['image_folderlrg']) && $data['image_folderlrg'] != '')
            {$data['image_folderlrg'] =  $params->get('image_folder', 0);}
        }        
        
        if (!isset($data['language']) || $data['language'] == '')
        {
            $data['language'] = '*';
        }
        
        $session = JFactory::getSession();
        // set image change session variable
        if (Tewebadmin::entrychanged('ministry_image_lrg', '#__piministry', $data) || Tewebadmin::entrychanged('image_folderlrg', '#__piministry', $data))
        {
            $session->set('minimagechange', true);
        }
        else {$session->set('minimagechange', false);}

        if (parent::save($data)) {

            return true;
        }


        return false;
    }
    
    /**
     * Method to delete one or more records.
     *
     * @param   array  &$pks  An array of record primary keys.
     *
     * @return  boolean  True if successful, false if an error occurs.
     *
     * @since   11.1
     */
    public function delete(&$pks)
    {
        $abspath    = JPATH_SITE;
        $app = JFactory::getApplication ();
        $db= JFactory::getDBO(); 
            require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/ministryimage.php');
            require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/message-info.php');
            jimport('joomla.filesystem.file');
            $pks = (array) $pks;
            foreach ($pks AS $key => $pk)
            {
                $table = $this->getTable();
                if ($table->load($pk))
                {
                     // see if any messages are associated with this series
                    $messageno = PIHelpermessageinfo::ministrycount($pk);
                    $seriesno = PIHelpermessageinfo::ministryseriescount($pk);
                    if ($messageno > 0 || $seriesno > 0)
                    {
                        // get series name
                        //get seriesname
                        $query = "
                        SELECT ".$db->quoteName('ministry_name')."
                        FROM ".$db->quoteName('#__piministry')."
                        WHERE ".$db->quoteName('id')." = ".$db->quote($pk).";
                        ";
                        $db->setQuery($query);
                        $name = $db->loadResult();
                        $app->enqueueMessage ( $name.' - '.JText::_('LIB_TEWEB_FAILED_DELETE_RECORDS_ATTACHED'), 'notice' );
                        unset($pks[$key]);
                        continue;
                    }
                    // set saccess to 0 in all relevent messages
                    $minaccess = PIHelperadmin::setminaccess($pk, 0); 
                    // remove ministry record from any relevant messages
                    $query='SELECT id, ministry FROM #__pistudies WHERE ministry REGEXP \'(.*:"'.$pk.'".*)\'';
                    $db->setQuery($query);
                    $messages = $db->loadObjectList();
                    if (is_array($messages))
                    {
                        foreach ($messages AS $mes)
                        {
                            $ministry = PIHelperadmin::removeJSONconnection($mes->ministry, $pk);
                            $db->setQuery ("UPDATE #__pistudies SET ministry = '".$ministry."' WHERE id = '{$mes->id}' ;"); 
                            $db->query();
                        }
                    }
                    // remove ministry record from any relevant series
                    $query='SELECT id, ministry FROM #__piseries WHERE ministry REGEXP \'(.*:"'.$pk.'".*)\'';
                    $db->setQuery($query);
                    $series = $db->loadObjectList();
                    if (is_array($series))
                    {
                        foreach ($series AS $ser)
                        {
                            $ministry = PIHelperadmin::removeJSONconnection($ser->ministry, $pk);
                            $db->setQuery ("UPDATE #__piseries SET ministry = '".$ministry."' WHERE id = '{$ser->id}' ;"); 
                            $db->query();
                        }
                    }
                    
                    
                    // delete resized images
                    $imagefile = array();
                    $imagefile[] = JPATH_ROOT.DIRECTORY_SEPARATOR.PIHelperminimage::getimagepath($pk, 'small', $table->ministry_image_lrg);
                    $imagefile[] = JPATH_ROOT.DIRECTORY_SEPARATOR.PIHelperminimage::getimagepath($pk, 'medium', $table->ministry_image_lrg);
                    $imagefile[] = JPATH_ROOT.DIRECTORY_SEPARATOR.PIHelperminimage::getimagepath($pk, 'large', $table->ministry_image_lrg);
                    foreach ($imagefile AS $im)
                    {
                        if (file_exists($im))
                        {
                            JFile::delete($im);
                        }
                    }     
                }
            }
        if (parent::delete($pks)) {
            
            return true;
        }
        return false;
            }
            
    /**
     * Method to toggle the featured setting of articles.
     *
     * @param   array    The ids of the items to toggle.
     * @param   integer  The value to toggle to.
     *
     * @return  boolean  True on success.
     */
    public function featured($pks, $value = 0)
    {
        // Sanitize the ids.
        $pks = (array) $pks;
        JArrayHelper::toInteger($pks);

        if (empty($pks))
        {
            $this->setError(JText::_('COM_PREACHIT_NO_ITEM_SELECTED'));
            return false;
        }

        try
        {
            $db = $this->getDbo();
            
            $db->setQuery(
                'UPDATE #__piministry' .
                    ' SET featured = ' . 0
            );
            $db->execute();

            $db->setQuery(
                'UPDATE #__piministry' .
                    ' SET featured = ' . (int) $value .
                    ' WHERE id = '.$pks[0]
            );
            $db->execute();
        }
        catch (Exception $e)
        {
            $this->setError($e->getMessage());
            return false;
        }

        $this->cleanCache();

        return true;
    }
}