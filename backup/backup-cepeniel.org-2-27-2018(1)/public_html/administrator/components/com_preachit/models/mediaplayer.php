<?php
/**
 * @Component - Preachit
 * @version 1.0.0 May, 2010
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha, LLC
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 *
 *
 */
// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

class PreachitModelMediaplayer extends JModelAdmin
{
	
	
        public function getForm($data = array(), $loadData = true) 
        {
        			jimport('joomla.form.form');
        			$abspath    = JPATH_SITE;
					$models_path = $abspath.DIRECTORY_SEPARATOR.'administrator'.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_preachit'.DIRECTORY_SEPARATOR.'models';
 
                JForm::addFormPath ($models_path . DIRECTORY_SEPARATOR . 'forms');
                JForm::addFieldPath($models_path . DIRECTORY_SEPARATOR . 'fields');
 
                $form = $this->loadForm('com_preachit.mediaplayersedit', 'mediaplayersedit', array('control' => 'jform', 'load_data' => $loadData));
                return $form;
        }
        
   
	/**
     * Method to get the data that should be injected in the form.
     *
     * @return  mixed  The data for the form.
     *
     * @since   1.6
     */
    protected function loadFormData()
    {
        // Check the session for previously entered form data.
        $app  = JFactory::getApplication();
        $data = $app->getUserState('com_preachit.edit.mediaplayer.data', array());

        if (empty($data))
        {
            $data = $this->getItem();
        }

        return $data;
    }
        
        /**
     * Method to get a table object, load it if necessary.
     *
     * @param   string  $name     The table name. Optional.
     * @param   string  $prefix   The class prefix. Optional.
     * @param   array   $options  Configuration array for model. Optional.
     *
     * @return  JTable  A JTable object
     *
     * @since   11.1
     */
    public function getTable($name = '', $prefix = 'Table', $options = array())
    {
        if (empty($name))
        {
            $name = $this->getName();
        }

        if ($table = $this->_createTable('mediaplayers', $prefix, $options))
        {
            return $table;
        }

        JError::raiseError(0, JText::sprintf('JLIB_APPLICATION_ERROR_TABLE_NAME_NOT_SUPPORTED', $name));

        return null;
    }
}