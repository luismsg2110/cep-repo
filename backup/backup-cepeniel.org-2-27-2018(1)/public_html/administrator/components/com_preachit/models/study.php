<?php
/**
 * @Component - Preachit
 * @version 1.0.0 May, 2010
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha, LLC
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 *
 *
 */
// no direct access
defined('_JEXEC') or die;
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/admin.php');
jimport('joomla.application.component.modeladmin');

class PreachitModelStudy extends JModelAdmin
{

        public function getForm($data = array(), $loadData = true) 
        {
        			jimport('joomla.form.form');
        			$abspath    = JPATH_SITE;
					$models_path = $abspath.DIRECTORY_SEPARATOR.'administrator'.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_preachit'.DIRECTORY_SEPARATOR.'models';
 
                JForm::addFormPath ($models_path . DIRECTORY_SEPARATOR . 'forms');
                JForm::addFieldPath($models_path . DIRECTORY_SEPARATOR . 'fields');
 
                $form = $this->loadForm('com_preachit.studyedit', 'studyedit', array('control' => 'jform', 'load_data' => $loadData));
                return $form;
        }
        
        /**
     * Method to get the data that should be injected in the form.
     *
     * @return  mixed  The data for the form.
     *
     * @since   1.6
     */
    protected function loadFormData()
    {
        // Check the session for previously entered form data.
        $app  = JFactory::getApplication();
        $data = $app->getUserState('com_preachit.edit.study.data', array());
        
        if (empty($data))
        {
            $data = $this->getItem();
            if (isset($data->teacher) && !is_array($data->teacher))
            {$registry = new JRegistry;
            $registry->loadString($data->teacher);
            $data->teacher = $registry->toArray();}
                                
            if (isset($data->ministry) && !is_array($data->ministry))
            {$registry = new JRegistry;
            $registry->loadString($data->ministry);
            $data->ministry = $registry->toArray();}
                        
            if (isset($data->publish_down))
            {
                if ($data->publish_down == '0000-00-00 00:00:00')
                {$data->publish_down = 'Never';}        
            }
            else {$data->publish_down = 'Never';}        
            if (isset($data->podpublish_down))
            {
                if ($data->podpublish_down == '0000-00-00 00:00:00')
                {$data->podpublish_down = 'Never';}        
            }
            else {$data->podpublish_down = 'Never';}  
        }

        return $data;
    }
    
    /**
     * Method to get a single record.
     *
     * @param   integer  $pk  The id of the primary key.
     *
     * @return  mixed    Object on success, false on failure.
     *
     * @since   11.1
     */
    public function getItem($pk = null)
    {
        $item = parent::getItem();
        if ($item->id >! 0 || !$item->id)
        {
            $auto = Tewebdetails::getparams('com_preachit'); 
            $item->series = $auto->get('series', 0);
            $item->ministry = $auto->get('ministry', array());
            $item->audio_type = $auto->get('audio_type', 0);
            $item->video_type = $auto->get('video_type', 0);
            $item->video_download = $auto->get('video_download', 0);
            $item->audio_download = $auto->get('audio_download', 0);
            $item->comments = $auto->get('comments', 0);
            $item->studylist = $auto->get('studylist', 1);
            $item->teacher = $auto->get('teacher', array());
            $item->add_downloadvid = $auto->get('add_downloadvid', 0);
            $item->notes = $auto->get('notes', 0);
            $item->slides = $auto->get('slides', 0);
            $item->slides_type = $auto->get('slides_type', 0);
            $item->access = $auto->get('access', 1);
            $item->audpurchase = $auto->get('audpurchase', 0);
            $item->audpurchase_folder = $auto->get('audpurchase_folder', 0);
            $item->vidpurchase = $auto->get('vidpurchase', 0);
            $item->vidpurchase_folder = $auto->get('vidpurchase_folder', 0);
            $item->language = $auto->get('language', '*'); 
        }
        return $item;
    }
        
        
/**
     * Method to save the form data.
     *
     * @param    array    The form data.
     *
     * @return    boolean    True on success.
     * @since    1.6
     */
    public function save($data)
    {
        
        $user    = JFactory::getUser();
        $app = JFactory::getApplication();
        $session = JFactory::getSession();
        $params = Tewebdetails::getparams('com_preachit');
        
        if(empty($data['study_alias'])) 
        {
            $data['study_alias'] = $data['study_name'];
        }
        $data['study_alias'] = Tewebadmin::uniquealias('#__pistudies',$data['study_alias'], $data['id'], 'study_alias');
        
        $data = PIHelperforms::checkentries($data, 'Studies', $params->get('mfhide', array()));
        
        if ($data['id'] == 0)
        {$data['user'] = $user->id;}
        
        // check to see if new teacher, series, ministry
        $newteacher = '';
        $newseries = '';
        $newministry = '';
        if (isset($data['newteacher']))
        {
        	$newteacher = $data['newteacher'];
        	unset($data['newteacher']);
        }
        if (isset($data['newseries']))
        {
        	$newseries = $data['newseries'];
        	unset($data['newseries']);
        }
        if (isset($data['newministry']))
        {
        	$newministry = $data['newministry'];
        	unset($data['newministry']);
        }
        
        if (trim($newteacher) != '')
        {
            $newteacher = PIHelperadmin::createnewteacher($newteacher, 1);
            if (isset($data['teacher']) && is_array($data['teacher'])) {
                $data['teacher'][] = $newteacher->id;
            }
            else {$data['teacher'][0] = $newteacher->id;}
        }
        
        if (trim($newseries) != '')
        {
            $newseries = PIHelperadmin::createnewseries($newseries);
            $data['series'] = $newseries->id;
        }
         
        if (trim($newministry) != '')
        {
            $newministry = PIHelperadmin::createnewministry($newministry);
            if (isset($data['ministry']) && is_array($data['ministry'])) {
                $data['ministry'][] = $newministry->id;
            }
            else {$data['ministry'][0] = $newministry->id;}
        }   

        if (isset($data['teacher']) && is_array($data['teacher'])) {
                $registry = new JRegistry;
                $registry->loadArray($data['teacher']);
                $data['teacher'] = (string)$registry;
        }
        elseif (!isset($data['teacher'])) 
        {
            $data['teacher'] = '{"0":""}';
        }
        
        if (isset($data['ministry']) && is_array($data['ministry'])) {
                $registry = new JRegistry;
                $registry->loadArray($data['ministry']);
                $data['ministry'] = (string)$registry;
        }
        elseif (!isset($data['ministry'])) 
        {
            $data['ministry'] = '{"0":""}';
        }
        
        if (isset($data['series'])) {
                //set saccess
                $data = PIHelperadmin::getsaccess($data);
        }
        else {$data['saccess'] = 0;}
        
        // change folder entries if only default allowed
        if ($params->get('default_folder_only', 0) == 1)
        {
            if (isset($data['audio_folder']) && $data['audio_folder'] != '')
            {$data['audio_folder'] = $params->get('audio_folder', 0);}
            if (isset($data['video_folder']) && $data['video_folder'] != '')
            {$data['video_folder'] = $params->get('video_folder', 0);}
            if (isset($data['downloadvid_folder']) && $data['downloadvid_folder'] != '')
            {$data['downloadvid_folder'] = $params->get('downloadvid_folder', 0);}
            if (isset($data['image_folderlrg']) && $data['image_folderlrg'] != '')
            {$data['image_folderlrg'] =  $params->get('image_folder', 0);}
            if (isset($data['notes_folder']) && $data['notes_folder'] != '')
            {$data['notes_folder'] = $params->get('notes_folder', 0);}
            if (isset($data['slides_folder']) && $data['slides_folder'] != '')
            {$data['slides_folder'] = $params->get('slides_folder', 0);}
        }
        
        // add 3rd party overrides if present
        $video_link = JRequest::getVar('videooverride', null);
        $video_type = JRequest::getInt('videotypeoverride', 0);
        if ($video_link && $video_type > 0)
        {
                $data['video_link'] = $video_link;
                $data['video_type'] = $video_type;
        }
        $vclip1_link = JRequest::getVar('vclip1override', null);
        $vclip1_type = JRequest::getInt('vclip1typeoverride', 0);
        if ($vclip1_link && $vclip1_type > 0)
        {
                $data['vclip1_link'] = $vclip1_link;
                $data['vclip1_type'] = $vclip1_type;
        }
        $vclip2_link = JRequest::getVar('vclip2override', null);
        $vclip2_type = JRequest::getInt('vclip2typeoverride', 0);
        if ($vclip2_link && $vclip2_type > 0)
        {
                $data['vclip2_link'] = $vclip2_link;
                $data['vclip2_type'] = $vclip2_type;
        }
        $vclip3_link = JRequest::getVar('vclip3override', null);
        $vclip3_type = JRequest::getInt('vclip3typeoverride', 0);
        if ($vclip3_link && $vclip3_type > 0)
        {
                $data['vclip3_link'] = $vclip3_link;
                $data['vclip3_type'] = $vclip3_type;
        }
        
        // get filesizes if needed
        $data = PIHelperadmin::getfilesize($data);
        // get study dates if needed
        $data = PIHelperadmin::getstudydates($data);
        
        if (!isset($data['language']) || $data['language'] == '')
        {
            $data['language'] = '*';
        }
           
        // set image change session variable
        if (Tewebadmin::entrychanged('imagelrg', '#__pistudies', $data) || Tewebadmin::entrychanged('image_folderlrg', '#__pistudies', $data))
        {
            $session->set('mesimagechange', true);
        }
        else {$session->set('mesimagechange', false);}

        if (parent::save($data)) {

            return true;
        }


        return false;
    }
    
    /**
     * Method to get a table object, load it if necessary.
     *
     * @param   string  $name     The table name. Optional.
     * @param   string  $prefix   The class prefix. Optional.
     * @param   array   $options  Configuration array for model. Optional.
     *
     * @return  JTable  A JTable object
     *
     * @since   11.1
     */
    public function getTable($name = '', $prefix = 'Table', $options = array())
    {
        if (empty($name))
        {
            $name = $this->getName();
        }

        if ($table = $this->_createTable('studies', $prefix, $options))
        {
            return $table;
        }

        JError::raiseError(0, JText::sprintf('JLIB_APPLICATION_ERROR_TABLE_NAME_NOT_SUPPORTED', $name));

        return null;
    }
    
/**
     * Method to delete one or more records.
     *
     * @param   array  &$pks  An array of record primary keys.
     *
     * @return  boolean  True if successful, false if an error occurs.
     *
     * @since   11.1
     */
    public function delete(&$pks)
    {
            $abspath    = JPATH_SITE;
            require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/messageimage.php');
            jimport('joomla.filesystem.file');
            $pks = (array) $pks;
            foreach ($pks AS $pk)
            {
                $table = $this->getTable();
                if ($table->load($pk))
                {
                    $imagefile = array();
                    $imagefile[] = JPATH_ROOT.DIRECTORY_SEPARATOR.PIHelpermimage::getimagepath($pk, 'small', $table->imagelrg);
                    $imagefile[] = JPATH_ROOT.DIRECTORY_SEPARATOR.PIHelpermimage::getimagepath($pk, 'medium', $table->imagelrg);
                    $imagefile[] = JPATH_ROOT.DIRECTORY_SEPARATOR.PIHelpermimage::getimagepath($pk, 'large', $table->imagelrg);
                    foreach ($imagefile AS $im)
                    {
                        if (file_exists($im))
                        {
                            JFile::delete($im);
                        }
                    }      
                }
            }
            
        if (parent::delete($pks)) {
            return true;
        }


        return false;
    }
    
        
    /**
     * Method to toggle the featured setting of articles.
     *
     * @param   array    The ids of the items to toggle.
     * @param   integer  The value to toggle to.
     *
     * @return  boolean  True on success.
     */
    public function featured($pks, $value = 0)
    {
        // Sanitize the ids.
        $pks = (array) $pks;
        JArrayHelper::toInteger($pks);

        if (empty($pks))
        {
            $this->setError(JText::_('COM_PREACHIT_NO_ITEM_SELECTED'));
            return false;
        }

        try
        {
            $db = $this->getDbo();
            
            $db->setQuery(
                'UPDATE #__pistudies' .
                    ' SET featured = ' . 0
            );
            $db->execute();

            $db->setQuery(
                'UPDATE #__pistudies' .
                    ' SET featured = ' . (int) $value .
                    ' WHERE id = '.$pks[0]
            );
            $db->execute();
        }
        catch (Exception $e)
        {
            $this->setError($e->getMessage());
            return false;
        }

        $this->cleanCache();

        return true;
    }
}