<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');
jimport('joomla.form.helper');
JFormHelper::loadFieldClass('list');
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/forms.php');

class JFormFieldFhide extends JFormFieldList
{
/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'fhide';

	/**
	 * Method to get the field input markup.
	 *
	 * @return	string	The field input markup.
	 * @since	1.6
	 */

protected function getOptions()
{
	
        if ($this->element['formtype'] == 'series')
        {
                $options = array();
                $options = PIHelperforms::seriesformitems();
                $options = array_merge(parent::getOptions() , $options);
                return $options;
        }
        elseif ($this->element['formtype'] == 'teacher')
        {
                $options = array();
                $options = PIHelperforms::teacherformitems();
                $options = array_merge(parent::getOptions() , $options);
                return $options;
        }
        elseif ($this->element['formtype'] == 'message')
        {
                $options = array();
                $options = PIHelperforms::messageformitems();
                $options = array_merge(parent::getOptions() , $options);
                return $options;
        }
        

}
}