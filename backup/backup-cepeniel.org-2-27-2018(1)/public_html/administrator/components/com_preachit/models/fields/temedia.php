<?php
/**
 * @package     Joomla.Platform
 * @subpackage  Form
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

defined('JPATH_PLATFORM') or die;

/**
 * Form Field class for the Joomla Platform.
 * Provides a modal media selector including upload mechanism
 *
 * @package     Joomla.Platform
 * @subpackage  Form
 * @since       11.1
 */
class JFormFieldTemedia extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var    string
	 * @since  11.1
	 */
	protected $type = 'Temedia';

	/**
	 * The initialised state of the document object.
	 *
	 * @var    boolean
	 * @since  11.1
	 */
	protected static $initialised = false;

	/**
	 * Method to get the field input markup for a media selector.
	 * Use attributes to identify specific created_by and asset_id fields
	 *
	 * @return  string  The field input markup.
	 *
	 * @since   11.1
	 */
	protected function getInput()
	{
        $app = JFactory::getApplication();
		$asset = JRequest::getVar('view', '');
		if ($asset == '')
		{
			$asset = JRequest::getCmd('option');
		}

		$link = (string) $this->element['link'];
		if (!self::$initialised)
		{

			// Load the modal behavior script.
			JHtml::_('behavior.modal');

			// Build the script.
			$script = array();
			$script[] = '	function teInsertFieldValue(value, id) {';
			$script[] = '		var old_value = document.id(id).value;';
			$script[] = '		if (old_value != value) {';
			$script[] = '			var elem = document.id(id);';
			$script[] = '			elem.value = value;';
			$script[] = '			elem.fireEvent("change");';
			$script[] = '			if (typeof(elem.onchange) === "function") {';
			$script[] = '				elem.onchange();';
			$script[] = '			}';
			$script[] = '		}';
			$script[] = '	}';

			// Add the script to the document head.
			JFactory::getDocument()->addScriptDeclaration(implode("\n", $script));

			self::$initialised = true;
		}

		// Initialize variables.
		$html = array();
        
         $attr = '';
         // Initialize some field attributes.
         $attr .= $this->element['class'] ? ' class="' . (string) $this->element['class'] . '"' : '';
         $attr .= $this->element['size'] ? ' size="' . (int) $this->element['size'] . '"' : '';

         // Initialize JavaScript field attributes.
         $attr .= $this->element['onchange'] ? ' onchange="' . (string) $this->element['onchange'] . '"' : '';
        
        
        if (Tewebcheck::getJversion() >= 3.0)
        {
            // The text field.
            $html[] = '<div class="input-prepend input-append">';
            $html[] = '    <input class="input-small" type="text" name="' . $this->name . '" id="' . $this->id . '"' . ' value="'
                . htmlspecialchars($this->value, ENT_COMPAT, 'UTF-8') . '"' . ' readonly="readonly"' . $attr . ' />';
            


            // The button.
            $html[] = '<a class="modal btn" title="' . JText::_('JLIB_FORM_BUTTON_SELECT') . '"' . ' href="'
                . ($this->element['readonly'] ? ''
                : ($link ? $link
                    : 'index.php?option=com_preachit&amp;view=temedia&amp;tmpl=component&amp;asset=' . $asset) . '&amp;fieldid=' . $this->id. '&amp;media=' . $this->element['media']. '&amp;foldchange=' . $this->element['foldchange']) . '"'
                . ' rel="{handler: \'iframe\', size: {x: 800, y: 500}}">';
            $html[] = JText::_('JLIB_FORM_BUTTON_SELECT') . '</a>';

            $html[] = '<a data-original-title="' . JText::_('JLIB_FORM_BUTTON_CLEAR') . '"' . ' href="#" class="btn" onclick="';
            $html[] = 'teInsertFieldValue(\'\', \'' . $this->id . '\');';
            $html[] = 'return false;';
            $html[] = '">';
            $html[] = '<i class="icon-remove"></i></a>';
            $html[] = '</div>';
        }
        elseif ($app->isSite())
        {
            // The text field.
            $html[] = '<input type="text" name="' . $this->name . '" id="' . $this->id . '"' . ' value="'
                . htmlspecialchars($this->value, ENT_COMPAT, 'UTF-8') . '"' . ' readonly="readonly"' . $attr . ' />';

            // The button.
            $html[] = '<a class="btn modal" title="' . JText::_('JLIB_FORM_BUTTON_SELECT') . '"' . ' href="'
                . ($this->element['readonly'] ? ''
                : ($link ? $link
                    : 'index.php?option=com_preachit&amp;view=temedia&amp;tmpl=component&amp;asset=' . $asset) . '&amp;fieldid=' . $this->id. '&amp;media=' . $this->element['media']. '&amp;foldchange=' . $this->element['foldchange']) . '"'
                . ' rel="{handler: \'iframe\', size: {x: 800, y: 500}}">';
            $html[] = JText::_('JLIB_FORM_BUTTON_SELECT') . '</a>';
            $html[] = '<a class="btn" title="' . JText::_('JLIB_FORM_BUTTON_CLEAR') . '"' . ' href="#" onclick="';
            $html[] = 'teInsertFieldValue(\'\', \'' . $this->id . '\');';
            $html[] = 'return false;';
            $html[] = '">';
            $html[] = JText::_('JLIB_FORM_BUTTON_CLEAR') . '</a>';
        }
        else {

            // The text field.
            $html[] = '<div class="fltlft">';
            $html[] = '    <input type="text" name="' . $this->name . '" id="' . $this->id . '"' . ' value="'
                . htmlspecialchars($this->value, ENT_COMPAT, 'UTF-8') . '"' . ' readonly="readonly"' . $attr . ' />';
            $html[] = '</div>';

            // The button.
            $html[] = '<div class="button2-left">';
            $html[] = '    <div class="blank">';
            $html[] = '        <a class="modal" title="' . JText::_('JLIB_FORM_BUTTON_SELECT') . '"' . ' href="'
                . ($this->element['readonly'] ? ''
                : ($link ? $link
                    : 'index.php?option=com_preachit&amp;view=temedia&amp;tmpl=component&amp;asset=' . $asset) . '&amp;fieldid=' . $this->id. '&amp;media=' . $this->element['media']. '&amp;foldchange=' . $this->element['foldchange']) . '"'
                . ' rel="{handler: \'iframe\', size: {x: 800, y: 500}}">';
            $html[] = JText::_('JLIB_FORM_BUTTON_SELECT') . '</a>';
            $html[] = '    </div>';
            $html[] = '</div>';

            $html[] = '<div class="button2-left">';
            $html[] = '    <div class="blank">';
            $html[] = '        <a title="' . JText::_('JLIB_FORM_BUTTON_CLEAR') . '"' . ' href="#" onclick="';
            $html[] = 'teInsertFieldValue(\'\', \'' . $this->id . '\');';
            $html[] = 'return false;';
            $html[] = '">';
            $html[] = JText::_('JLIB_FORM_BUTTON_CLEAR') . '</a>';
            $html[] = '    </div>';
            $html[] = '</div>';
        }
        
		return implode("\n", $html);
	}
}
