<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined('JPATH_BASE') or die;
$lang = JFactory::getLanguage();
$lang->load('lib_teweb', JPATH_SITE);
jimport('joomla.html.html');
jimport('joomla.form.formfield');
jimport('joomla.form.helper');
jimport('teweb.admin.javascript');
JFormHelper::loadFieldClass('list');

class JFormFieldTelist extends JFormFieldList
{
/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'Telist';

	/**
	 * Method to get the field input markup.
	 *
	 * @return	string	The field input markup.
	 * @since	1.6
	 */

protected function getOptions()
	{
        $db = JFactory::getDBO();
        $doc = JFactory::getDocument();
		if ($this->element['jsdisable'] == 'true')
        {
            $select = 'jform_'.$this->element['switcher'];
            $selector = $this->id; 
            if ($this->element['extra'])
            {$extra = $this->element['extra'];}
            else {$extra = null;}
            $js = Tewebadminjs::selectorjs($select, $selector, $extra);
            $doc->addScriptDeclaration($js);
        }
        
        if ($this->element['where'])
        {
            $where = ' '.$this->element['where'];
        }
        else {$where = ' WHERE published = 1';}
        
            $query = $db->getQuery(true); 
            $query = 'SELECT '.$this->element['return'].' AS id, '.$this->element['display'].' AS text'
            .' FROM #__'.$this->element['table']
            .$where
            .' ORDER BY id';
            $db->setQuery($query);
            $slists = $db->loadObjectList();
                $options = array();
                foreach($slists as $slist) 
                {
                        $options[] = JHtml::_('select.option', $slist->id, $slist->text);
                }
                $options = array_merge(parent::getOptions() , $options);
                return $options;

}
}