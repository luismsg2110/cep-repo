<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');
jimport('joomla.form.helper');
jimport('teweb.admin.javascript');
JFormHelper::loadFieldClass('list');

class JFormFieldTeacherlist extends JFormFieldList
{
/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'Teacherlist';

	/**
	 * Method to get the field input markup.
	 *
	 * @return	string	The field input markup.
	 * @since	1.6
	 */

protected function getOptions()
	{$db = JFactory::getDBO();

$doc = JFactory::getDocument();
		$abspath    = JPATH_SITE;
		require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/additional.php');	
		$translate = PIHelperadditional::translate();
        if ($this->element['jsdisable'] == 'true')
        {
            $select = 'jform_'.$this->element['switcher'];
            $selector = $this->id; 
            if ($this->element['extra'])
            {$extra = $this->element['extra'];}
            else {$extra = null;}
            $js = Tewebadminjs::selectorjs($select, $selector, $extra);
            $doc->addScriptDeclaration($js);
        }
		
$query = $db->getQuery(true); 
$query = 'SELECT id, teacher_name, lastname, language'
.' FROM #__piteachers'
.' WHERE published = 1'
.' ORDER BY lastname';
$db->setQuery($query);
$tlists = $db->loadObjectList();

                $options = array();
                foreach($tlists as $tlist) 
                {
                    if ($tlist->lastname != '')
                    {
                        $name = $tlist->teacher_name.' '.$tlist->lastname;
                        if ($translate)
                            {if ($tlist->language != '*') {$name = $tlist->teacher_name.' '.$tlist->lastname.' - '.$tlist->language;}}
                        $options[] = JHtml::_('select.option', $tlist->id, $name);
                    }
                }
                $options = array_merge(parent::getOptions() , $options);
                return $options;

}
}