<?php
/**
 * @Component - Preachit
 * @version 1.0.0 May, 2010
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha, LLC
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 *
 *
 */
// no direct access
defined('_JEXEC') or die;
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/admin.php');
jimport('joomla.application.component.modeladmin');

class PreachitModelSeries extends JModelAdmin
{
	
	
        public function getForm($data = array(), $loadData = true) 
        {
        			jimport('joomla.form.form');
        			$abspath    = JPATH_SITE;
					$models_path = $abspath.DIRECTORY_SEPARATOR.'administrator'.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_preachit'.DIRECTORY_SEPARATOR.'models';
 
                JForm::addFormPath ($models_path . DIRECTORY_SEPARATOR . 'forms');
                JForm::addFieldPath($models_path . DIRECTORY_SEPARATOR . 'fields');
 
                $form = $this->loadForm('com_preachit.seriesedit', 'seriesedit', array('control' => 'jform', 'load_data' => $loadData));
                return $form;
        }
        
        /**
     * Method to get the data that should be injected in the form.
     *
     * @return  mixed  The data for the form.
     *
     * @since   1.6
     */
    protected function loadFormData()
    {
        // Check the session for previously entered form data.
        $app  = JFactory::getApplication();
        $data = $app->getUserState('com_preachit.edit.series.data', array());

        if (empty($data))
        {
            $data = $this->getItem();
            if (isset($data->ministry))
            {$registry = new JRegistry;
            $registry->loadString($data->ministry);
            $data->ministry = $registry->toArray();}
        }

        return $data;
    }
        
   /**
     * Method to save the form data.
     *
     * @param    array    The form data.
     *
     * @return    boolean    True on success.
     * @since    1.6
     */
    public function save($data)
    {
        $user    = JFactory::getUser();
        $app = JFactory::getApplication();
        $session = JFactory::getSession();
        $params = Tewebdetails::getparams('com_preachit');
        if(empty($data['series_alias'])) 
        {
            $data['series_alias'] = $data['series_name'];
        }
        $data['series_alias'] = Tewebadmin::uniquealias('#__piseries',$data['series_alias'], $data['id'], 'series_alias');
        
        if ($app->isSite() || $params->get('adminhide', 0) == 1)  
        {
            // get hidden fields
            $row = PIHelperforms::checkentries($data, 'Series', $params->get('sfhide', array()));
        }
        
        if ($data['id'] == 0)
        {$data['user'] = $user->id;}
        
        if (isset($data['ministry']) && is_array($data['ministry'])) {
                $registry = new JRegistry;
                $registry->loadArray($data['ministry']);
                $data['ministry'] = (string)$registry;
        }
        else {$data['ministry'] = '{"0":""}';}
        
        // add 3rd party overrides if present
        $video_link = JRequest::getVar('videooverride', null);
        $video_type = JRequest::getInt('videotypeoverride', 0);
        if ($video_link && $video_type > 0)
        {
                $data['videolink'] = $video_link;
                $data['videoplayer'] = $video_type;
        }
        
        // change folder entries if only default allowed
        if ($params->get('default_folder_only', 0) == 1)
        {
            if (isset($data['image_folderlrg']) && $data['image_folderlrg'] != '')
            {$data['image_folderlrg'] =  $params->get('image_folder', 0);}
            if (isset($data['videofolder']) && $data['videofolder'] != '')
            {$data['videofolder'] =  $params->get('videofolder', 0);}
        }
        
        if (!isset($data['language']) || $data['language'] == '')
        {
            $data['language'] = '*';
        }
        
        // set image change session variable
        if (Tewebadmin::entrychanged('series_image_lrg', '#__piseries', $data) || Tewebadmin::entrychanged('image_folderlrg', '#__piseries', $data))
        {
            $session->set('serimagechange', true);
        }
        else {$session->set('serimagechange', false);}

        if (parent::save($data)) {

            return true;
        }


        return false;
    }
    
/**
     * Method to delete one or more records.
     *
     * @param   array  &$pks  An array of record primary keys.
     *
     * @return  boolean  True if successful, false if an error occurs.
     *
     * @since   11.1
     */
    public function delete(&$pks)
    {
            $abspath    = JPATH_SITE;
            $app = JFactory::getApplication ();
            $db=& JFactory::getDBO(); 
            require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/seriesimage.php');
            require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/message-info.php');
            jimport('joomla.filesystem.file');
            $pks = (array) $pks;
            foreach ($pks AS $key => $pk)
            {
                $table = $this->getTable();
                if ($table->load($pk))
                {   
                    // see if any messages are associated with this series
                    $messageno = PIHelpermessageinfo::seriescount($pk);
                    if ($messageno > 0)
                    {
                        // get series name
                        //get seriesname
                        $query = "
                        SELECT ".$db->quoteName('series_name')."
                        FROM ".$db->quoteName('#__piseries')."
                        WHERE ".$db->quoteName('id')." = ".$db->quote($pk).";
                        ";
                        $db->setQuery($query);
                        $name = $db->loadResult();
                        $app->enqueueMessage ( $name.' - '.JText::_('LIB_TEWEB_FAILED_DELETE_RECORDS_ATTACHED'), 'notice' );
                        unset($pks[$key]);
                        continue;
                    }
                    // set saccess to 0 in all relevent messages
                    $saccess = PIHelperadmin::setsaccess($pk, 0);
                    // set series to 0 in all relevent messages   
                    $query = "SELECT id, study_name FROM #__pistudies WHERE series = ".$pk; 
                    $db->setQuery($query);
                    $messages = $db->loadObjectList();
    
                    if (is_array($messages))
                    {    
                        foreach ($messages as $message)
                        {
                            $db->setQuery ("UPDATE #__pistudies SET series = 0 WHERE id = '{$message->id}' ;"); 
                            $db->query();
                        }
                    }
                    // delete resized images
                    $imagefile = array();
                    $imagefile[] = JPATH_ROOT.DIRECTORY_SEPARATOR.PIHelpersimage::getimagepath($pk, 'small', $table->series_image_lrg);
                    $imagefile[] = JPATH_ROOT.DIRECTORY_SEPARATOR.PIHelpersimage::getimagepath($pk, 'medium', $table->series_image_lrg);
                    $imagefile[] = JPATH_ROOT.DIRECTORY_SEPARATOR.PIHelpersimage::getimagepath($pk, 'large', $table->series_image_lrg);
                    foreach ($imagefile AS $im)
                    {
                        if (file_exists($im))
                        {
                            JFile::delete($im);
                        }
                    }  
                }
            }
        if (count($pks > 0))
        {
            if (parent::delete($pks)) {
            
                return true;
            }
        }
            return false;
    }
            
    /**
     * Method to toggle the featured setting of articles.
     *
     * @param   array    The ids of the items to toggle.
     * @param   integer  The value to toggle to.
     *
     * @return  boolean  True on success.
     */
    public function featured($pks, $value = 0)
    {
        // Sanitize the ids.
        $pks = (array) $pks;
        JArrayHelper::toInteger($pks);

        if (empty($pks))
        {
            $this->setError(JText::_('COM_PREACHIT_NO_ITEM_SELECTED'));
            return false;
        }

        try
        {
            $db = $this->getDbo();
            
            $db->setQuery(
                'UPDATE #__piseries' .
                    ' SET featured = ' . 0
            );
            $db->execute();

            $db->setQuery(
                'UPDATE #__piseries' .
                    ' SET featured = ' . (int) $value .
                    ' WHERE id = '.$pks[0]
            );
            $db->execute();
        }
        catch (Exception $e)
        {
            $this->setError($e->getMessage());
            return false;
        }

        $this->cleanCache();

        return true;
    }
}