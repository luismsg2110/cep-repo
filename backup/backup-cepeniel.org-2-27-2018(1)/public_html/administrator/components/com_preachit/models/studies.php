<?php
/**
 * @Component - Preachit
 * @version 1.0.0 May, 2010
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha, LLC
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 *
 *
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.modellist');
class PreachitModelStudies extends JModelList
{
var $_data = null;
var $_pagination = null;
var $_total = null;
var $_search = null;
var $_query = null;

function __construct($config = array())
{
    
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'id', 'id',
                'study_name', 'study_name',
                'study_alias', 'study_alias',
                'study_date', 'study_date',
                'published', 'published',
                'teacher', 'teacher',
                'language', 'language',
                'hits', 'hits',
                'downloads', 'downloads'
            );
        }
        parent::__construct($config);        
}

	function _buildQuery()
	{
		$where		= $this->_buildContentWhere();
		$orderby	= $this->_buildContentOrderBy();
		$query = "SELECT * FROM #__pistudies" 
			. $where
			. $orderby
			; 
		return $query;
	}

function getData() 
  {
        // if data hasn't already been obtained, load it
        if (empty($this->_data)) 
{
            $query = $this->_buildQuery();
            $this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit')); 
        }
        return $this->_data;
  }

function getTotal()
  {
        // Load the content if it doesn't already exist
        if (empty($this->_total)) {
            $query = $this->_buildQuery();
            $this->_total = $this->_getListCount($query);    
        }
        return $this->_total;
  }
  
function _buildContentWhere()
	{
		$where = array();

        $book = $this->getState('filter.book');
		if (is_numeric($book) && $book > 0) {
			$where[] = ' #__pistudies.study_book = '.(int) $book;
		}
        
        $teacher = $this->getState('filter.teacher');
		if (is_numeric($teacher) && $teacher > 0) {
			$where[] = ' #__pistudies.teacher REGEXP \'(.*:"'.(int) $teacher.'".*)\''; 
		}
        
        $series = $this->getState('filter.series');
		if (is_numeric($series) && $series > 0) {
			$where[] = ' #__pistudies.series = '.(int) $series;
		}
        
        $ministry = $this->getState('filter.ministry');
        if (is_numeric($ministry) && $ministry > 0) {
			$where[] = ' #__pistudies.ministry REGEXP \'(.*:"'.(int) $ministry.'".*)\''; 
		}
		
        $year = $this->getState('filter.year');
        if (is_numeric($year) && $year > 0) {
			$where[] = " date_format(#__pistudies.study_date, '%Y')= ".(int) $year;
		}
		
		// Filter by published state
        $published = $this->getState('filter.state');
        if (is_numeric($published)) {
            $where[] = " #__pistudies.published =".(int) $published;
        } elseif ($published === '') {
            $where[] = " #__pistudies.published IN (0, 1)";
        }
		if ($this->getState('filter.language')) {
            $where[] = ' #__pistudies.language = '.$this->_db->quote($this->getState('filter.language'));
        }
		
		$where 		= ( count( $where ) ? ' WHERE '. implode( ' AND ', $where ) : '' );

		return $where;
	}
	
function _buildContentOrderBy()
    {
        $app = JFactory::getApplication();
        $option = JRequest::getCmd('option');
        
        $filter_order    = $this->state->get('list.ordering', 'ordering');
        $filter_order_Dir    = $this->state->get('list.direction', 'ASC');
        $orderby = ' ORDER BY '.$filter_order.' '.$filter_order_Dir.' , id';
        
        return $orderby;
    }
    
    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @since    1.6
     */
    protected function populateState($ordering = null, $direction = null)
    {
        $app = JFactory::getApplication('administrator');

        // Load the filter state.
        $search = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
        $this->setState('filter.search', $search);

        $state = $this->getUserStateFromRequest($this->context.'.filter.statemes', 'filter_statemes', '', 'string');
        $this->setState('filter.state', $state);
        
        $language = $this->getUserStateFromRequest($this->context.'.filter.language', 'filter_language', '');
        $this->setState('filter.language', $language);
        
        $book = $app->getUserStateFromRequest( $this->context.'filter_book', 'filter_book', '', 'string' );
        $this->setState('filter.book', $book);
        
        $teacher = $app->getUserStateFromRequest( $this->context.'filter_teacher','filter_teacher', '', 'string' );
        $this->setState('filter.teacher', $teacher);
        
        $series = $app->getUserStateFromRequest( $this->context.'filter_series', 'filter_series', '', 'string' );
        $this->setState('filter.series', $series);
        
        $ministry = $app->getUserStateFromRequest( $this->context.'filter_ministry', 'filter_ministry', '', 'string' );
        $this->setState('filter.ministry', $ministry);
        
        $year = $app->getUserStateFromRequest( $this->context.'filter_year','filter_year', '', 'int' );
        $this->setState('filter.year', $year);

        // Load the parameters.
        $params = JComponentHelper::getParams('com_preachit');
        $this->setState('params', $params);
        
        // set the pagination variables
        $limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'), 'int');
        $limitstart = $app->getUserStateFromRequest( $this->context.'&view=studies.limitstart', 'limitstart', 0);
        $this->setState('limit', $limit);
        $this->setState('limitstart', $limitstart);

        // List state information.
        parent::populateState('study_date', 'desc');
    }

}