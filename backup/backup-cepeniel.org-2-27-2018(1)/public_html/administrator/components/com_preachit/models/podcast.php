<?php
/**
 * @Component - Preachit
 * @version 1.0.0 May, 2010
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha, LLC
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 *
 *
 */
// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

class PreachitModelPodcast extends JModelAdmin
{
	
	
        public function getForm($data = array(), $loadData = true) 
        {
        			jimport('joomla.form.form');
        			$abspath    = JPATH_SITE;
					$models_path = $abspath.DIRECTORY_SEPARATOR.'administrator'.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_preachit'.DIRECTORY_SEPARATOR.'models';
 
                JForm::addFormPath ($models_path . DIRECTORY_SEPARATOR . 'forms');
                JForm::addFieldPath($models_path . DIRECTORY_SEPARATOR . 'fields');
 
                $form = $this->loadForm('com_preachit.podcastedit', 'podcastedit', array('control' => 'jform', 'load_data' => $loadData));
                return $form;
        }
        
        /**
     * Method to get the data that should be injected in the form.
     *
     * @return  mixed  The data for the form.
     *
     * @since   1.6
     */
        protected function loadFormData()
        {
            // Check the session for previously entered form data.
            $app  = JFactory::getApplication();
            $data = $app->getUserState('com_preachit.edit.podcast.data', array());

            if (empty($data))
            {
                $data = $this->getItem();
                if (isset($data->teacher_list))
                {$registry = new JRegistry;
                $registry->loadString($data->teacher_list);
                $data->teacher_list = $registry->toArray();}
                                
                if (isset($data->ministry_list))
                {$registry = new JRegistry;
                $registry->loadString($data->ministry_list);
                $data->ministry_list = $registry->toArray();}
                                
                if (isset($data->series_list))
                {$registry = new JRegistry;
                $registry->loadString($data->series_list);
                $data->series_list = $registry->toArray();}
                                
                if (isset($data->media_list))
                {$registry = new JRegistry;
                $registry->loadString($data->media_list);
                $data->media_list = $registry->toArray();}
            }

            return $data;
        }
        
            /**
     * Method to save the form data.
     *
     * @param    array    The form data.
     *
     * @return    boolean    True on success.
     * @since    1.6
     */
    public function save($data)
    {
        
            if (isset($data['teacher_list']) && is_array($data['teacher_list'])) {
                $registry = new JRegistry;
                $registry->loadArray($data['teacher_list']);
                $data['teacher_list'] = (string)$registry;
            }
            else {$data['teacher_list'] = '{"0":""}';}
            
            if (isset($data['ministry_list']) && is_array($data['ministry_list'])) {
                $registry = new JRegistry;
                $registry->loadArray($data['ministry_list']);
                $data['ministry_list'] = (string)$registry;
            }
            else {$data['ministry_list'] = '{"0":""}';}
            
            if (isset($data['series_list']) && is_array($data['series_list'])) {
                $registry = new JRegistry;
                $registry->loadArray($data['series_list']);
                $data['series_list'] = (string)$registry;
            }
            else {$data['series_list'] = '{"0":""}';}
            
            if (isset($data['media_list']) && is_array($data['media_list'])) {
                $registry = new JRegistry;
                $registry->loadArray($data['media_list']);
                $data['media_list'] = (string)$registry;
            }
            else {$data['medialist'] = '{"0":""}';}

        if (parent::save($data)) {

            return true;
        }


        return false;
    }
    
}