<?php
/**
 * @Component - Preachit
 * @version 1.0.0 May, 2010
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha, LLC
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 *
 *
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.modellist');
jimport('teweb.file.functions');
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/helpers/temp.php');
class PreachitModelTemplates extends JModelList
{
var $_data = null;
var $_pagination = null;
var $_total = null;
var $_search = null;
var $_query = null;

function __construct($config = array())
{
    if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'id', 'id',
                'title', 'title',
                'def', 'def',
                'template', 'template',
                'language', 'language',
                'teacher_role', 'teacher_role'
            );
        }
        parent::__construct($config);
}

    
 function _buildQuery()
    {
        $where        = $this->_buildContentWhere();
        $orderby    = $this->_buildContentOrderBy();
        $query = "SELECT * FROM #__pitemplate"
        . $where
        . $orderby 
            ;
        return $query;
    }


 function getData() 
  {
        jimport('joomla.filesystem.folder');
        jimport( 'joomla.utilities.simplexml' );
        $client    = JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));
        $templateBaseDir = $client->path.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_preachit'.DIRECTORY_SEPARATOR.'templates';
        $templateDirs = JFolder::folders($templateBaseDir);
        $rows = array();        $i = 0;
        // if data hasn't already been obtained, load it
        
        if (empty($this->_data)) 
        {
            $query = $this->_buildQuery();
            $temps = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit')); 
            
            foreach ($temps AS $temp)
            {    
                            $rows[$i] = new stdClass();
                            $rows[$i]->name = $temp->title;
                            $rows[$i]->template = $temp->template;
                            $rows[$i]->def = $temp->def;
                            $rows[$i]->id = $temp->id;
                            $rows[$i]->css = null;
                            $rows[$i]->client_id = $temp->client_id;
                            $rows[$i]->checked_out = $temp->checked_out;
                            $rows[$i]->checked_out_time = $temp->checked_out_time;
                            $csstest = $templateBaseDir.DIRECTORY_SEPARATOR.$temp->template.'/css';    
                            $xmltest = $templateBaseDir.DIRECTORY_SEPARATOR.$temp->template.'/template.xml';                    
                            $xml = JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_preachit'.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.$temp->template.DIRECTORY_SEPARATOR. 'template.xml';
                            
                            if (Tewebfile::checkfile($xml, 1))
                            { 
                            if ($rss = new SimpleXMLElement($xml,null,true))
                            {
                            $authorUrl = $rss->authorUrl;
                            $authorUrl = str_replace('http://','', $authorUrl );    
                            $authorUrl = 'http://'.$authorUrl;                            
                            $rows[$i]->date = $rss->creationDate;
                            $rows[$i]->author = '<a href="'.$authorUrl.'">'.$rss->author.'</a>';
                            $rows[$i]->version = $rss->version;
                           }
                           }
                           else {
                            $rows[$i]->date = '';
                            $rows[$i]->author = '';
                            $rows[$i]->version = '';
                           }
                           if (JFolder::exists($csstest) && $temp->client_id == 0)
                           {
                               $cssfiles = PIHelpertemp::getcssfiles($csstest);
                               $rows[$i]->css = PIHelpertemp::getcssstring($temp->template, $cssfiles);}
                           else {$rows[$i]->css = PIHelpertemp::getcssorlink($temp->id);}
                           $i++;
            }
            $this->_data = $rows;
        }
        
        return $this->_data;
  }


function getTotal()
  {
        // Load the content if it doesn't already exist
        if (empty($this->_total)) {
            $query = $this->_buildQuery();
            $this->_total = $this->_getListCount($query);    
        }
        return $this->_total;
  }
  
    function _buildContentWhere()
    {
        
        return null;
    }
    
    function _buildContentOrderBy()
    {
        $app = JFactory::getApplication();
        $option = JRequest::getCmd('option');
        
        $filter_order    = $this->state->get('list.ordering', 'title');
        $filter_order_Dir    = $this->state->get('list.direction', 'ASC');
        $orderby = ' ORDER BY '.$filter_order.' '.$filter_order_Dir.' , id';
        
        return $orderby;
    }
    
      /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @since    1.6
     */
    protected function populateState($ordering = null, $direction = null)
    {
        $app = JFactory::getApplication('administrator');

        // Load the parameters.
        $params = JComponentHelper::getParams('com_preachit');
        $this->setState('params', $params);
        
        // set the pagination variables
        $limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'), 'int');
        $limitstart = $app->getUserStateFromRequest( $this->context.'&view=templates.limitstart', 'limitstart', 0);
        $this->setState('limit', $limit);
        $this->setState('limitstart', $limitstart);

        // List state information.
        parent::populateState('title', 'asc');
    }

}