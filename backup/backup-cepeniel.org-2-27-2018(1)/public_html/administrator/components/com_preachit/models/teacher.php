<?php
/**
 * @Component - Preachit
 * @version 1.0.0 May, 2010
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha, LLC
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 *
 *
 */
// no direct access
defined('_JEXEC') or die;
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/admin.php');
jimport('joomla.application.component.modeladmin');

class PreachitModelTeacher extends JModelAdmin
{
	
	
        public function getForm($data = array(), $loadData = true) 
        {
        			jimport('joomla.form.form');
        			$abspath    = JPATH_SITE;
					$models_path = $abspath.DIRECTORY_SEPARATOR.'administrator'.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_preachit'.DIRECTORY_SEPARATOR.'models';
 
                JForm::addFormPath ($models_path . DIRECTORY_SEPARATOR . 'forms');
                JForm::addFieldPath($models_path . DIRECTORY_SEPARATOR . 'fields');
 
                $form = $this->loadForm('com_preachit.teacheredit', 'teacheredit', array('control' => 'jform', 'load_data' => $loadData));
                return $form;
        }
        
    /**
     * Method to get the data that should be injected in the form.
     *
     * @return  mixed  The data for the form.
     *
     * @since   1.6
     */
    protected function loadFormData()
    {
        // Check the session for previously entered form data.
        $app  = JFactory::getApplication();
        $data = $app->getUserState('com_preachit.edit.teacher.data', array());

        if (empty($data))
        {
            $data = $this->getItem();
        }

        return $data;
    }
        
    
   /**
     * Method to save the form data.
     *
     * @param    array    The form data.
     *
     * @return    boolean    True on success.
     * @since    1.6
     */
    public function save($data)
    {
        $user    = JFactory::getUser();
        $app = JFactory::getApplication();
        $session = JFactory::getSession();
        $params = Tewebdetails::getparams('com_preachit');
        
        if(empty($data['teacher_alias'])) 
        {
            if ($data['teacher_name'])
            {$name = $data['teacher_name'].' '.$data['lastname'];}
            else {$name = $data['lastname'];}
            $data['teacher_alias'] = $name;
        }
        $data['teacher_alias'] = Tewebadmin::uniquealias('#__piteachers',$data['teacher_alias'], $data['id'], 'teacher_alias');
        
        // sanitise variables
        if ($app->isSite() || $params->get('adminhide', 0) == 1)  
        {
            // get hidden fields
            $data = PIHelperforms::checkentries($data, 'Teachers', $params->get('tfhide', array()));
        }
        
        // change folder entries if only default allowed
        if ($params->get('default_folder_only', 0) == 1)
        {
            if (isset($data['image_folderlrg']) && $data['image_folderlrg'] != '')
            {$data['image_folderlrg'] =  $params->get('image_folder', 0);}
        }
        
        if (!isset($data['language']) || $data['language'] == '')
        {
            $data['language'] = '*';
        }
        
        // set image change session variable
        if (Tewebadmin::entrychanged('teacher_image_lrg', '#__piteachers', $data) || Tewebadmin::entrychanged('image_folderlrg', '#__piteachers', $data))
        {
            $session->set('teaimagechange', true);
        }
        else {$session->set('teaimagechange', false);}
        
        if ($data['id'] == 0)
        {$data['user'] = $user->id;}
   

        if (parent::save($data)) {

            return true;
        }


        return false;
    }
    
    /**
     * Method to get a table object, load it if necessary.
     *
     * @param   string  $name     The table name. Optional.
     * @param   string  $prefix   The class prefix. Optional.
     * @param   array   $options  Configuration array for model. Optional.
     *
     * @return  JTable  A JTable object
     *
     * @since   11.1
     */
    public function getTable($name = '', $prefix = 'Table', $options = array())
    {
        if (empty($name))
        {
            $name = $this->getName();
        }

        if ($table = $this->_createTable('teachers', $prefix, $options))
        {
            return $table;
        }

        JError::raiseError(0, JText::sprintf('JLIB_APPLICATION_ERROR_TABLE_NAME_NOT_SUPPORTED', $name));

        return null;
    }

/**
     * Method to delete one or more records.
     *
     * @param   array  &$pks  An array of record primary keys.
     *
     * @return  boolean  True if successful, false if an error occurs.
     *
     * @since   11.1
     */
    public function delete(&$pks)
    {
            $abspath    = JPATH_SITE;
            $app = JFactory::getApplication ();
            $db= JFactory::getDBO(); 
            require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/teacherimage.php');
            require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/message-info.php');
            jimport('joomla.filesystem.file');
            $pks = (array) $pks;
            foreach ($pks AS $key => $pk)
            {
                $table = $this->getTable();
                if ($table->load($pk))
                {
                    // see if any messages are associated with this series
                    $messageno = PIHelpermessageinfo::teachercount($pk);
                    if ($messageno > 0)
                    {
                        //get teachername
                        $query = "
                        SELECT ".$db->quoteName('teacher_name')."
                        FROM ".$db->quoteName('#__piteachers')."
                        WHERE ".$db->quoteName('id')." = ".$db->quote($pk).";
                        ";
                        $db->setQuery($query);
                        $name = $db->loadResult();
                        $query = "
                        SELECT ".$db->quoteName('lastname')."
                        FROM ".$db->quoteName('#__piteachers')."
                        WHERE ".$db->quoteName('id')." = ".$db->quote($pk).";
                        ";
                        $db->setQuery($query);
                        $lastname = $db->loadResult();
                        if ($name)
                        {$teachername = $name.' '.$lastname;}
                        else {$teachername = $lastname;}
                        $app->enqueueMessage ( $teachername.' - '.JText::_('LIB_TEWEB_FAILED_DELETE_RECORDS_ATTACHED'), 'notice' );
                        unset($pks[$key]);
                        continue;
                    }
                    // remove ministry record from any relevant messages
                    $query='SELECT id, teacher FROM #__pistudies WHERE teacher REGEXP \'(.*:"'.$pk.'".*)\'';
                    $db->setQuery($query);
                    $messages = $db->loadObjectList();
                    if (is_array($messages))
                    {
                        foreach ($messages AS $mes)
                        {
                            $teacher = PIHelperadmin::removeJSONconnection($mes->teacher, $pk);
                            $db->setQuery ("UPDATE #__pistudies SET teacher = '".$teacher."' WHERE id = '{$mes->id}' ;"); 
                            $db->query();
                        }
                    }
                    // delete resized images
                    $imagefile = array();
                    $imagefile[] = JPATH_ROOT.DIRECTORY_SEPARATOR.PIHelpertimage::getimagepath($pk, 'small', $table->teacher_image_lrg);
                    $imagefile[] = JPATH_ROOT.DIRECTORY_SEPARATOR.PIHelpertimage::getimagepath($pk, 'medium', $table->teacher_image_lrg);
                    $imagefile[] = JPATH_ROOT.DIRECTORY_SEPARATOR.PIHelpertimage::getimagepath($pk, 'large', $table->teacher_image_lrg);
                    foreach ($imagefile AS $im)
                    {
                        if (file_exists($im))
                        {
                            JFile::delete($im);
                        }
                    }      
                }
            }
            
        if (parent::delete($pks)) {
            return true;
        }


        return false;
    }   
            
    /**
     * Method to toggle the featured setting of articles.
     *
     * @param   array    The ids of the items to toggle.
     * @param   integer  The value to toggle to.
     *
     * @return  boolean  True on success.
     */
    public function featured($pks, $value = 0)
    {
        // Sanitize the ids.
        $pks = (array) $pks;
        JArrayHelper::toInteger($pks);

        if (empty($pks))
        {
            $this->setError(JText::_('COM_PREACHIT_NO_ITEM_SELECTED'));
            return false;
        }

        try
        {
            $db = $this->getDbo();
            
            $db->setQuery(
                'UPDATE #__piteachers' .
                    ' SET featured = ' . 0
            );
            $db->execute();

            $db->setQuery(
                'UPDATE #__piteachers' .
                    ' SET featured = ' . (int) $value .
                    ' WHERE id = '.$pks[0]
            );
            $db->execute();
        }
        catch (Exception $e)
        {
            $this->setError($e->getMessage());
            return false;
        }

        $this->cleanCache();

        return true;
    }

}