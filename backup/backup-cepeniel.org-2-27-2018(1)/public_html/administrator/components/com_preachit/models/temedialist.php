<?php
/**
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('teweb.file.temedialistmodel');

/**
 * Media Component List Model
 *
 * @package		Joomla.Administrator
 * @subpackage	com_media
 * @since 1.5
 */
class PreachitModelTemedialist extends ModelTemedialist
{
    public function __construct ( $config = array()) 
    {
        $this->folder = $this->returnFolder();
        $this->basePATH = $this->getbasePATH();
        parent::__construct($config);
    }
    
    
	function returnFolder()
	{
		$folder = JRequest::getInt('folder', 0);
        $params = Tewebdetails::getparams('com_preachit');
        if ($folder == 0 || $params->get('default_folder_only', 0) == 1)
        {
            $media = JRequest::getVar('media', 'image');
            $folder = PIHelperadditional::getdefaultfolders($params, $media, -1);
        }
		return $folder;
	}
    
    function getbasePATH()
    {
        $current = $this->folder;
        if ($current != -1 && $current != '')
        {
            // get details for this folder
            $folders = JTable::getInstance('Filepath', 'Table');
            $folders->load($current);
            if ($current == 0 || $folders->type != 0)
            {return false;}
            $folderpath = Tewebbuildurl::cleanfolder($folders->folder);
        }
        else {$folderpath = 'images';}
        return $folderpath;
    }

}
