<?php
defined('_JEXEC') or die;
jimport('joomla.application.component.controller');
class PreachitController extends JControllerLegacy
{
    
	public function display($cachable = false, $urlparams = false)
	{
		// Set a default view 
		if (!JRequest::getWord('view')) {
			JRequest::setVar('view', 'cpanel');
		}

		parent::display();

		return $this;
	}
}