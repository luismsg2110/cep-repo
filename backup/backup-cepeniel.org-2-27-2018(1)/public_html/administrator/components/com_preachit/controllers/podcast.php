<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.controllerform');

if (Tewebcheck::getJversion() >= 3.0)
{

class PreachitControllerPodcast extends JControllerForm
{
    /**
     * Method to publish a podcast.
     *
     * @param    string    $key    The name of the primary key of the URL variable.
     * @param    string    $urlVar    The name of the URL variable if different from the primary key (sometimes required to avoid router collisions).
     *
     * @return    Boolean    True if successful, false otherwise.
     * @since    1.6
     */
     
    public function publishxml($key = null, $urlVar = 'id')
    {
        JRequest::checktoken() or jexit( 'Invalid Token' );
        $app = JFactory::getApplication();	
        $result = parent::save($key, $urlVar);
        $app->redirect('index.php?option=com_preachit&view=podcasts');
        return $result;
    }
    
    /**
     * Method to cpublish podcast on successful save
     *
     * @param    var        $model the model variable to access data
     * @param    array    $validData    the array carrying relevant data
     *
     * @return 
     * @since    1.6
     */
    
    protected function postSaveHook(JModelLegacy $model, $validData = array())
    {
        $task = $this->getTask();
        if ($task == 'publishxml')
        {
            $app = JFactory::getApplication();    
            $item = $model->getItem(); 
            $id = $item->get('id');
            $abspath = JPATH_SITE;
            require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/publishxml.php');
            $xml = PIHelperpodcast::publishsingle($id);
            $app->redirect('index.php?option=com_preachit&task=podcast.edit&id='.$id);
        }
        return;
    } 


/**
     * Method to save a record.
     *
     * @param    string    $key    The name of the primary key of the URL variable.
     * @param    string    $urlVar    The name of the URL variable if different from the primary key (sometimes required to avoid router collisions).
     *
     * @return    Boolean    True if successful, false otherwise.
     * @since    1.6
     */
    public function save($key = null, $urlVar = 'id')
    {
        $result = parent::save($key, $urlVar);

        // If ok, redirect to the return page.
        if ($result && ($return = $this->getReturnPage())) {
            $this->setRedirect($return);
        }

        return $result;
    }
    
    /**
     * Get the return URL.
     *
     * If a "return" variable has been passed in the request
     *
     * @return    string    The return URL.
     * @since    1.6
     */
    protected function getReturnPage()
    {
        $return = JRequest::getVar('return', null, 'default', 'base64');

        if (empty($return) || !JUri::isInternal(base64_decode($return))) {
            return false;
        }
        else {
            return base64_decode($return);
        }
    }
}
}
else {
    class PreachitControllerPodcast extends JControllerForm
{
    /**
     * Method to publish a podcast.
     *
     * @param    string    $key    The name of the primary key of the URL variable.
     * @param    string    $urlVar    The name of the URL variable if different from the primary key (sometimes required to avoid router collisions).
     *
     * @return    Boolean    True if successful, false otherwise.
     * @since    1.6
     */
     
    public function publishxml($key = null, $urlVar = 'id')
    {
        JRequest::checktoken() or jexit( 'Invalid Token' );
        $app = JFactory::getApplication();    
        $result = parent::save($key, $urlVar);
        $app->redirect('index.php?option=com_preachit&view=podcasts');
        return $result;
    }
    
    /**
     * Method to cpublish podcast on successful save
     *
     * @param    var        $model the model variable to access data
     * @param    array    $validData    the array carrying relevant data
     *
     * @return 
     * @since    1.6
     */
    
    protected function postSaveHook(JModel &$model, $validData = array())
    {
        $task = $this->getTask();
        if ($task == 'publishxml')
        {
            $app = JFactory::getApplication();    
            $item = $model->getItem(); 
            $id = $item->get('id');
            $abspath = JPATH_SITE;
            require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/publishxml.php');
            $xml = PIHelperpodcast::publishsingle($id);
            $app->redirect('index.php?option=com_preachit&task=podcast.edit&id='.$id);
        }
        return;
    } 


/**
     * Method to save a record.
     *
     * @param    string    $key    The name of the primary key of the URL variable.
     * @param    string    $urlVar    The name of the URL variable if different from the primary key (sometimes required to avoid router collisions).
     *
     * @return    Boolean    True if successful, false otherwise.
     * @since    1.6
     */
    public function save($key = null, $urlVar = 'id')
    {
        $result = parent::save($key, $urlVar);

        // If ok, redirect to the return page.
        if ($result && ($return = $this->getReturnPage())) {
            $this->setRedirect($return);
        }

        return $result;
    }
    
    /**
     * Get the return URL.
     *
     * If a "return" variable has been passed in the request
     *
     * @return    string    The return URL.
     * @since    1.6
     */
    protected function getReturnPage()
    {
        $return = JRequest::getVar('return', null, 'default', 'base64');

        if (empty($return) || !JUri::isInternal(base64_decode($return))) {
            return false;
        }
        else {
            return base64_decode($return);
        }
    }
}
}