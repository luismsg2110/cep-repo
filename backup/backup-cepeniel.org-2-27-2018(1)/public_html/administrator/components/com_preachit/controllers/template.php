<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/admin.php');
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/helpers/temp.php');
jimport('teweb.admin.records');
jimport('teweb.file.functions');
jimport('joomla.application.component.controlleradmin');
class PreachitControllerTemplate extends JControllerAdmin
{

/**
     * Method to save template changes
     *
     * @return    redirect
     */
    
function save()
{
    // check user can access
    JRequest::checktoken() or jexit( 'Invalid Token' );
    $user    = JFactory::getUser();
    if (!$user->authorise('core.admin', 'com_preachit'))
    {Tewebcheck::check403();}
    
    // get details
    $option = JRequest::getCmd('option');
    $app = JFactory::getApplication();	
    $row = JTable::getInstance('Template', 'Table');
    $jtemp = JRequest::getVar('jtemp', array(), 'post', 'array');  
    $row->load($jtemp['id']);
    $row->bind($jtemp);
    $params = JRequest::getVar('params', array(), 'post', 'array');
    $registry = new JRegistry();
    $registry->loadArray($params);
    $row->params = $registry->toString();
    // save details
    if (!$row->store())
    {JError::raiseError(500, $row->getError() );  }
    //save template code if custom
    if ($row->template == 'custom')
    {
        // get table, bind and store data
        $temp =Tewebadmin::getformdetails('Templates'); 
        if (!$temp->store())
        {JError::raiseError(500, $temp->getError() );}
    }	
    if ($this->getTask() == 'apply')
    {$app->enqueueMessage ( JText::_('LIB_TEWEB_MESSAGE_CHANGES_APPLIED'), 'message' );
    $url = 'index.php?option='.$option.'&view=template&template='.$row->id;}
    else {$app->enqueueMessage ( JText::_('LIB_TEWEB_MESSAGE_DETAILS_SAVED'), 'message' );
    $url = 'index.php?option='.$option.'&view=templates';}
    $this->setRedirect($url);
}	

/**
     * Method to redirect on cancel
     *
     * @return    redirect
     */
function cancel()
{
    $url = 'index.php?option=com_preachit&view=templates';
    $this->setRedirect($url);
}			

/**
     * Method to apply changes
     *
     * @return    redirect
     */

function apply()
{
    $this->save();
}	


//###########################################
//			START TEMPLATE MANAGER
//###########################################

/**
     * Method to install template
     *
     * @return    redirect
     */

	function extracttemplate()
	{
		JRequest::checktoken() or jexit( 'Invalid Token' );
		$app = JFactory::getApplication ();
		$option		= JRequest::getVar('option', '', '', 'cmd');
        $type = JRequest::getVar('installtype', 1);

		jimport ( 'joomla.filesystem.folder' );
		jimport ( 'joomla.filesystem.file' );
		jimport ( 'joomla.filesystem.archive' );
		$tmp = JPATH_ROOT . '/tmp/pitemp/';
		$dest = JPATH_ROOT . '/components/com_preachit/templates/';
		$file = JRequest::getVar ( 'install_package', NULL, 'FILES', 'array' );

		if (!$file || !is_uploaded_file ( $file ['tmp_name'])) 
			
			{
				$app->enqueueMessage ( JText::sprintf('LIB_TEWEB_A_TEMPLATE_MANAGER_INSTALL_EXTRACT_MISSING', $file ['name']), 'notice' );
			}
			
		else 
		
			{
				$success = JFile::upload($file ['tmp_name'], $tmp . $file ['name']); 
				$success = JArchive::extract ( $tmp . $file ['name'], $tmp );
				
				if (!$success) 
				{
					$app->enqueueMessage ( JText::sprintf('LIB_TEWEB_A_TEMPLATE_MANAGER_INSTALL_EXTRACT_FAILED', $file ['name']), 'notice' );
				}
			
				// Delete the tmp install directory
				if (JFolder::exists($tmp)) 
				
					{
						$templates = JFolder::folders($tmp);
				
						if (!empty($templates)) 
						
							{
								
								foreach ($templates as $template) 
								
									{
						
												$installtype = '';
												if (file_exists($tmp.$template.'/template.xml'))
												{
												if ($rss = new SimpleXMLElement($tmp.$template.'/template.xml',null,true))
													{
														$installtype = $rss['type'];
													}	
												}									
												
												if ($installtype == 'pitemplate')
													
													{	
                                                        $newcreate = false;
                                                        if (!JFolder::exists($dest.$template))
                                                        {
                                                            $type = 1;
                                                            $newcreate = true;
                                                            if (!JFolder::create($dest.$template))
                                                            {$noerror = false;}
                                                            
                                                        }
                                                        $filepresent = true;
                                                        // replace tmp params and css if update type
                                                        // test all folders and files present
                                                        foreach ($rss->core->folder AS $folder)
                                                        {
                                                            if (!JFolder::exists ($tmp.$template.'/'.$folder))
                                                            {
                                                                $app->enqueueMessage ( JText::_('LIB_TEWEB_A_TEMPLATE_MANAGER_TEMPLATE_FILE_NOT_FOUND').': ' . $folder, 'notice' );
                                                                $filepresent = false;
                                                            }
                                                        }
                                                        foreach ($rss->core->file AS $file)
                                                        {
                                                            if (!JFile::exists ($tmp.$template.'/'.$file))
                                                            {
                                                                $app->enqueueMessage ( JText::_('LIB_TEWEB_A_TEMPLATE_MANAGER_TEMPLATE_FILE_NOT_FOUND').': ' . $file, 'notice' );
                                                                $filepresent = false;
                                                            }
                                                        }
                                                        
                                                        if ($type == 1)
                                                        {
                                                            foreach ($rss->style->folder AS $folder)
                                                            {
                                                                if (!JFolder::exists ($tmp.$template.'/'.$folder))
                                                                {
                                                                    $app->enqueueMessage ( JText::_('LIB_TEWEB_A_TEMPLATE_MANAGER_TEMPLATE_FILE_NOT_FOUND').': ' . $folder, 'notice' );
                                                                    $filepresent = false;
                                                                }
                                                            }
                                                            foreach ($rss->style->file AS $file)
                                                            {
                                                                if (!JFile::exists ($tmp.$template.'/'.$file))
                                                                {
                                                                    $app->enqueueMessage ( JText::_('LIB_TEWEB_A_TEMPLATE_MANAGER_TEMPLATE_FILE_NOT_FOUND').': ' . $file, 'notice' );
                                                                    $filepresent = false;
                                                                }
                                                            }
                                                        }
                                                        
                                                        if (!$filepresent)
                                                        {
                                                            if ($newcreate == true)
                                                            {JFolder::delete($dest.$template);}
                                                            $app->redirect( JURI::base () . 'index.php?option='.$option.'&view=templateinstall');
                                                        }
                                                        
                                                        // having checked files present now copy the relevent files
                                                        
                                                        $noerror = true;
                                                        
                                                        foreach ($rss->core->folder AS $folder)
                                                        {
                                                            if (JFolder::exists ($dest.$template.'/'.$folder))
                                                            {
                                                                JFolder::delete($dest.$template.'/'.$folder);
                                                            }
                                                            if (!JFolder::copy($tmp.$template.'/'.$folder, $dest.$template.'/'.$folder))
                                                            {$noerror = false;}
                                                        }
                                                        foreach ($rss->core->file AS $file)
                                                        {
                                                            if (JFile::exists ($dest.$template.'/'.$file))
                                                            {
                                                                JFile::delete($dest.$template.'/'.$file);
                                                            }
                                                            if (!JFile::copy($tmp.$template.'/'.$file, $dest.$template.'/'.$file))
                                                            {$noerror = false;}
                                                        }
                                                        
                                                        if ($type == 1)
                                                        {
                                                            foreach ($rss->style->folder AS $folder)
                                                            {
                                                                if (JFolder::exists ($dest.$template.'/'.$folder))
                                                                {
                                                                    JFolder::delete($dest.$template.'/'.$folder);
                                                                }
                                                                if (!JFolder::copy($tmp.$template.'/'.$folder, $dest.$template.'/'.$folder))
                                                                {$noerror = false;}
                                                            }
                                                            foreach ($rss->style->file AS $file)
                                                            {
                                                                if (JFile::exists ($dest.$template.'/'.$file))
                                                                {
                                                                    JFile::delete($dest.$template.'/'.$file);
                                                                }
                                                                if (!JFile::copy($tmp.$template.'/'.$file, $dest.$template.'/'.$file))
                                                                {$noerror = false;}
                                                            }
                                                        }
                                                            
                                                        
                                
                                                        if ($noerror !== true) 
                                                            
                                                            {
                                                                $app->enqueueMessage ( JText::_('LIB_TEWEB_A_TEMPLATE_MANAGER_TEMPLATE_COPY_FAILED').': ' . $error, 'notice' );
                                                            }
                                                            
                                                        else 
                                                        
                                                            {
                                                                $record = PIHelpertemp::createtemprecord($dest.$template, $template);
                                                                $app->enqueueMessage ( JText::sprintf('LIB_TEWEB_A_TEMPLATE_MANAGER_INSTALL_EXTRACT_SUCCESS', $file ['name']) );
                                                            }
							
													}
													
												else 
												
													{
														$app->enqueueMessage ( JText::_('LIB_TEWEB_A_TEMPLATE_MANAGER_TEMPLATE').' ' . JText::_('LIB_TEWEB_A_TEMPLATE_MANAGER_NO_XML'), 'notice' );
													}
																		
									}						
						
								$retval = JFolder::delete($tmp);	
							} 
						
						else 
						
							{
								JError::raiseWarning(100, JText::_('LIB_TEWEB_A_TEMPLATE_MANAGER_TEMPLATE_MISSING_FILE'));
								$retval = false;
							}
					} 
					
				else 
				
					{
						JError::raiseWarning(100, JText::_('LIB_TEWEB_A_TEMPLATE_MANAGER_TEMPLATE').' '.JText::_('LIB_TEWEB_A_TEMPLATE_MANAGER_UNINSTALL').': '.JText::_('LIB_TEWEB_A_TEMPLATE_MANAGER_DIR_NOT_EXIST'));
						$retval = false;
					}
			}
			
		$app->redirect( JURI::base () . 'index.php?option='.$option.'&view=templates');
	}
	
}