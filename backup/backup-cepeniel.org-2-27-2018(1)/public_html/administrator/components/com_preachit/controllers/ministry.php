<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/admin.php');
jimport('teweb.admin.records');
jimport('teweb.file.upload');
jimport('teweb.file.urlbuilder');
jimport('teweb.file.temediamanager');
jimport('joomla.application.component.controllerform');

if (Tewebcheck::getJversion() >= 3.0)
{

class PreachitControllerMinistry extends JControllerForm
{
    
/**
     * Method to receive upload request from valums upload script
     *
     * @return JSON string
     */
    
    function uploadfile($key = null, $urlVar = 'id')
    {
        // run checks
        JRequest::checktoken() or jexit( 'Invalid Token' );
        $user    = JFactory::getUser();
        if (!$user->authorise('core.edit', 'com_preachit') && !$user->authorise('core.create', 'com_preachit') && !$user->authorise('core.edit.own', 'com_preachit'))
        {Tewebcheck::check403();}
        
        // get details
        $params = Tewebdetails::getparams('com_preachit');
        $media = JRequest::getInt('media', 'image');
        $folder = JRequest::getInt('folder', '');
        if ($params->get('default_folder_only', 0) == 1)
        {$folder = PIHelperadditional::getdefaultfolders($params, $media);}
        $uploader = new Temediamanager;
        $class= new PIHelperadmin;
        $result = $uploader->upload($params, $folder, $media, $class, '#__pimime');
        if (isset($result["artist"]))
        {
            $result['artistid'] = PIHelperadmin::autofillteacher($result["artist"]);
        }
        if (isset($result["artist"]))
        {
            $result['albumid'] = PIHelperadmin::autofillseries($result["album"]);
        }
        // to pass data through iframe you will need to encode all html tags
        echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
    }
    
/**
     * Method to get ID3 daya for a file - ajax call from media center
     *
     *
     * @return JSON string
     */

     function getId3()
     {
        JRequest::checktoken() or jexit( 'Invalid Token' );
        $uploader = new Temediamanager;
        $result = $uploader->getId3('pifilepath');
        if (isset($result["artist"]))
        {
            $result['artistid'] = PIHelperadmin::autofillteacher($result["artist"]);
        }
        if (isset($result["artist"]))
        {
            $result['albumid'] = PIHelperadmin::autofillseries($result["album"]);
        }
        echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        die();
     }

/**
     * Method to check whether folder can receive uploads - respond to ajax call
     *
     *
     * @return JSON string
     */

     function uploadFoldertest()
     {
        JRequest::checktoken() or jexit( 'Invalid Token' );
        $uploader = new Temediamanager;
        $result = $uploader->uploadFoldertest('Filepath');
        echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        die();
     }
     
/**
     * Method to delete Media through mediamanager
     *
     *
     * @return JSON string
     */
     
     function deleteMedia()
     {
        JRequest::checktoken() or jexit( 'Invalid Token' );
        $uploader = new Temediamanager;
        $result = $uploader->deleteMedia('Filepath');
        echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        die();
     }
     
/**
     * Method to create Folder through mediamanager
     *
     *
     * @return JSON string
     */
     
     function createFolder()
     {
        JRequest::checktoken() or jexit( 'Invalid Token' );
        $uploader = new Temediamanager;
        $result = $uploader->createFolder('Filepath');
        echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        die();
     }

/**
     * Method to cpublish podcast on successful save
     *
     * @param    var        $model the model variable to access data
     * @param    array    $validData    the array carrying relevant data
     *
     * @return 
     * @since    1.6
     */
    
    protected function postSaveHook(JModelLegacy $model, $validData = array())
    {
        // resize images if changed
        $session = JFactory::getSession();
        if ($validDate['id'] == 0)
        {
            $item = $model->getItem();
            $validData['id'] = $item->get('id'); 
        }
        if ($session->get('minimagechange'))
        {
            $abspath    = JPATH_SITE;
            require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/ministryimage.php');
            // delete existing images
            $image = PIHelperminimage::getimagepath($validData['id'], 'small', $validData['ministry_image_lrg']);
            $imagefile = JPATH_SITE.DIRECTORY_SEPARATOR.$image;
            if (file_exists($imagefile))
            {JFILE::delete($imagefile);}
            $image = PIHelperminimage::getimagepath($validData['id'], 'medium', $validData['ministry_image_lrg']);
            $imagefile = JPATH_SITE.DIRECTORY_SEPARATOR.$image;
            if (file_exists($imagefile))
            {JFILE::delete($imagefile);}
            $image = PIHelperminimage::getimagepath($validData['id'], 'large', $validData['ministry_image_lrg']);
            $imagefile = JPATH_SITE.DIRECTORY_SEPARATOR.$image;
            if (file_exists($imagefile))
            {JFILE::delete($imagefile);}
        
            //creat new ones
            if ($validData['ministry_image_lrg'] != '')
            {
                PIHelperminimage::ministryimage($validData['id'], 1, 0, 'small');
                PIHelperminimage::ministryimage($validData['id'], 1, 0, 'medium');
                PIHelperminimage::ministryimage($validData['id'], 1, 0, 'large');
            }
        }
        
        //set all minaccess in all related messages
        $saccess = PIHelperadmin::setminaccess($validData['id'], $validData['access']);
        
        return;
    } 

/**
     * Method to save a record.
     *
     * @param    string    $key    The name of the primary key of the URL variable.
     * @param    string    $urlVar    The name of the URL variable if different from the primary key (sometimes required to avoid router collisions).
     *
     * @return    Boolean    True if successful, false otherwise.
     * @since    1.6
     */
    public function save($key = null, $urlVar = 'id')
    {
        $result = parent::save($key, $urlVar);

        // If ok, redirect to the return page.
        if ($result && ($return = $this->getReturnPage())) {
            $this->setRedirect($return);
        }

        return $result;
    }
    
    /**
     * Get the return URL.
     *
     * If a "return" variable has been passed in the request
     *
     * @return    string    The return URL.
     * @since    1.6
     */
    protected function getReturnPage()
    {
        $return = JRequest::getVar('return', null, 'default', 'base64');

        if (empty($return) || !JUri::isInternal(base64_decode($return))) {
            return false;
        }
        else {
            return base64_decode($return);
        }
    }
}
}
else {
    class PreachitControllerMinistry extends JControllerForm
{
    
/**
     * Method to receive upload request from valums upload script
     *
     * @return JSON string
     */
    
    function uploadfile($key = null, $urlVar = 'id')
    {
        // run checks
        JRequest::checktoken() or jexit( 'Invalid Token' );
        $user    = JFactory::getUser();
        if (!$user->authorise('core.edit', 'com_preachit') && !$user->authorise('core.create', 'com_preachit') && !$user->authorise('core.edit.own', 'com_preachit'))
        {Tewebcheck::check403();}
        
        // get details
        $params = Tewebdetails::getparams('com_preachit');
        $media = JRequest::getInt('media', 'image');
        $folder = JRequest::getInt('folder', '');
        if ($params->get('default_folder_only', 0) == 1)
        {$folder = PIHelperadditional::getdefaultfolders($params, $media);}
        $uploader = new Temediamanager;
        $class= new PIHelperadmin;
        $result = $uploader->upload($params, $folder, $media, $class, '#__pimime');
        if (isset($result["artist"]))
        {
            $result['artistid'] = PIHelperadmin::autofillteacher($result["artist"]);
        }
        if (isset($result["artist"]))
        {
            $result['albumid'] = PIHelperadmin::autofillseries($result["album"]);
        }
        // to pass data through iframe you will need to encode all html tags
        echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
    }
    
/**
     * Method to get ID3 daya for a file - ajax call from media center
     *
     *
     * @return JSON string
     */

     function getId3()
     {
        JRequest::checktoken() or jexit( 'Invalid Token' );
        $uploader = new Temediamanager;
        $result = $uploader->getId3('pifilepath');
        if (isset($result["artist"]))
        {
            $result['artistid'] = PIHelperadmin::autofillteacher($result["artist"]);
        }
        if (isset($result["artist"]))
        {
            $result['albumid'] = PIHelperadmin::autofillseries($result["album"]);
        }
        echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        die();
     }

/**
     * Method to check whether folder can receive uploads - respond to ajax call
     *
     *
     * @return JSON string
     */

     function uploadFoldertest()
     {
        JRequest::checktoken() or jexit( 'Invalid Token' );
        $uploader = new Temediamanager;
        $result = $uploader->uploadFoldertest('Filepath');
        echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        die();
     }
     
/**
     * Method to delete Media through mediamanager
     *
     *
     * @return JSON string
     */
     
     function deleteMedia()
     {
        JRequest::checktoken() or jexit( 'Invalid Token' );
        $uploader = new Temediamanager;
        $result = $uploader->deleteMedia('Filepath');
        echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        die();
     }
     
/**
     * Method to create Folder through mediamanager
     *
     *
     * @return JSON string
     */
     
     function createFolder()
     {
        JRequest::checktoken() or jexit( 'Invalid Token' );
        $uploader = new Temediamanager;
        $result = $uploader->createFolder('Filepath');
        echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        die();
     }

/**
     * Method to cpublish podcast on successful save
     *
     * @param    var        $model the model variable to access data
     * @param    array    $validData    the array carrying relevant data
     *
     * @return 
     * @since    1.6
     */
    
    protected function postSaveHook(JModel &$model, $validData = array())
    {
        // resize images if changed
        $session = JFactory::getSession();
        if ($validDate['id'] == 0)
        {
            $item = $model->getItem();
            $validData['id'] = $item->get('id'); 
        }
        if ($session->get('minimagechange'))
        {
            $abspath    = JPATH_SITE;
            require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/ministryimage.php');
            // delete existing images
            $image = PIHelperminimage::getimagepath($validData['id'], 'small', $validData['ministry_image_lrg']);
            $imagefile = JPATH_SITE.DIRECTORY_SEPARATOR.$image;
            if (file_exists($imagefile))
            {JFILE::delete($imagefile);}
            $image = PIHelperminimage::getimagepath($validData['id'], 'medium', $validData['ministry_image_lrg']);
            $imagefile = JPATH_SITE.DIRECTORY_SEPARATOR.$image;
            if (file_exists($imagefile))
            {JFILE::delete($imagefile);}
            $image = PIHelperminimage::getimagepath($validData['id'], 'large', $validData['ministry_image_lrg']);
            $imagefile = JPATH_SITE.DIRECTORY_SEPARATOR.$image;
            if (file_exists($imagefile))
            {JFILE::delete($imagefile);}
        
            //creat new ones
            if ($validData['ministry_image_lrg'] != '')
            {
                PIHelperminimage::ministryimage($validData['id'], 1, 0, 'small');
                PIHelperminimage::ministryimage($validData['id'], 1, 0, 'medium');
                PIHelperminimage::ministryimage($validData['id'], 1, 0, 'large');
            }
        }
        
        //set all minaccess in all related messages
        $saccess = PIHelperadmin::setminaccess($validData['id'], $validData['access']);
        
        return;
    } 

/**
     * Method to save a record.
     *
     * @param    string    $key    The name of the primary key of the URL variable.
     * @param    string    $urlVar    The name of the URL variable if different from the primary key (sometimes required to avoid router collisions).
     *
     * @return    Boolean    True if successful, false otherwise.
     * @since    1.6
     */
    public function save($key = null, $urlVar = 'id')
    {
        $result = parent::save($key, $urlVar);

        // If ok, redirect to the return page.
        if ($result && ($return = $this->getReturnPage())) {
            $this->setRedirect($return);
        }

        return $result;
    }
    
    /**
     * Get the return URL.
     *
     * If a "return" variable has been passed in the request
     *
     * @return    string    The return URL.
     * @since    1.6
     */
    protected function getReturnPage()
    {
        $return = JRequest::getVar('return', null, 'default', 'base64');

        if (empty($return) || !JUri::isInternal(base64_decode($return))) {
            return false;
        }
        else {
            return base64_decode($return);
        }
    }
}
}
