<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/admin.php');
jimport('teweb.admin.records');
jimport('joomla.application.component.controlleradmin');
class PreachitControllerstudies extends JControllerAdmin
{ 
/**
     * Constructor.
     *
     * @param   array  $config    An optional associative array of configuration settings.

     * @return  ContentControllerArticles
     * @see     JController
     * @since   1.6
     */
    public function __construct($config = array())
    {
        parent::__construct($config);

        $this->registerTask('unfeatured',    'featured');
    }
 /**
     * Proxy for getModel.
     */
    public function &getModel($name = 'study', $prefix = 'PreachitModel', $config = array('ignore_request' => true))
    {
        $model = parent::getModel($name, $prefix, $config);
        return $model;
    }

function resetall()
{
JRequest::checktoken() or jexit( 'Invalid Token' );
$user    = JFactory::getUser();
if (!$user->authorise('core.admin', 'com_preachit'))
{Tewebcheck::check403();}
$app = JFactory::getApplication();
$option = JRequest::getCmd('option');
$msg = Tewebadmin::resethits('#__pistudies');		
$app->Redirect('index.php?option=' . $option . '&view=studies');
}

function resetdownloads()
{
JRequest::checktoken() or jexit( 'Invalid Token' );
$user    = JFactory::getUser();
if (!$user->authorise('core.admin', 'com_preachit'))
{Tewebcheck::check403();}
$app = JFactory::getApplication();
$option = JRequest::getCmd('option');
$msg = Tewebadmin::resetdownloads('#__pistudies');	
$app->Redirect('index.php?option=' . $option . '&view=studies');
}

/**
     * Method to toggle the featured setting of a list of articles.
     *
     * @return    void
     * @since    1.6
     */
    function featured()
    {
        // Check for request forgeries
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // Initialise variables.
        $user    = JFactory::getUser();
        $ids    = JRequest::getVar('cid', array(), '', 'array');
        $values    = array('featured' => 1, 'unfeatured' => 0);
        $task    = $this->getTask();
        $value    = JArrayHelper::getValue($values, $task, 0, 'int');

        // Access checks.
        foreach ($ids as $i => $id)
        {
            if (!$user->authorise('core.edit.state', 'com_preachit')) {
                // Prune items that you can't change.
                unset($ids[$i]);
                JError::raiseNotice(403, JText::_('JLIB_APPLICATION_ERROR_EDITSTATE_NOT_PERMITTED'));
            }
        }

        if (empty($ids)) {
            JError::raiseWarning(500, JText::_('JERROR_NO_ITEMS_SELECTED'));
        }
        else {
            // Get the model.
            $model = $this->getModel();

            // Publish the items.
            if (!$model->featured($ids, $value)) {
                JError::raiseWarning(500, $model->getError());
            }
        }

        $this->setRedirect('index.php?option=com_preachit&view=studies');
    }

}
