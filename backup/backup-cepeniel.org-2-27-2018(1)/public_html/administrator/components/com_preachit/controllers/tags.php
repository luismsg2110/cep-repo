<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('teweb.admin.records');
jimport('teweb.details.tags');
jimport('joomla.application.component.controlleradmin');
class PreachitControllerTags extends JControllerAdmin
{
 /**
     * Proxy for getModel.
     */
    public function &getModel($name = 'tag', $prefix = 'PreachitModel')
    {
        $model = parent::getModel($name, $prefix, array('ignore_request' => true));
        return $model;
    }
     
 /**
     * Method to remove tag
     *
     * @return    redirect
     */
    
    function remove()
    {
        JRequest::checkToken() or jexit( 'Invalid Token' );
        $user    = JFactory::getUser();
        if (!$user->authorise('core.delete', 'com_preachit'))
        {Tewebcheck::check403();}
        $option = JRequest::getCmd('option');
        $msg = Tewebtags::deletetag('#__pistudies');
        $this->setRedirect('index.php?option=' . $option . '&view=tags');
    }
}
