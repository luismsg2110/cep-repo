<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/admin.php');
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/helpers/temp.php');
jimport('teweb.admin.records');
jimport('teweb.file.functions');
jimport('joomla.application.component.controlleradmin');
class PreachitControllerTemplates extends JControllerAdmin
{
    
/**
     * Proxy for getModel.
     */
    public function &getModel($name = 'template', $prefix = 'PreachitModel')
    {
        $model = parent::getModel($name, $prefix, array('ignore_request' => true));
        return $model;
    }

/**
     * Method to set default template
     *
     * @return    redirect
     */

function deftemp()
{
JRequest::checkToken() or jexit( 'Invalid Token' );
$user    = JFactory::getUser();
if (!$user->authorise('core.admin', 'com_preachit'))
{Tewebcheck::check403();}
$app = JFactory::getApplication();
$db = JFactory::getDBO();
$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
$id = $cid[0];
$query = "SELECT id FROM #__pitemplate"; 
$db->setQuery($query);
$temps = $db->loadObjectList();
    
    foreach ($temps as $temp)
    
        {
            if ($temp->id != $id)
            {$db->setQuery ("UPDATE #__pitemplate SET def = 0 WHERE id = '{$temp->id}' ;"); 
                    $db->query();}
            else
            {$db->setQuery ("UPDATE #__pitemplate SET def = 1 WHERE id = '{$temp->id}' ;"); 
                    $db->query();}
        }


$this->setRedirect('index.php?option=com_preachit&view=templates');	
}
/**
     * Method to remove template
     *
     * @return    redirect
     */

function remove()
{
	JRequest::checktoken() or jexit( 'Invalid Token' );
    $user    = JFactory::getUser();
    jimport('joomla.filesystem.folder');
    if (!$user->authorise('core.admin', 'com_preachit'))
    {Tewebcheck::check403();}
	$app = JFactory::getApplication();
	$option = JRequest::getCmd('option');		
	$cid = JRequest::getVar('cid', array());
	$i = 0;
	
	// get default template to prevent it being deleted

	$db = JFactory::getDBO();
	$query = "
  		SELECT ".$db->quoteName('id')."
    	FROM ".$db->quoteName('#__pitemplate')."
    	WHERE ".$db->quoteName('def')." = ".$db->quote(1).";
  			";
		$db->setQuery($query);
	$default_template = $db->loadResult();
	
	foreach ($cid as $temp)
	
		{
			if ($temp == $default_template)	
			{
				$app->enqueueMessage ( JText::_('LIB_TEWEB_TEMPLATE_MANAGER_NOT_REMOVE_DEFAULT').': ' . $error, 'warning' );
			}
			else 
			{		
                $row = & JTable::getInstance('Template', 'Table');
                $row->load($temp);
                $title = $row->title;
                if ($row->client_id == 0)
				{    
                    $path = JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_preachit'.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.$row->template;
                    if (JFolder::exists($path))
                    {
                        if (!JFolder::delete($path))
                        {
                            $noerror = false;
                        }
                    }
                }
                if (!$row->delete($temp))
                {JError::raiseError(500, $row->getError() );}
				$app->enqueueMessage ( $title.' '.JText::_('LIB_TEWEB_TEMPLATE_MANAGER_TEMP_DELETED'), 'message');
			}
		}
	
	$app->redirect('index.php?option=' . $option . '&view=templates');
	
}

/**
     * Method to save copy templates
     *
     * @return    redirect
     */

function copy()
{
	JRequest::checktoken() or jexit( 'Invalid Token' );
    $user    = JFactory::getUser();
    if (!$user->authorise('core.admin', 'com_preachit'))
    {Tewebcheck::check403();}
    $db = JFactory::getDBO();
	$app = JFactory::getApplication();
	$option = JRequest::getCmd('option');		
	$cid = JRequest::getVar('cid', array());
	jimport ( 'joomla.filesystem.folder' );
	$no = '';	
	
	foreach ($cid as $temp)
	
		{
            $row = & JTable::getInstance('Template', 'Table');
            $row->load($temp);
            
            $newtemp = PIHelpertemp::initialisetemp();
            $newtemp->template = $row->template;
            $newtemp->params = PIHelpertemp::tempparams(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_preachit'.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.$row->template);
            $newtemp->client_id = $row->id;
			$count = 1;
            $title = $row->title;
			while ($count > 0)
            {
                    $query='SELECT COUNT(*) FROM #__pitemplate WHERE title  = '.$db->quote($title);
                    $db->setQuery($query);
                    $count =  intval($db->loadResult());
                    if ($count > 0)
                    {
                        // get number from end of folder name if present
                    
                        $parts = explode( '_', $title);
                        $num = count($parts);
                        if (is_array($parts))
                            {$no = $parts[$num - 1];
                            if (is_numeric($no))
                                {
                                    $no = $no + 1;
                                    $parts[$num - 1] = $no;
                                    $title = implode( '_', $parts );
                                }
                            else 
                                {
                                    $title = $title.'_1';
                                    $no = 1;
                                }
                        }
                        else 
                        {
                            $title = $title.'_1';
                            $no = 1;
                        }
                    }
            }
              $newtemp->title = $title;
            if (!$newtemp->store())
            {JError::raiseError(500, $temp->getError() );}       
    }
	$app->enqueueMessage ( JText::sprintf('LIB_TEWEB_TEMPLATE_MANAGER_TEMPLATE_COPIED'), 'message');
			
	$app->redirect('index.php?option=' . $option . '&view=templates');
}

/**
     * Method rediect to add template screen
     *
     * @return    redirect
     */						

function addPITemplate()
{
	JRequest::checktoken() or jexit( 'Invalid Token' );
    $user    = JFactory::getUser();
    if (!$user->authorise('core.admin', 'com_preachit'))
    {Tewebcheck::check403();}
    $url = 'index.php?option=com_preachit&view=templateinstall';
    $this->setRedirect($url);
}
}
