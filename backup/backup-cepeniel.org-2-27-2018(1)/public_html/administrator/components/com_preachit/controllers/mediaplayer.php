<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('teweb.admin.records');
jimport('joomla.application.component.controllerform');
class PreachitControllerMediaplayer extends JControllerForm
{
/**
     * Method to save a record.
     *
     * @param    string    $key    The name of the primary key of the URL variable.
     * @param    string    $urlVar    The name of the URL variable if different from the primary key (sometimes required to avoid router collisions).
     *
     * @return    Boolean    True if successful, false otherwise.
     * @since    1.6
     */
    public function save($key = null, $urlVar = 'id')
    {
        $result = parent::save($key, $urlVar);

        // If ok, redirect to the return page.
        if ($result && ($return = $this->getReturnPage())) {
            $this->setRedirect($return);
        }

        return $result;
    }
    
    /**
     * Get the return URL.
     *
     * If a "return" variable has been passed in the request
     *
     * @return    string    The return URL.
     * @since    1.6
     */
    protected function getReturnPage()
    {
        $return = JRequest::getVar('return', null, 'default', 'base64');

        if (empty($return) || !JUri::isInternal(base64_decode($return))) {
            return false;
        }
        else {
            return base64_decode($return);
        }
    }
}
