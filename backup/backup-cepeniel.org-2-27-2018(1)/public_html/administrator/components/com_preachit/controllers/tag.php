<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('teweb.admin.records');
jimport('teweb.details.tags');
jimport('joomla.application.component.controllerform');
class PreachitControllerTag extends JControllerForm
{

    /**
     * Method to save tag changes
     *
     * @return    redirect
     */
    
    function save()
    {
        // check user can access
        JRequest::checktoken() or jexit( 'Invalid Token' );
        $user    = JFactory::getUser();
        if (!$user->authorise('core.edit', 'com_preachit') && !$user->authorise('core.create', 'com_preachit') && !$user->authorise('core.edit.own', 'com_preachit'))
        {Tewebcheck::check403();}

        // get details
        $app = JFactory::getApplication();
        $option = JRequest::getCmd('option');
        // get tag details
        $original = JRequest::getVar('original', '');
        $original = JString::trim($original);
        $newtag = JRequest::getVar('tag', '');
        $newtag = JString::trim($newtag);
        
        // change tag
        $msg = Tewebtags::changetag($original, $newtag, '#__pistudies');
        if ($this->getTask() == 'apply')
        {
            $url = 'index.php?option=' . $option . '&view=tag&tagstring=' . $newtag;
        }
        else {
            $url = 'index.php?option=' . $option . '&view=tags';
        }
            $this->setRedirect($url);
    }
    
    /**
     * Method to redirect on cancel
     *
     * @return    redirect
     */
     
function cancel()
{
    $url = 'index.php?option=com_preachit&view=tags';
    $this->setRedirect($url);
}

}
