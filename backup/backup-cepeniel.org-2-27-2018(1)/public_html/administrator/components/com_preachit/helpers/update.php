<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
JTable::addIncludePath(JPATH_ADMINISTRATOR.
DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_preachit'.DIRECTORY_SEPARATOR.'tables');
@error_reporting(E_ERROR | E_WARNING | E_PARSE);

// Help get past php timeouts if we made it that far
// Joomla 1.5 installer can be very slow and this helps avoid timeouts
@set_time_limit(300);
$kn_maxTime = @ini_get('max_execution_time');

$maxMem = trim(@ini_get('memory_limit'));
if ($maxMem) {
    $unit = strtolower($maxMem{strlen($maxMem) - 1});
    switch($unit) {
        case 'g':
            $maxMem    *=    1024;
        case 'm':
            $maxMem    *=    1024;
        case 'k':
            $maxMem    *=    1024;
    }
    if ($maxMem < 16000000) {
        @ini_set('memory_limit', '16M');
    }
    if ($maxMem < 32000000) {
        @ini_set('memory_limit', '32M');
    }
    if ($maxMem < 48000000) {
        @ini_set('memory_limit', '48M');
    }
}
ignore_user_abort(true);

$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/admin.php');
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/lib/preachit.version.php');
require_once($abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/helpers/temp.php');
jimport('teweb.admin.records');

JTable::addIncludePath(JPATH_ADMINISTRATOR.
DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_preachit'.DIRECTORY_SEPARATOR.'tables');

class PIHelperupdate
{
 
/**
     * Get the current table version
     * return int
    */

public static function tableversion()
{
    $version = PIVersion::versionXML();

    return $version;
}

/**
     * Method to update pistudies table
     * return boolean
    */

public static function updatestudytable()
{
    
$db    = JFactory::getDBO();
$tn = '#__pistudies';
    $fields = $db->getTableColumns(  $tn  );   
    
    // drop unwanted columns
    
    $dropcols = array("image_folder", "image_foldermed", "imagesm", "imagemed", "video", "audio", "text", "podcast_audio", "podcast_video");
    
    foreach ($dropcols AS $cols)
    {
        $entry = false;
        $entry = isset( $fields[$cols] );
        if ($entry) {$db->setQuery ("ALTER TABLE #__pistudies DROP ".$cols.";");
            $db->query();}
    }    
    
    // change minaccess column to text
    
    if (trim($fields['minaccess']) == 'int')
    {
        $db->setQuery("ALTER TABLE #__pistudies CHANGE minaccess minaccess TEXT NOT NULL;");
        $db->query();
    } 
    
    // get all messages   
    
    $query = "SELECT id FROM #__pistudies";
    $db->setQuery($query);
    $rows = $db->loadObjectList();
    
    foreach ($rows AS $row)
    {
        $study = JTable::getInstance('Studies', 'Table');  
        $study->load($row->id);
        $study->ministry = Tewebadmin::jsonentry($study->ministry);
        $study->teacher = Tewebadmin::jsonentry($study->teacher);
        if (!$study->store())
        {JError::raiseError(500, $row->getError() );} 
    }
    $entry = false;
    $entry    = isset( $fields['checked_out'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pistudies ADD COLUMN checked_out INT(10) NOT NULL DEFAULT '0';");
            $db->query();}
            else {
                $db->setQuery ("ALTER TABLE #__pistudies CHANGE checked_out checked_out INT(10) NOT NULL DEFAULT '0';");
                $db->query(); 
            }
    $entry = false;
    $entry    = isset( $fields['checked_out_time'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pistudies ADD COLUMN checked_out_time DATETIME NOT NULL;");
            $db->query();}
    $entry = false;
    $entry    = isset( $fields['featured'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pistudies ADD COLUMN featured TINYINT(3) NOT NULL DEFAULT '0';");
            $db->query();}
    $entry = false;
    $entry    = isset( $fields['vclip1_type'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pistudies ADD COLUMN vclip1_type INT(11) NOT NULL;");
            $db->query();}
    $entry = false;
    $entry    = isset( $fields['vclip1_folder'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pistudies ADD COLUMN vclip1_folder INT(11) NOT NULL;");
            $db->query();}
    $entry = false;
    $entry    = isset( $fields['vclip1_link'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pistudies ADD COLUMN vclip1_link VARCHAR(250) NOT NULL;");
            $db->query();}
    $entry = false;
    $entry    = isset( $fields['vclip2_type'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pistudies ADD COLUMN vclip2_type INT(11) NOT NULL;");
            $db->query();}
    $entry = false;
    $entry    = isset( $fields['vclip2_folder'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pistudies ADD COLUMN vclip2_folder INT(11) NOT NULL;");
            $db->query();}
    $entry = false;
    $entry    = isset( $fields['vclip2_link'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pistudies ADD COLUMN vclip2_link VARCHAR(250) NOT NULL;");
            $db->query();}
    $entry = false;
    $entry    = isset( $fields['vclip3_type'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pistudies ADD COLUMN vclip3_type INT(11) NOT NULL;");
            $db->query();}
    $entry = false;
    $entry    = isset( $fields['vclip3_folder'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pistudies ADD COLUMN vclip3_folder INT(11) NOT NULL;");
            $db->query();}
    $entry = false;
    $entry    = isset( $fields['vclip3_link'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pistudies ADD COLUMN vclip3_link VARCHAR(250) NOT NULL;");
            $db->query();}
    $entry = false;
    $entry    = isset( $fields['newTags'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pistudies ADD COLUMN newTags VARCHAR (250) NOT NULL;");
            $db->query();}
    $db->setQuery ("ALTER TABLE #__pistudies CHANGE language language CHAR(7) NOT NULL DEFAULT '*';");
                $db->query(); 
    
    $db->setQuery ("ALTER TABLE #__pistudies CHANGE ministry ministry TEXT NOT NULL;");
                $db->query(); 

    return true;
}

/**
     * Method to update piseries table
     * return boolean
    */

public static function updateseriestable()
{

    $db    = JFactory::getDBO();
    $sn = '#__piseries';
    $fields = $db->getTableColumns( $sn );
    
    // drop unwanted columns
    
    $dropcols = array("image_folder", "series_image_sm");
    
    foreach ($dropcols AS $cols)
    {
        $entry = false;
        $entry = isset( $fields[$cols] );
        if ($entry) {$db->setQuery ("ALTER TABLE #__piseries DROP ".$cols.";");
            $db->query();}
    }   
    
    $entry = false;
    $entry    = isset( $fields['checked_out'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__piseries ADD COLUMN checked_out INT(10) NOT NULL DEFAULT '0';");
            $db->query();}
            else {
                $db->setQuery ("ALTER TABLE #__piseries CHANGE checked_out checked_out INT(10) NOT NULL DEFAULT '0';");
                $db->query(); 
            }
    $entry = false;
    $entry    = isset( $fields['checked_out_time'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__piseries ADD COLUMN checked_out_time DATETIME NOT NULL;");
            $db->query();}
    $entry = false;
    $entry    = isset( $fields['featured'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__piseries ADD COLUMN featured TINYINT(3) NOT NULL DEFAULT '0';");
            $db->query();}
    $db->setQuery ("ALTER TABLE #__piseries CHANGE language language CHAR(7) NOT NULL DEFAULT '*';");
                $db->query(); 
    
    // get all messages   
    
    $query = "SELECT id FROM #__piseries";
    $db->setQuery($query);
    $rows = $db->loadObjectList();
    foreach ($rows AS $row)
    {
        $series = JTable::getInstance('Series', 'Table');  
        $series->load($row->id);
        $series->ministry = Tewebadmin::jsonentry($series->ministry);
        if (!$series->store())
        {JError::raiseError(500, $row->getError() );} 
    }   
    
    $db->setQuery ("ALTER TABLE #__piseries CHANGE ministry ministry TEXT NOT NULL;");
                $db->query(); 

    return true;

}

/**
     * Method to update piministry table
     * return boolean
    */

public static function updateministrytable()
{

    $db    = JFactory::getDBO();
    $mn = '#__piministry';
    $fields = $db->getTableColumns( $mn );
    
    // drop unwanted columns
    
    $dropcols = array("image_folder", "ministry_image_sm");
    
    foreach ($dropcols AS $cols)
    {
        $entry = false;
        $entry = isset( $fields[$cols] );
        if ($entry) {$db->setQuery ("ALTER TABLE #__piministry DROP ".$cols.";");
            $db->query();}
    }      
    
    $entry = false;
    $entry    = isset( $fields['checked_out'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__piministry ADD COLUMN checked_out INT(10) NOT NULL DEFAULT '0';");
            $db->query();}
            else {
                $db->setQuery ("ALTER TABLE #__piministry CHANGE checked_out checked_out INT(10) NOT NULL DEFAULT '0';");
                $db->query(); 
            }
    $entry = false;
    $entry    = isset( $fields['checked_out_time'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__piministry ADD COLUMN checked_out_time DATETIME NOT NULL;");
            $db->query();}
    $entry = false;
    $entry    = isset( $fields['featured'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__piministry ADD COLUMN featured TINYINT(3) NOT NULL DEFAULT '0';");
            $db->query();}
     $db->setQuery ("ALTER TABLE #__piministry CHANGE language language CHAR(7) NOT NULL DEFAULT '*';");
                $db->query();        

        return true;
        
}

/**
     * Method to update piteachers table
     * return boolean
    */

public static function updateteachertable()
{

    $db    = JFactory::getDBO();
    $ten = '#__piteachers';
    $fields = $db->getTableColumns( $ten );
    
    // drop unwanted columns
    
    $dropcols = array("image_folder", "teacher_image_sm");
    
    foreach ($dropcols AS $cols)
    {
        $entry = false;
        $entry = isset( $fields[$cols] );
        if ($entry) {$db->setQuery ("ALTER TABLE #__piteachers DROP ".$cols.";");
            $db->query();}
    }    
    
    $entry = false;
    $entry    = isset( $fields['checked_out'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__piteachers ADD COLUMN checked_out INT(10) NOT NULL DEFAULT '0';");
            $db->query();}
            else {
                $db->setQuery ("ALTER TABLE #__piteachers CHANGE checked_out checked_out INT(10) NOT NULL DEFAULT '0';");
                $db->query(); 
            }
    $entry = false;
    $entry    = isset( $fields['checked_out_time'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__piteachers ADD COLUMN checked_out_time DATETIME NOT NULL");
            $db->query();}
    $entry = false;
    $entry    = isset( $fields['featured'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__piteachers ADD COLUMN featured TINYINT(3) NOT NULL DEFAULT '0';");
            $db->query();}
    $db->setQuery ("ALTER TABLE #__piteachers CHANGE language language CHAR(7) NOT NULL DEFAULT '*';");
                $db->query(); 

        return true;    
        
}

/**
     * Method to update pipodcast table
     * return boolean
    */

public static function updatepodcasttable()
{

    $db    = JFactory::getDBO();
    $pn = '#__pipodcast';
    $fields = $db->getTableColumns( $pn );
    $itunesdesc = false;
    if (trim($fields['itunestitle']) == 'int')
    {
        $db->setQuery("ALTER TABLE #__pipodcast CHANGE itunestitle itunestitle TEXT NOT NULL;");
        $db->query();
        $itunesdesc = true;
    }
    if (trim($fields['itunesdesc']) == 'int')
    {
        $db->setQuery("ALTER TABLE #__pipodcast CHANGE itunesdesc itunesdesc TEXT NOT NULL;");
        $db->query();
        $itunesdesc = true;
    }
    if (trim($fields['itunessub']) == 'int')
    {
        $db->setQuery("ALTER TABLE #__pipodcast CHANGE itunessub itunessub TEXT NOT NULL;");
        $db->query();
        $itunesdesc = true;
    }
    $entry = false;
    $entry    = isset( $fields['poddate'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pipodcast ADD COLUMN poddate TINYINT(3) NOT NULL;");
            $db->query();}
    $entry = false;
    $entry    = isset( $fields['checked_out'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pipodcast ADD COLUMN checked_out INT(10) NOT NULL DEFAULT '0';");
            $db->query();}
            else {
                $db->setQuery ("ALTER TABLE #__pipodcast CHANGE checked_out checked_out INT(10) NOT NULL DEFAULT '0';");
                $db->query(); 
            }
    $entry = false;
    $entry    = isset( $fields['checked_out_time'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pipodcast ADD COLUMN checked_out_time DATETIME NOT NULL;");
            $db->query();}
    
    // get all messages   
    
    $query = "SELECT id FROM #__pipodcast";
    $db->setQuery($query);
    $rows = $db->loadObjectList();
    
    foreach ($rows AS $row)
    {
        $pod = JTable::getInstance('Podcast', 'Table');  
        $pod->load($row->id);
        $pod->ministry_list = Tewebadmin::jsonentry($pod->ministry_list);
        $pod->teacher_list = Tewebadmin::jsonentry($pod->teacher_list);
        $pod->ministry_list = Tewebadmin::jsonentry($pod->ministry_list);
        $pod->teacher_list = Tewebadmin::jsonentry($pod->teacher_list);
        if ($itunesdesc == true)
        {
            $pod->itunestitle = '[title]';
            $pod->itunessub = '[description]';
            $pod->itunesdesc = '[scripture] - [description]';
        }
        if (!$pod->store())
        {JError::raiseError(500, $pod->getError() );} 
    }
        
        return true;
        
}

/**
     * Method to update pitemplates table (used by the custom template)
     * return boolean
    */

public static function updatetemptable()
{

    $db    = JFactory::getDBO();
        
    $temp = '#__pitemplates';
    $fields = $db->getTableColumns( $temp );
    
                    
        return true;
}

/**
     * Method to update piadmin table
     * return boolean
    */

public static function updateadmintable()
{

    $db    = JFactory::getDBO();
            
    $table = '#__pibckadmin';
    $fields = $db->getTableColumns ( $table );
    $entry = false;
    $entry    = isset( $fields['versioncheck'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pibckadmin ADD COLUMN versioncheck DATETIME NOT NULL;");
            $db->query();}
    $entry = false;
    $entry    = isset( $fields['curversion'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pibckadmin ADD COLUMN curversion TEXT NOT NULL;");
            $db->query();}
    
    $db->setQuery ("UPDATE #__pibckadmin SET versioncheck = '0000-00-00 00:00:00' WHERE id = '1' ;");
        $db->query();
        
    // drop unwanted columns
    
    $dropcols = array("studylist", "getupdate", "image_folder", "image_foldermed", "image_folderlrg", "notes_folder", "notes", "add_downloadvid", "downloadvid_folder", "default_template", "uploadtype", "checkin", "uploadfile", "access", "audpurchase", "audpurchase_folder", "vidpurchase", "vidpurchase_folder", "autopodcast", "droptables", "series", "ministry", "video", "video_type", "audio_type", "video_download", "audio", "audio_download", "comments", "text", "teacher", "audio_folder","video_folder", "mfhide", "sfhide", "tfhide", "mfupsel", "sfupsel", "tfupsel", "msimsmw", "msimsmh", "msimmedw", "msimmedh", "msimlrgw", "msimlrgh", "teaimsmw", "teaimsmh", "teaimmedw", "teaimmedh", "teaimlrgw", "teaimlrgh", "seimsmw", "serimsmh", "serimmedw", "serimmedh", "serimlrgw", "serimlrgh", "minimsmw", "minimsmh", "minimmedw", "minimmedh", "minimlrgw", "minimlrgh", "imagequal", "imageresize", "language", "multilanguage", "prefixm", "perfixi", "upload_selector", "upload_folder", "slides", "slides_folder", "slides_type", "cookieconsent");
    
    foreach ($dropcols AS $cols)
    {
        $entry = false;
        $entry = isset( $fields[$cols] );
        if ($entry) {$db->setQuery ("ALTER TABLE #__pibckadmin DROP ".$cols.";");
            $db->query();}
    }

    return true;
    
}

/**
     * Method to update pimediaplayers table
     * return boolean
    */

public static function updatemediaplayertable()
{

    $db    = JFactory::getDBO();
        
    $mp = '#__pimediaplayers';
    $fields = $db->getTableColumns ( $mp  );
    
    $db->setQuery ("ALTER TABLE #__pimediaplayers CHANGE vers vers DECIMAL (5,2) NOT NULL"); 
    $db->query(); 
    
    $query = "SELECT * FROM #__pimediaplayers";
    $db->setQuery($query);
    $meplays = $db->loadObjectList();
            
    foreach ($meplays as $meplay)
    {
        if ($meplay->vers < 3.66)
        {
            $meplay->playerurl = str_replace('components/com_preachit/assets', 'media/preachit', $meplay->playerurl);
            $db->setQuery ("UPDATE #__pimediaplayers SET playerurl = '{$meplay->playerurl}' WHERE id = '{$meplay->id}' ;"); 
            $db->query(); 
            $meplay->playerscript = str_replace('components/com_preachit/assets', 'media/preachit', $meplay->playerscript);
            $db->setQuery ("UPDATE #__pimediaplayers SET playerscript = '{$meplay->playerscript}' WHERE id = '{$meplay->id}' ;"); 
            $db->query(); 
        }
        $version = 3.66;    
        $db->setQuery ("UPDATE #__pimediaplayers SET vers = '{$version}' WHERE id = '{$meplay->id}' ;");
        $db->query(); 
    }
    
    $entry = false;
    $entry    = isset( $fields['checked_out'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pimediaplayers ADD COLUMN checked_out INT(10) NOT NULL DEFAULT '0';");
            $db->query();}
            else {
                $db->setQuery ("ALTER TABLE #__pimediaplayers CHANGE checked_out checked_out INT(10) NOT NULL DEFAULT '0';");
                $db->query(); 
            }
    $entry = false;
    $entry    = isset( $fields['checked_out_time'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pimediaplayers ADD COLUMN checked_out_time DATETIME NOT NULL;");
            $db->query();}


            
        return true;
}

/**
     * Method to update pishare table
     * return boolean
    */

public static function updatesharetable()
{

    $db    = JFactory::getDBO();
    $tables = $db->getTableList();
    $prefix = $db->getPrefix();
    $tablecheck = $prefix.'pishare';
    $mp = '#__pishare';
    $fields = $db->getTableColumns ( $mp  );
    
    $entry = false;
    $entry    = isset( $fields['checked_out'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pishare ADD COLUMN checked_out INT(10) NOT NULL DEFAULT '0';");
            $db->query();}
            else {
                $db->setQuery ("ALTER TABLE #__pishare CHANGE checked_out checked_out INT(10) NOT NULL DEFAULT '0';");
                $db->query(); 
            }
    $entry = false;
    $entry    = isset( $fields['checked_out_time'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pishare ADD COLUMN checked_out_time DATETIME NOT NULL;");
            $db->query();}

        return true;
}

/**
     * Method to update pitemplates table
     * return boolean
    */

public static function updatetemplatetable()
{

    $db    = JFactory::getDBO();
    $tables = $db->getTableList();
    $prefix = $db->getPrefix();
    $tablecheck = $prefix.'pitemplate';
    $mp = '#__pitemplate';
    $fields = $db->getTableColumns ( $mp  );
    if (!in_array($tablecheck, $tables))
    {    
        // set up table
        $db->setQuery ( "CREATE TABLE IF NOT EXISTS `#__pitemplate` LIKE `#__template_styles`;"
                                );
        $db->query();
        $db->setQuery("ALTER TABLE #__pitemplate DROP home;");
        $db->query();  
        $db->setQuery ("ALTER TABLE #__pitemplate ADD COLUMN def TINYINT(3) NOT NULL;");
        $db->query();
        $db->setQuery ("ALTER TABLE #__pitemplate ADD COLUMN cssoverride TEXT NOT NULL;");
        $db->query();
        $db->setQuery("ALTER TABLE #__pibckadmin DROP default_template;");
        $db->query();
          
    }
    
    $entry = false;
    $entry    = isset( $fields['checked_out'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pitemplate ADD COLUMN checked_out INT(10) NOT NULL DEFAULT '0';");
            $db->query();}
            else {
                $db->setQuery ("ALTER TABLE #__pitemplate CHANGE checked_out checked_out INT(10) NOT NULL DEFAULT '0';");
                $db->query(); 
            }
    $entry = false;
    $entry    = isset( $fields['checked_out_time'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pitemplate ADD COLUMN checked_out_time DATETIME NOT NULL;");
            $db->query();}

        return true;
}

/**
     * Method to update pibiblevers
     * return boolean
    */        

public static function updatebibleversiontable()
{

    $db    = JFactory::getDBO();
                    
    $bv = '#__pibiblevers';
    $fields = $db->getTableColumns(  $bv  );
    $entry = false;
    $entry    = isset( $fields['checked_out'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pibiblevers ADD COLUMN checked_out INT(10) NOT NULL DEFAULT '0';");
            $db->query();}
            else {
                $db->setQuery ("ALTER TABLE #__pibiblevers CHANGE checked_out checked_out INT(10) NOT NULL DEFAULT '0';");
                $db->query(); 
            }
    $entry = false;
    $entry    = isset( $fields['checked_out_time'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pibiblevers ADD COLUMN checked_out_time DATETIME NOT NULL;");
            $db->query();}


    return true;
}

/**
     * Method to update pibooks table
     * return boolean
    */

public static function updatebooktable()
{

    $db    = JFactory::getDBO();
            
    $bk = '#__pibooks';
    $fields = $db->getTableColumns( $bk  );
    
    $entry = false;
    $entry    = isset( $fields['checked_out'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pibooks ADD COLUMN checked_out INT(10) NOT NULL DEFAULT '0';");
            $db->query();}
            else {
                $db->setQuery ("ALTER TABLE #__pibooks CHANGE checked_out checked_out INT(10) NOT NULL DEFAULT '0';");
                $db->query(); 
            }
    $entry = false;
    $entry    = isset( $fields['checked_out_time'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pibooks ADD COLUMN checked_out_time DATETIME NOT NULL;");
            $db->query();}

    return true;

}

/**
     * Method to update picomments table
     * return boolean
    */

public static function updatecommenttable()
{

    $db    = JFactory::getDBO();

    $com = '#__picomments';
    $fields = $db->getTableColumns( $com  );
    
    $entry = false;
    $entry    = isset( $fields['checked_out'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__picomments ADD COLUMN checked_out INT(10) NOT NULL DEFAULT '0';");
            $db->query();}
            else {
                $db->setQuery ("ALTER TABLE #__picomments CHANGE checked_out checked_out INT(10) NOT NULL DEFAULT '0';");
                $db->query(); 
            }
    $entry = false;
    $entry    = isset( $fields['checked_out_time'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__picomments ADD COLUMN checked_out_time DATETIME NOT NULL;");
            $db->query();}
            
    return true;

}

/**
     * Method to update pifilepath table
     * return boolean
    */

public static function updatefiletable()
{

    $db    = JFactory::getDBO();
        
    $file = '#__pifilepath';
    $fields = $db->getTableColumns( $file  );
    $entry = false;
    $entry    = isset( $fields['checked_out'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pifilepath ADD COLUMN checked_out INT(10) NOT NULL DEFAULT '0';");
            $db->query();}
            else {
                $db->setQuery ("ALTER TABLE #__pifilepath CHANGE checked_out checked_out INT(10) NOT NULL DEFAULT '0';");
                $db->query(); 
            }
    $entry = false;
    $entry    = isset( $fields['checked_out_time'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pifilepath ADD COLUMN checked_out_time DATETIME NOT NULL;");
            $db->query();}
            
    return true;

}

/**
     * Method to update pimime table
     * return boolean
    */

public static function updatemimetable()
{

    $db    = JFactory::getDBO();
        
    $ml = '#__pimime';
    $fields = $db->getTableColumns( $ml );
    $entry = false;
    $entry    = isset( $fields['published'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pimime ADD COLUMN published TINYINT(3) NOT NULL DEFAULT '1';");
            $db->query();}
    
    //add necessary rows
    
    $doc = PIHelperupdate::addmimerow('doc', 'application/msword');
    $doc = PIHelperupdate::addmimerow('pdf', 'application/pdf');
    $doc = PIHelperupdate::addmimerow('ppt', 'application/vnd.ms-powerpoint');
    
    $row = JTable::getInstance('Mime', 'Table');
    
    $entry = false;
    $entry    = isset( $fields['checked_out'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pimime ADD COLUMN checked_out INT(10) NOT NULL DEFAULT '0';");
            $db->query();}
            else {
                $db->setQuery ("ALTER TABLE #__pimime CHANGE checked_out checked_out INT(10) NOT NULL DEFAULT '0';");
                $db->query(); 
            }
    $entry = false;
    $entry    = isset( $fields['checked_out_time'] );
    $check = $entry;    
            if (!$entry) {$db->setQuery ("ALTER TABLE #__pimime ADD COLUMN checked_out_time DATETIME NOT NULL;");
            $db->query();}
    
            
    return true;

}

/**
     * Method to set table version
     * return boolean
    */

public static function settableversion()
{
    $db = JFactory::getDBO();
    $version = PIHelperupdate::tableversion();
    $id = 1;

    $db->setQuery ("UPDATE #__pibckadmin SET tableversion = '{$version}' WHERE id = '{$id}' ;"); 
    $db->query();
    
    return true;
}

/**
     * Method to copy revolution template on fresh install
     * return boolean
    */

public static function movetemplates()
{
    $path =     JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_preachit'.DIRECTORY_SEPARATOR.'tmp';
    $copy = false;
    $revolution = PIHelperupdate::filemover('revolution');
    
    if ($revolution)
    {
        $copy = true;
    }
    
    return $copy;
    
}

/**
     * Method to copy file
     * @param string $temp temp folder name
     * return boolean
    */

public static function filemover($temp)
{
           
    jimport('joomla.filesystem.file');
    jimport('joomla.filesystem.folder');
    jimport('joomla.client.helper');
   $src = JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_preachit'.DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'original_templates'.DIRECTORY_SEPARATOR.$temp;
   $dest = JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_preachit'.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.$temp;
   $copy = false;
   $exists = JFolder::exists($dest);    
   $id = PIHelpertemp::tempid($temp);        
   if ($id >! 0 && $exists || $id == null && $exists)
   {$record = PIHelpertemp::createtemprecord($dest, $temp);
   JFolder::delete($dest);
   $copy = JFolder::copy($src, $dest);} 
   elseif ($id >! 0 && !$exists || $id == null && !$exists)
   {$copy = JFolder::copy($src, $dest);
   $record = PIHelpertemp::createtemprecord($dest, $temp);}
   
   // update revolving gif url
   
   // get all the template folders
   
   $folders = JFolder::folders(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_preachit'.DIRECTORY_SEPARATOR.'templates');
   
   //loop through folders
   
   foreach ($folders as $folder)
   {
       // fet files to check
       $files = JFolder::files(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_preachit'.DIRECTORY_SEPARATOR.'templates'.'/'.$folder);
       // loop through files to check and change if necessary
       foreach ($files AS $file)
       {
           $filecontent = null;
           $filepath = JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_preachit'.DIRECTORY_SEPARATOR.'templates'.'/'.$folder.'/'.$file;
           // open file
           if (!JFile::exists($filepath) || filesize($filepath) == 0)
           {continue;}
           $filecontent=fopen($filepath,"rb");
           $content = fread($filecontent,filesize($filepath));
           fclose($filecontent);
           
           // string replace the code
           
           $content = str_replace('components/com_preachit/assets', 'media/preachit', $content );
           $content = str_replace('PIHelperadditional::pagination($ajax)', 'PIHelperadditional::pagination($this->pagination, $ajax)', $content );
           $content = str_replace('$ajax = \'\';', '$ajax = false;', $content );
           
           // Set FTP credentials, if given
           JClientHelper::setCredentialsFromRequest('ftp');
           $ftp = JClientHelper::getCredentials('ftp');
           $client =& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));
           // Try to make the template file writeable
           
           JFILE::write($filepath, $content);
       }
   
   }
    
    return $copy;
    
}    

/**
     * Method to add a record to pimime
     * @param string $ext extension value
     * @param string $type extension type
     * return boolean
    */

public static function addmimerow($ext, $type)
{
    
    $db= JFactory::getDBO();    
    $query = "SELECT ".$db->quoteName('id')."
    FROM ".$db->quoteName('#__pimime')."
    WHERE ".$db->quoteName('extension')." = ".$db->quote($ext).";
  ";
  $db->setQuery($query);
  $entry = $db->loadResult();
  
  if ($entry >! 0 || !$entry)
    { 
        $row = JTable::getInstance('Mime', 'Table');
        $row->published = 1;
        $row->extension = $ext;
        $row->mediatype = $type;
        if (!$row->store())
        {JError::raiseError(500, $row->getError() );}
    }
  return true;
}

/**
     * Method to process alias' to be unique'
     * return boolean
    */
public static function processalias()
{
    $db    = JFactory::getDBO();
    define('tealiasadmin', true);
    // run alias update and chaeck
    
    $query = "SELECT id FROM #__pistudies";
            $db->setQuery($query);
            $studies = $db->loadObjectList();
            
                    foreach ($studies as $study)
                    {                                 
                        $row = JTable::getInstance('Studies', 'Table');
                        $row->load($study->id);
                        if(empty($row->study_alias)) 
                        {$row->study_alias = $row->study_name;} 
                        $row->study_alias = Tewebadmin::uniquealias('#__pistudies',$row->study_alias, $row->id, 'study_alias');
                        if (!$row->store())
                        {JError::raiseError(500, $row->getError() );}    
                    }
    
    $query = "SELECT id FROM #__piseries";
            $db->setQuery($query);
            $series = $db->loadObjectList();
            
                    foreach ($series as $ser)
                    {                                 
                        $row = JTable::getInstance('Series', 'Table');
                        $row->load($ser->id);
                        if(empty($row->series_alias)) 
                        {$row->series_alias = $row->series_name;} 
                        $row->series_alias = Tewebadmin::uniquealias('#__piseries',$row->series_alias, $row->id, 'series_alias');
                        if (!$row->store())
                        {JError::raiseError(500, $row->getError() );}    
                    }
    
    $query = "SELECT id FROM #__piministry";
            $db->setQuery($query);
            $ministry = $db->loadObjectList();
            
                    foreach ($ministry as $min)
                    {                                 
                        $row = JTable::getInstance('Ministry', 'Table');
                        $row->load($min->id);
                        if(empty($row->ministry_alias)) 
                        {$row->ministry_alias = $row->ministry_name;} 
                        $row->ministry_alias = Tewebadmin::uniquealias('#__piministry',$row->ministry_alias, $row->id, 'ministry_alias');
                        if (!$row->store())
                        {JError::raiseError(500, $row->getError() );}    
                    }
    
    $query = "SELECT id FROM #__piteachers";
            $db->setQuery($query);
            $teachers = $db->loadObjectList();
            
                    foreach ($teachers as $tea)
                    {                                 
                        $row = JTable::getInstance('Teachers', 'Table');
                        $row->load($tea->id);
                        if(empty($row->teacher_alias)) 
                        {
                            if ($row->teacher_name)
                            {$name = $row->teacher_name.' '.$row->lastname;}
                            else {$name = $row->lastname;}
                            $row->teacher_alias = $name;
                        }
                        $row->teacher_alias = Tewebadmin::uniquealias('#__piteachers',$row->teacher_alias, $row->id, 'teacher_alias');
                        if (!$row->store())
                        {JError::raiseError(500, $row->getError() );}    
                    }
    return true;
    
}

/**
     * Method to remove old folders
     * return boolean
    */
public static function removeoldfolders()
{
    jimport('joomla.filesystem.folder');
    $abspath = JPATH_SITE;
    $folders = array();
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/elements';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/images';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/bibleversedit';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/bibleverslist';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/bookedit';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/booklist';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/commentedit';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/commentlist';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/filepathedit';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/filepathlist';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/mediaplayersedit';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/mediaplayerslist';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/mimeedit';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/mimelist';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/ministryedit';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/ministrylist';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/podcastedit';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/podcastlist';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/seriesedit';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/serieslist';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/shareedit';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/sharelist';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/studyedit';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/studylist';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/tagedit';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/taglist';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/teacheredit';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/teacherlist';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/templateedit';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/templatelist';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/videothird';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/addrecord';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/assets/swfupload';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/assets/icons';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/assets/css';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/assets/js';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/assets/mediaplayers';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/assets/images';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/audio';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/audiopopup';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/ministry';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/series';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/teacher';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/video';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/videopopup';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/temedia';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/temedialist';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/addrecord';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/models/forms';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/models/fields';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/videothird';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/medialist';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/studyedit';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/seriesedit';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/teacheredit';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/podcastlist';
    $folders[] = $abspath.DIRECTORY_SEPARATOR.'libraries/teweb/file/swfupload';
    $noerror = true;
    foreach ($folders AS $f)
    {
        if (JFolder::exists($f))
        {
            if (!JFolder::delete($f))
            {
                $noerror = false;
            }
        }
    }
    return $noerror;  
}

/**
     * Method to remove old folders
     * return boolean
    */
public static function removeoldfiles()
{
    jimport('joomla.filesystem.file');
    $abspath = JPATH_SITE;
    $files = array();
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/tables/bckadmin.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/admin/tmpl/form15.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/bibleversedit/tmpl/form15.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/bookedit/tmpl/form15.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/commentedit/tmpl/form15.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/filepathedit/tmpl/form15.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/mediaplayersedit/tmpl/form15.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/mimeedit/tmpl/form15.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/ministryedit/tmpl/form15.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/podcastedit/tmpl/form15.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/seriesedit/tmpl/form15.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/studyedit/tmpl/form15.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/teacheredit/tmpl/form15.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/templateedit/tmpl/form15.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/controllers/booklist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/controllers/filepathlist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/controllers/mimelist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/controllers/ministrylist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/controllers/podcastlist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/controllers/serieslist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/controllers/sharelist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/controllers/studylist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/controllers/taglist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/controllers/teacherlist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/admin.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/bibleverslist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/booklist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/commentlist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/filepathlist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/mediaplayerslist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/mimelist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/ministrylist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/podcastlist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/serieslist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/sharelist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/studylist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/taglist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/teacherlist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/templatelist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/bibleversedit.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/bookedit.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/commentedit.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/filepathedit.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/mediaplayersedit.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/mimeedit.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/ministryedit.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/podcastedit.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/seriesedit.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/shareedit.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/studyedit.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/teacheredit.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/templateedit.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/videothird.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/fields/audioplaylist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/fields/audioview.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/fields/folderlist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/fields/messagelist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/fields/mfhide.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/fields/mfupsel.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/fields/mupsellist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/fields/ministrylist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/fields/ministryview.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/fields/templatestyle.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/fields/playertypelist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/fields/serieslist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/fields/seriesview.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/fields/sfhide.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/fields/sfupsel.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/fields/teacherview.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/fields/templatechooser.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/fields/textview.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/fields/tfhide.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/fields/tfupsel.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/fields/videoplaylist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/models/fields/videoview.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/biblever/tmpl/submitbutton.js';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/book/tmpl/submitbutton.js';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/comment/tmpl/submitbutton.js';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/filepath/tmpl/submitbutton.js';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/mediaplayer/tmpl/submitbutton.js';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/mime/tmpl/submitbutton.js';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/ministry/tmpl/submitbutton.js';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/podcast/tmpl/submitbutton.js';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/series/tmpl/submitbutton.js';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/series/tmpl/temediainsert.js';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/study/tmpl/submitbutton.js';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/study/tmpl/temediainsert.js';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/share/tmpl/submitbutton.js';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/tag/tmpl/submitbutton.js';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'administrator/components/com_preachit/views/teacher/tmpl/submitbutton.js';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/study/tmpl/temediainsert.js';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/series/tmpl/temediainsert.js';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/addrecord/tmpl/default.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/temedia/tmpl/default.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/temedialist/tmpl/default.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/temedialist/tmpl/default_docs.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/temedialist/tmpl/default_folder.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/temedialist/tmpl/default_image.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/models/series.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/models/teacher.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/models/ministry.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/models/temedia.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/models/temedialist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/studyedit/tmpl/form15.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/studyedit/tmpl/form15.xml';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/seriesedit/tmpl/modal15.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/seriesedit/tmpl/modal15.xml';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/teacheredit/tmpl/modal15.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/teacheredit/tmpl/modal15.xml';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/addchecks.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/adminpublic static functions.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/audioid3.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/imageresize.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/pluginviewaudio.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/pluginviewlist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/pluginviewvideo.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/urlbuilder.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/booklist/tmpl/default.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/views/datelist/tmpl/default.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/controllers/podcastlist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/controllers/seriesedit.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/controllers/teacheredit.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/controllers/studyedit.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/models/studyedit.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/models/podcastlist.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/models/seriesedit.php';
    $files[] = $abspath.DIRECTORY_SEPARATOR.'components/com_preachit/models/teacheredit.php';
    
    $noerror = true;
    foreach ($files AS $f)
    {
        if (JFile::exists($f))
        {
            if (!JFile::delete($f))
            {
                $noerror = false;
            }
        }
    }
    return $noerror;  
}
        
}
