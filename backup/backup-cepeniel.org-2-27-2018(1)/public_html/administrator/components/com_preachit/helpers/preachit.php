<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
 
class PreachitHelper
{
	/**
	 * Configure the Linkbar.
	 *
	 * @param	string	The name of the active view.
	 *
	 * @return	void
	 * @since	1.6
	 */
	public static function addSubmenu($smenu)
	{
        $user    = JFactory::getUser();
        if (Tewebcheck::getJversion() >= 3.0)
        {
            JHtmlSidebar::addEntry(
                JText::_('COM_PREACHIT_MENU_CPANEL'),
                'index.php?option=com_preachit&view=cpanel',
                $smenu == 'cpanel'
            );        
            JHtmlSidebar::addEntry(
                JText::_('COM_PREACHIT_MENU_MESSAGES'),
                'index.php?option=com_preachit&view=studies',
                $smenu == 'messages'
            );
            JHtmlSidebar::addEntry(
                JText::_('COM_PREACHIT_MENU_TEACHER'),
                'index.php?option=com_preachit&view=teachers',
                $smenu == 'teachers'
            );
            JHtmlSidebar::addEntry(
                JText::_('COM_PREACHIT_MENU_SERIES'),
                'index.php?option=com_preachit&view=sery',
                $smenu == 'sery'
            );
            JHtmlSidebar::addEntry(
                JText::_('COM_PREACHIT_MENU_MINISTRIES'),
                'index.php?option=com_preachit&view=ministries',
                $smenu == 'ministries'
            );
            JHtmlSidebar::addEntry(
                JText::_('COM_PREACHIT_MENU_COMMENTS'),
                'index.php?option=com_preachit&view=comments',
                $smenu == 'comments'
            );
            JHtmlSidebar::addEntry(
                JText::_('COM_PREACHIT_MENU_PODCASTS'),
                'index.php?option=com_preachit&view=podcasts',
                $smenu == 'podcasts'
            );
            JHtmlSidebar::addEntry(
                JText::_('COM_PREACHIT_MENU_TAGS'),
                'index.php?option=com_preachit&view=tags',
                $smenu == 'tags'
            );
            JHtmlSidebar::addEntry(
                JText::_('COM_PREACHIT_MENU_TEMPLATES'),
                'index.php?option=com_preachit&view=templates',
                $smenu == 'templates'
            );
            if ($user->authorise('core.admin', 'com_preachit'))
            {
                JHtmlSidebar::addEntry(
                JText::_('COM_PREACHIT_MENU_BACKENDADMIN'),
                'index.php?option=com_preachit&view=admin',
                $smenu == 'admin'
                );
            }
        }
        else {
		    JSubMenuHelper::addEntry(
			    JText::_('COM_PREACHIT_MENU_CPANEL'),
			    'index.php?option=com_preachit&view=cpanel',
			    $smenu == 'cpanel'
		    );		
		    JSubMenuHelper::addEntry(
			    JText::_('COM_PREACHIT_MENU_MESSAGES'),
			    'index.php?option=com_preachit&view=studies',
			    $smenu == 'messages'
		    );
		    JSubMenuHelper::addEntry(
			    JText::_('COM_PREACHIT_MENU_TEACHER'),
			    'index.php?option=com_preachit&view=teachers',
			    $smenu == 'teachers'
		    );
		    JSubMenuHelper::addEntry(
			    JText::_('COM_PREACHIT_MENU_SERIES'),
			    'index.php?option=com_preachit&view=sery',
			    $smenu == 'sery'
		    );
		    JSubMenuHelper::addEntry(
			    JText::_('COM_PREACHIT_MENU_MINISTRIES'),
			    'index.php?option=com_preachit&view=ministries',
			    $smenu == 'ministries'
		    );
		    JSubMenuHelper::addEntry(
			    JText::_('COM_PREACHIT_MENU_COMMENTS'),
			    'index.php?option=com_preachit&view=comments',
			    $smenu == 'comments'
		    );
		    JSubMenuHelper::addEntry(
			    JText::_('COM_PREACHIT_MENU_PODCASTS'),
			    'index.php?option=com_preachit&view=podcasts',
			    $smenu == 'podcasts'
		    );
            JSubMenuHelper::addEntry(
                JText::_('COM_PREACHIT_MENU_TAGS'),
                'index.php?option=com_preachit&view=tags',
                $smenu == 'tags'
            );
		    JSubMenuHelper::addEntry(
			    JText::_('COM_PREACHIT_MENU_TEMPLATES'),
			    'index.php?option=com_preachit&view=templates',
			    $smenu == 'templates'
		    );
            if ($user->authorise('core.admin', 'com_preachit'))
            {
		        JSubMenuHelper::addEntry(
			    JText::_('COM_PREACHIT_MENU_BACKENDADMIN'),
			    'index.php?option=com_preachit&view=admin',
			    $smenu == 'admin'
		        );
            }
        }
		
	}
public static function getActions($id = 0)
	{
		$user	= JFactory::getUser();
		$result	= new JObject;

		$assetName = 'com_preachit';

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.state', 'core.delete', 'core.edit.own'
		);

		foreach ($actions as $action) {
			$result->set($action,	$user->authorise($action, $assetName));
		}

		return $result;
	}
	
}