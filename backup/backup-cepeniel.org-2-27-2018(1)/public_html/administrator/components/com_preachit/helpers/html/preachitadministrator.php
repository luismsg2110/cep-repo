<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  COM_PREACHIT
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('ContentHelper', JPATH_ADMINISTRATOR . '/components/COM_PREACHIT/helpers/content.php');

/**
 * @package     Joomla.Administrator
 * @subpackage  COM_PREACHIT
 */
abstract class JHtmlPreachitAdministrator
{

	/**
	 * @param   int $value	The state value
	 * @param   int $i
	 */
	public static function featured($value = 0, $i, $controller = 'studies.', $canChange = true)
	{
		JHtml::_('bootstrap.tooltip');

		// Array of image, task, title, action
		$states	= array(
			0	=> array('star-empty',	$controller.'featured',	'COM_PREACHIT_UNFEATURED',	'COM_PREACHIT_TOGGLE_TO_FEATURE'),
			1	=> array('star',		$controller.'unfeatured',	'COM_PREACHIT_FEATURED',		'COM_PREACHIT_TOGGLE_TO_UNFEATURE'),
		);
		$state	= JArrayHelper::getValue($states, (int) $value, $states[1]);
		$icon	= $state[0];
		if ($canChange)
		{
			$html	= '<a href="#" onclick="return listItemTask(\'cb'.$i.'\',\''.$state[1].'\')" class="btn btn-micro hasTooltip' . ($value == 1 ? ' active' : '') . '" title="'.JText::_($state[3]).'"><i class="icon-'
					. $icon.'"></i></a>';
		}
		else
		{
			$html	= '<a class="btn btn-micro hasTooltip disabled' . ($value == 1 ? ' active' : '') . '" title="'.JText::_($state[2]).'"><i class="icon-'
					. $icon.'"></i></a>';
		}

		return $html;
	}
    /**
     * @param    int $value    The state value
     * @param    int $i
     */
    static function featured25($value = 0, $i, $controller = 'studies.', $canChange = true)
    {
        // Array of image, task, title, action
        $states    = array(
            0    => array('disabled.png',    $controller.'featured',    'COM_PREACHIT_UNFEATURED',    'COM_PREACHIT_TOGGLE_TO_FEATURE'),
            1    => array('featured.png',        $controller.'unfeatured',    'COM_PREACHIT_FEATURED',        'COM_PREACHIT_TOGGLE_TO_UNFEATURE'),
        );
        $state    = JArrayHelper::getValue($states, (int) $value, $states[1]);
        $html    = JHtml::_('image', 'admin/'.$state[0], JText::_($state[2]), NULL, true);
        if ($canChange) {
            $html    = '<a href="#" onclick="return listItemTask(\'cb'.$i.'\',\''.$state[1].'\')" title="'.JText::_($state[3]).'">'
                    . $html.'</a>';
        }

        return $html;
    }
}
