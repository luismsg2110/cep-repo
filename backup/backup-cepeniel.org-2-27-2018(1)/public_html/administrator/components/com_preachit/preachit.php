<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('teweb.checks.standard');
jimport('teweb.details.standard');
jimport('teweb.admin.records');
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/additional.php');
$lang = JFactory::getLanguage();
$lang->load('lib_teweb', JPATH_SITE);
JTable::addincludePath(JPATH_COMPONENT.DIRECTORY_SEPARATOR.'tables');
// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_preachit')) 
{
        return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}
// require helper file
JLoader::register('PreachitHelper', dirname(__FILE__).DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'preachit.php');

    // import joomla controller library
    jimport('joomla.application.component.controller');

    $controller    = JControllerLegacy::getInstance('Preachit');
    $controller->execute(JFactory::getApplication()->input->get('task'));
    $controller->redirect();
