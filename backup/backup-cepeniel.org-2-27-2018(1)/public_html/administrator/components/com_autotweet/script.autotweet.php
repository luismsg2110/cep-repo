<?php

/**
 * @package     Extly.Components
 * @subpackage  com_autotweet - AutoTweetNG posts content to social channels (Twitter, Facebook, LinkedIn, etc).
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */

/**
 * Based on script.akeeba.php / AkeebaBackup
 * Copyright (c)2009-2013 Nicholas K. Dionysopoulos
 * GNU General Public License version 3, or later
 */

defined('_JEXEC') or die();

jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');

/**
 * Com_AutoTweetInstallerScript
 *
 * @package     Extly.Components
 * @subpackage  com_autotweet
 * @since       1.0
 */
class Com_AutoTweetInstallerScript
{
	/** @var string The component's name */
	protected $_extly_extension = 'com_autotweet';

	/** @var array The list of extra modules and plugins to install */
	private $installation_queue = array(

			// Modules => { (folder) => { (module) => { (position), (published) } }* }*
			'modules' => array(
					'admin' => array(
							'autotweet_latest' => array('cpanel', 0),
					),
					'site' => array(
							'twfollow' => array('left', 0),
							'light_rss' => array('left', 0)
					)
			),

			// Plugins => { (folder) => { (element) => (published) }* }*
			'plugins' => array(
					'system' => array(
							'autotweetautomator' => 1,
							'autotweetcontent'	=> 1
					),
					'autotweet' => array(
							'autotweetpost' => 0
					),
					'content' => array(
							'autotweetweblinks' => 0
					),
			),

	);

	/** @var array Obsolete files and folders to remove from the Core release only */
	private $extlyRemoveFilesCore = array(
			'files'	=> array(
					// FOF 1.x files
					'libraries/fof/controller.php',
					'libraries/fof/dispatcher.php',
					'libraries/fof/inflector.php',
					'libraries/fof/input.php',
					'libraries/fof/model.php',
					'libraries/fof/query.abstract.php',
					'libraries/fof/query.element.php',
					'libraries/fof/query.mysql.php',
					'libraries/fof/query.mysqli.php',
					'libraries/fof/query.sqlazure.php',
					'libraries/fof/query.sqlsrv.php',
					'libraries/fof/render.abstract.php',
					'libraries/fof/render.joomla.php',
					'libraries/fof/render.joomla3.php',
					'libraries/fof/render.strapper.php',
					'libraries/fof/string.utils.php',
					'libraries/fof/table.php',
					'libraries/fof/template.utils.php',
					'libraries/fof/toolbar.php',
					'libraries/fof/view.csv.php',
					'libraries/fof/view.html.php',
					'libraries/fof/view.json.php',
					'libraries/fof/view.php',

					// AutoTweetNG 6.3, and before
					'administrator/components/com_autotweet/autotweet.xml',
					'administrator/components/com_autotweet/controller.php',
					'administrator/components/com_autotweet/controllers/account.json.php',
					'administrator/components/com_autotweet/controllers/account.php',
					'administrator/components/com_autotweet/controllers/account.raw.php',
					'administrator/components/com_autotweet/controllers/accounts.php',
					'administrator/components/com_autotweet/controllers/accounts.raw.php',
					'administrator/components/com_autotweet/controllers/autotweetentries.php',
					'administrator/components/com_autotweet/controllers/autotweetentry.php',
					'administrator/components/com_autotweet/controllers/autotweet.php',
					'administrator/components/com_autotweet/controllers/autotweet.raw.php',
					'administrator/components/com_autotweet/controllers/dashboard.php',
					'administrator/components/com_autotweet/controllers/fbwizzardbaseaccount.php',
					'administrator/components/com_autotweet/controllers/fbwizzardbaseaccounts.php',
					'administrator/components/com_autotweet/controllers/fbwizzardbaseaccounts.raw.php',
					'administrator/components/com_autotweet/controllers/manualmessage.php',
					'administrator/components/com_autotweet/controllers/queue.php',
					'administrator/components/com_autotweet/controllers/rule.php',
					'administrator/components/com_autotweet/controllers/rules.php',
					'administrator/components/com_autotweet/helpers/autotweetcronjob.php',
					'administrator/components/com_autotweet/helpers/autotweetextaccesshelper.php',
					'administrator/components/com_autotweet/helpers/autotweetguihelper.php',
					'administrator/components/com_autotweet/helpers/autotweetinfohelper.php',
					'administrator/components/com_autotweet/helpers/autotweetng16-test.ini',
					'administrator/components/com_autotweet/helpers/autotweetng.ini',
					'administrator/components/com_autotweet/helpers/autotweetpluginfactory.php',
					'administrator/components/com_autotweet/helpers/autotweetroutehelper.php',
					'administrator/components/com_autotweet/helpers/autotweetruleengine.php',
					'administrator/components/com_autotweet/helpers/autotweetutil.php',
					'administrator/components/com_autotweet/helpers/autotweetview.php',
					'administrator/components/com_autotweet/helpers/channels/autotweetaccountfactory.php',
					'administrator/components/com_autotweet/helpers/channels/autotweetchannelfactoryhelper.php',
					'administrator/components/com_autotweet/helpers/channels/autotweetchannel.php',
					'administrator/components/com_autotweet/helpers/channels/autotweetchannelfactory',
					'administrator/components/com_autotweet/helpers/channels/autotweetfacebookaccount.php',
					'administrator/components/com_autotweet/helpers/channels/autotweetfacebookbase.php',
					'administrator/components/com_autotweet/helpers/channels/autotweetfacebookevent.php',
					'administrator/components/com_autotweet/helpers/channels/autotweetfacebooklink.php',
					'administrator/components/com_autotweet/helpers/channels/autotweetfacebook.php',
					'administrator/components/com_autotweet/helpers/channels/autotweetlinkedinbase.php',
					'administrator/components/com_autotweet/helpers/channels/autotweetlinkedingroup.php',
					'administrator/components/com_autotweet/helpers/channels/autotweetlinkedin.php',
					'administrator/components/com_autotweet/helpers/channels/autotweetmail.php',
					'administrator/components/com_autotweet/helpers/channels/autotweetpingfm.php',
					'administrator/components/com_autotweet/helpers/channels/autotweetseesmicping.php',
					'administrator/components/com_autotweet/helpers/channels/autotweettwitter.php',
					'administrator/components/com_autotweet/helpers/channels/fmhttpcodehelper.php',
					'administrator/components/com_autotweet/helpers/fmregistryhelper.php',
					'administrator/components/com_autotweet/helpers/urlshortservices/autotweetshortservicefactory.php',
					'administrator/components/com_autotweet/install.autotweet.php',
					'administrator/components/com_autotweet/installer.php',
					'administrator/components/com_autotweet/models/account.php',
					'administrator/components/com_autotweet/models/accounts.php',
					'administrator/components/com_autotweet/models/autotweetentries.php',
					'administrator/components/com_autotweet/models/autotweetentry.php',
					'administrator/components/com_autotweet/models/autotweet.php',
					'administrator/components/com_autotweet/models/fbwizzardbaseaccount.php',
					'administrator/components/com_autotweet/models/fbwizzardbaseaccounts.php',
					'administrator/components/com_autotweet/models/info.php',
					'administrator/components/com_autotweet/models/manualmessage.php',
					'administrator/components/com_autotweet/models/queueentry.php',
					'administrator/components/com_autotweet/models/queue.php',
					'administrator/components/com_autotweet/models/rule.php',
					'administrator/components/com_autotweet/sql/install.sql',
					'administrator/components/com_autotweet/sql/uninstall.sql',
					'administrator/components/com_autotweet/tables/autotweetaccount.php',
					'administrator/components/com_autotweet/tables/autotweetautomator.php',
					'administrator/components/com_autotweet/tables/autotweetchannel.php',
					'administrator/components/com_autotweet/tables/autotweetchanneltype.php',
					'administrator/components/com_autotweet/tables/autotweetmsglog.php',
					'administrator/components/com_autotweet/tables/autotweetqueue.php',
					'administrator/components/com_autotweet/tables/autotweetrule.php',
					'administrator/components/com_autotweet/tables/autotweetruletype.php',
					'administrator/components/com_autotweet/uninstall.autotweet.php',
					'administrator/components/com_autotweet/views/rule/tmpl/default.php',
					'administrator/components/com_autotweet/views/rule/tmpl/edit.php',

					'administrator/components/com_autotweet/views/target/tmpl/criteria_template.php',

					// Extly Lib - Old CronParser
					'libraries/extly/helpers/CronParser.php',

					// German removal
					'administrator/language/de-DE/de-DE.com_autotweet.ini',
					'administrator/language/de-DE/de-DE.com_autotweet.sys.ini',
					'language/de-DE/de-DE.com_autotweet.ini',
					'language/de-DE/de-DE.com_autotweet.sys.ini',

					// Extly Lib - Old Css
					'media/lib_extly/css/extly-base.css',

					// Extly Lib - IE Shim
					'media/lib_extly/js/ie.js',
					'media/lib_extly/js/utils/ie.js',
					'media/lib_extly/js/utils/ie.min.js',

					// Utils
					'media/lib_extly/js/utils/tourist.js',

					// Extly Lib - defaultmain.js
					'media/lib_extly/js/defaultmain.js',
					'media/lib_extly/js/defaultmain.min.js',

					// Extly Lib - jquery.disabled.js
					'media/lib_extly/js/jquery/jquery.disabled.js',

					// Extly Lib - lodash.min.js new version
					'media/lib_extly/js/lodash.min.js',
					'media/lib_extly/js/backbone/lodash.min.js',
					'media/lib_extly/js/backbone/lodash.underscore.min.js',

					// Extlycorefront no more
					'media/lib_extly/js/extlycorefront.js',
					'media/lib_extly/js/extlycorefront.min.js',

					// No social 64 icons
					'media/com_autotweet/images/mail-forward.png',
					'media/com_autotweet/images/social_facebook_box_blue_64.png',
					'media/com_autotweet/images/social_linkedin_box_blue_64.png',
					'media/com_autotweet/images/social_twitter_box_blue_64.png',

					// // // Joocial Reset
					// Target reset
					'media/com_autotweet/js/target.js',
					'media/com_autotweet/js/target.min.js',
					'administrator/components/com_autotweet/controllers/targets.php',
					'administrator/components/com_autotweet/models/targets.php',
					'administrator/components/com_autotweet/tables/target.php',
					// ItemEditor reset
					'media/com_autotweet/js/itemeditor.js',
					'media/com_autotweet/js/itemeditor.min.js',
					'media/com_autotweet/js/itemeditor.helper.min.js',
					'administrator/components/com_autotweet/controllers/itemeditors.php',
					// Jootool reset
					'media/com_autotweet/js/jootool.js',
					'media/com_autotweet/js/jootool.min.js',
					// Virtual Manager reset
					'media/com_autotweet/js/manager.js',
					'media/com_autotweet/js/manager.min.js',
					'administrator/components/com_autotweet/controllers/managers.php',
					'administrator/components/com_autotweet/models/extensions.php',
					'administrator/components/com_autotweet/tables/extension.php',

					// - browseview
					'administrator/components/com_autotweet/helpers/browseview.php',

					// Twitter Lib 0.8.3
					'administrator/components/com_autotweet/helpers/channels/tmhOAuth/tmhUtilities.php'
			),
			'folders' => array(
					// Legacy Reset
					'administrator/components/com_autotweet/assets',
					'administrator/components/com_autotweet/cronjob',
					'administrator/components/com_autotweet/help',
					'administrator/components/com_autotweet/helpers/channels/atfacebook',
					'administrator/components/com_autotweet/helpers/channels/atlinkedin',
					'administrator/components/com_autotweet/helpers/channels/atmailer',
					'administrator/components/com_autotweet/helpers/channels/atpingfm',
					'administrator/components/com_autotweet/helpers/channels/attwitter',
					'administrator/components/com_autotweet/helpers/jsonwrapper',
					'administrator/components/com_autotweet/language',
					'administrator/components/com_autotweet/models/fields',
					'administrator/components/com_autotweet/models/forms',
					'administrator/components/com_autotweet/views/account',
					'administrator/components/com_autotweet/views/accounts',
					'administrator/components/com_autotweet/views/autotweet',
					'administrator/components/com_autotweet/views/autotweetentries',
					'administrator/components/com_autotweet/views/autotweetentry',
					'administrator/components/com_autotweet/views/dashboard',
					'administrator/components/com_autotweet/views/fbwizzardbaseaccount',
					'administrator/components/com_autotweet/views/fbwizzardbaseaccounts',
					'administrator/components/com_autotweet/views/info',
					'administrator/components/com_autotweet/views/manualmessage',
					'administrator/components/com_autotweet/views/queue',

					// Extly Lib - Scheduler backend
					'libraries/extly/scheduler/backend',
					'media/lib_extly/js/scheduler',

					// FontAwesome png 16x16 icons
					'media/lib_extly/images/icons',

					// // // Joocial Reset
					// Target reset
					'media/com_autotweet/js/target',
					'administrator/components/com_autotweet/views/target',
					'administrator/components/com_autotweet/views/targets',
					// ItemEditor reset
					'media/com_autotweet/js/itemeditor',
					'administrator/components/com_autotweet/views/itemeditor',
					'components/com_autotweet/views/itemeditor',
					// Jootool reset
					'components/com_autotweet/views/jootool',
					'components/com_autotweet/views/cpanels',
					'components/com_autotweet/views/post',
					'components/com_autotweet/views/request',
					'components/com_autotweet/views/posts',
					'components/com_autotweet/views/requests',
					'components/com_autotweet/views/channel',
					'components/com_autotweet/views/channels',
					'components/com_autotweet/views/fbchannel',
					'components/com_autotweet/views/gpluschannel',
					'components/com_autotweet/views/lichannel',
					'components/com_autotweet/views/licompanychannel',
					'components/com_autotweet/views/ligroupchannel',
					'components/com_autotweet/views/mailchannel',
					'components/com_autotweet/views/nochannel',
					'components/com_autotweet/views/twchannel',
					'components/com_autotweet/views/vkchannel',
					// Virtual Manager reset
					'administrator/components/com_autotweet/views/manager'
				)
	);

	/** @var array Obsolete files and folders to remove from the Core and Pro releases */
	private $extlyRemoveFilesPro = array(
			'files'	=> array(
			),
			'folders' => array(
			)
	);

	private $extlyCliScripts = array(
			'autotweetstartcronjob.php'
	);

	/**
	 * Joomla! pre-flight event
	 *
	 * @param   string      $type    Installation type (install, update, discover_install)
	 * @param   JInstaller  $parent  Parent object
	 *
	 * @return	bool
	*/
	public function preflight($type, $parent)
	{
		// Only allow to install on Joomla! 2.5.0 or later with PHP 5.3.0 or later
		if (defined('PHP_VERSION'))
		{
			$version = PHP_VERSION;
		}
		elseif (function_exists('phpversion'))
		{
			$version = phpversion();
		}
		else
		{
			// All bets are off!
			$version = '5.0.0';
		}

		if (!version_compare(JVERSION, '2.5.6', 'ge'))
		{
			$msg = "<p>You need Joomla! 2.5.6 or later to install this component</p>";

			if (version_compare(JVERSION, '3.0', 'gt'))
			{
				JLog::add($msg, JLog::WARNING, 'jerror');
			}
			else
			{
				JError::raiseWarning(100, $msg);
			}

			return false;
		}

		// PHP 5.3.1
		if (!version_compare($version, '5.3.1', 'ge'))
		{
			$msg = "<p>You need PHP 5.3.1 or later to install this component</p>";

			if (version_compare(JVERSION, '3.0', 'gt'))
			{
				JLog::add($msg, JLog::WARNING, 'jerror');
			}
			else
			{
				JError::raiseWarning(100, $msg);
			}

			return false;
		}

		// PHP 5.2, You shall not pass | Anonymous function variable assignment example
		try
		{
			$date = new DateTime;
			$now = $date->getTimestamp();
			$greet = function($name) {
				return sprintf("Hello %s\r\n", $name);
			};

			$test = $greet('Extly');
		}
		catch (Exception $e)
		{
			$msg = "<p>You need PHP 5.3.1 or later to install this component (Anonymous function)</p>";

			if (version_compare(JVERSION, '3.0', 'gt'))
			{
				JLog::add($msg, JLog::WARNING, 'jerror');
			}
			else
			{
				JError::raiseWarning(100, $msg);
			}

			return false;
		}

		// Update on AutoTweet v6.5, or superior
		if (($type == 'update') && ($this->_isUnder65()))
		{
			$msg = "<p>Update is only allowed on AutoTweetNG v6.5, or superior. Please, uninstall AutoTweetNG (manually drop old autotweet tables from database), and install the new version.</p>";

			if (version_compare(JVERSION, '3.0', 'gt'))
			{
				JLog::add($msg, JLog::WARNING, 'jerror');
			}
			else
			{
				JError::raiseWarning(100, $msg);
			}

			return false;
		}

		// Bugfix for "Can not build admin menus"
		// Workarounds for JInstaller bugs
		if (in_array($type, array('install')))
		{
			$this->_bugfixDBFunctionReturnedNoError();
		}
		elseif ($type != 'discover_install')
		{
			$this->_bugfixCantBuildAdminMenus();
			$this->_fixBrokenSQLUpdates($parent);
			$this->_fixSchemaVersion();
			$this->_resetLiveUpdate();
		}

		return true;
	}

	/**
	 * _isUnder65
	 *
	 * @return bool
	 */
	private function _isUnder65()
	{
		$db = JFactory::getDbo();
		$sql = $db->getQuery(true)
			->select($db->qn('version_id'))
			->from($db->qn('#__extensions', 'e'))
			->from($db->qn('#__schemas', 's'))
			->where($db->qn('s') . ' . ' . $db->qn('extension_id') . ' = ' . $db->qn('e') . ' . ' . $db->qn('extension_id'))
			->where($db->qn('name') . ' = ' . $db->q('com_autotweet'))
			->where($db->qn('type') . ' = ' . $db->q('component'));
		$db->setQuery($sql);
		$version_id = $db->loadResult();

		return (version_compare($version_id, '6.5.0', 'lt'));
	}

	/**
	 * Runs after install, update or discover_update
	 *
	 * @param   string      $type    install, update or discover_update
	 *
	 * @param   JInstaller  $parent  Param
	 *
	 * @return void
	 */
	function postflight($type, $parent)
	{
		// Install subextensions
		$status = $this->_installSubextensions($parent);

		// Install FOF
		$fofStatus = $this->_installFOF($parent);

		// Install Extly Straper
		$straperStatus = $this->_installStraper($parent);

		// Remove obsolete files and folders
		// is_dir($parent->getParent()->getPath('source') . '/plugins/system/srp');
		$isExtlyPro = false;

		if ($isExtlyPro)
		{
			$extlyRemoveFiles = $this->extlyRemoveFilesPro;
		}
		else
		{
			$extlyRemoveFiles = array(
							'files'		 => array_merge($this->extlyRemoveFilesPro['files'], $this->extlyRemoveFilesCore['files']),
							'folders'	 => array_merge($this->extlyRemoveFilesPro['folders'], $this->extlyRemoveFilesCore['folders']),
			);
		}

		// Remove Professional version plugins from Extly Core
		if (!$isExtlyPro)
		{
			$this->_removeProPlugins($parent);
		}

			$this->_removeObsoleteFilesAndFolders($extlyRemoveFiles);
		$this->_copyCliFiles($parent);

		// Make sure the two plugins folders exist in Core release and are empty
		if (!$isExtlyPro)
		{
			if (!JFolder::exists(JPATH_ADMINISTRATOR . '/components/com_extly/plugins'))
			{
				JFolder::create(JPATH_ADMINISTRATOR . '/components/com_extly/plugins');
			}

			if (!JFolder::exists(JPATH_ADMINISTRATOR . '/components/com_extly/extly/plugins'))
			{
				JFolder::create(JPATH_ADMINISTRATOR . '/components/com_extly/extly/plugins');
			}
		}

		// Show the post-installation page
		$this->_renderPostInstallation($type, $status, $fofStatus, $straperStatus, $parent);

		// Kill update site
		$this->_killUpdateSite();

		// Clear FOF's cache
		if (!defined('FOF_INCLUDED'))
		{
			@include_once JPATH_LIBRARIES . '/fof/include.php';
		}

		if (defined('FOF_INCLUDED'))
		{
			$platform = FOFPlatform::getInstance();

			if (method_exists($platform, 'clearCache'))
			{
				FOFPlatform::getInstance()->clearCache();
			}
		}
	}

	/**
	 * Runs on uninstallation
	 *
	 * @param   JInstaller  $parent  Param
	 *
	 * @return void
	 */
	function uninstall($parent)
	{
		$this->_uninstallAutotweetPlugins($parent);

		// Uninstall subextensions
		$status = $this->_uninstallSubextensions($parent);

		// Show the post-uninstallation page
		$this->_renderPostUninstallation($status, $parent);
	}

	/**
	 * _uninstallAutotweetPlugins.
	 *
	 * @param   object  $parent  Params.
	 *
	 * @return	boolean.
	 *
	 * @since	1.5
	 */
	public function _uninstallAutotweetPlugins($parent)
	{
		$db = JFactory::getDBO();

		// Uninstall additional elements (plugins and modules)

		$inst = new JInstaller;
		$query = 'SELECT * FROM ' . $db->quoteName('#__extensions')
		. ' WHERE (' .
		// $db->quoteName('type') . ' = ' . $db->Quote('package') . ' OR ' .
		$db->quoteName('type') . ' = ' . $db->Quote('plugin') . ' OR ' .
		$db->quoteName('type') . ' = ' . $db->Quote('module') . ') AND (' .
		$db->quoteName('element') . ' like ' . $db->Quote('%autotweet%') . ' OR ' .
		$db->quoteName('name') . ' like ' . $db->Quote('%AutoTweet%') . ' OR ' .
		$db->quoteName('element') . ' like ' . $db->Quote('%joocial%') . ' OR ' .
		$db->quoteName('name') . ' like ' . $db->Quote('%Joocial%')
		. ')' .	' ORDER BY ' . $db->quoteName('extension_id');

		$db->setQuery($query);
		$extensions = $db->loadAssocList();

		foreach ($extensions as $ext)
		{
			$inst_result = $inst->uninstall($ext['type'], $ext['extension_id']);

			if (!$inst_result)
			{
				echo '<p>' . JText::sprintf('COM_AUTOTWEET_UNINSTALL_EXT_FAILED', $ext['name']) . '</p>';
			}
			else
			{
				echo '<p>' . JText::sprintf('COM_AUTOTWEET_UNINSTALL_EXT_SUCCESS', $ext['name']) . '</p>';
			}
		}

		echo '<p><strong>' . JText::_('COM_AUTOTWEET_UNINSTALL_SUCCESS') . '</strong></p>';
	}

	/**
	 * Removes the Professional edition's plugins from the Core version
	 *
	 * @param   JInstaller  $parent  Param
	 *
	 * @return void
	 */
	private function _removeProPlugins($parent)
	{
		$src = $parent->getParent()->getPath('source');
		$db	 = JFactory::getDbo();

		// ----- System - System Restore Points
		$sql = $db->getQuery(true)
		->select($db->qn('extension_id'))
		->from($db->qn('#__extensions'))
		->where($db->qn('type') . ' = ' . $db->q('plugin'))
		->where($db->qn('element') . ' = ' . $db->q('srp'))
		->where($db->qn('folder') . ' = ' . $db->q('system'));
		$db->setQuery($sql);
		$id	 = $db->loadResult();

		if ($id)
		{
			$installer	 = new JInstaller;
			$result		 = $installer->uninstall('plugin', $id, 1);
		}

		// ----- System - Extly Update Check
		$sql = $db->getQuery(true)
			->select($db->qn('extension_id'))
			->from($db->qn('#__extensions'))
					->where($db->qn('type') . ' = ' . $db->q('plugin'))
					->where($db->qn('element') . ' = ' . $db->q('extlyupdatecheck'))
					->where($db->qn('folder') . ' = ' . $db->q('system'));

		$db->setQuery($sql);
		$id	 = $db->loadResult();

		if ($id)
		{
			$installer	 = new JInstaller;
			$result		 = $installer->uninstall('plugin', $id, 1);
		}
	}

	/**
	 * Copies the CLI scripts into Joomla!'s cli directory
	 *
	 * @param   JInstaller  $parent  Param
	 *
	 * @return void
	 */
	private function _copyCliFiles($parent)
	{
		$src = $parent->getParent()->getPath('source');

		jimport("joomla.filesystem.file");
		jimport("joomla.filesystem.folder");

		foreach ($this->extlyCliScripts as $script)
		{
			if (JFile::exists(JPATH_ROOT . '/cli/' . $script))
			{
				JFile::delete(JPATH_ROOT . '/cli/' . $script);
			}

			if (JFile::exists($src . '/cli/' . $script))
			{
				JFile::move($src . '/cli/' . $script, JPATH_ROOT . '/cli/' . $script);
			}
		}
	}

	/**
	 * Renders the post-installation message
	 *
	 * @param   string      $type           install, update or discover_update
	 * @param   bool        $status         Param
	 * @param   bool        $fofStatus      Param
	 * @param   bool        $straperStatus  Param
	 * @param   JInstaller  $parent         Param
	 *
	 * @return void
	 */
	private function _renderPostInstallation($type, $status, $fofStatus, $straperStatus, $parent)
	{
		?>

<?php $rows = 1;?>

<div class="extly">

	<img src="../media/com_autotweet/images/autotweet-logo.png" width="57" height="57" alt="Extly - Joomla Extensions" align="right" />

	<h1>Welcome to AutoTweetNG!</h1>

	<p>
		<strong>Enhance your social media management!</strong>
	</p>

<?php

	if ($type == 'update')
	{
?>
	<div class="alert">
			<p>This is a full update. Please, remember to <b>manually install your additional plugins, apps, and addons</b>; and <b>re-validate the channels</b>.</p>
	</div>
<?php
	}
?>
	<h2>Installation Status</h2>

	<table class="adminlist table table-striped table-bordered" width="100%">
		<thead>
			<tr>
				<th class="title" colspan="2">Extension</th>
				<th width="30%">Status</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="3"></td>
			</tr>
		</tfoot>
		<tbody>
			<tr class="row0 success">
				<td class="key" colspan="2">AutoTweetNG component</td>
				<td>Installed</td>
			</tr>

			<tr class="row0 <?php echo $straperStatus['required'] ? ($straperStatus['installed'] ? 'success' : 'error') : 'info'; ?>">
				<td class="key" colspan="2">Extly Framework <?php echo $straperStatus['version']?> [<?php echo $straperStatus['date'] ?>]
				</td>
				<td><span style=""> <?php echo $straperStatus['required'] ? ($straperStatus['installed'] ?'Installed':'Not Installed') : 'Already up-to-date'; ?>
				</span>
				</td>
			</tr>

			<?php

			if (count($status->modules))
			{
				?>
			<tr>
				<th>Module</th>
				<th>Client</th>
				<th></th>
			</tr>
			<?php
			foreach ($status->modules as $module)
			{
				?>
			<tr class="row<?php
			echo ($rows++ % 2);
			echo ($module['result']) ? " success" :  "error";
			?>">
				<td class="key"><?php echo $module['name']; ?></td>
				<td class="key"><?php echo ucfirst($module['client']); ?></td>
				<td><?php echo ($module['result'])?'Installed':'Not installed'; ?>
				</td>
			</tr>
			<?php
			}
			}
			?>

			<?php

			if (count($status->plugins))
			{
			?>
			<tr>
				<th>Plugin</th>
				<th>Group</th>
				<th></th>
			</tr>
			<?php

				foreach ($status->plugins as $plugin)
				{
			?>
			<tr class="row<?php
				echo ($rows++ % 2);
			?> <?php
				echo ($plugin['result'])? "success" : "error";
			?>">
				<td class="key"><?php echo ucfirst($plugin['name']); ?></td>
				<td class="key"><?php echo ucfirst($plugin['group']); ?></td>
				<td><?php echo ($plugin['result'])?'Installed':'Not installed'; ?>
				</td>
			</tr>
			<?php
				}

			?>
			<?php
			}

			?>

			<tr class="row1 <?php echo $fofStatus['required'] ? ($fofStatus['installed']?'success':'error') : 'info'; ?> ">
				<td class="key" colspan="2">Framework on Framework (FOF) <?php echo $fofStatus['version']?> [<?php echo $fofStatus['date'] ?>]
				</td>
				<td><span style=""> <?php echo $fofStatus['required'] ? ($fofStatus['installed'] ?'Installed':'Not Installed') : 'Already up-to-date'; ?>
				</span>
				</td>
			</tr>
		</tbody>
	</table>

	<h2>Tutorials</h2>

	<h3>Tutorial: How to AutoTweet from Joomla in 5 minutes</h2>

	<p><a target="_blank" href="http://dzg4p0ib0opco.cloudfront.net/https://docs.google.com/presentation/d/16hGWWGq1IeAZcAkSd8BqnzS2dBDCoC42om6O4bs8gKg/edit?usp=sharing">How to AutoTweet from Joomla in 5 minutes - Online Web Presentation</a></p>
	<p><a href="http://dzg4p0ib0opco.cloudfront.net/media/PPT/How_to_AutoTweet_from_Joomla_in_5_minutes.pdf">Adobe PDF</a> | <a href="http://dzg4p0ib0opco.cloudfront.net/media/PPT/How_to_AutoTweet_from_Joomla_in_5_minutes.pptx">Microsoft PowerPoint PPTX</a></p>
	<p><a target="_blank" href="http://dzg4p0ib0opco.cloudfront.net/https://docs.google.com/presentation/d/16hGWWGq1IeAZcAkSd8BqnzS2dBDCoC42om6O4bs8gKg/edit?usp=sharing"><img alt="How to AutoTweet from Joomla in 5 minutes" src="http://www.extly.com/images/autotweet-documentation/How_to_AutoTweet_from_Joomla_in_5_minutes.jpg" height="300" width="480" /></a></p>

	<ul style="list-style-type: none;">
		<li><a href="http://www.extly.com/how-to-autotweet-from-your-own-facebook-app.html" target="_blank">How to AutoTweet from Your Own Facebook App</a></li>
		<li><a href="http://www.extly.com/how-to-autotweet-from-your-own-facebook-heroku-app.html" target="_blank"> How to AutoTweet from your own Facebook-Heroku App (No SSL certificate)</a></li>
	</ul>

	<hr />

	<p>
		If you have any question, please, don't hesitate to contact us.<br /> Technical Support: <a href="http://support.extly.com" target="_blank">http://support.extly.com</a>
	</p>

	<p>
		We are passionately committed to your success.<br /> Support Team<br /> <strong>Extly.com - Extensions</strong><br /> <a href="http://support.extly.com" target="_blank">http://support.extly.com</a>
		| <a href="http://twitter.com/extly" target="_blank">@extly</a> | <a href="http://www.facebook.com/extly" target="_blank">facebook.com/extly</a>
	</p>


</div>

<?php
	}

	/**
	 * _renderPostUninstallation
	 *
	 * @param   bool        $status  Param
	 * @param   JInstaller  $parent  Param
	 *
	 * @return void
	 */
	private function _renderPostUninstallation($status, $parent)
	{
		?>
<?php $rows = 0;?>
<h2>AutoTweetNG uninstallation status</h2>
<table class="adminlist table table-striped" width="100%">
	<thead>
		<tr>
			<th class="title" colspan="2"><?php echo JText::_('Extension'); ?></th>
			<th width="30%"><?php echo JText::_('Status'); ?></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="3"></td>
		</tr>
	</tfoot>
	<tbody>
		<tr class="row0 success">
			<td class="key" colspan="2"><?php echo 'AutoTweetNG ' . JText::_('Component'); ?></td>
			<td><strong><?php echo JText::_('Removed'); ?> </strong></td>
		</tr>
		<?php

		if (count($status->modules))
		{
		?>
		<tr>
			<th><?php echo JText::_('Module'); ?></th>
			<th><?php echo JText::_('Client'); ?></th>
			<th></th>
		</tr>
		<?php

			foreach ($status->modules as $module)
			{
		?>
		<tr class="row<?php

			echo (++ $rows % 2);

			?> <?php

			echo ($module['result'])? "success" : "error";

			?>">
			<td class="key"><?php echo $module['name']; ?></td>
			<td class="key"><?php echo ucfirst($module['client']); ?></td>
			<td><strong><?php echo ($module['result'])?JText::_('Removed'):JText::_('Not removed'); ?> </strong></td>
		</tr>
		<?php
			}
		}

		if (count($status->plugins))
		{
		?>
		<tr>
			<th><?php echo JText::_('Plugin'); ?></th>
			<th><?php echo JText::_('Group'); ?></th>
			<th></th>
		</tr>
		<?php

			foreach ($status->plugins as $plugin)
			{
		?>
		<tr class="row<?php
		echo (++ $rows % 2);
		?> <?php
		echo ($plugin['result'])? "success" : "error";
		?>">
			<td class="key"><?php echo ucfirst($plugin['name']); ?></td>
			<td class="key"><?php echo ucfirst($plugin['group']); ?></td>
			<td><strong><?php echo ($plugin['result'])?JText::_('Removed'):JText::_('Not removed'); ?> </strong></td>
		</tr>
		<?php
			}
		}
		?>
	</tbody>
</table>
<p></p>
<p></p>
<?php
	}

	/**
	 * Based on script.akeeba.php / AkeebaBackup
	 * Copyright (c)2009-2013 Nicholas K. Dionysopoulos
	 * GNU General Public License version 3, or later
	 */

	/**
	 * Joomla! 1.6+ bugfix for "DB function returned no error"
	 *
	 * @return void
	 */
	private function _bugfixDBFunctionReturnedNoError()
	{
		$db = JFactory::getDbo();

		// Fix broken #__assets records
		$query = $db->getQuery(true);
		$query->select('id')
			->from('#__assets')
			->where($db->qn('name') . ' = ' . $db->q($this->_extly_extension));
		$db->setQuery($query);

		try
		{
			$ids = $db->loadColumn();
		}
		catch (Exception $exc)
		{
			return;
		}

		if (!empty($ids))
			foreach ($ids as $id)
			{
				$query = $db->getQuery(true);
				$query->delete('#__assets')
					->where($db->qn('id') . ' = ' . $db->q($id));
				$db->setQuery($query);

				try
				{
					$db->execute();
				}
				catch (Exception $exc)
				{
					// Nothing
				}
			}

		// Fix broken #__extensions records
		$query	 = $db->getQuery(true);
		$query->select('extension_id')
			->from('#__extensions')
			->where($db->qn('element') . ' = ' . $db->q($this->_extly_extension));
		$db->setQuery($query);
		$ids	 = $db->loadColumn();

		if (!empty($ids))
		{
			foreach ($ids as $id)
			{
				$query = $db->getQuery(true);
				$query->delete('#__extensions')
					->where($db->qn('extension_id') . ' = ' . $db->q($id));
				$db->setQuery($query);

				try
				{
					$db->execute();
				}
				catch (Exception $exc)
				{
					// Nothing
				}
			}
		}

		// Fix broken #__menu records
		$query	 = $db->getQuery(true);
		$query->select('id')
			->from('#__menu')
			->where($db->qn('type') . ' = ' . $db->q('component'))
			->where($db->qn('menutype') . ' = ' . $db->q('main'))
			->where($db->qn('link') . ' LIKE ' . $db->q('index.php?option=' . $this->_extly_extension));
		$db->setQuery($query);
		$ids	 = $db->loadColumn();

		if (!empty($ids))
		{
			foreach ($ids as $id)
			{
				$query = $db->getQuery(true);
				$query->delete('#__menu')
					->where($db->qn('id') . ' = ' . $db->q($id));
				$db->setQuery($query);

				try
				{
					$db->execute();
				}
				catch (Exception $exc)
				{
					// Nothing
				}
			}
		}
	}

	/**
	 * Joomla! 1.6+ bugfix for "Can not build admin menus"
	 *
	 * @return void
	 */
	private function _bugfixCantBuildAdminMenus()
	{
		$db = JFactory::getDbo();

		// If there are multiple #__extensions record, keep one of them
		$query	 = $db->getQuery(true);
		$query->select('extension_id')
			->from('#__extensions')
			->where($db->qn('element') . ' = ' . $db->q($this->_extly_extension));
		$db->setQuery($query);

		try
		{
			$ids = $db->loadColumn();
		}
		catch (Exception $exc)
		{
			return;
		}

		if (count($ids) > 1)
		{
			asort($ids);

			// Keep the oldest id
			$extension_id = array_shift($ids);

			foreach ($ids as $id)
			{
				$query = $db->getQuery(true);
				$query->delete('#__extensions')
					->where($db->qn('extension_id') . ' = ' . $db->q($id));
				$db->setQuery($query);

				try
				{
					$db->execute();
				}
				catch (Exception $exc)
				{
					// Nothing
				}
			}
		}

		// If there are multiple assets records, delete all except the oldest one
		$query	 = $db->getQuery(true);
		$query->select('id')
			->from('#__assets')
			->where($db->qn('name') . ' = ' . $db->q($this->_extly_extension));
		$db->setQuery($query);
		$ids	 = $db->loadObjectList();

		if (count($ids) > 1)
		{
			asort($ids);

			// Keep the oldest id
			$asset_id = array_shift($ids);

			foreach ($ids as $id)
			{
				$query = $db->getQuery(true);
				$query->delete('#__assets')
					->where($db->qn('id') . ' = ' . $db->q($id));
				$db->setQuery($query);

				try
				{
					$db->execute();
				}
				catch (Exception $exc)
				{
					// Nothing
				}
			}
		}

		// Remove #__menu records for good measure!
		$query	 = $db->getQuery(true);
		$query->select('id')
			->from('#__menu')
			->where($db->qn('type') . ' = ' . $db->q('component'))
			->where($db->qn('menutype') . ' = ' . $db->q('main'))
			->where($db->qn('link') . ' LIKE ' . $db->q('index.php?option=' . $this->_extly_extension));
		$db->setQuery($query);

		try
		{
			$ids1 = $db->loadColumn();
		}
		catch (Exception $exc)
		{
			$ids1 = array();
		}

		if (empty($ids1))
		{
			$ids1	 = array();
		}

		$query	 = $db->getQuery(true);
		$query->select('id')
			->from('#__menu')
			->where($db->qn('type') . ' = ' . $db->q('component'))
			->where($db->qn('menutype') . ' = ' . $db->q('main'))
			->where($db->qn('link') . ' LIKE ' . $db->q('index.php?option=' . $this->_extly_extension . '&%'));
		$db->setQuery($query);

		try
		{
			$ids2 = $db->loadColumn();
		}
		catch (Exception $exc)
		{
			$ids2 = array();
		}

		if (empty($ids2))
		{
			$ids2 = array();
		}

		$ids = array_merge($ids1, $ids2);

		if (!empty($ids))
		{
			foreach ($ids as $id)
			{
				$query = $db->getQuery(true);
				$query->delete('#__menu')
					->where($db->qn('id') . ' = ' . $db->q($id));
				$db->setQuery($query);

				try
				{
					$db->execute();
				}
				catch (Exception $exc)
				{
					// Nothing
				}
			}
		}
	}

	/**
	 * Installs subextensions (modules, plugins) bundled with the main extension
	 *
	 * @param   JInstaller  $parent  Param
	 *
	 * @return JObject The subextension installation status
	 */
	private function _installSubextensions($parent)
	{
		$src = $parent->getParent()->getPath('source');

		$db = JFactory::getDbo();

		$status			 = new JObject;
		$status->modules = array();
		$status->plugins = array();

		// Modules installation
		if (count($this->installation_queue['modules']))
		{
			foreach ($this->installation_queue['modules'] as $folder => $modules)
			{
				if (count($modules))
				{
					foreach ($modules as $module => $modulePreferences)
					{
						// Install the module
						if (empty($folder))
						{
							$folder	 = 'site';
						}

						$path	 = "$src/modules/$folder/$module";

						if (!is_dir($path))
						{
							$path = "$src/modules/$folder/mod_$module";
						}

						if (!is_dir($path))
						{
							$path = "$src/modules/$module";
						}

						if (!is_dir($path))
						{
							$path = "$src/modules/mod_$module";
						}

						if (!is_dir($path))
						{
							continue;
						}

						// Was the module already installed?
						$sql = $db->getQuery(true)
							->select('COUNT(*)')
							->from('#__modules')
							->where($db->qn('module') . ' = ' . $db->q('mod_' . $module));
						$db->setQuery($sql);

						try
						{
							$count = $db->loadResult();
						}
						catch (Exception $exc)
						{
							$count = 0;
						}

						$installer			 = new JInstaller;
						$result				 = $installer->install($path);
						$status->modules[]	 = array(
							'name'	 => 'mod_' . $module,
							'client' => $folder,
							'result' => $result
						);

						// Modify where it's published and its published state
						if (!$count)
						{
							// A. Position and state
							list($modulePosition, $modulePublished) = $modulePreferences;

							if ($modulePosition == 'cpanel')
							{
								$modulePosition = 'icon';
							}

							$sql = $db->getQuery(true)
								->update($db->qn('#__modules'))
								->set($db->qn('position') . ' = ' . $db->q($modulePosition))
								->where($db->qn('module') . ' = ' . $db->q('mod_' . $module));

							if ($modulePublished)
							{
								$sql->set($db->qn('published') . ' = ' . $db->q('1'));
							}

							$db->setQuery($sql);

							try
							{
								$db->execute();
							}
							catch (Exception $exc)
							{
								// Nothing
							}

							// B. Change the ordering of back-end modules to 1 + max ordering
							if ($folder == 'admin')
							{
								try
								{
									$query		 = $db->getQuery(true);
									$query->select('MAX(' . $db->qn('ordering') . ')')
										->from($db->qn('#__modules'))
										->where($db->qn('position') . '=' . $db->q($modulePosition));
									$db->setQuery($query);
									$position = $db->loadResult();
									$position++;

									$query = $db->getQuery(true);
									$query->update($db->qn('#__modules'))
										->set($db->qn('ordering') . ' = ' . $db->q($position))
										->where($db->qn('module') . ' = ' . $db->q('mod_' . $module));
									$db->setQuery($query);
									$db->execute();
								}
								catch (Exception $exc)
								{
									// Nothing
								}
							}

							// C. Link to all pages
							try
							{
								$query		 = $db->getQuery(true);
								$query->select('id')->from($db->qn('#__modules'))
									->where($db->qn('module') . ' = ' . $db->q('mod_' . $module));
								$db->setQuery($query);
								$moduleid	 = $db->loadResult();

								$query		 = $db->getQuery(true);
								$query->select('*')->from($db->qn('#__modules_menu'))
									->where($db->qn('moduleid') . ' = ' . $db->q($moduleid));
								$db->setQuery($query);
								$assignments = $db->loadObjectList();
								$isAssigned	 = !empty($assignments);

								if (!$isAssigned)
								{
									$o = (object) array(
											'moduleid'	 => $moduleid,
											'menuid'	 => 0
									);
									$db->insertObject('#__modules_menu', $o);
								}
							}
							catch (Exception $exc)
							{
								// Nothing
							}
						}
					}
				}
			}
		}

		// Plugins installation
		if (count($this->installation_queue['plugins']))
		{
			foreach ($this->installation_queue['plugins'] as $folder => $plugins)
			{
				if (count($plugins))
				{
					foreach ($plugins as $plugin => $published)
					{
						$path = "$src/plugins/$folder/$plugin";

						if (!is_dir($path))
						{
							$path = "$src/plugins/$folder/plg_$plugin";
						}

						if (!is_dir($path))
						{
							$path = "$src/plugins/$plugin";
						}

						if (!is_dir($path))
						{
							$path = "$src/plugins/plg_$plugin";
						}

						if (!is_dir($path))
						{
							continue;
						}

						// Was the plugin already installed?
						$query	 = $db->getQuery(true)
							->select('COUNT(*)')
							->from($db->qn('#__extensions'))
							->where($db->qn('element') . ' = ' . $db->q($plugin))
							->where($db->qn('folder') . ' = ' . $db->q($folder));
						$db->setQuery($query);

						try
						{
							$count = $db->loadResult();
						}
						catch (Exception $exc)
						{
							$count = 0;
						}

						$installer	 = new JInstaller;
						$result		 = $installer->install($path);

						$status->plugins[] = array('name'	 => 'plg_' . $plugin, 'group'	 => $folder, 'result' => $result);

						if ($published && !$count)
						{
							$query = $db->getQuery(true)
								->update($db->qn('#__extensions'))
								->set($db->qn('enabled') . ' = ' . $db->q('1'))
								->where($db->qn('element') . ' = ' . $db->q($plugin))
								->where($db->qn('folder') . ' = ' . $db->q($folder));
							$db->setQuery($query);

							try
							{
								$db->execute();
							}
							catch (Exception $exc)
							{
								// Nothing
							}
						}
					}
				}
			}
		}

		return $status;
	}

	/**
	 * Uninstalls subextensions (modules, plugins) bundled with the main extension
	 *
	 * @param   JInstaller  $parent  Param
	 *
	 * @return JObject The subextension uninstallation status
	 */
	private function _uninstallSubextensions($parent)
	{
		JLoader::import('joomla.installer.installer');

		$db = JFactory::getDBO();

		$status			 = new JObject;
		$status->modules = array();
		$status->plugins = array();

		$src = $parent->getParent()->getPath('source');

		// Modules uninstallation
		if (count($this->installation_queue['modules']))
		{
			foreach ($this->installation_queue['modules'] as $folder => $modules)
			{
				if (count($modules))
				{
					foreach ($modules as $module => $modulePreferences)
					{
						// Find the module ID
						$sql = $db->getQuery(true)
							->select($db->qn('extension_id'))
							->from($db->qn('#__extensions'))
							->where($db->qn('element') . ' = ' . $db->q('mod_' . $module))
							->where($db->qn('type') . ' = ' . $db->q('module'));
						$db->setQuery($sql);

						try
						{
							$id = $db->loadResult();
						}
						catch (Exception $exc)
						{
							$id = 0;
						}

						// Uninstall the module
						if ($id)
						{
							$installer			 = new JInstaller;
							$result				 = $installer->uninstall('module', $id, 1);
							$status->modules[]	 = array(
								'name'	 => 'mod_' . $module,
								'client' => $folder,
								'result' => $result
							);
						}
					}
				}
			}
		}

		// Plugins uninstallation
		if (count($this->installation_queue['plugins']))
		{
			foreach ($this->installation_queue['plugins'] as $folder => $plugins)
			{
				if (count($plugins))
				{
					foreach ($plugins as $plugin => $published)
					{
						$sql = $db->getQuery(true)
							->select($db->qn('extension_id'))
							->from($db->qn('#__extensions'))
							->where($db->qn('type') . ' = ' . $db->q('plugin'))
							->where($db->qn('element') . ' = ' . $db->q($plugin))
							->where($db->qn('folder') . ' = ' . $db->q($folder));
						$db->setQuery($sql);

						try
						{
							$id = $db->loadResult();
						}
						catch (Exception $exc)
						{
							$id = 0;
						}

						if ($id)
						{
							$installer			 = new JInstaller;
							$result				 = $installer->uninstall('plugin', $id, 1);
							$status->plugins[]	 = array(
								'name'	 => 'plg_' . $plugin,
								'group'	 => $folder,
								'result' => $result
							);
						}
					}
				}
			}
		}

		return $status;
	}

	/**
	 * Removes obsolete files and folders
	 *
	 * @param   array  $extlyRemoveFiles  Param
	 *
	 * @return void
	 */
	private function _removeObsoleteFilesAndFolders($extlyRemoveFiles)
	{
		// Remove files
		JLoader::import('joomla.filesystem.file');

		if (!empty($extlyRemoveFiles['files']))
		{
			foreach ($extlyRemoveFiles['files'] as $file)
			{
				$f = JPATH_ROOT . '/' . $file;

				if (!JFile::exists($f))
				{
					continue;
				}

				JFile::delete($f);
			}
		}

		// Remove folders
		JLoader::import('joomla.filesystem.file');

		if (!empty($extlyRemoveFiles['folders']))
		{
			foreach ($extlyRemoveFiles['folders'] as $folder)
			{
				$f = JPATH_ROOT . '/' . $folder;

				if (!JFolder::exists($f))
				{
					continue;
				}

				JFolder::delete($f);
			}
		}
	}

	/**
	 * _installFOF
	 *
	 * @param   JInstaller  $parent  Param
	 *
	 * @return void
	 */
	private function _installFOF($parent)
	{
		$src = $parent->getParent()->getPath('source');

		// Install the FOF framework
		JLoader::import('joomla.filesystem.folder');
		JLoader::import('joomla.filesystem.file');
		JLoader::import('joomla.utilities.date');
		$source = $src . '/fof';

		if (!defined('JPATH_LIBRARIES'))
		{
			$target = JPATH_ROOT . '/libraries/fof';
		}
		else
		{
			$target = JPATH_LIBRARIES . '/fof';
		}

		$haveToInstallFOF = false;

		if (!JFolder::exists($target))
		{
			$haveToInstallFOF = true;
		}
		else
		{
			$fofVersion = array();

			if (JFile::exists($target . '/version.txt'))
			{
				$rawData				 = JFile::read($target . '/version.txt');
				$info					 = explode("\n", $rawData);
				$fofVersion['installed'] = array(
					'version'	 => trim($info[0]),
					'date'		 => new JDate(trim($info[1]))
				);
			}
			else
			{
				$fofVersion['installed'] = array(
					'version'	 => '0.0',
					'date'		 => new JDate('2011-01-01')
				);
			}

			$rawData				 = JFile::read($source . '/version.txt');
			$info					 = explode("\n", $rawData);

			$fofVersion['package']	 = array(
				'version'	 => trim($info[0]),
				'date'		 => new JDate(trim($info[1]))
			);

			$haveToInstallFOF = $fofVersion['package']['date']->toUNIX() > $fofVersion['installed']['date']->toUNIX();

			// Do not install FOF on Joomla! 3.2.0 beta 1 or later
			if (version_compare(JVERSION, '3.1.999', 'gt'))
			{
				$haveToInstallFOF = false;
			}
		}

		$installedFOF = false;

		if ($haveToInstallFOF)
		{
			$versionSource	 = 'package';
			$installer		 = new JInstaller;
			$installedFOF	 = $installer->install($source);
		}
		else
		{
			$versionSource = 'installed';
		}

		if (!isset($fofVersion))
		{
			$fofVersion = array();

			if (JFile::exists($target . '/version.txt'))
			{
				$rawData				 = JFile::read($target . '/version.txt');
				$info					 = explode("\n", $rawData);
				$fofVersion['installed'] = array(
					'version'	 => trim($info[0]),
					'date'		 => new JDate(trim($info[1]))
				);
			}
			else
			{
				$fofVersion['installed'] = array(
					'version'	 => '0.0',
					'date'		 => new JDate('2011-01-01')
				);
			}

			$rawData				 = JFile::read($source . '/version.txt');
			$info					 = explode("\n", $rawData);

			$fofVersion['package']	 = array(
				'version'	 => trim($info[0]),
				'date'		 => new JDate(trim($info[1]))
			);

			$versionSource			 = 'installed';
		}

		if (!($fofVersion[$versionSource]['date'] instanceof JDate))
		{
			$fofVersion[$versionSource]['date'] = new JDate;
		}

		return array(
			'required'	 => $haveToInstallFOF,
			'installed'	 => $installedFOF,
			'version'	 => $fofVersion[$versionSource]['version'],
			'date'		 => $fofVersion[$versionSource]['date']->format('Y-m-d'),
		);
	}

	/**
	* _installStraper
	*
	* @param   JInstaller  $parent  Param
	*
	* @return void
	*/
	private function _installStraper($parent)
	{
		$src = $parent->getParent()->getPath('source');

		// Install the FOF framework
		JLoader::import('joomla.filesystem.folder');
		JLoader::import('joomla.filesystem.file');
		JLoader::import('joomla.utilities.date');

		/*
		$source	 = $src . '/strapper';
		$target	 = JPATH_ROOT . '/media/extly_strapper';
		*/
		$source = $src . '/strapper/lib_extly';
		$target = JPATH_ROOT . '/libraries/extly';

		$haveToInstallStraper = false;

		if (!JFolder::exists($target))
		{
			$haveToInstallStraper = true;
		}
		else
		{
			$straperVersion = array();

			if (JFile::exists($target . '/version.txt'))
			{
				$rawData					 = JFile::read($target . '/version.txt');
				$info						 = explode("\n", $rawData);
				$straperVersion['installed'] = array(
					'version'	 => trim($info[0]),
					'date'		 => new JDate(trim($info[1]))
				);
			}
			else
			{
				$straperVersion['installed'] = array(
					'version'	 => '0.0',
					'date'		 => new JDate('2011-01-01')
				);
			}

			$rawData					 = JFile::read($source . '/version.txt');
			$info						 = explode("\n", $rawData);
			$straperVersion['package']	 = array(
				'version'	 => trim($info[0]),
				'date'		 => new JDate(trim($info[1]))
			);

			$haveToInstallStraper = $straperVersion['package']['date']->toUNIX() > $straperVersion['installed']['date']->toUNIX();
		}

		$installedStraper = false;

		if ($haveToInstallStraper)
		{
			$versionSource		 = 'package';
			$installer			 = new JInstaller;
			$installedStraper	 = $installer->install($source);
		}
		else
		{
			$versionSource = 'installed';
		}

		if (!isset($straperVersion))
		{
			$straperVersion = array();

			if (JFile::exists($target . '/version.txt'))
			{
				$rawData					 = JFile::read($target . '/version.txt');
				$info						 = explode("\n", $rawData);
				$straperVersion['installed'] = array(
					'version'	 => trim($info[0]),
					'date'		 => new JDate(trim($info[1]))
				);
			}
			else
			{
				$straperVersion['installed'] = array(
					'version'	 => '0.0',
					'date'		 => new JDate('2011-01-01')
				);
			}

			$rawData					 = JFile::read($source . '/version.txt');
			$info						 = explode("\n", $rawData);
			$straperVersion['package']	 = array(
				'version'	 => trim($info[0]),
				'date'		 => new JDate(trim($info[1]))
			);
			$versionSource				 = 'installed';
		}

		if (!($straperVersion[$versionSource]['date'] instanceof JDate))
		{
			$straperVersion[$versionSource]['date'] = new JDate;
		}

		return array(
			'required'	 => $haveToInstallStraper,
			'installed'	 => $installedStraper,
			'version'	 => $straperVersion[$versionSource]['version'],
			'date'		 => $straperVersion[$versionSource]['date']->format('Y-m-d'),
		);
	}

	/**
	 * Remove the update site specification from Joomla! – we no longer support
	 * that misbehaving crap, thank you very much...
	 *
	 * @return void
	 */
	private function _killUpdateSite()
	{
		// Get some info on all the stuff we've gotta delete
		$db		 = JFactory::getDbo();
		$query	 = $db->getQuery(true)
			->select(
			array(
				$db->qn('s') . '.' . $db->qn('update_site_id'),
				$db->qn('e') . '.' . $db->qn('extension_id'),
				$db->qn('e') . '.' . $db->qn('element'),
				$db->qn('s') . '.' . $db->qn('location'),
				)
			)
			->from($db->qn('#__update_sites') . ' AS ' . $db->qn('s'))
			->join('INNER', $db->qn('#__update_sites_extensions') . ' AS ' . $db->qn('se') . ' ON(' .
				$db->qn('se') . '.' . $db->qn('update_site_id') . ' = ' .
				$db->qn('s') . '.' . $db->qn('update_site_id')
				. ')')
			->join('INNER', $db->qn('#__extensions') . ' AS ' . $db->qn('e') . ' ON(' .
				$db->qn('e') . '.' . $db->qn('extension_id') . ' = ' .
				$db->qn('se') . '.' . $db->qn('extension_id')
				. ')')
			->where($db->qn('s') . '.' . $db->qn('type') . ' = ' . $db->q('extension'))
			->where($db->qn('e') . '.' . $db->qn('type') . ' = ' . $db->q('component'))
			->where($db->qn('e') . '.' . $db->qn('element') . ' = ' . $db->q($this->_extly_extension));
		$db->setQuery($query);

		try
		{
			$oResult = $db->loadObject();
		}
		catch (Exception $exc)
		{
			return;
		}

		// If no record is found, do nothing. We've already killed the monster!
		if (is_null($oResult))
		{
			return;
		}

		// Delete the #__update_sites record
		$query = $db->getQuery(true)
			->delete($db->qn('#__update_sites'))
			->where($db->qn('update_site_id') . ' = ' . $db->q($oResult->update_site_id));
		$db->setQuery($query);

		try
		{
			$db->execute();
		}
		catch (Exception $exc)
		{
			// If the query fails, don't sweat about it
		}

		// Delete the #__update_sites_extensions record
		$query = $db->getQuery(true)
			->delete($db->qn('#__update_sites_extensions'))
			->where($db->qn('update_site_id') . ' = ' . $db->q($oResult->update_site_id));
		$db->setQuery($query);

		try
		{
			$db->execute();
		}
		catch (Exception $exc)
		{
			// If the query fails, don't sweat about it
		}

		// Delete the #__updates records
		$query = $db->getQuery(true)
			->delete($db->qn('#__updates'))
			->where($db->qn('update_site_id') . ' = ' . $db->q($oResult->update_site_id));
		$db->setQuery($query);

		try
		{
			$db->execute();
		}
		catch (Exception $exc)
		{
			// If the query fails, don't sweat about it
		}
	}

	/**
	 * When you are upgrading from an old version of the component or when your
	 * site is upgraded from Joomla! 1.5 there is no "schema version" for our
	 * component's tables. As a result Joomla! doesn't run the database queries
	 * and you get a broken installation.
	 *
	 * This method detects this situation, forces a fake schema version "0.0.1"
	 * and lets the crufty mess Joomla!'s extensions installer is to bloody work
	 * as anyone would have expected it to do!
	 *
	 * @return void
	 */
	private function _fixSchemaVersion()
	{
		// Get the extension ID
		$db = JFactory::getDbo();

		$query	 = $db->getQuery(true);
		$query->select('extension_id')
			->from('#__extensions')
			->where($db->qn('element') . ' = ' . $db->q($this->_extly_extension));
		$db->setQuery($query);

		try
		{
			$eid = $db->loadResult();
		}
		catch (Exception $exc)
		{
			return;
		}

		if (!$eid)
		{
			return;
		}

		$query	 = $db->getQuery(true);
		$query->select('version_id')
			->from('#__schemas')
			->where('extension_id = ' . $eid);
		$db->setQuery($query);

		try
		{
			$version = $db->loadResult();
		}
		catch (Exception $exc)
		{
			$version = false;
		}

		if (!$version)
		{
			// No schema version found. Fix it.
			$o = (object) array(
					'version_id'	 => '0.0.1-2007-08-15',
					'extension_id'	 => $eid,
			);
			$db->insertObject('#__schemas', $o);
		}
	}

	/**
	 * Let's say that a user tries to install a component and it somehow fails
	 * in a non-graceful manner, e.g. a server timeout error, going over the
	 * quota etc. In this case the component's administrator directory is
	 * created and not removed (because the installer died an untimely death).
	 * When the user retries installing the component JInstaller sees that and
	 * thinks it's an update. This causes it to neither run the installation SQL
	 * file (because it's not supposed to run on extension update) nor the
	 * update files (because there is no schema version defined). As a result
	 * the files are installed, the database tables are not, the component is
	 * broken and I have to explain to non-technical users how to edit their
	 * database with phpMyAdmin.
	 *
	 * This method detects this stupid situation and attempts to execute the
	 * installation file instead.
	 *
	 * @param   JInstaller  $parent  Param
	 *
	 * @return void
	 */
	private function _fixBrokenSQLUpdates($parent)
	{
		// Get the extension ID
		$db = JFactory::getDbo();

		$query	 = $db->getQuery(true);
		$query->select('extension_id')
			->from('#__extensions')
			->where($db->qn('element') . ' = ' . $db->q($this->_extly_extension));
		$db->setQuery($query);

		try
		{
			$eid = $db->loadResult();
		}
		catch (Exception $exc)
		{
			return;
		}

		if (!$eid)
		{
			return;
		}

		// Get the schema version
		$query	 = $db->getQuery(true);
		$query->select('version_id')
			->from('#__schemas')
			->where('extension_id = ' . $eid);
		$db->setQuery($query);

		try
		{
			$version = $db->loadResult();
		}
		catch (Exception $exc)
		{
			return;
		}

		// If there is a schema version it's not a false update
		if ($version)
		{
			return;
		}

		// Execute the installation SQL file. Since I don't have access to
		// the manifest, I will improvise (again!)
		$dbDriver = strtolower($db->name);

		if ($dbDriver == 'mysqli')
		{
			$dbDriver = 'mysql';
		}
		elseif ($dbDriver == 'sqlsrv')
		{
			$dbDriver = 'sqlazure';
		}

		// Get the name of the sql file to process
		$sqlfile = $parent->getParent()->getPath('source') . '/backend/sql/install/' . $dbDriver . '/install.sql';

		if (file_exists($sqlfile))
		{
			$buffer = file_get_contents($sqlfile);

			if ($buffer === false)
			{
				return;
			}

			$queries = JInstallerHelper::splitSql($buffer);

			if (count($queries) == 0)
			{
				// No queries to process
				return;
			}

			// Process each query in the $queries array (split out of sql file).
			foreach ($queries as $query)
			{
				$query = trim($query);

				if ($query != '' && $query{0} != '#')
				{
					$db->setQuery($query);

					try
					{
						$result = $db->execute();
					}
					catch (Exception $exc)
					{
						$result = false;
					}

					if (!$result)
					{
						JError::raiseWarning(1, JText::sprintf('JLIB_INSTALLER_ERROR_SQL_ERROR', $db->stderr(true)));

						return false;
					}
				}
			}
		}

		// Update #__schemas to the latest version. Again, since I don't have
		// access to the manifest I have to improvise...
		$path	 = $parent->getParent()->getPath('source') . '/backend/sql/update/' . $dbDriver;
		$files	 = str_replace('.sql', '', JFolder::files($path, '\.sql$'));

		if (count($files) > 0)
		{
			usort($files, 'version_compare');
			$version = array_pop($files);
		}
		else
		{
			$version = '0.0.1-2007-08-15';
		}

		$query = $db->getQuery(true);
		$query->insert($db->quoteName('#__schemas'));
		$query->columns(array($db->quoteName('extension_id'), $db->quoteName('version_id')));
		$query->values($eid . ', ' . $db->quote($version));
		$db->setQuery($query);

		try
		{
			$db->execute();
		}
		catch (Exception $exc)
		{
			// Nothing
		}
	}

	/**
	 * Deletes the Live Update information, forcing its reload during the first
	 * run of the component. This makes sure that the Live Update doesn't show
	 * an update available right after installing the component.
	 *
	 * @return void
	 */
	private function _resetLiveUpdate()
	{
		// Load the component parameters, not using JComponentHelper to avoid conflicts ;)
		JLoader::import('joomla.html.parameter');
		JLoader::import('joomla.application.component.helper');
		$db			 = JFactory::getDbo();
		$sql		 = $db->getQuery(true)
			->select($db->qn('params'))
			->from($db->qn('#__extensions'))
			->where($db->qn('type') . ' = ' . $db->q('component'))
			->where($db->qn('element') . ' = ' . $db->q($this->_extly_extension));
		$db->setQuery($sql);

		try
		{
			$rawparams	 = $db->loadResult();
		}
		catch (Exception $exc)
		{
			return;
		}

		$params	= new JRegistry;

		if (version_compare(JVERSION, '3.0', 'ge'))
		{
			$params->loadString($rawparams, 'JSON');
		}
		else
		{
			$params->loadJSON($rawparams);
		}

		// Reset the liveupdate key
		$params->set('liveupdate', null);

		// Save the modified component parameters
		$data	 = $params->toString();
		$sql	 = $db->getQuery(true)
			->update($db->qn('#__extensions'))
			->set($db->qn('params') . ' = ' . $db->q($data))
			->where($db->qn('type') . ' = ' . $db->q('component'))
			->where($db->qn('element') . ' = ' . $db->q($this->_extly_extension));

		$db->setQuery($sql);

		try
		{
			$db->execute();
		}
		catch (Exception $exc)
		{
			// Nothing
		}
	}
}
