<?php

/**
 * @package     Extly.Components
 * @subpackage  com_autotweet - AutoTweetNG posts content to social channels (Twitter, Facebook, LinkedIn, etc).
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

defined('AUTOTWEET_API') || define('AUTOTWEET_API', true);

defined('CAUTOTWEETNG') || define('CAUTOTWEETNG', 'com_autotweet');
defined('CAUTOTWEETNG_VERSION') || define('CAUTOTWEETNG_VERSION', '7.1.1');

// Load FOF
if (!defined('FOF_INCLUDED'))
{
	include_once JPATH_LIBRARIES . '/fof/include.php';
}

if (!defined('FOF_INCLUDED'))
{
	JError::raiseError(
	'500',
	'Your AutoTweetNG installation is broken; please re-install.
			Alternatively, extract the installation archive and copy the fof directory inside your site\'s libraries directory.');
}

defined('JPATH_AUTOTWEET') || define('JPATH_AUTOTWEET', JPATH_ADMINISTRATOR . '/components/com_autotweet');
defined('JPATH_AUTOTWEET_HELPERS') || define('JPATH_AUTOTWEET_HELPERS', JPATH_AUTOTWEET . '/helpers');

JLoader::import('extly.extlyframework');

JLoader::register('AclPermsHelper', JPATH_AUTOTWEET_HELPERS . '/acl.php');
JLoader::register('AdvancedattrsHelper', JPATH_AUTOTWEET_HELPERS . '/advancedattrs.php');
JLoader::register('AutotweetBaseHelper', JPATH_AUTOTWEET_HELPERS . '/autotweetbasehelper.php');
JLoader::register('AutotweetBitlyService', JPATH_AUTOTWEET_HELPERS . '/urlshortservices/autotweetbitly.php');
JLoader::register('AutotweetDefaultView', JPATH_AUTOTWEET_HELPERS . '/defaultview.php');
JLoader::register('AutotweetGooglService', JPATH_AUTOTWEET_HELPERS . '/urlshortservices/autotweetgoogl.php');
JLoader::register('AutotweetIsgdService', JPATH_AUTOTWEET_HELPERS . '/urlshortservices/autotweetisgd.php');
JLoader::register('AutotweetLogger', JPATH_AUTOTWEET_HELPERS . '/logger.php');
JLoader::register('AutotweetPostHelper', JPATH_AUTOTWEET_HELPERS . '/autotweetposthelper.php');
JLoader::register('AutotweetRenderBack25', JPATH_AUTOTWEET_HELPERS . '/render.php');
JLoader::register('AutotweetRenderBack3', JPATH_AUTOTWEET_HELPERS . '/render.php');
JLoader::register('AutotweetRenderFront25', JPATH_AUTOTWEET_HELPERS . '/render.php');
JLoader::register('AutotweetRenderFront3', JPATH_AUTOTWEET_HELPERS . '/render.php');
JLoader::register('AutotweetShortservice', JPATH_AUTOTWEET_HELPERS . '/urlshortservices/autotweetshortservice.php');
JLoader::register('AutotweetTinyurlcomService', JPATH_AUTOTWEET_HELPERS . '/urlshortservices/autotweettinyurlcom.php');
JLoader::register('AutotweetURLShortserviceFactory', JPATH_AUTOTWEET_HELPERS . '/urlshortservices/autotweeturlshortservicefactory.php');
JLoader::register('AutotweetYourlsService', JPATH_AUTOTWEET_HELPERS . '/urlshortservices/autotweetyourls.php');
JLoader::register('ChannelFactory', JPATH_AUTOTWEET_HELPERS . '/channels/channelfactory.php');
JLoader::register('ChannelHelper', JPATH_AUTOTWEET_HELPERS . '/channels/channel.php');
JLoader::register('CronjobHelper', JPATH_AUTOTWEET_HELPERS . '/cronjob.php');
JLoader::register('FacebookApp', JPATH_AUTOTWEET_HELPERS . '/facebookapp.php');
JLoader::register('FacebookBaseChannelHelper', JPATH_AUTOTWEET_HELPERS . '/channels/facebookbase.php');
JLoader::register('FacebookChannelHelper', JPATH_AUTOTWEET_HELPERS . '/channels/facebook.php');
JLoader::register('FacebookEventChannelHelper', JPATH_AUTOTWEET_HELPERS . '/channels/facebookevent.php');
JLoader::register('FacebookLinkChannelHelper', JPATH_AUTOTWEET_HELPERS . '/channels/facebooklink.php');
JLoader::register('FacebookPhotoChannelHelper', JPATH_AUTOTWEET_HELPERS . '/channels/facebookphoto.php');
JLoader::register('FacebookVideoChannelHelper', JPATH_AUTOTWEET_HELPERS . '/channels/facebookvideo.php');
JLoader::register('FbAppHelper', JPATH_AUTOTWEET_HELPERS . '/channels/fbapp.php');
JLoader::register('FeedLoaderHelper', JPATH_AUTOTWEET_HELPERS . '/feedloader.php');
JLoader::register('FeedImporterHelper', JPATH_AUTOTWEET_HELPERS . '/feedimporter.php');
JLoader::register('FeedTextHelper', JPATH_AUTOTWEET_HELPERS . '/feedtext.php');
JLoader::register('GplusAppHelper', JPATH_AUTOTWEET_HELPERS . '/channels/gplusapp.php');
JLoader::register('GplusChannelHelper', JPATH_AUTOTWEET_HELPERS . '/channels/gplus.php');
JLoader::register('GridHelper', JPATH_AUTOTWEET_HELPERS . '/grid.php');
JLoader::register('ImageHelper', JPATH_AUTOTWEET_HELPERS . '/image.php');
JLoader::register('LiAppHelper', JPATH_AUTOTWEET_HELPERS . '/channels/liapp.php');
JLoader::register('LinkedinBaseChannelHelper', JPATH_AUTOTWEET_HELPERS . '/channels/linkedinbase.php');
JLoader::register('LinkedinChannelHelper', JPATH_AUTOTWEET_HELPERS . '/channels/linkedin.php');
JLoader::register('LinkedinGroupChannelHelper', JPATH_AUTOTWEET_HELPERS . '/channels/linkedingroup.php');
JLoader::register('LinkedinCompanyChannelHelper', JPATH_AUTOTWEET_HELPERS . '/channels/linkedincompany.php');
JLoader::register('MailChannelHelper', JPATH_AUTOTWEET_HELPERS . '/channels/mail.php');
JLoader::register('PlgAutotweetBase', JPATH_AUTOTWEET_HELPERS . '/autotweetbase.php');
JLoader::register('PostHelper', JPATH_AUTOTWEET_HELPERS . '/post.php');
JLoader::register('RequestHelp', JPATH_AUTOTWEET_HELPERS . '/request.php');
JLoader::register('RouteHelp', JPATH_AUTOTWEET_HELPERS . '/route.php');
JLoader::register('RuleEngineHelper', JPATH_AUTOTWEET_HELPERS . '/ruleengine.php');
JLoader::register('SelectControlHelper', JPATH_AUTOTWEET_HELPERS . '/select.php');
JLoader::register('SharingHelper', JPATH_AUTOTWEET_HELPERS . '/sharing.php');
JLoader::register('ShorturlHelper', JPATH_AUTOTWEET_HELPERS . '/shorturl.php');
JLoader::register('TextUtil', JPATH_AUTOTWEET_HELPERS . '/text.php');
JLoader::register('TwAppHelper', JPATH_AUTOTWEET_HELPERS . '/channels/twapp.php');
JLoader::register('VkAppHelper', JPATH_AUTOTWEET_HELPERS . '/channels/vkapp.php');
JLoader::register('TwitterChannelHelper', JPATH_AUTOTWEET_HELPERS . '/channels/twitter.php');
JLoader::register('VirtualManager', JPATH_AUTOTWEET_HELPERS . '/virtualmanager.php');
JLoader::register('VkChannelHelper', JPATH_AUTOTWEET_HELPERS . '/channels/vk.php');
JLoader::register('VkGroupChannelHelper', JPATH_AUTOTWEET_HELPERS . '/channels/vk.php');
JLoader::register('UpdateNgHelper', JPATH_AUTOTWEET_HELPERS . '/update.php');
JLoader::register('VersionHelper', JPATH_AUTOTWEET_HELPERS . '/version.php');

JLoader::load('VersionHelper');

if (!defined('AUTOTWEET_LOGGER'))
{
	define('AUTOTWEET_LOGGER', true);
	AutotweetLogger::getInstance();
}

/**
 *
 * This class is a facade for the posting function of AutoTweet.
 * It should be used as an API for external components only.
 *
 * Instructions to enhance AutoTweetNG  for external component.
 *
 * There are two ways to use AutoTweet with your own component.
 *
 * 1. Write an extension plugin for AutoTweet
 * 2. Create a new request with the API function AutotweetAPI::insertRequest
 *
 * 1. Write an extension plugin for AutoTweet
 * ------------------------------------------
 *
 * Your plugin must derive form the base class  plgAutotweetBase (see
 * /administrator/components/com_autotweet/helpers/autotweetbase.php)
 * and must implement the function getData or getExtendedData to return
 * the data for the post. Also it must trigger the function
 * postStatusMessage to queue the message for posting. See my own
 * AutoTweetNG extension plugins as example.
 *
 * The Weblinks plugin is simple and has all the required features.
 *
 * 2. Insert a content Request with the API function  AutotweetAPI::insertRequest
 * ------------------------------------------------------------------------------
 *
 * With this function you can send a request over AutoTweetNG without
 * the need to write a plugin. So, you can call this function directly
 * from your component.
 *
 * AutoTweetNG v6.4 Breaking Changes
 * ---------------------------------
 * All messages follow the same processing rules:
 *
 * "Source" -> Request -> Post -> "Social Networks"
 *
 * - "Source" is a plugin, getData or getExtendedData are called to fill the message.

            ref_id			=	1231
            plugin			=	autotweetk2
            publish_up		=	2013-03-01 10:53:14
            description		=	Software Extends Integration with Widely-Used Accounting System
            typeinfo		=	0
            url				=
            image_url		=	http://www.domain.com/media/feed/images/monthly/2013/03/19_repost_bttn_tiny.png
            native_object	=	{"id":"1","title":"Software Extends Integration with Widely-Used
Accounting
System","alias":"software-extends-integration-with-widely-used-account
ing-system","catid":"1","published":"1","introtext":"<p>HOUSTON,
Texas, March 1, 2013 (SEND2PRESS NEWSWIRE) \u2014 Financial
Softworks, LLC announced today ....}

 * - A Manual "Post" is now also a Plugin, following the same rules.

            ref_id			=	1362433365
            plugin			=	autotweetpost
            publish_up		=	2013-03-04 21:42:45
            description		=	This a test
            typeinfo		=	NULL
            url				=	http://www.extly.com/
            image_url		=	http://demo.autotweetng.com/3/images/AutoTweeNG-B1-Compatibility-Chart.png
            native_object	=	{"title":"This a test message to fill this
field","article_text":"This the full text to fill this
field","hashtags":"#a-hashtag","catid":"9","author":"924","language":"
*","create_event":"0","location":"","street":"","city":"","start_time"
:"2013-03-04 21:42:45","end_time":""}

 * - If the "Source" is not plugin, getData or getExtendedData are not called and the Request
 * must have all required information for a valid Post.
 *
*/

/**
 * AutotweetAPI
 *
 * @package     Extly.Components
 * @subpackage  com_autotweet
 * @since       1.0
 */
abstract class AutotweetAPI
{
	/**
	 * __construct
	 *
	 * @since	1.5
	 */
	private function __construct()
	{
		// Nothing to do
	}

	/**
	 * insertRequest
	 *
	 * @param   string  $ref_id         Param
	 * @param   string  $plugin         Param
	 * @param   string  $publish_up     Param
	 * @param   string  $description    Param
	 * @param   string  $typeinfo       Param
	 * @param   string  $url            Param
	 * @param   string  $image_url      Param
	 * @param   json    $native_object  Param
	 *
	 * @return	bool
	 *
	 * @since	1.5
	 */
	public static function insertRequest(
		$ref_id,
		$plugin,
		$publish_up,
		$description,
		$typeinfo = 0,
		$url = '',
		$image_url = '',
		$native_object = null)
	{
		RequestHelp::insertRequest(
			$ref_id,
			$plugin,
			$publish_up,
			$description,
			$typeinfo,
			$url,
			$image_url,
			$native_object
		);
	}
}
