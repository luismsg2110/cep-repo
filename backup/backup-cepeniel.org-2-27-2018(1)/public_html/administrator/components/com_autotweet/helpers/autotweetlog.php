<?php
/**
* Logger for AutoTweet debug info.
*
* @version 1.4
* @author Ulli Storck
* @copyright (C) 2009-2011 Ulli Storck
* @license GNU/GPLv3 www.gnu.org/licenses/gpl-3.0.html
**/

// Check to ensure this file is included in Joomla!
defined( '_JEXEC' ) or die( 'Restricted access' );

require_once (JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_autotweet' . DS . 'helpers' . DS . 'autotweetutil.php');

if (AutotweetUtil::isJoomla17orLater()) {
	// Joomla 1.7 or higher
	jimport( 'joomla.log.log' );
}
else {
	// Joomla 1.6
	jimport( 'joomla.error.log' );
}

jimport( 'joomla.error.error' );


class AutotweetLog
{
	// status of logged entry
	const LOG_OK		= 'OK';
	const LOG_WARNING	= 'WARNING';
	const LOG_ERROR		= 'ERROR';
	
	protected $level	= 0;	// 0 = off, 1 = errors only, 2 = errors and warnings, 3 = all 
	protected $mode		= 0;	// 0 = to logfile only, 1 = to logfile and on screen
	protected $logger	= null;	// Joomla JLog
	
	private static $instance	= null;
	
	
	protected function AutotweetLog($log_level, $log_mode)
	{
		$this->level	= (int)$log_level;
		$this->mode		= (int)$log_mode;
		$this->logger	=& JLog::getInstance('autotweet.log');
	}
	
	public static function &getInstance($log_level, $log_mode)
	{
		if (!self::$instance) {
			self::$instance = new AutotweetLog($log_level, $log_mode);
		}
		
		return self::$instance;
	}
	
	public function log($status, $comment)
	{
		$log_result = false;
		
		if (0 < (int)$this->level)
		{
			// logging is enabled
			if (((self::LOG_OK == $status) && (3 <= (int)$this->level))
				|| ((self::LOG_WARNING == $status) && (2 <= (int)$this->level))
				|| ((self::LOG_ERROR == $status) && (1 <= (int)$this->level)))
			{
				if (empty($this->logger)) {
					JError::raiseWarning($this->level, 'AutoTweet NG Log - Warning: Logger not initialized, entry not written to logfile.');
				}
				else {
/*
					if (AutotweetUtil::isJoomla17orLater()) {
						// Joomla 1.7 or higher
						if ($status == self::LOG_OK) {
							$priority = JLog::INFO;
						}
						elseif ($status == self::LOG_WARNING) {
							$priority = JLog::WARNING;
						}
						elseif ($status == self::LOG_ERROR) {
							$priority = JLog::ERROR;
						}
						else {
							$priority = JLog::DEBUG;
						}
						
						JLog::add($comment, $priority, $this->level);
					}
					else {
						// Joomla 1.6
						$entry = array('level' => $this->level, 'status' => $status, 'comment' => $comment);
						$this->logger->addEntry($entry);
					}
*/

					// Joomla 1.6 (and as legacy with 1.7)
					$entry = array('level' => $this->level, 'status' => $status, 'comment' => $comment);
					@$this->logger->addEntry($entry);
				}
			}
			
			if (0 < (int)$this->mode)
			{
				// enhanced logging enabled: display messages also on screen
				$message = 'AutoTweet NG debug log - ' . $status . ': ' . htmlspecialchars($comment);
				
				if ((self::LOG_OK == $status) && (3 <= (int)$this->level)) {
					JError::raiseNotice($this->level, $message);
				}
				elseif (((self::LOG_WARNING == $status) && (2 <= (int)$this->level)) || ((self::LOG_ERROR == $status) && (1 <= (int)$this->level))) {
					JError::raiseWarning($this->level, $message);
				}
			}
		}
	
		return $log_result;
	}
	
}	
?>