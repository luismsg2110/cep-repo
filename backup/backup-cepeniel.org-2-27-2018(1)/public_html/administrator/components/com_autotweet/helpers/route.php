<?php

/**
 * @package     Extly.Components
 * @subpackage  com_autotweet - AutoTweetNG posts content to social channels (Twitter, Facebook, LinkedIn, etc).
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 *  Helper for SEF urls
*/

/**
 * RouteHelp
 *
 * @package     Extly.Components
 * @subpackage  com_autotweet
 * @since       1.0
*/
class RouteHelp
{
	// Routing via front end url call
	const ROUTING_MODE_COMPATIBILITY = 0;

	// Routing by initializing site routing in backend
	const ROUTING_MODE_PERFORMANCE = 1;

	// Language management
	const LANGMGMT_REMOVELANG = 1;
	const LANGMGMT_REPLACELANG = 2;

	protected $langmgmt_enabled = 0;

	protected $langmgmt_default_language = '';

	protected $routing_mode = 0;

	// Disable URL routing when wrong URLs are returned by Joomla
	protected $urlrouting_enabled = 1;

	// Base url overwrite
	protected $overwrite_baseurl = 0;

	protected $base_url = '';

	private static $_instance = null;

	/**
	 * RouteHelp
	 *
	 * @since	1.5
	 */
	protected function RouteHelp()
	{
		$this->langmgmt_enabled = EParameter::getComponentParam(CAUTOTWEETNG, 'langmgmt_enabled', 0);
		$this->langmgmt_default_language = EParameter::getComponentParam(CAUTOTWEETNG, 'langmgmt_default_language', '');

		$this->routing_mode = EParameter::getComponentParam(CAUTOTWEETNG, 'routing_mode', 0);

		// Base url overwrite
		$this->overwrite_baseurl = EParameter::getComponentParam(CAUTOTWEETNG, 'overwrite_baseurl', 0);
		$this->base_url = EParameter::getComponentParam(CAUTOTWEETNG, 'base_url', '');

		// Disable URL routing when wrong URLs are returned by Joomla
		$this->urlrouting_enabled = EParameter::getComponentParam(CAUTOTWEETNG, 'urlrouting_enabled', 1);
	}

	/**
	 * getInstance
	 *
	 * @return	Instance
	 *
	 * @since	1.5
	 */
	public static function &getInstance()
	{
		if (!self::$_instance)
		{
			self::$_instance = new RouteHelp;
		}

		return self::$_instance;
	}

	/**
	 * getAbsoluteUrl
	 *
	 * @param   string  $url       Param
	 * @param   string  $is_image  Param
	 *
	 * @return	string
	 */
	public function getAbsoluteUrl($url, $is_image = false)
	{
		$is_absolute_url = (JString::substr($url, 0, 4) == 'http');

		if ($is_absolute_url)
		{
			return $url;
		}
		else
		{
			if ($is_image)
			{
				return $this->routeImageUrl($url);
			}
			else
			{
				return $this->routeUrl($url);
			}
		}
	}

	/**
	 * Routes the URL.
	 * This is a substitute for the original Joomla route function JRoute::_
	 * because JRoute::_ does work from frontend only and has some special behavoir
	 * with image URLs.
	 *
	 * @param   string  $url  Param
	 *
	 * @return	String
	 *
	 * @since	1.5
	 */
	public function routeUrl($url)
	{
		$logger = AutotweetLogger::getInstance();

		$logger->log(JLog::INFO, 'internal url = ' . $url);

		if (!empty($url))
		{
			// Get (sef) url for frontend and backend
			if ($this->urlrouting_enabled)
			{
				$url = $this->build($url);
				$logger->log(JLog::INFO, 'routeURL: routed url = ' . $url);
			}
			else
			{
				$logger->log(JLog::WARNING, 'routeURL: url routing disabled');
			}

			// Check for language management mode and correct url language if needed
			if ($this->langmgmt_enabled)
			{
				$url = $this->correctUrlLang($url);
				$logger->log(JLog::INFO, 'routeURL: language corrected url = ' . $url);
			}

			$url = $this->createAbsoluteUrl($url);
		}

		$logger->log(JLog::INFO, 'routeURL: final full url = ' . $url);

		return $url;
	}

	/**
	 * Routes the Image.
	 *
	 * @param   string  $url  Param
	 *
	 * @return	String
	 *
	 * @since	1.5
	 */
	public function routeImageUrl($url)
	{
		$logger = AutotweetLogger::getInstance();
		$logger->log(JLog::INFO, 'routeImageUrl url = ' . $url);

		if (!empty($url))
		{
			$url = $this->createAbsoluteUrl($url);
		}

		$logger->log(JLog::INFO, 'routeImageUrl: final image url = ' . $url);

		return $url;
	}

	/**
	 * build
	 *
	 * Route/build the URL.
	 * This is a substitute for the original Joomla route function JRoute::_
	 * because JRoute::_ does work from frontend only for SEF urls.
	 * Works also for JoomSEF and sh404sef.
	 *
	 * @param   string  $url  Param
	 *
	 * @return	object
	 *
	 * @since	1.5
	 */
	public function build($url)
	{
		if ((JFactory::getApplication()->isAdmin()) || (defined('AUTOTWEET_CRONJOB_RUNNING')))
		{
			if ($this->routing_mode == self::ROUTING_MODE_COMPATIBILITY)
			{
				$base_url = $this->getRoot();
				$callsef = $base_url . '/index.php?option=com_autotweet&view=sef&task=route&url=' . urlencode($url);

				// Get the url
				$c = curl_init();
				curl_setopt($c, CURLOPT_URL, $callsef);
				curl_setopt($c, CURLOPT_HEADER, 0);
				curl_setopt($c, CURLOPT_NOBODY, 0);
				curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 20);
				curl_setopt($c, CURLOPT_TIMEOUT, 40);
				$sefurl = curl_exec($c);
				$result_code = curl_getinfo($c);

				$logger = AutotweetLogger::getInstance();
				$logger->log(JLog::INFO, 'Calling SEF Router: ' . $callsef . ' result: ' . $sefurl, $result_code);

				// Error handling
				if (curl_errno($c))
				{
					$sefurl = JRoute::_($url, false);

					$logger = AutotweetLogger::getInstance();
					$logger->log(JLog::WARNING, 'Error routing SEF URL via frontend request - curl_error: ' . curl_errno($c) . ' ' . curl_error($c));
				}

				// In some situations a different code than 200 is returned also when url is correct returned.
				// To avoid problems all codes 2xx and 3xx are catched here as OK.
				elseif (((int) $result_code['http_code'] < 200) || ((int) $result_code['http_code'] >= 400) || ((int) $result_code['http_code'] == 303))
				{
					$logger = AutotweetLogger::getInstance();
					$logger->log(JLog::WARNING, 'Error routing SEF URL via frontend request - http error: ' . $result_code['http_code'] . ' - callurl = ' . $url . ' - return url = ' . $sefurl);
					$sefurl = JRoute::_($url, false);
				}
				else
				{
					// In backend we need to remove some parts from the url
					// $sefurl = str_replace('/components/com_autotweet/', '', $sefurl);
					$sefurl = str_replace('/components/com_autotweet/', '/', $sefurl);
				}

				curl_close($c);
			}
			else
			{
				// In the back end we need to set the application to the site app instead
				JFactory::$application = JApplication::getInstance('site');
				JFactory::$application->initialise();

				$sefurl = JRoute::_($url, false);

				// Set the appilcation back to the administartor app
				JFactory::$application = JApplication::getInstance('administrator');
			}
		}
		else
		{
			// Route the URL when app is in site mode
			$sefurl = JRoute::_($url, false);
		}

		return $sefurl;
	}

	/**
	 * Helps with the Joomla url hell and creates corect url savely for frontend, backend and images.
	 *
	 * @param   string  $site_url  Param
	 *
	 * @return	string
	 *
	 * @since	1.5
	 */
	protected function createAbsoluteUrl($site_url)
	{
		// Just in case
		$site_url = str_replace('/administrator', '', $site_url);

		if ($this->hasPath($site_url))
		{
			$baseurl = $this->getHost();
			$url = $baseurl . $site_url;
		}
		else
		{
			// Sometimes different value for backend and frontend post (one with slash)
			if (JString::substr($site_url, 0, 1) != '/')
			{
				// Remove slash at the beginning
				$site_url = '/' . $site_url;
			}

			$baseurl = $this->getRoot();
			$url = $baseurl . $site_url;
		}

		return $url;
	}

	/**
	 * correctUrlLang.
	 *
	 * @param   string  $url  Param
	 *
	 * @return	string
	 *
	 * @since	1.5
	 */
	protected function correctUrlLang($url)
	{
		$logger = AutotweetLogger::getInstance();

		if (!empty($this->langmgmt_default_language))
		{
			// Analyze URL and find lang tag
			$lang_code = $this->getLanguageTag($this->langmgmt_default_language);
			$lang_codes = $this->getLanguageTags();
			$lang_tag = '&lang=';
			$pos = JString::strpos($url, $lang_tag);

			if (false !== $pos)
			{
				// Case 1: check for lang tag in non SEF url - http://blabla.com/index.php?option=com_content&view=article&id=999&Itemid=42&lang=en
				if (self::LANGMGMT_REPLACELANG == $this->langmgmt_enabled)
				{
					// Replace language tag with default language
					$replace = $lang_tag . $lang_code;
				}
				else
				{
					// Remove language from URL
					$replace = '';
				}

				foreach ($lang_codes as $code)
				{
					$search = $lang_tag . $code;
					$r_count = 0;
					$tmp_url = str_ireplace($search, $replace, $url, $r_count);

					if (0 < (int) $r_count)
					{
						$url = $tmp_url;
						break;
					}
				}

				if ((int) $r_count != 1)
				{
					$logger->log(JLog::WARNING, 'correctUrlLang: wrong language code found in URL.');
				}
			}
			else
			{
				// Case 2: check for lang tag in SEF url - http://blabla.com/en/extensions-for-joomla
				if (self::LANGMGMT_REPLACELANG == $this->langmgmt_enabled)
				{
					// Replace language tag with default language
					$replace = '/' . $lang_code . '/';
				}
				else
				{
					// Remove language from URL
					$replace = '/';
				}

				foreach ($lang_codes as $code)
				{
					$search = '/' . $code . '/';
					$r_count = 0;
					$tmp_url = str_ireplace($search, $replace, $url, $r_count);

					if (0 < (int) $r_count)
					{
						$url = $tmp_url;
						break;
					}
				}

				if ((int) $r_count > 1)
				{
					$logger->log(JLog::WARNING, 'correctUrlLang: multiple language codes found in URL.');
				}
			}
		}
		else
		{
			$logger->log(JLog::WARNING, 'correctUrlLang: default language not set in AutoTweet parameters.');
		}

		return $url;
	}

	/**
	 * getLanguageTag.
	 *
	 * @param   string  $lang_code  Param
	 *
	 * @return	string
	 *
	 * @since	1.5
	 */
	protected function getLanguageTag($lang_code)
	{
		$table = '#__languages';

		$db = JFactory::getDBO();
		$query = 'SELECT ' . $db->quoteName('sef') . ' FROM ' . $db->quoteName($table) . ' WHERE ' . $db->quoteName('lang_code') . ' = ' . $db->Quote($lang_code);

		$db->setQuery($query);

		return $db->loadResult();
	}

	/**
	 * getLanguageTags.
	 *
	 * @return	array
	 *
	 * @since	1.5
	 */
	protected function getLanguageTags()
	{
		$table = '#__languages';

		$db = JFactory::getDBO();
		$query = 'SELECT ' . $db->quoteName('sef') . ' FROM ' . $db->quoteName($table);

		$db->setQuery($query);

		return $db->loadColumn();
	}

	/**
	 * getRoot.
	 *
	 * @return	string
	 */
	public function getRoot()
	{
		if ($this->overwrite_baseurl)
		{
			$baseurl = $this->base_url;
		}
		else
		{
			try
			{
				$baseurl = JUri::root();
			}
			catch (Exception $e)
			{
				$baseurl = 'http://undefined-domain.com';
			}
		}

		// Correct base url (when installed in subfolder...)
		if (JString::substr($baseurl, -1) == '/')
		{
			$baseurl = JString::substr($baseurl, 0, JString::strlen($baseurl) - 1);
		}

		// In backend we need to remove administrator from URL as it is added even though we set the application to the site app
		// $baseurl = str_replace('/administrator', '', $baseurl);

		// Forced front-end SSL
		$jconfig = JFactory::getConfig();

		if (EXTLY_J3)
		{
			$force_ssl = $jconfig->get('force_ssl');
		}
		else
		{
			$force_ssl = $jconfig->getValue('config.force_ssl');
		}

		if (($force_ssl == 2) && (strpos($baseurl, 'http:') === 0))
		{
			$baseurl = str_replace('http:', 'https:', $baseurl);
		}

		return $baseurl;
	}

	/**
	 * getHost.
	 *
	 * @return	string
	 */
	protected function getHost()
	{
		$baseurl = $this->getRoot();
		$uri = JUri::getInstance();

		if ($uri->parse($baseurl))
		{
			$host = $uri->toString(
					array(
							'scheme',
							'host',
							'port'
				)
			);

			return $host;
		}

		return null;
	}

	/**
	 * getPath.
	 *
	 * @return	string
	 */
	protected function getPath()
	{
		$baseurl = $this->getRoot();
		$uri = JUri::getInstance();

		if ($uri->parse($baseurl))
		{
			$path = $uri->toString(
					array(
							'path'
				)
			);

			return $path;
		}

		return null;
	}

	/**
	 * hasPath.
	 *
	 * @param   string  $url  Param
	 *
	 * @return	string
	 */
	protected function hasPath($url)
	{
		$path = $this->getPath();
		$l = strlen($path);

		return (($l > 0) && (JString::substr($url, 0, $l) == $path));
	}
}
