<?php
/**
* Factory to create channel classes.
* This is the central point to get and handle channel classes. Also all needed files are included here.
*
* @version 1.6
* @author Ulli Storck
* @copyright (C) 2009-2011 Ulli Storck
* @license GNU/GPLv3 www.gnu.org/licenses/gpl-3.0.html
**/

// include new channels here only!!!
require_once (dirname(__FILE__) . '/autotweettwitter.php');
require_once (dirname(__FILE__) . '/autotweetfacebook.php'); 
//require_once (dirname(__FILE__) . '/autotweetfacebookevent.php'); 
//require_once (dirname(__FILE__) . '/autotweetmail.php'); 
 
require_once (dirname(__FILE__) . '/autotweetchannelfactoryhelper.php'); 

 
class AutotweetChannelFactory
{
	private $table_channel	= '#__autotweet_channel';
	private $table_type		= '#__autotweet_channeltype';
	
	protected $channels = array ();
	
	private static $instance = null;
	
	
	protected function AutotweetChannelFactory()
	{
		// singleton, no public access to constructor
	}

	public static function &getInstance()
	{
		if (!self::$instance) {
			self::$instance = new AutotweetChannelFactory();
		}
		
		return self::$instance;
	}
	
	protected function createChannel($auth_data)
	{
		$classname = 'AutoTweet' . $auth_data['type']. 'Channel';
		
		return new $classname($auth_data);
	}
	
	public function initChannels()
	{
		$db = &JFactory::getDBO();
		$query = AutotweetChannelFactoryHelper::getQueryAll($db, $this->table_channel, $this->table_type);
		$db->setQuery($query);
		$enabled_channels = $db->loadAssocList();

		foreach ($enabled_channels as $channel) {
			if (!array_key_exists($channel['id'], $this->channels)) {
				$this->channels[$channel['id']] =& $this->createChannel($channel);
			}
		}
	}

	public function &getChannels()
	{
		return $this->channels;
	}

	public function &getChannel($id)
	{
		if (!array_key_exists($id, $this->channels)) {
			$db = &JFactory::getDBO();
			$query = AutotweetChannelFactoryHelper::getQueryEntity($id, $db, $this->table_channel, $this->table_type);
			$db->setQuery($query);
			$channel = $db->loadAssoc();
			
			$this->channels[$id] =& $this->createChannel($channel);
		}
		
		return $this->channels[$id];
	}

}
	
?>