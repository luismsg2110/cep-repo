<?php

/**
 * @package     Extly.Components
 * @subpackage  com_autotweet - AutoTweetNG posts content to social channels (Twitter, Facebook, LinkedIn, etc).
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * TwAppHelper class.
 *
 * @package     Extly.Components
 * @subpackage  com_autotweet
 * @since       1.0
*/
class TwAppHelper
{
	private $_twitter = null;

	protected $consumer_key = null;

	protected $consumer_secret = null;

	protected $access_token = null;

	protected $access_token_secret = null;

	/**
	 * TwAppHelper.
	 *
	 * @param   string  $consumer_key         Params.
	 * @param   string  $consumer_secret      Params.
	 * @param   string  $access_token         Params.
	 * @param   string  $access_token_secret  Params.
	 *
	 * @since	1.5
	 */
	public function TwAppHelper($consumer_key, $consumer_secret, $access_token, $access_token_secret)
	{
		require_once dirname(__FILE__) . '/tmhOAuth/tmhOAuth.php';

		$this->consumer_key = $consumer_key;
		$this->consumer_secret = $consumer_secret;
		$this->access_token = $access_token;
		$this->access_token_secret = $access_token_secret;
	}

	/**
	 * login.
	 *
	 * @return	object
	 *
	 * @since	1.5
	 */
	public function login()
	{
		if (!$this->_twitter)
		{
			$this->_twitter = new tmhOAuth\tmhOAuth(
					array(
							'consumer_key' => $this->consumer_key,
							'consumer_secret' => $this->consumer_secret,
							'user_token' => $this->access_token,
							'user_secret' => $this->access_token_secret
				)
			);
		}

		return $this->_twitter;
	}

	/**
	 * getApi.
	 *
	 * @return	object
	 *
	 * @since	1.5
	 */
	public function getApi()
	{
		return $this->_twitter;
	}

	/**
	 * verify.
	 *
	 * @return	boolean
	 *
	 * @since	1.5
	 */
	public function verify()
	{
		JLoader::register('TwitterChannelHelper', JPATH_AUTOTWEET_HELPERS . '/channels/twitter.php');

		$api = $this->login();
		$code = $api->request('GET', $api->url('1.1/account/verify_credentials'));
		$response = TwitterChannelHelper::_processResponse($code, $api);

		if ($response[0])
		{
			// Process response-headers
			return TwitterChannelHelper::_processHeaders($code, $api);
		}

		return $response;
	}

	/**
	 * checkTimestamp.
	 *
	 * @return	boolean
	 *
	 * @since	1.5
	 */
	public static function checkTimestamp()
	{
		// Get component parameter - Offline mode
		$version_check = EParameter::getComponentParam(CAUTOTWEETNG, 'version_check', 1);

		if (!$version_check)
		{
			return 998;
		}

		$appHelper = new TwAppHelper('TOCHECK', 'TOCHECK', 'TOCHECK', 'TOCHECK');
		$result = $appHelper->verify();
		$api = $appHelper->getApi();

		$dateCompare = 999;

		if (array_key_exists('headers', $api->response))
		{
			$headers = $api->response['headers'];
			$twitterDate = $headers['date'];
			$twistamp = strtotime($twitterDate);
			$srvstamp = time();
			$dateCompare = abs($srvstamp - $twistamp);
		}

		return $dateCompare;
	}

	/**
	 * get_usertimeline.
	 *
	 * @param   string  $twUsername   Param
	 * @param   int     $twMaxTweets  Param
	 *
	 * @return	array
	 *
	 * @since	1.5
	 */
	public function get_usertimeline($twUsername, $twMaxTweets)
	{
		JLoader::register('TwitterChannelHelper', JPATH_AUTOTWEET_HELPERS . '/channels/twitter.php');

		$api = $this->login();
		$code = $api->request('GET', $api->url('1.1/statuses/user_timeline'),
				array(
						'screen_name' => $twUsername,
						'count' => $twMaxTweets
			)
		);
		$response = TwitterChannelHelper::processJsonResponse($code, $api);

		if ($response[0])
		{
			// Process response-headers
			return $response[1];
		}

		return null;
	}
}
