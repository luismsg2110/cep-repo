<?php

/**
 * @package     Extly.Components
 * @subpackage  com_autotweet - AutoTweetNG posts content to social channels (Twitter, Facebook, LinkedIn, etc).
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

JLoader::import('channel', dirname(__FILE__));

/**
 * FacebookBaseChannelHelper class.
 *
 * @package     Extly.Components
 * @subpackage  com_autotweet
 * @since       1.0
*/
abstract class FacebookBaseChannelHelper extends ChannelHelper
{
	protected $facebook = null;

	/**
	 * getApiInstance.
	 *
	 * @return	object
	 *
	 * @since	1.5
	 */
	protected function getApiInstance()
	{
		if (!$this->facebook)
		{
			$params = array(
							'appId' => $this->get('app_id', 'My-App-ID'),
							'secret' => $this->get('secret', 'My-App-Secret'),
							'cookie' => true
			);

			JLoader::import('facebook-php-sdk.facebook', dirname(__FILE__));
			$this->facebook = new facebookphpsdk\Facebook($params);
		}

		return $this->facebook;
	}

	/**
	 * addTargetArguments.
	 *
	 * @param   array  &$arguments  Params
	 * @param   int    $target_id   Params
	 *
	 * @return	void
	 */
	protected function addTargetArguments(&$arguments, $target_id)
	{
		$target = FOFModel::getTmpInstance('Targets', 'AutoTweetModel')->getItem($target_id);

		$criterias = $target->xtform->toArray();

		if (is_array($criterias))
		{
			$targeting = array();

			foreach ($criterias as $key => $criteria)
			{
				$fbkey = str_replace('fb', '', $key);
				$criteria = urldecode($criteria);
				$criteria = json_decode($criteria);

				if (!array_key_exists($fbkey, $targeting))
				{
					$targeting[$fbkey] = array();
				}

				$value = $criteria->criteriaValue;

				if (empty($value))
				{
					$value = $criteria->criteriaValueText;
				}

				$targeting[$fbkey][] = $value;
			}

			if ($targeting)
			{
				$arguments['targeting'] = json_encode($targeting);
			}
		}

		$logger = AutotweetLogger::getInstance();
		$logger->log(JLog::INFO, 'Targeting', $arguments);
	}
}
