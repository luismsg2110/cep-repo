<?php

/**
 * @package     Extly.Components
 * @subpackage  com_autotweet - AutoTweetNG posts content to social channels (Twitter, Facebook, LinkedIn, etc).
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

JLoader::import('facebookbase', dirname(__FILE__));

/**
 * FacebookEventChannelHelper class.
 * AutoTweet Facebook event channel.
 * Creates new events on profile, group, page
 *
 * @package     Extly.Components
 * @subpackage  com_autotweet
 * @since       1.0
 */
class FacebookEventChannelHelper extends FacebookBaseChannelHelper
{
	/**
	 * sendMessage.
	 *
	 * @param   string  $message  Params
	 * @param   object  $data     Params
	 *
	 * @return	booleans
	 *
	 * @since	1.5
	 */
	public function sendMessage($message, $data)
	{
		if (isset($data->event))
		{
			return $this->sendFacebookMessage($message, $data->title, $data->fulltext, $data->event, $data->image_url, $this->getMediaMode());
		}
		else
		{
			$result = array(
							false,
							'Message is not an event or event data is missing/incomplete (data->event).'
			);

			return $result;
		}
	}

	/**
	 * sendFacebookMessage.
	 *
	 * @param   string  $message     Params
	 * @param   string  $title       Params
	 * @param   string  $text        Params
	 * @param   string  $event_data  Params
	 * @param   string  $image_url   Params
	 * @param   string  $media_mode  Params
	 *
	 * @return	boolean
	 *
	 * @since	1.5
	 */
	protected function sendFacebookMessage($message, $title, $text, $event_data, $image_url, $media_mode)
	{
		$fb_id = $this->get('fbchannel_id');
		$fb_token = $this->get('fbchannel_access_token');

		$result = null;

		if (!empty($event_data))
		{
			// Allowed privacy types: OPEN, CLOSED, SECRET
			if (!isset($event_data->privacy) || (($event_data->privacy != 'CLOSED') && ($event_data->privacy != 'SECRET')))
			{
				$privacy = 'OPEN';
			}
			else
			{
				$privacy = $event_data->privacy;
			}

			$arguments = array(
							'access_token' => $fb_token,
							'name' => $title,
							'description' => $text,
							'location' => $event_data->location,
							'street' => $event_data->street,
							'city' => $event_data->city,
							'privacy_type' => $privacy,
							'start_time' => JFactory::getDate($event_data->start_time)->toISO8601(),
							'picture' => $image_url
			);

			if (!empty($event_data->end_time))
			{
				$arguments['end_time'] = JFactory::getDate($event_data->end_time)->toISO8601();
			}

			$arguments = array_filter($arguments);

			try
			{
				$result = $this->getApiInstance()->api("/{$fb_id}/events", 'post', $arguments);
				$msg = 'Facebook id: ' . $result['id'];
				$result = array(
								true,
								$msg
				);
			}
			catch (Exception $e)
			{
				$result = array(
								false,
								$e->getCode() . ' - ' . $e->getMessage()
				);
			}
		}
		else
		{
			$result = array(
							false,
							'Message is not an event or event data is missing/incomplete.'
			);
		}

		return $result;
	}
}
