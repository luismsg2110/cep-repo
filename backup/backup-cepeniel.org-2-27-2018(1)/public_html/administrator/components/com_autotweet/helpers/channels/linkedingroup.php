<?php

/**
 * @package     Extly.Components
 * @subpackage  com_autotweet - AutoTweetNG posts content to social channels (Twitter, Facebook, LinkedIn, etc).
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

JLoader::import('linkedinbase', dirname(__FILE__));

/**
 * LinkedinGroupChannelHelper - AutoTweet LinkedIn channel for posts to groups.
 *
 * @package     Extly.Components
 * @subpackage  com_autotweet
 * @since       1.0
*/
class LinkedinGroupChannelHelper extends LinkedinBaseChannelHelper
{
	/**
	 * sendMessage.
	 *
	 * @param   string  $message  Params
	 * @param   object  $data     Params
	 *
	 * @return  boolean
	 *
	 * @since   1.5
	 */
	public function sendMessage($message, $data)
	{
		return $this->sendLinkedinMessage($message, $data->title, $data->fulltext, $this->getMediaMode());
	}

	/**
	 * sendLinkedinMessage.
	 *
	 * @param   string  $message     Params
	 * @param   string  $title       Params
	 * @param   string  $text        Params
	 * @param   string  $media_mode  Params
	 *
	 * @return  boolean
	 *
	 * @since   1.5
	 */
	protected function sendLinkedinMessage($message, $title, $text, $media_mode)
	{
		$result = null;

		// Summary: message, text or both
		switch ($media_mode)
		{
			case 'attachment':
				$summary = $text;
				break;
			case 'both':
			case 'message':
			default:
				$summary = $message;
		}

		try
		{
			$api = $this->getApiInstance();

			$summary = TextUtil::truncString($summary, JString::strlen($summary), self::MAX_CHARS_TEXT, true);
			$response = $api->createPost($this->get('group_id'), $title, $summary);

			if ($response['success'] === true)
			{
				$result = array(
								true,
								'OK'
				);
			}
			else
			{
				$http_code = $response['info']['http_code'];

				if ($http_code == 202)
				{
					$result = array(
									true,
									'202 - Accepted / waiting for approval'
					);
				}
				else
				{
					$msg = $http_code . ' - ' . JText::_('COM_AUTOTWEET_HTTP_ERR_' . $response['info']['http_code']);
					$result = array(
									false,
									$msg
					);
				}
			}
		}
		catch (LinkedInException $e)
		{
			$result = array(
							false,
							$e->getMessage()
			);
		}

		return $result;
	}
}
