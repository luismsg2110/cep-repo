<?php

/**
 * @package     Extly.Components
 * @subpackage  com_autotweet - AutoTweetNG posts content to social channels (Twitter, Facebook, LinkedIn, etc).
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * LiAppHelper class.
 *
 * @package     Extly.Components
 * @subpackage  com_autotweet
 * @since       1.0
*/
class LiAppHelper
{
	private $_linkedin;

	protected $api_key = null;

	protected $secret_key = null;

	protected $oauth_user_token = null;

	protected $oauth_user_secret = null;

	/**
	 * LiAppHelper.
	 *
	 * @param   string  $api_key            Params.
	 * @param   string  $secret_key         Params.
	 * @param   string  $oauth_user_token   Params.
	 * @param   string  $oauth_user_secret  Params.
	 *
	 * @since	1.5
	 */
	public function LiAppHelper($api_key, $secret_key, $oauth_user_token, $oauth_user_secret)
	{
		require_once dirname(__FILE__) . '/Simple-LinkedIn/linkedin_3.2.0.class.php';

		$this->api_key = $api_key;
		$this->secret_key = $secret_key;
		$this->oauth_user_token = $oauth_user_token;
		$this->oauth_user_secret = $oauth_user_secret;
	}

	/**
	 * login.
	 *
	 * @return	object
	 *
	 * @since	1.5
	 */
	public function login()
	{
		if (!$this->_linkedin)
		{
			$API_CONFIG = array(
					'appKey' => $this->api_key,
					'appSecret' => $this->secret_key,
					'callbackUrl' => null
			);

			$this->_linkedin = new SimpleLinkedIn\LinkedIn($API_CONFIG);

			$ACCESS_TOKEN = array(
					'oauth_token' => $this->oauth_user_token,
					'oauth_token_secret' => $this->oauth_user_secret
			);
			$this->_linkedin->setTokenAccess($ACCESS_TOKEN);
		}

		return $this->_linkedin;
	}

	/**
	 * getUser.
	 *
	 * @return	object
	 *
	 * @since	1.5
	 */
	public function getUser()
	{
		if (empty($this->api_key)
			|| empty($this->secret_key)
			|| empty($this->oauth_user_token)
			|| empty($this->oauth_user_secret))
		{
			return array(false, 'Access Token and/or Token secret not entered (getUser).');
		}

		$result = null;

		try
		{
			$api = $this->login();
			$response = $api->profile();

			if ($response['success'] === true)
			{
				$xml = $response['linkedin'];
				$user = simplexml_load_string($xml);
				$user = json_decode(json_encode($user));

				if (preg_match('/id=([0-9]+)/', $xml, $matches))
				{
					$user->id = $matches[1];
				}

				$result = array(true, 'Ok!', $user);
			}
			else
			{
				$msg = $response['info']['http_code'] . ' ' . JText::_('COM_AUTOTWEET_HTTP_ERR_' . $response['info']['http_code']);
				$result = array(false, $msg);
			}
		}
		catch (LinkedInException $e)
		{
			$result = array(false, $e->getMessage());
		}

		return $result;
	}

	/**
	 * getMyGroup.
	 *
	 * @return	object
	 *
	 * @since	1.5
	 */
	public function getMyGroups()
	{
		if (empty($this->api_key)
			|| empty($this->secret_key)
			|| empty($this->oauth_user_token)
			|| empty($this->oauth_user_secret))
		{
			return array(false, 'Access Token and/or Token secret not entered (getMyGroup).');
		}

		$result = null;

		try
		{
			$api = $this->login();
			$response = $api->groupXTDOwnerships();

			if ($response['success'] === true)
			{
				$xml = $response['linkedin'];
				$groups = simplexml_load_string($xml);
				$groups = json_decode(json_encode($groups));

				$result = array();

				if (isset($groups->{'group-membership'}))
				{
					$results = $groups->{'group-membership'};

					if (is_array($results))
					{
						foreach ($results as $group)
						{
							$result[] = $group->group;
						}
					}
					elseif (is_object($results))
					{
						$result[] = $results->group;
					}
				}
			}
			else
			{
				$msg = $response['info']['http_code'] . ' ' . JText::_('COM_AUTOTWEET_HTTP_ERR_' . $response['info']['http_code']);
				$result = array(false, $msg);

				return $result;
			}

			$response = $api->groupXTDMemberships();

			if ($response['success'] === true)
			{
				$xml = $response['linkedin'];
				$groups = simplexml_load_string($xml);
				$groups = json_decode(json_encode($groups));

				if (isset($groups->{'group-membership'}))
				{
					$results = $groups->{'group-membership'};

					if (is_array($results))
					{
						foreach ($results as $group)
						{
							$result[] = $group->group;
						}
					}
					elseif (is_object($results))
					{
						$result[] = $results->group;
					}
				}
			}
		}
		catch (LinkedInException $e)
		{
			$result = array('id' => false, 'name' => $e->getMessage());
		}

		return $result;
	}

	/**
	 * getMyCompanies.
	 *
	 * @return	object
	 *
	 * @since	1.5
	 */
	public function getMyCompanies()
	{
		if (empty($this->api_key)
			|| empty($this->secret_key)
			|| empty($this->oauth_user_token)
			|| empty($this->oauth_user_secret))
		{
			return array(false, 'Access Token and/or Token secret not entered (getMyCompanies).');
		}

		$result = null;

		try
		{
			$api = $this->login();
			$response = $api->company('?is-company-admin=true');

			if ($response['success'] === true)
			{
				$xml = $response['linkedin'];
				$companies = simplexml_load_string($xml);
				$companies = json_decode(json_encode($companies));

				$result = array();

				if (isset($companies->company))
				{
					$result = $companies->company;

					if (is_array($result))
					{
						return $result;
					}
					else
					{
						// It's an object wrapped in an array
						return array($result);
					}
				}
			}
			else
			{
				$msg = $response['info']['http_code'] . ' ' . JText::_('COM_AUTOTWEET_HTTP_ERR_' . $response['info']['http_code']);
				$result = array(false, $msg);
			}
		}
		catch (LinkedInException $e)
		{
			$result = array('id' => false, 'name' => $e->getMessage());
		}

		return $result;
	}
}
