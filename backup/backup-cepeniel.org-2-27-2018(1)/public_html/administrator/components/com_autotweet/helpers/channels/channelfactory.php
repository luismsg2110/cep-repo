<?php

/**
 * @package     Extly.Components
 * @subpackage  com_autotweet - AutoTweetNG posts content to social channels (Twitter, Facebook, LinkedIn, etc).
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Factory to create channel classes.
 * This is the central point to get and handle channel classes. Also all needed files are included here.
*/

/**
 * ChannelFactory class.
 *
 * @package     Extly.Components
 * @subpackage  com_autotweet
 * @since       1.0
*/
class ChannelFactory
{
	protected $channels = array();

	private static $_instance = null;

	/**
	 * ChannelFactory
	 *
	 * @since	1.0
	 */
	protected function ChannelFactory()
	{
		// Singleton, no public access to constructor
	}

	/**
	 * getInstance
	 *
	 * @return	object.
	 *
	 * @since	1.5
	 */
	public static function getInstance()
	{
		if (!self::$_instance)
		{
			$channeltype = FOFModel::getTmpInstance('Channeltypes', 'AutoTweetModel');
			$channeltype->registerChannelTypes();

			self::$_instance = new ChannelFactory;
			self::$_instance->_initChannels();
		}

		return self::$_instance;
	}

	/**
	 * createChannel
	 *
	 * @param   string  &$channel  Param
	 *
	 * @return	object
	 *
	 * @since	1.5
	 */
	protected function createChannel(&$channel)
	{
		$channeltype = FOFModel::getTmpInstance('Channeltypes', 'AutoTweetModel');
		$classname = $channeltype->getChannelClass($channel->channeltype_id);
		JLoader::load($classname);

		return new $classname($channel);
	}

	/**
	 * initChannels
	 *
	 * @return	void
	 *
	 * @since	1.5
	 */
	private function _initChannels()
	{
		$db = JFactory::getDBO();

		// Load the model
		$channels = FOFModel::getTmpInstance('Channels', 'AutoTweetModel');

		$channels->set('published', true);
		$channels->set('filter_order', 'ordering');
		$channels->set('filter_order_Dir', 'ASC');

		$list = $channels->getItemList(true);

		foreach ($list as $channel)
		{
			$this->channels[$channel->id] = self::createChannel($channel);
		}
	}

	/**
	 * getChannels
	 *
	 * @return	array
	 *
	 * @since	1.5
	 */
	public function getChannels()
	{
		return $this->channels;
	}

	/**
	 * getChannel
	 *
	 * @param   int  $id  Param
	 *
	 * @return	object.
	 *
	 * @since	1.5
	 */
	public function getChannel($id)
	{
		return $this->channels[$id];
	}

	/**
	 * hasChannel
	 *
	 * @param   int  $id  Param
	 *
	 * @return	bool.
	 */
	public function hasChannel($id)
	{
		return array_key_exists($id, $this->channels);
	}
}
