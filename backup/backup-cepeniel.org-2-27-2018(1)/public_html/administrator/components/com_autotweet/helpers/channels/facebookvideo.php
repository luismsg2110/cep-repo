<?php

/**
 * @package     Extly.Components
 * @subpackage  com_autotweet - AutoTweetNG posts content to social channels (Twitter, Facebook, LinkedIn, etc).
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

JLoader::import('facebookbase', dirname(__FILE__));

/**
 * FacebookVideoChannelHelper class.
 *
 * @package     Extly.Components
 * @subpackage  com_autotweet
 * @since       1.0
 */
class FacebookVideoChannelHelper extends FacebookBaseChannelHelper
{
	/**
	 * sendMessage.
	 *
	 * @param   string  $message  Params
	 * @param   object  $data     Params
	 *
	 * @return	boolean
	 *
	 * @since	1.5
	 */
	public function sendMessage($message, $data)
	{
		return $this->sendFacebookMessage($message, $data->title, $data->fulltext, $data->url, $data->org_url, $data->media_url, $this->getMediaMode(), $data);
	}

	// Internal service functions

	/**
	 * sendFacebookMessage.
	 *
	 * @param   string  $message     Params
	 * @param   string  $title       Params
	 * @param   string  $text        Params
	 * @param   string  $url         Params
	 * @param   string  $org_url     Params
	 * @param   string  $media_url   Params
	 * @param   string  $media_mode  Params
	 * @param   object  &$post       Params
	 *
	 * @return	boolean
	 *
	 * @since	1.5
	 */
	protected function sendFacebookMessage($message, $title, $text, $url, $org_url, $media_url, $media_mode, &$post)
	{
		if (empty($media_url))
		{
			return array(
							true,
							'No video.'
			);
		}

		$fb_id = $this->get('fbchannel_id');
		$fb_token = $this->get('fbchannel_access_token');

		$result = null;

		$media = JFile::read($media_url);

		// Video object: /user/videos
		$arguments = array(
						'message' => $message,
						'file' => $image,
						'access_token' => $fb_token,
						'type' => 'video',
						'created_time' => JFactory::getDate()->toISO8601()
		);

		$target_id = $post->xtform->get('target_id');

		if ($target_id)
		{
			$this->addTargetArguments($arguments, $target_id);
		}

		try
		{
			$api = $this->getApiInstance();
			$result = $api->api("/{$fb_id}/videos", 'post', $arguments);

			$msg = 'Facebook id: ' . $result['id'];
			$result = array(
							true,
							$msg
			);
		}
		catch (Exception $e)
		{
			$result = array(
							false,
							$e->getCode() . ' - ' . $e->getMessage()
			);
		}

		return $result;
	}
}
