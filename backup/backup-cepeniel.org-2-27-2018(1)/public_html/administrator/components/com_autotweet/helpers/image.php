<?php

/**
 * @package     Extly.Components
 * @subpackage  com_autotweet - AutoTweetNG posts content to social channels (Twitter, Facebook, LinkedIn, etc).
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * ImageHelper
 *
 * @package     Extly.Components
 * @subpackage  com_autotweet
 * @since       1.0
 */
class ImageHelper
{
	/**
	 * getURL.
	 *
	 * @param   string  $filename  Params
	 *
	 * @return	string.
	 *
	 * @since	1.5
	 */
	public static function getUrl($filename)
	{
		// Get the base site URL
		$url = JURI::base();
		$url = rtrim($url, '/');

		// Take into account relative URL for administrator
		list($isCLI, $isAdmin) = FOFDispatcher::isCliAdmin();

		if ($isAdmin)
		{
			$url .= '/..';
		}

		if (!class_exists('AkeebasubsHelperCparams'))
		{
			require_once JPATH_ADMINISTRATOR . '/components/com_akeebasubs/helpers/cparams.php';
		}

		$imagePath = trim(AkeebasubsHelperCparams::getParam('imagedir', 'images/'), '/');

		if (EXTLY_J25)
		{
			// Joomla! 2.5 : Pretty much straightforward
			return $url . '/' . $imagePath . '/' . $filename;
		}
		else
		{
			// Joomla! 3.0+ : Where the heck is the image?
			$testJ25 = JPATH_SITE . '/' . $imagePath . '/' . $filename;
			$testJ30 = JPATH_SITE . '/' . $filename;

			if (file_exists($testJ30))
			{
				return $url . '/' . $filename;
			}
			else
			{
				return $url . '/' . $imagePath . '/' . $filename;
			}
		}
	}
}
