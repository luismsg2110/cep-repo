<?php

/**
 * @package     Extly.Components
 * @subpackage  com_autotweet - AutoTweetNG posts content to social channels (Twitter, Facebook, LinkedIn, etc).
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Helper for posts form AutoTweet to channels (twitter, Facebook, ...)
 *
 * @package     Extly.Components
 * @subpackage  com_autotweet
 * @since       1.0
*/
abstract class RequestHelp
{
	/**
	 * queueMessage
	 *
	 * @param   string  $articleid        Param
	 * @param   string  $source_plugin    Param
	 * @param   string  $publish_up       Param
	 * @param   string  $description      Param
	 * @param   string  $typeinfo         Param
	 * @param   string  $url              Param
	 * @param   string  $image_url        Param
	 * @param   object  &$native_object   Param
	 * @param   string  &$advanced_attrs  Param
	 *
	 * @return	boolean
	 */
	public static function insertRequest($articleid, $source_plugin, $publish_up, $description, $typeinfo = 0, $url = '', $image_url = '', &$native_object = null, &$advanced_attrs = null)
	{
		$logger = AutotweetLogger::getInstance();

		// Check if message is already queued (it makes no sense to queue message more than once when modfied)
		// if message is already queued, correct the publish date

		$requestsModel = FOFModel::getTmpInstance('Requests', 'AutoTweetModel');
		$requestsModel->set('ref_id', $articleid);
		$requestsModel->set('plugin', $source_plugin);
		$requestsModel->set('typeinfo', $typeinfo);
		$list = $requestsModel->getItemList();

		$id = 0;

		if (count($list) > 0)
		{
			$id = $list[0]->id;
		}

		// Avoid databse warnings when desc is longer then expected
		if (!empty($description))
		{
			$description = JString::substr($description, 0, SharingHelper::MAX_CHARS_TITLE * 2);
		}

		$routeHelp = RouteHelp::getInstance();
		$url = $routeHelp->getAbsoluteUrl($url);

		if (empty($image_url))
		{
			// Default image: used in media mode when no image is available
			$image_url = EParameter::getComponentParam(CAUTOTWEETNG, 'default_image', '');
		}

		if (!empty($image_url))
		{
			$image_url = $routeHelp->getAbsoluteUrl($image_url, true);
		}

		$row = FOFModel::getTmpInstance('Requests', 'AutoTweetModel')->getTable();
		$row->reset();

		if ($id)
		{
			$row->load($id);
		}

		$request = array(
						'id' => $id,
						'ref_id' => $articleid,
						'plugin' => $source_plugin,
						'publish_up' => $publish_up,
						'description' => $description,
						'typeinfo' => $typeinfo,
						'url' => $url,
						'image_url' => $image_url,
						'native_object' => $native_object,
						'published' => 0
		);

		$logger->log(JLog::INFO, 'Enqueued request', $request);

		// Saving the request
		$queued = $row->save($request);

		if (!$queued)
		{
			$logger->log(JLog::ERROR, 'queueMessage: error storing message to database message queue, article id = ' . $articleid . ', error message = ' . $row->getError());
		}
		else
		{
			$logger->log(JLog::INFO, 'queueMessage: message stored/updated to database message queue, article id = ' . $articleid);
		}

		if (!$id)
		{
			$id = $row->id;
		}

		if (($advanced_attrs) && isset($advanced_attrs->attr_id))
		{
			$row = FOFModel::getTmpInstance('Advancedattrs', 'AutoTweetModel')->getTable();
			$row->reset();
			$row->load($advanced_attrs->attr_id);

			$attr = array(
				'id' => $advanced_attrs->attr_id,
				'request_id' => $id,
			);

			// Updating attr
			$result = $row->save($attr);

			if (!$result)
			{
				$logger->log(JLog::ERROR, 'Updating attr, attr_id = ' . $advanced_attrs->attr_id . ', error message = ' . $row->getError());
			}
			else
			{
				$logger->log(JLog::INFO, 'Updating attr, attr_id = ' . $advanced_attrs->attr_id);
			}
		}

		return $queued;
	}

	/**
	 * processRequests
	 *
	 * @param   array  $rids  Param
	 *
	 * @return	boolean
	 */
	public static function processRequests($rids)
	{
		$requestsModel = FOFModel::getTmpInstance('Requests', 'AutoTweetModel');
		$requestsModel->set('rids', $rids);
		$requests = $requestsModel->getItemList(true);

		return self::publishRequests($requests);
	}

	/**
	 * publishRequests
	 *
	 * @param   array  &$requests  Param
	 *
	 * @return	boolean
	 */
	public static function publishRequests(&$requests)
	{
		JLoader::register('SharingHelper', JPATH_AUTOTWEET_HELPERS . '/sharing.php');

		$sharinghelper = SharingHelper::getInstance();

		foreach ($requests as $request)
		{
			try
			{
				if ($sharinghelper->publishRequest($request))
				{
					// Remove only, when post is logged successfully
					self::processed($request->id);
				}
				else
				{
					self::saveError($request->id);
				}
			}
			catch (Exception $e)
			{
				self::saveError($request->id, $e->getMessage());
			}
		}

		return true;
	}

	/**
	 * processed
	 *
	 * @param   int  $id  Param
	 *
	 * @return	boolean
	 */
	public static function processed($id)
	{
		$request = FOFModel::getTmpInstance('Requests', 'AutotweetModel')->getTable();
		$request->reset();

		if ($request->load($id))
		{
			// Native Object
			if (isset($request->native_object))
			{
				$nativeObject = json_decode($request->native_object);
			}
			else
			{
				$nativeObject = new StdClass;
			}

			$nativeObject->error = false;
			$nativeObject->error_message = 'Ok!';

			// It's processed
			$data = array();
			$data['published'] = true;
			$data['native_object'] = json_encode($nativeObject);

			// Advanced Attrs
			$advancedattrs = FOFModel::getTmpInstance('Advancedattrs', 'AutoTweetModel');
			$advancedattrs->setState('request_id', $id);
			$advancedattr = $advancedattrs->getFirstItem();

			$params = null;

			if (isset($advancedattr->params))
			{
				$params = json_decode($advancedattr->params);
			}

			// Checking if there are future tasks
			if ((AUTOTWEETNG_JOOCIAL) && (isset($params->agenda)) && (count($params->agenda) > 0))
			{
				$publish_up = self::_getNextAgendaPublishUp($params->agenda);

				if ($publish_up)
				{
					$data['publish_up'] = $publish_up;

					// Not finished yet, we have at least a date to schedule
					$data['published'] = false;
				}
				else
				{
					$data['published'] = true;
				}
			}

			// Repeat
			if ((AUTOTWEETNG_JOOCIAL) && (isset($params->unix_mhdmd)) && (!empty($params->unix_mhdmd)))
			{
				// Not finished yet, we have at least a date to schedule
				$nextRepeatDate = self::_getNextRepeatPublishUp($params->unix_mhdmd);

				if ($nextRepeatDate)
				{
					$data['publish_up'] = $nextRepeatDate->toSql();
					$data['published'] = false;
				}
				else
				{
					$data['published'] = true;
				}
			}

			// Saving
			$request->save($data);

			if ($params)
			{
				$params = json_encode($params);
				$data = array('params' => $params);
				$advancedattr->save($data);
			}
		}
	}

	/**
	 * _getNextAgendaPublishUp
	 *
	 * @param   array  &$agenda  Param
	 *
	 * @return	string
	 */
	private static function _getNextAgendaPublishUp(&$agenda)
	{
		$now = JFactory::getDate();
		$dpcheck = EParameter::getComponentParam(CAUTOTWEETNG, 'dpcheck_time_intval', 12) * 3600;
		$now_unix = $now->toUnix() + $dpcheck;

		// The first date
		do
		{
			$publish_up = array_shift($agenda);
			$publish_up_unix = JFactory::getDate($publish_up)->toUnix();
		}

		while ((count($agenda) > 0) && ($publish_up_unix <= $now_unix));

		if ($publish_up_unix > $now_unix)
		{
			return $publish_up;
		}

		return null;
	}

	/**
	 * _getNextRepeatPublishUp
	 *
	 * @param   string  $repeat  Param
	 *
	 * @return	string
	 */
	private static function _getNextRepeatPublishUp($repeat)
	{
		$publish_up = null;

		try
		{
			$now = JFactory::getDate();
			$dpcheck = EParameter::getComponentParam(CAUTOTWEETNG, 'dpcheck_time_intval', 12) * 3600;
			$now_unix = $now->toUnix() + $dpcheck;

			$datePublishUp = new DateTime;
			$datePublishUp->setTimestamp($now_unix);

			$publish_up = VirtualManager::getInstance()->nextScheduledDate($repeat, $datePublishUp);
		}
		catch (Exception $e)
		{
			$logger = AutotweetLogger::getInstance();
			$logger->log(JLog::ERROR, '_getNextRepeatPublishUp' . $e->getMessage());
		}

		return $publish_up;
	}

	/**
	 * saveError
	 *
	 * @param   int     $id       Param
	 * @param   string  $message  Param
	 *
	 * @return	boolean
	 */
	public static function saveError($id, $message = null)
	{
		$request = FOFModel::getTmpInstance('Requests', 'AutotweetModel')->getTable();
		$request->reset();

		if ($request->load($id))
		{
			$nativeObject = json_decode($request->native_object);

			$nativeObject->error = true;

			if ($message)
			{
				$nativeObject->error_message = $message;
			}
			else
			{
				$nativeObject->error_message = 'COM_AUTOTWEET_ERROR_PROCESSING';
			}

			$data = array();
			$data['native_object'] = json_encode($nativeObject);
			$data['published'] = true;

			$request->save($data);
		}
	}

	/**
	 * getRequestList
	 *
	 * @param   JDate  $check_date  Param
	 * @param   int    $limit       Param
	 *
	 * @return	array
	 */
	public static function getRequestList($check_date, $limit)
	{
		$requestsModel = FOFModel::getTmpInstance('Requests', 'AutoTweetModel');
		$requestsModel->set('until_date', $check_date->toSql());
		$requestsModel->set('filter_order', 'publish_up');
		$requestsModel->set('filter_order_Dir', 'ASC');
		$requestsModel->set('limit', $limit);

		return $requestsModel->getItemList();
	}

	/**
	 * moveToState - This static function should used from backend only for manual (re)posting attempts
	 *
	 * @param   array   $ids        Param
	 * @param   int     $userid     Param
	 * @param   string  $published  Param
	 *
	 * @return	boolean
	 */
	public static function moveToState($ids, $userid = null, $published = null)
	{
		$success = true;

		if (!$pubstate)
		{
			$pubstate = 0;
		}

		if (count($ids) > 0)
		{
			$idslist = implode(',', $ids);
			$idslist = '(' . $idslist . ')';

			$now = JFactory::getDate();

			$db = JFactory::getDBO();
			$query = $db->getQuery(true);
			$query->update('#__autotweet_requests')
				->set($db->qn('published') . ' = ' . $db->q($published))
				->set($db->qn('modified') . ' = ' . $db->q($now->toSql()))
				->set($db->qn('modified_by') . ' = ' . $db->q($userid))
				->where($db->qn('id') . ' IN ' . $idslist);

			$db->setQuery($query);
			$db->execute();
		}

		return $success;
	}
}
