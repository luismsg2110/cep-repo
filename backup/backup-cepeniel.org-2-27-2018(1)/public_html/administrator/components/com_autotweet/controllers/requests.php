<?php

/**
 * @package     Extly.Components
 * @subpackage  com_autotweet - AutoTweetNG posts content to social channels (Twitter, Facebook, LinkedIn, etc).
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

include_once 'default.php';

/**
 * AutotweetControllerRequests
 *
 * @package     Extly.Components
 * @subpackage  com_autotweet
 * @since       1.0
*/
class AutotweetControllerRequests extends AutotweetControllerDefault
{
	/**
	 * publish.
	 *
	 * @return	void
	 */
	public function publish()
	{
		$this->process();
	}

	/**
	 * process.
	 *
	 * @return	void
	 */
	public function process()
	{
		// CSRF prevention
		if ($this->csrfProtection)
		{
			$this->_csrfProtection();
		}

		$model = $this->getThisModel();

		if (!$model->getId())
		{
			$model->setIDsFromRequest();
		}

		$status = $model->process();

		// Check if i'm using an AJAX call, in this case there is no need to redirect
		$format = $this->input->get('format', null, 'string');

		if ($format == 'json')
		{
			echo json_encode($status);

			return;
		}

		// Redirect
		$customUrl = $this->input->get('returnurl', null, 'string');

		if ($customUrl)
		{
			$customURL = base64_decode($customURL);
		}

		$url = !empty($customURL) ? $customURL : 'index.php?option=' . $this->component . '&view=' . FOFInflector::pluralize($this->view);

		if (!$status)
		{
			$this->setRedirect($url, $model->getError(), 'error');
		}
		else
		{
			$this->setRedirect($url);
		}

		return;
	}

	/**
	 * purge.
	 *
	 * @return	void
	 */
	public function purge()
	{
		// CSRF prevention
		if ($this->csrfProtection)
		{
			$this->_csrfProtection();
		}

		$model = $this->getThisModel();
		$status = $model->purge();

		// Check if i'm using an AJAX call, in this case there is no need to redirect
		$format = $this->input->get('format', null, 'string');

		if ($format == 'json')
		{
			echo json_encode($status);

			return;
		}

		// Redirect
		$customUrl = $this->input->get('returnurl', null, 'string');

		if ($customUrl)
		{
			$customURL = base64_decode($customURL);
		}

		$url = !empty($customURL) ? $customURL : 'index.php?option=' . $this->component . '&view=' . FOFInflector::pluralize($this->view);

		if (!$status)
		{
			$this->setRedirect($url, $model->getError(), 'error');
		}
		else
		{
			$this->setRedirect($url);
		}

		return;
	}

	/**
	 * batch.
	 *
	 * @return	void
	 */
	public function batch()
	{
		// CSRF prevention
		if ($this->csrfProtection)
		{
			$this->_csrfProtection();
		}

		$model = $this->getThisModel();

		if (!$model->getId())
		{
			$model->setIDsFromRequest();
		}

		$batch_published = $this->input->get('batch_published', null, 'int');
		$status = true;
		$status = $model->moveToState($batch_published);

		// Check if i'm using an AJAX call, in this case there is no need to redirect
		$format = $this->input->get('format', '', 'string');

		if ($format == 'json')
		{
			echo json_encode($status);

			return;
		}

		// Redirect
		if ($customURL = $this->input->get('returnurl', '', 'string'))
		{
			$customURL = base64_decode($customURL);
		}

		$url = !empty($customURL) ? $customURL : 'index.php?option=' . $this->component . '&view=' . FOFInflector::pluralize($this->view);

		if (!$status)
		{
			$this->setRedirect($url, $model->getError(), 'error');
		}
		else
		{
			$this->setRedirect($url);
		}
	}
}
