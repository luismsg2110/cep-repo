<?php
/**
 * @package     Extly.Components
 * @subpackage  com_autotweet - AutoTweetNG posts content to social channels (Twitter, Facebook, LinkedIn, etc).
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

?>
	<div id="fbchannel" class="tab-pane fade">

		<div class="control-group">
			<label class="required control-label" for="xtformfbchannel_id"
				id="fbchannel_id-lbl"><?php echo JText::_('COM_AUTOTWEET_VIEW_CHANNEL_FIELD_FBCHANNEL_TITLE'); ?> <span
				class="star">&nbsp;*</span></label>
			<div class="controls">
				<?php echo SelectControlHelper::fbchannels($this->item->xtform->get('fbchannel_id'), 'xtform[fbchannel_id]', array(), $this->item->xtform->get('app_id'), $this->item->xtform->get('secret'), $this->item->xtform->get('access_token')); ?>
			</div>
		</div>

		<div class="control-group">
			<label class="required control-label" for="fbchannel_access_token"
				id="fbchannel_access_token-lbl"><?php echo JText::_('COM_AUTOTWEET_VIEW_FBWACCOUNT_USERTOKEN_TITLE'); ?> <span
				class="star">&nbsp;*</span></label>
			<div class="controls">
				<input type="text" maxlength="255" size="50"
					value="<?php echo $this->item->xtform->get('fbchannel_access_token'); ?>"
					id="fbchannel_access_token" name="xtform[fbchannel_access_token]"
					class="required validate-token" required="required"
					readonly="readonly">
			</div>
		</div>

		<?php
		if ($requireAlbum)
		{
			?>
		<div class="control-group">
			<label class="required control-label" for="xtformfbalbum_id"
				id="fbalbum_id-lbl"><?php
			echo JText::_('COM_AUTOTWEET_VIEW_CHANNEL_FIELD_FBALBUM_TITLE');
			?> <span class="star">&nbsp;*</span></label>
			<div class="controls">
				<a class="btn btn-info" id="fbalbumloadbutton"><?php echo JText::_('COM_AUTOTWEET_VIEW_CHANNEL_LOADBUTTON_TITLE'); ?></a>
				<?php

echo SelectControlHelper::fbalbums($this->item->xtform->get('fbalbum_id'), 'xtform[fbalbum_id]', array(), $this->item->xtform->get('app_id'), $this->item->xtform->get('secret'), $this->item->xtform->get('access_token'), $this->item->xtform->get('fbchannel_id'));
			?>
			</div>
		</div>
     	<?php
		}
		?>

		<div class="control-group">
			<label class="control-label"> <a class="btn btn-info"
				id="fbchvalidationbutton"><?php echo JText::_('COM_AUTOTWEET_VIEW_CHANNEL_VALIDATEBUTTONCHANNEL_TITLE'); ?></a>
			</label>
		</div>

		<div class="control-group" style="display:none">
			<label class=" required control-label" for="channel_issued_at" id="channel_issued_at-lbl"><?php echo JText::_('COM_AUTOTWEET_VIEW_FBWACCOUNT_ISSUEDAT_TITLE'); ?><span
				class="star">&nbsp;*</span> </label>
			<div class="controls">
				<input type="text" maxlength="255" size="64"
					value="<?php echo $this->item->xtform->get('channel_issued_at'); ?>"
					id="channel_issued_at" name="xtform[channel_issued_at]"
					class="required" required="required"
					readonly="readonly">
			</div>
		</div>

		<div class="control-group">
			<label class=" required control-label" for="channel_expires_at" id="channel_expires_at-lbl"><?php echo JText::_('COM_AUTOTWEET_VIEW_FBWACCOUNT_EXPIRESAT_TITLE'); ?><span
				class="star">&nbsp;*</span> </label>
			<div class="controls">
				<input type="text" maxlength="255" size="64"
					value="<?php echo $this->item->xtform->get('channel_expires_at'); ?>"
					id="channel_expires_at" name="xtform[channel_expires_at]"
					class="required" required="required"
					readonly="readonly">
			</div>
		</div>

		<div class="group-warn alert" style="display: none;">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<p>
				<i class="xticon xticon-thumbs-up"></i> <b><?php
				echo JText::_('COM_AUTOTWEET_VIEW_FACEBOOK_GROUP_LABEL');
				?></b>
			</p>
			<p>
			<?php
			echo JText::_('COM_AUTOTWEET_VIEW_FACEBOOK_GROUP_DESC');
			?>
			</p>
		</div>

	</div>
