<?php
/**
 * @package     Extly.Components
 * @subpackage  com_autotweet - AutoTweetNG posts content to social channels (Twitter, Facebook, LinkedIn, etc).
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

$this->loadHelper('select');

$urlBase = JUri::root();
$isBackend = FOFPlatform::getInstance()->isBackend();

?>
<div class="extly dashboard">
	<div class="extly-body">

			<div class="row-fluid">
				<div class="span8">
					<div class="well">

						<h2>
							<i class="<?php echo JText::_('COM_AUTOTWEET_ICON_CPANELS')?>"></i>
							<?php echo JText::_('COM_AUTOTWEET_JOOCIAL_METER')?>
						</h2>

						<h3>
							<i class="<?php echo JText::_('COM_AUTOTWEET_ICON_REQUESTS')?>"></i>
							<?php echo JText::_('COM_AUTOTWEET_TITLE_REQUESTS')?>
						</h3>

						<div class="progress">
							<?php
							if ($this->posts)
							{
								?>
							<div class="bar bar-success" style="width: <?php echo $this->p_posts; ?>%;">
								<a href="index.php?option=com_autotweet&view=posts">
								<i class="<?php echo JText::_('COM_AUTOTWEET_ICON_POSTS')?>"></i>
								<?php echo JText::_('COM_AUTOTWEET_TITLE_POSTS'); ?>
								(<?php

									echo number_format($this->posts);

								?>)
								</a>
							</div>
							<?php
							}

							if ($this->requests)
							{
								?>
							<div class="bar bar-info" style="width: <?php echo $this->p_requests; ?>%;">
								<a href="index.php?option=com_autotweet&view=requests">
								<i class="<?php echo JText::_('COM_AUTOTWEET_ICON_REQUESTS')?>"></i>
								<?php echo JText::_('COM_AUTOTWEET_TITLE_REQUESTS'); ?>
								(<?php

									echo number_format($this->requests);

								?>)
								</a>
							</div>
							<?php
							}
							?>
						</div>

						<h3>
							<i class="<?php echo JText::_('COM_AUTOTWEET_ICON_POSTS')?>"></i>
							<?php echo JText::_('COM_AUTOTWEET_TITLE_POSTS')?>
						</h3>

						<div class="progress">
							<?php
							if ($this->error)
							{
								?>
							<div class="bar bar-danger" style="width: <?php echo $this->p_error; ?>%;">
								<?php echo SelectControlHelper::getTextForEnum('error', true); ?>
								<a href="index.php?option=com_autotweet&view=posts">
								(<?php

								echo number_format($this->error);

								?>)
								</a>
							</div>
							<?php
							}

							if ($this->approve)
							{
								?>
							<div class="bar bar-warning" style="width: <?php echo $this->p_approve; ?>%;">
								<?php echo SelectControlHelper::getTextForEnum('approve', true); ?>
								<a href="index.php?option=com_autotweet&view=posts">
								(<?php

								echo number_format($this->approve);

								?>)
								</a>
							</div>
							<?php
							}

							if ($this->cronjob)
							{
								?>
							<div class="bar bar-info" style="width: <?php echo $this->p_cronjob; ?>%;">
								<?php echo SelectControlHelper::getTextForEnum('cronjob', true); ?>
								<a href="index.php?option=com_autotweet&view=posts">
								(<?php

								echo number_format($this->cronjob);

								?>)
								</a>
							</div>
							<?php
							}

							if ($this->success)
							{
								?>
							<div class="bar bar-success" style="width: <?php echo $this->p_success; ?>%;">
								<?php echo SelectControlHelper::getTextForEnum('success', true); ?>
								<a href="index.php?option=com_autotweet&view=posts">
								(<?php

								echo number_format($this->success);

								?>)
								</a>
							</div>
							<?php
							}
							?>
						</div>

					</div>

					<?php

					if ($isBackend)
					{
					?>

					<h2>
						<?php echo JText::_('COM_AUTOTWEET_VIEW_CHANNELS_TITLE')?>
					</h2>
					<p>
						<a href="index.php?option=com_autotweet&view=channels" class="btn btn-large">
							<i class="xticon xticon-facebook"></i>
						</a>
						<a href="index.php?option=com_autotweet&view=channels" class="btn btn-large">
							<i class="xticon xticon-google-plus"></i>
						</a>
						<a href="index.php?option=com_autotweet&view=channels" class="btn btn-large">
							<i class="xticon xticon-linkedin"></i>
						</a>
						<a href="index.php?option=com_autotweet&view=channels" class="btn btn-large">
							<i class="xticon xticon-envelope-o"></i>
						</a>
						<a href="index.php?option=com_autotweet&view=channels" class="btn btn-large">
							<i class="xticon xticon-twitter"></i>
						</a>
						<a href="index.php?option=com_autotweet&view=channels" class="btn btn-large">
							<i class="xticon xticon-vk"></i>
						</a>
					</p>

					<h2>
						<?php echo JText::_('COM_AUTOTWEET_SHORTCUTS')?>
					</h2>
					<p>
						<a href="index.php?option=com_autotweet&view=posts" class="btn btn-large">
							<i class="<?php echo JText::_('COM_AUTOTWEET_ICON_POSTS')?>"></i>
							<?php echo JText::_('COM_AUTOTWEET_TITLE_POSTS')?>
						</a>
						<a href="index.php?option=com_autotweet&view=requests" class="btn btn-large">
							<i class="<?php echo JText::_('COM_AUTOTWEET_ICON_REQUESTS')?>"></i>
							<?php echo JText::_('COM_AUTOTWEET_TITLE_REQUESTS')?>
						</a>
						<a href="index.php?option=com_autotweet&view=channels" class="btn btn-large">
							<i class="<?php echo JText::_('COM_AUTOTWEET_ICON_CHANNELS')?>"></i>
							<?php echo JText::_('COM_AUTOTWEET_TITLE_CHANNELS')?>
						</a>
						<a href="index.php?option=com_autotweet&view=rules" class="btn btn-large">
							<i class="<?php echo JText::_('COM_AUTOTWEET_ICON_RULES')?>"></i>
							<?php echo JText::_('COM_AUTOTWEET_TITLE_RULES')?>
						</a>
						<a href="index.php?option=com_autotweet&view=feeds" class="btn btn-large">
							<i class="<?php echo JText::_('COM_AUTOTWEET_ICON_FEEDS')?>"></i>
							<?php echo JText::_('COM_AUTOTWEET_TITLE_FEEDS')?>
						</a>
					</p>

					<?php
					}

					?>


					<hr />

					<p>
						For more information: <a
							href="http://www.extly.com/autotweetng-documentation-faq.html"
							target="_blank"><?php echo VersionHelper::getFlavourName(); ?> Documentation</a>
					</p>
					<p>
						Support: <a href="http://support.extly.com" target="_blank">http://support.extly.com</a>
					</p>
					<ul class="footer-links">
						<li><a href="http://www.extly.com/blog.html" target="_blank">Read
								the Extly.com blog</a></li>
						<li><a href="http://support.extly.com" target="_blank">Submit
								issues</a></li>
						<li><a
							href="http://www.extly.com/autotweet-ng-pro.html#changelog"
							target="_blank">Roadmap and changelog</a></li>
					</ul>
				</div>
				<div class="span4">

					<?php

					if ($isBackend)
					{
						if (AUTOTWEETNG_JOOCIAL)
						{
							$manager = EExtensionHelper::getExtensionId('system', 'autotweetautomator');

							$url = 'index.php?option=com_autotweet&view=managers&task=edit&id=' . $manager;

							echo '<p class="text-right lead"><i class="xticon xticon-user"></i> <a class="btn btn-primary span10" href="' . $url . '">' .
								JText::_('COM_AUTOTWEET_VIEW_ABOUT_VIRTUALMANAGER_TITLE')
								. '</a></p><p class="text-right">';

							if (VirtualManager::getInstance()->isWorking())
							{
								echo JText::_('COM_AUTOTWEET_VIEW_ABOUT_VIRTUALMANAGER_WORKING');
								echo ' <i class="xticon xticon-sun-o"></i>';
							}
							else
							{
								echo JText::_('COM_AUTOTWEET_VIEW_ABOUT_VIRTUALMANAGER_RESTING');
								echo ' <i class="xticon xticon-moon-o"></i>';
							}

							echo '</p>';
						}
					?>

					<h3>
						<?php echo JText::_('COM_AUTOTWEET_VIEW_ABOUT_COMPONENTINFO_TITLE'); ?>
					</h3>

					<dl>
						<dt>
							<?php echo JText::_('COM_AUTOTWEET_VIEW_ABOUT_VERSIONINFO_NAME'); ?>
						</dt>
						<dd>
							<?php echo JText::_($this->comp['name']); ?>
						</dd>
						<dt>
							<?php echo JText::_('COM_AUTOTWEET_VIEW_ABOUT_VERSIONINFO_INSTALLED'); ?>
						</dt>
						<dd>
							<?php
							$version_client = $this->comp['client_version'];
							$version_color = null;
							$version_server = $this->comp['server_version'];

							if (version_compare($version_client, $version_server, '<'))
							{
								$version_html = '<a class="btn btn-danger" href="' . $this->comp['download'] . '" target="_blank">' . JText::_('COM_AUTOTWEET_VIEW_ABOUT_VERSIONINFO_DOWNLOAD') . '</a>';
								$version_message = $this->comp['message'];
							}
							else
							{
								$version_html = '<icon class="xticon xticon-check"></i>';
								$version_message = '';
							}

							echo $version_client;

							?>
						</dd>
						<dt>
							<?php echo JText::_('COM_AUTOTWEET_VIEW_ABOUT_VERSIONINFO_NEWEST'); ?>
						</dt>
						<dd>
							<?php echo $version_server; ?>
						</dd>
						<dt>
							<?php echo JText::_('COM_AUTOTWEET_VIEW_ABOUT_VERSIONINFO_INFO'); ?>
						</dt>
						<dd>
							<?php echo $version_html; ?>
							<br />
							<?php echo $version_message; ?>
						</dd>
					</dl>

					<?php
					}

					?>

					<h3>
						<a href="https://support.extly.com" target="_blank">
						<?php echo JText::_('COM_AUTOTWEET_VIEW_ABOUT_SUPPORT_TITLE'); ?>
						<i class="xticon xticon-link"></i>
						</a>
					</h3>
					<p>
						<?php echo JText::_('COM_AUTOTWEET_VIEW_ABOUT_SUPPORT_TWITTERFOLLOW'); ?>

					<div class="customsocialicons">
						<a target="_blank" href="http://twitter.com/extly"> <i
							class="xticon xticon-twitter"> </i>
						</a> <a target="_blank" href="http://www.facebook.com/extly"> <i
							class="xticon xticon-facebook-sign"></i>
						</a> <a target="_blank"
							href="http://www.linkedin.com/company/extly-com---joomla-extensions?trk=hb_tab_compy_id_2890809">
							<i class="xticon xticon-linkedin"></i>
						</a> <a target="_blank"
							href="https://plus.google.com/108048364795063174131"> <i
							class="xticon xticon-google-plus"> </i>
						</a> <a target="_blank" href="http://pinterest.com/extly/"> <i
							class="xticon xticon-pinterest"> </i>
						</a> <a target="_blank" href="https://github.com/anibalsanchez"> <i
							class="xticon xticon-github"> </i>
						</a>
					</div>

					<p>
						<?php

echo JText::sprintf('COM_AUTOTWEET_VIEW_ABOUT_SUPPORT_JEDREVIEW', '<a href="' . $this->comp['jed'] . '" target="_blank">Joomla! Extensions Directory: ');
						echo '</a>.';
						?>
					</p>
				</div>
			</div>

	</div>
</div>
