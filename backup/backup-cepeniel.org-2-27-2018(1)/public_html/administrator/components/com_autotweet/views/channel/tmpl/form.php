<?php
/**
 * @package     Extly.Components
 * @subpackage  com_autotweet - AutoTweetNG posts content to social channels (Twitter, Facebook, LinkedIn, etc).
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

$this->loadHelper('select');

JHtml::_('behavior.formvalidation');

?>

<div class="extly">
	<div class="extly-body">

		<?php echo Extly::showInvalidFormAlert(); ?>

		<form name="adminForm" id="adminForm" action="index.php" method="post" class="form form-horizontal form-validate">
			<input type="hidden" name="option" value="com_autotweet" />
			<input type="hidden" name="view" value="channels" />
			<input type="hidden" name="task" value="" />
			<input id="token" type="hidden" name="<?php echo JFactory::getSession()->getFormToken();?>" value="1" />
			<input type="hidden" name="Itemid" value="<?php echo $this->input->getInt('Itemid', 0); ?>" />

			<div class="row-fluid">

				<div class="span5">

					<fieldset class="basic">

						<legend>
							<?php echo JText::_('COM_AUTOTWEET_VIEW_CHANNEL_SELECTCHANNEL_TITLE'); ?>
						</legend>

						<div class="control-group">
							<label for="channeltype_id" class="control-label required" rel="tooltip" data-original-title="<?php echo JText::_('COM_AUTOTWEET_VIEW_CHANNEL_SELECTCHANNEL_DESC'); ?>"> <?php echo
							JText::_('COM_AUTOTWEET_VIEW_TYPE_TITLE');
							?> <span class="star">&#160;*</span>
							</label>
							<div class="controls">
								<?php echo SelectControlHelper::channeltypes($this->item->channeltype_id, 'channeltype_id', array('class' => 'required')); ?>
							</div>
						</div>

					</fieldset>

					<fieldset class="details">

						<legend>
							<?php echo JText::_('COM_AUTOTWEET_VIEW_CHANNEL_CHANNELDATA_TITLE'); ?>
						</legend>

						<div class="control-group">
							<label for="name" class="control-label required"><?php echo JText::_('COM_AUTOTWEET_VIEW_CHANNEL_NAME_TITLE'); ?> <span class="star">&#160;*</span> </label>
							<div class="controls">
								<input type="text" name="name" id="name" value="<?php echo $this->item->name; ?>" class="required" maxlength="64" required="required" />
							</div>
						</div>

						<div class="control-group">
							<label for="description" class="control-label"><?php echo JText::_('COM_AUTOTWEET_VIEW_DESCRIPTION_TITLE'); ?> </label>
							<div class="controls">
								<textarea name="description" id="description" rows="3" cols="40" maxlength="512"><?php
									echo $this->item->description;
								?></textarea>
							</div>
						</div>

<?php

						echo EHtmlSelect::publishedControl($this->item->get('published'), 'published');
?>


						<div class="control-group">
							<label for="autopublish" class="control-label" rel="tooltip" data-original-title="<?php echo JText::_('COM_AUTOTWEET_VIEW_AUTOPUBLISH_DESC'); ?>"><?php
							echo JText::_('COM_AUTOTWEET_VIEW_AUTOPUBLISH_TITLE'); ?> </label>
							<div class="controls">
								<?php echo EHtmlSelect::booleanList($this->item->autopublish, 'autopublish', null, 'JON', 'JOFF', 'autopublish'); ?>
							</div>
						</div>

						<div class="control-group">
							<label for="hashtags" class="control-label" rel="tooltip" data-original-title="<?php echo JText::_('COM_AUTOTWEET_VIEW_HASHTAGS_DESC'); ?>"><?php
							echo JText::_('COM_AUTOTWEET_VIEW_HASHTAGS_TITLE'); ?> </label>
							<div class="controls">
								<?php echo EHtmlSelect::booleanList(
										$this->item->xtform->get('hashtags', 1),
										'xtform[hashtags]',
										null,
										'JON',
										'JOFF',
										'hashtags'); ?>
							</div>
						</div>

						<div class="control-group">
							<label for="media_mode" class="control-label" rel="tooltip" data-original-title="<?php echo JText::_('COM_AUTOTWEET_VIEW_CHANNEL_MEDIAMODE_DESC'); ?>"><?php
							echo JText::_('COM_AUTOTWEET_VIEW_CHANNEL_MEDIAMODE_TITLE'); ?> </label>
							<div class="controls">
								<?php echo SelectControlHelper::mediamodes($this->item->media_mode, 'media_mode', null); ?>
							</div>
						</div>

						<?php
						if ((AUTOTWEETNG_JOOCIAL) && (EParameter::getComponentParam(CAUTOTWEETNG, 'targeting', false)))
						{
						?>
			            <hr/>

			            <div class="control-group">
			              <label class="control-label" for="xtformtarget_id" id="target_id-lbl"><?php echo JText::_('COM_AUTOTWEET_VIEW_FBWACCOUNT_TARGETING_TITLE'); ?></label>
			              <div class="controls">
			                <?php echo SelectControlHelper::targets($this->item->xtform->get('target_id'), 'xtform[target_id]', null); ?>
			              </div>
			            </div>

						<?php
						}
						?>

						<div class="control-group">
							<label for="channel_id" class="control-label" rel="tooltip" data-original-title="<?php echo JText::_('JGLOBAL_FIELD_ID_DESC'); ?>"><?php
							echo JText::_('JGLOBAL_FIELD_ID_LABEL'); ?> </label>
							<div class="controls">
								<input type="text" name="id" id="channel_id" value="<?php echo $this->item->id; ?>" class="uneditable-input" readonly="readonly">
							</div>
						</div>

					</fieldset>

				</div>

				<div class="span7">

					<div class="row-fluid">
						<div class="span12">
							<div id="channel_data">
								<fieldset class="channel_data">
									<p style="text-align: center;">
										<span class="loaderspinner">&nbsp;</span>
									</p>
									<legend>
										<?php echo JText::_('...requesting channel data...'); ?>
									</legend>
								</fieldset>
							</div>
						</div>
					</div>

					<?php

					$alert_message = $this->get('alert_message');

					if ( ($this->item->id) && (!empty($alert_message)) )
					{
						include_once 'audit.php';
					}

					?>

				</div>

			</div>
		</form>
	</div>
</div>
<script type="text/javascript">

window.addEvent('domready', function() {

	document.formvalidator.setHandler('token',
			function (value) {
				regex=/^[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]$/;
				return regex.test(value);
			}
	);

	document.formvalidator.setHandler('facebookapp',
			function (value) {
				regex=/^http(s)?\:\/\/apps\.facebook\.com\/[a-zA-Z0-9-_]+$/;
				return regex.test(value);
			}
	);

});

</script>
