<?php
/**
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');

/**
 * Media Component List Model
 *
 * @package		Joomla.Administrator
 * @subpackage	com_media
 * @since 1.5
 */
class ModelTemedia extends JModelList
{
    
    /**
     * Indicates if view is allowed
     *
     * @var    boolean
     */
    protected $folder = -1;
    
	function getState($property = null, $default = null, $parent = true)
    {
        static $set;

        if (!$set  && $parent) {
            $this->setState('folder', $this->folder);

            $parent = str_replace("\\", "/", dirname($this->folder));
            $parent = ($parent == '.') ? null : $parent;
            $this->setState('parent', $parent);
            $set = true;
        }

        return parent::getState($property, $default);
    }
}
