<?php
/**
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');
jimport('teweb.file.urlbuilder');
jimport('teweb.media.functions');

/**
 * Media Component List Model
 *
 * @package		Joomla.Administrator
 * @subpackage	com_media
 * @since 1.5
 */
class ModelTemedialist extends JModelList
{
	/**
     * Indicates the folder setting
     *
     * @var    boolean
     */
    protected $folder = -1;
    
    /**
     * Indicates the baseURL for the download
     *
     * @var    boolean
     */
    protected $basePATH = 'images';
    
    function getState($property = null, $default = null, $parent = true)
    {
        static $set;

        if (!$set  && $parent) {
            $this->setState('folder', $this->folder);

            $parent = str_replace("\\", "/", dirname($this->folder));
            $parent = ($parent == '.') ? null : $parent;
            $this->setState('parent', $parent);
            $set = true;
        }

        return parent::getState($property, $default);
    }
    
    function getbaseURL()
    {
        $this->_baseURL = JURI::ROOT().$this->basePATH;
        return $this->_baseURL;
    }

	function getImages()
	{
		$list = $this->getList();

		return $list['images'];
	}

	function getFolders()
	{
		$list = $this->getList();

		return $list['folders'];
	}

	function getDocuments()
	{
		$list = $this->getList();

		return $list['docs'];
	}

	/**
	 * Build imagelist
	 *
	 * @param string $listFolder The image directory to display
	 * @since 1.5
	 */
	function getList()
	{
		static $list;

		// Only process the list once per request
		if (is_array($list)) {
			return $list;
		}
        
        $list = array('folders' => array(), 'docs' => array(), 'images' => array());

		// Get current path from request
		$current = $this->getState('folder');
        $relpath = '/'.JRequest::getVar('relpath', '');
        
        $folderpath = $this->basePATH;
        if ($folderpath === false)
        {return $list;}
        
        // build path
        
        $basePath = JPATH_ROOT.DIRECTORY_SEPARATOR.$folderpath.$relpath;

		$mediaBase = str_replace(DIRECTORY_SEPARATOR, '/', JPATH_ROOT.DIRECTORY_SEPARATOR.$folderpath.'/');
		$images		= array ();
		$folders	= array ();
		$docs		= array ();

		$fileList = false;
		$folderList = false;
		if (file_exists($basePath))
		{
			// Get the list of files and folders from the given folder
			$fileList	= JFolder::files($basePath);
			$folderList = JFolder::folders($basePath);
		}

		// Iterate over the files if they exist
		if ($fileList !== false) {
			foreach ($fileList as $file)
			{
				if (is_file($basePath.'/'.$file) && substr($file, 0, 1) != '.' && strtolower($file) !== 'index.html') {
					$tmp = new JObject();
					$tmp->name = $file;
					$tmp->title = $file;
					$tmp->path = str_replace(DIRECTORY_SEPARATOR, '/', JPath::clean($basePath . '/' . $file));
                    if ($current != -1 && $current != '')
                    {
					    $tmp->path_relative = str_replace($mediaBase, '', $tmp->path);
                        $tmp->path_rel = $tmp->path_relative;
                    }
                    else {$tmp->path_relative = str_replace(str_replace(DIRECTORY_SEPARATOR, '/', JPATH_ROOT.DIRECTORY_SEPARATOR), '', $tmp->path);
                            $tmp->path_rel = substr($tmp->path_relative, 7);
                    }
					$tmp->size = filesize($tmp->path);

					$ext = strtolower(JFile::getExt($file));
					switch ($ext)
					{
						// Image
						case 'jpg':
						case 'png':
						case 'gif':
						case 'xcf':
						case 'odg':
						case 'bmp':
						case 'jpeg':
						case 'ico':
							$info = @getimagesize($tmp->path);
							$tmp->width		= @$info[0];
							$tmp->height	= @$info[1];
							$tmp->type		= @$info[2];
							$tmp->mime		= @$info['mime'];

							if (($info[0] > 16) || ($info[1] > 16)) {
								$dimensions = Tewebmedia::imageadjustsize($info[0], $info[1], 16);
								$tmp->width_60 = $dimensions[0];
								$tmp->height_60 = $dimensions[1];
							}
							else {
								$tmp->width_60 = $tmp->width;
								$tmp->height_60 = $tmp->height;
							}

							if (($info[0] > 16) || ($info[1] > 16)) {
								$dimensions = Tewebmedia::imageadjustsize($info[0], $info[1], 16);
								$tmp->width_16 = $dimensions[0];
								$tmp->height_16 = $dimensions[1];
							}
							else {
								$tmp->width_16 = $tmp->width;
								$tmp->height_16 = $tmp->height;
							}

							$images[] = $tmp;
							break;

						// Non-image document
						default:
                        if (file_exists(JPATH_SITE.DIRECTORY_SEPARATOR."lib_teweb/mime-icon-16/".$ext.".png"))
						{
                            $tmp->icon_16 = "lib_teweb/mime-icon-16/".$ext.".png";
                        }
                        else {
                            $tmp->icon_16 = "lib_teweb/mime-icon-16/doc.png";
                        }
							$docs[] = $tmp;
							break;
					}
				}
			}
		}

		// Iterate over the folders if they exist
		if ($folderList !== false) {
			foreach ($folderList as $folder)
			{
				$tmp = new JObject();
				$tmp->name = basename($folder);
				$tmp->path = str_replace(DIRECTORY_SEPARATOR, '/', JPath::clean($basePath . '/' . $folder));
				$tmp->path_relative = str_replace($mediaBase, '', $tmp->path);
				$count = Tewebmedia::countFiles($tmp->path);
				$tmp->files = $count[0];
				$tmp->folders = $count[1];

				$folders[] = $tmp;
			}
		}

		$list = array('folders' => $folders, 'docs' => $docs, 'images' => $images);

		return $list;
	}
}
