<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.view');
class ViewTemedia extends JViewLegacy
{
    protected $filelist = true;
    protected $folder = null;
    protected $folderchange = null;
    protected $media = 'image';
    protected $id3data = false;
    protected $mediavalue = 1;
    protected $thirdparty = true;
    protected $uploadstyle = 'block';
    protected $upload_folder = null;
    protected $f_folder = null;
    protected $linkto_folder = null;
    protected $artistlist = null;
    protected $albumlist = null;
    protected $params = null;
    protected $title_title = null;
    protected $artist_title = null;
    protected $album_title = null;
    protected $tags_title = null;
    protected $comment_title = null;
    protected $duration_title = null;
    protected $canMediaAdmin = null;
    protected $canMediaCreate = null;
    protected $JTPREFIX = null;
    
    
	function display($tpl = null)
	{
        $user    = JFactory::getUser();
        $this->asset = JRequest::getVar('asset', '');
		$app	= JFactory::getApplication();
		$lang	= JFactory::getLanguage();
        $option = 
        $params = $this->params;
		$append = '';
        $host = JURI::root();
        JHtml::_('behavior.framework', true);
        $document = JFactory::getDocument();
        $document->addScript($host.'media/lib_teweb/valumsupload/js/header.js');
        $document->addScript($host.'media/lib_teweb/mediamanager/temediamanager.js');
        $document->addScript($host.'media/lib_teweb/valumsupload/js/util.js');
        $document->addScript($host.'media/lib_teweb/valumsupload/js/button.js');
        $document->addScript($host.'media/lib_teweb/valumsupload/js/ajax.requester.js');
        $document->addScript($host.'media/lib_teweb/valumsupload/js/handler.base.js');
        $document->addScript($host.'media/lib_teweb/valumsupload/js/deletefile.ajax.requester.js');
        $document->addScript($host.'media/lib_teweb/valumsupload/js/window.receive.message.js');
        $document->addScript($host.'media/lib_teweb/valumsupload/js/handler.form.js');
        $document->addScript($host.'media/lib_teweb/valumsupload/js/handler.xhr.js');
        $document->addScript($host.'media/lib_teweb/valumsupload/js/uploader.basic.js');
        $document->addScript($host.'media/lib_teweb/valumsupload/js/dnd.js');
        $document->addScript($host.'media/lib_teweb/valumsupload/js/uploader.js');
        $document->addStyleSheet($host.'media/lib_teweb/valumsupload/fineuploader.css');
        $document->addStyleSheet($host.'media/lib_teweb/mediamanager/temediamanager.css');
        
        // add path to admin tmpl area
        $this->_addPath('template', JPATH_SITE.DIRECTORY_SEPARATOR.'libraries/teweb/file/temediatmpl');

		JHtml::_('behavior.framework', true);

		if ($lang->isRTL()) {
			$document->addStyleSheet($host.'media/lib_teweb/mediamanager/temediamanager-rtl.css');
		}

		/*
		 * Display form for FTP credentials?
		 * Don't set them here, as there are other functions called before this one if there is any file write operation
		 */
		$ftp = !JClientHelper::hasCredentials('ftp');

		$this->session = JFactory::getSession();
		$this->state = $this->get('state');
		$this->require_ftp = $ftp;
        $this->fieldid = JRequest::getVar('fieldid', '');

		parent::display($tpl);
	}
}
