<?php
/**
 * @Library - teweb library
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('teweb.file.upload');
jimport('teweb.file.urlbuilder');
jimport('teweb.media.functions');
jimport('teweb.file.uploadscript');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');
/**
 * Handle file uploads via XMLHttpRequest
 */
class Temediamanager {
    
/**
     * Method to upload file to temp folder and then to folder setting
     * @param unknown type $params component params
     * @param int $folder folder setting
     * @param string $media media type
     * @param unknown type $class class object
     * @return JSON string
     */
    
    function upload($params, $folder, $media, $class, $mimetable = null)
    {
        $relpath = JRequest::getVar('relpath', '');
        if ($relpath != '')
        {$relpath = $relpath.'/';}
        $file = $_FILES['qqfile']['name'];
        $asset = JRequest::getVar('asset', '');
        $allowedExtensions = array();
        $sizeLimit = Tewebdetails::maxuploadbytes();
        $temp_folder = Tewebupload::gettempfolder();
        
         // test to see if this is a valid request
        if (!$media || !$folder || $media == 0 || $folder == 0 || $file == '' || !Tewebupload::checkfile($file))
        {   
            $result =  array('error' => JText::_('LIB_TEWEB_MESSAGE_ERROR_NO_FILE'));
        }
        else
        {
            $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
            $result = $uploader->handleUpload($temp_folder);

            // if successful upload - use folder details to transfer files from tmp folder
            if (isset($result['success']) && $result['success'] == true)
            {
                $tempfile = $temp_folder.$file;  
                //need to generate and get studyid to prefixes working properly
                $filename = $class->buildpath($file, 1, $media, '', $folder, 1, $relpath);  
                // get id3 info if available available before moving file
                $data = Tewebmedia::getid3($tempfile, 1);
                $uploadmsg = Tewebupload::processflashfile($tempfile, $filename, $mimetable);
                if ($uploadmsg) 
                { 
                    $result = array('error' => $uploadmsg);
                }     
                else {
                    $filesize = filesize($tempfile);
                    // delete temp file
                    Tewebupload::deletetempfile($tempfile);
                    $details = array("title" => $data["song"], "hrs" => $data["hrs"], "mins" => $data["mins"], "secs" => $data["secs"], "album" => $data["album"], "artist" => $data["artist"], "comment" => $data["comment"], "albumid" => $data["album"], "artistid" => $data["artist"], "setfolder" => $folder, "setfile" => $filename->file);
                    $result = array_merge($result, $details);
                }
            }
        }
        return $result;
    }
    
/**
     * Method to get ID3 daya for a file - ajax call from media center
     *
     *
     * @return JSON string
     */

     function getId3($foldertable)
     {
        $file = JRequest::getVar('file', '');
        $folder = JRequest::getInt('folder', '');
        $fileurl = Tewebbuildurl::geturl($file, $folder, $foldertable);
        $filepath = Tewebfile::adjusttoabsolute($fileurl);
        $data = Tewebmedia::getid3($filepath, 1);   
        $details = array("title" => $data["song"], "hrs" => $data["hrs"], "mins" => $data["mins"], "secs" => $data["secs"], "album" => $data["album"], "artist" => $data["artist"], "comment" => $data["comment"], "albumid" => $data["album"], "artistid" => $data["artist"], "setfolder" => $folder, "setfile" => $file);
        return $details;
     }

/**
     * Method to get third party video details
     *
     *
     * @return JSON string
     */
     
     function get3rdpartydata()
     {
        $url = JRequest::getVar('file', '');
        $details = array();
        $type = '';
        $info = '';
        if ($url)
        {
            $test = strtolower($url);
            $vimeo = 'vimeo.com';
            $youtube = 'youtube.com';
            $bliptv = 'blip.tv';
            if (strlen(strstr($url,$vimeo))>0)
            {
                $type = 'vimeo';
                $info = Tewebmedia::vimeoinfo($url);
            }

            elseif (strlen(strstr($url,$youtube))>0)
            {
                $type = 'youtube';
                $info = Tewebmedia::youtubeinfo($url);
            }
            elseif (strlen(strstr($url,$bliptv))>0)
            {
                $type = 'bliptv';
                $info = Tewebmedia::bliptvinfo($url);
            }    
        }
        if ($info == '')
        {
            $details = array ("error" => JText::_('COM_PREACHIT_ERROR_NOT_POSSIBLE_GET_INFO')." ".JText::_('COM_PREACHIT_ERROR_NOT_POSSIBLE_GET_INFO_DERVICE'));
        }
        else {
            $details = array("title" => $info->name, "hrs" => $info->hrs, "mins" => $info->mins, "secs" => $info->secs, "tags" => $info->tags, "comment" => $info->description, "setfolder" => "", "setfile" => $info->link, "image" => $info->imagelrg, "video_type" => $info->type, "thirdparty" => '1');
        }
        return $details;
     }
     
/**
     * Method to check whether folder can receive uploads - respond to ajax call
     * @param string $table JTable name for the details of the folderr
     *
     * @return JSON string
     */

     function uploadFoldertest($table)
     {
        $current = JRequest::getVar('folder', 0);
        $folders = JTable::getInstance($table, 'Table');
        $folders->load($current);
        if ($current == 0 || ($folders->type != 0 && $folders->type != 2 && $folders->type != 3))
        {$details = array ("upfolder" => '0');}
        else {$details = array ("upfolder" => '1');}
        return $details;
     }
     
/**
     * Method to delete Media
     * @param string $table Jtable to search
     * @param string $folderpath folder path to build from
     *
     * @return JSON string
     */

     function deleteMedia($table, $folderpath = null)
     {
        $type = JRequest::getVar('type', 'file'); 
        $file = JRequest::getVar('file', '');
        $folder = JRequest::getInt('folder', 0);
        $abspath = JPATH_SITE;
        if (!$file)
        {return array('error' => JText::_('LIB_TEWEB_MESSAGE_ERROR_DELETE_FILE'));}
        if ($folderpath == null)
        {
            if ($folder > 0)
            {
                //get ministry alias
                $folder_details =& JTable::getInstance($table, 'Table');  
                $folder_details->load($folder);
                $folderpath = $folder_details->folder;
                if ($folder_details->type != 0)
                {return array('error' => JText::_('LIB_TEWEB_MESSAGE_ERROR_DELETE_FILE'));} 
                
            }
            elseif ($folder == -1 && $type == 'folder')
            {
                $folderpath = 'images';
            }
            elseif ($folder != -1)
            {return array('error' => JText::_('LIB_TEWEB_MESSAGE_ERROR_DELETE_FILE'));}
        }
        
        //remove last / if present from folder
        $folderpath = Tewebbuildurl::cleanfolder($folderpath);
        if ($folderpath != '')
        {
            $folderpath = $folderpath.'/';
        }
        
        if ($type == 'folder')
        {
            $folders = array();
            $folders[] = $abspath.DIRECTORY_SEPARATOR.$folderpath.$file;
            $noerror = true;
            foreach ($folders AS $f)
            {
                if (JFolder::exists($f))
                {
                    if (!JFolder::delete($f))
                    {
                        $noerror = false;
                    }
                }
            }
            if ($noerror)
            {return array('success' => JText::_('LIB_TEWEB_MESSAGE_DELETED'));} 
            else {return array('error' => JText::_('LIB_TEWEB_MESSAGE_ERROR_DELETE_FILE'));} 
        }
        
        elseif ($type == 'file')
        {
            $files = array();
            $files[] = $abspath.DIRECTORY_SEPARATOR.$folderpath.$file;
            $noerror = true;
            foreach ($files AS $f)
            {
                if (JFile::exists($f))
                {
                    if (!JFile::delete($f))
                    {
                        $noerror = false;
                    }
                }
            }
            if ($noerror)
            {return array('success' => JText::_('LIB_TEWEB_MESSAGE_DELETED'));} 
            else {return array('error' => JText::_('LIB_TEWEB_MESSAGE_ERROR_DELETE_FILE'));} 
        }
        else {return array('error' => JText::_('LIB_TEWEB_MESSAGE_ERROR_DELETE_FILE'));}
     }
     
/**
     * Method to create folder
     * @param string $table Jtable to search
     * @param string $folderpath folder path to build from
     *
     * @return JSON string
     */

     function createFolder($table, $folderpath = null)
     {
        $path = JRequest::getVar('crfolder', '');
        $relpath = JRequest::getVar('relpath', '');
        $folder = JRequest::getInt('folder', 0);
        $abspath = JPATH_SITE;
        if (!$path)
        {return array('error' => JText::_('LIB_TEWEB_MESSAGE_ERROR_CREATE_FOLDER'));}
        if ($folderpath == null)
        {
            if ($folder > 0)
            {
                //get ministry alias
                $folder_details =& JTable::getInstance('Filepath', 'Table');  
                $folder_details->load($folder);
                $folderpath = $folder_details->folder;
                if ($folder_details->type != 0)
                {return array('error' => JText::_('LIB_TEWEB_MESSAGE_ERROR_CREATE_FOLDER'));} 
            }
            elseif ($folder == -1)
            {
                $folderpath = 'images';
            }
            else {return array('error' => JText::_('LIB_TEWEB_MESSAGE_ERROR_CREATE_FOLDER'));}
        }
        
        //remove last / if present from folder
        $folderpath = Tewebbuildurl::cleanfolder($folderpath);
        if ($folderpath != '')
        {
            $folderpath = $folderpath;
        }
        $relpath = Tewebbuildurl::cleanfolder($relpath);
        if ($relpath != '')
        {
            $folderpath = $folderpath.'/'.$relpath;
        }
        $path = Tewebbuildurl::cleanfolder($path);
        
        $folders = array();
        $folders[] = $abspath.DIRECTORY_SEPARATOR.$folderpath.'/'.$path;
        $noerror = true;
        foreach ($folders AS $f)
        {
            if (!JFolder::exists($f))
            {
                if (!JFolder::create($f))
                {
                    $noerror = false;
                }
            }
        }
        if ($noerror)
        {return array('success' => JText::_('LIB_TEWEB_MESSAGE_FOLDER_CREATED'));} 
        else {return array('error' => JText::_('LIB_TEWEB_MESSAGE_ERROR_CREATE_FOLDER'));} 
     }
}
