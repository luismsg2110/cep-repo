<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_media
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;
$user = JFactory::getUser();
$option = JRequest::getCmd('option');
JText::script('LIB_TEWEB_ID3_ALERT');
JHtml::_('behavior.keepalive');
?>
<script language="javascript" type="text/javascript">
function techangetab(element, id)
{
    //hide all elements
    $$('div.tabcontent').set('class', 'tabcontent tabcontent-closed');
    // set all li to closed
    $$('li.tabs').set('class', 'tabs closed');
    // show right id
    $(id).set('class', 'tabcontent tabcontent-open');
    $(element).set('class', 'tabs open');
    
}
if (window.addEventListener){
 window.addEventListener('load', function() { return techangetab('tetab1', 'teupload'); }, false);
} else if (window.attachEvent){
 window.attachEvent('load', function() { return techangetab('tetab1', 'teupload'); });
}
</script>
<form action="index.php?option=<?php echo $option;?>&amp;asset=<?php echo JRequest::getCmd('asset');?>" id="imageForm" method="post" enctype="multipart/form-data">
	<div id="messages" style="display: none;">
		<span id="message"></span><?php echo JHtml::_('image', 'media/dots.gif', '...', array('width' =>22, 'height' => 12), true)?>
	</div>
    <div class="title-corner">
        <?php echo JText::_('LIB_TEWEB_MEDIAMANAGER_FILE_LEGEND');?>
    </div>
    <div class="teinsert">
        <div class="fltrt temain-btn">
            <button class="btn btn-primary" type="button" onclick="temediaManager.enterValues('<?php echo $this->fieldid;?>', 'jform_<?php echo $this->folderchange;?>'); temediaManager.fillMainform('<?php echo $this->folderchange;?>');window.parent.SqueezeBox.close();"><?php echo JText::_('LIB_TEWEB_INSERT') ?></button>
            <button class="btn" type="button" onclick="window.parent.SqueezeBox.close();"><?php echo JText::_('JCANCEL') ?></button>
            <div class="clear: both;"></div>
        </div>
        <ul>
                <li class="temedia-inline"><label for="f_folder"><?php echo JText::_('LIB_TEWEB_MEDIA_FOLDER') ?></label>
                    <?php echo $this->f_folder;?></li>
                <li class="temedia-inline"><label for="f_url"><?php echo JText::_('LIB_TEWEB_MEDIA_URL') ?></label>
                    <input type="text" id="f_url" value="" disabled="true"/></li>
        </ul>
    </div>
    <div class="clr"></div>
<div class="tab-container">
    <ul id="tab_group_id" class="tabs">
        <li id="tetab1" class="tabs open" style="cursor: pointer;"><a onclick="techangetab('tetab1', 'teupload'); return false;" href="#">Upload/Select</a></li>
        <?php if ($this->params->get('no_ext_url', 0) != 1) {?>
        <li id="tetab2" class="tabs closed" style="cursor: pointer;"><a onclick="techangetab('tetab2', 'telinkto'); return false;" href="#">Link to</a></li>
        <?php } ?>
        <?php
        if ($this->canMediaCreate && $this->thirdparty) {
        ?>
        <li id="tetab3" class="tabs closed" style="cursor: pointer;"><a onclick="techangetab('tetab3', '3rdpartyform'); return false;" href="#">3rd party Video</a></li>
        <?php } ?>
        <?php
        if ($this->id3data == 1) {
        ?>
        <li id="tetab4" class="tabs closed" style="cursor: pointer;"><a onclick="techangetab('tetab4', 'id3data'); return false;" href="#">ID3 data</a></li>
        <?php } ?>
    </ul>
    <div id="teupload" class="tabcontent tabcontent-open">
        <?php if ($this->filelist || $this->params->get('default_folder_only', 0) == 0) {?>
        <?php if ($this->filelist) {?>
        <iframe id="temediaframe" name="temediaframe" src="index.php?option=<?php echo $option;?>&amp;view=temedialist&amp;tmpl=component&amp;media=<?php echo $this->media?>&amp;folder=<?php echo $this->state->folder?>"></iframe>
        <?php } ?>
		<div class="fltlft">
            <ul>
            <?php if ($this->params->get('default_folder_only', 0) != 1) {?>
                <li>
			        <label for="folder"><?php echo JText::_('LIB_TEWEB_ADMIN_FOLDER') ?></label>
                    <?php echo $this->upload_folder; ?>
                </li>
            <?php } ?>
            <?php if ($this->filelist) {?>
                <li>
                    <label for="folder"><?php echo JText::_('LIB_TEWEB_MEDIA_REL_PATH') ?></label>
                    <input style="background-color: transparent; border: none;" id="relpath_notice" readonly="readonly" value=""/> 
                    <button class="btn" type="button" id="upbutton" onclick="temediaManager.upFolder()" title="<?php echo JText::_('LIB_TEWEB_DIRECTORY_UP') ?>"><?php echo JText::_('LIB_TEWEB_UP') ?></button>
                </li>
                <?php if ($this->canMediaAdmin) {?>
                <li>
                    <label for="folder"><?php echo JText::_('LIB_TEWEB_MEDIA_CREATE_PATH') ?></label>
                    <input id="create_folder" value=""/> 
                    <button class="btn" type="button" id="crbutton" onclick="temediaManager.createFolder()" title="<?php echo JText::_('LIB_TEWEB_FOLDER_CREATE') ?>"><?php echo JText::_('LIB_TEWEB_FOLDER_CREATE') ?></button>
                </li>
            <?php } ?>
            <?php } ?>
            </ul>
                <div class="clr clearfix"></div>
		</div>
        <div class="clr clearfix"></div>
        <div class="teseparator"></div>
    <?php } ?>    
    <?php if ($this->canMediaCreate): ?>
        <div id="uploadform" style="display: <?php echo $this->uploadstyle;?>;">
            <label><?php echo JText::_('LIB_TEWEB_UPLOAD_LABEL');?></label>
            <div id="file-uploader">
            </div>
            <script language="javascript" type="text/javascript">

var uploader = new qq.FineUploader({
    // pass the dom node (ex. $(selector)[0] for jQuery users)
    element: document.getElementById('file-uploader'),
    // path to server-side upload script
    request: {
        endpoint: '<?php echo JURI::ROOT();?>administrator/index.php?'
    },
    callbacks: {
        onSubmit: function(id, fileName){
                document.id('upload-error-response').innerHTML = '';
                var contask = parent.document.getElementsByName('controller')[0].value+'.uploadfile';
                var obj1 = $(parent.document.adminForm).toQueryString().parseQueryString();
                var obj2 = { folder: document.id('view_folder').value, relpath: temediaManager.getMediaFolder(), media: '<?php echo $this->mediavalue;?>', asset: '<?php echo $this->asset;?>', task: contask, studyid: temediaManager.getID() };
                var object3 = temediaManager.merge_options(obj1, obj2); 
            uploader.setParams(object3); },
        onComplete: function(id, fileName, responseJSON){
            if (responseJSON.error)
            {
                document.id('upload-error-response').innerHTML = responseJSON.error;
            }
            else {
                // refresh the iframe
                <?php if ($this->filelist) {?>
                temediaManager.setFolder(document.getElementById('relpath_notice').value);
                <?php } ?>
                temediaManager.populateFieldsupload(responseJSON);
                <?php if ($this->id3data == 1) {?>
                temediaManager.popId3table(responseJSON);
                <?php } ?>
            }
        }
    },
    messages: {
            typeError: "<?php echo JText::_('LIB_TEWEB_MESSAGE_UPLOAD_FAILED_CHECK_EXT');?>",
            sizeError: "<?php echo JText::_('LIB_TEWEB_MESSAGE_FILE_TOO_LARGE_THAN_PHP_INI_ALLOWS');?>",
            minSizeError: "<?php echo JText::_('LIB_TEWEB_MESSAGE_ERROR_NO_FILE');?>",
            emptyError: "<?php echo JText::_('LIB_TEWEB_MESSAGE_ERROR_NO_FILE');?>",
            onLeave: "<?php echo JText::_('LIB_TEWEB_LEAVE_ERROR');?>"            
        },
});


</script>
        <div id="upload-error-response"></div>
        <span class="te-help small"><?php echo JText::_($this->JTPREFIX.'_ADMIN_MAX_UPLOAD').' '.Tewebdetails::maxupload();?></span>
        </div>
<?php  endif; ?>
    </div>
    <?php if ($this->params->get('no_ext_url', 0) == 0) {?>
        <div id="telinkto" class="tabcontent tabcontent-closed">
            <div class="tetext">
                <label>
                    <?php echo JText::_('LIB_TEWEB_LINK_LABEL');?>
                </label>
            </div>
            <ul>
                <li>
                    <label for="folder"><?php echo JText::_('LIB_TEWEB_ADMIN_FOLDER') ?></label>
                    <?php echo $this->lt_folder; ?>
                </li>
                <li class="temedia-inline"><label for="linkto_url"><?php echo JText::_('LIB_TEWEB_LINK_URL') ?></label>
                    <input type="text" size="60" id="lt_url" value=""/>
                    <button class="btn btn-success" type="button" id="ltbutton" onclick="temediaManager.processLink()" title="<?php echo JText::_('LIB_TEWEB_LINK_PROCESS') ?>"><?php echo JText::_('LIB_TEWEB_LINK_PROCESS') ?></button>
                    <span class="te-help small"><?php echo JText::_('LIB_TEWEB_LINK_HELP_GUIDE');?></span>
                </li>
            </ul>
        </div>
    <?php } ?>
    <?php
        if ($this->canMediaCreate && $this->thirdparty) {
    ?>
    <div id="3rdpartyform" class="tabcontent tabcontent-closed">
        <div class="tetext">
            <label><?php echo JText::_($this->JTPREFIX.'_ADMIN_THIRDPARTY_TITLE');?></label>
            <p class="desc">
                <?php echo JText::_($this->JTPREFIX.'_ADMIN_THIRDPARTY_HELP');?>
            </p>
        </div>
        <ul>
            <li class="temedia-inline">
                <label><?php echo JText::_($this->JTPREFIX.'_ADMIN_3RD_PARTY_VIDEO');?></label>
                <input type="text" name="video_third" id="video_third" size="60" maxlength="150" value ="" />
                <button class="btn btn-success" type="button" onclick="temediaManager.get3rdparty()"><?php echo JText::_($this->JTPREFIX.'_ADMIN_BUTTON_THIRD_PARTY');?> </button>
            </li>
        </ul>
        <div id="3rd-error-response"></div>
    </div>
    <?php  } ?>
    <?php
        if ($this->id3data == 1) {
    ?>
    <div id="id3data" class="tabcontent tabcontent-closed">
        <div class="tetext">
            <label><?php echo JText::_('LIB_TEWEB_MEDIAMANAGER_FILE_INFO_LEGEND');?></label>
        </div>
        <ul class="file-info">    
            <li class="temedia-inline" id="title-cont"><label for="title"><?php echo $this->title_title; ?></label><input size="50" type="text" id="title" value="" /></li>
            <li class="temedia-inline" id="album-cont"><label for="album"><?php echo $this->album_title; ?></label><?php echo $this->albumlist;?></li>
            <li class="temedia-inline" id="artist-cont"><label for="artist"><?php echo $this->artist_title;  ?></label><?php echo $this->artistlist;?></li>
            <li class="temedia-inline" id="duration-cont"><label for="duration"><?php echo $this->duration_title;  ?></label><input type="text" size="3" id="hrs" value="" /> <input type="text" size="3" id="mins" value="" /> <input type="text" size="3" id="secs" value="" /></li>
            <li class="temedia-inline" id="tags-cont"><label for="tags"><?php echo $this->tags_title;  ?></label><input size="55" type="text" id="tags" value="" /></li>
            <li class="temedia-inline" id="mediaimage-cont"><label for="mediaimage"><?php echo $this->image_title;  ?></label><input size="35" type="text" id="mediaimage" value="" /></li>
            <li class="temedia-inline" id="comment-cont"><label for="comment"><?php echo $this->comment_title;  ?></label><textarea rows="5" cols="30" id="comment"></textarea></li>
        </ul>
    </div>
</div>
    <?php } ?>
		<input type="hidden" id="dirPath" name="dirPath" />
		<input type="hidden" id="f_file" name="f_file" />
		<input type="hidden" id="tmpl" name="component" />
        <input type="hidden" id="id" name="id" value="0" />
        <input type="hidden" id="video_type" name="video_type" value="" />
        <input type="hidden" id="thirdparty" name="thirdparty" value="" />
        <?php if ($this->params->get('default_folder_only', 0) == 1) {?>
        <input type="hidden" id="view_folder" name="view_folder" value="<?php echo $this->folder;?>" />
        <?php } ?>
</form>
