<?php
/**
 * @Library - teweb library
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('teweb.file.functions');

class Tewebupload{

/**
     * Method to initiliase filename array
     *
     * @return    array
     */
     
public static function initialisfileinfo()
{
$filename = new stdClass();
$filename->type = null;
$filename->ftphost = null;
$filename->ftpuser = null;
$filename->ftppassword = null;
$filename->ftpport = null;
$filename->aws_key = null;
$filename->aws_secret = null;
$filename->aws_bucket = null;
return $filename;
}
    
/**
	 * Method to make safe filename
	 *
	 * @param	string $file  Filename.
	 *
	 * @return	array
	 */

public static function makesafe($file)
{
    jimport('joomla.filesystem.file');
    //This removes any characters that might cause headaches to browsers. This also does the same thing in the model
    $badchars = array(' ', '\'', '"', '`', '@', '^', '!', '#', '$', '%', '*', '(', ')', '[', ']', '{', '}', '~', '?', '>', '<', ',', '|', '\\', ';', '&', '_and_');
    $safefile = str_replace($badchars, '_', $file);
    $safefile = JFILE::makeSafe($safefile);
    return $safefile;
}

/**
	 * Method to get temp file name from post
	 *
	 * @return	string
	 */

public static function gettempfile()
{
	$temp = JRequest::getVar ( 'flupfile', '', 'POST', 'STRING');
	return $temp;
}

/**
	 * Method to build temp folder
	 *
	 * @return	string
	 */

public static function gettempfolder()
{
	$app = JFactory::getApplication();
	$temp_folder = $app->getCfg('tmp_path').DIRECTORY_SEPARATOR;
	return $temp_folder;
}

/**
	 * Method to get path variable and redirect if none
	 *
	 * @param	array $row  message details.
	 * @param	string $tempfile  Temp file path.	 
	 * @return	string
	 */

public static function getpath($url, $tempfile, $front = '')
{
	jimport('joomla.filesystem.file');
    $app = JFactory::getApplication();
	$path = JRequest::getVar('upload_folder', '', 'POST', 'INT');
	if ($path == '')
	{
		if ($tempfile)
		{JFile::delete($tempfile);}
        $app->enqueueMessage ( JText::_('LIB_TEWEB_MESSAGE_UPLOAD_FAILED_NO_FOLDER'), 'warning' );
		$this->setRedirect($url);
	}
	return $path;
}

/**
	 * Method to delete temp file
	 *
	 * @param	string $tempfile  Temp file path.
	 *
	 * @return	bolean
	 */

public static function deletetempfile($tempfile)
{
	$db = JFactory::getDBO();
	jimport('joomla.filesystem.file');
	// delete file
	JFile::delete($tempfile);
	return true;
}

/**
	 * Method to check upload file to see if it is allowed
	 *
	 * @param	array $file  File info
	 *
	 * @return	bolean
	 */

public static function checkfile($file, $blacklist = null, $whitelist = null)
{
	$allow = false;	
    if (!$blacklist)
	{$blacklist = array(".php", ".phtml", ".php3", ".php4", ".js", ".shtml", ".pl" ,".py");}
    if (!$whitelist) {
	    foreach ($blacklist as $ext)
	    {
		    if(preg_match("/$ext\$/i", $file))
		    {$allow = false; }
            else {$allow = true;}
	    }
    }
    else  {
        foreach ($whitelist as $ext)
        {
            if(preg_match("/$ext\$/i", $file))
            {$allow = true; break;}
        }
    }
	
	return $allow;
}

/**
	 * Method to process flash uploaded file
	 *
	 * @param string $tempfile tempfile location
	 * @param	array $filename File info
     * @param string $dtable mime table needed for aws call
	 *
	 * @return	string
	 */

public static function processflashfile($tempfile, $filename, $dtable = null)
{
	jimport('joomla.filesystem.file');
    $app = JFactory::getApplication();
	$uploadmsg = false;	
	if ($filename->type == 1)
	{
	    $app->enqueueMessage ( JText::_('LIB_TEWEB_MESSAGE_UPLOAD_FAILED_NOT_UPLOAD_THIS_FOLDER'), 'warning');
        $uploadmsg = JText::_('LIB_TEWEB_MESSAGE_UPLOAD_FAILED_NOT_UPLOAD_THIS_FOLDER');
	}
	elseif ($filename->type == 2)
	{
	if (!$copy = Tewebupload::ftp($tempfile, $filename, 1))
	{
        $app->enqueueMessage ( JText::_('LIB_TEWEB_MESSAGE_FILE_NO_UPLOADED_FTP'), 'warning');
        $uploadmsg = JText::_('LIB_TEWEB_MESSAGE_FILE_NO_UPLOADED_FTP');}
	}
	elseif ($filename->type == 3)
	{
	if (!$copy = Tewebupload::aws($tempfile, $filename, 1, $dtable))
	{
        $app->enqueueMessage ( JText::_('LIB_TEWEB_MESSAGE_FILE_NO_UPLOADED_AWS'), 'warning');
        $uploadmsg = JText::_('LIB_TEWEB_MESSAGE_FILE_NO_UPLOADED_AWS');}
	}

	else {
	if (!Tewebfile::copyfile($tempfile, $filename->path, true))
	{
        $app->enqueueMessage ( JText::_('LIB_TEWEB_MESSAGE_FILE_NO_UPLOADED'), 'warning');
        $uploadmsg = JText::_('LIB_TEWEB_MESSAGE_FILE_NO_UPLOADED');}
	}
	
	return $uploadmsg;
}

/**
	 * Method to process flash uploaded file
	 *
	 * @param array $file tempfile location
	 * @param	array $filename File info
     * @param string $dtable mime table needed for aws call
	 *
	 * @return	string
	 */

public static function processuploadfile($file, $filename, $dtable = null)
{
	jimport('joomla.filesystem.file');
    $app = JFactory::getApplication();
	$uploadmsg = false;	
	if ($filename->type == 1)
	{
        $app->enqueueMessage ( JText::_('LIB_TEWEB_MESSAGE_UPLOAD_FAILED_NOT_UPLOAD_THIS_FOLDER'), 'warning');
        $uploadmsg = true;
	}
	elseif ($filename->type == 2)
	{
	$temp_folder = Tewebupload::gettempfolder();
	$tempfile = $temp_folder.$file['name'];	
	$uploadmsg = Tewebupload::uploadftp($tempfile, $file);
	if (!$uploadmsg)
		{
			if (!$copy = Tewebupload::ftp($tempfile, $filename, 1))
			{
                $app->enqueueMessage ( JText::_('LIB_TEWEB_MESSAGE_FILE_NO_UPLOADED_FTP'), 'warning');
                $uploadmsg = true;
            }

	JFile::delete($tempfile);	
		}
	}
	elseif ($filename->type == 3)
	{
	$temp_folder = Tewebupload::gettempfolder();
	$tempfile = $temp_folder.$file['name'];	
	$uploadmsg = Tewebupload::uploadftp($tempfile, $file);
	if (!$uploadmsg)
		{
			if (!$copy = Tewebupload::aws($tempfile, $filename, 1, $dtable))
			{ 
                $app->enqueueMessage ( JText::_('LIB_TEWEB_MESSAGE_FILE_NO_UPLOADED_AWS'), 'warning');
                $uploadmsg = true;
            }

		JFile::delete($tempfile);
		}	
	}
	else
	{
		$uploadmsg = Tewebupload::upload($filename, $file);
	}
	
	return $uploadmsg;
}

/**
	 * Method to upload the file
	 *
	 * @param	array $file  Source File details.
	 * @param	array $filename Destination file details.
	 *
	 * @return	string
	 */

public static function upload($filename, $file)
{$msg = false;
$app = JFactory::getApplication();
jimport('joomla.filesystem.file');
if (!JFILE::upload($file['tmp_name'], $filename->path))
{ $app->enqueueMessage ( JText::_('LIB_TEWEB_MESSAGE_UPLOAD_FAILED_CHECK_PATH').' '. $filename->path .' '.JText::_('LIB_TEWEB_MESSAGE_UPLOAD_EXISTS'), 'warning');
$msg = true;}
return $msg;
}

/**
	 * Method to upload the file for ftp upload
	 *
	 * @param	array $file  Source File details.
	 * @param	array $filename Destination file details.
	 *
	 * @return	string
	 */

public static function uploadftp($filename, $file)
{$msg = false;
jimport('joomla.filesystem.file');
$app = JFactory::getApplication();
if (!JFILE::upload($file['tmp_name'], $filename))
{ $app->enqueueMessage ( JText::_('LIB_TEWEB_MESSAGE_UPLOAD_FAILED_CHECK_PATH').' '. $filename->path .' '.JText::_('LIB_TEWEB_MESSAGE_UPLOAD_EXISTS'), 'warning');
$msg = true;}
return $msg;
}

/**
	 * Method to upload the file over ftp
	 *
	 * @param	array $file  Source File details.
	 * @param	array $filename Destination file details.
	 * @param	bolean $admin Sets whether call is from Joomla admin or site.
	 *
	 * @return	bolean
	 */

public static function ftp($file, $filename, $admin = 0)
{
$app = JFactory::getApplication();
$ftpsuccess = true;
$ftpsuccess1 = true;
$ftpsuccess2 = true;
$ftpsuccess3 = true;
$ftpsuccess4 = true;
	// FTP access parameters
$host = $filename->ftphost;
$usr = $filename->ftpuser;
$pwd = $filename->ftppassword;
$port = $filename->ftpport;
 
// file to move:
$local_file = $file;
$ftp_path = $filename->path;
// connect to FTP server (port 21)
if (!$conn_id = ftp_connect($host, $port))
{
 if ($admin == 0)
 {
 $app->enqueueMessage ( JText::_('LIB_TEWEB_MESSAGES_FTP_NO_CONNECT'), 'error' );}
 $ftpsuccess1 = false;
}


// send access parameters
if (!ftp_login($conn_id, $usr, $pwd))
{
 if ($admin == 0)
 {
 $app->enqueueMessage ( JText::_('LIB_TEWEB_MESSAGES_FTP_NO_LOGIN'), 'error' );}
 $ftpsuccess2 = false;
}


 
// turn on passive mode transfers (some servers need this)
// ftp_pasv ($conn_id, true);
 
// perform file upload
if (!$upload = ftp_put($conn_id, $ftp_path, $local_file, FTP_BINARY))
{
 if ($admin == 0)
 {
 $app->enqueueMessage ( JText::_('LIB_TEWEB_MESSAGES_FTP_NO_UPLOAD'), 'error' );}
 $ftpsuccess3 = false;
}


 
/*
** Chmod the file (just as example)
*/
 
// If you are using PHP4 then you need to use this code:
// (because the "ftp_chmod" command is just available in PHP5+)
if (!function_exists('ftp_chmod')) {
   function ftp_chmod($ftp_stream, $mode, $filename){
        return ftp_site($ftp_stream, sprintf('CHMOD %o %s', $mode, $filename));
   }
}
 
// try to chmod the new file to 666 (writeable)
if (ftp_chmod($conn_id, 0755, $ftp_path) == false)
{
	if ($admin == 0)
 	{
    $app->enqueueMessage ( JText::_('LIB_TEWEB_MESSAGES_FTP_NO_CHMOD'), 'error' );}
 	$ftpsuccess4 = false;
}
 
// close the FTP stream
ftp_close($conn_id);

if (!$ftpsuccess1 || !$ftpsuccess2 || !$ftpsuccess3)
{$ftpsuccess = false;}

return $ftpsuccess;
}

/**
	 * Method to upload the file over AWS
	 *
	 * @param	array $file  Source File details.
	 * @param	array $filename Destination file details.
	 * @param   string $dtable table name to look up mime
	 * @return	bolean
	 */


public static function aws($file, $filename, $admin = 0, $dtable)
{
$app = JFactory::getApplication();
jimport('teweb.file.S3');
//AWS access info   
$awsAccessKey = $filename->aws_key;
$awsSecretKey = $filename->aws_secret;
$awsbucket = $filename->aws_bucket;
$awsfile = $filename->file;
$source_file = $file;

//instantiate the class
$s3 = new S3($awsAccessKey, $awsSecretKey);
if (!$s3->getBucket($awsbucket))
{
    $s3->putBucket($awsbucket, S3::ACL_PUBLIC_READ);
} 
//move the file  
if ($s3->putObjectFile($source_file, $awsbucket, $awsfile, S3::ACL_PUBLIC_READ) === false) {  
    $app->enqueueMessage ( JText::_('LIB_TEWEB_MESSAGES_AWS_CANNOT_CREATE_FILE'), 'error' ); 
    $awssuccess = false;
} else{  
   $awssuccess = true;
}  

return $awssuccess;

}

/**
     * Method to upload a file through the swf upload client
     *
     * @return    exit
     */

public static function flashupload()
{
//import joomla filesystem public static functions, we will do all the filewriting with joomlas public static functions,
//so if the ftp layer is on, joomla will write with that, not the apache user, which might
//not have the correct permissions
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');
$abspath    = JPATH_SITE;
//this is the name of the field in the html form, filedata is the default name for swfupload
//so we will leave it as that
$fieldName = 'Filedata';
//any errors the server registered on uploading
$fileError = $_FILES[$fieldName]['error'];
if ($fileError > 0) 
{
        switch ($fileError) 
        {
        case 1:
        echo JText::_( 'LIB_TEWEB_MESSAGE_FILE_TOO_LARGE_THAN_PHP_INI_ALLOWS' );
        return;
 
        case 2:
        echo JText::_( 'LIB_TEWEB_MESSAGE_FILE_TO_LARGE_THAN_HTML_FORM_ALLOWS' );
        return;
 
        case 3:
        echo JText::_( 'LIB_TEWEB_MESSAGE_ERROR_PARTIAL_UPLOAD' );
        return;
 
        case 4:
        echo JText::_( 'LIB_TEWEB_MESSAGE_ERROR_NO_FILE' );
        return;
        }
}
 
//check for filesize
$fileSize = $_FILES[$fieldName]['size'];
if($fileSize > 500000000)
{
    echo JText::_( 'LIB_TEWEB_MESSAGE_FILE_BIGGER_THAN').' 500MB';
}

//check the file extension is ok
$fileName = $_FILES[$fieldName]['name'];
$extOk = Tewebfile::checkfile($fileName);
 
if ($extOk == false) 
{
        echo JText::_( 'LIB_TEWEB_MESSAGE_NOT_UPLOAD_THIS_FILE_EXT' );
        return;
}
 
//the name of the file in PHP's temp directory that we are going to move to our folder
$fileTemp = $_FILES[$fieldName]['tmp_name'];
 
//always use constants when making file paths, to avoid the possibilty of remote file inclusion
$uploadPath = Tewebupload::gettempfolder().DIRECTORY_SEPARATOR.$fileName;
 
if(!JFile::upload($fileTemp, $uploadPath)) 
{
        echo JText::_( 'LIB_TEWEB_MESSAGE_ERROR_MOVING_FILE' );
        return;
}
else
{
   // success, exit with code 0 for Mac users, otherwise they receive an IO Error
   exit(0);
}
}

	
}
?>