<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
class ViewTemedialist extends JViewLegacy
{
    /**
     * Indicates if view is allowed
     *
     * @var    boolean
     */
    protected $allowView = true;
    
	function display($tpl = null)
	{
		// Do not allow cache
		JResponse::allowCache(false);

		$app = JFactory::getApplication();
        $user    = JFactory::getUser();
        if (!$this->allowView)
        {Tewebcheck::check403();}
		$lang	= JFactory::getLanguage();
        JHtml::_('behavior.framework', true);
        $host = JURI::root();
		$document = JFactory::getDocument();
		$document->addScriptDeclaration("var temediaManager = window.parent.temediaManager;");
        $document->addStyleSheet($host.'media/lib_teweb/mediamanager/temedialist.css');

        if ($lang->isRTL()) {
            $document->addStyleSheet($host.'media/lib_teweb/mediamanager/temedialist-rtl.css');
        }
        
        // add path to admin tmpl area
        $this->_addPath('template', JPATH_SITE.DIRECTORY_SEPARATOR.'libraries/teweb/file/temedialisttmpl');
        
        $this->media = JRequest::getVar('media', 'image');
        if ($this->media == 'image')
        {
            $images = $this->get('images');
            $folders = $this->get('folders');
            $docs = array();
        }
        else {
            $images = array();
            $folders = $this->get('folders');
            $docs = $this->get('documents');
        }

		$state = $this->get('state');
        $this->baseURL = $this->get('baseURL');
        $this->mediaURL = JURI::ROOT().'media';
		$this->assignRef('images', $images);
		$this->assignRef('folders', $folders);
        $this->assignRef('docs', $docs);
		$this->assignRef('state', $state);

		parent::display($tpl);
	}


	function setFolder($index = 0)
	{
		if (isset($this->folders[$index])) {
			$this->_tmp_folder = &$this->folders[$index];
		} else {
			$this->_tmp_folder = new JObject;
		}
	}

	function setImage($index = 0)
	{
		if (isset($this->images[$index])) {
			$this->_tmp_img = &$this->images[$index];
		} else {
			$this->_tmp_img = new JObject;
		}
	}
    
    function setDoc($index = 0)
    {
        if (isset($this->docs[$index])) {
            $this->_tmp_doc = &$this->docs[$index];
        } else {
            $this->_tmp_doc = new JObject;
        }
    }
}
