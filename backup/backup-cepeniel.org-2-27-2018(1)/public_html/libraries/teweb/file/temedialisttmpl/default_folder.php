<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_media
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;
$user = JFactory::getUser();
$option = JRequest::getCmd('option');
?>
<div class="medialist-row">
<?php if ($user->authorise('core.delete', 'com_admin')):?>
    <div class="medialist-delete">
        <a href="#" onclick="if (confirm('<?php echo JText::_('LIB_TEWEB_MEDIA_CENTER_DELETE_WARNING');?> <?php echo $this->_tmp_folder->path_relative; ?>')){temediaManager.deleteMedia('<?php echo $this->_tmp_folder->path_relative; ?>', 'folder');}" title="<?php echo JText::_('JACTION_DELETE'); ?>" ><?php echo JHtml::_('image', 'media/remove.png', JText::_('JACTION_DELETE'), array('width' => 16, 'height' => 16, 'border' => 0), true); ?></a>
    </div>
 <?php endif;?>
    <div class="medialist-icon">
        <a href="index.php?option=<?php echo $option;?>&amp;view=temedialist&amp;tmpl=component&amp;media=<?php echo $this->media;?>&amp;folder=<?php echo $this->state->folder;?>&amp;relpath=<?php echo $this->_tmp_folder->path_relative; ?>">
                <?php echo JHtml::_('image', 'media/folder.gif', $this->_tmp_folder->name, array('height' => 20, 'width' => 20), true); ?></a>
    </div>
    <div class="medialist-title">
        <a href="index.php?option=<?php echo $option;?>&amp;view=temedialist&amp;tmpl=component&amp;media=<?php echo $this->media;?>&amp;folder=<?php echo $this->state->folder;?>&amp;relpath=<?php echo $this->_tmp_folder->path_relative; ?>"><?php echo $this->_tmp_folder->name; ?></a>
    </div>
</div>
