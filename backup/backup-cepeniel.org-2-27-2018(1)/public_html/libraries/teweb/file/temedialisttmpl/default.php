<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_media
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;
?>
<?php if (count($this->images) > 0 || count($this->folders) > 0 || count($this->docs) > 0) { ?>
<style>
.medialist-delete {
    display: table-cell;
    width: 20px;
}
.medialist-icon {
    display: table-cell;
    width: 20px;
}
.medialist-title {
    display: table-cell;
}
.medialist-row {
    min-height:  20px;
    border-bottom: 1px solid #f6f6f6;
}

</style>
<div class="manager">

		<?php for ($i=0, $n=count($this->folders); $i<$n; $i++) :
			$this->setFolder($i);
			echo $this->loadTemplate('folder');
		endfor; ?>

        <?php for ($i=0, $n=count($this->docs); $i<$n; $i++) :
            $this->setDoc($i);
            echo $this->loadTemplate('docs');
        endfor; ?>
        
		<?php for ($i=0, $n=count($this->images); $i<$n; $i++) :
			$this->setImage($i);
			echo $this->loadTemplate('image');
		endfor; ?>

</div>
<?php } else { ?>
	<div id="media-noimages">
		<p><?php echo JText::_('LIB_TEWEB_NO_FILE_FOUND'); ?></p>
	</div>
<?php } ?>
<script type='text/javascript'>
window.addEvent('domready', function(){
    temediaManager.setRelpath('');
});
</script>
