<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_media
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;
$params = new JRegistry;
$user = JFactory::getUser();
$option = JRequest::getCmd('option');
$dispatcher	= JDispatcher::getInstance();
$dispatcher->trigger('onContentBeforeDisplay', array('com_media.file', &$this->_tmp_img, &$params));
?>
<div class="medialist-row">
<?php if ($user->authorise('core.delete', 'com_admin')):?>
    <div class="medialist-delete">
        <a href="#" onclick="if (confirm('<?php echo JText::_('LIB_TEWEB_MEDIA_CENTER_DELETE_WARNING');?> <?php echo $this->_tmp_img->path_relative; ?>')){temediaManager.deleteMedia('<?php echo addslashes($this->_tmp_img->path_relative); ?>', 'file');}" title="<?php echo JText::_('JACTION_DELETE'); ?>" ><?php echo JHtml::_('image', 'media/remove.png', JText::_('JACTION_DELETE'), array('width' => 16, 'height' => 16, 'border' => 0), true); ?></a>
    </div>
 <?php endif;?>
    <div class="medialist-icon">
        <a href="javascript:temediaManager.populateFields('<?php echo addslashes($this->_tmp_img->path_relative) ?>')" title="<?php echo $this->_tmp_img->name; ?>" >
                <?php echo JHtml::_('image', $this->baseURL.'/'.$this->_tmp_img->path_rel, JText::sprintf('LIB_TEWEB_IMAGE_TITLE', $this->_tmp_img->title, Tewebmedia::parseSize($this->_tmp_img->size)), array('width' => $this->_tmp_img->width_60, 'height' => $this->_tmp_img->height_60)); ?></a>
    </div>
    <div class="medialist-title">
        <a href="javascript:temediaManager.populateFields('<?php echo addslashes($this->_tmp_img->path_relative); ?>')" title="<?php echo $this->_tmp_img->name; ?>" ><?php echo $this->_tmp_img->title; ?></a>
    </div>
</div>
<?php
$dispatcher->trigger('onContentAfterDisplay', array('com_media.file', &$this->_tmp_img, &$params));
?>
