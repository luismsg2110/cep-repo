<?php
/**
 * @Component - Preachit
 * @version 1.0.0 May, 2010
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha, LLC
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 *
 *
 */
// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.modelform');
jimport('joomla.event.dispatcher');

class Modelcomment extends JModelForm
{
	
	
        public function getForm($data = array(), $loadData = true) 
        {
        			jimport('joomla.form.form');
        			$abspath    = JPATH_SITE;
					$models_path = $abspath.DIRECTORY_SEPARATOR.'libraries'.DIRECTORY_SEPARATOR.'teweb'.DIRECTORY_SEPARATOR.'messages';
 
                JForm::addFormPath ($models_path . DIRECTORY_SEPARATOR . 'forms');
                JForm::addFieldPath($models_path . DIRECTORY_SEPARATOR . 'fields');
 
                $form = $this->loadForm('teweb.comments', 'comments', array('control' => 'jform', 'load_data' => $loadData));
                return $form;
        }
        
   
	public function &getData() 
        {
        			
                if (empty($this->data)) 
                {
                    $data = null;
                    $user    = JFactory::getUser();
                    if ($user->id > 0)
                    {
                        $data['full_name'] = $user->username;
                        $data['email'] = $user->email;
                    } 
					$this->data = $data;						
                }
                
                return $this->data;
        }
}