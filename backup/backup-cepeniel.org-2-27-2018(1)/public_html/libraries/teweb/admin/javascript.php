<?php
/**
 * @Library - teweb library
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('teweb.checks.standard');
class Tewebadminjs
{

/**
     * Method to get javascript for selector lists to highlight all when all selected
     *
     * $param string $select id for the all/choose select switch
     * $param string $selector id for the list
     * $param string $extra list of extra values to trigger the off sequence as a list separated by commas
     *
     * @return    string
     */
     
public static function selectorjs($select, $selector, $extra = null)
{
if (Tewebcheck::getJversion() < 3.0)
{
$js = "
        window.addEvent('domready', function(){
            var filter0 = $('".$select."0');
            if (!filter0) return;
            filter0.addEvent('click', function(){
                $('".$selector."').setProperty('disabled', 'disabled');
                $$('#".$selector." option').each(function(el) {
                    el.setProperty('selected', 'selected');
                });
            })
            
            if ($('".$select."0').checked) {
                $('".$selector."').setProperty('disabled', 'disabled');
                $$('#".$selector." option').each(function(el) {
                    el.setProperty('selected', 'selected');
                });
            }";

$ids = array();
$ids[0] = 1;
if ($extra != null && $extra != '')
{
    $newids = explode(',', $extra);
    $ids = array_merge($ids, $newids);
}

foreach ($ids AS $id)
{
            
            
    $js .=      "
                $('".$select.$id."').addEvent('click', function(){
                $('".$selector."').removeProperty('disabled');
                $$('#".$selector." option').each(function(el) {
                    el.removeProperty('selected');
                });

            })
            
            if ($('".$select.$id."').checked) {
                $('".$selector."').removeProperty('disabled');
            }";
}
            
$js .=       "});";
}
else {return null;}

return $js;
}

}
