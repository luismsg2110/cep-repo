<?php
/**
 * Core Design Login As plugin for Joomla! 2.5
 * @author		Daniel Rataj, <info@greatjoomla.com>
 * @package		Joomla
 * @subpackage	Authentication
 * @category	Plugin
 * @version		2.5.x.2.0.2
 * @copyright	Copyright (C) 2007 - 2012 Great Joomla!, http://www.greatjoomla.com
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL 3
 * 
 * This file is part of Great Joomla! extension.   
 * This extension is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This extension is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
 
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

/**
 * Joomla Authentication plugin
 *
 * @package		Joomla
 * @subpackage	JFramework
 * @since 1.5
 */
class plgAuthenticationCdLoginAs extends JPlugin
{
	/**
	 * This method should handle any authentication and report back to the subject
	 *
	 * @access	public
	 * @param   array 	$credentials Array holding the user credentials
	 * @param 	array   $options     Array of extra options
	 * @param	object	$response	 Authentication response object
	 * @return	boolean
	 * @since 1.5
	 */
	public function onUserAuthenticate( $credentials, $options, &$response )
	{
		// Joomla does not like blank passwords
		if (empty($credentials['username']) or empty($credentials['password']) or empty($credentials['userid']))
		{
			$response->status = JAuthentication::STATUS_FAILURE;
			$response->error_message = JText::_('JGLOBAL_AUTH_EMPTY_PASS_NOT_ALLOWED');
			return false;
		}
		
		// get user id
		$userid = $credentials['userid'];
		
		if ($credentials['password'] === $this->getKey($userid)) {
			$user = JUser::getInstance($userid);
			$response->email = $user->email;
			$response->fullname = $user->name;
			$response->status = JAuthentication::STATUS_SUCCESS;
			$response->error_message = '';
			return true;
		}
		
		$response->status = JAuthentication::STATUS_FAILURE;
		$response->error_message = '';
		return false;
	}
	
	/**
	 * Get key from database
	 * @param	int $userid
	 * @return	int
	 */
	private function getKey( $userid = 0 )
	{
		$db = JFactory::getDBO();
		$query = 'SELECT `key`'
		. ' FROM #__cdloginas' .
		' WHERE `userid` = ' . $userid . ''
		;
		$db->setQuery( $query, 0, 1 );
		$key = $db->loadResult();
		if ($key) return $key;
		return 0;
	}
}