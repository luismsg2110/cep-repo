<?php
/**
 * @package     ContentBuilder
 * @author      Markus Bopp
 * @link        http://www.crosstec.de
 * @license     GNU/GPL
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

class plgContentbuilder_submitEdit_logging extends JPlugin
{
        function __construct( &$subject, $params )
        {
            parent::__construct($subject, $params);
        }
        
        /**
         *
         * @param mixed $record_id The record id, type as required by the type of storage
         * @param stdClass $form the form object
         * @param array $values indices are the element IDs as defined by the type of storage. Group values will be stored as array
         */
        function onBeforeSubmit($record_id, $form, array $values){
            
        }
        
        /**
         *
         * @param mixed $record_id The record id, type as required by the type of storage
         * @param int $article_id the article id, 0 if there isn't any
         * @param stdClass $form the form object
         * @param array $values indices are the element IDs as defined by the type of storage. Group values will be stored as array
         */
        function onAfterSubmit($record_id, $article_id, $form, array $values){
            
            $title = '';
            
            if(JRequest::getVar('option','') == 'com_contentbuilder'){
            
                $db = JFactory::getDBO();
                $db->setQuery('Select title_field From #__contentbuilder_forms Where id = '.JRequest::getInt('id',0).' And published = 1');
                $title_field = $db->loadResult();
                if( isset($values[$title_field]) ){
                    if( is_array( $values[$title_field] ) ) {
                        $title = implode(', ', $values[$title_field]);
                    } else {
                        $title = $values[$title_field];
                    }
                }
            }
            
            jimport('joomla.error.log');
            $options = array(
                'format' => "{DATE}\t{TIME}\t{USER_ID}\t{USER_NAME}\t{USER_FULLNAME}\t{RECORD_ID}\t{ARTICLE_ID}\t{TITLE}\t{FORM_ID}\t{FORM_TITLE}"
            );
            $log = JLog::getInstance('contentbuilder_edit.log.php', $options);
            $user = JFactory::getUser();
            $userId = $user->get('id');
            $userName = $user->get('username');
            $userFullname = $user->get('name');
            $log->addEntry(
                    array(
                        'user_id' => $userId,
                        'user_name' => $userName,
                        'user_fullname' => $userFullname,
                        'record_id' => intval($record_id),
                        'article_id' => intval($article_id),
                        'title' => $title,
                        'form_id' => intval($form->getReferenceId()),
                        'form_title' => $form->getTitle()
                    )
            );
        }
}