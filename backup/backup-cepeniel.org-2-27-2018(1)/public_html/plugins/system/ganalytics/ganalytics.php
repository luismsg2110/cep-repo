<?php
/**
 * @package		GAnalytics
 * @author		Digital Peak http://www.digital-peak.com
 * @copyright	Copyright (C) 2012 Digital Peak, All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL
 */

defined('_JEXEC') or die();

jimport( 'joomla.plugin.plugin');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ganalytics'.DS.'helper'.DS.'ganalytics.php');

class plgSystemGAnalytics extends JPlugin{

	public function onAfterRoute() {
		if(JFactory::getApplication()->isAdmin() && JRequest::getVar('option', null) != 'com_ganalytics'){
			return;
		}

		if($this->params->get('load-jquery', 1) == 0 || JFactory::getDocument()->getType() != 'html'){
			return;
		}

		if($this->params->get('load-jquery', 1) == 2 && JRequest::getVar('option', null) != 'com_ganalytics'){
			return;
		}

		JHTML::_(' behavior.mootools');

		JFactory::getDocument()->addScript("/GAJQYLIB");
		JFactory::getDocument()->addScriptDeclaration("jQuery.noConflict();");

		JFactory::getApplication()->set('jQuery', true);
		JFactory::getApplication()->set('jquery', true);
	}

	public function onAfterRender() {
		if(JFactory::getApplication()->isAdmin() && JRequest::getVar('option', null) != 'com_ganalytics'){
			return;
		}

		if(JFactory::getDocument()->getType() != 'html'){
			return;
		}

		if(!JFactory::getApplication()->isAdmin() && strpos($_SERVER["PHP_SELF"], "index.php") !== false && $this->params->get('load-tr-code', 1) == 1){
			$model = JModel::getInstance('Profiles', 'GAnalyticsModel');
			$model->setState('ids', $this->params->get('accountids', null));
			$results = $model->getItems();
			if(!empty($results)){
				foreach ($results as $result) {
					$javascript = "<script type=\"text/javascript\">
										  var _gaq = _gaq || [];
										  _gaq.push(['_setAccount', '".$result->webPropertyId."']);
											_gaq.push(['_trackPageview']);
										  (function() {
											var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
											ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
											var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
										  })();
										</script>";

					$buffer = JResponse::getBody();
					$buffer = preg_replace ("/<\/head>/", "\n\n".$javascript."\n\n</head>", $buffer);
					JResponse::setBody($buffer);
				}
			}
		}

		if($this->params->get('load-jquery', 1) == 1 || $this->params->get('load-jquery', 1) == 2){
			if($this->params->get('load-jquery', 1) == 2 && JRequest::getVar('option', null) != 'com_ganalytics'){
				return;
			}

			$body = JResponse::getBody();

			$body = preg_replace("#([\\\/a-zA-Z0-9_:\.-]*)jquery([0-9\.-]|min|pack)*?.js#", "", $body);
			$body = str_ireplace('<script src="" type="text/javascript"></script>', "", $body);
			$body = preg_replace("#/GAJQYLIB#", JURI::root().'administrator/components/com_ganalytics/libraries/jquery/jquery.min.js', $body);

			JResponse::setBody($body);
		}
	}
}