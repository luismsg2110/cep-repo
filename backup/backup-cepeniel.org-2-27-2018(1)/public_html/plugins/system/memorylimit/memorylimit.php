<?php
// no direct access
defined('JPATH_BASE') or die;

jimport( 'joomla.plugin.plugin' );

/**
 * Memory Limit system plugin
 */
class plgSystemMemoryLimit extends JPlugin
{
	/**
	 * Object Constructor.
	 *
	 * @access	public
	 * @param	object	The object to observe -- event dispatcher.
	 * @param	object	The configuration object for the plugin.
	 * @return	void
	 * @since	1.0
	 */
	function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);

		// Do some extra initialisation in this constructor if required
		$newMemLimit = intval($this->params->get('maxmemsize', 32))."M";
		$oldValue = ini_set("memory_limit",$newMemLimit);
	}
}
?>
