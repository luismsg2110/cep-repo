<?php
/**
 * @package		Google Site Verification by Websoft Plugins
 * @subpackage	 Site Verify System Plugin
 * @copyright    Copyright (C) 2010 Websoft Agency <office@websoft.me>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.plugin.plugin');

/**
 * GSV Plugin
 *
 * @package		Websoft
 * @subpackage	Site Verify System Plugin
 * @since 		1.6
 */
class plgSystemGSV extends JPlugin {
    
    public function __construct($subject, $params){
        
        parent::__construct($subject, $params);
    
    }
  
	function onAfterInitialise(){

#		$baseurl = JURI::base();
#		$currenturl = JURI::current(); 

#		// No need to display on anthing but the main page
#		if ($baseurl != $currenturl) {
#			return;
#		}


		// get each of the verification keys
		$googlekey  = $this->params->get('googlekey');
		$bingkey    = $this->params->get('bingkey');
		$yahookey   = $this->params->get('yahookey');
		$alexakey   = $this->params->get('alexakey');
		$yandexkey   = $this->params->get('yandexkey');
	
		// get document
		$document =& JFactory::getDocument();

		// set meta tags
		if(!empty($googlekey)){
			$document->setMetaData('google-site-verification', $googlekey);
		}		
		if(!empty($bingkey)){
			$document->setMetaData('msvalidate.01', $bingkey);
		}
		if(!empty($yahookey)){
			$document->setMetaData('y_key', $yahookey);
		}
		if(!empty($alexakey)){
			$document->setMetaData('alexaVerifyID', $alexakey);
		}
		if(!empty($yandexkey)){
			$document->setMetaData('yandex-verification', $yandexkey);
		}
	}
    
}
