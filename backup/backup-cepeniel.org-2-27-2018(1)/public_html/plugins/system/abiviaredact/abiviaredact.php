<?php
/**
 *
 * @package plg_redact
 * @copyright Copyright (C)2009 Abivia Inc. All rights reserved.
 * @license GNU/GPL v2.0, see LICENSE.php
 * @version $Id: $
 * @author Abivia Inc. (http://www.abivia.net).
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.plugin.plugin');

class plgSystemAbiviaredact  extends JPlugin {

    protected $_params;

    function __construct(&$subject, $config) {
        parent::__construct($subject, $config);
        if (version_compare(JVERSION, '1.6.0') < 0) {
            $plugin = JPluginHelper::getPlugin('system', 'redact');
            $this -> _params = new JParameter($plugin -> params);
        } else {
            $this -> _params = $this -> params;
        }
    }

    /**
     * This is a somewhat hacked and scary forward and reverse scanner...
     *
     * @param string The page buffer.
     * @param integer Where to start reverse scanning, should be on the &lt; of
     * the beginning of a tag in inTag is true.
     * @param integer Where to start forward scanning, should follow the &gt; of
     * the end of a tag if inTag is true.
     * @param boolean Tells the scanner if the substring defined by backScan and
     * foreScan is a HTML tag. Use null if this is not known.
     * @return array The result array  will contain "element", the type of HTML
     * element that has been found, and "back" and "fore", the postions of the
     * element start and end in the buffer. If the element has id or
     * class attributes, they will be in "id" and "class" respectively.
     * @throws Exception If the scanner runs out of data.
     */
    protected static function _findTag($buffer, $backScan, $foreScan, $inTag = false) {
        if ($inTag === null) {
            // Figure out if we're starting in a tag ot not
            while ($backScan) {
                if ($buffer[$backScan] == '>') {
                    $inTag = false;
                    break;
                }
                // We should never see "</" so ignore that case.
                if ($buffer[$backScan] == '<') {
                    $inTag = true;
                    $element = self::_getElement($buffer, $backScan + 1);
                    break;
                }
                --$backScan;
            }
        }
        $bufLen = strlen($buffer);
        if ($inTag) {
            while ($foreScan < $bufLen) {
                if ($buffer[$foreScan] == '>') {
                    $selfClose = $buffer[$foreScan - 1] == '/';
                    ++$foreScan;
                    break;
                }
                ++$foreScan;
            }
            $findEnd = !$selfClose;
        } else {
            /*
             * For the reverse scan, continue until we get an unmatched opening
             * tag.
             */
            if (--$backScan < 0) {
                throw new Exception('plg_redact: Back scan past BOF.');
            }
            $selfClose = false;
            $count = 1;
            while ($count && $backScan >= 0) {
                if ($buffer[$backScan] == '>') {
                    $inTag = true;
                    $selfClose = $buffer[--$backScan] == '/';
                    continue;
                }
                if ($buffer[$backScan] == '<') {
                    $inTag = false;
                    if ($selfClose) {
                        // Self-closed tags get ignored
                        $selfClose = false;
                        --$backScan;
                        continue;
                    }
                    if ($buffer[$backScan + 1] == '/') {
                        ++$count;
                    } elseif (--$count == 0) {
                        $element = self::_getElement($buffer, $backScan + 1);
                        break;
                    }
                }
                --$backScan;
            }
            $findEnd = true;
        }
        if ($findEnd) {
            if ($element == '!--') {
                /*
                 * We are in a comment. Look for the end of the comment.
                 */
                 $foreScan = strpos($buffer, '-->', $foreScan);
                 if ($foreScan === false) {
                    if (empty($matchEnd)) {
                        throw new Exception('plg_redact: Unable to match "' . $element . '".');
                    }
                 }
                 $foreScan += 3;
            } else {
                /*
                 * Look for a closing tag that matches the opening tag. This
                 * means we need to look for nested opening tags. Continue until
                 * this tag is closed.
                 */
                $count = 1;
                while ($count) {
                    preg_match(
                        '#</?' . $element . '(?:\s|>)#', $buffer,
                        $matchEnd, PREG_OFFSET_CAPTURE, $foreScan
                    );
                    if (empty($matchEnd)) {
                        throw new Exception('plg_redact: Unable to match "' . $element . '".');
                    }
                    $foreScan = $matchEnd[0][1] + strlen($matchEnd[0][0]) - 1;
                    if ($matchEnd[0][0][1] == '/') {
                        --$count;
                    } else {
                        ++$count;
                    }
                }
                while ($foreScan < $bufLen) {
                    if ($buffer[$foreScan++] == '>') {
                        break;
                    }
                }
            }
        }
        $tagInfo = array('element' => $element, 'back' => $backScan, 'fore' => $foreScan);
        // Pick up the class and id attributes while we're here
        $tagEnd = strpos($buffer, '>', $backScan);
        $tag = substr($buffer, $backScan + 1, $tagEnd - $backScan);
        preg_match_all('/(id|class)="(.*)"/iU', $tag, $matches, PREG_SET_ORDER);
        foreach ($matches as $hit) {
            $tagInfo[strtolower($hit[1])] = $hit[2];
        }
        preg_match_all('/(id|class)=\'(.*)\'/iU', $tag, $matches, PREG_SET_ORDER);
        foreach ($matches as $hit) {
            $tagInfo[strtolower($hit[1])] = $hit[2];
        }
        return $tagInfo;
    }

    /**
     * Extract the element from a buffer.
     *
     * @param string HTML buffer.
     * @param integer Start position of an element, following the less than.
     * @return string Name of the element, without whitespace.
     */
    protected static function _getElement($buffer, $start) {
        $bufLen = strlen($buffer);
        // Skip leading whitespace
        while ($start < $bufLen) {
            if (strpos(" \t\n\r", $buffer[$start]) === false) {
                break;
            }
            ++$start;
        }
        // Skip to whitespace
        $scan = $start;
        while ($scan < $bufLen) {
            if (strpos(" \t\n\r>/", $buffer[$scan]) !== false) {
                break;
            }
            ++$scan;
        }
        return substr($buffer, $start, $scan - $start);
    }

    /**
     * Compare a tag match expression with a matched tag.
     *
     * @param array A parsed match expression. must contain "wild", a flag
     * indicating if the match is a wildcard or not. If "wild" is false, must
     * contain "element", the element to be matched. Optionally can contain
     * "class" and "id" to further restrict the match.
     * @param array Data on a found tag, as generated by the tag parser {@see
     * _findTag()}. Relevant information is in "element", and optionally "class"
     * and "id".
     * @return array Match results. The "match" entry is a boolean indicating if
     * a match was found or not. Boolean entries for "element", "class", and
     * "id" will be present if they were evaluated.
     */
    protected static function _match($ebits, $tagInfo) {
        $hits = array('match' => false);
        $match = true;
        if (!$ebits['wild']) {
            if ($ebits['element'] != $tagInfo['element']) {
                return $hits;
            }
            $hits['element'] = true;
        }
        if (isset($ebits['class'])) {
            $hits['class'] = false;
            if (isset($tagInfo['class'])) {
                if (!($hits['class'] = $ebits['class'] == $tagInfo['class'])) {
                    return $hits;
                }
            } else {
                $match = false;
            }
        }
        if (isset($ebits['id'])) {
            $hits['id'] = false;
            if (isset($tagInfo['id'])) {
                if (!($hits['id'] = $ebits['id'] == $tagInfo['id'])) {
                    return $hits;
                }
            } else {
                $match = false;
            }
        }
        $hits['match'] = $match;
        return $hits;
    }

    /**
     * Parse an element expression in a match rule.
     *
     * @param string Component of a match rule, can be of the form "element",
     * "element.class", "element#id", "element.class#id", or element#id.class".
     * If multiple classes or ids are specified, the last one is used. The
     * element can be any of the wildcard characters "*", "?", or "+".
     * @return array Element match rules. Contains "wild", a boolean that is
     * set if the expression is a wildcard; "element" the element to match
     * (including wildcard characters), and optionally "class" and "id", the
     * class and id selectors, respectively.
     * @throws Exception If the element expression is not of a known syntax.
     */
    protected static function _parseElement($scanExpr) {
        if (
            !preg_match(
                '/^([a-z][a-z0-9]*|[\+\?\*])([#\.][a-z][a-z0-9_\-]*)?([#\.][a-z][a-z0-9_\-]*)?/i',
                $scanExpr,
                $match
            )
        ) {
            throw new Exception('plg_redact: Bad element expression');
        }
        unset($match[0]);
        $ebits = array('element' => strtolower(array_shift($match)));
        $ebits['wild'] = strpos('?*+', $ebits['element']) !== false;
        foreach ($match as $qualifier) {
            if ($qualifier[0] == '.') {
                $ebits['class'] = substr($qualifier, 1);
            } else {
                $ebits['id'] = substr($qualifier, 1);
            }
        }
        return $ebits;
    }

    /**
     * Parse a rule set into a list of rule objects.
     *
     * @param string A linefeed delimited list of elements and search/replace
     * expressions.
     * @return array Parsed rule entries.
     */
    static protected function _parseRules($rules) {
        /*
         * Parse the rules into something more useful.
         */
        $ruleSet = explode(chr(10), $rules);
        foreach ($ruleSet as $key => &$rule) {
            $rule = trim($rule);
            if (empty($rule)) {
                unset($ruleSet[$key]);
                continue;
            }
            if (($posn = strpos($rule, ',')) === false) {
                $rule = array('regex' => $rule, 'elements' => null);
            } else {
                if ($posn > 0) {
                    $elements = explode(' ', substr($rule, 0, $posn));
                } else {
                    $elements = null;
                }
                $rule = array(
                    'regex' => substr($rule, $posn + 1),
                    'elements' => $elements,
                );
            }
            $rule['regex'] = trim($rule['regex']);
            // Check for a replacement in the regex
            $reDelim = $rule['regex'][0];
            $reParts = explode($reDelim, $rule['regex']);
            $flags = ($reDelim == substr($rule['regex'], -1, 1)) ? 0 : 1;
            if (count($reParts) >= 4) {
                $rule['replace'] = $reParts[2];
                array_splice($reParts, 2, 1);
                $rule['regex'] = implode($reDelim, $reParts);
            } else {
                $rule['replace'] = '';
            }
        }
        return $ruleSet;
    }

    /**
     * Apply a set of match rules to a HTML buffer.
     *
     * @param array match rules. Each entry in the array should contain
     * "elements", an array of element match expressions, and "regex" the match
     * string to search for.
     * @param string The HTML buffer.
     * @return string The modified HTML buffer.
     */
    static function applyRules($ruleSet, $buffer) {
        foreach ($ruleSet as $rule) {
            /*
             * Fairly ugly code to test the regex before using it
             */
            if ($rule['regex'] != '') {
                $before = error_get_last();
                @preg_match($rule['regex'], '', $match);
                if ($before != error_get_last()) {
                    continue;
                }
            } else {
                // Unable to handle empty rules just yet...
            }
            if (empty($rule['elements'])) {
                // Simple case where we just kill anything that matches the regex.
                $buffer = preg_replace($rule['regex'], $rule['replace'], $buffer);
            } else {
                $offset = 0;
                while (
                    @preg_match($rule['regex'], $buffer, $match, PREG_OFFSET_CAPTURE, $offset)
                ) {
                    $backScan = $match[0][1] - 1;
                    $foreScan = $match[0][1] + strlen($match[0][0]);
                    $elements = $rule['elements'];
                    $inTagStatus = null;
                    $elementMatch = true;
                    $scanExpr = null;
                    $wild = null;
                    while (!empty($elements) || $scanExpr !== null) {
                        try {
                            /*
                             * The scan expression can be element#id, element.class,
                             * and the element can be * or ?.
                             */
                            if ($wild === null) {
                                while (!empty($elements)) {
                                    $scanExpr = array_pop($elements);
                                    // Parse the element expression
                                    $ebits = self::_parseElement($scanExpr);
                                    if ($ebits['wild']) {
                                        $wild = $ebits;
                                        $scanExpr = null;
                                    } else {
                                        break;
                                    }
                                }
                            }
                        } catch (Exception $e) {
                            if (strpos($e -> getMessage(), 'plg_redact: ') === false) {
                                throw $e;
                            }
                            // Malformed expression, just exit.
                            break;
                        }
                        if ($scanExpr === null) {
                            // Malformed expression, just exit.
                            break;
                        }
                        try {
                            $tagInfo = self::_findTag($buffer, $backScan, $foreScan, $inTagStatus);
                        } catch (Exception $e) {
                            if (strpos($e -> getMessage(), 'plg_redact: ') === false) {
                                throw $e;
                            }
                            // Malformed expression, just exit.
                            break;
                        }
                        $backScan = $tagInfo['back'];
                        $foreScan = $tagInfo['fore'];
                        $inTagStatus = false;
                        /*
                         * Now that we've extracted the next tag, see if we have
                         * something to do with it.
                         */
                        $compare = self::_match($ebits, $tagInfo);
                        if ($compare['match']) {
                            if ($wild !== null && $wild['element'] == '+') {
                                // We had to match at least one of these but didn't.
                                $elementMatch = false;
                                break;
                            }
                            $scanExpr = null;
                            $wild = null;
                        } elseif ($wild !== null) {
                            $compare = self::_match($wild, $tagInfo);
                            if ($compare['match']) {
                                // See if there's anything we need to do with the wildcard.
                                switch ($wild['element']) {
                                    case '+': {
                                        $wild['element'] = '*';
                                    }
                                    break;

                                    case '?': {
                                        $wild = null;
                                    }
                                    break;

                                }
                            } else {
                                // Wildcard was qualified and match failed.
                                $elementMatch = false;
                                break;
                            }
                        } else {
                            // Failed to match the expression
                            $elementMatch = false;
                            break;
                        }
                    }
                    if ($elementMatch) {
                        // We have something to kill!
                        $buffer = substr($buffer, 0, $tagInfo['back'])
                            . $rule['replace']
                            . substr($buffer, $tagInfo['fore']);
                        $offset = $tagInfo['back'] + 1;
                    } else {
                        $offset = $match[0][1] + strlen($match[0][0]) - 1;
                    }
                }
            }
        }
        return $buffer;
    }

    /**
     * Apply match rules to a rendered page.
     */
    function onAfterRender() {
        $buffer = JResponse::getBody();
        // Rules that apply to both front and back end
        $rules = $this -> params -> get('rulesBoth', '');
        $ruleSet = self::_parseRules($rules);
        $buffer = self::applyRules($ruleSet, $buffer);
        $app = &JFactory::getApplication();
        if ($app -> isAdmin()) {
            $rules = $this -> params -> get('rulesBack', '');
            $ruleSet = self::_parseRules($rules);
            $buffer = self::applyRules($ruleSet, $buffer);
        } else {
            $rules = $this -> params -> get('rules', '');
            $ruleSet = self::_parseRules($rules);
            $buffer = self::applyRules($ruleSet, $buffer);
        }
        JResponse::setBody($buffer);
        return true;
    }

}

/**
 * Backward compatible class for J1.5
 */
class plgSystemRedact extends plgSystemAbiviaredact {
}
