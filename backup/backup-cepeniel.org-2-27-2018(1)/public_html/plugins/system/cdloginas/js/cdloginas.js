/**
 * Core Design Login As plugin for Joomla! 2.5
 * @author		Daniel Rataj, <info@greatjoomla.com>
 * @package		Joomla
 * @subpackage	Authentication
 * @category	Plugin
 * @version		2.5.x.2.0.2
 * @copyright	Copyright (C) 2007 - 2012 Great Joomla!, http://www.greatjoomla.com
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL 3
 * 
 * This file is part of Great Joomla! extension.   
 * This extension is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This extension is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

(function($) {
	$.cdloginas = {
		
		// Init application
		initiator: function() {
			$.cdloginas.toolbarIcon(); // toolbar icon
		},
		
		toolbarIcon: function() {
			var toolbar = $('#toolbar');
			
			var first_toolbar = toolbar.find('.button').eq(0);
			
			var template_toolbar_divider = '<li class="divider"></li>';
			
			var template_toolbar_icon = 
				'<li id="toolbar-cdloginas" class="button">' +
				'<a class="toolbar" href="javascript: void(0);" onclick="jQuery(document).ready( function($) { $.cdloginas.login(); });">' +
				'<span title="' + $.cdloginas.language.PLG_SYSTEM_CDLOGINAS_TOOLBAR_TITLE + '" class="icon-32-cdloginas">' +
				'</span>' + $.cdloginas.language.PLG_SYSTEM_CDLOGINAS_TOOLBAR_TITLE +
				'</a>' +
				'</li>';
			first_toolbar.before(template_toolbar_icon);
			first_toolbar.before(template_toolbar_divider);
		},
		
		// Disable toolbar
		disableToolbar: function() {
			$('#toolbar-cdloginas').find('span').css({
				'background-position' : '0 32px'
			});
		},
		
		enableToolbar: function() {
			$('#toolbar-cdloginas').find('span').css({
				'background-position' : '0 0'
			});
		},
		
		// Login routine
		login: function() {
			var ids = $.cdloginas.getcids();
			
			// no user selected
			if (ids.length == 0) {
				alert($.cdloginas.language.PLG_SYSTEM_CDLOGINAS_ERROR_SELECT_USER);
				return false;
			}
			
			// more then 1 user
			if (ids.length > 1) {
				alert($.cdloginas.language.PLG_SYSTEM_CDLOGINAS_ERROR_SELECT_ONE_USER);
				return false;
			}
			
			var adminForm = this.adminForm();
			
			if($("input[name='cdloginas_task']", adminForm).length === 0) {
				adminForm.append($('<input />', {
					type : 'hidden',
					name : 'cdloginas_task',
					value : 'task_createLogin'
				}));
			} else {
				$("input[name='cdloginas_task']", adminForm).val('task_createLogin');
			}
			
			
			// create a temporary login
			$.ajax({
				type: 'POST',
				data: adminForm.serialize(),
				beforeSend: function() {
					$.cdloginas.disableToolbar();
				},
				success: function(msg) {
					var key = msg;
					if (key) {
					// key length = 30 characters
					 if (key.length === 30) {
						 $.cdloginas.loginUser(ids[0], key);
						 return true;
					 } else {
						 alert($.cdloginas.language.PLG_SYSTEM_CDLOGINAS_ERROR_UNKNOWN);
						 return false;
					 }
					} else {
						alert($.cdloginas.language.PLG_SYSTEM_CDLOGINAS_ERROR_UNKNOWN);
						return false;
					}
				},
				error: function() {
					alert($.cdloginas.language.PLG_SYSTEM_CDLOGINAS_ERROR_UNKNOWN);
					$.cdloginas.enableToolbar();
					return false;
				},
				complete: function() {
					$.cdloginas.enableToolbar();
				}
			});
			return true;
		},
		
		// Login user on frontend
		loginUser: function(id, key) {
			
			var adminForm =  this.adminForm();
			$("input[name='cdloginas_task']", adminForm).val('task_loginUser');
			
			if($("input[name='cdloginas_key']", adminForm).length === 0) {
				adminForm.append($('<input />', {
					type : 'hidden',
					name : 'cdloginas_key',
					value : key
				}));
			} else {
				$("input[name='cdloginas_key']", adminForm).val(key);
			}
			
			$.ajax({
				type: 'POST',
				url: $.cdloginas.option.livepath,
				data: adminForm.serialize(),
				success: function(msg) {
					if (msg == '1') {
						$.cdloginas.uncheckInput(id);
						var open_new =  confirm($.cdloginas.language.PLG_SYSTEM_CDLOGINAS_SUCCESS_PROMPT);
						if (open_new) {
							window.open($.cdloginas.option.livepath);
							return true;
						} else {
							return false;
						}
					} else {
						// error
						alert(msg);
						return false;
					}
				},
				error: function() {
					alert($.cdloginas.language.PLG_SYSTEM_CDLOGINAS_ERROR_UNKNOWN);
					$.cdloginas.enableToolbar();
					return false;
				},
				complete: function() {
					$.cdloginas.enableToolbar();
				}
			});
			return true;
		},
		
		// Uncheck all checked inputs
		uncheckInput: function(cid) {
			if (cid) {
				$.cdloginas.getTr(cid).children('td:eq(0)').children('input:checked').attr('checked', false);
			}
			return false;
		},
		
		// Return adminForm
		adminForm: function () {
			var form = $('form[name="adminForm"]');
			if (form) return form;
			return false;
		},
		
		// Retrieve all checked inputs from adminForm
		getcids: function() {
			var tr = $.cdloginas.adminFormTR();
			
			var td = $.map(tr.find('td:eq(0)').children('input:checked'), function(element) {
				return $(element).val();
			});
			
			return td;
			
		},
		
		// Return form with name "adminForm"
		adminFormTR: function () {
			var tr = $.cdloginas.adminForm().find("table.adminlist > tbody > tr");
			if (tr) return tr;
			return '';
		},
		
		// return actual table > tr row based on "id" by input
		getTr: function (cid) {			
			var tr = $.cdloginas.adminFormTR().find("input[value='" + cid + "']").parent().parent();
			return tr;
		}
		
	};
})(jQuery);