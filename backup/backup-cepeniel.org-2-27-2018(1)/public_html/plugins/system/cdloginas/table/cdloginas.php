<?php
/**
 * Core Design Login As plugin for Joomla! 2.5
 * @author		Daniel Rataj, <info@greatjoomla.com>
 * @package		Joomla
 * @subpackage	Authentication
 * @category	Plugin
 * @version		2.5.x.2.0.2
 * @copyright	Copyright (C) 2007 - 2012 Great Joomla!, http://www.greatjoomla.com
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL 3
 * 
 * This file is part of Great Joomla! extension.   
 * This extension is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This extension is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// no direct access
defined('_JEXEC') or die;

/**
 * Table
 *
 * @package 		Joomla.Framework
 * @subpackage		Table
 * @since			1.0
 */
class JTableCdLoginAs extends JTable
{
	/** @var int Primary key */
	public $id = null;
	
	/** @public int */
	public $userid = null;
	
	/** @public string */
	public $key = null;	
	
	
	/**
	 * A database connector object
	 * 
	 * @param database A database connector object
	 * @return void
	 */
	public function __construct( &$db ) {
		parent::__construct( '#__cdloginas', 'id', $db );
	}
	
	/**
	 * Create a database
	 * 
	 * @return boolean
	 */
	public function createDB() {
		$query = '
	  		CREATE TABLE IF NOT EXISTS ' . $this->_db->nameQuote( $this->_tbl ) . ' (' .
            '`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,' .
			'`userid` int(11) UNSIGNED NOT NULL DEFAULT 0,' .
			'`key` varchar(30) NOT NULL DEFAULT \'\',' .
			' PRIMARY KEY (`id`)' .
            ') ENGINE=MyISAM;';
		
		$this->_db->setQuery($query);
		
		if (!$this->_db->query()) {
			$this->setError($this->_db->stderr());
			return false;
		}
		return true;
	}
	
	/**
	 * Delete row based on key
	 * 
	 * @return boolean
	 */
	public function deleteRowBasedOnKey() {
		$query = 'DELETE FROM ' . $this->_db->nameQuote( $this->_tbl ) . ' WHERE `key` LIKE ' . $this->_db->quote( $this->get( 'key' ) );
		
		$this->_db->setQuery($query);
		
		if (!$this->_db->query()) {
			$this->setError($this->_db->stderr());
			return false;
		}
		return true;
	}
	
	/**
	 * Return temporary key for user
	 * 
	 * @return int
	 */
	public function getKey()
	{
		$query = 'SELECT ' . $this->_db->nameQuote( 'key' )
		. ' FROM ' . $this->_db->nameQuote( $this->_tbl ) .
		' WHERE ' . $this->_db->nameQuote( 'userid' ) . ' = ' . $userid . ''
		;
		
		$this->_db->setQuery( $query, 0, 1 );
		
		if (!$this->_db->query()) {
			$this->setError($this->_db->stderr());
			return false;
		}
		
		return (int) $db->loadResult();
	}
}
?>