<?php
/**
 * Core Design Login As plugin for Joomla! 2.5
 * @author		Daniel Rataj, <info@greatjoomla.com>
 * @package		Joomla
 * @subpackage	Authentication
 * @category	Plugin
 * @version		2.5.x.2.0.2
 * @copyright	Copyright (C) 2007 - 2012 Great Joomla!, http://www.greatjoomla.com
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL 3
 * 
 * This file is part of Great Joomla! extension.   
 * This extension is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This extension is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// no direct access
defined('_JEXEC') or die;

// Import library dependencies
jimport('joomla.plugin.plugin');

/**
 * Core Design Login As plugin
 *
 * @author		Daniel Rataj <info@greatjoomla.com>
 * @package		Core Design
 * @subpackage	System
 */
class plgSystemCdLoginAs extends JPlugin
{
	
	private static	$livepath	=	'';
	private static	$row		=	null;
	
	/**
	 * Constructor
	 *
	 * @access      protected
	 * @param       object  $subject The object to observe
	 * @param       array   $config  An array that holds the plugin configuration
	 * @since       1.5
	 */
	public function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);

		// define language
		$this->loadLanguage();
		
		self::$livepath = JURI::root(true);
	}
	
	/**
	 * Joomla! onAfterDispatch() function
	 * 
	 * @return void
	 */
	public function onAfterDispatch()
	{
		$application = JFactory::getApplication();
		
		// include table
		JTable::addIncludePath(array(
			dirname(__FILE__) . DS . 'table')
		);
		
		// table instance
		self::$row = @$this->dbInstance();
		
		// site authorization
		if ($application->isSite()) {
			// cdloginas post action
			if (JRequest::getCmd($this->_name . '_task', '', 'post') === 'task_loginUser') {
				$msg = $this->task_loginUser();
				$application->close($msg);
			}
		}
		
		// administration
		if ($application->isAdmin() and JRequest::getCmd('view', '') === 'users') {
			
			$document = JFactory::getDocument(); // set document
			
			$option = JRequest::getCmd( 'option', '', 'GET' );
			
			// only com_users component
			if ( $option !== 'com_users' ) return false;
			
			// do not continue if some task is performed in user manager (add user, edit user...)
			if (JRequest::getCmd( 'task' ) or JRequest::getCmd( 'layout' ) ) return false;
			
			// Scriptegrator check
			if ( !class_exists( 'JScriptegrator' ) ) {
				JError::raiseNotice( '', JText::_( 'PLG_SYSTEM_CDLOGINAS_ERROR_ENABLE_SCRIPTEGRATOR' ) );
				return false;
			} else {
				$JScriptegrator = JScriptegrator::getInstance( '2.5.x.2.1.8' );
				$JScriptegrator->importLibrary( array( 'jquery' ) );
				if ( $error = $JScriptegrator->getError() ) {
					JError::raiseNotice( '', $error );
					return false;
				}
			}
			
			//create table
			if ( ! self::$row->createDB() ) {
				JError::raiseNotice( '', self::$row->getError() );
				return false;
			}
			
			// attach JS file
			$document->addScript( self::$livepath . '/plugins/system/' . $this->_name . '/js/' . $this->_name . '.js' );
			
			// attach CSS style
			$document->addStyleSheet( self::$livepath . '/plugins/system/' . $this->_name . '/css/' . $this->_name . '.css', 'text/css' );
			
			$init_js = "<!--
			jQuery(document).ready(function($){
				if ($.$this->_name) {
					// language
					$.extend( $.$this->_name, { language: {
						PLG_SYSTEM_CDLOGINAS_TOOLBAR_TITLE : '" . JText::_('PLG_SYSTEM_CDLOGINAS_TOOLBAR_TITLE', true) . "',
						PLG_SYSTEM_CDLOGINAS_ERROR_SELECT_USER : '" . JText::_('PLG_SYSTEM_CDLOGINAS_ERROR_SELECT_USER', true) . "',
						PLG_SYSTEM_CDLOGINAS_ERROR_SELECT_ONE_USER : '" . JText::_('PLG_SYSTEM_CDLOGINAS_ERROR_SELECT_ONE_USER', true) . "',
						PLG_SYSTEM_CDLOGINAS_ERROR_UNKNOWN : '" . JText::_('PLG_SYSTEM_CDLOGINAS_ERROR_UNKNOWN', true) . "',
						PLG_SYSTEM_CDLOGINAS_SUCCESS_PROMPT : '" . JText::_('PLG_SYSTEM_CDLOGINAS_SUCCESS_PROMPT', true) . "'
					}});
					// option
					$.extend( $.$this->_name, { option: {
						livepath : '" . self::$livepath . "/'
					}});
					$.$this->_name.initiator();					
				}
			});
			// -->";
			$document->addScriptDeclaration( $init_js );
			
			if ( JRequest::getCmd($this->_name . '_task', '', 'post') === 'task_createLogin' ) {
				if ($this->authorize()) {
					$install_plugin = $this->installAuthPlugin();
					if (is_bool($install_plugin) and $install_plugin === true) {
						$create_login = $this->task_createLogin();
						
						// no boolean, that means success
						if (!is_bool($create_login)) {
							// return created authentication key
							$msg = $create_login;
						} else {
							// failed, return empty string
							$msg = '';
						}
						
						$application->close($msg);
					} else {
						//$application->close($install_plugin);
						$application->close($install_plugin);
					}
				}
			}		
		}
		
	}
	
	/**
	 * User authorization
	 * 
	 * @return boolean		True if user has permission to manage users.
	 */
	private function authorize()
	{
		return JFactory::getUser()->authorise( 'core.manage', 'com_users' );
	}
	
	/**
	 * Create a temporary login
	 * 
	 * @return	mixed		boolean, string		Boolean false if failed, string (key) if success.
	 */
	private function task_createLogin()
	{
		// Check for request forgeries
		if ( !JRequest::checkToken() ) return false;
		
		// get user id
		$cids = JRequest::getVar('cid', array(), 'POST', 'array');
		JArrayHelper::toInteger($cids);
		
		$user = JFactory::getUser($cids[0]);
		
		jimport('joomla.user.helper');
		
		$data = array();
		$data['userid'] = (int) $user->get( 'id', 0 );
		$data['key'] = JUserHelper::genRandomPassword(30);
		
		if ( ! self::$row->bind( $data ) ) {
			return self::$row->getError();
		}
		if ( ! self::$row->store() ) {
			return self::$row->getError();
		}
		
		return self::$row->get( 'key', '' );
	}
	
	/**
	 * Delete temporary login
	 * 
	 * @return boolean		True if temporary login have been deleted.
	 */
	private function deleteLogin($key = '')
	{
		if (!$key) return false;
		
		$data = array();
		$data['key'] = $key;
		
		if ( ! self::$row->bind( $data ) ) {
			return self::$row->getError();
		}
		if ( ! self::$row->deleteRowBasedOnKey() ) {
			return self::$row->getError();
		}
		
		return true;
		
		$db = JFactory::getDBO();
		
		$query = 'DELETE FROM `#__' . $this->_name . '` WHERE `key` LIKE ' . $db->Quote($key) . '';
		
		$db->setQuery($query);
		if ($db->query()) return true;
		
		return false;
	}
	
	/**
	 * Get stored key
	 * 
	 * @return		string
	 */
	private function getKey($userid = 0)
	{
		if (!$userid) return 0;
		
		$data = array();
		$data['userid'] = $userid;
		
		if ( ! self::$row->bind( $data ) ) {
			return self::$row->getError();
		}
		if ( ! $key = (int) self::$row->getKey() ) {
			return self::$row->getError();
		}
		
		return $key;
	}
	
	/**
	 * Login user
	 * 
	 * @return mixed	string|boolean		True if user was successfully logged, string if failed.
	 */
	private function task_loginUser()
	{
		jimport('joomla.utilities.arrayhelper');
		
		$application = JFactory::getApplication();
		
		// get user id
		$cids = JRequest::getVar( 'cid', array(), 'POST', 'array' );
		JArrayHelper::toInteger( $cids );
		
		$user = JFactory::getUser( $cids[0] );
		
		if ( $user->get( 'id', 0 ) ) {
				
			$credentials = array();
			$credentials['username'] = $user->get( 'username' );
			$credentials['password'] = JRequest::getString( $this->_name . '_key', '', 'POST' );
			$credentials['userid'] = $user->get( 'id', 0 );
			
			if ( $application->login($credentials) ) {
				// delete auth key from #__cdloginas table
				if ( ! $this->deleteLogin( $credentials['password'] ) ) {
					return JText::_('PLG_SYSTEM_CDLOGINAS_ERROR_DATABASE');
				}
				
				if ( ! $this->unpublishPlugin() ) {
					return JText::_( 'PLG_SYSTEM_CDLOGINAS_ERROR_UNPUBLISHING_FAILED' );
				}
				
				// everything ok
				return true;
			} else {
				// login failed
				return JText::_( 'PLG_SYSTEM_CDLOGINAS_ERROR_LOGIN_FAILED' );
			}
		}
		
		// something else is wrong
		return JText::_( 'PLG_SYSTEM_CDLOGINAS_ERROR_UNKNOWN' );
	}
	
	/**
	 * Install authentication plugin
	 * 
	 * @return mixed		boolean, string		True, if plugin was installed, message if error.
	 */
	private function installAuthPlugin()
	{
		if ( ! JPluginHelper::isEnabled('authentication', $this->_name) ) {
			// install auth plugin
			
			jimport('joomla.installer.installer');
			jimport('joomla.installer.helper');
			
			$installer = JInstaller::getInstance();
			$package = $this->getPackageFromFolder();
			
			if (!$installer->install($package['dir'])) {
				// installation failed
				return JText::_('PLG_SYSTEM_CDLOGINAS_ERROR_INSTALLATION_FAILED');
			}
			
			// Cleanup the install files
			if (!is_file($package['packagefile'])) {
				$config = JFactory::getConfig();
				$package['packagefile'] = $config->getValue('config.tmp_path').DS.$package['packagefile'];
			}
			
			JInstallerHelper::cleanupInstall($package['packagefile'], $package['extractdir']);
		}
		
		// don't forget to publish plugin automatically
		if ($this->publishPlugin()) {
			return true;
		} else {
			return JText::_('PLG_SYSTEM_CDLOGINAS_ERROR_PUBLISH_PLUGIN_FAILED');
		}
	}
	
	/**
	 * Publish plugin
	 * 
	 * @return boolean
	 */
	private function publishPlugin()
	{
		$plugin_id = $this->getPluginId();
		$plugin = JTable::getInstance('extension');
		if($plugin->publish(array($plugin_id), 1)) return true;
		return false;
	}
	
	/**
	 * Unpublish authentication plugin
	 * 
	 * @return boolean		True, if plugin has been unpublished.
	 */
	private function unpublishPlugin()
	{
		$plugin_id = $this->getPluginId();
		$plugin = JTable::getInstance('extension');
		if($plugin->publish(array($plugin_id), 0)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Install an extension from a directory
	 *
	 * @static
	 * @see /administrator/components/com_installer/models/install.php
	 * 
	 * @return boolean True on success
	 * @since 1.0
	 */
	private function getPackageFromFolder()
	{
		jimport('joomla.installer.helper');
		
		// Get the path to the package to install
		$p_dir = dirname(__FILE__) . DS . 'install' . DS . 'plugins' . DS . 'authentication';
		
		$p_dir = JPath::clean( $p_dir );

		// Did you give us a valid directory?
		if (!is_dir($p_dir)) {
			JError::raiseWarning('SOME_ERROR_CODE', JText::_('Please enter a package directory'));
			return false;
		}

		// Detect the package type
		$type = JInstallerHelper::detectType($p_dir);

		// Did you give us a valid package?
		if (!$type) {
			JError::raiseWarning('SOME_ERROR_CODE', JText::_('Path does not have a valid package'));
			return false;
		}

		$package['packagefile'] = null;
		$package['extractdir'] = null;
		$package['dir'] = $p_dir;
		$package['type'] = $type;

		return $package;
	}
	
	/**
	 * Get Authentication plugin ID from DB
	 * 
	 * @return		int		Plugin ID.
	 */
	private function getPluginId() {
		$db = JFactory::getDBO();
		$query = 'SELECT `extension_id`'
		. ' FROM #__extensions' .
		' WHERE `element` LIKE ' . $db->Quote($this->_name) . ' AND `folder` LIKE ' . $db->Quote('authentication') . ''
		;
		$db->setQuery( $query, 0, 1 );
		$key = (int)$db->loadResult();
		return $key;
	}
	
	/**
	 * Load DB instance
	 * 
	 * @return 		A database object
	 */
	private function dbInstance( $instance = '' )
	{
		$row = JTable::getInstance('CdLoginAs' . $instance);
		return $row;
	}
	
}
?>