<?php
/**
 * @file       sr2win.php
 * @brief      System Plugin that allow Search And Replace any string after Content Rendering.
 * @version    1.6.0
 * @author     Edwin CHERONT     (e.cheront@jms2win.com)
 *             Edwin2Win sprlu   (www.jms2win.com)
 * @copyright  Joomla Multi Sites
 *             Single Joomla! 1.5.x installation using multiple configuration (One for each 'slave' sites).
 *             (C) 2008-2012 Edwin2Win sprlu - all right reserved.
 * @license    This program is free software; you can redistribute it and/or
 *             modify it under the terms of the GNU General Public License
 *             as published by the Free Software Foundation; either version 2
 *             of the License, or (at your option) any later version.
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *             A full text version of the GNU GPL version 2 can be found in the LICENSE.php file.
 * @par History:
 * - V1.0.0 07-MAR-2009: File creation
 * - V1.1.0 25-FEB-2010: Add possibilty to process regular expression.
 *                       Start the expression with a ~ to specify that you want process it as a regular Search/Replace
 * - V1.2.0 30-JUL-2010: Add the possibility to get the list of Search/Replace expressions from file.
 * - V1.3.0 07-MAR-2011: Add Joomla 1.6 compatibility.
 * - V1.4.0 03-JUL-2011: Add processing of keywords {host-1}, ..., {host-5} that are replaced by the value
 *                       of the host domain parameter.
 *                       http://host-5.host-4.host-3.host-2.host-1/xxx
 * - V1.5.0 04-NOV-2011: Add PCRE customization and error handling with
 *                       then contribution of "Konstantin Ignatov" (AKA e-kinst)
 * - V1.6.0 10-MAY-2012: Fix calling _regExProcess() by reference that is now a Fatal Error on PHP 5.4
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.filesystem.file');
jimport( 'joomla.plugin.plugin' );


class plgSystemSR2Win extends JPlugin
{

	var $_db = null;

	/**
	 * Constructor
	 *
	 * For php4 compatability we must not use the __constructor as a constructor for plugins
	 * because func_get_args ( void ) returns a copy of all passed arguments NOT references.
	 * This causes problems with cross-referencing necessary for the observer design pattern.
	 *
	 * @access	protected
	 * @param	object	$subject The object to observe
	 * @param 	array   $config  An array that holds the plugin configuration
	 * @since	1.0
	 */
	function plgSystemSR2Win(& $subject, $config)
	{
		parent :: __construct($subject, $config);
	}

   //------------ onAfterInitialise ---------------
	/**
     * Apply the processing on the environment vairables
     */
	function onAfterInitialise()
	{
      $sr_applyonurl  = $this->params->get("sr_applyonurl", 0) == 1;
      
      if ( !$sr_applyonurl) {
         return true;
      }
      
      return $this->doSearchReplace( true);
	}

   //------------ onAfterRender ---------------
	/**
     * Converting the site URL to fit to the HTTP request
     */
	function onAfterRender()
	{
	   $this->doSearchReplace();
	}

   //------------ onAfterRender ---------------
	/**
     * Converting the site URL to fit to the HTTP request
     */
	function doSearchReplace( $ApplyOnURL=false)
	{
		$app =& JFactory::getApplication();

      $scope  = $this->params->get("sr_scope", 'site');  // By default, the SearchReplace is only active for the front-end 

		// If scope match then OK
		if ( $app->getName() == $scope) {}
		// If always processed then OK
		else if ( $scope == 'always') {}
		// Otherwise, discard the processing
		else {
			return true;
		}


		$buffer = JResponse::getBody();
		
		// ------ Compute the {host-n} keywords ----
		$host_search  = array();
		$host_replace = array_reverse( explode( '.', $_SERVER['HTTP_HOST']));
		if ( !empty($host_replace) && is_array( $host_replace)) {
   		for ( $i=0; $i<count($host_replace); $i++) {
   		   $host_search[$i] = '{host-'.($i+1).'}';
   		}
   	}

      // ------ Read Plugin Parameters ----
      $sr_list  = $this->params->get("sr_list", '');
      $this->_loadFile( $sr_list, 'sr_file1');
      $this->_loadFile( $sr_list, 'sr_file2');

      $lines         = explode( "\n", $sr_list);
   	$searchs       = array();
   	$replaces      = array();
   	$expsearchs    = array();
   	$expreplaces   = array();
      foreach( $lines as $line) {
         $line = trim( $line);
         // Skip empty lines
         if ( empty( $line)) {
            continue;
         }

         // If comment
         if ( substr( $line, 0, 1) == ';') {
         	continue;
         }
         
         // Update the "line" with the "{host-x}" keywords
			if ( !empty( $host_search)) {
   			$line = str_replace( $host_search, $host_replace, $line);
			}
         
         // If regexp
         if ( substr( $line, 0, 1) == '~') {
         	if ( !empty( $searchs)) {
               if ( $ApplyOnURL) {
         			$_SERVER["PHP_SELF"] = str_replace( $searchs, $replaces, $_SERVER["PHP_SELF"]);
         			$_SERVER['SCRIPT_NAME'] = str_replace( $searchs, $replaces, $_SERVER['SCRIPT_NAME']);
               }
               else {
         			$buffer = str_replace( $searchs, $replaces, $buffer);
               }
         	}
         	$searchs  = array();
         	$replaces = array();
         	
            $line       = trim( substr( $line, 1));
            $separator  = substr( $line, 0, 1);
   		   $p0 = strpos( $line, $separator, 1);      // Search for closing separator (the second one)
   		   if ( $p0 === false) {
   		      continue;
   		   }
            // Parse statment search=replace
   		   $p1 = strpos( $line, '=', $p0);
   		   if ( $p1 === false) {
   		      continue;
   		   }
   		   
   		   $s = trim( substr( $line, 0, $p1));
   		   $r = trim( substr( $line, $p1+1));
   		   if ( !empty( $s)) {
   		   	$expsearchs[]  = $s;
   		   	$expreplaces[] = $r;
   		   }
   		   
         }
         else {
         	// If there are regexp SR to apply
         	if ( !empty( $expsearchs)) {
               if ( $ApplyOnURL) {
         			$_SERVER["PHP_SELF"] = preg_replace( $expsearchs, $expreplaces, $_SERVER["PHP_SELF"]);
         			$_SERVER['SCRIPT_NAME'] = preg_replace( $expsearchs, $expreplaces, $_SERVER['SCRIPT_NAME']);
               }
               else {
                  // Call specific regular expression in case or error and add possibility of reporting.
         			if( $this->_regExProcess( $buffer, $expsearchs, $expreplaces) != PREG_NO_ERROR ) {
         			   // In case of error, stop the processing
         			   break;
         			}
               }
            	$expsearchs    = array();
            	$expreplaces   = array();
         	}
         	
            // Parse statment search=replace
   		   $p1 = strpos( $line, '=');
   		   if ( $p1 === false) {
   		      continue;
   		   }
   		   
   		   $s = trim( substr( $line, 0, $p1));
   		   $r = trim( substr( $line, $p1+1));
   		   if ( !empty( $s)) {
   		   	$searchs[]  = $s;
   		   	$replaces[] = $r;
   		   }
         }
      }
      
      if ( $ApplyOnURL) {
      	if ( !empty( $searchs)) {
   			$_SERVER["PHP_SELF"]    = str_replace( $searchs, $replaces, $_SERVER["PHP_SELF"]);
   			$_SERVER['SCRIPT_NAME'] = str_replace( $searchs, $replaces, $_SERVER['SCRIPT_NAME']);
      	}
      	if ( !empty( $expsearchs)) {
   			$_SERVER["PHP_SELF"]    = preg_replace( $expsearchs, $expreplaces, $_SERVER["PHP_SELF"]);
   			$_SERVER['SCRIPT_NAME'] = preg_replace( $expsearchs, $expreplaces, $_SERVER['SCRIPT_NAME']);
      	}
      }
      else {
      	if ( !empty( $searchs)) {
   			$buffer = str_replace( $searchs, $replaces, $buffer);
      	}
      	if ( !empty( $expsearchs)) {
      	   // Call specific regular expression to manage the error and avoid empty page
      		$this->_regExProcess( $buffer, $expsearchs, $expreplaces);
      	}
   
   		JResponse::setBody($buffer);
      }


		return true;
	}

   //------------ _loadFile ---------------
	function _loadFile( &$sr_list, $sr_fileX)
	{
      $sr_file  = $this->params->get( $sr_fileX, '');
      if ( !empty( $sr_file)) {

   	   if ( defined( 'JPATH_ROOT'))  { $jpath_root = JPATH_ROOT; }
   	   else                          { $jpath_root = dirname( dirname( dirname(__FILE__))); }

         $site_id = '';
         if ( defined( 'MULTISITES_ID')) {
            $sr_dot  = $this->params->get("sr_dot", 1);
            if ( $sr_dot == 1) { $site_id = '.'; }
            $site_id .=  MULTISITES_ID;
         }
         
         
         $search_keywords  = array( '{root}', '{site_id}');
         $replace_keywords = array( $jpath_root, $site_id);

			$filename = str_replace( $search_keywords, $replace_keywords, $sr_file);
			if ( JFile::exists( $filename)) {
   			$file_list = JFile::read( $filename);
   			// If there is something in the files
   			if ( !empty( $file_list)) {
   			   // Append the file list to the current list and put a new line between both list
   			   $sr_list .= "\n"
   			            .  $file_list
   			            ;
   			}
			}
      }
	}

   //------------ _regExProcess ---------------
   /**
    * @brief Process the regular expression and in case or error
    */
	function _regExProcess(&$buffer, &$expsearchs, &$expreplaces)
	{
      // Read the "Advanced" parameters (PCRE)
      $sr_err  = $this->params->get("sr_err", ''); // RegEx error behavior

      $sr_re   = $this->params->get("sr_re", '');  // RegEx limits set and check it has 6 or 7 digits
      $arr     = array();
      $sr_re   = ($sr_re && preg_match('#^\s*(\d{6,7})\s*$#', $sr_re, $arr))? $arr[1] : 0;

      # ****** $buffer may be large enough beacause it's whole page HTML
      #				So lets try to have PREG parameters large enough too
      $bytes   = mb_strlen($buffer,'8bit');
      
      // Configure the "backtrack limit"
      $btLimit = ini_get('pcre.backtrack_limit');
      // If the PCRE limit is lower that 2.05 time the number of bytes in the buffer to process
      // and that a new limit is provided with a higher value
      if( $btLimit < 2.05*$bytes  &&  $btLimit < $sr_re ) {
         // Then set the new backtrack_limit with the one provided in parameter
         ini_set('pcre.backtrack_limit', $sr_re);
      }
      else {
         // Otherwise, don't changed anything
         $btLimit = 0;
      }

      // Configure the "recursion limit"
      $rLimit = ini_get('pcre.recursion_limit');
      if( $rLimit < 2.05*$bytes  &&  $rLimit < $sr_re ) {
         // Set a new "recursion limit" with the one provided in paramter
         ini_set('pcre.recursion_limit', $sr_re);
      }
      else {
         // Otherwise, don't changed anything
         $rLimit = 0;
      }
      
      // Process the regular expression
      $bufNew  = preg_replace( $expsearchs, $expreplaces, $buffer);
      $errCode = preg_last_error();
      
      // When OK
      if( $errCode == PREG_NO_ERROR ) {
         // Return the result present in the new buffer
         $buffer = $bufNew;
      }
      // When Error
      else {
         // If there is a special error processing
         if( $sr_err ) {
            // Replace the page by an error page with different level of reporting.
            $chars = mb_strlen( $buffer,'utf8');
      		$buffer = '<html><body>SR plugin: RegEx error: ';
      		// If show detailed error is requested
   	   	if( $sr_err == 2 ) {
               switch( $errCode )
               {
                  case PREG_INTERNAL_ERROR:        { $buffer .= 'PREG_INTERNAL_ERROR'; break; }
                  case PREG_BACKTRACK_LIMIT_ERROR: { $buffer .= 'PREG_BACKTRACK_LIMIT_ERROR'; break; }
                  case PREG_RECURSION_LIMIT_ERROR: { $buffer .= 'PREG_RECURSION_LIMIT_ERROR'; break; }
                  case PREG_BAD_UTF8_ERROR:        { $buffer .= 'PREG_BAD_UTF8_ERROR'; break; }
                  case PREG_BAD_UTF8_OFFSET_ERROR: { $buffer .= 'PREG_BAD_UTF8_OFFSET_ERROR'; break; }
                  default:                         { $buffer .= 'PREG ERROR ' . $errCode; break; }
               }
               
               // Display PCRE detailled informations
               $buffer .= '<br />pcre.backtrack_limit = ' . ini_get('pcre.backtrack_limit');
               $buffer .= '<br />pcre.recursion_limit = ' . ini_get('pcre.recursion_limit');
               $buffer .= "<br />String length = $chars chars ($bytes bytes)<br />";
               $buffer .= '<br />' . implode('<br />', $expsearchs) . '<br />';
               $buffer .= '<br />' . implode('<br />', $expreplaces);
    			}
    			else {
    			   $buffer .= 'PREG ERROR ' . $errCode;
    			}
    	      $buffer .= '</body></html>';
      	}
      }
      $expsearchs    = array();		# this means 'no RegEx pending'
      $expreplaces   = array();
      
      // restore the original value in case where they were changed
      $btLimit and  ini_set('pcre.backtrack_limit', $btLimit);
      $rLimit  and  ini_set('pcre.recursion_limit', $rLimit);
      
      return( $errCode);
	}

} // End class
