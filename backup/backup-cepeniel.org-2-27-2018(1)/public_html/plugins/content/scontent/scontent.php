<?php
/**
 * Scontent plugin for Joomla! 1.7
 * @package    Joomla
 * @subpackage Content Plugin
 * @license    GNU/GPL
 * @author Juan Padial <http://www.shikle.com>
*/
// no direct access
defined('_JEXEC') or die('Restricted access');

// Import library dependencies
jimport('joomla.plugin.plugin');
jimport('joomla.utilities.arrayhelper');
require_once (JPATH_SITE.DS.'components'.DS.'com_content'.DS.'helpers'.DS.'route.php');

class plgContentScontent extends JPlugin
{
	function plgContentScontent(&$subject,$config)
	{
	        $app = JFactory::getApplication();
                if(!$app->isSite()) {
                       return;
                }
		parent::__construct($subject,$config);
		
		// load language
		JPlugin::loadLanguage('plg_content_scontent', JPATH_BASE);
	        $document = JFactory::getDocument();
                $vars = 'var uribase = "'.JURI::base().'";';
                $vars .= 'var fillf = "'.JTEXT::_('PLG_CONTENT_SCONTENT_FILL_THE_FORM').'";';
                $document->addScriptDeclaration($vars);
	}

	function onContentBeforeDisplay($context,&$row,&$params,$page=0) {
		$app = JFactory::getApplication();
                if(!$app->isSite()) {
                       return;
                }
		$check_dbtable = $this->params->get('check_dbtable','yes');
		$drop_dbtable = $this->params->get('drop_dbtable','no');
		/** /CHECKING FOR DATABASE INTEGRITY **/
		if ($drop_dbtable == "yes") {
			$this->s_dropscontentTable();		
		}
				
		if ($check_dbtable == "yes") {
			$this->s_checkDatabase();		
		}

		if (isset($row->id)) {	
			if ($this->params->get('display_content')) {
			  if(isset($row->text)){
	    		   $row->text .= '<br />'.$this->setTemplate($row, $params);
	    		  }else{
	    		   $row->introtext .= '<br />'.$this->setTemplate($row, $params);
	    		  }
			} else {
			  if(isset($row->text)){
	    		   $row->text = $this->setTemplate($row, $params).'<br />'.$row->text;
	    		  }else{
	    		   $row->introtext = $this->setTemplate($row, $params).'<br />'.$row->introtext;
	    		  }
			}	
	        }
	}	
	
	function setTemplate($row, $params, $page=0) {

			//enable blog, article, featured all if required
			$view  = JRequest::getVar( 'view', false );
			$layout = JRequest::getVar( 'layout', false );
			$enable_view[0] = $this->params->get('enable_view');
			
			if ($view == 'featured' || $layout == 'blog') {$layout = 'blog';}
			if ($view == 'article') {$layout = 'article';}
			if ($enable_view[0] !== 'all'){			
				if (!in_array($layout, $enable_view)) return;
			} else {
			   if(in_array($layout, $enable_view)) return;
			}

			//disable categories or article if required			
	
			$disablecategory = explode(',', trim($this->params->get('disablecategory' , '')));
			JArrayHelper::toInteger($disablecategory);
	                if ($row->catid && in_array($row->catid, $disablecategory)) return;
			$disablearticle = explode(',', trim($this->params->get('disablearticle' , '')));
			JArrayHelper::toInteger($disablearticle);
		        (int)$id = $row->id;
			if ($id && in_array($id, $disablearticle)) return;
			
			$template = $this->params->get('tmpl','default');
			$document = JFactory::getDocument(); // set document for next usage

			$document->addStyleSheet(JURI::base().'plugins/content/scontent/assets/tmpl/'.$template.'/'.$template.'.css');

			if($this->params->get('icons','icons_text')=="icons_text") {
			 $bstyle = true;
			} else {
			 $bstyle = false;
			}

		       if($view != "article") {
		         $catslug = $this->getCatslug($id);
		        } else {
		         $catslug = $row->catslug;
		        }
		        $alink = rtrim(JURI::base(),"/").JRoute::_(ContentHelperRoute::getArticleRoute($row->slug, $catslug));
                        $ajax =JURI::base().'plugins/content/scontent/assets/js/scontent.js';
                        $document->addScript($ajax);
			$use_sharingcode = $this->params->get('enable_sharing','yes');
			$show_tags = $this->params->get('enable_tags','yes');
                        $thelength = $this->params->def('thelength','300');
                        $vote_in_blog = $this->params->get('vote_in_blog','yes');
                        $show_quote = $this->params->get('show_quote','yes');
                        $enable_report = $this->params->get('enable_report','yes');
                        $db = JFactory::getDbo();
			$query = 'SELECT *' . ' FROM #__scontent_votes' . ' WHERE content_id = ' . $id;
            		$db->setQuery($query);
		        $votes = $db->loadObjectList();

			if (!$db->query()) {
                		$msg = JText::_('PLG_CONTENT_SCONTENT_PLEASE_ENABLE_CHECK_DB_TABLE_OPTIONS');
                		$total_count = 0;				
			}
			(int)$up_count = 0;
                	(int)$down_count = 0;
                	(int)$total_count = 0;
                	if ($votes) { 
                		foreach ($votes as $v) {
                			(int)$up_count = $up_count + $v->up_count;
                			(int)$down_count = $down_count + $v->down_count;
                		}
                		$total_count = $up_count - $down_count;
                	}
		        $html = ""; 
		        include(JPATH_SITE.DS.'plugins'.DS.'content'.DS.'scontent'.DS.'assets'.DS.'tmpl'.DS.$template.DS.$template.'.php');

           		if($view == 'article') {
           			       $html .= '<div style="float:right;font-size:80%;">Powered by <a href="http://www.shikle.com/scontent.htm">Scontent</a></div>';
           		}
			return $html;		
	}
	
function s_checkDatabase() {
	$db  = JFactory::getDbo();
	$query = " CREATE TABLE IF NOT EXISTS `#__scontent_votes` ( "
  			." `id` int(11) NOT NULL auto_increment, "
  			." `content_id` int(11) NOT NULL, "
  			." `user_id` int(11) NOT NULL default '0', "
  			." `up_count` int(11) NOT NULL default '0', "
  			." `down_count` int(11) NOT NULL default '0', "
  			." `last_ip` varchar(254) NOT NULL, "
  			." `date` int(11) NOT NULL, "
 	 		." PRIMARY KEY  (`id`) "
			." ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ; " ;
	$db->setQuery($query);
	if($db->query()) {
		$query = " CREATE TABLE IF NOT EXISTS `#__scontent_votes_totals` ( "
  			." `content_id` int(11) NOT NULL, "
  			." `total_count` int(11) NOT NULL default '0', "
 	 		." PRIMARY KEY  (`content_id`) "
			." ) ENGINE=MyISAM DEFAULT CHARSET=utf8; " ;
	$db->setQuery($query);
	}
	if($db->query()) {
		$query = " CREATE TABLE IF NOT EXISTS `#__scontent_reports` ( "
		        ." `report_id` int(11) NOT NULL auto_increment, "
  			." `content_id` int(11) NOT NULL, "
  			." `user_id` int(11) NOT NULL default '0', "
  			." `user_ip` varchar(254) NOT NULL default '0', "
  			." `date` int(11) NOT NULL, "
  			." `message` varchar(255) NOT NULL default '0', "
 	 		." PRIMARY KEY  (`report_id`) "
			." ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1; " ;
	$db->setQuery($query);
	}
	if($db->query()) {
		$query = " CREATE TABLE IF NOT EXISTS `#__scontent_captcha` ( "
		        ." `captcha_id` bigint(13) NOT NULL auto_increment, "
  			." `captcha_time` int(10) NOT NULL default '0', "
  			." `ip_address` varchar(16) NOT NULL default '0', "
  			." `word` varchar(20) NOT NULL, "
 	 	        ."  PRIMARY KEY  (`captcha_id`) "
			."  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	$db->setQuery($query);
	}
	return $db->query();
}
	
function s_dropscontentTable() {
	$db  = JFactory::getDbo();
	$query = " DROP TABLE IF EXISTS #__scontent_votes,#__scontent_votes_totals,#__scontent_reports,#__scontent_captcha";
	$db->setQuery($query);	
	return $db->query();
}
function getCatslug($id) {
   $db  = JFactory::getDbo();
   $query = "SELECT CASE WHEN CHAR_LENGTH(b.alias) THEN CONCAT_WS(':', a.catid, b.alias) ELSE a.catid END as catslug FROM #__content AS a LEFT JOIN #__categories AS b ON b.id = a.catid WHERE a.id = $id";
   $db->SetQuery($query);
   return $db->loadResult();
  }
/* cleanText function - From Metagenerator */
function cleanText( $text ) {
		// Remove tags				
		$text = preg_replace( "'<script[^>]*>.*?</script>'si", '', $text );		
		$text = preg_replace( '/<!--.+?-->/', '', $text );
		$text = preg_replace( '/{.+?}/', '', $text );
		//$text = strip_tags( $text );
		$text = preg_replace( '/<a\s+.*?href="([^"]+)"[^>]*>([^<]+)<\/a>/is', '\2 (\1)', $text );
		$text = preg_replace('/<[^>]*>/', ' ', $text);
		
		// Remove any email addresses
		$regex = '/(([_A-Za-z0-9-]+)(\\.[_A-Za-z0-9-]+)*@([A-Za-z0-9-]+)(\\.[A-Za-z0-9-]+)*)/iex';
		$text = preg_replace($regex, '', $text);
		$text = html_entity_decode($text,ENT_QUOTES,'UTF-8');	
		$text = str_replace('"', '\'', $text); //Make sure all quotes play nice with meta.
                $text = str_replace(array("\r\n", "\r", "\n", "\t"), " ", $text); //Change spaces to spaces		
		//convert all separators to a normal space
		$text = preg_replace(array('/[\p{Zl}\p{Zp}\p{Zs}]/u',),' ',$text ); //http://www.fileformat.info/info/unicode/category/index.htm
                // remove any extra spaces
		while (strchr($text,"  ")) {
			$text = str_replace("  ", " ",$text);
		}
		
		// general sentence tidyup
		for ($cnt = 1; $cnt < JString::strlen($text)-1; $cnt++) {
			// add a space after any full stops or comma's for readability
			// added as strip_tags was often leaving no spaces
			if ( ($text{$cnt} == '.') || (($text{$cnt} == ',') && !(is_numeric($text{$cnt+1})))) {
				if ($text{$cnt+1} != ' ') {
					$text = JString::substr_replace($text, ' ', $cnt + 1, 0);
				}
			}
		}
			
		return $text;
 }
}
?>