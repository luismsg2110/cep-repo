<?php
/**
 * Scontent plugin for Joomla! 1.6
 * @package    Joomla
 * @subpackage Content Plugin
 * @license    GNU/GPL
*/

// Set flag that this is a parent file
define('_JEXEC', 1);

// no direct access
defined('_JEXEC') or die('Restricted access');

define( 'DS', DIRECTORY_SEPARATOR );

define('JPATH_BASE', dirname(__FILE__).DS.'..'.DS.'..'.DS.'..'.DS.'..' );

require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );
jimport('joomla.database.database');
jimport('joomla.plugin.helper');
	$mainframe = JFactory::getApplication('site');
	$mainframe->initialise();
	$lang = JFactory::getLanguage();
	$lang->load('plg_content_scontent', JPATH_PLUGINS.DS.'content'.DS.'scontent', $lang->getTag(), false, true);
	$aid = JRequest::getInt('article_id', 0);
	$task = JRequest::getVar('task');
	$user = JFactory::getUser();
	$plugin = JPluginHelper::getPlugin('content', 'scontent');
	$params = new JRegistry($plugin->params);	
	$enable_report = $params->getValue('enable_report','yes');		 	
	$captcha = $params->getValue('show_captcha','yes');
	if($captcha == "yes" || ($captcha == "only_guest" && $user->guest)) {
            $captcha = true;
        } else {
            $captcha = false;
        }
	$_site = $params->getValue('site_domain','');
	$disable_rguests = $params->getValue('disable_rguests','no');
	
	if($task=='form' && $aid>0) {
	 if($disable_rguests=="yes" && $user->guest) {
           echo JText::_('PLG_CONTENT_SCONTENT_ONLY_REGISTERED_FLAG');
          } else {
	    echo createform($captcha,$user,$aid,$_site);
	  }
	 return 0;
	} elseif($task=='form' && $aid<1) {
          echo JText::_('PLG_CONTENT_SCONTENT_NO_ARTICLE');
	}
        if($task=="report" && ($enable_report=="yes" || $enable_report=="yes_art_only") && (int)$aid>0) {
          (int)$auto_unpublish = $params->getValue('auto_unpublish','0');
          jimport('joomla.filter.filterinput');
          $filter = new JFilterInput('', '', 1, 1);
          $comment = $filter->clean(JRequest::getVar('str',''));
          $captchacode = JRequest::getVar('c','');
          $status_code = sendReport($aid,$disable_rguests,$_site,$user,$comment,$captcha,$captchacode,$auto_unpublish);
        } else {
          $jsondata['msg']=JText::_('PLG_CONTENT_SCONTENT_NO_ARTICLE');
          echo json_encode($jsondata);
        }

function sendReport($aid,$disable_rguests,$_site,$user,$comment,$captcha,$captchacode,$auto_unpublish) {
   if($aid>0) {
        //check if user has reported the article before
        $userIP = GetUserIp();
        $db = JFactory::getDbo();
        if($user->guest) {
           $and = " AND user_ip = ".$db->Quote($userIP);
         } else {
          $and = " AND user_id = ".$user->id;
         }
         $query = "SELECT user_id,user_ip".
                  " FROM #__scontent_reports WHERE content_id = ".$aid.$and;
         $db->SetQuery($query);
         $reported = $db->loadObject();
         if($reported) {
           $jsondata['msg'] = JText::_('PLG_CONTENT_SCONTENT_ALREADY_REPORTED');
           $jsondata['c'] = "1";
         } else {
            //check captcha
            if($captcha) {
              $checkarray = array('word' => $captchacode, 'ip' => $userIP);
              $res = checkcaptcha($checkarray);
            }
            if(($captcha && $res) || !$captcha) {
              //update reports database
              $nowdate = gmdate('Ymd');
              $query = "INSERT INTO #__scontent_reports ( content_id, user_id, user_ip, date, message) VALUES (".$aid.",".$user->id.",".$db->Quote( $userIP ).",".$nowdate.",".$db->Quote($comment).")";
              $db->SetQuery($query);
              $db->query() or die( $db->stderr() );
              $unp = "";
              //check autounpublish
              if($auto_unpublish > 0) {
                $query = "SELECT COUNT(report_id) FROM #__scontent_reports WHERE content_id = ".$aid;
                $db->SetQuery($query);
                if($db->loadResult() >= $auto_unpublish) {
                 //unpublish the article
                 $query = "UPDATE #__content SET state = 0 WHERE id = ".$aid;
                 $db->SetQuery($query);
                 $db->query() or die($db->stderr());
                 $unp = "<p>The article has reached the limit of reports and it has been unpublished automatically.</p>";
                 cleanthecache();
                }
              }
              if($db->getErrorNum()>0) {
               $jsondata['msg'] = $db->getErrorMsg();
              } else {
              $query = "SELECT a.title as title,".
                " CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(':', a.id, a.alias) ELSE a.id END as slug,".
                " CASE WHEN CHAR_LENGTH(b.alias) THEN CONCAT_WS(':', a.catid, b.alias) ELSE a.catid END as catslug".
                " FROM #__content AS a LEFT JOIN #__categories AS b ON b.id = a.catid WHERE a.id = $aid";
              $row = $db->SetQuery($query);
              $row = $db->loadObject();
              if($db->getErrorNum()>0) {
               $jsondata['msg'] = $db->getErrorMsg();
              } elseif(is_object($row)) {
               require_once (JPATH_SITE.DS.'components'.DS.'com_content'.DS.'helpers'.DS.'route.php');
               $link = $_site."/".substr(JRoute::_(ContentHelperRoute::getArticleRoute($row->slug, $row->catslug)),strlen(JURI::base(true)) + 1);
               $link = "<a href=\"".$link."\">".$row->title."</a>";
               $mail = JFactory::getMailer();
               $site_config = &JFactory::getConfig();
               $site_email = $site_config->getValue('mailfrom');
               $fromname = $site_config->getValue('fromname');
               $mail->setSender($site_email);
               $mail->addRecipient($site_email);
               $mail->setSubject("A user has reported an article");
               $mail->isHTML(true);
               $mail->setBody("<p>A user has reported the article ".$link.".</p><p>The user said:</p><p>\"".$comment."\"</p>".$unp."<p>Please, visit your site and take the proper action.</p>");
               if($mail->Send())  {
                $jsondata['msg'] = JText::_('PLG_CONTENT_SCONTENT_THANKS_FLAG');
                $jsondata['c'] = "1";
               } else {
                $jsondata['msg'] = JText::_('PLG_CONTENT_SCONTENT_ERROR_FLAG');
            }
         }
        }
       } elseif($captcha && !$res) {
        if($captcha){
          $jsondata['msg'] = JText::_('PLG_CONTENT_SCONTENT_ERROR_CAPTCHA');
         }
       }
      }
    }
 echo json_encode($jsondata);
}
function GetUserIp()
{
 if (!empty($_SERVER['HTTP_CLIENT_IP'])) 
 {
   $ip=$_SERVER['HTTP_CLIENT_IP'];
 }
 elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
 {
  $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
 }
 else
 {
   $ip=$_SERVER['REMOTE_ADDR'];
 }
 return $ip;
}
function createform($captcha,$user,$id,$_site) {
 $report_form = '<p>'.JTEXT::_('PLG_CONTENT_SCONTENT_REPORT_HEAD').'</p>'.
	        '<form class="mstr"><textarea id="mstr'.$id.'" name="mstr'.$id.'" class="textarea"></textarea><br/>';
 if($captcha) {
   $captcha = getCaptcha($_site);
   $report_form .= $captcha."<br/>";
   $report_form .= '<label for="captcha'.$id.'">'.JTEXT::_('PLG_CONTENT_SCONTENT_CAPTCHA_CODE').'</label><br/><input id="captcha'.$id.'" class="inputbox" type="text" value="" name="captcha'.$id.'"/></form>';
 } else {
   $report_form .= "</form>";
 }
 $report_form .='<p><a href="javascript:void(0);" onclick="scontent_report('.$id.');" class="send">'.JTEXT::_('PLG_CONTENT_SCONTENT_SEND').'</a>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="scontent_cancel('.$id.');" class="cancel">'.JTEXT::_('PLG_CONTENT_SCONTENT_CANCEL').'</a></p>';
 return $report_form;
}
function getCaptcha($_site) {
	require_once('captcha_pi'.DS.'captcha_pi.php');
	$vals = array(
		  'word'		 => '',
		  'img_path'	 => JPATH_CACHE.DS,
		  'img_url'	 => $_site.DS.'cache'.DS,
		  'font_path'	 => 'texb.ttf',					
		  'img_width'	 => '100',
		  'img_height' => '30',
		  'bg_color' => '#fff',
	          'border_color'=>'#996666',
		  'text_color' => '#6f6f6f',
		  'grid_color'=>'#ffb6b6',
		  'shadow_color'=>'#fff0f0',
		  'expiration' => 3600
		);

	$captcha = create_captcha($vals);
	$db  = JFactory::getDbo();
	$query = "INSERT INTO #__scontent_captcha (captcha_time,ip_address,word)"
		. "\n VALUES ('{$captcha['time']}','".GetUserIp()."','{$captcha['word']}')";
		
	$db->setQuery($query);
		
	$db->query() or die( $db->stderr() );
		
	return $captcha['image'];
 }
function checkcaptcha($checkarray) {
          // First, delete old captchas
	 $expiration = time()-3600; // Two hour limit
	 $db  = JFactory::getDbo();
	 $db->setQuery("DELETE FROM #__scontent_captcha WHERE captcha_time < ".$expiration);
	 $db->query();
	 // Then see if a captcha exists:
	 $sql = "SELECT COUNT(*) AS count "
		. "\n FROM #__scontent_captcha "
		. "\n WHERE word = '{$checkarray['word']}' AND ip_address = '{$checkarray['ip']}' AND captcha_time > $expiration";
	 $query = $db->setQuery($sql);
		
	 if ($db->loadResult()) {
		return true;
	  } else {
		return false;
	  }
}
function cleanthecache()
{
  //clean the cache
  $cache = JFactory::getCache('com_content');
  $cache->clean();
  $cache = JFactory::getCache('page');
  $cache->clean();
}
?>