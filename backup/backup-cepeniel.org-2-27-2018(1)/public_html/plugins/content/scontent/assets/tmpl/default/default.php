<?php
defined('_JEXEC') or die('Restricted access');

  if($show_tags == "yes" || ($show_tags == "yes_art_only" && $view == "article")) {
    if($row->metakey != '') {
	$tags = explode(',',$row->metakey);
	$Itemid = $this->params->get('search_itemid');
	$taglist = '<img class="tags" src="'.JURI::base().'plugins/content/scontent/assets/tmpl/default/images/tags.png"></img>';
	foreach($tags as $tag) {
	  $taglist .= '<a href="'.JRoute::_('index.php?option=com_search&searchword='.trim($tag).'&ordering=newest&searchphrase=all&Itemid='.$Itemid.'').'">'.$tag.'</a> - ';
	}
	$taglist = JString::rtrim($taglist,' - ');
     }
  }
  $html ='<div class="scontent">';// open container
  if($total_count>=0){
    //vote results
    if($total_count==0){
      $html .= '<div id="totalvotes-'.$id.'" class="scneutral">'.$total_count.'</div>';
    } else {
      $html .= '<div id="totalvotes-'.$id.'" class="scup">+'.$total_count.'</div>';
    }
  } else {
    $html .= '<div id="totalvotes-'.$id.'" class="scdown">'.$total_count.'</div>';
  }
  
  $html .= '<div class="scontent_buttons">';//buttons
  
  if(($vote_in_blog == "yes" && $layout == "blog") || $view == "article") {
    $html .= '<a rel="nofollow,noindex" href="javascript:void(0);" class="voteup_button" onclick="scontent_up('.$id.');"><span class="voteup">'.($bstyle ? JTEXT::_('PLG_CONTENT_SCONTENT_VOTE_UP') : '').'</span></a>';//vote up button
    $html .= '<a rel="nofollow,noindex" href="javascript:void(0);" class="votedown_button" onclick="scontent_down('.$id.');"><span class="votedown">'.($bstyle ? JTEXT::_('PLG_CONTENT_SCONTENT_VOTE_DOWN') : '').'</span></a>';//vote down button
  }
  
  if($use_sharingcode == "yes" || ($use_sharingcode == "yes_art_only" && $view == "article")) {
			 $addthis_account = $this->params->get('addthis_account','');
			 $sharingcode = '<a href="http://addthis.com/bookmark.php?v=250&amp;pubid='.$addthis_account.'" rel="nofollow,noindex" class="addthis_button" addthis:url="'.$alink.'"><span class="addthis">'.($bstyle ? JTEXT::_('PLG_CONTENT_SCONTENT_SHARE') : '').'</span></a>';
			 $addthisjs = 'http://s7.addthis.com/js/250/addthis_widget.js#pubid='.$addthis_account.'';
			 $document -> addScript($addthisjs);
  }
  
  $quote='';
  if($show_quote == "yes" || ($show_quote == "yes_art_only" && $view == "article")) {
    $html .= '<a rel="nofollow,noindex" href="javascript:void(0);" class="quote_button" onclick="scontent_quote('.$id.');"><span class="quote">'.($bstyle ? JTEXT::_('PLG_CONTENT_SCONTENT_QUOTE') : '').'</span></a>';
    if(isset($row->introtext)) {
	$quote = $row->introtext;
    } else {
	$quote = $row->text;
    }
    $quote = $this->cleanText($quote);
    $quote = JString::substr($quote,0,$thelength);
    $quote = JString::substr($quote,0,JString::strrpos($quote,' '));
    $quote = "<p><a title=\"".$row->title."\" href=\"".$alink."\">".$row->title."</a></p><p>".$quote."...<a href=\"".$alink."\" rel=\"nofollow\">".JTEXT::_('PLG_CONTENT_SCONTENT_READ_MORE')."</a></p>";
  }
  
  if($enable_report == "yes" || ($enable_report == "yes_art_only" && $view == "article")) {
	$html .= '<a rel="nofollow,noindex" href="javascript:void(0);" class="flag_button" onclick="scontent_reportform('.$id.');"><span class="flag">'.($bstyle ? JTEXT::_('PLG_CONTENT_SCONTENT_FLAG') : '').'</span></a>';
   }
  
  $html .= "</div>";//end buttons

  if(isset($taglist) && $taglist!='') {
    $html .= "<div class=\"taglist\">".$taglist."</div>";
  }
  
  $html .= '</div>';//close container
  $html .= '<div class="clear"></div>';
  $html .= '<div id="quote-'.$id.'" class="quote"><textarea class="textarea" onfocus="this.select();">'.$quote.'</textarea><p><a href="javascript:void(0);" onclick="scontent_cancel('.$id.');" class="cancel">'.JTEXT::_('PLG_CONTENT_SCONTENT_CLOSE').'</a></p></div>';
  $html .= '<div id="status-'.$id.'" class="status"></div>'.'<div class="clear"></div><div id="report-'.$id.'" class="report"></div>';
?>