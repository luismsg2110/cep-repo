function scontent_up(id){
  scontent_loading(id);
  scontent_ajax(id,"voteup","","");
}
function scontent_down(id){
  scontent_loading(id)
  scontent_ajax(id,"votedown","","");
}
function scontent_quote(id){
 var quote = $("quote-"+id).get("html");
 $("status-"+id).innerHTML = quote;
}
function scontent_reportform(id){
  scontent_loading(id);
  scontent_ajax(id,"form","","")
}
function scontent_cancel(id){
  $("status-"+id).innerHTML = "";
  $("report-"+id).innerHTML = "";
}
function scontent_report(id){
  var mstr = ""+$("mstr"+id).get("value")+"";
  var captcha = "";
  var iscaptcha = false;
  if($("captcha"+id)){
     captcha = ""+$("captcha"+id).get("value")+"";
     iscaptcha = true;
  }
  if(mstr=="" || (iscaptcha && captcha=="")) {
     $("status-"+id).innerHTML = "<span class=\"warning\">"+fillf+"</span>";
  } else {
    scontent_loading(id);
    scontent_ajax(id,"report",mstr,captcha);
  }
}
function scontent_ajax(id,type,mstr,captcha){
     var thedata = "task="+type+"&article_id="+id;
     var thef = "vote.php";
     if(type=="report" && mstr!="") {
      thedata += "&str="+encodeURIComponent(mstr);
      thedata += "&c="+captcha;
      thef = "report.php";
     }
     var datatype = "JSON";
     if(type=="form") {
      var datatype = "HTML";
      thef = "report.php";
     }
     var reqoptions = { 
         noCache: true,
         method: 'get',
         url: uribase+"plugins/content/scontent/assets/"+thef+"",
         onSuccess: function(data){
           voteresults(data,id,type); 
         }
        }
    if(datatype == "JSON"){
      var screquest = new Request.JSON(reqoptions).send(thedata);
    } else {
      var screquest = new Request(reqoptions).send(thedata);
    }
}
function voteresults(data,id,type) {
    if(type!="report" && type!="form") {
       if(data.count>=0) {
        if(data.count==0) {
          $("totalvotes-"+id).set("class", "totalvotes-"+id+" scneutral").set("text",data.count);
         } else {
          $("totalvotes-"+id).set("class", "totalvotes-"+id+" scup").set("text","+"+data.count);
         }
        } else {
          $("totalvotes-"+id).set("class", "totalvotes-"+id+" scdown").set("text",data.count);
        }
     }
     $("status-"+id).innerHTML=data.msg;
     if(data.c=="1"){
       $("report-"+id).innerHTML = "";
     }
     if(type=="form") {
      $("status-"+id).innerHTML = "";
      $("report-"+id).setStyle('display', 'block').innerHTML = data;
     }
}
function scontent_loading(id) {
   $("status-"+id).innerHTML = "<img src=\""+uribase+"plugins/content/scontent/assets/images/loading.gif\"/img>";
}