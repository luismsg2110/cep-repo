<?php
/*------------------------------------------------------------------------
# plg_authorlist - Author List Plugin
# ------------------------------------------------------------------------
# author    Joomla!Vargas
# copyright Copyright (C) 2010 joomla.vargas.co.cr. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://joomla.vargas.co.cr
# Technical Support:  Forum - http://joomla.vargas.co.cr/forum
-------------------------------------------------------------------------*/

defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');

class plgContentAuthorList extends JPlugin
{
	public function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);
		$this->loadLanguage();
	}

	public function onContentBeforeDisplay($context, &$row, &$params, $page=0)
	{
		if ($params->get('show_author')) :
		
			$com_path = JPATH_SITE.'/components/com_authorlist/';
			require_once $com_path.'helpers/route.php';
			
			$aParams = JComponentHelper::getParams('com_authorlist', true);
			
			$authors = array();
			
			$db	= JFactory::getDBO();
			
			$query	= $db->getQuery(true);		
			$query->select('u.name AS name, u.username AS username');
			$query->from('#__users AS u');
			
			$query->select('a.userid AS userid, CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(\':\', a.userid, a.alias) ELSE a.userid END as slug');
			$query->join('LEFT', '#__authorlist AS a ON a.userid = u.id');
			
			$db->setQuery($query);
			$authorlist = $db->loadObjectList();
			
			foreach ($authorlist as $list) :
				$authors[$list->userid]['slug'] = $list->slug;
				$authors[$list->userid]['name']  = $list->name;
				$authors[$list->userid]['username']  = $list->username;
			endforeach;
			
			$go = 1;
			if ( $this->params->get('display') != 0 ) :
				if (!empty($row->contactid) && $this->params->get('display') == 1) :
					$go = 0;
				endif;
			endif;
			if (!empty($row->created_by_alias) && $this->params->get('alias') == 0) :
				$go = 0;
			endif;
			
			if ( $go == 1 ) :
								
				if ( isset($authors[$row->created_by]) ) :
				
					$link = JRoute::_(AuthorListHelperRoute::getAuthorRoute($authors[$row->created_by]['slug']));
					
					$author = $row->author;
					
					if (($row->created_by_alias=='') && ($aParams->get('show_author_name') == 1)) {
						$author = $authors[$row->created_by]['username'];
					}
					$row->created_by_alias = '';
					$row->contactid = '';
					
					$row->author = JHtml::_('link',$link,$author, array('title' => sprintf(JText::_('PLG_CONTENT_AUTHOLIST_TITLE_VIEW_AUTHOR'), $author)));
									
				endif;
			
			endif;
		
		endif;

	}
}
