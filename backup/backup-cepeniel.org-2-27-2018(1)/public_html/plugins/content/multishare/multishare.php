<?php
/**
 * @ version	$Id: multishare.php 2011-02-02  v1.2.5
 * @ package	Multishare
 * @ Copyright (C) 2009 by Juan Padial. All rights reserved.
 * @ license	GNU/GPL
 * @ Website    http://www.shikle.com/
 */

// Check to ensure this file is included in Joomla!
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

class plgContentmultishare extends JPlugin {

	function plgContentmultishare( &$subject, $config )
	{
		parent::__construct( $subject, $config );
	}

	function onContentPrepare($context, &$article, &$params, $page=0)
	{	

		$document	= & JFactory::getDocument();
		$view		= JRequest::getCmd('view');
		
		if ( $view != 'article' || $context!='com_content.article') return;		
			
		$excludeCategoryID   = $this->params->get( 'excludeCategoryID', '' );
		$excludeID           = $this->params->get( 'excludeID',         '' );
		$listexcludeCategory = @explode ( ",", $excludeCategoryID );	
		$listexclude 	   = @explode ( ",", $excludeID );		
		if ( in_array ( $article->id, $listexclude ) || in_array ( $article->catid, $listexcludeCategory ) ) return;
		$shareservice           = $this->params->def( 'shareservice','' );
		$addthis_pub   		= $this->params->def( 'addthis_pub', '' );
		$LogoURL          	= $this->params->def( 'LogoURL','');
		$Language               = $this->params->def( 'Language','en');
		$AddThisBrand           = $this->params->def( 'addthis_brand','');
		$AddThisLogoBackground  = $this->params->def( 'addthis_logo_background_color','');
		$AddThisLogoColor       = $this->params->def( 'addthis_logo_color','');
		$AddThisServicesOrder   = $this->params->def( 'addthis_services_order',  '' );
	      $AddThisBrandColor       = $this->params->def( 'addthis_brand_color','');
		$AddThisBrandBackgroundColor   = $this->params->get( 'addthis_brand_background_color',  '' );
		$URI               = $this->params->def( 'URI',  '' );
		$Buttonimage   = $this->params->def('Buttonimage',  'http://s7.addthis.com/static/btn/lg-share-' . $Language . '.gif');
		$SharethisCode    = $this->params->def( 'SharethisCode',  '' );
		$TellafriendCode    = $this->params->def( 'TellafriendCode',  '' );
		$CustomCode    = $this->params->def( 'CustomCode',  '' );
		$show          = $this->params->def( 'Show',  '' );
		$css           = $this->params->def( 'DivCSS',  '' );
		$html  = "";
		$code ="";
		if ( $shareservice == '1' ){
		$code  ="<div style=\"$css\">";
		$code .="<!-- ADDTHIS BUTTON BEGIN -->
<script type=\"text/javascript\">
addthis_pub             = '$addthis_pub'; 
addthis_language        = '$Language';
addthis_logo            = '$LogoURL';
addthis_logo_background = '$AddThisLogoBackground';
addthis_logo_color      = '$AddThisLogoColor';
addthis_brand           = '$AddThisBrand';
addthis_options         = '$AddThisServicesOrder';
addthis_header_color = '#$AddThisBrandColor';
addthis_header_background = '#$AddThisBrandBackgroundColor';
</script>
<a href=\"$URI\" onmouseover=\"return addthis_open(this, '', '[URL]', '[TITLE]')\" onmouseout=\"addthis_close()\" onclick=\"return addthis_sendto()\"><img src=\"$Buttonimage\" alt=\"Addthis\" border=\"0\"/></span></a><script type=\"text/javascript\" src=\"http://s7.addthis.com/js/200/addthis_widget.js\"></script>
<!-- ADDTHIS BUTTON END -->\n";	
              $code .="</div>";
	 }
	 if ( $shareservice == '2' ){
              $code  ="<div style=\"$css\">";
	      $code .="$SharethisCode\n";
	      $code .="</div>";
	 }
	 if ( $shareservice == '3' ){
              $code  ="<div style=\"$css\">";
	      $code .="$TellafriendCode\n";
	      $code .="</div>";
	 }
	 if ( $shareservice == '4' ){
	      $code  ="<div style=\"$css\">";
	      $code .="$CustomCode\n";
	      $code .="</div>";
	 }
       if ($show == '1'){
            $html = $article->text;
            $html .= $code;
            $article->text = $html;
        }
        if ($show == '2'){
            $html = $code;
            $html .= $article->text;
            $article->text = $html;
        }
        if ($show == '3'){
            $html  = $code;
            $html .= $article->text;
            $html .= $code;
            $article->text = $html;
        }
    }
}
?>