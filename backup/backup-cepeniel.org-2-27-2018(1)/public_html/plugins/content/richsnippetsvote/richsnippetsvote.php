<?php

/**
 * @version		1.1
 * @package		Rich Snippets Vote - Plugin for Joomla!
 * @author		DeConf - http://www.deconf.com
 * @copyright	Copyright (c) 2010 - 2012 DeConf.com
 * @license		GNU/GPL license: http://www.gnu.org/licenses/gpl-2.0.html
 */
 
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.plugin.plugin' );
jimport( 'joomla.html.parameter' );

class plgContentRichSnippetsVote extends JPlugin {

 function plgContentRichSnippetsVote(&$subject, $params) { 
	parent::__construct($subject, $params); 
    
	$this->plugin = &JPluginHelper::getPlugin('content', 'richsnippetsvote');
    
	$this->params = new JParameter($this->plugin->params);
 }

 function is_bot($user_agent)
 {

	if($user_agent == "")
		return 1;

	$bot_strings = Array( "bot",
			"Mediapartners", "Google Web Preview",
			"yahoo",     "spider",
			"archiver",   "curl",
			"python",     "nambu",
			"twitt",     "perl",
			"sphere",     "PEAR",
			"java",     "wordpress",
			"radian",     "crawl",
			"yandex",     "eventbox",
			"monitor",   "mechanize",
			"facebookexternal"
		  );
	foreach($bot_strings as $bot)
	{
		if(strpos($user_agent,$bot) !== false)
			{ return 1; }
	}
	
	return 0;
 }
 
 function onBeforeDisplayContent( &$article, &$params ) {
  global $mainframe; 
	if(( JRequest::getVar( 'view' ) == 'article' ) OR ( JRequest::getVar( 'view' ) == 'item' )){
		if ($this->is_bot($_SERVER['HTTP_USER_AGENT']) OR $this->params->get('showtousers')){
			
			$rating_sum=0;
			$rating_cont=0;
			$db	=& JFactory::getDBO();
			
			$query='SELECT * FROM #__content_rating WHERE content_id='. $article->id;
			$db->setQuery($query);
			$votes=$db->loadObject();

			if (!$votes){
				$query='SELECT * FROM #__content_extravote WHERE content_id='. $article->id;
				$db->setQuery($query);
				$votes=$db->loadObject();
			}

			if (!$votes){
				$query='SELECT * FROM #__k2_rating WHERE itemID='. $article->id;
				$db->setQuery($query);
				$votes=$db->loadObject();
			}

			if($votes) {
				if ($this->params->get( 'displaymode' )){
				
					$revvot_text="votes";
					$revvot_value="votes";
				
				}
				else{
				
					$revvot_text="reviews";
					$revvot_value="count";
			
				}
				$rating_sum = intval($votes->rating_sum);
				$rating_count = intval($votes->rating_count);
				$evaluate = ($rating_count==0) ? "0" : number_format($rating_sum/$rating_count,1);
				$microdata="<div id='richsnippetsvote'>
								<div itemscope itemtype='http://data-vocabulary.org/Review-aggregate'>
									<span itemprop='itemreviewed'>".$article->title."</span>
									<span itemprop='rating' itemscope itemtype='http://data-vocabulary.org/Rating'>
										<span itemprop='average'>". $evaluate."</span> out of 
										<span itemprop='best'>5</span>
									</span>	
									based on 
									<span itemprop='".$revvot_value."'>".$rating_count."</span> ".$revvot_text.".
								</div>
							</div>";
				$article->text=$microdata.$article->text;			
			}
		}
	}
 }
 
}
