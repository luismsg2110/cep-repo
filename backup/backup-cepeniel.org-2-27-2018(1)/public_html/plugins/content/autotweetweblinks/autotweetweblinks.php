<?php

/**
 * @package     Extly.Components
 * @subpackage  autotweetweblinks - Plugin AutoTweetNG Weblinks-Extension
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.database.table');

// Check for component
if (!JComponentHelper::getComponent('com_autotweet', true)->enabled)
{
	JError::raiseWarning('5', 'AutoTweet NG Component is not installed or not enabled. - ' . __FILE__);

	return;
}

include_once JPATH_ROOT . '/administrator/components/com_autotweet/helpers/autotweetbase.php';

/**
 * PlgContentAutotweetWeblinks
 *
 * @package     Extly.Components
 * @subpackage  com_autotweet
 * @since       1.0
 */
class PlgContentAutotweetWeblinks extends plgAutotweetBase
{
	// Typeinfo
	const TYPE_WEBLINK = 2;

	// Plugin params
	protected $categories = '';

	protected $excluded_categories = '';

	protected $post_modified = 0;

	protected $show_catsec = 0;

	protected $show_hash = 0;

	protected $use_text = 0;

	protected $use_text_count = 100;

	protected $static_text = '';

	protected $static_text_pos = 1;

	protected $static_text_source = 0;

	protected $metakey_count = 1;

	// -1 means: nothing special to do
	private $_post_modified_as_new = -1;

	/**
	 * plgContentAutotweetWeblinks
	 *
	 * @param   string  &$subject  Param
	 * @param   object  $params    Param
	 *
	 * @since	1.5
	 */
	public function plgContentAutotweetWeblinks(&$subject, $params)
	{
		parent::__construct($subject, $params);

		JLoader::register('WeblinksHelperRoute', JPATH_ROOT . '/components/com_weblinks/helpers/route.php');

		$pluginParams = $this->pluginParams;

		// Joomla article specific params
		$this->categories = $pluginParams->get('categories', '');
		$this->excluded_categories = $pluginParams->get('excluded_categories', '');
		$this->post_modified = (int) $pluginParams->get('post_modified', 0);
		$this->show_catsec = (int) $pluginParams->get('show_catsec', 0);
		$this->show_hash = (int) $pluginParams->get('show_hash', 0);
		$this->use_text = (int) $pluginParams->get('use_text', 0);
		$this->use_text_count = $pluginParams->get('use_text_count', 100);
		$this->static_text = strip_tags($pluginParams->get('static_text', ''));
		$this->static_text_pos = (int) $pluginParams->get('static_text_pos', 1);
		$this->static_text_source = (int) $pluginParams->get('static_text_source', 0);
		$this->metakey_count = (int) $pluginParams->get('metakey_count', 1);

		// Check type and range, and correct if needed
		$this->use_text_count = $this->getTextcount($this->use_text_count);
	}

	/**
	 * onContentAfterSave
	 *
	 * @param   object  $context  The context of the content passed to the plugin.
	 * @param   object  $article  A JTableContent object
	 * @param   bool    $isNew    If the content is just about to be created
	 *
	 * @return	boolean
	 *
	 * @since	1.5
	 */
	public function onContentAfterSave($context, $article, $isNew)
	{
		/*
		 * request from backend: - com_content.article - com_weblinks.weblink - com_contact.contact - com_banners.banner - com_newsfeeds.newsfeed requests form frontend: com_xxx.form content article
		 */
		// Weblink
		if ((($context == 'com_weblinks.weblink') || ($context == 'com_weblinks.form')) && ($isNew || $this->post_modified) && (1 == $article->state))
		{
			$this->postWeblink($article);
		}

		return true;
	}

	/**
	 * onContentAfterSave
	 *
	 * @param   object  $context  The context of the content passed to the plugin.
	 * @param   array   $pks      A list of primary key ids of the content that has changed state.
	 * @param   int     $value    The value of the state that the content has been changed to.
	 *
	 * @return	boolean
	 *
	 * @since	1.5
	 */
	public function onContentChangeState($context, $pks, $value)
	{
		// Weblink
		if ((($context == 'com_weblinks.weblink') || ($context == 'com_weblinks.form')) && ($value == 1))
		{
			$article = JTable::getInstance('Weblink', 'WeblinksTable');

			foreach ($pks as $id)
			{
				$article->load($id);
				$this->postWeblink($article);
			}
		}

		return true;
	}

	/**
	 * postWeblink
	 *
	 * @param   object  $article  The item object.
	 *
	 * @return	boolean
	 *
	 * @since	1.5
	 */
	protected function postWeblink($article)
	{
		if (empty($article->publish_up) || (!(int) $article->publish_up))
		{
			$publish_up = JFactory::getDate()->toSql();
		}
		else
		{
			$publish_up = $article->publish_up;
		}

		$cats = $this->getContentCategories($article->catid);
		$cat_alias = $cats[2];

		// Use main category for article url
		$cat_slug = $catids[0] . ':' . JFilterOutput::stringURLSafe($cat_alias[0]);
		$id_slug = $article->id . ':' . JFilterOutput::stringURLSafe($article->alias);

		// Create internal url for Joomla weblink
		$url = WeblinksHelperRoute::getWeblinkRoute($id_slug, $cat_slug);

		// Get the first image from the text
		$image_url = $this->getImageFromText($article->description);

		$native_object = json_encode($article);
		$this->postStatusMessage($article->id, $publish_up, $article->title, self::TYPE_WEBLINK, $url, $image_url, $native_object);
	}

	/**
	 * getExtendedData
	 *
	 * @param   string  $id              Param.
	 * @param   string  $typeinfo        Param.
	 * @param   string  &$native_object  Param.
	 *
	 * @return	array
	 *
	 * @since	1.5
	 */
	public function getExtendedData($id, $typeinfo, &$native_object)
	{
		$article = json_decode($native_object);

		// Get category path for article
		$cats = $this->getContentCategories($article->catid);
		$catids = $cats[0];
		$cat_names = $cats[1];

		// Needed for url only
		$cat_alias = $cats[2];

		// Use article title or text as message
		$title = $article->title;
		$article_text = $article->description;
		$text = $this->getMessagetext($this->use_text, $this->use_text_count, $article->title, $article_text);
		$hashtags = '';

		// Use metakey or static text or nothing
		if ((2 == $this->static_text_source) || ((1 == $this->static_text_source) && (empty($article->metakey))))
		{
			$title = $this->addStatictext($this->static_text_pos, $title, $this->static_text);
			$text = $this->addStatictext($this->static_text_pos, $text, $this->static_text);
		}
		elseif (1 == $this->static_text_source)
		{
			$hashtags .= $this->getHashtags($article->metakey, $this->metakey_count);
		}

		// Title
		$catsec_result = $this->addCategories($this->show_catsec, $cat_names, $title, 0);
		$title = $catsec_result['text'];

		// Text
		$catsec_result = $this->addCategories($this->show_catsec, $cat_names, $text, $this->show_hash);
		$text = $catsec_result['text'];

		if ('' != $catsec_result['hashtags'])
		{
			$hashtags .= ' ';
			$hashtags .= $catsec_result['hashtags'];
		}

		$data = array(
						'title' => $title,
						'text' => $text,
						'hashtags' => $hashtags,

						// Already done when msg is inserted in queue
						'url' => '',

						// Already done when msg is inserted in queue
						'image_url' => '',

						'fulltext' => $article_text,
						'catids' => $catids,
						'cat_names' => $cat_names,
						'author' => $this->getArticleAuthor($article),
						'language' => $article->language,
						'access' => $article->access,
						'is_valid' => true
		);

		return $data;
	}
}
