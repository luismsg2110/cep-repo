<?php
/*****************************************************************************************
 Title          Mod_pc_subscribe PrayerCenter prayer chain subscription module for Joomla
 Author         Mike Leeper
 Version        2.5.0
 URL            http:www.mlwebtechnologies.com
 Email          web@mlwebtechnologies.com
 License        This is free software and you may redistribute it under the GPL.
                Mod_pc_subscribe comes with absolutely no warranty. For details, 
                see the license at http:www.gnu.org/licenses/gpl.txt
                YOU ARE NOT REQUIRED TO KEEP COPYRIGHT NOTICES IN
                THE HTML OUTPUT OF THIS SCRIPT. YOU ARE NOT ALLOWED
                TO REMOVE COPYRIGHT NOTICES FROM THE SOURCE CODE.
                Requires the PrayerCenter component v2.5.0 or higher
******************************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once( dirname(__FILE__).DS.'helper.php' );
$return	= mod_pc_subscribeHelper::getReturnURL();
$moduleclasssfx = $params->get('moduleclass_sfx');
require( JModuleHelper::getLayoutPath( 'mod_pc_subscribe' ) );
?>