<?php
/*****************************************************************************************
 Title          Mod_pc_subscribe PrayerCenter prayer chain subscription module for Joomla
 Author         Mike Leeper
 Version        2.5.0
 URL            http:www.mlwebtechnologies.com
 Email          web@mlwebtechnologies.com
 License        This is free software and you may redistribute it under the GPL.
                Mod_pc_subscribe comes with absolutely no warranty. For details, 
                see the license at http:www.gnu.org/licenses/gpl.txt
                YOU ARE NOT REQUIRED TO KEEP COPYRIGHT NOTICES IN
                THE HTML OUTPUT OF THIS SCRIPT. YOU ARE NOT ALLOWED
                TO REMOVE COPYRIGHT NOTICES FROM THE SOURCE CODE.
                Requires the PrayerCenter component v2.5.0 or higher
******************************************************************************************/
defined( '_JEXEC' ) or die( 'Restricted access' );// no direct access
$user = &JFactory::getUser();
$lang =& Jfactory::getLanguage();
$lang->load( 'com_prayercenter', JPATH_SITE);
if(file_exists(JPATH_ROOT.DS."administrator".DS."components".DS."com_prayercenter".DS."config.xml")){
  require_once( JPATH_ROOT.DS."components".DS."com_prayercenter".DS."helpers".DS."pc_includes.php" );
  require_once( JPATH_ROOT.DS."components".DS."com_prayercenter".DS."helpers".DS."pc_class.php" );
  global $pcConfig;
  $prayercentermsub = new prayercenter();
  $pc_rights = $prayercentermsub->intializePCRights();
  $config_captcha = $pcConfig['config_captcha'];
  $config_captcha_bypass = $pcConfig['config_captcha_bypass_4member'];
  $show_subscribe = $pcConfig['config_show_subscribe'];
  $user_subscribe = $pcConfig['config_user_subscribe'];
  $livesite = JURI::base();
  ?><script language="JavaScript" type="text/javascript">
    var enter_email = '<?php echo htmlentities(JText::_('USRLENTEREMAIL'),ENT_COMPAT,'UTF-8');?>';
    var enter_sec_code = '<?php echo htmlentities(JText::_('USRLENTERSECCODE'),ENT_COMPAT,'UTF-8');?>';
    var enter_valid_email = '<?php echo htmlentities(JText::_('USRLINVALIDEMAIL'),ENT_COMPAT,'UTF-8');?>';
    var livesite = '<?php echo $livesite;?>';
  </script>
  <?php
  $document =& JFactory::getDocument();
  $document->addScript('components/com_prayercenter/assets/js/pc.js');
  $js_script = "";
  if(session_id() == "") session_start();
  if ($show_subscribe == 1 && $pc_rights->get( 'pc.subscribe' ) == 1){
    echo '<div class="moduletable'.$moduleclasssfx.'">';
    echo '<a name="pcmsub"></a>';
    echo '<form method="post" action="index.php?option=com_prayercenter&modtype=return_subscribmsg&mod=pcmsub" name="msub">';
    echo '<label for="newsubscribe">'.htmlentities(JText::_('USRLEMAIL'),ENT_COMPAT,'UTF-8').': </label><br />';
    echo '<input type="text" name="newsubscribe" id="newsubscribe" class="inputbox" value="'.$user->email.'" /><br />';
    echo '<br /><input type="radio" class="radio" name="subscribe" value="subscribesubmit" onClick="javascript:document.msub.task.value=this.value;" checked="checked" /><label for="unsubscribe">'.htmlentities(JText::_('USRLSUBSCRIBE')).'</label><br />';
    echo '<input type="radio" class="radio" name="subscribe" value="unsubscribesubmit" onClick="javascript:document.msub.task.value=this.value;" /><label for="unsubscribe">'.htmlentities(JText::_('USRLUNSUBSCRIBE')).'</label>';
    if((!$config_captcha_bypass && $config_captcha) || ($config_captcha_bypass && $user->get('id') == 0 && $config_captcha)){
      $js_script = 'return validateSub('.$config_captcha.', livesite, this.form, \'pcmsub\');';
      echo $prayercentermsub->PCgetCaptchaImg('pcmsub','msub');
    } else {
      $js_script = 'return validateSub(0, livesite, this.form, \'pcmsub\');';
      echo '<br /><br />';
    }
    echo '&nbsp;<button type="button" onclick="javascript:'.$js_script.'">';
    echo htmlentities(JText::_('USRLSUBMIT'),ENT_COMPAT,'UTF-8').'</button>';
    echo '<input type="hidden" name="option" value="com_prayercenter" />';
    echo '<input type="hidden" name="controller" value="prayercenter" />';
    echo '<input type="hidden" name="task" value="subscribesubmit" />';
    echo '<input type="hidden" name="return" value="'.$return.'" />';
    echo JHTML::_( 'form.token' );
    echo '</form>';
    $return_subscribmsg = "";
    if(JRequest::getVar( 'return_subscribmsg', null, 'get', '' )) $return_subscribmsg = JRequest::getVar( 'return_subscribmsg', null, 'get', '' );
    echo '<div><br /><font color="red"><b>'.wordwrap($return_subscribmsg,22,"<br />").'</b></font></div>';
    echo '</div>'; 
  }
} else { 
  if(!defined('PCCOMNOTINSTALL')) define('PCCOMNOTINSTALL','PrayerCenter Component Not Installed');
  echo '<div><center><font color="red"><b>'.htmlentities(JText::_('PCCOMNOTINSTALL'),ENT_COMPAT,'UTF-8').'</b></font></center></div>';
}
?>