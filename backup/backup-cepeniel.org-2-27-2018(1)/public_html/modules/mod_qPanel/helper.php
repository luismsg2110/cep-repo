<?php
/* Qlue Panel
*  Copyright (C) 2010 Qlue
*
*  This file is part of Qlue P.
*
*  Qlue Panel is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  Qlue Panel is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with Qlue Panel. If not, see <http://www.gnu.org/licenses/>.
*/
 
//stop direct access
defined("_JEXEC") or die("Restricted Access");
/**
 * Class to get and render the content within the Qlue Panel module.
 * 
 */
class QluePanel extends JObject{
   
   //set global variables
   public $_id = 0;
   private $_params = null;
   
   public static function getInstance($params) {
       static $instance;
       if(!isset($instance)) {
         $instance = new QluePanel();
       }
       // Update the id
       $instance->id++;
       // Update the params
       $instance->setParams($params);
       return $instance;
   }
   public function setParams($params) {
     $this->_params = $params;
   }
   
   /**
    * Get the content from the Category the use has specified. The information is then 
    * sorted and a tab is created and outputted to an array for use within the rest of
    * the module.
    * @parameter $params = parameters and other data supplied from the site
    * @return = an array of the Panel and the content held within them
    */
   function getContent() {
     //user detection for articles that show for registered users only
     $user = &JFactory::getUser();
     //get the access level the current user has (registered, administrator e.t.c)
     $access = $user->aid;
     $groups = implode(',', $user->getAuthorisedViewLevels());
     $db = &JFactory::getDBO();
     
     //get the parameters that the user has specified
     $numberPanel = (int)$this->_params->get("numberPanel");
     $catId  = $this->_params->get("categoryId", null);
     $order  = $this->_params->get("orderBy", "article"); 
     
    // Get dates and times
    $nullDate   = $db->getNullDate();
    $date = JFactory::getDate();
    $now = $date->toMySQL();
     
     //prepare the sql order
     switch($order){
       case "article":
         $order = "ordering ASC";
         break;
       case "alpha":
         $order = "title ASC";
         break;
       case "modified":
         $order = "modified ASC";
         break;
       case "newDate":
         $order = "created ASC";
         break;
       case "oldDate":
         $order = "created DESC";
         break;
     }
   
      //build the sql query, Searches all articles which are in the specified category and is published. Will only bring back the articles the user is allowed to see. Example guests will not see registered content
      $query = $db->getQuery(true);
      
      $query->clear();
      
      $query->select('a.id, a.title AS title, a.metadesc, a.metakey, a.created AS created, '
              .'a.introtext AS text, c.title AS section, '
              .'CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug, '
              .'CASE WHEN CHAR_LENGTH(c.alias) THEN CONCAT_WS(":", c.id, c.alias) ELSE c.id END as catslug, '
              .'"2" AS browsernav');
      $query->from('#__content AS a');
      $query->innerJoin('#__categories AS c ON c.id=a.catid');
      $query->where('a.catid = '. (int)$catId .' AND a.state=1 AND c.published = 1 AND a.access IN ('.$groups.') '
              .'AND c.access IN ('.$groups.') '
              .'AND (a.publish_up = '.$db->Quote($nullDate).' OR a.publish_up <= '.$db->Quote($now).') '
              .'AND (a.publish_down = '.$db->Quote($nullDate).' OR a.publish_down >= '.$db->Quote($now).')' );
      $query->group('a.id');
      $query->order($order);
      
      $db->setQuery($query, 0, (int)$numberPanel);
     
      //put the results into the variable $articles
      $articles = $db->loadObjectList();
      //return the variable to prepare for display
      return $articles;
   }

   function renderContent( $content ){
    return JHtml::_('content.prepare', $content->text);
   }
}
?>