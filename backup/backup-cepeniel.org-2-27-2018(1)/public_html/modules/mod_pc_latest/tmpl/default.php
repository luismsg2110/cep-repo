<?php
/*****************************************************************************************
 Title          PrayerCenter Latest Prayer Module for Joomla 1.6
 Author         Mike Leeper
 Version        1.6.0
 License        This is free software and you may redistribute it under the GPL.
                PrayerCenter Latest Prayer comes with absolutely no warranty. For details, 
                see the license at http://www.gnu.org/licenses/gpl.txt
                YOU ARE NOT REQUIRED TO KEEP COPYRIGHT NOTICES IN
                THE HTML OUTPUT OF THIS SCRIPT. YOU ARE NOT ALLOWED
                TO REMOVE COPYRIGHT NOTICES FROM THE SOURCE CODE.
 
******************************************************************************************/
defined( '_JEXEC' ) or die( 'Restricted access' );// no direct access

if(file_exists(JPATH_ROOT.DS."administrator".DS."components".DS."com_prayercenter".DS."config.xml")){
  require_once( JPATH_ROOT.DS."components".DS."com_prayercenter".DS."helpers".DS."pc_includes.php" );
  require_once( JPATH_ROOT.DS."components".DS."com_prayercenter".DS."helpers".DS."pc_class.php" );
  global $db,$pcConfig;
  $prayercenterlmod = new prayercenter();
  $pc_rights = $prayercenterlmod->intializePCRights();
  $itemid = $prayercenterlmod->PCgetItemid();
  $lang =& Jfactory::getLanguage();
  $lang->load( 'com_prayercenter', JPATH_SITE); 
  $db	=& JFactory::getDBO();
  $user_view = $pcConfig['config_user_view'];
  $count = $params->get( 'count' );
	$wordcount = $params->get('word_count');
  $link = JRoute::_('index.php?option=com_prayercenter&task=view&Itemid='.$itemid);
  $image = JHTML::_('image.site', 'separator.gif', '/components/com_prayercenter/assets/fe-images/', NULL, NULL, NULL, 'width="100"');
  $request = "";  
	$query = "SELECT a.id, a.requester, a.request, TIMESTAMP(CONCAT( a.date,' ', a.time)) AS date "
	. "\n FROM #__prayercenter AS a"
	. "\n WHERE a.publishstate='1' AND a.displaystate='1' AND a.archivestate='0'"
	. "\n ORDER BY date DESC"
	. "\n LIMIT $count"
	;
	$db->setQuery( $query );
	$rows = $db->loadObjectList();
  if($pc_rights->get( 'pc.view' )){
    if(count($rows) > 0){
  	?>
    <style>
      div.moduletable ul.pcl {
      	margin-left: 0px;
        list-style-type: none;
        padding-left:0px;
      }
    </style>
  	<div cellpadding="0" cellspacing="0" class="moduletable<?php echo $params->get('moduleclass_sfx'); ?>">
  			<ul class="pcl<?php echo $params->get( 'moduleclass_sfx'); ?>">
          <?php
          for( $i=0; $i<count($rows); $i++ ) {
  					if ($wordcount)
  					{
  						$texts = explode(' ', $rows[$i]->request);
  						$count = count($texts);
  						if ($count > $wordcount)
  						{
  							for ($j = 0; $j < $wordcount; $j++) {
  								$request .= ' '.$texts[$j];
  							}
  							$request .= '...';
  						} else {
                $request = $rows[$i]->request;
              }
  					} else {
              $request = $rows[$i]->request;
            }
            $viewlink = JRoute::_('index.php?option=com_prayercenter&task=view_request&id='.$rows[$i]->id.'&Itemid='.$itemid);
          	?>
          	<li style="padding-bottom:10px">
            <?php if($i > 0 && $i != count($rows)) echo '<br />';?>
              <?php echo '<b>'.JText::_('USRLPOSTEDBY').'</b><br />'.wordwrap($rows[$i]->requester,22,"<br />\n",true).'<br />';?>
              <?php echo '('.date("M j,Y",strtotime($rows[$i]->date)).')<br />'; ?><br />
              <?php echo '<i>"'.$prayercenterlmod->PCkeephtml($request).'"</i><small>&nbsp;&nbsp;&nbsp;<a href="'.$viewlink.'" /><i>'.JText::_('READMORE').'</i></a></small>'; ?>
          	</li>
          	<?php
            echo $image;
            $request = "";
          }
        ?>
        </ul><br />
          <small><a class="readon" style="margin-top: 4px;margin-right: 2px;" href="<?php echo $link; ?>"><?php echo JText::_('USRLVIEWLIST'); ?></a></small>
      	</div>
    	<?php
      } else {
      	echo '<div><center><b>';
      	echo wordwrap(JText::_('USRLNOREQUEST'),20,"<br />\n",true);
      	echo '</b></center></div>';
      }
    } else {
      echo '<div>&nbsp;</div>';
    }
} else { 
  if(!defined('PCCOMNOTINSTALL')) define('PCCOMNOTINSTALL','PrayerCenter Component Not Installed');
  echo '<div><center><font color="red"><b>'.htmlentities(JText::_('PCCOMNOTINSTALL')).'</b></font></center></div>';
}

?>
