<?php
/*****************************************************************************************
 Title          PrayerCenter Latest Prayer Module for Joomla 1.6
 Author         Mike Leeper
 Version        1.6.0
 License        This is free software and you may redistribute it under the GPL.
                PrayerCenter Latest Prayer comes with absolutely no warranty. For details, 
                see the license at http://www.gnu.org/licenses/gpl.txt
                YOU ARE NOT REQUIRED TO KEEP COPYRIGHT NOTICES IN
                THE HTML OUTPUT OF THIS SCRIPT. YOU ARE NOT ALLOWED
                TO REMOVE COPYRIGHT NOTICES FROM THE SOURCE CODE.
 
******************************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

require_once( dirname(__FILE__).DS.'helper.php' );
require( JModuleHelper::getLayoutPath( 'mod_pc_latest' ) );

?>
