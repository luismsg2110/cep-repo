<?php
/**
 * @package		GAnalytics
 * @author		Digital Peak http://www.digital-peak.com
 * @copyright	Copyright (C) 2012 Digital Peak, All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL
 */

defined('_JEXEC') or die();

require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ganalytics'.DS.'helper'.DS.'ganalytics.php');

require_once(dirname(__FILE__).DS.'helper.php');

$profile = ModGAnalyticsStatsCountHelper::getSelectedProfile($params);
if(empty($profile)){
	return;
}
require(JModuleHelper::getLayoutPath('mod_ganalytics_stats_count'));