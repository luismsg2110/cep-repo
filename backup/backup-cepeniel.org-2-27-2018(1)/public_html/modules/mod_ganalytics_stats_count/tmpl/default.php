<?php
/**
 * @package		GAnalytics
 * @author		Digital Peak http://www.digital-peak.com
 * @copyright	Copyright (C) 2012 Digital Peak, All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL
 */

defined('_JEXEC') or die();

$document = &JFactory::getDocument();
$document->addStyleSheet(JURI::base().'components/com_ganalytics/views/ganalytics/tmpl/ganalytics.css');

$startDate = new JDate($profile->startDate);
$yearData = null;
$output = "<table class=\"ganalytics-table\">\n";
if($params->get('selectTotalVisitors', 'yes') == 'yes'){
	$yearData = GAnalyticsDataHelper::getData($profile, array('year'), array('visits'), $startDate);
	$sum = 0;
	foreach ($yearData as $row) {
		$sum += $row->getVisits();
	}
	$output .=  "<tr><td>".JText::_('MOD_GANALYTICS_STATS_COUNT_TOTAL_VISITORS').":</td><td>".$sum."</td></tr>\n";
}
if($params->get('selectVisitorsDay', 'yes') == 'yes'){
	if($yearData === null){
		$yearData = GAnalyticsDataHelper::getData($profile, array('year'), array('visits'), $startDate);
	}
	$sum = 0;
	foreach ($yearData as $row) {
		$sum += $row->getVisits();
	}
	$days = (time() - $startDate->format('U')) / 86400;
	if($days == 0)$days = 1;
	$output .=  "<tr><td>".JText::_('MOD_GANALYTICS_STATS_COUNT_VISITORS_A_DAY').":</td><td align=\"right\">".ceil($sum / $days)."</td></tr>\n";
}
$output .= "</table>\n";
echo $output;