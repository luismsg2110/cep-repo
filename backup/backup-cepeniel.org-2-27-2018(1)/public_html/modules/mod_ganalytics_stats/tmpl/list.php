<?php
/**
 * @package		GAnalytics
 * @author		Digital Peak http://www.digital-peak.com
 * @copyright	Copyright (C) 2012 Digital Peak, All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL
 */

defined('_JEXEC') or die();

echo $title;

$document = &JFactory::getDocument();
$document->addScript('http://www.google.com/jsapi');
if(GAnalyticsHelper::isPROMode()){
	$document->addScript(JURI::base().'administrator/components/com_ganalytics/libraries/jquery/ganalytics/chart.js');
}else{
	$document->addScript(JURI::base().'administrator/components/com_ganalytics/libraries/jquery/ganalytics/list.js');
}
$document->addStyleSheet(JURI::base().'modules/mod_ganalytics_stats/tmpl/ganalytics.css');
$document->addScript(JURI::base().'modules/mod_ganalytics_stats/tmpl/ganalytics.js');

$showDateSelection = $params->get('showDateSelection', 'yes') == 'yes';
?>
<?php if($showDateSelection) {?>
<div>
<table class="mod-ganalytics-table">
	<tr>
		<td style="padding-right:20px">
		<?php
		echo JText::_('MOD_GANALYTICS_STATS_CHART_VIEW_SELECT_DATE_FROM');
		echo JHtml::_('calendar', strftime('%Y-%m-%d', $startDate), 'mod_date_from', 'mod_date_from_'.$module->id, '%Y-%m-%d', array('size' => 10));
		?>
		</td>
		<td style="padding-right:20px">
		<?php
		echo JText::_('MOD_GANALYTICS_STATS_CHART_VIEW_SELECT_DATE_TO');
		echo JHtml::_('calendar', strftime('%Y-%m-%d', $endDate), 'mod_date_to', 'mod_date_to_'.$module->id, '%Y-%m-%d', array('size' => 10));
		?>
		</td>
		<td style="padding-right:20px"><button onclick="showModCharts();"><?php echo JText::_('MOD_GANALYTICS_STATS_CHART_VIEW_BUTTON_UPDATE');?></button></td>
	</tr>
</table>
<?php
}
$scriptCode = "jQuery(document).ready(function(){\n";
?>
<fieldset class="mod-date-range-container">
<legend><?php echo GAnalyticsHelper::translate($dimensions).' -- '.GAnalyticsHelper::translate($metrics);?></legend>
	<?php
	if(stripos($dimensions, 'ga:date') !== false){?>
	<div class="mod-date-range-toolbar">
		<img src="media/com_ganalytics/images/dayrange/month-disabled-32.png" class="date-range-button date-range-month"
			alt="<?php echo JText::_('MOD_GANALYTICS_STATS_CHART_VIEW_IMAGE_DATE_RANGE_MONTH');?>"
			title="<?php echo JText::_('MOD_GANALYTICS_STATS_CHART_VIEW_IMAGE_DATE_RANGE_MONTH');?>" />
		<img src="media/com_ganalytics/images/dayrange/week-disabled-32.png" class="date-range-button date-range-week"
			alt="<?php echo JText::_('MOD_GANALYTICS_STATS_CHART_VIEW_IMAGE_DATE_RANGE_WEEK');?>"
			title="<?php echo JText::_('MOD_GANALYTICS_STATS_CHART_VIEW_IMAGE_DATE_RANGE_WEEK');?>" />
		<img src="media/com_ganalytics/images/dayrange/day-32.png" class="date-range-button date-range-day"
			alt="<?php echo JText::_('MOD_GANALYTICS_STATS_CHART_VIEW_IMAGE_DATE_RANGE_DAY');?>"
			title="<?php echo JText::_('MOD_GANALYTICS_STATS_CHART_VIEW_IMAGE_DATE_RANGE_DAY');?>" />
	</div>
	<?php } ?>
	<div id="ga-mod-chart-<?php echo $module->id;?>" class="mod-ga-chart"></div>
</fieldset>
</div>
<?php
$scriptCode .= "jQuery('#ga-mod-chart-".$module->id."').gaChart({
	chartDivID: 'gaChartDiv".$module->id."',
	url: 'index.php?option=com_ganalytics&source=module&view=data&format=raw&moduleid=".$module->id."',
	start: '".strftime("%Y-%m-%d", $startDate)."',
	end: '".strftime("%Y-%m-%d", $endDate)."',
	pathPrefix: '".JURI::root()."'
});\n";

$scriptCode .= "showModCharts();});\n";
$document->addScriptDeclaration($scriptCode);
