<?php
/**
 * @package		GAnalytics
 * @author		Digital Peak http://www.digital-peak.com
 * @copyright	Copyright (C) 2012 Digital Peak, All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL
 */

defined('_JEXEC') or die();

require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ganalytics'.DS.'helper'.DS.'ganalytics.php');
require_once(dirname(__FILE__).DS.'helper.php');

JFactory::getLanguage()->load('com_ganalytics', JPATH_ADMINISTRATOR);

$profile = ModGAnalyticsStatsHelper::getSelectedProfile($params);
if(empty($profile))return;

$startDate = time();
$endDate = time();
if($params->get('daterange', 'month') == 'advanced'){
	$tmp = $params->get('advancedDateRange', null);
	if(!empty($tmp)){
		$startDate = strtotime($tmp);
	} else {
		$tmp = $params->get('startdate', null);
		if(!empty($tmp)){
			sscanf($tmp,"%u-%u-%u", $year, $month, $day);
			$startDate = mktime(0, 0, 0, $month, $day, $year);
		}
		$tmp = $params->get('enddate', null);
		if(!empty($tmp)){
			sscanf($tmp,"%u-%u-%u", $year, $month, $day);
			$endDate = mktime(0, 0, 0, $month, $day, $year);
		}
	}
}else{
	$range = '';
	switch ($params->get('daterange', 'month')) {
		case 'day':
			$range = '-1 day';
			break;
		case 'week':
			$range = '-1 week';
			break;
		case 'month':
			$range = '-1 month';
			break;
		case 'year':
			$range = '-1 year';
			break;
	}
	$startDate = strtotime($range);
	$endDate = time();
}

$dimensions = '';
$metrics = '';
$sort = '';
if($params->get('type', 'visits') == 'advanced'){
	$dimensions = $params->get('dimensions', 'ga:date');
	$metrics = $params->get('metrics', 'ga:visits');
	$sort = $params->get('sort', '');
}else{
	switch ($params->get('type', 'visits')) {
		case 'visits':
			$dimensions = 'ga:date';
			$metrics = 'ga:visits';
			break;
		case 'visitsbytraffic':
			$dimensions = 'ga:source';
			$metrics = 'ga:visits';
			$sort = '-ga:visits';
			break;
		case 'visitsbybrowser':
			$dimensions = 'ga:browser';
			$metrics = 'ga:visits';
			$sort = '-ga:visits';
			break;
		case 'visitsbycountry':
			$dimensions = 'ga:country';
			$metrics = 'ga:visits';
			$sort = '-ga:visits';
			break;
		case 'timeonsite':
			$dimensions = 'ga:region';
			$metrics = 'ga:timeOnSite';
			$sort = '-ga:timeOnSite';
			break;
		case 'toppages':
			$dimensions = 'ga:pagePath';
			$metrics = 'ga:pageviews';
			$sort = '-ga:pageviews';
			break;
	}
}
$max = $params->get('max', 1000);

$dim = GAnalyticsHelper::translate($dimensions);
$metr = GAnalyticsHelper::translate($metrics);
$dateFormat = $params->get('dateFormat', '%d.%m.%Y');
$title = $params->get('titleFormat', '');
$title = str_replace("{accountname}", $profile->accountName, $title);
$title = str_replace("{profilename}", $profile->profileName, $title);
$title = str_replace("{dimension}", substr($dim, 0, strpos($dim, ' [ga:')), $title);
$title = str_replace("{metric}", substr($metr, 0, strpos($metr, ' [ga:')), $title);
$width = $params->get('width', '200px');
$height = $params->get('height', '200px');
$color = $params->get('color', null);

$mode = $params->get('mode', 'list');
if(!GAnalyticsHelper::isProMode()){
	$mode = 'list';
}

require( JModuleHelper::getLayoutPath('mod_ganalytics_stats',  $mode) );