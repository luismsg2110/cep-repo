<?php
/*------------------------------------------------------------------------
# mod_imgscrawler - Images Crawler
# ------------------------------------------------------------------------
# author    Joomla!Vargas
# copyright Copyright (C) 2010 joomla.vargas.co.cr. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://joomla.vargas.co.cr
# Technical Support:  Forum - http://joomla.vargas.co.cr/forum
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die;

jimport('joomla.filesystem.file');

abstract class modImgsCrawlerHelper
{

	public static function getImages(&$params, $folder)
	{
		$images	= array();
		
		$links     = explode( ',', $params->get( 'links' ) );
		$target    = $params->get( 'target', '_self' );
		$repeat    = $params->get( 'repeat', 0 );
		$margin    = intval($params->get( 'margin', 3 ));
		$direction = $params->get( 'direction', 'left' );
		$substoo   = $params->get( 'substoo', 0 );
		$random    = $params->get( 'random', 0 );
		$iwidth    = $params->get( 'iwidth' );
		$iheight   = $params->get( 'iheight' );
		
		if ($margin) :
			switch ( $direction ) {
				case 'left';
					$space = 'margin-right:'.$margin.'px';
					break;
				case 'right';
					$space = 'margin-left:'.$margin.'px';
					break;
			}
		endif;

		// check if directory exists
		if (is_dir(JPATH_BASE.DS.$folder))
		{
			$files = modImgsCrawlerHelper::getFiles($folder,$substoo,$random);
			$i = 0;
			$repeated = 0;
			foreach ($files as $file)
			{
				if (modImgsCrawlerHelper::isImage($file)) {
					$file	    = str_replace( '\\', '/', $file );
					$attribs    = array();
					if ($margin) $attribs['style']  = $space;
					if ($iwidth) $attribs['width'] = intval($iwidth);
					if ($iheight) $attribs['height'] = intval($iheight);
					$images[$i] = JHTML::_('image', $file, '', $attribs);
					if ($links && (isset($links[$i]) or $repeat)) :
						if ($links[$i]) {
							$images[$i] = JHTML::_('link', trim($links[$i]), $images[$i], ($target ? array('target' => '_blank') : '' ) );
						} else {
							$repeated++;
							$images[$i] = JHTML::_('link', trim($links[$repeated-1]), $images[$i], ($target ? array('target' => '_blank') : '' ) );
							if ($repeated == count($links)) $repeated = 0 ;
						}
					endif;
					++$i;
				}
			}
		}

		return $images;
	}

	public static function getFolder(&$params)
	{
		$folder 	= $params->get( 'folder' );
		
		// Remove the trailing slash from the url (if any)
		if (substr($folder, -1) == '/') {
			$folder = substr($folder, 0 ,-1);
		}

		$LiveSite 	= JURI::base();

		// if folder includes livesite info, remove
		if ( JString::strpos($folder, $LiveSite) === 0 ) {
			$folder = str_replace( $LiveSite, '', $folder );
		}
		// if folder includes absolute path, remove
		if ( JString::strpos($folder, JPATH_SITE) === 0 ) {
			$folder= str_replace( JPATH_BASE, '', $folder );
		}
		$folder = str_replace('\\',DS,$folder);
		$folder = str_replace('/',DS,$folder);

		return $folder;
	}
	
	public static function getFiles($folder,$substoo,$random) {
	
		$dir      = JPATH_BASE.DS.$folder;
			
		$files	  = array();
		$subfiles = array();
	
		if ($handle = opendir($dir)) {
			while (false !== ($file = readdir($handle))) {
				if ($file != '.' && $file != '..' && $file != 'CVS' && $file != 'index.html' ) {
					if (!is_dir($dir . DS . $file))
					{
						$files[] = $folder . DS . $file;
					} elseif ($substoo != 0) {
						$newfolder = $folder . DS . $file;
						$subfiles[]  = modImgsCrawlerHelper::getFiles($newfolder,$substoo,$random);
					}
				}
			}
		}
		closedir($handle);
		
		sort($files);
		
		foreach ($subfiles as $subfile) :
			$files = array_merge($files,$subfile);
		endforeach;
		
		if ($random) :
			shuffle($files);
		endif;
		
		return $files;
	}
		
    public static function isImage($file) {
		$file  = JFile::getName($file);
		$file  = JFile::getExt($file);
		$file  = strtolower($file);
        $types = array("jpg", "jpeg", "gif", "png");
        if (in_array($file, $types)) return true;
        else return false;
    }
}
