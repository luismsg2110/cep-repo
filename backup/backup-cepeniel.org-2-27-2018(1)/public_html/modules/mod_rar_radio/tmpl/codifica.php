<?php
/**
 * Element: codifica
 * codifica las URL
 *
 * @package    rar_radio
 * @version    v0.3.3
 *
 * @author     Roberto Arias <info@rarcompucion.com>
 * @link       http://www.rarcomputacion.com
 * @copyright  Copyright (C) 2010 RARComputacion.com All Rights Reserved
 * @license    http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

/***************** decodificado de variables **************************/

function base64_url_decode($input) {
    return base64_decode(strtr($input, '-_,', '+/='));
    }
    
function base64_url_encode($input) {
    return strtr(base64_encode($input), '+/=', '-_,');
    }
?>
