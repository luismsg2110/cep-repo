<?php
/************************************************************************************************
 Title          Mod_pc_submit_request PrayerCenter prayer request submit module for Joomla
 Author         Mike Leeper
 Version        2.5.0
 URL            http://www.mlwebtechnologies.com
 Email          web@mlwebtechnologies.com
 License        This is free software and you may redistribute it under the GPL.
                Mod_pc_submit_request comes with absolutely no warranty. For details, 
                see the license at http://www.gnu.org/licenses/gpl.txt
                YOU ARE NOT REQUIRED TO KEEP COPYRIGHT NOTICES IN
                THE HTML OUTPUT OF THIS SCRIPT. YOU ARE NOT ALLOWED
                TO REMOVE COPYRIGHT NOTICES FROM THE SOURCE CODE.
                Requires the PrayerCenter component v2.5.0 or higher
*************************************************************************************************
// no direct access*/
defined( '_JEXEC' ) or die( 'Restricted access' );
class mod_pc_submit_requestHelper{
	static function getReturnURL()
	{
		$app	= JFactory::getApplication();
		$router = $app->getRouter();
		$uri = clone JFactory::getURI();
		$vars = $router->parse($uri);
		unset($vars['lang']);
		if($router->getMode() == JROUTER_MODE_SEF)
		{
			if (isset($vars['Itemid']))
			{
				$itemid = $vars['Itemid'];
				$menu = $app->getMenu();
				$item = $menu->getItem($itemid);
				unset($vars['Itemid']);
				if ($uri.'/' == JURI::base()) {
					$url = '';
				}
				else {
					$url = 'index.php?'.JURI::buildQuery($vars).'&Itemid='.$itemid;
				}
			}
			else
			{
				$url = 'index.php?'.JURI::buildQuery($vars);
			}
		}
		else
		{
			$url = 'index.php?'.JURI::buildQuery($vars);
		}
		return $url;
	}
}
?>