<?php
/********************************************************************************************
 Title          Mod_pc_submit_request PrayerCenter prayer request submit module for Joomla
 Author         Mike Leeper
 Version        2.5.0
 URL            http:www.mlwebtechnologies.com
 Email          web@mlwebtechnologies.com
 License        This is free software and you may redistribute it under the GPL.
                Mod_pc_submit_request comes with absolutely no warranty. For details, 
                see the license at http:www.gnu.org/licenses/gpl.txt
                YOU ARE NOT REQUIRED TO KEEP COPYRIGHT NOTICES IN
                THE HTML OUTPUT OF THIS SCRIPT. YOU ARE NOT ALLOWED
                TO REMOVE COPYRIGHT NOTICES FROM THE SOURCE CODE.
                Requires the PrayerCenter component v2.5.0 or higher
*********************************************************************************************/
defined( '_JEXEC' ) or die( 'Restricted access' );// no direct access
$user = &JFactory::getUser();
$access_gid =$user->get('gid');
$lang =& Jfactory::getLanguage();
$lang->load( 'com_prayercenter', JPATH_SITE);
if(file_exists(JPATH_ROOT.DS."administrator".DS."components".DS."com_prayercenter".DS."config.xml")){
  require_once( JPATH_ROOT.DS."components".DS."com_prayercenter".DS."helpers".DS."pc_includes.php" );
  require_once( JPATH_ROOT.DS."components".DS."com_prayercenter".DS."helpers".DS."pc_class.php" );
  global $pcConfig;
  $prayercentermsr = new prayercenter();
  $pc_rights = $prayercentermsr->intializePCRights();
  $config_captcha = $pcConfig['config_captcha'];
  $config_captcha_bypass = $pcConfig['config_captcha_bypass_4member'];
  $user_post = $pcConfig['config_user_post'];
  $show_email_option = $pcConfig['config_email_option'];
  $show_priv_option = $pcConfig['config_show_priv_option'];
  $maxattempts = $pcConfig['config_captcha_maxattempts'];
  $config_use_admin_alert = $pcConfig['config_use_admin_alert'];
  $livesite = JURI::base();
  ?><script language="JavaScript" type="text/javascript">
    var enter_req = '<?php echo htmlentities(JText::_('USRLENTERREQ'),ENT_COMPAT,'UTF-8');?>';
    var confirm_enter_email = '<?php echo htmlentities(JText::_('USRLCONFIRMENTEREMAIL'),ENT_COMPAT,'UTF-8');?>';
    var enter_sec_code = '<?php echo htmlentities(JText::_('USRLENTERSECCODE'),ENT_COMPAT,'UTF-8');?>';
    var enter_valid_email = '<?php echo htmlentities(JText::_('USRLINVALIDEMAIL'),ENT_COMPAT,'UTF-8');?>';
    var livesite = '<?php echo $livesite;?>';
  </script>
  <?php
  $document =& JFactory::getDocument();
  $document->addScript('components/com_prayercenter/assets/js/pc.js');
  if ($pc_rights->get('pc.post')){
    $id = $user->name;
    $email = $user->email;
    $sendpriv=1;
    $subpraise=0;
    $js_script = "";
    if(session_id() == "") session_start();
    echo '<div class="moduletable'.$moduleclasssfx.'">';
    echo '<a name="pcmsr"></a>';
    echo '<form method="post" action="index.php?modtype=return_submsg&mod=pcmsr" name="mme">';
    echo '<label for="newrequester">'.htmlentities(JText::_('USRLNAME'),ENT_COMPAT,'UTF-8').': ('.htmlentities(JText::_('USRLOPTIONAL'),ENT_COMPAT,'UTF-8').')</label><br />';
    echo '<input type="text" name="newrequester" id="newrequester" size="'.$cols.'" value="'.$id.'" /><br />';
    if ($show_email_option == '1'){
      echo '<label for="newemail">'.htmlentities(JText::_('USRLEMAIL'),ENT_COMPAT,'UTF-8').': ';
      if($config_use_admin_alert != 1) echo '('.htmlentities(JText::_('USRLOPTIONAL'),ENT_COMPAT,'UTF-8').')';
      echo '</label><br />';
      echo '<input type="text" name="newemail" id="newemail" size="'.$cols.'" value="'.$email.'" /><br />';
    }
    echo '<label for="newtitle">'.htmlentities(JText::_('PCREQTITLE'),ENT_COMPAT,'UTF-8').'</label><br />';
    echo '<INPUT TYPE="TEXT" name="newtitle" id="newtitle" size="'.$cols.'" class="inputbox" value="" /><br />';
    echo '<label for="newtopic">'.htmlentities(JText::_('PCREQTOPIC'),ENT_COMPAT,'UTF-8').'</label><br />';
    $newtopicarray = $prayercentermsr->PCgetTopics();
    echo '<select name="newtopic">';
    $topics = '<option value="">'.htmlentities(JText::_('PCSELECTTOPIC'),ENT_COMPAT,'UTF-8').'</option>';
    foreach($newtopicarray as $nt){
      $topics .= '<option value="'.$nt['val'].'">'.html_entity_decode($nt['text']).'</option>';
    }
    echo $topics;
    echo '</select><br /><br />';
    echo '<label for="newrequest">'.htmlentities(JText::_('USRLREQUEST'),ENT_COMPAT,'UTF-8').':</label><br />';
    echo '<textarea name="newrequest" id="mnewrequest" class="inputbox" rows="11" cols="'.$cols.'" style="resize:none;"></textarea>';
    echo '<input type="hidden" name="sendpriv" size="5" class="inputbox" value="'.$sendpriv.'" />';
    echo '<span style="display:none;visibility:hidden;">';
    echo '<input type="text" name="temail" size="5" class="inputbox" value="" />';
    echo '<input type="text" name="formtime" size="5" class="inputbox" value="'.time().'\" />';
    echo '</span>';
    if ($show_priv_option == '1'){
      echo '<br /><br /><input type="checkbox" class=radio name="msend" id="msend" onClick="javascript:if(document.adminForm.msend.checked){document.adminForm.sendpriv.value=0;}else{document.adminForm.sendpriv.value=1;}" />';
      echo '&nbsp;<label for="msend"><span style="font-size:x-small;white-space:nowrap;">'.htmlentities(JText::_('USRLPRIV'),ENT_COMPAT,'UTF-8').'</span></label>';
    }
    $user = &JFactory::getUser();
    if((!$config_captcha_bypass && $config_captcha) || ($config_captcha_bypass && $user->get('id') == 0 && $config_captcha)){
      if($config_use_admin_alert == 1){
        $js_script = "document.getElementById('valreq').value=document.getElementById('mnewrequest').value;return validateNewE(".$config_captcha.", 'none', livesite, this.form, 'pcmsr');";
        } else {
        $js_script = "document.getElementById('valreq').value=document.getElementById('mnewrequest').value;return validateNew(".$config_captcha.", 'none', livesite, this.form, 'pcmsr');";
        }
      if ($config_captcha) {
        echo $prayercentermsr->PCgetCaptchaImg('pcmsr','mme');
      }
    } else {
      if($config_use_admin_alert == 1){
        $js_script = 'document.getElementById(\'valreq\').value=document.getElementById(\'mnewrequest\').value;return validateNewE(0, \'none\', livesite, this.form, \'pcmsr\');';
        } else {
        $js_script = 'document.getElementById(\'valreq\').value=document.getElementById(\'mnewrequest\').value;return validateNew(0, \'none\', livesite, this.form, \'pcmsr\');';
        }
      echo '<br /><br />';
     }
    echo '&nbsp;<button type="button" onclick="javascript:'.$js_script.'">';
    echo htmlentities(JText::_('USRLSUBMIT'),ENT_COMPAT,'UTF-8').'</button>';
    echo '<input type="hidden" name="valreq" id="valreq" value="" />';
    echo '<input type="hidden" name="option" value="com_prayercenter" />';
    echo '<input type="hidden" name="controller" value="prayercenter" />';
    echo '<input type="hidden" name="task" value="newreqsubmit" />';
    echo '<input type="hidden" name="return" value="'.$return.'" />';
    echo JHTML::_( 'form.token' );
    echo '</form>';
    $return_submsg = "";
    if(JRequest::getVar( 'return_submsg', null, 'get', '' )) $return_submsg = JRequest::getVar( 'return_submsg', null, 'get', '' );
    echo '<div style="text-align:center;"><br /><font color="red"><b>'.wordwrap($return_submsg,22,"<br />").'</b></font></div>';
    echo '</div>';
  }
} else { 
  if(!defined('PCCOMNOTINSTALL')) define('PCCOMNOTINSTALL','PrayerCenter Component Not Installed');
  echo '<div><center><font color="red"><b>'.htmlentities(JText::_('PCCOMNOTINSTALL'),ENT_COMPAT,'UTF-8').'</b></font></center></div>';
}
?>