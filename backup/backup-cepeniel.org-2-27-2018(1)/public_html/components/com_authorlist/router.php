<?php
/*------------------------------------------------------------------------
# com_authorlist - Author List
# ------------------------------------------------------------------------
# author    Joomla!Vargas
# copyright Copyright (C) 2012 joomla.vargas.co.cr. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://joomla.vargas.co.cr
# Technical Support:  Forum - http://joomla.vargas.co.cr/forum
-------------------------------------------------------------------------*/

// No direct access
defined('_JEXEC') or die;

function AuthorListBuildRoute(&$query)
{
	$segments = array();
	$app	= JFactory::getApplication();
	$menu	= $app->getMenu();

	if (empty($query['Itemid'])) {
		$menuItem = $menu->getActive();
	} else {
		$menuItem = $menu->getItem($query['Itemid']);
	}
	$mView	= (empty($menuItem->query['view'])) ? null : $menuItem->query['view'];
	$mId	= (empty($menuItem->query['id'])) ? null : $menuItem->query['id'];

	if (isset($query['view']))
	{
		$view = $query['view'];
		if (empty($query['Itemid'])) {
			$segments[] = $query['view'];
		}
		unset($query['view']);
	};

	if (isset($query['view']) && ($mView == $query['view']) and (isset($query['id'])) and ($mId == intval($query['id']))) {
		unset($query['view']);
		unset($query['id']);

		return $segments;
	}
	
	if (isset($view) and $view == 'author') {
		if ($mId != intval($query['id']) || $mView != $view) {
			if (isset($query['id'])) {
				$id = $query['id'];
			}
			$segments[] = $id;
		}
		unset($query['id']);
	}
	if (isset($query['layout'])) {
		unset($query['layout']);	
	}
	
	unset($query['id']);
	
	return $segments;
}

function AuthorListParseRoute($segments)
{
	$app	= JFactory::getApplication();
	$menu	= $app->getMenu();
	$item	= $menu->getActive();

	$count = count($segments);

	if (!isset($item)) {
		$vars['view']	= $segments[0];
		$vars['id']		= $segments[$count - 1];

		return $vars;
	}

	$vars['view'] = 'author';
	$vars['id'] = (int)$segments[$count - 1];

	return $vars;
}
