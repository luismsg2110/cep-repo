<?php
/*------------------------------------------------------------------------
# com_authorlist - Author List
# ------------------------------------------------------------------------
# author    Joomla!Vargas
# copyright Copyright (C) 2012 joomla.vargas.co.cr. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://joomla.vargas.co.cr
# Technical Support:  Forum - http://joomla.vargas.co.cr/forum
-------------------------------------------------------------------------*/

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class AuthorListModelAuthors extends JModelList
{

	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 'a.id',
				'name', 'u.name',
				'username', 'u.username',
				'email', 'a.email',
				'ordering', 'a.ordering',
				'articles_count',
			);
		}

		parent::__construct($config);
	}

	public function &getItems()
	{
		$items = parent::getItems();

		for ($i = 0, $n = count($items); $i < $n; $i++) {
			$item = &$items[$i];
			if (!isset($this->_params)) {
				$params = new JRegistry();
				$params->loadJSON($item->params);
				$item->params = $params;
			}
		}

		return $items;
	}

	protected function getListQuery()
	{
		$user	= JFactory::getUser();
		$groups	= implode(',', $user->getAuthorisedViewLevels());

		$db		= $this->getDbo();
		$query	= $db->getQuery(true);

		$query->select($this->getState('list.select', 'u.name AS name,u.username AS username,u.email AS email,a.*') . ','
		. ' CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(\':\', a.userid, a.alias) ELSE a.userid END as slug');
		
		$query->from('`#__users` AS u');

		$query->select('COUNT(c.id) AS articles_count');
		$query->join('LEFT', '#__content AS c ON c.created_by = u.id');
		
		$query->join('LEFT', '#__authorlist AS a ON a.userid = u.id');
		
		$query->where('a.access IN ('.$groups.')');

		$state = $this->getState('filter.published');
		if (is_numeric($state)) {
			$query->where('a.state = '.(int) $state);
		}
		
		$query->group('a.id');

		$query->order($db->getEscaped($this->getState('list.ordering', 'a.ordering')).' '.$db->getEscaped($this->getState('list.direction', 'ASC')));
		
		return $query;
	}

	protected function populateState($ordering = null, $direction = null)
	{
		$app	= JFactory::getApplication();
		$params	= JComponentHelper::getParams('com_authorlist');
		$db		= $this->getDbo();
		$format = JRequest::getWord('format');
		if ($format=='feed') {
			$limit = $app->getCfg('feed_limit');
		}
		else {
			$limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'));
		}
		$this->setState('list.limit', $limit);

		$limitstart = JRequest::getVar('limitstart', 0, '', 'int');
		$this->setState('list.start', $limitstart);
		
		$order = 'a.ordering';
		$direc = 'ASC';
		if ($params->get('authors_order') != 'order') {
			if ($params->get('show_author_name') == 1) {
				$order = 'u.username';
			} else {
				$order = 'u.name';
			}
			$direc = $params->get('authors_order');
		}

		$orderCol	= JRequest::getCmd('filter_order', $order);
		if (!in_array($orderCol, $this->filter_fields)) {
			$orderCol = 'a.ordering';
		}
		$this->setState('list.ordering', $orderCol);

		$listOrder	=  JRequest::getCmd('filter_order_Dir', $direc);
		if (!in_array(strtoupper($listOrder), array('ASC', 'DESC', ''))) {
			$listOrder = 'ASC';
		}
		$this->setState('list.direction', strtoupper($listOrder));

		$user = JFactory::getUser();
		if ((!$user->authorise('core.edit.state', 'com_contact')) &&  (!$user->authorise('core.edit', 'com_contact'))){
			$this->setState('filter.published', 1);
			$this->setState('filter.publish_date', true);
		}
		$this->setState('filter.language',$app->getLanguageFilter());

		$this->setState('params', $params);
	}

}
