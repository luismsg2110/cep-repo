<?php
/*------------------------------------------------------------------------
# com_authorlist - Author List
# ------------------------------------------------------------------------
# author    Joomla!Vargas
# copyright Copyright (C) 2012 joomla.vargas.co.cr. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://joomla.vargas.co.cr
# Technical Support:  Forum - http://joomla.vargas.co.cr/forum
-------------------------------------------------------------------------*/

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class AuthorListModelAuthor extends JModelList
{

	protected $_item = null;

	protected $_articles = null;

	protected $_context = 'com_authorlist.author';

	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 'a.id',
				'name', 'a.name',
				'alias', 'a.alias',
				'catid', 'a.catid', 'category_title',
				'state', 'a.state',
				'access', 'a.access', 'access_level',
				'created', 'a.created',
				'created_by', 'a.created_by',
				'ordering', 'a.ordering',
				'hits', 'a.hits',
				'rating',
				'author', 'a.author'
			);
		}

		parent::__construct($config);
	}

	protected function populateState($ordering = null, $direction = null)
	{
		$app	= JFactory::getApplication('site');
		
		$pk = JRequest::getInt('id');
		$this->setState('author.id', $pk);

		$params = $app->getParams();
		$this->setState('params', $params);

		if (!$params->get('show_noauth')) {
			$this->setState('filter.access', true);
		}
		else {
			$this->setState('filter.access', false);
		}

		$this->setState('list.filter', JRequest::getString('filter-search'));

		$itemid = JRequest::getInt('id', 0) . ':' . JRequest::getInt('Itemid', 0);
		$orderCol = $app->getUserStateFromRequest('com_authorlist.author.list.' . $itemid . '.filter_order', 'filter_order', '', 'string');
		if (!in_array($orderCol, $this->filter_fields)) {
			$orderCol = 'a.ordering';
		}
		$this->setState('list.ordering', $orderCol);

		$listOrder = $app->getUserStateFromRequest('com_authorlist.author.list.' . $itemid . '.filter_order_Dir',
			'filter_order_Dir', '', 'cmd');
		if (!in_array(strtoupper($listOrder), array('ASC', 'DESC', ''))) {
			$listOrder = 'ASC';
		}
		$this->setState('list.direction', $listOrder);

		$this->setState('list.start', JRequest::getVar('limitstart', 0, '', 'int'));
		
		if ((JRequest::getCmd('layout') == 'blog') || $params->get('layout_type') == 'blog') {
			$limit = $params->get('num_leading_articles') + $params->get('num_intro_articles') + $params->get('num_links');
			$this->setState('list.links', $params->get('num_links'));
		}
		else {
			$limit = $app->getUserStateFromRequest('com_authorlist.author.list.' . $itemid . '.limit', 'limit', $params->get('display_num'));
		}
		
		$this->setState('list.limit', $limit);

	}

	function getItems()
	{
		$params = $this->getState('params');
		$limit = $this->getState('list.limit');

		if ($this->_articles === null) {
			//JModel::addIncludePath('components'.DS.'com_content'.DS.'models', 'ContentModel');
			$model = JModel::getInstance('Articles', 'AuthorListModel', array('ignore_request' => true));
			$model->setState('params', JFactory::getApplication()->getParams());
			$model->setState('filter.published', $this->getState('filter.published',1));
			$model->setState('filter.access', $this->getState('filter.access'));
			$model->setState('list.ordering', $this->_buildContentOrderBy());
			$model->setState('list.start', $this->getState('list.start'));
			$model->setState('list.limit', $limit);
			$model->setState('list.direction', $this->getState('list.direction'));
			$model->setState('list.filter', $this->getState('list.filter'));
			$model->setState('filter.category_id', $params->get('catid'));
			$model->setState('filter.category_id.include', $params->get('category_mode'));
			$model->setState('filter.sub_category', $params->get('sub_category'));
			$authorId = $this->getState('author.id');
			if ( $authorId != 0 ) {
				$model->setState('filter.author_id',$authorId);
			}
			if ($limit >= 0) {
				$this->_articles = $model->getItems();

				if ($this->_articles === false) {
					$this->setError($model->getError());
				}
			}
			else {
				$this->_articles=array();
			}

			$this->_pagination = $model->getPagination();
			
		}

		return $this->_articles;
	}

	protected function _buildContentOrderBy()
	{
		$app		= JFactory::getApplication('site');
		$db			= $this->getDbo();
		$params		= $this->state->params;
		$itemid		= JRequest::getInt('id', 0) . ':' . JRequest::getInt('Itemid', 0);
		$orderCol	= $app->getUserStateFromRequest('com_authorlist.author.list.' . $itemid . '.filter_order', 'filter_order', '', 'string');
		$orderDirn	= $app->getUserStateFromRequest('com_authorlist.author.list.' . $itemid . '.filter_order_Dir', 'filter_order_Dir', '', 'cmd');
		$orderby	= ' ';

		if (!in_array($orderCol, $this->filter_fields)) {
			$orderCol = null;
		}

		if (!in_array(strtoupper($orderDirn), array('ASC', 'DESC', ''))) {
			$orderDirn = 'ASC';
		}

		if ($orderCol && $orderDirn) {
			$orderby .= $db->getEscaped($orderCol) . ' ' . $db->getEscaped($orderDirn) . ', ';
		}

		$articleOrderby		= $params->get('orderby_sec', 'rdate');
		$articleOrderDate	= $params->get('order_date');
		$categoryOrderby	= $params->def('orderby_pri', '');
		$secondary			= AuthorListHelperQuery::orderbySecondary($articleOrderby, $articleOrderDate) . ', ';
		$primary			= AuthorListHelperQuery::orderbyPrimary($categoryOrderby);

		$orderby .= $db->getEscaped($primary) . ' ' . $db->getEscaped($secondary) . ' a.created ';

		return $orderby;
	}
	
	public function getAuthor () {
		$db		   = $this->getDbo();
		$author_id = $this->getState('author.id');
		$author = JUser::getInstance($author_id);
		if ($author) {
			$query = 'SELECT * FROM #__authorlist WHERE userid=' . $author_id;
			$db->setQuery($query);
			$result = $db->loadObject();
			
			if ($result) {	
				$filter = JFilterInput::getInstance();	
				jimport('joomla.mail.helper');
				
				$this->setState('location', $filter->clean(JRoute::_(AuthorListHelperRoute::getAuthorRoute($author_id.':'.$result->alias))));
				$author->description = $result->description;
				$author->image = $result->image;
				$temp = clone ($this->getState('params'));
				$registry = new JRegistry();
				$registry->loadJSON($result->params);
				$temp->merge($registry);
				if ($temp->get('show_author_name') == 1) {
					$author->displayName = $author->username;	
				} else {
					$author->displayName = $author->name;
				}
				if ($temp->get('show_email') == 1) {
					$author->email = trim($author->email);		
					if (!empty($author->email) && JMailHelper::isEmailAddress($author->email)) {
						$author->email = JHtml::_('email.cloak', $author->email);
					}
				}
				else {
					$author->email = '';
				}
				$author->metakey  = $result->metakey;
				$author->metadesc = $result->metadesc;
				$author->metadata = $result->metadata;					
			} else {
				$author->link = $author->description = $author->image = '';
			}
			
			return $author;
		}
		
		return null;
	}
	
	public function getAuthOptions ()
	{
		$filter = JFilterInput::getInstance();
		$this->setState('location', $filter->clean(JRoute::_(AuthorListHelperRoute::getAuthorRoute(0))));

		$authorsModel=JModel::getInstance('Authors', 'AuthorListModel', array('ignore_request' => true));
		$authorsModel->setState('list.ordering', 'a.ordering');
		$authorsModel->setState('list.direction', 'ASC');   
    	$authorsModel->setState('filter.published', 1);    
		$options = $authorsModel->getItems();
		$count=count($options);
		if ($this->getState('params')->get('show_author_select') && $options > 0) {
			$filter = JFilterInput::getInstance();
			foreach($options as &$option)
			{	
				$temp = clone ($this->getState('params'));
				$registry = new JRegistry();
				$registry->loadJSON($option->params);
				$temp->merge($registry);
				if ($temp->get('show_author_name') == 1) {
					$option->text = $option->username;	
				} else {
					$option->text = $option->name;
				}
				$option->value = $filter->clean(JRoute::_(AuthorListHelperRoute::getAuthorRoute($option->slug)));
			}
			array_unshift($options, JHtml::_('select.option', $filter->clean(JRoute::_(AuthorListHelperRoute::getAuthorRoute(0))), JText::_('COM_AUTHORLIST_OPTION_SELECT_AUTHOR')));
			
			return $options;
		}
		
		return null;
	}

	public function getPagination()
	{
		if (empty($this->_pagination)) {
			return null;
		}
		return $this->_pagination;
	}

}
