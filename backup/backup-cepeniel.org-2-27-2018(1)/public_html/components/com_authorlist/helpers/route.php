<?php
/*------------------------------------------------------------------------
# com_authorlist - Author List
# ------------------------------------------------------------------------
# author    Joomla!Vargas
# copyright Copyright (C) 2012 joomla.vargas.co.cr. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://joomla.vargas.co.cr
# Technical Support:  Forum - http://joomla.vargas.co.cr/forum
-------------------------------------------------------------------------*/

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.helper');

abstract class AuthorListHelperRoute
{
	protected static $lookup;

	public static function getAuthorRoute($id)
	{
		$needles = array(
			'author'  => array((int) $id),
			'authors'  => array()
		);
		
		$link = 'index.php?option=com_authorlist&view=author&id='. $id;
		
		if ($item = self::_findItem($needles)) {
			$link .= '&Itemid='.$item;
		}
		elseif ($item = self::_findItem(array('author'=>array(0)))) {
			$link .= '&Itemid='.$item;
		}

		return $link;
	}

	protected static function _findItem($needles = null)
	{
		$app		= JFactory::getApplication();
		$menus		= $app->getMenu('site');

		if (self::$lookup === null)
		{
			self::$lookup = array();

			$component	= JComponentHelper::getComponent('com_authorlist');
			$items		= $menus->getItems('component_id', $component->id);
			if ($items) {
				foreach ($items as $item)
				{
					if (isset($item->query) && isset($item->query['view']))
					{
						$view = $item->query['view'];
						if ($view === 'authors') {
							self::$lookup[$view] = $item->id;
						}
						if (!isset(self::$lookup[$view])) {
							self::$lookup[$view] = array();
						}
						if (isset($item->query['id'])) {
							self::$lookup[$view][$item->query['id']] = $item->id;
						}
					}
				}
			}
		}

		if ($needles)
		{
			foreach ($needles as $view => $ids)
			{
				if (isset(self::$lookup[$view]))
				{
					if (count($ids)) {
						foreach($ids as $id)
						{
							if (isset(self::$lookup[$view][(int)$id])) {
								return self::$lookup[$view][(int)$id];
							}
						}
					} else {
						return self::$lookup[$view];	
					}
				}
			}
		}
		else
		{
			$active = $menus->getActive();
			if ($active) {
				return $active->id;
			}
		}

		return null;
	}
}
