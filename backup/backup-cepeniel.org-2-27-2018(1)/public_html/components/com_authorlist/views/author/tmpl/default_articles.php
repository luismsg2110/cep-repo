<?php
/*------------------------------------------------------------------------
# com_authorlist - Author List
# ------------------------------------------------------------------------
# author    Joomla!Vargas
# copyright Copyright (C) 2012 joomla.vargas.co.cr. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://joomla.vargas.co.cr
# Technical Support:  Forum - http://joomla.vargas.co.cr/forum
-------------------------------------------------------------------------*/

// No direct access
defined('_JEXEC') or die;

//JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::core();

// Create some shortcuts.
$params		= &$this->item->params;
$n			= count($this->items);
$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
$cols       = 1;
?>
<?php if (empty($this->items)) : ?>

	<?php if ($this->params->get('show_no_articles',1)) : ?>
	<p><?php echo JText::_('COM_AUTHORLIST_NO_ARTICLES'); ?></p>
	<?php endif; ?>

<?php else : ?>

<form action="<?php echo JFilterOutput::ampReplace(JFactory::getURI()->toString()); ?>" method="post" name="adminForm" id="adminForm">
	<?php if ($this->params->get('show_headings') || $this->params->get('filter_field') || $this->params->get('show_pagination_limit') || $this->params->get('show_author_select')) :?>
	<fieldset class="filters">
		<?php if ($this->params->get('filter_field')) :?>	
		<div class="filter-search" style="float:left;">
			<label class="filter-search-lbl" for="filter-search"><?php echo JText::_('COM_AUTHORLIST_TITLE_FILTER_LABEL').'&#160;'; ?></label>
			<input type="text" name="filter-search" id="filter-search" value="<?php echo $this->escape($this->state->get('list.filter')); ?>" class="inputbox" onchange="document.adminForm.submit();" title="<?php echo JText::_('COM_AUTHORLIST_FILTER_SEARCH_DESC'); ?>" />
		</div>
		<?php endif; ?>

		<?php if ($this->params->get('show_pagination_limit')) : ?>
		<div class="display-limit" style="float:right;">
			<?php echo JText::_('JGLOBAL_DISPLAY_NUM'); ?>&#160;
			<?php echo $this->pagination->getLimitBox(); ?>
		</div>
		<?php endif; ?>
		
	<!-- @TODO add hidden inputs -->
		<input type="hidden" name="filter_order" value="" />
		<input type="hidden" name="filter_order_Dir" value="" />
		<input type="hidden" name="limitstart" value="" />
	</fieldset>
	<?php endif; ?>  
    <table class="authorlist-table<?php echo $this->pageclass_sfx;?>">
        <?php if ($this->params->get('show_headings')) :?>
        <thead>
            <tr>
                <th class="list-title" id="tableOrdering">
                    <?php  echo JHtml::_('grid.sort', 'JGLOBAL_TITLE', 'a.title', $listDirn, $listOrder) ; ?>
                </th>

                <?php if ($date = $this->params->get('list_show_date')) : $cols++; ?>
                <th class="list-date" id="tableOrdering2">
                    <?php echo JHtml::_('grid.sort', 'COM_AUTHORLIST_'.$date.'_DATE', 'a.created', $listDirn, $listOrder); ?>
                </th>
                <?php endif; ?>

                <?php if ($this->params->get('list_show_category',0)) : $cols++; ?>
                <th class="list-category" id="tableOrdering3">
                    <?php echo JHtml::_('grid.sort', 'JCATEGORY', 'category_title', $listDirn, $listOrder); ?>
                </th>
                <?php endif; ?>

                <?php if ($this->params->get('list_show_hits',1)) : $cols++; ?>
                <th class="list-hits" id="tableOrdering4">
                    <?php echo JHtml::_('grid.sort', 'JGLOBAL_HITS', 'a.hits', $listDirn, $listOrder); ?>
                </th>
                <?php endif; ?>

                <?php if ($this->params->get('list_show_rating',0)) : $cols++; ?>
                <th class="list-votes" id="tableOrdering5">
                    <?php echo JHtml::_('grid.sort', 'COM_AUTHORLIST_RATING', 'rating', $listDirn, $listOrder); ?>
                </th>
                <?php endif; ?>
            </tr>
        </thead>
        <?php endif; ?>

        <tbody>

        <?php foreach ($this->items as $i => $article) : ?>
            <?php if ($this->items[$i]->state == 0) : ?>
                <tr class="system-unpublished authorlist-row<?php echo $i % 2; ?>">
            <?php else: ?>
                <tr class="authorlist-row<?php echo $i % 2; ?>" >
            <?php endif; ?>
                <?php if (in_array($article->access, $this->user->getAuthorisedViewLevels())) : ?>

                    <td class="list-title">
                        <a href="<?php echo JRoute::_(ContentHelperRoute::getArticleRoute($article->slug, $article->catid)); ?>">
                            <?php echo $this->escape($article->title); ?></a>
                    </td>

                    <?php if ($this->params->get('list_show_date')) : ?>
                    <td class="list-date">
                        <?php echo JHtml::_('date',$article->displayDate, $this->escape(
                        $this->params->get('date_format', JText::_('DATE_FORMAT_LC3')))); ?>
                    </td>
                    <?php endif; ?>

                    <?php if ($this->params->get('list_show_category',0)) : ?>
                    <td class="list-category">
                        <?php $category =  $article->category_title ?>
                        <?php //$author = ($article->created_by_alias ? $article->created_by_alias : $author);?>

                        <?php if ($this->params->get('link_category') == true):?>
                            <?php echo JHtml::_(
                                    'link',
                                    JRoute::_(ContentHelperRoute::getCategoryRoute($article->catid)),
                                    $category
                            ); ?>

                        <?php else :?>
                            <?php echo $category; ?>
                        <?php endif; ?>
                    </td>
                    <?php endif; ?>

                    <?php if ($this->params->get('list_show_hits',1)) : ?>
                    <td class="list-hits">
                        <?php echo $article->hits; ?>
                    </td>
                    <?php endif; ?>

                    <?php if ($this->params->get('list_show_rating',0)) : ?>
                    <td class="list-votes" valign="middle">
                        <?php echo $article->rating * 20; ?>% <small>(<?php echo (int)$article->rating_count; ?> <?php echo ($article->rating_count == 1 ? JText::_('COM_AUTHORLIST_VOTE') : JText::_('COM_AUTHORLIST_VOTES')); ?>)</small> 
                    </td>
                    <?php endif; ?>

                <?php else : // Show unauth links. ?>
                    <td colspan="<?php echo $cols; ?>">
                        <?php
                            echo $this->escape($article->title).' : ';
                            $menu		= JFactory::getApplication()->getMenu();
                            $active		= $menu->getActive();
                            $itemId		= $active->id;
                            $link = JRoute::_('index.php?option=com_users&view=login&Itemid='.$itemId);
                            $returnURL = JRoute::_(ContentHelperRoute::getArticleRoute($article->slug));
                            $fullURL = new JURI($link);
                            $fullURL->setVar('return', base64_encode($returnURL));
                        ?>
                        <a href="<?php echo $fullURL; ?>" class="register">
                            <?php echo JText::_( 'COM_AUTHORLIST_REGISTER_TO_READ_MORE' ); ?></a>
                    </td>
                <?php endif; ?>
                </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <?php if (($this->params->def('show_pagination', 2) == 1  || ($this->params->get('show_pagination') == 2)) && ($this->pagination->get('pages.total') > 1)) : ?>
    <div class="pagination">

        <?php if ($this->params->def('show_pagination_results', 1)) : ?>
            <p class="counter">
                <?php echo $this->pagination->getPagesCounter(); ?>
            </p>
        <?php endif; ?>

        <?php echo $this->pagination->getPagesLinks(); ?>
    </div>
    <?php endif; ?>
</form>
<?php endif; ?>
