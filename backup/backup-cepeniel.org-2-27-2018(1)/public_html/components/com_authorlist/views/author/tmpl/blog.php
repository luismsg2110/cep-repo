<?php
/*------------------------------------------------------------------------
# com_authorlist - Author List
# ------------------------------------------------------------------------
# author    Joomla!Vargas
# copyright Copyright (C) 2012 joomla.vargas.co.cr. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://joomla.vargas.co.cr
# Technical Support:  Forum - http://joomla.vargas.co.cr/forum
-------------------------------------------------------------------------*/

// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers');

?>
<div class="blog<?php echo $this->pageclass_sfx;?>">
<?php if ($this->params->get('show_author_image') && $this->author && $this->author->image) : ?>
    <img src="<?php echo $this->author->image;?>" style="float:left; margin-right:10px;" alt="" />
<?php endif; ?>

<?php if ($this->params->get('show_page_heading', 1)) : ?>
    <h1>
        <?php echo $this->escape($this->params->get('page_heading')); ?>
    </h1>
<?php endif; ?>

<?php if ( $this->author ) : ?>

    <?php if ($this->params->get('show_author_name', 1)) : ?>
        <h2>
            <?php echo $this->author->displayName;?>
        </h2>
    <?php endif; ?>
    
	<?php if ($this->params->get('show_email', 0)) : ?>
    <span class="authorlist_email">
        <?php echo $this->author->email;?>
    </span>
    <?php endif; ?>
    
    <?php if ($this->params->get('show_author_description') && $this->author->description) : ?>
        <?php echo $this->author->description;?>
    <?php endif; ?>
    
<?php endif; ?>

<?php if ($this->params->get('show_author_select')) :?>	
    <form action="#" method="get" name="selectForm" id="selectForm">
        <label class="filter-author-lbl" for="filter-author"><?php echo JText::_('COM_AUTHORLIST_AUTHOR_FILTER_LABEL').'&#160;'; ?></label>
            <?php echo JHtml::_('select.genericlist',  $this->authors, 'id', 'class="inputbox" onchange="document.location.href = this.value"', 'link', 'name', $this->state->get('location') );?>
    </form>        
<?php endif; ?>

<?php $leadingcount=0 ; ?>
<?php if (!empty($this->lead_items)) : ?>
<div class="items-leading">
	<?php foreach ($this->lead_items as &$item) : ?>
		<div class="leading-<?php echo $leadingcount; ?><?php echo $item->state == 0 ? ' system-unpublished' : null; ?>">
			<?php
				$this->item = &$item;
				echo $this->loadTemplate('item');
			?>
		</div>
		<?php
			$leadingcount++;
		?>
	<?php endforeach; ?>
</div>
<?php endif; ?>
<?php
	$introcount=(count($this->intro_items));
	$counter=0;
?>
<?php if (!empty($this->intro_items)) : ?>

	<?php foreach ($this->intro_items as $key => &$item) : ?>
	<?php
		$key= ($key-$leadingcount)+1;
		$rowcount=( ((int)$key-1) %	(int) $this->columns) +1;
		$row = $counter / $this->columns ;

		if ($rowcount==1) : ?>
	<div class="items-row cols-<?php echo (int) $this->columns;?> <?php echo 'row-'.$row ; ?>">
	<?php endif; ?>
	<div class="item column-<?php echo $rowcount;?><?php echo $item->state == 0 ? ' system-unpublished' : null; ?>">
		<?php
			$this->item = &$item;
			echo $this->loadTemplate('item');
		?>
	</div>
	<?php $counter++; ?>
	<?php if (($rowcount == $this->columns) or ($counter ==$introcount)): ?>
				<span class="row-separator"></span>
				</div>

			<?php endif; ?>
	<?php endforeach; ?>


<?php endif; ?>

<?php if (!empty($this->link_items)) : ?>

	<?php echo $this->loadTemplate('links'); ?>

<?php endif; ?>

<?php if (($this->params->def('show_pagination', 2) == 1  || ($this->params->get('show_pagination') == 2)) && ($this->pagination->get('pages.total') > 1)) : ?>
    <div class="pagination">
        <?php // if ($this->params->def('show_pagination_results', 1)) : ?>
        <p class="counter">
                <?php echo $this->pagination->getPagesCounter(); ?>
        </p>

        <?php //endif; ?>
        <?php echo $this->pagination->getPagesLinks(); ?>
    </div>
<?php  endif; ?>

</div>
