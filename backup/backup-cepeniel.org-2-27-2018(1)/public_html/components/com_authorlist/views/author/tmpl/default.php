<?php
/*------------------------------------------------------------------------
# com_authorlist - Author List
# ------------------------------------------------------------------------
# author    Joomla!Vargas
# copyright Copyright (C) 2012 joomla.vargas.co.cr. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://joomla.vargas.co.cr
# Technical Support:  Forum - http://joomla.vargas.co.cr/forum
-------------------------------------------------------------------------*/

// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers');

?>
<div class="authorlist<?php echo $this->pageclass_sfx;?>">
    
	<?php if ($this->params->get('show_author_image') && $this->author && $this->author->image) : ?>
		<img src="<?php echo $this->author->image;?>" style="float:left; margin-right:10px;" alt="" />
	<?php endif; ?>
    
	<?php if ($this->params->get('show_page_heading', 1)) : ?>
	<h1>
		<?php echo $this->escape($this->params->get('page_heading')); ?>
	</h1>
	<?php endif; ?>
    
    <?php if ( $this->author ) : ?>

		<?php if ($this->params->get('show_author_name', 1)) : ?>
        <h2>
            <?php echo $this->author->displayName;?>
        </h2>
        <?php endif; ?>
        
        <?php if ($this->author->email) : ?>
            <span class="authorlist_email">
                <?php echo $this->author->email;?>
            </span>
        <?php endif; ?>
        
        <?php if ($this->params->get('show_author_description') && $this->author->description) : ?>
            <?php echo $this->author->description;?>
        <?php endif; ?>
        
    <?php endif; ?>
    
	<?php if ($this->params->get('show_author_select')) :?>	
	<form action="#" method="get" name="selectForm" id="selectForm">
        <label class="filter-author-lbl" for="filter-author"><?php echo JText::_('COM_AUTHORLIST_AUTHOR_FILTER_LABEL').'&#160;'; ?></label>
			<?php echo JHtml::_('select.genericlist',  $this->authOptions, 'id', 'class="inputbox" onchange="document.location.href = this.value"', 'value', 'text', $this->state->get('location') );?>
    </form>        
	<?php endif; ?>
        
	<div class="author-items">
		<?php echo $this->loadTemplate('articles'); ?>
	</div>

</div>
