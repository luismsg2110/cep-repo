<?php
/**
 * @package		GAnalytics
 * @author		Digital Peak http://www.digital-peak.com
 * @copyright	Copyright (C) 2012 Digital Peak, All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL
 */

defined('_JEXEC') or die();

jimport( 'joomla.application.component.view');

class GAnalyticsViewData extends JView {

	function display($tpl = null) {
		$user = JFactory::getUser();
		$access = 0;
		$params = null;

		if(JRequest::getVar('source', 'component') == 'component'){
			$menu = JSite::getMenu()->getActive();
			$params = $menu->params;
			$params->set('mode', $menu->query['layout'] == 'image' ? 'image' : 'list');
			$access = $menu->access;
		}
		if(JRequest::getVar('source', 'component') == 'module'){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('m.*');
			$query->from('#__modules AS m');
			$query->where('id = '.JRequest::getInt('moduleid'));
			$db->setQuery($query);
			$module = $db->loadObject();

			if($module != null){
				$params = new JRegistry($module->params);
				$params->set('mode', $params->get('mode', 'list'));
				$access = $module->access;
			}
		}
		if($user->authorise('core.admin') || in_array((int) $access, $user->getAuthorisedViewLevels())){
			$this->getModel()->setState('params', $params);
			$this->assignRef('params', $params);
			$this->assignRef('data', $this->get('StatsData'));
		}else{
			JError::raiseWarning(0, 'JERROR_ALERTNOAUTHOR');
		}
		parent::display($tpl);
	}
}