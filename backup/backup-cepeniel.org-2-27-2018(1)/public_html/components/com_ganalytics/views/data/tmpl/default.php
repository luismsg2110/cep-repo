<?php
/**
 * @package		GAnalytics
 * @author		Digital Peak http://www.digital-peak.com
 * @copyright	Copyright (C) 2012 Digital Peak, All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL
 */

defined('_JEXEC') or die();

require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ganalytics'.DS.'libraries'.DS.'gviz'.DS.'gviz_api.php');

$data = isset($this->data) ? $this->data:null;

$dataTable = new GvizDataTable(JRequest::getVar('tqx', ''));
if($data === null) {
	$message = array_shift(JFactory::getApplication()->getMessageQueue());
	if(key_exists('message', $message)){
		$message = $message['message'];
	} else {
		$message = print_r($message, true);
	}
	$dataTable->addError('invalid_request', JText::_('COM_GANALYTICS_JSONDFEED_VIEW_ERROR'), $message);
} else {
	$dataTable->addWarning('other', $this->params->get('mode', 'list'));
	if(count($data) > 0){
		$dateRange = JRequest::getVar('dateRange', 'day');
		//$dateFormat = $params->get('dateFormat', '%d.%m.%Y');
		$dimensions = $data[0]->getDimensions();
		$metrics = $data[0]->getMetrics();

		foreach ($dimensions as $dimension => $value) {
			$type = 'string';
			if(stripos($dimension, 'date') !== false)
			$type = 'date';

			$dataTable->addColumn('ga:'.$dimension, GAnalyticsHelper::translate('ga:'.$dimension), $type);
		}
		foreach ($metrics as $metric => $value) {
			$dataTable->addColumn('ga:'.$metric, GAnalyticsHelper::translate('ga:'.$metric), 'number');
		}

		$counter = -1;
		foreach($data as $item){
			$counter++;
			$rowId = -1;
			foreach($item->getDimensions() as $dimension => $value) {
				$formatted = $value;
				if(stripos($dimension, 'date') !== false){
					$value = mktime(0, 0, 0, substr($value, 4, 2), substr($value, 6, 2), substr($value, 0, 4));
					$formatted = strftime(GAnalyticsHelper::getComponentParameter('dateFormat', '%d.%m.%Y'), $value);
					if($dateRange == 'week' && strftime('%u', $value) > 1 && $counter > 0 && $counter < count($data)-1){
						break;
					}
					if($dateRange == 'month' && strftime('%e', $value) > 1 && $counter > 0 && $counter < count($data)-1){
						break;
					}
				} else {
					$value = addslashes($value);
					$formatted = $value;
					if(strlen($value) > 70)
					$formatted = substr($value, 0, 70).'...';
				}
				$rowId = $dataTable->newRow();
				$property = '';
				if(stripos($dimension, 'country') !== false){
					$flag = GAnalyticsHelper::convertCountryNameToISO($value);
					if(!empty($flag))
					$property = "style: 'background-image:url(\"".JURI::root().'media/com_ganalytics/images/flags/'.strtolower($flag).".gif\"); background-repeat: no-repeat;background-position: 5px 4px;padding-left:30px;'";
				}
				$dataTable->addCell($rowId, 'ga:'.$dimension, JText::_($value), JText::_($formatted), $property);
			}
			if($rowId == -1) continue;
			foreach($item->getMetrics() as $metric => $value) {
				$dataTable->addCell($rowId, 'ga:'.$metric, JText::_($value));
			}
		}
	}
}
echo $dataTable->toJsonResponse();