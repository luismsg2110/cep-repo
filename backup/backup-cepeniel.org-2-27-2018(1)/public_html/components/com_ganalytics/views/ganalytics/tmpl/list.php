<?php
/**
 * @package		GAnalytics
 * @author		Digital Peak http://www.digital-peak.com
 * @copyright	Copyright (C) 2012 Digital Peak, All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL
 */

defined('_JEXEC') or die();

$params = $this->params;
$document = &JFactory::getDocument();
$document->addScript('http://www.google.com/jsapi');
if(GAnalyticsHelper::isPROMode()){
	$document->addScript(JURI::base().'administrator/components/com_ganalytics/libraries/jquery/ganalytics/chart.js');
}else{
	$document->addScript(JURI::base().'administrator/components/com_ganalytics/libraries/jquery/ganalytics/list.js');
}
$document->addStyleSheet(JURI::base().'components/com_ganalytics/views/ganalytics/tmpl/ganalytics.css');
$document->addScript(JURI::base().'components/com_ganalytics/views/ganalytics/tmpl/ganalytics.js');

$showDateSelection = $params->get('showDateSelection', 'yes') == 'yes';
$profile = $this->profile;

$title = $this->titleFormat;
$title = str_replace("{accountname}", $profile->accountName, $title);
$title = str_replace("{profilename}", $profile->profileName, $title);
echo $title;
?>
<?php if($showDateSelection) {?>
<table class="ganalytics-table">
	<tr>
		<td style="padding-right:20px">
		<?php
		echo JText::_('COM_GANALYTICS_CHART_VIEW_SELECT_DATE_FROM');
		echo JHtml::_('calendar', strftime('%Y-%m-%d', $this->startDate), 'date_from', 'date_from', '%Y-%m-%d', array('size' => 10));
		?>
		</td>
		<td style="padding-right:20px">
		<?php
		echo JText::_('COM_GANALYTICS_CHART_VIEW_SELECT_DATE_TO');
		echo JHtml::_('calendar', strftime('%Y-%m-%d', $this->endDate), 'date_to', 'date_to', '%Y-%m-%d', array('size' => 10));
		?>
		</td>
		<td style="padding-right:20px"><button onclick="showCharts();"><?php echo JText::_('COM_GANALYTICS_CHART_VIEW_BUTTON_UPDATE');?></button></td>
	</tr>
</table>
<?php
}

$scriptCode = "jQuery(document).ready(function(){\n";
if($params->get('showVisitors', 'yes') == 'yes'){?>
<fieldset class="date-range-container"><legend><?php echo JText::_('COM_GANALYTICS_ga:visits');?></legend>
	<div class="date-range-toolbar">
		<img src="media/com_ganalytics/images/dayrange/month-disabled-32.png" class="date-range-button date-range-month"
			alt="<?php echo JText::_('COM_GANALYTICS_CHART_VIEW_IMAGE_DATE_RANGE_MONTH');?>"
			title="<?php echo JText::_('COM_GANALYTICS_CHART_VIEW_IMAGE_DATE_RANGE_MONTH');?>" />
		<img src="media/com_ganalytics/images/dayrange/week-disabled-32.png" class="date-range-button date-range-week"
			alt="<?php echo JText::_('COM_GANALYTICS_CHART_VIEW_IMAGE_DATE_RANGE_WEEK');?>"
			title="<?php echo JText::_('COM_GANALYTICS_CHART_VIEW_IMAGE_DATE_RANGE_WEEK');?>" />
		<img src="media/com_ganalytics/images/dayrange/day-32.png" class="date-range-button date-range-day"
			alt="<?php echo JText::_('COM_GANALYTICS_CHART_VIEW_IMAGE_DATE_RANGE_DAY');?>"
			title="<?php echo JText::_('COM_GANALYTICS_CHART_VIEW_IMAGE_DATE_RANGE_DAY');?>" />
	</div>

	<div id="ga-visitors-chart-<?php echo $profile->id;?>" class="ga-chart"></div>
</fieldset>
	<?php
	$scriptCode .= "jQuery('#ga-visitors-chart-".$profile->id."').gaChart({
		chartVisDivID: 'gaChartDiv".$profile->id."',
		url: 'index.php?option=com_ganalytics&view=data&source=component&format=raw&type=visitor&Itemid=".JSite::getMenu()->getActive()->id."',
		start: '".strftime("%Y-%m-%d", $this->startDate)."',
		end: '".strftime("%Y-%m-%d", $this->endDate)."',
		pathPrefix: '".JURI::root()."'
	});\n";
}

?>
<fieldset class="date-range-container">
	<legend><?php echo GAnalyticsHelper::translate($this->dimensions).' -- '.GAnalyticsHelper::translate($this->metrics);?></legend>
	<?php
	if(stripos($this->dimensions, 'ga:date') !== false){?>
	<div class="date-range-toolbar">
		<img src="media/com_ganalytics/images/dayrange/month-disabled-32.png" class="date-range-button date-range-month"
			alt="<?php echo JText::_('COM_GANALYTICS_CHART_VIEW_IMAGE_DATE_RANGE_MONTH');?>"
			title="<?php echo JText::_('COM_GANALYTICS_CHART_VIEW_IMAGE_DATE_RANGE_MONTH');?>" />
		<img src="media/com_ganalytics/images/dayrange/week-disabled-32.png" class="date-range-button date-range-week"
			alt="<?php echo JText::_('COM_GANALYTICS_CHART_VIEW_IMAGE_DATE_RANGE_WEEK');?>"
			title="<?php echo JText::_('COM_GANALYTICS_CHART_VIEW_IMAGE_DATE_RANGE_WEEK');?>" />
		<img src="media/com_ganalytics/images/dayrange/day-32.png" class="date-range-button date-range-day"
			alt="<?php echo JText::_('COM_GANALYTICS_CHART_VIEW_IMAGE_DATE_RANGE_DAY');?>"
			title="<?php echo JText::_('COM_GANALYTICS_CHART_VIEW_IMAGE_DATE_RANGE_DAY');?>" />
	</div>
	<?php } ?>
	<div id="ga-main-chart-<?php echo $profile->id;?>" class="ga-chart"></div>
</fieldset>
<?php
$scriptCode .= "jQuery('#ga-main-chart-".$profile->id."').gaChart({
	chartDivID: 'gaChartDiv".$profile->id."',
	url: 'index.php?option=com_ganalytics&view=data&source=component&format=raw&Itemid=".JSite::getMenu()->getActive()->id."',
	start: '".strftime("%Y-%m-%d", $this->startDate)."',
	end: '".strftime("%Y-%m-%d", $this->endDate)."',
	pathPrefix: '".JURI::root()."'
});\n";

$scriptCode .= "showCharts();});\n";
$document->addScriptDeclaration($scriptCode);

if(!GAnalyticsHelper::isProMode()){
	echo "<div style=\"text-align:center;margin-top:10px\" id=\"ganalytics_powered\"><a href=\"http://g4j.laoneo.net\" target=\"_blank\">Powered by GAnalytics</a></div>\n";
}