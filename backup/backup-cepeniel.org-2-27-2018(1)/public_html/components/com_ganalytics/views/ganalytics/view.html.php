<?php
/**
 * @package		GAnalytics
 * @author		Digital Peak http://www.digital-peak.com
 * @copyright	Copyright (C) 2012 Digital Peak, All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL
 */

defined('_JEXEC') or die();

jimport( 'joomla.application.component.view');

class GAnalyticsViewGAnalytics extends JView {

	public function display($tpl = null) {
		$mainframe = &JFactory::getApplication();
		$params = &$mainframe->getParams();

		$profile = $this->get('SelectedProfile');
		$this->assignRef('profile', $profile);

		$startDate = time();
		$endDate = time();
		if($params->get('daterange', 'month') == 'advanced'){
			$tmp = $params->get('advancedDateRange', null);
			if(!empty($tmp)){
				$startDate = strtotime($tmp);
			} else {
				$tmp = $params->get('startdate', null);
				if(!empty($tmp)){
					sscanf($tmp,"%u-%u-%u", $year, $month, $day);
					$startDate = mktime(0, 0, 0, $month, $day, $year);
				}
				$tmp = $params->get('enddate', null);
				if(!empty($tmp)){
					sscanf($tmp,"%u-%u-%u", $year, $month, $day);
					$endDate = mktime(0, 0, 0, $month, $day, $year);
				}
			}
		}else{
			$range = '';
			switch ($params->get('daterange', 'month')) {
				case 'day':
					$range = '-1 day';
					break;
				case 'week':
					$range = '-1 week';
					break;
				case 'month':
					$range = '-1 month';
					break;
				case 'year':
					$range = '-1 year';
					break;
			}
			$startDate = strtotime($range);
			$endDate = time();
		}

		$dimensions = '';
		$metrics = '';
		$sort = '';
		if($params->get('type', 'visits') == 'advanced'){
			$dimensions = $params->get('dimensions', 'ga:date');
			$metrics = $params->get('metrics', 'ga:visits');
			$sort = $params->get('sort', '');
		}else{
			switch ($params->get('type', 'visitsbytraffic')) {
				case 'visits':
					$dimensions = 'ga:date';
					$metrics = 'ga:visits';
					break;
				case 'visitsbytraffic':
					$dimensions = 'ga:source';
					$metrics = 'ga:visits';
					$sort = '-ga:visits';
					break;
				case 'visitsbybrowser':
					$dimensions = 'ga:browser';
					$metrics = 'ga:visits';
					$sort = '-ga:visits';
					break;
				case 'visitsbycountry':
					$dimensions = 'ga:country';
					$metrics = 'ga:visits';
					$sort = '-ga:visits';
					break;
				case 'timeonsite':
					$dimensions = 'ga:region';
					$metrics = 'ga:timeOnSite';
					$sort = '-ga:timeOnSite';
					break;
				case 'toppages':
					$dimensions = 'ga:pagePath';
					$metrics = 'ga:pageviews';
					$sort = '-ga:pageviews';
					break;
			}
		}
		$max = $params->get('max', 1000);

		$this->assignRef('startDate', $startDate);
		$this->assignRef('endDate', $endDate);
		$this->assignRef('dimensions', $dimensions);
		$this->assignRef('metrics', $metrics);
		$this->assignRef('sort', $sort);
		$this->assignRef('max', $max);
		$this->assignRef('titleFormat', $params->get('titleFormat', ''));
		$this->assignRef('dateFormat', $params->get('dateFormat', '%d.%m.%Y'));
		$this->assignRef('params', $params );
		parent::display($tpl);
	}
}