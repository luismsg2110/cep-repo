<?php
/**
 * @package		GAnalytics
 * @author		Digital Peak http://www.digital-peak.com
 * @copyright	Copyright (C) 2012 Digital Peak, All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL
 */

defined('_JEXEC') or die();

jimport( 'joomla.application.component.model' );

class GAnalyticsModelData extends JModel {

	public function getStatsData() {
		$params = $this->getState('params');

		$profile = JTable::getInstance('Profile', 'GAnalyticsTable');
		$profile->load($params->get('accountids', 0));

		$startDate = time();
		$endDate = time();
		if($params->get('daterange', 'month') == 'advanced'){
			$tmp = $params->get('advancedDateRange', null);
			if(!empty($tmp)){
				$startDate = strtotime($tmp);
			} else {
				$tmp = $params->get('startdate', null);
				if(!empty($tmp)){
					sscanf($tmp,"%u-%u-%u", $year, $month, $day);
					$startDate = mktime(0, 0, 0, $month, $day, $year);
				}
				$tmp = $params->get('enddate', null);
				if(!empty($tmp)){
					sscanf($tmp,"%u-%u-%u", $year, $month, $day);
					$endDate = mktime(0, 0, 0, $month, $day, $year);
				}
			}
		}else{
			$range = '';
			switch ($params->get('daterange', 'month')) {
				case 'day':
					$range = '-1 day';
					break;
				case 'week':
					$range = '-1 week';
					break;
				case 'month':
					$range = '-1 month';
					break;
				case 'year':
					$range = '-1 year';
					break;
			}
			$startDate = strtotime($range);
			$endDate = time();
		}

		$dimensions = '';
		$metrics = '';
		$sort = '';
		if($params->get('type', 'visits') == 'advanced'){
			$dimensions = $params->get('dimensions', 'ga:date');
			$metrics = $params->get('metrics', 'ga:visits');
			$sort = $params->get('sort', '');
		}else{
			switch ($params->get('type', 'visitsbytraffic')) {
				case 'visits':
					$dimensions = 'ga:date';
					$metrics = 'ga:visits';
					$sort = 'ga:date';
					break;
				case 'visitsbytraffic':
					$dimensions = 'ga:source';
					$metrics = 'ga:visits';
					$sort = '-ga:visits';
					break;
				case 'visitsbybrowser':
					$dimensions = 'ga:browser';
					$metrics = 'ga:visits';
					$sort = '-ga:visits';
					break;
				case 'visitsbycountry':
					$dimensions = 'ga:country';
					$metrics = 'ga:visits';
					$sort = '-ga:visits';
					break;
				case 'timeonsite':
					$dimensions = 'ga:region';
					$metrics = 'ga:timeOnSite';
					$sort = '-ga:timeOnSite';
					break;
				case 'toppages':
					$dimensions = 'ga:pagePath';
					$metrics = 'ga:pageviews';
					$sort = '-ga:pageviews';
					break;
			}
		}
		$max = $params->get('max', 1000);
		if(!empty($sort)){
			$sort = array(str_replace('ga:', '', $sort));
		} else {
			$sort = null;
		}

		if(JRequest::getVar('type', null) == 'visitor'){
			$dimensions = 'ga:date';
			$metrics = 'ga:visits';
			$sort = array('date');
			$max = 1000;
		}

		$startDate = new JDate($startDate);
		$endDate = new JDate($endDate);

		if(JRequest::getVar('start', null) != null){
			sscanf(JRequest::getVar('start', null), "%u-%u-%u", $year, $month, $day);
			$startDate = new JDate();
			$startDate->setDate($year, $month, $day);
			$startDate->setTime(0, 0);
		}
		if(JRequest::getVar('end', null) != null){
			sscanf(JRequest::getVar('end', null), "%u-%u-%u", $year, $month, $day);
			$endDate = new JDate();
			$endDate->setDate($year, $month, $day);
			$endDate->setTime(0, 0);
		}

		return GAnalyticsDataHelper::getData($profile, array(str_replace('ga:', '', $dimensions)), array(str_replace('ga:', '', $metrics)), $startDate, $endDate, $sort, $max, null);
	}
}