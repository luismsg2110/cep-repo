<?php
/* *************************************************************************************
Title          PrayerCenter Component for Joomla
Author         Mike Leeper
License        This program is free software: you can redistribute it and/or modify
               it under the terms of the GNU General Public License as published by
               the Free Software Foundation, either version 3 of the License, or
               (at your option) any later version.
               This program is distributed in the hope that it will be useful,
               but WITHOUT ANY WARRANTY; without even the implied warranty of
               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
               GNU General Public License for more details.
               You should have received a copy of the GNU General Public License
               along with this program.  If not, see <http://www.gnu.org/licenses/>.
Copyright      2006-2012 - Mike Leeper (MLWebTechnologies) 
****************************************************************************************
No direct access*/
defined('_JEXEC') or die( 'Restricted access' );
jimport('joomla.application.component.controller');
class PrayerCenterControllerPrayerCenter extends PrayerCenterController
{
  function newreqsubmit()
  {
    global $pcConfig, $prayercenter;
 		JRequest::checkToken() or jexit( 'Invalid Token' );
    $app =& JFactory::getApplication();
    $pc_rights = $prayercenter->pc_rights;
    $mod = JRequest::getVar( 'mod', null, 'get', 'string' );
    $modtype = JRequest::getVar( 'modtype', null, 'get', 'string' );
    $returnto = JRequest::getVar( 'return', null, 'post', 'string' );;
    jimport('joomla.utilities.date');
    jimport('joomla.mail.helper');
    jimport('joomla.filter.filteroutput');
		$user =& JFactory::getUser();
    $db		=& JFactory::getDBO();
    $itemid = $prayercenter->PCgetItemid();
    $dateset = new JDate(gmdate('Y-m-d H:i:s'));
    $time = $dateset->toFormat('%H:%M:%S');
    $date = $dateset->toFormat('%Y-%m-%d');
    $session =& JFactory::getSession();
    $sessionid = $session->get('session.token');
    $newtitle = JRequest::getVar('newtitle',null,'post','string',JREQUEST_ALLOWHTML);
    $newrequest = JRequest::getVar('newrequest',null,'post','string',JREQUEST_ALLOWHTML);
    $newrequester = JRequest::getVar('newrequester',null,'post','string');
    $newrequesterid = JRequest::getVar('requesterid',null,'post','int');
    $newemail = JRequest::getVar('newemail',null,'post','string');
    $newtopic = JRequest::getVar('newtopic',null,'post','int');
    if(!empty($newemail) && JMailHelper::isEmailAddress($newemail)){
        if( !$prayercenter->PCcheckEmail($newemail) ) {
            if(isset($_GET['modtype'])){
              $prayercenter->PCRedirect($returnto, htmlentities(JText::_('USRLINVALIDDOMAIN'),ENT_COMPAT,'UTF-8'));
            } else {
            	$returnurl = JRoute::_("index.php?option=com_prayercenter&task=newreq&Itemid=".$itemid.'&return_msg='.htmlentities(JText::_('USRLINVALIDDOMAIN'),ENT_COMPAT,'UTF-8'));
              $prayercenter->PCRedirect($returnurl);
            }
          }
        if( !$prayercenter->PCcheckBlockedEmail($newemail) ) {
            if(isset($_GET['modtype'])){
              $prayercenter->PCRedirect($returnto, JText::_('USRLINVALIDEMAIL'));
            } else {
            	$returnurl = JRoute::_("index.php?option=com_prayercenter&task=newreq&Itemid=".$itemid.'&return_msg='.htmlentities(JText::_('USRLINVALIDEMAIL'),ENT_COMPAT,'UTF-8'));
              $prayercenter->PCRedirect($returnurl);
            }
          }
        }
      if(!empty($newrequest)){
      if(!$pcConfig['config_captcha_bypass_4member'] || $pcConfig['config_captcha_bypass_4member'] && $user->guest){
        $this->pcCaptchaValidate($returnto,$itemid,$modtype,'newreq');
      }
      if($prayercenter->PCspamcheck($newrequest) && $prayercenter->PCspamcheck($newrequester)){
      $newtitle = $prayercenter->PCcleanText($newtitle);
      $newrequest = $prayercenter->PCcleanText($newrequest);
      $newrequest = addslashes($newrequest);
      $newrequester = JFilterOutput::cleanText($newrequester);
      $newemail = JMailHelper::cleanAddress($newemail);
      if(!JMailHelper::isEmailAddress($newemail) && $pcConfig['config_use_admin_alert'] == 1){
        if(isset($_GET['modtype'])){
          $prayercenter->PCRedirect($returnto, htmlentities(JText::_('USRLINVALIDEMAIL'),ENT_COMPAT,'UTF-8'));
        } else {
        	$returnurl = JRoute::_('index.php?option=com_prayercenter&task=newreq&Itemid='.$itemid.'&return_msg='.htmlentities(JText::_('USRLINVALIDEMAIL'),ENT_COMPAT,'UTF-8'));
          $prayercenter->PCRedirect($returnurl);
        }
      }
      $subpraise = JRequest::getVar('subpraise',null,'post','boolean');
      $sendpriv = JRequest::getVar('sendpriv',null,'post','boolean');
      $sendto = $sendpriv; 
      if ($newrequester=='') {$newrequester= htmlentities(JText::_('USRLANONUSER'),ENT_COMPAT,'UTF-8');}
      if ($pcConfig['config_use_wordfilter'] > 0) {
        $newtitle = $prayercenter->PCbadword_replace($newtitle);
        $newrequest = $prayercenter->PCbadword_replace($newrequest);
        $newemail = $prayercenter->PCbadword_replace($newemail);
        $newrequester = $prayercenter->PCbadword_replace($newrequester);
      }
      if (($pcConfig['config_use_admin_alert'] == 0 && !$pcConfig['config_use_admin_alert'] == 1) || ($pc_rights->get('pc.publish') && $user->guest)) {
        $sql="INSERT INTO #__prayercenter (id,requesterid,requester,date,time,request,publishstate,archivestate,displaystate,sendto,email,praise,sessionid,title,topic) VALUES ('',".(int)$newrequesterid.",".$db->quote($db->getEscaped($newrequester),false).",".$db->quote($db->getEscaped($date),false).",".$db->quote($db->getEscaped($time),false).",".$db->quote($db->getEscaped($newrequest),false).",'1','0',".(int)$sendpriv.",".(int)$sendto.",".$db->quote($db->getEscaped($newemail),false).",".(int)$subpraise.",".$db->quote($db->getEscaped($sessionid),false).",".$db->quote($db->getEscaped($newtitle),false).",".(int)$newtopic.")";
        		$db->setQuery($sql);
      		if (!$db->query()) {
  					JError::raiseError( 500, $db->stderr());
      		}	
    	  $lastId = $db->insertid();
      } elseif($pcConfig['config_use_admin_alert'] > 0 || $pcConfig['config_use_admin_alert'] == 1){
        $sql="INSERT INTO #__prayercenter (id,requesterid,requester,date,time,request,publishstate,archivestate,displaystate,sendto,email,praise,sessionid,title,topic) VALUES ('',".(int)$newrequesterid.",".$db->quote($db->getEscaped($newrequester),false).",".$db->quote($db->getEscaped($date),false).",".$db->quote($db->getEscaped($time),false).",".$db->quote($db->getEscaped($newrequest),false).",'0','0',".(int)$sendpriv.",".(int)$sendto.",".$db->quote($db->getEscaped($newemail),false).",".(int)$subpraise.",".$db->quote($db->getEscaped($sessionid),false).",".$db->quote($db->getEscaped($newtitle),false).",".(int)$newtopic.")";
    		$db->setQuery($sql);
      		if (!$db->query()) {
  					JError::raiseError( 500, $db->stderr());
      		}	
      	$lastId = $db->insertid();
      }		
      // Notify Site Admin(s) and/or moderators on event of new request
      if($pcConfig['config_use_admin_alert'] > 1 && !$pc_rights->get('pc.publish')){
        if($pcConfig['config_admin_distrib_type'] == 1 || $pcConfig['config_admin_distrib_type'] == 3) {
          $prayercenter->PCadmin_email_notification($newrequest, $newrequester, $newemail, 0, $lastId, $sessionid, $sendpriv);
        	} 
        if($pcConfig['config_admin_distrib_type'] > 1 && $pcConfig['config_pms_plugin']) 
          {
          $prayercenter->PCsendPM($newrequesterid,$newrequester,$newrequest,$newemail,$sendpriv,$lastId,$sessionid,true);
          }
      } elseif($pcConfig['config_use_admin_alert'] < 2) {
        if($pcConfig['config_use_admin_alert'] == 1 && !empty($newemail))
          {
          $prayercenter->PCconfirm_notification($newrequester,$newrequest,$newemail,$lastId,$sessionid,0);
          if(isset($_GET['modtype'])){
            $prayercenter->PCRedirect($returnto, htmlentities(JText::_('PCREQSUBMITCONFIRM'),ENT_COMPAT,'UTF-8'));
          } else {
          	$returnurl = JRoute::_('index.php?option=com_prayercenter&task=view&Itemid='.$itemid.'&return_msg='.htmlentities(JText::_('PCREQSUBMITCONFIRM'),ENT_COMPAT,'UTF-8'));
            $prayercenter->PCRedirect($returnurl);
          }
        }
        if($sendpriv){
          $prayercenter->PCemail_notification($newrequest,$newrequester,$newemail,0,$sendpriv,$lastId,$sessionid);
          $prayercenter->PCemail_prayer_chain($newrequest,$newrequester);
          if($pcConfig['config_distrib_type'] > 1 && $pcConfig['config_pms_plugin']){
            $prayercenter->PCsendPM($newrequester,$newrequest,$newemail,$sendpriv);
          }
        } elseif(!$sendpriv){
          $prayercenter->PCemail_notification($newrequest,$newrequester,$newemail,0,$sendpriv,$lastId,$sessionid);
          if($pcConfig['config_distrib_type'] > 1 && $pcConfig['config_pms_plugin']){
            $prayercenter->PCsendPM($newrequesterid,$newrequester,$newrequest,$newemail,$sendpriv,$lastId,$sessionid);
          }
        }
       }
        if(isset($_GET['modtype'])){
         $prayercenter->PCRedirect($returnto, htmlentities(JText::_('PCREQSUBMIT'),ENT_COMPAT,'UTF-8'));
        } else {
        	$returnurl = JRoute::_('index.php?option=com_prayercenter&task=view&Itemid='.$itemid.'&return_msg='.htmlentities(JText::_('PCREQSUBMIT'),ENT_COMPAT,'UTF-8'));
          $prayercenter->PCRedirect($returnurl);
        }
      }
      if(stristr($_SERVER['HTTP_REFERER'],$_SERVER['HTTP_HOST'])){
        if(isset($_GET['modtype'])){
          $prayercenter->PCRedirect($returnto, JText::_('SPAMMSG'));
        } else {
        	$returnurl = JRoute::_('index.php?option=com_prayercenter&task=newreq&Itemid='.$itemid.'&return_msg='.htmlentities(JText::_('SPAMMSG'),ENT_COMPAT,'UTF-8'));
          $prayercenter->PCRedirect($returnurl);
        }
      } else {
        header( 'HTTP/1.0 403 Forbidden' );
        sleep(rand(2, 5)); // delay spammers a bit
  			JError::raiseError(403, _NOT_AUTH );
      }
     }
      if(isset($_GET['modtype'])){
        $prayercenter->PCRedirect($returnto, htmlentities(JText::_('PCFORMNC'),ENT_COMPAT,'UTF-8'));
      } else {
      	$returnurl = JRoute::_('index.php?option=com_prayercenter&task=view&Itemid='.$itemid.'&return_msg='.htmlentities(JText::_('PCFORMNC'),ENT_COMPAT,'UTF-8'));
        $prayercenter->PCRedirect($returnurl);
      }
    }
    function subscribesubmit()
    {
      global $pcConfig, $prayercenter;
  		JRequest::checkToken() or jexit( 'Invalid Token' );
      $app = JFactory::getApplication();
      $pc_rights = $prayercenter->pc_rights;
      $mod = JRequest::getVar( 'mod', null, 'get', 'string' );
      $modtype = JRequest::getVar( 'modtype', null, 'get', 'string' );
      $returnto = JRequest::getVar( 'return', null, 'post', 'string' );;
      jimport('joomla.utilities.date');
      jimport('joomla.mail.helper');
      jimport('joomla.filter.filteroutput');
      $itemid = $prayercenter->PCgetItemid();
  		$user =& JFactory::getUser();
      if(!$pcConfig['config_captcha_bypass_4member'] || $pcConfig['config_captcha_bypass_4member'] && $user->guest){
        $this->pcCaptchaValidate($returnto,$itemid,$modtype,'subscribe');
      }
      $session =& JFactory::getSession();
      $sessionid = $session->get('session.token');
      $newsubscribe = JRequest::getVar('newsubscribe',null,'post','string');
      if(!empty($newsubscribe) && JMailHelper::isEmailAddress($newsubscribe)){
        if( !$prayercenter->PCcheckEmail($newsubscribe) ) {
            if(isset($_GET['modtype'])){
              $prayercenter->PCRedirect($returnto, htmlentities(JText::_('USRLINVALIDDOMAIN'),ENT_COMPAT,'UTF-8'));
            } else {
            	$returnurl = JRoute::_('index.php?option=com_prayercenter&task=subscribe&Itemid='.$itemid.'&return_msg='.htmlentities(JText::_('USRLINVALIDDOMAIN'),ENT_COMPAT,'UTF-8'));
              $prayercenter->PCRedirect($returnurl);
            }
        }
        if( !$prayercenter->PCcheckBlockedEmail($newsubscribe) ) {
            if(isset($_GET['modtype'])){
              $prayercenter->PCRedirect($returnto, htmlentities(JText::_('USRLINVALIDEMAIL'),ENT_COMPAT,'UTF-8'));
            } else {
            	$returnurl = JRoute::_("index.php?option=com_prayercenter&task=subscribe&Itemid=".$itemid.'&return_msg='.htmlentities(JText::_('USRLINVALIDEMAIL'),ENT_COMPAT,'UTF-8'));
              $prayercenter->PCRedirect($returnurl);
            }
          }
      }
      if ($pcConfig['config_use_wordfilter'] > 0) {
        $newrequest = $prayercenter->PCbadword_replace($newrequest);
        $newsubscribe = $prayercenter->PCbadword_replace($newsubscribe);
      }
      $newsubscribe = JMailHelper::cleanAddress($newsubscribe);
      if (JMailHelper::isEmailAddress( $newsubscribe ))
      {
        $dateset = new JDate(gmdate('Y-m-d H:i:s'));
        $dateset->setOffset($app->getCfg( 'config.offset' ) + date('I'));
        $date = $dateset->toFormat('%Y-%m-%d');
        $db	=& JFactory::getDBO();
        $db->setQuery("SELECT email FROM #__prayercenter_subscribe");
        $readq = $db->loadObjectList();
        $duplicate = '0';
        foreach ($readq as $dup){
          if ($newsubscribe == $dup->email){
            $duplicate = '1';
          }
        }
        if ($duplicate != '1'){
          if ($pcConfig['config_admin_approve_subscribe'] == 0){
          $sql="INSERT INTO #__prayercenter_subscribe (id,email,date,approved,sessionid) VALUES ('',".$db->quote($db->getEscaped($newsubscribe),false).",".$db->quote($db->getEscaped($date),false).",'1',".$db->quote($db->getEscaped($sessionid),false).")";
      		$db->setQuery($sql);
      		if (!$db->query()) {
  					JError::raiseError( 500, $db->stderr());
            }
      	  $lastId = $db->insertid();
          } elseif ($pcConfig['config_admin_approve_subscribe'] > 0) {
          $sql="INSERT INTO #__prayercenter_subscribe (id,email,date,approved,sessionid) VALUES ('',".$db->quote($db->getEscaped($newsubscribe),false).",".$db->quote($db->getEscaped($date),false).",'0',".$db->quote($db->getEscaped($sessionid),false).")";
      		$db->setQuery($sql);
      		if (!$db->query()) {
  					JError::raiseError( 500, $db->stderr());
                 }
      	  $lastId = $db->insertid();
          }
          if($pcConfig['config_admin_approve_subscribe'] == 2){
            $prayercenter->PCconfirm_sub_notification($newsubscribe,$lastId,$sessionid,0);
            if(isset($_GET['modtype'])){
              $prayercenter->PCRedirect($returnto, htmlentities(JText::_('PCREQSUBMITCONFIRM'),ENT_COMPAT,'UTF-8'));
            } else {
            	$returnurl = JRoute::_('index.php?option=com_prayercenter&task=subscribe&Itemid='.$itemid.'&return_msg='.htmlentities(JText::_('PCREQSUBMITCONFIRM'),ENT_COMPAT,'UTF-8'));
              $prayercenter->PCRedirect($returnurl);
            }
          }
          if($pc_rights->get('pc.subscribe') && $pcConfig['config_admin_approve_subscribe'] == 0){
        	  $prayercenter->PCemail_subscribe($newsubscribe);
      		 }
            if(isset($_GET['modtype'])){
              $prayercenter->PCRedirect($returnto, htmlentities(JText::_('USRLENTRYACCEPTED'),ENT_COMPAT,'UTF-8'));
            } else {
            	$returnurl = JRoute::_("index.php?option=com_prayercenter&task=subscribe&Itemid=".$itemid."&return_msg=".htmlentities(JText::_('USRLENTRYACCEPTED'),ENT_COMPAT,'UTF-8'));
              $prayercenter->PCRedirect($returnurl);
            }
        } else {
          if(isset($_GET['modtype'])){
            $prayercenter->PCRedirect($returnto, htmlentities(JText::_('USRLDUPLICATEDENTRY'),ENT_COMPAT,'UTF-8'));
          } else {
          	$returnurl = JRoute::_("index.php?option=com_prayercenter&task=subscribe&Itemid=".$itemid."&return_msg=".htmlentities(JText::_('USRLDUPLICATEDENTRY'),ENT_COMPAT,'UTF-8'));
            $prayercenter->PCRedirect($returnurl);
          }
        }
      } else { 
          if(isset($_GET['modtype'])){
            $prayercenter->PCRedirect($returnto, htmlentities(JText::_('USRLINVALIDEMAIL'),ENT_COMPAT,'UTF-8'));
          } else {
          	$returnurl = JRoute::_("index.php?option=com_prayercenter&task=subscribe&Itemid=".$itemid."&return_msg=".htmlentities(JText::_('USRLINVALIDEMAIL'),ENT_COMPAT,'UTF-8'));
            $prayercenter->PCRedirect($returnurl);
          }
        }
    }
    function unsubscribesubmit()
    {
      global $db, $pcConfig, $prayercenter;
  		JRequest::checkToken() or jexit( 'Invalid Token' );
      $mod = JRequest::getVar( 'mod', null, 'get', 'string' );
      $modtype = JRequest::getVar( 'modtype', null, 'get', 'string' );
      $returnto = JRequest::getVar( 'return', null, 'post', 'string' );;
      jimport('joomla.utilities.date');
      jimport('joomla.mail.helper');
      jimport('joomla.filter.filteroutput');
      $itemid = $prayercenter->PCgetItemid();
  		$user =& JFactory::getUser();
      if(!$pcConfig['config_captcha_bypass_4member'] || $pcConfig['config_captcha_bypass_4member'] && $user->guest){
        $this->pcCaptchaValidate($returnto,$itemid,$modtype,'subscribe');
      }
      $newsubscribe = JRequest::getVar('newsubscribe',null,'post','string');
      if(!empty($newsubscribe) && JMailHelper::isEmailAddress($newsubscribe)){
        if( !$prayercenter->PCcheckEmail($newsubscribe) ) {
            if(isset($_GET['modtype'])){
              $prayercenter->PCRedirect($returnto, htmlentities(JText::_('USRLINVALIDDOMAIN'),ENT_COMPAT,'UTF-8'));
            } else {
            	$returnurl = JRoute::_('index.php?option=com_prayercenter&task=subscribe&Itemid='.$itemid.'&return_msg='.htmlentities(JText::_('USRLINVALIDDOMAIN'),ENT_COMPAT,'UTF-8'));
              $prayercenter->PCRedirect($returnurl);
            }
          }
      if( !$prayercenter->PCcheckBlockedEmail($newsubscribe) ) {
          if(isset($_GET['modtype'])){
            $prayercenter->PCRedirect($returnto, htmlentities(JText::_('USRLINVALIDEMAIL'),ENT_COMPAT,'UTF-8'));
          } else {
          	$returnurl = JRoute::_("index.php?option=com_prayercenter&task=subscribe&Itemid=".$itemid.'&return_msg='.htmlentities(JText::_('USRLINVALIDEMAIL'),ENT_COMPAT,'UTF-8'));
            $prayercenter->PCRedirect($returnurl);
          }
        }
      }
      $newsubscribe = JMailHelper::cleanAddress($newsubscribe);
      if (JMailHelper::isEmailAddress( $newsubscribe )){
        $db	=& JFactory::getDBO();
        $db->setQuery("SELECT * FROM #__prayercenter_subscribe WHERE email=".$db->quote($db->getEscaped($newsubscribe),false)."");
        $readq = $db->loadObjectList();
        if($pcConfig['config_admin_approve_subscribe'] == 2){
          $prayercenter->PCconfirm_unsub_notification($newsubscribe,$readq[0]->id,$readq[0]->sessionid,0);
          if(isset($_GET['modtype'])){
            $prayercenter->PCRedirect($returnto, htmlentities(JText::_('PCREQSUBMITCONFIRM'),ENT_COMPAT,'UTF-8'));
          } else {
          	$returnurl = JRoute::_('index.php?option=com_prayercenter&task=subscribe&Itemid='.$itemid.'&return_msg='.htmlentities(JText::_('PCREQSUBMITCONFIRM'),ENT_COMPAT,'UTF-8'));
            $prayercenter->PCRedirect($returnurl);
          }
        }
        if ($newsubscribe == $readq[0]->email){
          $db->setQuery("DELETE FROM #__prayercenter_subscribe WHERE email=".$db->quote($db->getEscaped($newsubscribe),false)."");
      		if (!$db->query()) {
  					JError::raiseError( 500, $db->stderr());
      		}	
        	$prayercenter->PCemail_unsubscribe($newsubscribe);
          if(isset($_GET['modtype'])){
            $prayercenter->PCRedirect($returnto, htmlentities(JText::_('USRLENTRYREMOVED'),ENT_COMPAT,'UTF-8'));
          } else {
          	$returnurl = JRoute::_("index.php?option=com_prayercenter&task=subscribe&Itemid=".$itemid."&return_msg=".htmlentities(JText::_('USRLENTRYREMOVED'),ENT_COMPAT,'UTF-8'));
            $prayercenter->PCRedirect($returnurl);
          }
        } else {
            if(isset($_GET['modtype'])){
              $prayercenter->PCRedirect($returnto, htmlentities(JText::_('USRLNOTSUBSCRIBED'),ENT_COMPAT,'UTF-8'));
            } else {
            	$returnurl = JRoute::_("index.php?option=com_prayercenter&task=subscribe&Itemid=".$itemid."&return_msg=".htmlentities(JText::_('USRLNOTSUBSCRIBED'),ENT_COMPAT,'UTF-8'));
              $prayercenter->PCRedirect($returnurl);
            }
        }
      }
      if(isset($_GET['modtype'])){
        $prayercenter->PCRedirect($returnto, htmlentities(JText::_('USRLINVALIDEMAIL'),ENT_COMPAT,'UTF-8'));
      } else {
      	$returnurl = JRoute::_("index.php?option=com_prayercenter&task=subscribe&Itemid=".$itemid."&return_msg=".htmlentities(JText::_('USRLINVALIDEMAIL'),ENT_COMPAT,'UTF-8'));
        $prayercenter->PCRedirect($returnurl);
      }
    }
    function editrequest()
    {   
      global $db, $pcConfig, $prayercenter;
      $itemid = $prayercenter->PCgetItemid();
      $db		=& JFactory::getDBO();
      $app =& JFactory::getApplication();
      jimport('joomla.utilities.date');
      $dateset = new JDate(gmdate('Y-m-d H:i:s'));
      $dateset->setOffset($app->getCfg( 'config.offset' ) + date('I'));
      $time = $dateset->toFormat('%H:%M:%S');
      $date = $dateset->toFormat('%Y-%m-%d');
  		$id = JRequest::getVar('id',null,'post','int');
      $request = JRequest::getVar('newrequest',null,'post','string',JREQUEST_ALLOWHTML);
     	$db->setQuery("UPDATE #__prayercenter SET request=".$db->quote($db->getEscaped($request),false).", date=".$db->quote($db->getEscaped($date),false).", time=".$db->quote($db->getEscaped($time),false)." WHERE id=".(int)$id."");
  		if (!$db->query()) {
				JError::raiseError( 500, $db->stderr());
  		}	
     	$db->setQuery("SELECT * FROM #__prayercenter WHERE id=".(int)($id)."");
      $readresult = $db->loadObjectList();
  		$model =& $this->getModel('prayercenter');
  		$model->checkin();
     	$returnurl = JRoute::_("index.php?option=com_prayercenter&task=".$_POST['last']."&Itemid=".$itemid);
      $prayercenter->PCRedirect($returnurl);
    }
    function closeedit()
    {
      global $db, $prayercenter;
      $itemid = $prayercenter->PCgetItemid();
      $last = JRequest::getVar('last',null,'post','string');
   		$id = JRequest::getVar('id',null,'post','int');
  		$model =& $this->getModel('prayercenter');
  		$model->checkin();
    	$returnurl = JRoute::_('index.php?option=com_prayercenter&task='.$last.'&Itemid='.(int)$itemid);
      $prayercenter->PCRedirect($returnurl);
    }
    function delrequest()
    {
      global $db, $pcConfig, $prayercenter;
      if($pcConfig['config_comments'] == 1) {
        $jcomments = JPATH_SITE . DS .'components' . DS . 'com_jcomments' . DS . 'jcomments.php';
        if (file_exists($jcomments)) {
          require_once($jcomments);
        }
      } elseif($pcConfig['config_comments'] == 2) {
        $jsc = JPATH_SITE.DS.'components'.DS.'com_jsitecomments'.DS.'helpers'.DS.'jsc_class.php';
        if (file_exists($jsc)) {
          require_once($jsc);
        }
      }
      $itemid = $prayercenter->PCgetItemid();
      $db		=& JFactory::getDBO();
    	$cid = (JRequest::getVar('delete',array(0),'post','array'));
    	while(list($key, $val) = each($cid))
    		{
          	$delreq = "DELETE FROM #__prayercenter WHERE id=".(int)$key."";
            $db->setQuery($delreq);
        		if (!$db->query()) {
    					JError::raiseError( 500, $db->stderr());
        		}	
            if($pcConfig['config_comments'] > 0) {
              if (file_exists($jcomments)) {
                  JComments::deleteComments($id, 'com_prayercenter');
              } elseif (file_exists($jsc)) {
                  jsitecomments::JSCdelComment('com_prayercenter', $id);
              }
            }
        }
    	$returnurl = JRoute::_('index.php?option=com_prayercenter&task=moderate&Itemid='.$itemid);
      $prayercenter->PCRedirect($returnurl);
    }
    function editdelrequest()
    {
      global $db, $pcConfig, $prayercenter;
      if($pcConfig['config_comments'] == 1) {
        $jcomments = JPATH_SITE . DS .'components' . DS . 'com_jcomments' . DS . 'jcomments.php';
        if (file_exists($jcomments)) {
          require_once($jcomments);
        }
      } elseif($pcConfig['config_comments'] == 2) {
        $jsc = JPATH_SITE.DS.'components'.DS.'com_jsitecomments'.DS.'helpers'.DS.'jsc_class.php';
        if (file_exists($jsc)) {
          require_once($jsc);
        }
      }
      $itemid = $prayercenter->PCgetItemid();
      $db		=& JFactory::getDBO();
    	$id = JRequest::getVar('id',null,'post','int');
     	$delreq = "DELETE FROM #__prayercenter WHERE id=".(int)$id."";
      $db->setQuery($delreq);
   		if (!$db->query()) {
				JError::raiseError( 500, $db->stderr());
   		}	
      if($pcConfig['config_comments'] > 0) {
        if (file_exists($jcomments)) {
            JComments::deleteComments($id, 'com_prayercenter');
        } elseif (file_exists($jsc)) {
            jsitecomments::JSCdelComment('com_prayercenter', $id);
        }
      }
    	$returnurl = JRoute::_('index.php?option=com_prayercenter&task=view&Itemid='.$itemid);
      $prayercenter->PCRedirect($returnurl);
    }
    function pubrequest()
    {
      global $db, $pcConfig, $prayercenter;
      $itemid = $prayercenter->PCgetItemid();
      $db		=& JFactory::getDBO();
    	$cid = (JRequest::getVar('delete',array(0),'post','array'));
    	while(list($key, $val) = each($cid))
    		{
      	$pubreq = "UPDATE #__prayercenter SET publishstate='1' WHERE id=".(int)$key."";
            $db->setQuery($pubreq);
        		if (!$db->query()) {
    					JError::raiseError( 500, $db->stderr());
        		}	
    		$model =& $this->getModel('prayercenter');
    		$model->checkin();
        $query = $db->setQuery("SELECT * FROM #__prayercenter WHERE id=".(int)$key."");
        $result = $db->loadObjectList();
        $newrequester = $result[0]->requester;
        $newrequest = stripslashes($result[0]->request);
        $newemail = $result[0]->email;
        $sendpriv = $result[0]->displaystate;
        $sessionid = $result[0]->sessionid;
        if($sendpriv){
          $prayercenter->PCemail_notification($newrequest,$newrequester,$newemail,0,$sendpriv);
          $prayercenter->PCemail_prayer_chain($newrequest,$newrequester);
          if($pcConfig['config_distrib_type'] > 1 && !empty($pcConfig['config_pms_plugin'])){
            $prayercenter->PCsendPM($newrequesterid,$newrequester,$newrequest,$newemail,$sendpriv);
          }
        } elseif(!$sendpriv){
          $prayercenter->PCemail_notification($newrequest,$newrequester,$newemail,0,$sendpriv,(int)$key,$sessionid);
          if($pcConfig['config_distrib_type'] > 1 && !empty($pcConfig['config_pms_plugin'])){
            $prayercenter->PCsendPM($newrequesterid,$newrequester,$newrequest,$newemail,$sendpriv,(int)$key,$sessionid);
            }
        }
      }
    	$returnurl = JRoute::_('index.php?option=com_prayercenter&task=moderate&Itemid='.$itemid.'&return_msg='.htmlentities(JText::_('PCREQSUBMIT'),ENT_COMPAT,'UTF-8'));
      $prayercenter->PCRedirect($returnurl);
    }
    function unpubrequest()
    {
      global $db, $prayercenter;
   		JRequest::checkToken() or jexit( 'Invalid Token' );
      $db	=& JFactory::getDBO();
      $itemid = $prayercenter->PCgetItemid();
    	$id = JRequest::getVar('id',null,'post','int');
    	$unpubreq = "UPDATE #__prayercenter SET publishstate='0' WHERE id=".(int)$id."";
      $db->setQuery($unpubreq);
  		if (!$db->query()) {
				JError::raiseError( 500, $db->stderr());
  		}	
  		$model =& $this->getModel('prayercenter');
  		$model->checkin();
    	$returnurl = JRoute::_('index.php?option=com_prayercenter&task=moderate&Itemid='.$itemid);
      $prayercenter->PCRedirect($returnurl);
    }
    function pcCaptchaValidate($returnto,$itemid,$modtype,$task){
      global $pcConfig, $prayercenter;
      $JVersion = new JVersion();
      if ($pcConfig['config_captcha'] == '1') {
        $scode = JRequest::getVar('security_code',null,'post');
        if (!$prayercenter->PCCaptchaValidate($scode,'newreq')){
          if(isset($_GET['modtype'])){
            $prayercenter->PCRedirect($returnto.'&'.$modtype.'='.htmlentities(JText::_('USRLINVALIDCODE'),ENT_COMPAT,'UTF-8'));
          } else {
          	$returnurl = JRoute::_('index.php?option=com_prayercenter&task='.$task.'&Itemid='.$itemid.'&return_msg='.htmlentities(JText::_('USRLINVALIDCODE'),ENT_COMPAT,'UTF-8'));
            $prayercenter->PCRedirect($returnurl);
          }
        } 
      } elseif($pcConfig['config_captcha'] == '2' && JPluginHelper::isEnabled('alikonweb','alikonweb.captchabot')){
        $psw3 = strtolower(JRequest::getVar('password3', '', 'post', 'cmd'));
        JPluginHelper::importPlugin('alikonweb');
        $dispatcher =& JDispatcher::getInstance();
        $results = $dispatcher->trigger('onVerify',array($psw3));
        if($results[0] !== true){
          if(isset($_GET['modtype'])){
            $prayercenter->PCRedirect($returnto.'&'.$modtype.'='.htmlentities(JText::_('USRLINVALIDCODE'),ENT_COMPAT,'UTF-8'));
          } else {
          	$returnurl = JRoute::_('index.php?option=com_prayercenter&task='.$task.'&Itemid='.$itemid.'&return_msg='.htmlentities(JText::_('USRLINVALIDCODE'),ENT_COMPAT,'UTF-8'));
            $prayercenter->PCRedirect($returnurl);
          }
        }
      } elseif($pcConfig['config_captcha'] == '3' && JPluginHelper::isEnabled('system','crosscheck')){
        $results = plgSystemCrossCheck::checkCrossChk(JRequest::getVar('user_code',null,'method'));
        if($results !== true){
          if(isset($_GET['modtype'])){
            $prayercenter->PCRedirect($returnto.'&'.$modtype.'='.$results);
          } else {
          	$returnurl = JRoute::_('index.php?option=com_prayercenter&task='.$task.'&Itemid='.$itemid.'&return_msg='.$results.'-'.$test.'-'.$test2);
            $prayercenter->PCRedirect($returnurl);
          }
        }
      } elseif($pcConfig['config_captcha'] == '4' && JPluginHelper::isEnabled('system','tincaptcha')){
        $captcha = plgSystemTincaptcha::check(JRequest::getVar('captcha',null,'method'));
        if($captcha !== true){
          if(isset($_GET['modtype'])){
            $prayercenter->PCRedirect($returnto.'&'.$modtype.'='.$captcha);
          } else {
          	$returnurl = JRoute::_('index.php?option=com_prayercenter&task='.$task.'&Itemid='.$itemid.'&return_msg='.$captcha);
            $prayercenter->PCRedirect($returnurl);
          }
        }
      } elseif($pcConfig['config_captcha'] == '5' && JPluginHelper::isEnabled('system','moovur')){
          if(isset($_GET['modtype'])){
            $returnurl = $returnto;
          } else {
            $returnurl = JRoute::_('index.php?option=com_prayercenter&task='.$task.'&Itemid='.$itemid);
          }
          Moovur::checkCaptcha($returnurl);
        } elseif($pcConfig['config_captcha'] == '6' && $pcConfig['config_recap_pubkey'] != "" && $pcConfig['config_recap_privkey'] != ""){
          require_once(JPATH_ROOT.'/components/com_prayercenter/assets/captcha/recaptchalib.php');
          $privatekey = $pcConfig['config_recap_privkey'];
          $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);    
          if (!$resp->is_valid) {     
            if(isset($_GET['modtype'])){
              $prayercenter->PCRedirect($returnto.'&'.$modtype.'='.htmlentities(JText::_('USRLINVALIDCODE'),ENT_COMPAT,'UTF-8'));
            } else {
            	$returnurl = JRoute::_('index.php?option=com_prayercenter&task='.$task.'&Itemid='.$itemid.'&return_msg='.htmlentities(JText::_('USRLINVALIDCODE'),ENT_COMPAT,'UTF-8'));
              $prayercenter->PCRedirect($returnurl);
            }
          }
        } elseif($pcConfig['config_captcha'] == '7' && (real)$JVersion->RELEASE >= 2.5){
          $session =& JFactory::getSession();
          $respchk = $session->has('pc_respchk');
          $plugin  = JFactory::getApplication()->getParams()->get('captcha', JFactory::getConfig()->get('captcha'));
          $captcha = JCaptcha::getInstance($plugin, array('namespace' => 'adminForm'));
          $captcha_code = "";
          $resp = $captcha->checkAnswer($captcha_code);
          if($resp == false && !$respchk) {     
            if(isset($_GET['modtype'])){
              $prayercenter->PCRedirect($returnto.'&'.$modtype.'='.htmlentities(JText::_('USRLINVALIDCODE'),ENT_COMPAT,'UTF-8'));
            } else {
            	$returnurl = JRoute::_('index.php?option=com_prayercenter&task='.$task.'&Itemid='.$itemid.'&return_msg='.htmlentities(JText::_('USRLINVALIDCODE'),ENT_COMPAT,'UTF-8'));
              $prayercenter->PCRedirect($returnurl);
            }
          } elseif($respchk) {
            $session->clear('pc_respchk');
          }
        }
      return true;
    }
}
?>