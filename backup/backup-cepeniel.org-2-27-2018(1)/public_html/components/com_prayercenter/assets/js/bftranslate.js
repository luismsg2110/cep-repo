function getTranslator(reqid){
   var langstr = document.getElementById("tol").value;
   if(langstr == '') {alert(langtranmsg);return false;}
   var hname = window.location.protocol+'//'+window.location.hostname;
   var requesturl = hname + '/index.php?option=com_prayercenter&task=view_request&id=' + reqid + '&pop=1&tmpl=component';
   var requrl = encodeURIComponent(requesturl);
   var url = 'http://babelfish.yahoo.com/translate_url?lp=' + langstr + '&trurl=' + requrl;
   window.open(url,'RequestTranslation','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=800,height=480,directories=no,location=no');
   document.getElementById("tol").value='';
   return false;
}

function showLanguageDropDown(wsel,se){
  var frl=document.getElementById(wsel);
  if (frl.length>0) return;
  var a = {
   'Translate request' : '',
   'Chinese-Simp to English' : 'zh_en',
   'Chinese-Simp to Chinese-Trad' : 'zh_zt',
   'Chinese-Trad to English' : 'zt_en',
   'Chinese-Trad to Chinese-Simp' : 'zt_zh',
   'English to Chinese-Simp' : 'en_zh',
   'English to Chinese-Trad' : 'en_zt',
   'English to Dutch' : 'en_nl',
   'English to French' : 'en_fr',
   'English to German' : 'en_de',
   'English to Greek' : 'en_el',
   'English to Italian' : 'en_it',
   'English to Japanese' : 'en_ja',
   'English to Korean' : 'en_ko',
   'English to Portuguese' : 'en_pt',
   'English to Russian' : 'en_ru',
   'English to Spanish' : 'en_es',
   'Dutch to English' : 'nl_en',
   'Dutch to French' : 'nl_fr',
   'French to Dutch' : 'fr_nl',
   'French to English' : 'fr_en',
   'French to German' : 'fr_de',
   'French to Greek' : 'fr_el',
   'French to Italian' : 'fr_it',
   'French to Portuguese' : 'fr_pt',
   'French to Spanish' : 'fr_es',
   'German to English' : 'de_en',
   'German to French' : 'de_fr',
   'Greek to English' : 'el_en',
   'Greek to French' : 'el_fr',
   'Italian to English' : 'it_en',
   'Italian to French' : 'it_fr',
   'Japanese to English' : 'ja_en',
   'Korean to English' : 'ko_en',
   'Portuguese to English' : 'pt_en',
   'Portuguese to French' : 'pt_fr',
   'Russian to English' : 'ru_en',
   'Spanish to English' : 'es_en',
   'Spanish to French' : 'es_fr'
  };
  var r=0;
  for(var key in a){
    var lan= key.toLowerCase();
    lan = capitalize(lan);
    var lcode=a[key];
    frl.options[r]=new Option (lan,lcode,false,false);
    r++;
  }
}
