<?php
/* *************************************************************************************
Title          PrayerCenter Component for Joomla
Author         Mike Leeper
License        This program is free software: you can redistribute it and/or modify
               it under the terms of the GNU General Public License as published by
               the Free Software Foundation, either version 3 of the License, or
               (at your option) any later version.
               This program is distributed in the hope that it will be useful,
               but WITHOUT ANY WARRANTY; without even the implied warranty of
               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
               GNU General Public License for more details.
               You should have received a copy of the GNU General Public License
               along with this program.  If not, see <http://www.gnu.org/licenses/>.
Copyright      2006-2011 - Mike Leeper (MLWebTechnologies) 
****************************************************************************************
No direct access*/
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view');
class PrayerCenterViewLinks extends JView
{
	function display( $tpl = null)
	{
		global $pcConfig;
    $lang =& Jfactory::getLanguage();
    $lang->load( 'com_prayercenter', JPATH_SITE); 
    $db		=& JFactory::getDBO();
    $db->setQuery("SELECT name,url,descrip,alias FROM #__prayercenter_links WHERE published='1' ORDER BY ordering");
    $link_array = $db->loadObjectList();
		// Set pathway information
		$this->assignRef('config_show_page_headers',	$pcConfig['config_show_page_headers']);
    $this->assignRef('link_array',$link_array);
    $this->assignRef('config_two_columns', $pcConfig['config_two_column']);
    $this->assignRef('config_use_gb', $pcConfig['config_use_gb']);
		$this->assignRef('title', JText::_('PCTITLE'));
		$this->assignRef('intro',	nl2br(JText::_('PCLISTINTRO')));
		parent::display($tpl);
	}
}
