<?php
/* *************************************************************************************
Title          PrayerCenter Component for Joomla
Author         Mike Leeper
License        This program is free software: you can redistribute it and/or modify
               it under the terms of the GNU General Public License as published by
               the Free Software Foundation, either version 3 of the License, or
               (at your option) any later version.
               This program is distributed in the hope that it will be useful,
               but WITHOUT ANY WARRANTY; without even the implied warranty of
               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
               GNU General Public License for more details.
               You should have received a copy of the GNU General Public License
               along with this program.  If not, see <http://www.gnu.org/licenses/>.
Copyright      2006-2012 - Mike Leeper (MLWebTechnologies) 
****************************************************************************************
No direct access*/
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view');
class PrayerCenterViewSubscribe extends JView
{
	function display( $tpl = null)
	{
		global $pcConfig;
		$uri     	=& JFactory::getURI();
    $lang =& Jfactory::getLanguage();
    $lang->load( 'com_prayercenter', JPATH_SITE); 
		// Set pathway information
		$this->assign('action', 	$uri->toString());
		$this->assignRef('title', JText::_('PCTITLE'));
		$this->assignRef('config_show_page_headers',	$pcConfig['config_show_page_headers']);
		$this->assignRef('intro', JText::_('PCLISTINTRO'));
		$this->assignRef('user_view',	$pcConfig['user_view']);
		$this->assignRef('config_captcha',	$pcConfig['config_captcha']);
    $this->assignRef('config_captcha_bypass', $pcConfig['config_captcha_bypass_4member']);
		parent::display($tpl);
	}
}
