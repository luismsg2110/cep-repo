<?php
/* *************************************************************************************
Title          PrayerCenter Component for Joomla
Author         Mike Leeper
Enhancements   Douglas Machado 
License        This program is free software: you can redistribute it and/or modify
               it under the terms of the GNU General Public License as published by
               the Free Software Foundation, either version 3 of the License, or
               (at your option) any later version.
               This program is distributed in the hope that it will be useful,
               but WITHOUT ANY WARRANTY; without even the implied warranty of
               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
               GNU General Public License for more details.
               You should have received a copy of the GNU General Public License
               along with this program.  If not, see <http://www.gnu.org/licenses/>.
Copyright      2006-2012 - Mike Leeper (MLWebTechnologies) 
****************************************************************************************
No direct access*/
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.controller');
class PrayerCenterController extends JController
{
	function display()
    {
		parent::display();
    }
  function confirm(){
    global $db, $prayercenter, $pcConfig;
    $db		=& JFactory::getDBO();
    $id = JFilterOutput::cleanText(JRequest::getVar('id',null,'get','int'));
    $sessionid = JFilterOutput::cleanText(JRequest::getVar('sessionid',null,'get','string'));
    $itemid = $prayercenter->PCgetItemid();
    if(is_numeric($id) && $prayercenter->PCSIDvalidate($sessionid)){
      $db->setQuery("SELECT request,requester,email,displaystate FROM #__prayercenter WHERE id='".$id."' AND sessionid='".$sessionid."' AND publishstate='0'");
      $cresults = $db->loadObjectList();    
      if(count($cresults)>0){
    		$db->setQuery("UPDATE #__prayercenter SET publishstate='1' WHERE id='".$id."' AND sessionid='".$sessionid."'");
    		if (!$db->query()) {
    			die("SQL error" . $db->stderr(true));
    		}	
        $sendpriv = $cresults[0]->displaystate;
        if($sendpriv){
          $prayercenter->PCemail_notification($cresults[0]->request,$cresults[0]->requester,$cresults[0]->email,0,$sendpriv);
          $prayercenter->PCemail_prayer_chain($cresults[0]->request,$cresults[0]->requester);
          if($pcConfig['config_distrib_type'] > 1 && $pcConfig['config_pms_plugin']){
            $prayercenter->PCsendPM($cresults[0]->requester,$cresults[0]->request,$cresults[0]->email,$sendpriv);
          }
        } elseif(!$sendpriv){
          $prayercenter->PCemail_notification($cresults[0]->request,$cresults[0]->requester,$cresults[0]->email,0,$sendpriv);
          if($pcConfig['config_distrib_type'] > 1 && $pcConfig['config_pms_plugin']){
            $prayercenter->PCsendPM($cresults[0]->requester,$cresults[0]->request,$cresults[0]->email,$sendpriv);
          }
         }
    	$returnurl = JRoute::_('index.php?option=com_prayercenter&task=view&Itemid='.$itemid.'&return_msg='.htmlentities(JText::_('PCREQSUBMIT'),ENT_COMPAT,'UTF-8'));
      $prayercenter->PCRedirect($returnurl);
    }
   }
  	$returnurl = JRoute::_('index.php?option=com_prayercenter&task=view&Itemid='.$itemid);
    $prayercenter->PCRedirect($returnurl);
  }
  function confirm_adm(){
    global $db, $prayercenter, $pcConfig;
    $db		=& JFactory::getDBO();
    $id = JFilterOutput::cleanText(JRequest::getVar('id',null,'get','int'));
    $sessionid = JFilterOutput::cleanText(JRequest::getVar('sessionid',null,'get','string'));
    $itemid = $prayercenter->PCgetItemid();
    if(is_numeric($id) && $prayercenter->PCSIDvalidate($sessionid)){
      $db->setQuery("SELECT request,requester,email,displaystate FROM #__prayercenter WHERE id='".$id."' AND sessionid='".$sessionid."' AND publishstate='0'");
      $cresults = $db->loadObjectList();    
      if(count($cresults)>0){
    		$db->setQuery("UPDATE #__prayercenter SET publishstate='1' WHERE id='".$id."' AND sessionid='".$sessionid."'");
    		if (!$db->query()) {
    			die("SQL error" . $db->stderr(true));
    		}	
        $sendpriv = $cresults[0]->displaystate;
        if($sendpriv){
          $prayercenter->PCemail_notification($cresults[0]->request,$cresults[0]->requester,$cresults[0]->email,0,$sendpriv);
          $prayercenter->PCemail_prayer_chain($cresults[0]->request,$cresults[0]->requester);
          if($pcConfig['config_distrib_type'] > 1 && $pcConfig['config_pms_plugin']){
            $prayercenter->PCsendPM($cresults[0]->requester,$cresults[0]->request,$cresults[0]->email,$sendpriv);
          }
        } elseif(!$sendpriv){
          $prayercenter->PCemail_notification($cresults[0]->request,$cresults[0]->requester,$cresults[0]->email,0,$sendpriv);
          if($pcConfig['config_distrib_type'] > 1 && $pcConfig['config_pms_plugin']){
            $prayercenter->PCsendPM($cresults[0]->requester,$cresults[0]->request,$cresults[0]->email,$sendpriv);
          }
        }
      	$returnurl = JRoute::_('index.php?option=com_prayercenter&task=view&Itemid='.$itemid.'&return_msg='.htmlentities(JText::_('PCREQAPPROVE'),ENT_COMPAT,'UTF-8'));
        $prayercenter->PCRedirect($returnurl);
      }
    }
  	$returnurl = JRoute::_('index.php?option=com_prayercenter&task=view&Itemid='.$itemid);
    $prayercenter->PCRedirect($returnurl);
  }
  function confirm_sub(){
    global $db, $prayercenter;
    $db		=& JFactory::getDBO();
    $id = JFilterOutput::cleanText(JRequest::getVar('id',null,'get','int'));
    $sessionid = JFilterOutput::cleanText(JRequest::getVar('sessionid',null,'get','string'));
    $itemid = $prayercenter->PCgetItemid();
    if(is_numeric($id) && $prayercenter->PCSIDvalidate($sessionid)){
      $db->setQuery("SELECT email FROM #__prayercenter_subscribe WHERE id='".$id."' AND sessionid='".$sessionid."' AND approved='0'");
      $subresults = $db->loadObjectList();    
      if(count($subresults)>0){
    		$db->setQuery("UPDATE #__prayercenter_subscribe SET approved='1' WHERE id='".$id."' AND sessionid='".$sessionid."'");
    		if (!$db->query()) {
    			die("SQL error" . $db->stderr(true));
    		}	
        $prayercenter->PCemail_subscribe($subresults[0]->email);
      	$returnurl = JRoute::_('index.php?option=com_prayercenter&task=subscribe&Itemid='.$itemid.'&return_msg='.htmlentities(JText::_('USRLENTRYACCEPTED'),ENT_COMPAT,'UTF-8'));
        $prayercenter->PCRedirect($returnurl);
      }
    }
  	$returnurl = JRoute::_('index.php?option=com_prayercenter&task=subscribe&Itemid='.$itemid);
    $prayercenter->PCRedirect($returnurl);
  }
  function confirm_unsub(){
    global $db, $prayercenter;
    $db		=& JFactory::getDBO();
    $id = JFilterOutput::cleanText(JRequest::getVar('id',null,'get','int'));
    $sessionid = JFilterOutput::cleanText(JRequest::getVar('sessionid',null,'get','string'));
    $itemid = $prayercenter->PCgetItemid();
    if(is_numeric($id) && $prayercenter->PCSIDvalidate($sessionid)){
      $db->setQuery("SELECT email FROM #__prayercenter_subscribe WHERE id='".$id."' AND sessionid='".$sessionid."' AND approved='1'");
      $unsubresults = $db->loadObjectList();    
      if(count($unsubresults)>0){
        $db->setQuery("DELETE FROM #__prayercenter_subscribe WHERE id='".$id."' AND sessionid='".$sessionid."'");
    		if (!$db->query()) {
    			die("SQL error" . $db->stderr(true));
    		}	
        $prayercenter->PCemail_subscribe($unsubresults[0]->email);
      	$returnurl = JRoute::_('index.php?option=com_prayercenter&task=subscribe&Itemid='.$itemid.'&return_msg='.htmlentities(JText::_('USRLENTRYREMOVED'),ENT_COMPAT,'UTF-8'));
        $prayercenter->PCRedirect($returnurl);
      }
    }
  	$returnurl = JRoute::_('index.php?option=com_prayercenter&task=subscribe&Itemid='.$itemid);
    $prayercenter->PCRedirect($returnurl);
  }
  function newreq($cachable = false){
		JRequest::setVar('view', JRequest::getCmd('view', 'newreq') );
		parent::display($cachable);
    }
  function subscribe(){
		JRequest::setVar('view', 'subscribe' );
		parent::display();
    }
  function unsubscribe(){
		JRequest::setVar('view', 'subscribe' );
		parent::display();
    }
  function view(){
    global $pcConfig;
		$view = & $this->getView('list', 'html');
    if($pcConfig['config_view_template'] == 1) {
  		$view->setLayout('rounded');
    }elseif($pcConfig['config_view_template'] == 2) {
  		$view->setLayout('basic');
    }
		$view->display();
   }
  function pdf(){
   	global $pc_rights, $pcConfig, $prayercenter;
    if ($pc_rights->get('pc.view') && $pcConfig['config_show_pdf']){
      $JVersion = new JVersion();
      $lang =& Jfactory::getLanguage();
      $lang->load( 'com_prayercenter', JPATH_SITE);  
      if( (real)$JVersion->RELEASE == 1.5 ) {
        global $mainframe;
        $pop = 1;
        $app = &JFactory::getApplication();
        jimport('joomla.utilities.date');
        $livesite = JURI::base();
        $sitename = $mainframe->getCfg( 'sitename' );
        $offset = $mainframe->getCfg( 'offset' );
        $dateset = new JDate(gmdate('Y-m-d H:i:s'));
        $dateset->setOffset($mainframe->getCfg( 'config.offset' ) + date('I'));
        $date = $dateset->toFormat('%B %d, %Y %I:%M:%S %p');
    		$listtype = JRequest::getVar( 'listtype', null, 'method', 'int' );
        $db	=& JFactory::getDBO();
    		$document = &JFactory::getDocument();
    		$document->setTitle($sitename.' - '.htmlentities(JText::_('PCTITLE'),ENT_COMPAT,'UTF-8'));
        if($listtype == 0){
      		$id = JRequest::getVar( 'id', null, 'method', 'int' );
          $db->setQuery("SELECT * FROM #__prayercenter WHERE id='$id' AND publishstate='1' AND displaystate='1'");
        } elseif($listtype == 1){
          $db->setQuery("SELECT * FROM #__prayercenter WHERE publishstate='1' AND displaystate='1' AND date=CURDATE() ORDER BY topic,DATE_FORMAT(CONCAT_WS(' ',date,time),'%Y-%m-%d %T') DESC");
        } elseif($listtype == 2){
          $db->setQuery("SELECT * FROM #__prayercenter WHERE publishstate='1' AND displaystate='1' AND WEEKOFYEAR(date)=WEEKOFYEAR(CURDATE()) AND YEAR(date)=YEAR(CURDATE()) ORDER BY topic,DATE_FORMAT(CONCAT_WS(' ',date,time),'%Y-%m-%d %T') DESC");
        }
    		$replies = $db->loadObjectList();
    		$countReplies = count($replies);
    		$document->setName(htmlentities(JText::_('USRLPRAYERREQUESTS'),ENT_COMPAT,'UTF-8'));
    		$text = "";
    		$text .= "\n";
        $topicarray = $prayercenter->PCgetTopics();
    		$text .= htmlentities(JText::_('USRLPRAYERREQUESTS'),ENT_COMPAT,'UTF-8');
    		$document->setHeader($text);
        $temptopic = "";
        $txt = '<table width="100%"><small>';
    		if ($countReplies > 0 ) {
    			foreach ($replies as $reply) {
            if($temptopic != $reply->topic){
      				$txt .= '<tr><td colspan="3"><b><u>'.$topicarray[$reply->topic+1]['text'].'</u></b></td><td>&nbsp;</td><td>&nbsp;</td></tr>';
      				$txt .= "<tr><td><u>".JText::_('Date')."</u></td><td><u>".JText::_('PCMODREQ')."</u></td><td><u>".JText::_('PCMODREQR')."</u></td></tr>";
              $temptopic = $reply->topic;
    				}
        		$request_date = $reply->date ." - ". $reply->time;
    				$txt .= "<tr><td>".$request_date."</td><td>".$reply->request."</td><td>".$reply->requester."</td></tr>";
    			}
        $txt .= '</small></table>';
        echo $txt;
    		echo "<hr>";
        echo '<small>'.htmlentities(JText::_('PDFGEN'),ENT_COMPAT,'UTF-8').' '.$date.'</small>';
    		} else {	
          echo '<br /><br />';
          echo JText::_('No requests found');
      		echo "<hr>";
        }
      } elseif( (real)$JVersion->RELEASE >= 1.6 ){
        $headerarr = array(JText::_('PCMODREQ'),JText::_('PCMODREQR'));
    		$listtype = JRequest::getVar( 'listtype', null, 'method', 'int' );
        require_once('components/com_prayercenter/helpers/pc_pdf_class.php');
    		$pdf = new PDF();
        $pdf->listtype = $listtype;
        $pdf->AddPage();
        $pdf->Ln(7);
        $pdf->SetFont('helvetica','',10);
        if($listtype == 0){
      		$id = JRequest::getVar( 'id', null, 'method', 'int' );
          $pdf->Table($headerarr,"SELECT * FROM #__prayercenter WHERE id='$id' AND publishstate='1' AND displaystate='1'");
        } elseif($listtype == 1){
          $pdf->Table($headerarr,"SELECT * FROM #__prayercenter WHERE publishstate='1' AND displaystate='1' AND date=CURDATE() ORDER BY topic,DATE_FORMAT(CONCAT_WS(' ',date,time),'%Y-%m-%d %T') DESC");
        } elseif($listtype == 2){
          $pdf->Table($headerarr,"SELECT topic,request,requester FROM #__prayercenter WHERE publishstate='1' AND displaystate='1' AND WEEKOFYEAR(date)=WEEKOFYEAR(CURDATE()) AND YEAR(date)=YEAR(CURDATE()) ORDER BY topic,DATE_FORMAT(CONCAT_WS(' ',date,time),'%Y-%m-%d %T') DESC");
        }
        $pdf->Output();
        exit(0);
      }
    } else {
      echo '<div class="componentheading">'.JText::_('PCTITLE').'</div>';
      echo '<h5><center>'.JText::_('ALERTNOTAUTH').'<br />'.JText::_('YOU NEED TO LOGIN.').'</center></h5>';
      echo '<br /><br /><br /><br />';
      $notAuth = 1;
    }
  }
  function view_links(){
		JRequest::setVar('view', 'links' );
		parent::display();
    }
  function view_devotion(){
		JRequest::setVar('view', 'devotions' );
		parent::display();
    }
  function moderate(){
  global $pc_rights, $pcConfig, $prayercenter;
    if ( $pcConfig['config_use_admin_alert'] > 1 && $pc_rights->get('pc.moderate') ){
  		JRequest::setVar('view', 'moderate' );
  		parent::display();
    } else {
      echo '<div class="componentheading">'.htmlentities(JText::_('PCTITLE'),ENT_COMPAT,'UTF-8').'</div>';
      echo '<h5><center>'.JText::_('ALERTNOTAUTH').'<br />'.JText::_('YOU NEED TO LOGIN.').'</center></h5>';
      echo '<br /><br /><br /><br />';
      echo '<div align="center">';
      $prayercenter->PCbackButton();
      echo '</div>';
      $notAuth = 1;
    }
  }
  function edit(){
    global $pc_rights, $pcConfig, $prayercenter;
    $eid = JFilterOutput::cleanText(JRequest::getVar('id',null,'get','int'));
 		JRequest::setVar('view', 'edit' );
 		parent::display();
   }
  function view_request(){
    $eid = JFilterOutput::cleanText(JRequest::getVar('id',null,'get','int'));
		JRequest::setVar('view', 'showreq' );
		parent::display();
    }
  function rss(){
  	global $db, $pcConfig, $prayercenter, $pc_rights;
    $user = &JFactory::getUser();
    $app = &JFactory::getApplication();
    $JVersion = new JVersion();
    $offset = "";
    if ($pc_rights->get('pc.view') && $pcConfig['config_show_pdf']){
      $lang =& Jfactory::getLanguage();
      $lang->load( 'com_prayercenter', JPATH_SITE); 
      $livesite = JURI::base();
      $sitename = $app->getCfg( 'sitename' );
      $itemid = $prayercenter->PCgetItemid();
      $config_rss_num = $pcConfig['config_rss_num'];
      $db		=& JFactory::getDBO();
    	while( @ob_end_clean() );
      if( (real)$JVersion->RELEASE == 1.5 ) {
        jimport('bitfolge.feedcreator');
      } elseif( (real)$JVersion->RELEASE >= 1.6 ){
    		require_once('components/com_prayercenter/assets/rss/feedcreator.php');
      }
    	$feed_type = 'RSS2.0';
    	$filename = 'pc_feed.xml';
      $cacheDir = JPATH_BASE.DS.'cache';
    	$cachefile = $cacheDir.'/'. $filename;
    	$rss 	= new UniversalFeedCreator();
    	$image 	= new FeedImage();
    	if ( $pcConfig['config_enable_rss_cache'] ) {
    		$rss->useCached( $feed_type, $cachefile, $pcConfig['config_rss_update_time'] );
    	}
    	$rss->title = stripslashes(htmlspecialchars($sitename)).' - '.JText::_('PCTITLE');
    	$rss->description = JText::_('PCRSSFEEDMSG').' '.$sitename;
    	$rss->link = htmlspecialchars( $livesite).'index.php?option=com_prayercenter&amp;Itemid='.$itemid;
    	$rss->syndicationURL = htmlspecialchars( $livesite).'index.php?option=com_prayercenter&amp;Itemid='.$itemid;
    	$rss->cssStyleSheet	= NULL;
    	$feed_image	= $livesite.'components/com_prayercenter/assets/images/prayer.png';
    	if ( $feed_image ) {
    		$image->url 		= $feed_image;
    		$image->link 		= $rss->link;
    		$image->title 		= 'Powered by Joomla! & PrayerCenter';
    		$image->description	= $rss->description;
    		$rss->image 		= $image;
    	}
      $db->setQuery( "SELECT * FROM #__prayercenter "
      . "\n WHERE publishstate = 1 "
      . "\n AND displaystate = 1 "
      . "\n ORDER BY id DESC "
      . "\n LIMIT ".$config_rss_num.""
      );
      $rows = $db->loadObjectList();
   		foreach($rows as $row) {
    		$item = new FeedItem();
    		$item->title = $row->requester;
        $item->link = JRoute::_("index.php?option=com_prayercenter&amp;Itemid=".$itemid."&amp;task=view_request&amp;type=rss&amp;id=".$row->id);
        $words = $row->request;
    			if( $pcConfig['config_rss_limit_text'] ) {
    				$words = substr( $words, 0, $pcConfig['config_rss_text_length'] );
    			}
    			$item->description = $words;
          if( (real)$JVersion->RELEASE == 1.5 ) {
        		$conf = JFactory::getConfig();
            $offset = $conf->get('offset');
          } elseif( (real)$JVersion->RELEASE >= 1.6 ){
            $seconds = date_offset_get(new DateTime);
            $offset =  $seconds / 3600;
          }
          $itemdate = date("r",strtotime($row->date.' '.$row->time.' '.$offset));
    			$item->date	= $itemdate;
    			$rss->addItem( $item );
    		}
    	$rss->saveFeed( $feed_type, $cachefile );
    } else {
      $this->setRedirect( 'index.php?option=com_prayercenter', JText::_('ALERTNOTAUTH') );
    }
  }
  function PCCapValid()
  {
    global $pcConfig;
    $JVersion = new JVersion();
    $captcha = JRequest::getVar( 'cap', null, 'post', 'int' );
    $modtype = JRequest::getVar( 'modtype', null, 'post', 'string' );
    $mod = JRequest::getVar( 'mod', null, 'post', 'string' );
    $returnto = $_SERVER['HTTP_REFERER'];
    preg_match("!index.php\?!",$returnto,$match);
    if(!$match) $returnto = $returnto.'index.php?';
  	if($captcha == 1){
      $session =& JFactory::getSession();
      if(isset($mod)){
      $security_code = strtolower($session->get($mod.'_security_code'));
      } else {
      $security_code = strtolower($session->get('pc_security_code'));
      }
      $usercode = strtolower((JRequest::getVar( 'usercode', null, 'post', 'string' )));
      if($security_code != $usercode){
        $i = $session->get('pc_max_attempts');
        $i++;
        $session->set('pc_max_attempts',$i);
        if($pcConfig['config_captcha_maxattempts'] > $i){
          $message = htmlentities(JText::_('USRLINVALIDCODE'),ENT_COMPAT,'UTF-8');
        } else {
          $message = htmlentities(JText::_('PCCAPTCHAMAXATTEMPTS'),ENT_COMPAT,'UTF-8');
        }
        exit($message);
      } else {
        exit(true);
      }
    } elseif($captcha == 6){
      require_once(JPATH_ROOT.'/components/com_prayercenter/assets/captcha/recaptchalib.php');
      $privatekey = $pcConfig['config_recap_privkey'];
      $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge"], $_POST["recaptcha_response"]);    
      if (!$resp->is_valid) {     
        $message = htmlentities(JText::_('USRLINVALIDCODE'),ENT_COMPAT,'UTF-8');
        exit($message);
      } else {
        exit(true);
      }
    } elseif($captcha == 7 && (real)$JVersion->RELEASE >= 2.5){
      $session =& JFactory::getSession();
      $plugin  = JFactory::getApplication()->getParams()->get('captcha', JFactory::getConfig()->get('captcha'));
      $captcha = JCaptcha::getInstance($plugin, array('namespace' => 'adminForm'));
      $captcha_code = "";
      $resp = $captcha->checkAnswer($captcha_code);
      if($resp == false) {     
        $message = htmlentities(JText::_('USRLINVALIDCODE'),ENT_COMPAT,'UTF-8');
        echo $message;
      } else {
        $session->set('pc_respchk',true);
        echo true;
      }
    }
  }
}
?>
