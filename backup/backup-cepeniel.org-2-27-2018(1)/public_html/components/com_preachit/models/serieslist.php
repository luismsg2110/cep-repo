<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.modellist');
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/additional.php');
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/message-info.php');
class PreachitModelSerieslist extends JModelList
{
var $_data = null;
var $_pagination = null;
var $_total = null;
var $_search = null;
var $_query = null;

function __construct()
  {
        parent::__construct();
 	$abspath    = JPATH_SITE;
  	require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/additional.php');
  	$params = PIHelperadditional::getPIparams();
  // Get pagination request variables
		$this->setState('limit', $params->get('serieslist_no'), 'limit', $params->get('serieslist_no'), 'int');
			$this->setState('limitstart', JRequest::getVar('limitstart', 0, '', 'int'));
	}

	function _buildQuery()
	{
  		$params = PIHelperadditional::getPIparams();
  		$where = $this->_buildContentWhere();
 		$sort = $params->get('seriessort', '1');
 		if ($sort == '1') {$order = 'id DESC';}
 		if ($sort == '2') {$order = 'id ASC';}
 		if ($sort == '3') {$order = 'ordering';}
 		if ($sort == '4') {$order = 'ordering DESC';}
        if ($sort == '5') {$order = 'series_name DESC';}
         if ($sort == '6') {$order = 'series_name ASC';}
 		$orderby = ' ORDER BY '.$order;
		$query = "SELECT * FROM #__piseries"
		. $where
		. $orderby;
		return $query;
	}


function getData() 
  {
        // if data hasn't already been obtained, load it
        if (empty($this->_data)) 
        {
            $params = PIHelperadditional::getPIparams();
            $sort = $params->get('seriessort', '1');
            $query = $this->_buildQuery();
            if ($sort != 1 && $sort != 2)
            {$this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));}
            else {
                if ($sort == 1)
                {$order = 'DESC';}
                else {$order = 'ASC';}
                $column = $params->get('seriessort_column', 'enddate');
                $items = $this->_getList($query);
                $this->_data = array_slice($this->_sortlistbydate($items, $column, $order), $this->getState('limitstart'), $this->getState('limit'));
            }
        }
        return $this->_data;
  }

function getTotal()
  {
        // Load the content if it doesn't already exist
        if (empty($this->_total)) {
            $query = $this->_buildQuery();
            $this->_total = $this->_getListCount($query);    
        }
        return $this->_total;
  }

  function getPagination()
  {
        // Load the content if it doesn't already exist
        if (empty($this->_pagination)) {
            jimport('joomla.html.pagination');
            $this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit') );
        }
        return $this->_pagination;
  }
  
  function getFData()
  {
      if (empty($this->_fData))
      {
          $query = "SELECT * FROM #__piseries WHERE featured = 1 AND published = 1";
          $this->_fData = $this->_getList($query, 0, 1); 
      }
      return $this->_fData;
  }
  
  function getFMessage($series = null)
  {
      if (empty($this->_fMessage))
      {
          if ($series == null)
          {
              $data = $this->getFData();
              if (empty($data) || count($data) >! 0)
              {$this->_fMessage = null; return $this->_fMessage;}
              $series = $data[0]->id;
          }
          $user    = JFactory::getUser();
          $language = JFactory::getLanguage()->getTag();
          $now = gmdate ( 'Y-m-d H:i:s' );
          $nullDate = $this->_db->getNullDate();
          $groups = implode(',', $user->getAuthorisedViewLevels());
          $wehre = array();
          $where[] = ' (#__pistudies.access IN ('.$groups.') OR #__pistudies.access = 0)';
          $where[] = ' (#__pistudies.saccess IN ('.$groups.') OR #__pistudies.saccess = 0)';
          $where[] = ' #__pistudies.series = '.(int)$series;
          $where[] = ' #__pistudies.published = 1';
          // min access
            $minaccess = array();
            foreach ($user->getAuthorisedViewLevels() AS $level)
            {
                $minaccess[] = '#__pistudies.minaccess REGEXP "[[:<:]]'.$level.'[[:>:]]"';
            }
            $where[] = ' (('. ( count( $minaccess ) ? implode( ' OR ', $minaccess ) : '' ) .') OR #__pistudies.minaccess = 0)';
            $where[] = ' #__pistudies.language IN ('.$this->_db->quote($language).','.$this->_db->quote('*').')';
            $where[] = '(#__pistudies.publish_up = '.$this->_db->Quote($nullDate).' OR #__pistudies.publish_up <= '.$this->_db->Quote($now).')';
            $where[] = '(#__pistudies.publish_down = '.$this->_db->Quote($nullDate).' OR #__pistudies.publish_down >= '.$this->_db->Quote($now).')';
            $where = ( count( $where ) ? ' WHERE '. implode( ' AND ', $where ) : '' );
          $query = "SELECT * FROM #__pistudies ".$where." ORDER BY #__pistudies.study_date DESC";
          $this->_fMessage = $this->_getList($query, 0, 1); 
      }
      return $this->_fMessage;
  }
  
  function _buildContentWhere()
	{
			$app = JFactory::getApplication();			
			$user	= JFactory::getUser();
			$language = JFactory::getLanguage()->getTag();
			$menuparams = $app->getParams();
			$seriessel = $menuparams->get('seriessel', 0);
			$seriesselection = $menuparams->get('seriesselect');
            $letter = JRequest::getVar('alpha', '');
            $layout = JRequest::getVar('layout', '');
            if ($layout == 'ministry')
            {$ministry = JRequest::getInt('ministry', 0);}
            else {$ministry = 0;}
			$slist = array();	
		             
			$where = array();
			$where[] = ' #__piseries.published = 1';
            
            if ($letter)
            {
               $where[] = ' SUBSTRING(#__piseries.series_name, 1, 1) = '.$this->_db->quote($letter); 
            }
			
			$groups = implode(',', $user->getAuthorisedViewLevels());
			$where[] = ' (#__piseries.access IN ('.$groups.') OR #__piseries.access = 0)';

			if (($seriessel == 1 || $seriessel == 2)&& $ministry == 0) { 
			if (count($seriesselection) > 1)
			{
                if ($seriessel == 1)
                {$sign = '=';}
                else {$sign = '!=';}
			    foreach ($seriesselection AS $sl)
				{
					$slist[] = '#__piseries.id '.$sign.' '.$sl;
				}
			if ($seriessel == 1)
            {$where[] = '('. ( count( $slist ) ? implode( ' OR ', $slist ) : '' ) .')';}
            else {$where[] = '('. ( count( $slist ) ? implode( ' AND ', $slist ) : '' ) .')';}
			}
			elseif ($seriessel == 1)
			{
				$where[] = '#__piseries.id = '.PIHelperadditional::getwherevalue($seriesselection);
			}
		}
        elseif ($ministry > 0)
        {$where[] = '(#__piseries.ministry REGEXP \'[[:<:]]' .$ministry .'[[:>:]]\' OR #__piseries.ministry REGEXP  \'(.*:"' .$ministry .'".*)\')';}
			$where[] = ' #__piseries.language IN ('.$this->_db->quote($language).','.$this->_db->quote('*').')';
			
			$where 		= ( count( $where ) ? ' WHERE '. implode( ' AND ', $where ) : '' );
		return $where;
		
		}
        
    function _sortlistbydate($items, $column, $order)
    {
        foreach ($items AS $key => $item)        
        {
            if ($column == 'startdate')
            {
                $items[$key]->date = PIHelpermessageinfo::getseriesdate($item->id, 'ASC', null);
            }
            else {
                $items[$key]->date = PIHelpermessageinfo::getseriesdate($item->id, 'DESC', null);
            }
        }
        $order = strtolower($order);
        if ($order == 'asc')
        {
            uasort($items, array('self', 'cmpSeriesDateAsc'));
        }
        elseif ($order == 'desc')
        {
            uasort($items, array('self', 'cmpSeriesDateDesc'));
        }
        
        return $items;
        
    }
    
    /**
     * Compare function for series array. Sort criteria: date reverse
     * @param stdClass $a tag object A
     * @param stdClass $b tag object B
     * @return -1, if lesser; 0, if equal; +1 if greater.
     */
     public function cmpSeriesDateDesc(&$a, &$b)
    {
        $va = $a->date;
        $vb = $b->date;
        if ($va == $vb)
        {
            return 0;
        }
        return ($va < $vb) ? +1 : -1;
    }
    
/**
     * Compare function for series array. Sort criteria: date ascending
     * @param stdClass $a tag object A
     * @param stdClass $b tag object B
     * @return +1, if lesser; 0, if equal; -1 if greater.
     */
   public function cmpSeriesDateAsc(&$a, &$b)
    {
        $va = $a->date;
        $vb = $b->date;
        if ($va == $vb)
        {
            return 0;
        }
        return ($va < $vb) ? -1 : +1;
    }
}