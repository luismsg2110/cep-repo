<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
class PreachitViewterms extends JViewLegacy
{
function display($tpl = null)
{
$app = JFactory::getApplication();
$option = JRequest::getCmd('option');
$document = JFactory::getDocument();
$abspath    = JPATH_SITE;
$id = (int) JRequest::getVar('study', 0);
$dmedia = (int) JRequest::getVar('media', 0);
$study = JTable::getInstance('Studies', 'Table');
$study->load($id);
$item = JRequest::getInt('Itemid', '');
$layout = JRequest::getVar('layout', '');
$user	= JFactory::getUser();

//check that study published
	
if ($study->published != 1) {
JError::raiseError(404, JText::_('COM_PREACHIT_404_ERROR_MESSAGE_NOT_AVAILABLE') );
}

//helpers

require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/additional.php');
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/links.php');
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/info-builder.php');

// get params

$params = PIHelperadditional::getPIparams();

//set layout and load template details

$temp = PIHelperadditional::template();
$override = PIHelperadditional::loadtempcssoverride();
$document->addStyleSheet('components/' . $option . '/templates/'.$temp.'/css/preachit.css');
$this->setLayout('download');

// Look for template files in component folders
$this->_addPath('template', JPATH_COMPONENT.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.$temp);

// Look for overrides in template folder (Joomla! template structure)
$this->_addPath('template', JPATH_SITE.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.$app->getTemplate().DIRECTORY_SEPARATOR.'html'.DIRECTORY_SEPARATOR.'com_preachit');

// set breadcrumb
$pathway = & $app->getPathWay();
$new_pathway_item = $study->study_name;
$blink = PIHelperadditional::getmenulink($item);
$pathway->addItem($new_pathway_item, JRoute::_($blink.'&Itemid='.$item));

// main info 

$message = PIHelperinfobuilder::messageinfo($study, $params, 1);
$this->assignRef('message',    $message);

//get html info
$params->set('downloadterms', 0);
if ($dmedia == 1)
{
    $downloadlink = PIHelperlinks::download($study->id, $study->video_download, 1, $params, $study->video_link, $study->downloadvid_link, $study->add_downloadvid);
}
else
{
    $downloadlink = PIHelperlinks::download($study->id, $study->audio_download, 0, $params, $study->audio_link); 
}
$html = $params->get('downloadtermshtml', '[downloadlink]');
$html = str_replace('[downloadlink]', $downloadlink, $html);
$this->assignRef('html', $html);

//check user has access

$groups = $user->getAuthorisedViewLevels();
if (!in_array($params->get('access', 0), $groups) && $params->get('access', 0) != 0 || !in_array($message->access->message, $groups) && $message->access->message != 0 || 
!in_array($message->access->series, $groups) && $message->access->series != 0 || !in_array($message->access->ministry, $groups) && $message->access->ministry != 0)
{Tewebcheck::check403($params);}

//set metadate & title

$this->setmetadata($study, $params);

//backlink

$bcklinktext = JText::_('COM_PREACHIT_BACK_BUTTON');
if ($params->get('backlinkmain', '1') == '1')
{$backlink = PIHelperadditional::getbacklink($bcklinktext);}
else {$backlink = '';}

$this->assignRef('backlink', $backlink);

//powerby notice

$powered_by = PIHelperadditional::powered();

$this->assignRef('powered_by', $powered_by);

parent::display($tpl);
}

function setmetadata($study, $params)
{
    $app = JFactory::getApplication();
    $document = JFactory::getDocument();
    $Mparams = $app->getParams();

    // Set MetaData
    if ($study->metadesc)
    {
        $metadescription = $study->metadesc;
    } 
    elseif ($Mparams->get('menu-meta_description')) 
    {
        $metadescription = $Mparams->get('menu-meta_description');
    } 
    else 
    {
        if ($this->message->scripture == '')
        {
            $metadescription = strip_tags($study->study_description);
        }
        else 
        {
            $metadescription = strip_tags($this->message->scripture.' - '.$study->study_description);
        }      
    }
    $document->setDescription( $metadescription); 
    
    if ($study->metakey)
    {
        $document->setMetadata('keywords', $study->metakey);
    } 
    elseif ($Mparams->get('menu-meta_keywords')) 
    {
        $document->setMetadata('keywords', $Mparams->get('menu-meta_keywords'));
    }
    
    $params->set('page_title', $study->study_name);
    $title = $study->study_name . ' - '.JText::_('COM_PREACHIT_VIEW_AUDIO') .' '. JText::_('COM_PREACHIT_BY').' '.strip_tags($this->message->teachername);
    // Check for empty title and add site name if param is set
    if (empty($title)) {
    $title = $app->getCfg('sitename');
    }
    elseif ($app->getCfg('sitename_pagetitles', 0) == 1) {
    $title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
    }
    elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
    $title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));}
    $document->setTitle( $title );
    if ($app->getCfg('MetaTitle')){
        $document->setMetaData('title', $title);
    }
    if ($app->getCfg('MetaAuthor')){
        $document->setMetaData('author', strip_tags($this->message->teachername));
    }
}
}