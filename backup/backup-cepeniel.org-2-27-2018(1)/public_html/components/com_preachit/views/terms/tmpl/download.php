<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
// set heading vaiables
$app = JFactory::getApplication();
$Mparams = $app->getParams();
$show = $Mparams->get('show_page_heading', 1);
$enttitle = $Mparams->get('page_heading', '');
$pgtitle = $Mparams->get('page_title', '');
if ($enttitle)
{$pgheader = $enttitle;}
else {$pgheader = $pgtitle;} 
?>

<?php /** Begin Page Title **/ if ($show) : ?>
        <h1 class="title">
            <?php echo $pgheader; ?>
        </h1>
<?php /** End Page Title **/ endif; ?>

<div id="studyview" class="pipage">
<h1 class="study_name"><?php echo $this->message->name;?></h1>
<div class="clr clear-fix"></div>
<?php echo str_replace('class="pilink"', 'class="pilink btn btn-success"', $this->html);?>
<?php echo $this->backlink;?>
</div>
