<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
// load language file
$lang = JFactory::getLanguage();
$lang->load('lib_teweb', JPATH_SITE);
jimport('teweb.checks.standard');
jimport('teweb.details.standard');
JTable::addincludePath(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components/com_preachit/tables');

    $controller    = JControllerLegacy::getInstance('Preachit');
    $controller->execute(JFactory::getApplication()->input->get('task'));
    $controller->redirect();
