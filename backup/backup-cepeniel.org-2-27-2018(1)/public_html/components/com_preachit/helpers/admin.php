<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/adminmedia.php');
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/forms.php');
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/messageimage.php');
jimport('teweb.admin.adjust');
JTable::addIncludePath(JPATH_ADMINISTRATOR.
DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_preachit'.DIRECTORY_SEPARATOR.'tables');
class PIHelperadmin extends PIHelperadminmedia {

/**
	 * Method to get message dates with time zone adjustments
	 *
	 * @param	array $row  Message details.
	 *
	 * @return	array
	 */

public static function getstudydates($data)
{
	
$db= JFactory::getDBO();
	
	//get existing dates from database

$query = "
  SELECT ".$db->quoteName('study_date')."
    FROM ".$db->quoteName('#__pistudies')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($data['id']).";
  ";
$db->setQuery($query);
$date = $db->loadResult();

$query2 = "
  SELECT ".$db->quoteName('publish_up')."
    FROM ".$db->quoteName('#__pistudies')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($data['id']).";
  ";
$db->setQuery($query2);
$publish_up = $db->loadResult();

$query3 = "
  SELECT ".$db->quoteName('publish_down')."
    FROM ".$db->quoteName('#__pistudies')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($data['id']).";
  ";
$db->setQuery($query3);
$publish_down = $db->loadResult();

$query4 = "
  SELECT ".$db->quoteName('podpublish_up')."
    FROM ".$db->quoteName('#__pistudies')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($data['id']).";
  ";
$db->setQuery($query4);
$podpublish_up = $db->loadResult();

$query5 = "
  SELECT ".$db->quoteName('podpublish_down')."
    FROM ".$db->quoteName('#__pistudies')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($data['id']).";
  ";
$db->setQuery($query5);
$podpublish_down = $db->loadResult();

// RBC : if study_date is left blank, get current date and time (adjusted for site offset)
if(!$data['study_date'])

	{
// RBC : if offset is not specified, getDate() adjusts using configured site offset
		$data['study_date'] = JFactory::getDate()->toSql();
	}

else 

{
// PIK : Check to see if the date has changed been changed or new date added - 
//if not return adjusted date from last entry and don't offset again

$data['study_date'] = Tewebadjust::adjustdate($data['study_date'], $date);

}

if (!$data['publish_up'] || $data['publish_up'] == '0000-00-00 00:00:00')

	{
		$data['publish_up'] = JFactory::getDate()->toSql();
	}

else 

	{
		$data['publish_up'] = Tewebadjust::adjustdate($data['publish_up'], $publish_up);
	}
    
if (!$data['podpublish_up'] || $data['podpublish_up'] == '0000-00-00 00:00:00')

    {
        $data['podpublish_up'] = JFactory::getDate()->toSql();
    }

else 

    {
        $data['podpublish_up'] = Tewebadjust::adjustdate($data['podpublish_up'], $podpublish_up);
    }

if (!$data['publish_down'] || $data['publish_down'] == '0000-00-00 00:00:00' || $data['publish_down'] == 'never' || $data['publish_down'] == 'Never')

	{
		$data['publish_down'] = '';
	}
	
else

	{
		$data['publish_down'] = Tewebadjust::adjustdate($data['publish_down'], $publish_down);
	}

if (!$data['podpublish_down'] || $data['podpublish_down'] == '0000-00-00 00:00:00' || $data['podpublish_down'] == 'never' || $data['podpublish_down'] == 'Never')

    {
        $data['podpublish_down'] = '';
    }
    
else

    {
        $data['podpublish_down'] = Tewebadjust::adjustdate($data['podpublish_down'], $podpublish_down);
    }
	
	return $data;
	
}

/**
	 * Method to set the saccess database entry for each message
	 *
	 * @param	int $sid  Series id
	 * @param	int $saccess  Access level
	 *
	 * @return	bolean
	 */

public static function setsaccess($sid, $saccess)
{
$db= JFactory::getDBO();	
$query = "SELECT id, study_name FROM #__pistudies WHERE series = ".$sid; 
	$db->setQuery($query);
	$messages = $db->loadObjectList();
	
	if (is_array($messages))
	{	
	
	foreach ($messages as $message)
	
		{
			$db->setQuery ("UPDATE #__pistudies SET saccess = '".$saccess."' WHERE id = '{$message->id}' ;"); 
					$db->query();
		}
	}
		return true;
}

/**
	 * Method to set the minaccess database entry for each message
	 *
	 * @param	int $mid  Ministry id
	 * @param	int $minaccess  Access level
	 *
	 * @return	bolean
	 */

public static function setminaccess($mid, $minaccess)
{
$db= JFactory::getDBO();	
$query = 'SELECT id, study_name, ministry FROM #__pistudies WHERE ministry REGEXP \'(.*:"'.$mid.'".*)\' OR ministry REGEXP \'[[:<:]]'. $mid .'[[:>:]]\'';
	$db->setQuery($query);
	$messages = $db->loadObjectList();
	if (is_array($messages))
	{	
	
	    foreach ($messages as $message)
	    {
            $minaccess = PIHelperadmin::getminaccessstring($message->ministry);
			$db->setQuery ("UPDATE #__pistudies SET minaccess = '".$minaccess."' WHERE id = '{$message->id}' ;"); 
					$db->query();
		}
	}	
		return true;	

}

/**
	 * Method to get the saccess database entry for each message
	 *
	 * @param	array $row Message details
	 *
	 * @return	array
	 */

public static function getsaccess($data)
{
$db= JFactory::getDBO();
$query = "SELECT ".$db->quoteName('access')."
    FROM ".$db->quoteName('#__piseries')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($data['series']).";
  ";
$db->setQuery($query);
$access = $db->loadResult();	
	
$data['saccess'] = $access;

return $data;
}

/**
     * Method to get the minaccess database entry for each message
     *
     * @param    array $row Message details
     *
     * @return    array
     */

public static function getminaccess($data)
{
    $data['minaccess'] = PIHelperadmin::getminaccessstring($data['ministry']);
    return $data;
}

/**
     * Method to form the minnaccess string from a ministry object
     *
     * @param    string $ministry object for a message
     *
     * @return    string
     */

public static function getminaccessstring($ministry)
{
    $db= JFactory::getDBO();
    // get ministry array
    $registry = new JRegistry; 
    $registry->loadString($ministry);
    $minarray = $registry->toArray(); 
    // get mysql string separated by commas for each access for ministries
    $i = 0;
    $minstring = null;
    foreach ($minarray AS $min)
    {
        if ($min && $min > 0)
        {
            if ($i == 0)
            {$comma = null;}
            else {$comma = ',';}
            $query = "
            SELECT ".$db->quoteName('access')."
            FROM ".$db->quoteName('#__piministry')."
            WHERE ".$db->quoteName('id')." = ".$db->quote($min).";
            ";
            $db->setQuery($query);
            $access = $db->loadResult();
            $minstring .= $comma.$access;
            $i++;
        }
    } 
    if (!$minstring || $minstring == '')
    {
        $minstring = 0;
    }
    return $minstring;
}

/**
	 * Method to auto fill series entry
	 *
	 * @param	string series name from id3 tags
	 *
	 * @return	integer
	 */	
	
public static function autofillseries($series) 
{
    if ($series == '')
    {$value = '';}
    else {
	    $db= JFactory::getDBO();
	    $series = strtolower($series);
	    $query = "
        SELECT ".$db->quoteName('id')."
        FROM ".$db->quoteName('#__piseries')."
        WHERE LOWER(series_name) = ".$db->quote($series).";
 	     ";
	    $db->setQuery($query);
	    $value = $db->loadResult();
	}
	return $value;
}

/**
	 * Method to auto fill teacher entry
	 *
	 * @param	string teacher name from id3 tags
	 *
	 * @return	integer
	 */	
	
public static function autofillteacher($teacher) 
{
    if ($teacher == '')
    {$value = '';}
    else {
	    $db= JFactory::getDBO();
	    $teacher = strtolower($teacher);
	    $query = "SELECT id FROM #__piteachers WHERE LOWER(CONCAT(lastname)) LIKE ".$db->quote($teacher)." OR LOWER(CONCAT(teacher_name,' ',lastname)) LIKE ".$db->quote($teacher)." OR LOWER(CONCAT(teacher_name)) LIKE ".$db->quote($teacher);
        $db->setQuery( $query); 
        $rows = $db->loadObjectList();
        if (isset($rows[0]->id))
        {
            $value = $rows[0]->id;
        }
        else $value = '';
	}
	return $value;	
}

/**
     * Method to sanitise study entries
     *
     * @param    array $row series details
     * @param    string $Jversion version of Joomla
     *
     * @return    array
     */    

public static function sanitisestudyrow($row, $button = false)
{
    
    if(empty($row->study_alias)) 
    {$row->study_alias = $row->study_name;}
    $row->study_alias = Tewebadmin::uniquealias('#__pistudies',$row->study_alias, $row->id, 'study_alias'); 
    $row = PIHelperadmin::getcreator($row);
    
    return $row;
}


/**
	 * Method to sanitise series entries
	 *
	 * @param	array $row series details
	 * @param	string $Jversion version of Joomla
	 *
	 * @return	array
	 */	

public static function sanitiseseriesrow($row, $button = false)
{
    
	if(empty($row->series_alias)) 
    {$row->series_alias = $row->series_name;}
    $row->series_alias = Tewebadmin::uniquealias('#__piseries',$row->series_alias, $row->id, 'series_alias'); 
    $row = PIHelperadmin::getcreator($row);
	
	return $row;
}

/**
	 * Method to sanitise teacher entries
	 *
	 * @param	array $row Teacher details
	 * @param	string $Jversion version of Joomla
	 *
	 * @return	array
	 */	

public static function sanitiseteacherrow($row, $button = false)
{

	if(empty($row->teacher_alias)) 
	{
        if ($row->teacher_name)
        {$name = $row->teacher_name.' '.$row->lastname;}
        else {$name = $row->lastname;}
        $row->teacher_alias = $name;
    }
    $row->teacher_alias = Tewebadmin::uniquealias('#__piteachers',$row->teacher_alias, $row->id, 'teacher_alias');
    
    $row = PIHelperadmin::getcreator($row);
	
	return $row;
}

/**
     * Method to sanitise ministry entries
     *
     * @param    array $row Ministry details
     * @param    string $Jversion version of Joomla
     *
     * @return    array
     */    

public static function sanitiseministryrow($row, $button = false)
{
    $row->ministry_alias = Tewebadmin::uniquealias('#__piministry',$row->ministry_alias, $row->id, 'ministry_alias');
    
    return $row;
}

public static function createnewseries($name)
{
if ($name)
{
	$series = PIHelperadmin::setnewrecord('Series', 'series', $name);
	return $series;
}
else {return 'error';}		
}

public static function createnewteacher($name, $tview)
{
if ($name)
{
	$teacher = PIHelperadmin::setnewrecord('Teachers', 'teacher', $name, $tview);
	return $teacher;
}
else {return 'error';}			

}

public static function createnewministry($name)
{
if ($name)
{
	$ministry = PIHelperadmin::setnewrecord('Ministry', 'ministry', $name);
	return $ministry;
}
else {return 'error';}			
}

public static function setnewrecord($table, $type, $name, $tview = null)
{
	$user = JFactory::getUser();
	$row = JTable::getInstance($table, 'Table');
	$record = new stdClass();
	$decoder = $type.'_name';
	$bind[$decoder] = $name;
	if ($type == 'teacher')
	{
        $bind['teacher_view'] = $tview;
        $name = explode(' ', $bind[$decoder]);
        if (!isset($name[1]))
        {
            $bind['teacher_name'] = '';
            $bind['lastname'] = $name[0];
            $test = true;
        }
        else {
            $bind['lastname'] = str_replace($name[0].' ', '', $bind['teacher_name']);
            $bind['teacher_name'] = $name[0];  
        } 
    }
	if (!$row->bind($bind))
	{JError::raiseError(500, $row->getError() );}
	if ($type == 'series')
	{$row = PIHelperadmin::sanitiseseriesrow($row, 1);}
	if ($type == 'teacher')
	{$row = PIHelperadmin::sanitiseteacherrow($row, 1);}
	if ($type == 'ministry')
	{$row = PIHelperadmin::sanitiseministryrow($row, 1);}
	if (!$user->authorise('core.edit.state', 'com_preachit'))
	{ $row->published = 0;}
    else {$row->published = 1;}
	if (!$row->store()){JError::raiseError(500, $row->getError() );}
	$record->id = $row->id;
    if ($type == 'teacher')
    {
        if ($row->teacher_name != '')
        {
            $record->name = $row->teacher_name.' '.$row->lastname;
        }
        else {$record->name = $row->lastname;}
    }
    else {
	    $record->name = $row->$decoder;
    }
	return $record;
}

/**
     * Method to get creator
     *
     * @param    array $row record details
     * @param    string $define definition of user cell
     *
     * @return    array
     */    

public static function getcreator($row, $define = 'user')
{
$user    = JFactory::getUser();
if ($row->$define >! 0)
{$row->user = $user->id;}
return $row;
}


/**
     * Method to remove a value from a JSON string array
     *
     * @param    string $string json string
     * @param    string $value value to remove
     *
     * @return    strong
     */    


public static function removeJSONconnection($string, $value)
{
    $registry = new JRegistry;
    $registry->loadString($string);
    $array = $registry->toArray();
    foreach ($array AS $key => $ar)
    {
        if ($ar == $value)
        {
            unset ($array[$key]);
        }
    }
    $registry = new JRegistry;
    $registry->loadArray($array);
    $newstring = (string)$registry;
    return $newstring;
}	
}
?>