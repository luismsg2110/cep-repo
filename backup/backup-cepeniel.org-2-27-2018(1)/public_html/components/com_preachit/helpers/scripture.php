<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
JTable::addIncludePath(JPATH_ADMINISTRATOR.
DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_preachit'.DIRECTORY_SEPARATOR.'tables');
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/additional.php');
	
class PIHelperscripture{

    public $ref1 = null;
    public $ref1link = null;
    public $ref2 = null;
    public $ref2link = null;
    public $ref2and = null;
    public $ref2andlink = null;
    public $params = null;	
    
    /**
     * Constructor 
     * @param array $item record for a study
     */
    public function __construct($item) {
        
        // Get params
        $app = JFactory::getApplication();
        $this->params = PIHelperadditional::getPIparams();
        
        // build references
        
        $ref1 = $this->getbibleref($item->study_book, $item->ref_ch_beg, $item->ref_vs_beg, $item->ref_ch_end, $item->ref_vs_end);
        $ref2 = $this->getbibleref($item->study_book2, $item->ref_ch_beg2, $item->ref_vs_beg2, $item->ref_ch_end2, $item->ref_vs_end2);
        $this->ref1 = $ref1->ref;
        $this->ref1link = $ref1->link;
        $this->ref2 = $ref2->ref;
        $this->ref2link = $ref2->link;
        $this->ref2and = JText::_('COM_PREACHIT_AND_SYMBOL').' '.$ref2->ref;
        $this->ref2andlink = JText::_('COM_PREACHIT_AND_SYMBOL').' '.$ref2->link;
    }
    
/**
     * Method to build process scripture reference
     * @param int $bookid book id for the reference
     * @param int $ch_beg chapter to begin the reference
     * @param int $vs_beg verse to begin the reference
     * @param int $ch_end chapter to end the reference
     * @param int $vs_end verse to end the chapter
     * @return   array returns a basic reference and a linked reference
     */       

     private function getbibleref($bookid, $ch_beg, $vs_beg, $ch_end, $vs_end)
     {
        $reference = new stdClass();
        $reference->ref = null;
        $reference->link = null;
        $book = $this->getbookname($bookid, 'display_name');
        $lbook = PIHelperscripture::getbookname($bookid, 'book_name');
        $book = $this->getdisplayname($book);
        if ($bookid == '67')
        {$ref = $book;}
        elseif ($bookid == '0')
        {$ref = '';}
        else {$ref = $this->buildref($bookid, $book, $ch_beg, $vs_beg, $ch_end, $vs_end);}
        if ($ref != '')
        {
            if ($this->params->get('biblelinktype', '1') == 1)
            {        
                $reflink = $this->getBGlink($ref, $bookid, $lbook, $ch_beg, $vs_beg, $ch_end, $vs_end);
            }
            if ($this->params->get('biblelinktype', '1') == 2)
            {
                $reflink = $this->getBiblialink($ref, $bookid, $lbook, $ch_beg, $vs_beg, $ch_end, $vs_end);
            }
            if ($this->params->get('biblelinktype', '1') == 3)
            {
                $reflink = $this->getYouversionlink($ref, $bookid, $ch_beg, $vs_beg);
            }
            $reference->ref = $ref;
            $reference->link = $reflink;
        }
        return $reference;
    }
	
/**
     * Method to get scripture with links if needed
     * @param int $id message id
     * @return   string
     */     
    
    public function scripture()
    {
        if ($this->ref2 != '')
        {
            $join = ' '.JText::_('COM_PREACHIT_AND_SYMBOL').' ';
        }
        else {$join = null;}
        
        if ($this->params->get('bible_link', '1') == 1)
        {
            $scripture = $this->ref1link.$join.$this->ref2link; 
        }
        else {
            $scripture = $this->ref1.$join.$this->ref2; 
        }
        return $scripture;
    }


/**
     * Method to build raw date into readable reference
     * @param int $id message id
     * @param string $book book name
     * @param int $ch1 first chapter in reference
     * @param int $vs1 first verse in reference
     * @param int $ch2 second chapter in reference
     * @param int $vs2 second verse in reference
     * @return   string
     */    

    private function buildref($id, $book, $ch1, $vs1, $ch2, $vs2)
    {
        $sep = $this->params->get('bible_sep', ':');	
        if ($id == '0')
        {
        $scripture = '';	
        }
        else {
	        
        if ($ch1 > 0 && $ch1 == $ch2) {
	        if ($vs1 == $vs2)
		        {$scripture = $book . ' ' . $ch1 . $sep . $vs1;}
	        else {$scripture = $book . ' ' . $ch1 . $sep . $vs1 . '-' . $vs2; }}
	        
        elseif ($ch2 == 0 && $ch1 > 0 && $vs1 > 0)
		        {$scripture = $book . ' ' . $ch1 . $sep . $vs1;}
		        
        elseif ($ch2 == 0 && $ch1 > 0 && $vs1 == 0)
		        {$scripture = $book . ' ' . $ch1;}		
		        
        elseif ($ch2 > 0 && $ch1 > 0 && $vs1 == 0 && $vs2 == 0)
		        {$scripture = $book . ' ' . $ch1 . '-' . $ch2;}

        elseif ($ch1 == 0)
		        {$scripture = $book;}
	        
        else {$scripture = $book . ' ' . $ch1 . $sep . $vs1 . '-' . $ch2 . $sep . $vs2; }
        }

        return $scripture;

    }

/**
     * Method to build bible gateway link
     * @param string $scripture scripture reference
     * @param int $id message id
     * @param string $book book name
     * @param int $ch1 first chapter in reference
     * @param int $vs1 first verse in reference
     * @param int $ch2 second chapter in reference
     * @param int $vs2 second verse in reference
     * @return   string
     */    

    private function getBGlink($scripture, $id, $book, $ch1, $vs1, $ch2, $vs2)
    {
	    $scripturelinkfill = "'width=1050,height=650,scrollbars=1'";
	    $linkbvers = $this->buildref($id, $book, $ch1, $vs1, $ch2, $vs2);
	    $biblevers = $this->getbiblevers();
        $modal = $this->getmodal();
        if ($modal != null)
        {$js = $modal;}
        else $js = 'onclick="window.open(this. href,this.target,' . $scripturelinkfill . ');return false;"';
	    
	    $scripturelink = '<a '.$js.' target="_blank" href="http://www.biblegateway.com/passage/?search=' . $linkbvers . '&version='.$biblevers.'">' 
		    . $scripture . '</a>';
		    
	    return $scripturelink;
    }

/**
     * Method to build Bibia.com link
     * @param string $scripture scripture reference
     * @param int $id message id
     * @param string $book book name
     * @param int $ch1 first chapter in reference
     * @param int $vs1 first verse in reference
     * @param int $ch2 second chapter in reference
     * @param int $vs2 second verse in reference
     * @return   string
     */    


    private function getBiblialink($scripture, $id, $book, $ch1, $vs1, $ch2, $vs2)
    {
	    $linkbvers = PIHelperscripture::buildref($id, $book, $ch1, $vs1, $ch2, $vs2);
	    $sep = $this->params->get('bible_sep', ':');	
	    $linkbvers = str_replace($sep, '.', $linkbvers);
	    $linkbvers = htmlentities($linkbvers);
	    $biblevers = PIHelperscripture::getbiblevers();
        $modal = PIHelperscripture::getmodal();

	    $scripturelink = '<a target="_blank"'.$modal.' href="http://biblia.com/bible/'.$biblevers.'/' . $linkbvers . '">' 
		    . $scripture . '</a>';
		    
		    return $scripturelink;
    }

/**
     * Method to build you version link
     * @param string $scripture scripture reference
     * @param int $id message id
     * @param int $ch first chapter in reference
     * @param int $vs first verse in reference
     * @return   string
     */    


private function getYouversionlink($scripture, $id, $ch, $vs)
    {
	    // get shortform
        $sform = PIHelperscripture::getbookname($id, 'shortform');
	    if ($vs == 0)
	    {$vs = 1;}
	    $modal = PIHelperscripture::getmodal();
	    $biblevers = strtolower(PIHelperscripture::getbiblevers());
	    $scripturelink = '<a target="_blank"'.$modal.'  href="http://www.bible.com/bible/'. $biblevers . '/' . $sform . '/'. $ch .'/'. $vs . '">' 
		    . $scripture . '</a>';
		    
		return $scripturelink;
    }

/**
     * Method to get bible version from params
     * @param unknown_type $params template params
     * @return   string
     */    

    private function getbiblevers()
    {
	    $biblevers = $this->params->get('bible_version', 'NIV');
	    return $biblevers;
    }

/**
     * Method to get display name from database or translation
     * @param string $name Bible book name
     * @param boolean $translate override selection to translate or get from database
     * @return   string
     */    

    public static function getdisplayname($name, $translate = null)
    {
	    if (!$translate)
	    {$translate = PIHelperadditional::translate();}
	    if ($translate == 1)
	    {
		    $option = JRequest::getCmd('option');
		    if ($option != 'com_preachit')
		    {$lang = & JFactory::getLanguage();
  		    $lang->load('com_preachit');}
		    $name = str_replace(' ', '_', $name);
		    $name = JText::_($name);
	    }
	    return $name;
    }

/**
     * Method to get list of books
     * @return   array
     */    

    public static function getbooklist()
    {
	    $translate = PIHelperadditional::translate();
	    $db = JFactory::getDBO();
	    $db->setQuery('SELECT id AS value, display_name AS text FROM #__pibooks WHERE published = 1');
	    $list = $db->loadObjectList();
	    if ($translate == 1)
	    {
		    $i = 0;
		    foreach ($list AS $bl)
		    {
				$booklist[$i] = new stdClass();
			    $booklist[$i]->value = $bl->value;
			    $booklist[$i]->text = self::getdisplayname($bl->text, $translate);
			    $i++;
		    }
		    return $booklist;
	    }
	    else {return $list;}
    }

/**
     * Method to determine whether link should be in modal window or not
     * @param unknown_type $params template params
     * @return   string
     */    

    private function getmodal()
    {
        $modal = null;
        if ($this->params->get('bible_link_modal', 0) == 1)
        {
            $modal = ' onclick="SqueezeBox.setContent( \'iframe\', this.href ); return false;"';
        }
        return $modal;
    }
/**
     * Method to get book name
     * @param int
     * @param string search value
     * @return   string
     */  
    public static function getbookname($id, $column)
    {
        $db = JFactory::getDBO();
        $query = "SELECT ".$db->quoteName($column)."
        FROM ".$db->quoteName('#__pibooks')."
        WHERE ".$db->quoteName('id')." = ".$db->quote($id).";
      ";
      $db->setQuery($query);

      $book = $db->loadResult();
      return $book;
    }

}
?>