<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

JTable::addIncludePath(JPATH_ADMINISTRATOR.
DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_preachit'.DIRECTORY_SEPARATOR.'tables');
jimport('teweb.file.upload');

class PIHelperadminupload{

/**
	 * Method to build filepath
	 *
	 * @param	array $file  File details.
     * @param   int $type sets the type of upload - study, series, teacher, ministry
     * @param   int $media defines the type of media being uploaded
     * @param   int $id the id for the record
	 * @param	int $path The path id.
	 * @param	bolean $flash	Sets whether this is a flash upload or normal php upload and chooses right path through public static function.
     * @param   string $dir additional directory needs
	 *
	 * @return	array
	 */

public static function buildpath($file, $type, $media, $id, $path, $flash = 0, $dir = null)
{
    $pifilepath= JTable::getInstance('Filepath', 'Table');
    $pifilepath->load($path);
    if ($path != -1) 
    {   
        $folder = $pifilepath->folder;
    }
    else {
        $folder = 'images';
    }
	$filename = Tewebupload::initialisfileinfo();
	$filename->type = $pifilepath->type;
	$filename->ftphost = $pifilepath->ftphost;
	$filename->ftpuser = $pifilepath->ftpuser;
	$filename->ftppassword = $pifilepath->ftppassword;
	$filename->ftpport = $pifilepath->ftpport;
	$filename->aws_key = $pifilepath->aws_key;
	$filename->aws_secret = $pifilepath->aws_secret;
	$filename->aws_bucket = $pifilepath->folder;

	//sanitise folder

	//remove last / if present from folder

	$last1 = substr($folder, -1);
	if ($last1 == '/')
	{$folder = substr_replace($folder,"",-1);}

	//remove first / if present from folder

	$first = substr($folder, 0, 1);
	if ($first == '/')
	{$folder = substr_replace($folder,'',0, 1);}
	$pre = PIHelperadmin::buildprefix($type, $media, $id);
	if ($pre != '')
	{$file = $pre.$file;}

	//This removes any characters that might cause headaches to browsers. This also does the same thing in the model
	$badchars = array(' ', '\'', '"', '`', '@', '^', '!', '#', '$', '%', '*', '(', ')', '[', ']', '{', '}', '~', '?', '>', '<', ',', '|', '\\', ';', '&', '_and_');
	$file = str_replace($badchars, '_', $file);
	$filename->file = $dir.Tewebupload::makesafe($file);
	if ($path != -1) 
    {  
		if ($filename->type == 2)
		{$filename->path = $folder . '/' . $filename->file;}
		else {
		$filename->path = JPATH_SITE . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $filename->file;}
	}
	else {
		$filename->path = JPATH_SITE . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $filename->file;
		$filename->file = $folder.'/'.$filename->file;
	}

	return $filename;
}

/**
	 * Method to build prefix for file naming convention
	 *
	 * @param	int $type Type of record
	 * @param   int $media Type of media
	 * @param   int $id  $id for the record
	 *
	 * @return	bolean
	 */

public static function buildprefix($type, $media, $id)
{
$pre = null;
$params = Tewebdetails::getparams('com_preachit');
if ($type == 1)
{
// build prefix to prevent overwriting file
if ($media == 1 && $params->get('prefixm', 0) == 1)
{$pre = 'pa'.time().'_';}
elseif ($media == 2 && $params->get('prefixm', 0) == 1)
{$pre = 'pv'.time().'_';}
elseif ($media == 3 && $params->get('prefixm', 0) == 1)
{$pre = 'pn'.time().'_';}
elseif ($media == 4 && $params->get('prefixi', 0) == 1)
{$pre = 'pims'.time().'_';}
elseif ($media == 5 && $params->get('prefixi', 0) == 1)
{$pre = 'pimm'.time().'_';}
elseif ($media == 6 && $params->get('prefixi', 0) == 1)
{$pre = 'piml'.time().'_';}
elseif ($media == 7 && $params->get('prefixm', 0) == 1)
{$pre = 'pid'.time().'_';}
elseif ($media == 8 && $params->get('prefixm', 0) == 1)
{$pre = 'pisl'.time().'_';}
else {$pre = '';}
}
elseif ($type == 2)
{
if ($media == 1 && $params->get('prefixi', 0) == 1)
{$pre = 'ptis'.time().'_';}
elseif ($media == 2 && $params->get('prefixi', 0) == 1)
{$pre = 'ptil'.time().'_';}
else {$pre = '';}
}
elseif ($type == 3)
{
if ($media == 1 && $params->get('prefixi', 0) == 1)
{$pre = 'psis'.time().'_';}
elseif ($media == 2 && $params->get('prefixi', 0) == 1)
{$pre = 'psil'.time().'_';}
elseif ($media == 3 && $params->get('prefixm', 0) == 1)
{$pre = 'psv'.time().'_';}
else {$pre = '';}
}

elseif ($type == 4)
{
if ($media == 1 && $params->get('prefixi', 0) == 1)
{$pre = 'pmnis'.time().'_';}
elseif ($media == 2 && $params->get('prefixi', 0) == 1)
{$pre = 'pmnil'.time().'_';}
else {$pre = '';}
}

return $pre;
}

}
?>