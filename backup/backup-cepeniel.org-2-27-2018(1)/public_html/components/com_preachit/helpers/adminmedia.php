<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/additional.php');
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/adminupload.php');
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/seriesimage.php');
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/messageimage.php');
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/teacherimage.php');
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/ministryimage.php');
jimport('teweb.file.urlbuilder');
jimport('teweb.file.functions');
jimport('teweb.media.functions');
JTable::addIncludePath(JPATH_ADMINISTRATOR.
DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_preachit'.DIRECTORY_SEPARATOR.'tables');
class PIHelperadminmedia extends PIHelperadminupload
{


/**
	 * Method to get the filesize of a file in bytes
	 *
	 * @param	array $row Message details
	 *
	 * @return	array
	 */

public static function getfilesize($row)
{
	
	// get check values to use to skip filesize check if not necessary
	
	$check = PIHelperadmin::checkfilevalues($row);
	
	// get audio file
	
	if (isset($row['audio_folder']) && isset($row['audio_link']) && isset($row['audiofs']) && ($check->af != $row['audio_folder'] || $check->al != $row['audio_link'] || !$row['audiofs'] || $row['audiofs'] == 0) && trim($row['audio_link']) != '')
	{
		$afile = Tewebbuildurl::geturl($row['audio_link'], $row['audio_folder'], 'pifilepath');
		$afileinfo = PIHelperadmin::fileinfo($afile);
		if ($afileinfo->exists)
		{$row['audiofs'] = $afileinfo->size;}
	}
	
	// get video file

	if (isset($row['video_folder']) && isset($row['video_link']) && isset($row['videofs']) && ($check->vf != $row['video_folder'] || $check->vl != $row['video_link'] || !$row['videofs'] || $row['videofs'] == 0) && trim($row['video_link']) != '')
	{
		$vfile = Tewebbuildurl::geturl($row['video_link'], $row['video_folder'], 'pifilepath');
		$vfileinfo = PIHelperadmin::fileinfo($vfile);
		if ($vfileinfo->exists)
		{$row['videofs'] = $vfileinfo->size;}
	}
	
	// get notes file
	
	if (isset($row['notes_folder']) && isset($row['notes_link']) && isset($row['notesfs']) && ($check->nf != $row['notes_folder'] || $check->nl != $row['notes_link'] || !$row['notesfs'] || $row['notesfs'] == 0) && trim($row['notes_link']) != '')
	{
		$nfile = Tewebbuildurl::geturl($row['notes_link'], $row['notes_folder'], 'pifilepath');
		$nfileinfo = PIHelperadmin::fileinfo($nfile);
		if ($nfileinfo->exists)
		{$row['notesfs'] = $nfileinfo->size;}
	}
    
    // get notes file
    
    if (isset($row['slides_folder']) && isset($row['slides_link']) && isset($row['slidesfs']) && ($check->sf != $row['slides_folder'] || $check->sl != $row['slides_link'] || !$row['slidesfs'] || $row['slidesfs'] == 0) && trim($row['slides_link']) != '')
    {
        $sfile = Tewebbuildurl::geturl($row['slides_link'], $row['slides_folder'], 'pifilepath');
        $sfileinfo = PIHelperadmin::fileinfo($sfile);
        if ($sfileinfo->exists)
        {$row['slidesfs'] = $sfileinfo->size;}
    }
	
	// get add downlod file
	
	if (isset($row['downloadvid_folder']) && isset($row['downloadvid_link']) && isset($row['advideofs']) && ($check->avf != $row['downloadvid_folder'] || $check->avl != $row['downloadvid_link'] || !$row['advideofs'] || $row['advideofs'] == 0) && trim($row['downloadvid_link']) != '')
	{
		$avfile = Tewebbuildurl::geturl($row['downloadvid_link'], $row['downloadvid_folder'], 'pifilepath');
		$avfileinfo = PIHelperadmin::fileinfo($avfile);
		if ($avfileinfo->exists)
		{$row['advideofs'] = $avfileinfo->size;}
	}
	
	return $row;
	
}

/**
	 * Method to get all the folder and link values for all message meda
	 *
	 * @param	array $row Message details
	 *
	 * @return	array
	 */

public static function checkfilevalues($row)
{
    $check = new stdClass();
	$db= JFactory::getDBO();
	$query = "SELECT ".$db->quoteName('audio_folder')."
    FROM ".$db->quoteName('#__pistudies')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($row['id']).";
  ";
	$db->setQuery($query);
	$check->af = $db->loadResult();	
	
	$query = "SELECT ".$db->quoteName('audio_link')."
    FROM ".$db->quoteName('#__pistudies')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($row['id']).";
  ";
	$db->setQuery($query);
	$check->al = $db->loadResult();	
	
	$query = "SELECT ".$db->quoteName('video_folder')."
    FROM ".$db->quoteName('#__pistudies')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($row['id']).";
  ";
	$db->setQuery($query);
	$check->vf = $db->loadResult();	
	
	$query = "SELECT ".$db->quoteName('video_link')."
    FROM ".$db->quoteName('#__pistudies')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($row['id']).";
  ";
	$db->setQuery($query);
	$check->vl = $db->loadResult();	
	
	$query = "SELECT ".$db->quoteName('notes_folder')."
    FROM ".$db->quoteName('#__pistudies')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($row['id']).";
  ";
	$db->setQuery($query);
	$check->nf = $db->loadResult();	
	
	$query = "SELECT ".$db->quoteName('notes_link')."
    FROM ".$db->quoteName('#__pistudies')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($row['id']).";
  ";
	$db->setQuery($query);
	$check->nl = $db->loadResult();	
    
    $query = "SELECT ".$db->quoteName('slides_folder')."
    FROM ".$db->quoteName('#__pistudies')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($row['id']).";
  ";
    $db->setQuery($query);
    $check->sf = $db->loadResult();    
    
    $query = "SELECT ".$db->quoteName('slides_link')."
    FROM ".$db->quoteName('#__pistudies')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($row['id']).";
  ";
    $db->setQuery($query);
    $check->sl = $db->loadResult();    
	
	$query = "SELECT ".$db->quoteName('downloadvid_folder')."
    FROM ".$db->quoteName('#__pistudies')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($row['id']).";
  ";
	$db->setQuery($query);
	$check->avf = $db->loadResult();	
	
	$query = "SELECT ".$db->quoteName('downloadvid_link')."
    FROM ".$db->quoteName('#__pistudies')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($row['id']).";
  ";
	$db->setQuery($query);
	$check->avl = $db->loadResult();	
	
	return $check;
}

/**
	 * Method to get file info
	 *
	 * @param	string $file filepath as absolute url or server path
	 *
	 * @return	array
	 */

public static function fileinfo($file)
{
	$fileinfo = Tewebfile::buildfileinfo($file);
	
	return $fileinfo;
}

/**
     * Method to resize teacher images on upload or when triggered in the admin area
     *
     * @param    string $file image filename
     * @param   int $media upload media type - 1 = image
     * @param   int $id teacher id
     *
     * @return    array
     */

public static function resizeteaimage($file, $media, $id, $resize = false)
{
	if ($media == 1 && PIHelperadditional::allowresize())
	{
        // create small image
        $image = PIHelpertimage::getimagepath($id, 'small', $file);
        $imagefile = JPATH_SITE.DIRECTORY_SEPARATOR.$image;
        if (file_exists($imagefile))
        {JFILE::delete($imagefile);}
        $details = PIHelperadditional::getimagesize('teacher', 'small');
        Tewebfile::copyfile($file, $imagefile);
        Tewebmedia::imageresize($details->width, $details->height, $imagefile, $details->qual);
        
        // create medium image
        $image = PIHelpertimage::getimagepath($id, 'medium', $file);
        $imagefile = JPATH_SITE.DIRECTORY_SEPARATOR.$image;
        if (file_exists($imagefile))
        {JFILE::delete($imagefile);}
        $details = PIHelperadditional::getimagesize('teacher', 'medium');
        Tewebfile::copyfile($file, $imagefile);
        Tewebmedia::imageresize($details->width, $details->height, $imagefile, $details->qual);
        
        // create large image
        $image = PIHelpertimage::getimagepath($id, 'large', $file);
        $imagefile = JPATH_SITE.DIRECTORY_SEPARATOR.$image;
        if (file_exists($imagefile))
        {JFILE::delete($imagefile);}
        $details = PIHelperadditional::getimagesize('teacher', 'large');
        Tewebfile::copyfile($file, $imagefile);
        Tewebmedia::imageresize($details->width, $details->height, $imagefile, $details->qual);  
        
        if ($resize)
        {
            $series = JTable::getInstance('Teachers', 'Table');
            $series->load($id);
            $imagefile = JPATH_SITE.DIRECTORY_SEPARATOR.'media/preachit/teachers/default_teasm.jpg';
            if (file_exists($imagefile))
            {JFILE::delete($imagefile);}
            $imagefile = JPATH_SITE.DIRECTORY_SEPARATOR.'media/preachit/teachers/default_teamed.jpg';
            if (file_exists($imagefile))
            {JFILE::delete($imagefile);}
            $imagefile = JPATH_SITE.DIRECTORY_SEPARATOR.'media/preachit/teachers/default_tealrg.jpg';
            if (file_exists($imagefile))
            {JFILE::delete($imagefile);}
            PIHelpertimage::getdefault($series, 'small');
            PIHelpertimage::getdefault($series, 'medium');
            PIHelpertimage::getdefault($series, 'large');
        }
    }

	return true;
}

/**
     * Method to resize ministry images on upload or when triggered in the admin area
     *
     * @param    string $file image filename
     * @param   int $media upload media type - 1 = image
     * @param   int $id ministry id
     *
     * @return    array
     */

public static function resizeminimage($file, $media, $id, $resize = false)
{
    if ($media == 1 && PIHelperadditional::allowresize())
    {
        // create small image
        $image = PIHelperminimage::getimagepath($id, 'small', $file);
        $imagefile = JPATH_SITE.DIRECTORY_SEPARATOR.$image;
        if (file_exists($imagefile))
        {JFILE::delete($imagefile);}
        $details = PIHelperadditional::getimagesize('ministry', 'small');
        Tewebfile::copyfile($file, $imagefile);
        Tewebmedia::imageresize($details->width, $details->height, $imagefile, $details->qual);  
        
        // create medium image
        $image = PIHelperminimage::getimagepath($id, 'medium', $file);
        $imagefile = JPATH_SITE.DIRECTORY_SEPARATOR.$image;
        if (file_exists($imagefile))
        {JFILE::delete($imagefile);}
        $details = PIHelperadditional::getimagesize('ministry', 'medium');
        Tewebfile::copyfile($file, $imagefile);
        Tewebmedia::imageresize($details->width, $details->height, $imagefile, $details->qual);  
        
        // create large image
        $image = PIHelperminimage::getimagepath($id, 'large', $file);
        $imagefile = JPATH_SITE.DIRECTORY_SEPARATOR.$image;
        if (file_exists($imagefile))
        {JFILE::delete($imagefile);}
        $details = PIHelperadditional::getimagesize('ministry', 'large');
        Tewebfile::copyfile($file, $imagefile);
        Tewebmedia::imageresize($details->width, $details->height, $imagefile, $details->qual);    
        
        if ($resize)
        {
            $ministry = JTable::getInstance('Ministry', 'Table');
            $ministry->load($id);
            $imagefile = JPATH_SITE.DIRECTORY_SEPARATOR.'media/preachit/ministry/default_minsm.jpg';
            if (file_exists($imagefile))
            {JFILE::delete($imagefile);}
            $imagefile = JPATH_SITE.DIRECTORY_SEPARATOR.'media/preachit/ministry/default_minmed.jpg';
            if (file_exists($imagefile))
            {JFILE::delete($imagefile);}
            $imagefile = JPATH_SITE.DIRECTORY_SEPARATOR.'media/preachit/ministry/default_minlrg.jpg';
            if (file_exists($imagefile))
            {JFILE::delete($imagefile);}
            PIHelperminimage::getdefault($ministry, 'small');
            PIHelperminimage::getdefault($ministry, 'medium');
            PIHelperminimage::getdefault($ministry, 'large');
        }
    }
    
	return true;
}

/**
     * Method to resize series images on upload or when triggered in the admin area
     *
     * @param    string $file image filename
     * @param   int $media upload media type - 1 = image
     * @param   int $id series id
     *
     * @return    array
     */

public static function resizeserimage($file, $media, $id, $resize = false)
{
	if ($media != 3)
	{
    if ($media == 1 && PIHelperadditional::allowresize())
    {
        // create small image
        $image = PIHelpersimage::getimagepath($id, 'small', $file);
        $imagefile = JPATH_SITE.DIRECTORY_SEPARATOR.$image;
        if (file_exists($imagefile))
        {JFILE::delete($imagefile);}
        $details = PIHelperadditional::getimagesize('series', 'small');
        Tewebfile::copyfile($file, $imagefile);
        Tewebmedia::imageresize($details->width, $details->height, $imagefile, $details->qual); 
        
        // create medium image
        $image = PIHelpersimage::getimagepath($id, 'medium', $file);
        $imagefile = JPATH_SITE.DIRECTORY_SEPARATOR.$image;
        if (file_exists($imagefile))
        {JFILE::delete($imagefile);}
        $details = PIHelperadditional::getimagesize('series', 'medium');
        Tewebfile::copyfile($file, $imagefile);
        Tewebmedia::imageresize($details->width, $details->height, $imagefile, $details->qual); 
        
        // create large image
        $image = PIHelpersimage::getimagepath($id, 'large', $file);
        $imagefile = JPATH_SITE.DIRECTORY_SEPARATOR.$image;
        if (file_exists($imagefile))
        {JFILE::delete($imagefile);}
        $details = PIHelperadditional::getimagesize('series', 'large');
        Tewebfile::copyfile($file, $imagefile);
        Tewebmedia::imageresize($details->width, $details->height, $imagefile, $details->qual); 
        
        if ($resize)
        {
            $series = JTable::getInstance('Series', 'Table');
            $series->load($id);
            $imagefile = JPATH_SITE.DIRECTORY_SEPARATOR.'media/preachit/series/default_sersm.jpg';
            if (file_exists($imagefile))
            {JFILE::delete($imagefile);}
            $imagefile = JPATH_SITE.DIRECTORY_SEPARATOR.'media/preachit/series/default_sermed.jpg';
            if (file_exists($imagefile))
            {JFILE::delete($imagefile);}
            $imagefile = JPATH_SITE.DIRECTORY_SEPARATOR.'media/preachit/series/default_serlrg.jpg';
            if (file_exists($imagefile))
            {JFILE::delete($imagefile);}
            PIHelpersimage::getdefault($series, 'small');
            PIHelpersimage::getdefault($series, 'medium');
            PIHelpersimage::getdefault($series, 'large');
        }
    }
	}
	return true;
}

/**
     * Method to resize message images on upload or when triggered in the admin area
     *
     * @param    string $file image filename
     * @param   int $media upload media type - 4 = image
     * @param   int $id message id
     *
     * @return    array
     */

public static function resizemesimage($file, $media, $id)
{	
	if ($media == 4 && PIHelperadditional::allowresize())
	{
        // create small image
        $image = PIHelpermimage::getimagepath($id, 'small', $file);
        $imagefile = JPATH_SITE.DIRECTORY_SEPARATOR.$image;
        if (file_exists($imagefile))
        {JFILE::delete($imagefile);}
        $details = PIHelperadditional::getimagesize('message', 'small');
        Tewebfile::copyfile($file, $imagefile);
        Tewebmedia::imageresize($details->width, $details->height, $imagefile, $details->qual);
            
        // create medium image
        $image = PIHelpermimage::getimagepath($id, 'medium', $file);
        $imagefile = JPATH_SITE.DIRECTORY_SEPARATOR.$image;
        if (file_exists($imagefile))
        {JFILE::delete($imagefile);}
        $details = PIHelperadditional::getimagesize('message', 'medium');
        Tewebfile::copyfile($file, $imagefile);
        Tewebmedia::imageresize($details->width, $details->height, $imagefile, $details->qual);
        
        // create large image
        $image = PIHelpermimage::getimagepath($id, 'large', $file);
        $imagefile = JPATH_SITE.DIRECTORY_SEPARATOR.$image;
        if (file_exists($imagefile))
        {JFILE::delete($imagefile);}
        $details = PIHelperadditional::getimagesize('message', 'large');
        Tewebfile::copyfile($file, $imagefile);
        Tewebmedia::imageresize($details->width, $details->height, $imagefile, $details->qual); 
	}
	return true;
}

}
?>