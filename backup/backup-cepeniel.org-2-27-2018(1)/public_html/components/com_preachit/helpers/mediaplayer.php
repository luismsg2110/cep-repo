<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
JTable::addIncludePath(JPATH_ADMINISTRATOR.
DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_preachit'.DIRECTORY_SEPARATOR.'tables');
jimport('teweb.file.urlbuilder');
jimport('teweb.checks.standard');
$abspath    = JPATH_SITE;
require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/additional.php');

class PIHelpermediaplayer{
    
    private $config;
    private $params;
    private $comparams;


/**
     * Constructor 
     * @param array $config should be an array of config options. Valid options:
     *  - videoheight (height of the videoplayer)
     *  - videowidth (width of the videoplayer)
     *  - audioheight (height of the audioplayer)
     *  - audiowidth(width of the audio player)
     *  - videoskin (skin file)
     *  - audioskin (skin file)
     */
    public function __construct($config = array()) {
        // Get params
        $app = JFactory::getApplication();
        $this->params = PIHelperadditional::getPIparams();
        $this->comparams = Tewebdetails::getparams('com_preachit', true);
        
        if (!is_array($config)){
            JError::raiseWarning(100, 'Wrong calling of player helper, second parameter needs to be an array');
            $config = array();
        }
        if(!isset($config['videoheight'])){
            $config['videoheight'] = $this->params->get('videoplayer_height', '300');
        }
        if(!isset($config['videowidth'])){
            $config['videowidth'] = $this->params->get('videoplayer_width', '400');
        }
        if(!isset($config['audioheight'])){
            $config['audioheight'] = $this->params->get('audioplayer_height', '20');
        }
        if(!isset($config['audiowidth'])){
            $config['audiowidth'] = $this->params->get('audioplayer_width', '500');
        }
        if(!isset($config['audioskin'])){
            $config['audioskin'] = $this->params->get('audioskin', '');
        }
        if(!isset($config['videoskin'])){
            $config['videoskin'] = $this->params->get('videoskin', '');
        }
        $this->config = $config;
    
    }
/**
     * Method to build mediaplayer code
     * @param array $item details of the record to get the player for
     * @param string $type type of player to get
     * @return   string
     */   
    
    public function mediaplayer($item, $type = 'video')
    {
        if (!$this->testmep($item, $type))
        {return false;}
        $player = new stdClass();
        $mp = $this->getplayerdetails($item, $type);
        $fileurl = $this->getfileurl($item, $type);
        $height = $this->getheight($type);
        $width = $this->getwidth($type);
        $skin = $this->getskin($type);
        $playerurl = $mp->playerurl;
        $root = JURI::base();
        $skinurl = JURI::base() . 'media/preachit/mediaplayers/jwskins/' . $skin;
        if (Tewebcheck::checktablet() || Tewebcheck::checkmobile())
        {$html5only = true;}
        else {$html5only = false;}
        Tewebcheck::checkmobile();
        $html5 = $mp->html5;
        // load html5 script if required
        if ($html5only && $html5 == 1 )
        {
            $mediaplayer = PIHelpermediaplayer::gethtml5code($fileurl, $width, $height, $type);
            $player->script = '';
        }
        
        else {
    
            // run ccconsent check on the code

            if ($this->comparams->get('cookieconsent', 0) == 1)
            {
                if (!Tewebcheck::checkccconsent('analytics'))
                {  
                    $mp->playercode = Tewebcheck::removeccconsent($mp->playercode, true);
                    $mp->playerscript = Tewebcheck::removeccconsent($mp->playerscript, true);
                }
            }
            $mp->playercode = Tewebcheck::removeccconsent($mp->playercode, false);
            $mp->playerscript = Tewebcheck::removeccconsent($mp->playerscript, false);

            // load any javascript and get mp code

            $player->script = $mp->playerscript;
            $mediaplayer = $mp->playercode;

            //get the 2 pictures that can be used to thumbnails
            $image = $this->getplayerthumb($mp->image, $item, $type);
            $unique_id = $item->id.$type;
            
            // personalise the code
            $mediaplayer  = str_replace("[height]",$height, $mediaplayer );
            $mediaplayer  = str_replace("[width]",$width, $mediaplayer );
            $mediaplayer  = str_replace("[fileid]",$fileurl->id, $mediaplayer );
            $mediaplayer  = str_replace("[fileurl]",$fileurl->url, $mediaplayer );
            $mediaplayer  = str_replace("[playerurl]",$playerurl, $mediaplayer );
            $mediaplayer  = str_replace("[root]",$root, $mediaplayer );
            $mediaplayer  = str_replace("[skin]",$skinurl, $mediaplayer );
            $mediaplayer = str_replace("[thumbnail]",$image, $mediaplayer );
            $mediaplayer = str_replace("[unique_id]",$unique_id, $mediaplayer );
        }

        if ($mp->runplugins)
        {
            jimport('teweb.effects.standard');
            $mediaplayer = Tewebeffects::runcontentplugins('onContentPrepare', 'com_preachit.mediaplayer', $mediaplayer, $params);
        }
        $player->code = $mediaplayer;
        return $player;

    }

/**
     * Method to test if there is a need to return a player
     * @param array $item details of record
     * @param string $type type of player to get
     * @return   boolean
     */   

     private function testmep($item, $type)
     {
    // get player id
        if ($type == 'video')
        {
            if (trim($item->video_link) != null)
            {return true;}
        }
        elseif ($type == 'audio')
        {
            if (trim($item->audio_link) != null)
            {return true;}
        }
        elseif ($type == 'svideo')
        {
            if (trim($item->videolink) != null)
            {return true;}
        }  
        elseif ($type == 'vclip1')
        {
            if (trim($item->vclip1_link) != null)
            {return true;}
        }  
        elseif ($type == 'vclip2')
        {
            if (trim($item->vclip2_link) != null)
            {return true;}
        }  
        elseif ($type == 'vclip3')
        {
            if (trim($item->vclip3_link) != null)
            {return true;}
        }    
        return false;
    }

/**
     * Method to get mediaplayer details
     * @param array $item details of record
     * @param string $type type of player to get
     * @return   array
     */   

     private function getplayerdetails($item, $type)
     {
    // get player id
        if ($type == 'video')
        {
            $playerid = $item->video_type;
        }
        elseif ($type == 'audio')
        {
            $playerid = $item->audio_type;
        }
        elseif ($type == 'svideo')
        {
            $playerid = $item->videoplayer;
        }
        elseif ($type == 'vclip1')
        {
            $playerid = $item->vclip1_type;
        }
        elseif ($type == 'vclip2')
        {
            $playerid = $item->vclip2_type;
        }
        elseif ($type == 'vclip3')
        {
            $playerid = $item->vclip3_type;
        }
        else {$playerid = 0;}
        $mp = JTable::getInstance('Mediaplayers', 'Table');
        $mp->load($playerid);
        return $mp;
    }

/**
     * Method to get fileurl
     * @param array $item details of record
     * @param string $type type of player to get
     * @return   string
     */ 
    
    private function getfileurl($item, $type)
    {
        if ($type == 'video')
        {
            $fileid = $item->video_link;
            $folderid = $item->video_folder;
        }
        elseif ($type == 'audio')
        {
            $fileid = $item->audio_link;
            $folderid = $item->audio_folder;
        }
        elseif ($type == 'vclip1')
        {
            $fileid = $item->vclip1_link;
            $folderid = $item->vclip1_folder;
        }
        elseif ($type == 'vclip2')
        {
            $fileid = $item->vclip2_link;
            $folderid = $item->vclip2_folder;
        }
        elseif ($type == 'vclip3')
        {
            $fileid = $item->vclip3_link;
            $folderid = $item->vclip3_folder;
        }
        elseif ($type == 'svideo')
        {
            $fileid = $item->videolink;
            $folderid = $item->videofolder;
        }
        else {
            $fileid = '';
            $folderid = 0;
        }
        $fileurl = new stdClass();
        $fileurl->url = Tewebbuildurl::geturl($fileid, $folderid, 'pifilepath');
        $fileurl->id = $fileid;
        return $fileurl;
    }

/**
     * Method to get mediaplayer height
     * @param string $type type of player to get
     * @return   array
     */   

     private function getheight($type)
     {
    // get player id
        if ($type == 'video' || $type == 'svideo' || $type == 'vclip1' || $type == 'vclip2' || $type == 'vclip3')
        {
            $height = $this->config['videoheight'];
        }
        elseif ($type == 'audio')
        {
            $height = $this->config['audioheight'];
        }
        else {$height = 0;}

        return $height;
    }
    
/**
     * Method to get mediaplayer width
     * @param string $type type of player to get
     * @return   array
     */   

     private function getwidth($type)
     {
    // get player id
        if ($type == 'video' || $type == 'svideo' || $type == 'vclip1' || $type == 'vclip2' || $type == 'vclip3')
        {
            $width = $this->config['videowidth'];
        }
        elseif ($type == 'audio')
        {
            $width = $this->config['audiowidth'];
        }
        else {$width = 0;}

        return $width;
    }

/**
     * Method to get mediaplayer skin
     * @param string $type type of player to get
     * @return   array
     */   

     private function getskin($type)
     {
    // get player id
        if ($type == 'video' || $type == 'svideo' || $type == 'vclip1' || $type == 'vclip2' || $type == 'vclip3')
        {
            $skin = $this->config['videoskin'];
        }
        elseif ($type == 'audio')
        {
            $skin = $this->config['audioskin'];
        }
        else {$skin = null;}

        return $skin;
    }

/**
     * Method to get html5 mediaplayer code
     * @param string $fileurl media file url
     * @params int $width mediaplayer width
     * @params int $height mediaplayer height
     * @params string $type media type
     * @return   string
     */   

     private function gethtml5code($fileurl, $width, $height, $type)
     {
        $mediaplayer = '<div class="localhtmlplayer" id="player-div"><video id="player-div" class="localaudioplayer" src="'.$fileurl->url.'" width='.$width.' height='.$height.' controls></video></div>';
        return $mediaplayer;
    }
    
/**
     * Method to get media player thumbnail picture
     * @param int $imagecode the code selector for the type of image to get
     * @params array $item details of the record
     * @params atring $type type of player we are working with 
     * @return   string
     */   

     private function getplayerthumb($imagecode, $item, $type)
     {  
        if ($imagecode == 1 && ($type == 'video' || $type == 'audio' || $type == 'vclip1' || $type == 'vclip2' || $type == 'vclip3'))
        {
            if(method_exists('Tewebdetails', "getscr"))
            {
                $image = PIHelpermimage::messageimage($item->id, 1, 0, 0, 'large');
                $image = Tewebdetails::getscr($image);
            }
            else {$image = Tewebbuildurl::geturl($item->imagelrg, $item->image_folderlrg, 'pifilepath');}
        }
        elseif ($imagecode == 2 && ($type == 'video' || $type == 'audio' || $type == 'vclip1' || $type == 'vclip2' || $type == 'vclip3'))
        {
            if(method_exists('Tewebdetails', "getscr"))
            {
                $image = PIHelpersimage::seriesimage($item->series, 0, 0, 'large');
                $image = Tewebdetails::getscr($image);
            }
            else {
                $series = JTable::getInstance('Series', 'Table');
                $series->load($item->series);
                $image = Tewebbuildurl::geturl($series->series_image_lrg, $series->image_folderlrg, 'pifilepath');
            }
        }
        elseif ($type = 'svideo' && $imagecode > 0)
        {
            if(method_exists('Tewebdetails', "getscr"))
            {
                $image = PIHelpersimage::seriesimage($item->id, 0, 0, 'large');
                $image = Tewebdetails::getscr($image);
            }
            else {
                $image = Tewebbuildurl::geturl($item->series_image_lrg, $item->image_folderlrg, 'pifilepath');
            }
        }
        else {$image = null;}
        return $image;
    }

}
?>