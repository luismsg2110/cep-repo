<?php
/**
 * @Component - Preachit
 * @author Paul Kosciecha http://www.truthengaged.org.uk
 * @copyright Copyright (C) Paul Kosciecha
 * @license GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

JTable::addIncludePath(JPATH_ADMINISTRATOR.
DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_preachit'.DIRECTORY_SEPARATOR.'tables');
jimport('teweb.media.functions');
jimport('teweb.file.functions');
jimport('teweb.checks.standard');
class PIHelperaddsettings {

/**
     * template parameters
     *
     * @var    JObject
     */
    private static $tempparams = null;

/**
     * template id
     *
     * @var    int
     */
    private static $tempid = 0;
    
    
/**
     * Get template id
     *
     * @return    int
     */
public static function templateid()
{
    if (self::$tempid == 0)
    {
        $app = JFactory::getApplication();
        $website = JURI::BASE();
        $test = substr($website, -14);
        $temp = '';
        if ($test != 'administrator/')
        {
            $params = $app->getParams();
            $temp = $params->get('template_override', '');
        }
        if (!$temp)
        {
            $db = JFactory::getDBO();
            $query = "SELECT ".$db->quoteName('id')."
            FROM ".$db->quoteName('#__pitemplate')."
            WHERE ".$db->quoteName('def')." = ".$db->quote('1').";
            ";
            $db->setQuery($query);
            $temp = $db->loadResult();
        }
    
        $mobtemp = Tewebcheck::checkmobile();
    
        if ($mobtemp)
        {
            $piparams = PIHelperadditional::getPIparams($temp);
            $tempswitch = $piparams->get('mobtemp', '');
            if ($tempswitch)
            {$temp = $tempswitch;}
            self::$tempparams = null;
        }
        self::$tempid = $temp;
    }
    return self::$tempid;

}    
    
/**
	 * Get template folder
	 *
	 * @return	string
	 */

public static function template()
{
	$id = PIHelperadditional::templateid();
	$db = JFactory::getDBO();
	$query = "SELECT ".$db->quoteName('template')."
    FROM ".$db->quoteName('#__pitemplate')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($id).";
  ";             
	$db->setQuery($query);
	$temp = $db->loadResult();
	return $temp;
}

/**
	 * Method to get Preachit params
	 *
	 * @param	int $id  Send this if you want to force public static function to get params from a set template.
	 *
	 * @return	array
	 */

public static function getPIparams($id='')
{
    if (!$id)
    {$id = PIHelperadditional::templateid();}
    if (self::$tempparams == null || $id != self::$tempid)
	{
        $db = JFactory::getDBO();
        $query = "SELECT ".$db->quoteName('params')."
        FROM ".$db->quoteName('#__pitemplate')."
        WHERE ".$db->quoteName('id')." = ".$db->quote($id).";
        ";
        $db->setQuery($query);
        $piparams = $db->loadResult();
        $params = new JRegistry;
        $params->loadString($piparams);
        self::$tempparams = $params;
    }
	return self::$tempparams;
}

/**
	 * Method to set filter for the message list drop downs
	 *
	 * @param	array $rows The array to run through
	 * @param	int $value  The value to match in the array
	 *
	 * @return	string
	 */

public static function setfilter($rows, $value)
{
	$pif = 0;
	$i = 1;
	foreach ($rows AS $row)
	{
	if ($row->value == $value)
		{
			$pif = $i;
		}
	$i = $i + 1;
	}
	return $pif;
}

public static function ajaxitemid($item)
{
    $app = JFactory::getApplication();
    if ($app->getTemplate() == 'weever_cartographer' || JRequest::getInt('r3stest', 0) == 1)
    {return;}
	echo '<div id="piitemid" style="display: none;">'.$item.'</div>';
}

/**
	 * Method to get right value from single line array
	 *
	 * @return	int
	 */

public static function getwherevalue($array)
{
if ($array[0]){$value = $array[0];}
else {$value = 0;}
return $value;
}

/**
     * Method to get image sizes
     * @param string $type sets the type of image - series, teacher, message, ministry
     * @param string $size sets the size of the image - small, medium, large
     * @return    array
     */

public static function getimagesize($type, $size)
{
    $params = Tewebdetails::getparams('com_preachit');
    $image = new stdClass();
    $image->qual = $params->get('imagequal', 60);
    $image->width = null;
    $image->height = null;
    if  ($size == 'small' && $type == 'series')
    {$image->width = $params->get('serimsmw', 50); $image->height = $params->get('serimsmh', 50);}
    elseif  ($size == 'medium' && $type == 'series')
    {$image->width = $params->get('serimmedw', 100); $image->height = $params->get('serimmedh', 100);}
    elseif  ($size == 'large' && $type == 'series')
    {$image->width = $params->get('serimlrgw', 200); $image->height = $params->get('serimlrgh', 200);}
    elseif  ($size == 'small' && $type == 'teacher')
    {$image->width = $params->get('teaimsmw', 50); $image->height = $params->get('teaimsmh', 50);}
    elseif  ($size == 'medium' && $type == 'teacher')
    {$image->width = $params->get('teaimmedw', 100); $image->height = $params->get('teaimmedh', 100);}
    elseif  ($size == 'large' && $type == 'teacher')
    {$image->width = $params->get('teaimlrgw', 200); $image->height = $params->get('teaimlrgh', 200);}
    elseif  ($size == 'small' && $type == 'ministry')
    {$image->width = $params->get('minimsmw', 50); $image->height = $params->get('minimsmh', 50);}
    elseif  ($size == 'medium' && $type == 'ministry')
    {$image->width = $params->get('minimmedw', 100); $image->height = $params->get('minimmedh', 100);}
    elseif  ($size == 'large' && $type == 'ministry')
    {$image->width = $params->get('minimlrgw', 200); $image->height = $params->get('minimlrgh', 200);}
    elseif  ($size == 'small' && $type == 'message')
    {$image->width = $params->get('msimsmw', 50); $image->height = $params->get('msimsmh', 50);}
    elseif  ($size == 'medium' && $type == 'message')
    {$image->width = $params->get('msimmedw', 100); $image->height = $params->get('msimmedh', 100);}
    elseif  ($size == 'large' && $type == 'message')
    {$image->width = $params->get('msimlrgw', 200); $image->height = $params->get('msimlrgh', 200);}
    return $image;
}

/**
     * Method to get default folders from params
     * @param unknown type $params component params
     * @param string $media type of media
     * @return    int
     */

public static function getdefaultfolders($params, $media, $jimage = 0)
{
    if ($media == 'audio' || $media == 1)
    {
        $folder = $params->get('audio_folder', $jimage);
    }
    elseif ($media == 'video' || $media == 'svideo' || $media == 2)
    {
        $folder = $params->get('video_folder', $jimage);
    }
    elseif ($media == 'dvideo' || $media == 7)
    {
        $folder = $params->get('downloadvid_folder', $jimage);
    }
    elseif ($media == 'image' || $media == 4)
    {
        $folder = $params->get('image_folder', $jimage);
    }
    elseif ($media == 'notes' || $media == 3)
    {
        $folder = $params->get('notes_folder', $jimage);
    }
    elseif ($media == 'slides' || $media == 8)
    {
        $folder = $params->get('slides_folder', $jimage);
    }
    elseif ($media == 'videoclip' || $media == 9)
    {
        $folder = $params->get('vclip_folder', $jimage);
    }
    else {$folder = $jimage;}
    return $folder;
}

/**
     * Method to get image resize value
     * @return    boolean
     */

public static function allowresize()
{
    return true;
}

/**
     * Method to create default teacher/series/ministry image
     * @param int $width set width for the default image
     * @param int $height set height for the default image
     * @param string $image path to the image file to create the default image from
     * @param int $qual quality of the default image
     * @param string $type defines the type of image - teacher, series or ministry
     * @return    string
     */

public static function createdefault($width, $height, $image, $qual, $type)
{
	// check that image doesn't already exist
	jimport('joomla.filesystem.folder');
	jimport('joomla.filesystem.file');
    jimport('teweb.media.public static functions');
    $abspath = JPATH_SITE;
	if ($type == 'series' || $type == 'ministry')
	{$source = 'media/preachit/images/default_gen.jpg';}
	if ($type == 'teacher')
	{$source = 'media/preachit/images/default_portrait.jpg';}
    $imagefile = $abspath.DIRECTORY_SEPARATOR.$image;
    $ext = JFile::getext($source);
    if (!file_exists($imagefile))
    {
        Tewebfile::copyfile($source, $imagefile, true);
        if (!Tewebmedia::checksize($width, $height, $imagefile) && Tewebcheck::checkgd())
        {Tewebmedia::imageresize($width, $height, $imagefile, $qual);}
    } 

	return true;
}	

/**
     * Method to subsitute series, message or teacher image as the image url placed in the page metatags for facebook links
     * @param string $entry the facebook entry given in the template parameters
     * @param int $id message id
     * @return    string
     */

public static function facebookimage($entry, $id)
{	$abspath    = JPATH_SITE;
    $entry = trim($entry);
	if ($entry == '{series}' || $entry == '{teacher}' || $entry == '{message}'	)
	{
		if ($entry == '{series}')
		{$study = JTable::getInstance('Studies', 'Table');
		$study->load($id);	
		require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/seriesimage.php');
		$image = PIHelpersimage::seriesimage($study->series, '', '', 'large', 'fb');}
		elseif ($entry == '{teacher}')
		{$study =& JTable::getInstance('Studies', 'Table');
		$study->load($id);	
		require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/teacherimage.php');
		$image = PIHelpertimage::teacherimage($study->teacher, '', '', 'large', 'fb');}
		elseif ($entry == '{message}')
		{$study = JTable::getInstance('Studies', 'Table');
		$study->load($id);	
		require_once($abspath.DIRECTORY_SEPARATOR.'components/com_preachit/helpers/messageimage.php');
		$image = PIHelpermimage::messageimage($study->id, '', '', '', 'large', 'fb');}
	return $image;	
	}
	else {return $entry;}
}

/**
     * Method to determine whether the site is set as multilanguage in the admin area
     * @return    string
     */

public static function translate()
{
    $params = Tewebdetails::getparams('com_preachit');
    $translate = $params->get('multilanguage', 0);
	return $translate;
}

/**
     * Method to get menu link from specific id
     * @param int $id menu id
     * @return    string
     */

public static function getmenulink($id)
{
    $db = JFactory::getDBO();
    $query = "SELECT ".$db->quoteName('link')."
    FROM ".$db->quoteName('#__menu')."
    WHERE ".$db->quoteName('id')." = ".$db->quote($id).";
  ";
    $db->setQuery($query);
    $link = $db->loadResult();
    return $link;
}

}
?>