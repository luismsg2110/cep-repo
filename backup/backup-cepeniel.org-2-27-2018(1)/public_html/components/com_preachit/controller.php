<?php
defined('_JEXEC') or die;
class PreachitController extends JControllerLegacy
{
    
	public function display($cachable = false, $urlparams = false)
	{
        // Set a default view 
        $view = JRequest::getWord('view');
        if (!$view) {
            JRequest::setVar('view', 'studylist');
        }
        if ($view == 'video') 
        {
            JRequest::setVar('view', 'study');
            JRequest::setVar('mode', 'watch');
        }
        if ($view == 'audio') 
        {
            JRequest::setVar('view', 'study');
            JRequest::setVar('mode', 'listen');
        }
        if ($view == 'text') 
        {
            if (JRequest::getVar('layout') == 'print')
            {
                JRequest::setVar('view', 'text');
            }
            else
            {
                JRequest::setVar('view', 'study');
                JRequest::setVar('mode', 'read');
            }
        }
        if ($view == 'videopopup') 
        {
            JRequest::setVar('view', 'studypopup');
            JRequest::setVar('mode', 'watch');
        }
        if ($view == 'audiopopup') 
        {
            JRequest::setVar('view', 'studypopup');
            JRequest::setVar('mode', 'listen');
        }
		
		parent::display();

		// Load the submenu.
		$view = JRequest::getWord('view', 'studylist');

		return $this;
	}
}