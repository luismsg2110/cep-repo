teInsertId3Values = function(value, type)
{
        if (type == 'title')
        {
            if (value != '' && document.getElementById('jform_series_name').value == '')
            {document.getElementById('jform_series_name').value = value;}
        }
        else if (type == 'comment')
        {
            if (value != '' && document.getElementById('jform_series_description').value == '')
            {document.getElementById('jform_series_description').value = value;}
        }
        else if (type == 'thirdparty')
        {
            if (value == 1)
            {
                if (document.getElementById('jform_video_folder'))
                {document.getElementById('jform_video_folder').value = '';}
                else if (document.getElementById('extrainputs')) 
                {
                    window.parent.document.getElementById('extrainputs').innerHTML += '<input type="hidden" id="videofolderoverride" name="videofolderoverride" value ="'+value+'" />';
                }
            }
        }
        else if (type == 'mediaimage')
        {
            if (document.getElementById('jform_imagelrg'))
            {
                if (value != '' && value != 0 && document.getElementById('jform_series_image_lrg').value == '')
                {
                    document.getElementById('jform_series_image_lrg').value = value;
                    if (document.getElementById('jform_image_folderlrg'))
                    {document.getElementById('jform_image_folderlrg').value = '';}
                    else if (document.getElementById('extrainputs')) 
                    {document.getElementById('extrainputs').innerHTML += '<input type="hidden" id="imagefolderoverride" name="imagefolderoverride" value ="'+value+'" />'}
                }
            }
        }
        else if (type == 'video_type')
        {
            if (document.getElementById('jform_videoplayer') && value != '' && value != 0)
            {    
                    document.getElementById('jform_videoplayer').value = value;
            }
            else if (document.getElementById('extrainputs') && value != '' && value != 0) 
            {
                window.parent.document.getElementById('extrainputs').innerHTML += '<input type="hidden" id="videotypeoverride" name="videotypeoverride" value ="'+value+'" />';
            }
        }
        return;
}

