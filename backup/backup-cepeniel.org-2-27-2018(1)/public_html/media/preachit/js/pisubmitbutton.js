Joomla.submitbutton = function(task)
{
        if (task == '')
        		{
                return false;
        		}
		  else if  (pistrpos(task, 'reset')) 
				{
                    if (typeof setDisabledall === 'function')
                    {setDisabledall(false);}
					if(confirm("<?php echo JText::_('COM_PREACHIT_JAVA_SURE_RESET_HITS');?>"))
						{Joomla.submitform(task);
                            if (typeof setDisabledall === 'function')
                            {setDisabledall(false);}
       							return true;}
                                else {
                                    if (typeof setDisabledall === 'function')
                                    {setDisabledall(false);}
                                    return false;}
				}
          else if (task == 'tag.cancel')
          {
              Joomla.submitform(task);
              return true;
          }     
        else
       	 {
                if (typeof setDisabledall === 'function')
                {setDisabledall(false);}
                var isValid=true;
                if (!pistrpos(task, 'cancel'))
                {
                        var forms = $$('form.form-validate');
                        for (var i=0;i<forms.length;i++)
                        {
                                if (!document.formvalidator.isValid(forms[i]))
                                {
                                        isValid = false;
                                        break;
                                }
                        }
                }
 
                if (isValid)
                {
                    if (pistrpos(task, 'tag'))
                    {
                        if (document.getElementById('tag').value != document.getElementById('original').value)
                        {
                            Joomla.submitform(task);
                            return true;
                        }
                        else
                        {
                        alert(Joomla.JText._('COM_PREACHIT_TAG_NO_CAHNGE_WARNING','You haven\'t made any changes'));
                        return false;
                        }
                    }
                    else {
                    
                        Joomla.submitform(task);
                        if (typeof setDisabledall === 'function')
                        {setDisabledall(true);}
                        return true;
                    }
                }
                else
                {
                        if (typeof setDisabledall === 'function')
                        {setDisabledall(true);}
                        alert(Joomla.JText._('COM_PREACHIT_FIELDS_INVALID','Some required fields are missing'));
                        return false;
                }
        }
}

function pistrpos (haystack, needle, offset) {
  var i = (haystack+'').indexOf(needle, (offset || 0));
  return i === -1 ? false : i;
}


