teInsertId3Values = function(value, type, folder)
{
        if (type == 'title')
        {
            if (value != '' && document.getElementById('jform_study_name').value == '')
            {document.getElementById('jform_study_name').value = value;}
        }
        else if (type == 'album')
        {
            if (value != '' && document.getElementById('jform_series').value == '')
            {document.getElementById('jform_series').value = value;}
        }
        else if (type == 'artist')
        {
            if (value != '' && document.getElementById('jform_teacher').value == '')
            {document.getElementById('jform_teacher').value = value;}
        }
        else if (type == 'hrs')
        {
            if (value != '' && (document.getElementById('jform_dur_hrs').value == '0' || document.getElementById('jform_dur_hrs').value == ''))
            {document.getElementById('jform_dur_hrs').value = value;}
        }
        else if (type == 'mins')
        {
            if (value != '' && (document.getElementById('jform_dur_mins').value == '0' || document.getElementById('jform_dur_mins').value == ''))
            {document.getElementById('jform_dur_mins').value = value;}
        }
        else if (type == 'secs')
        {
            if (value != '' && (document.getElementById('jform_dur_secs').value == '0' || document.getElementById('jform_dur_secs').value == ''))
            {document.getElementById('jform_dur_secs').value = value;}
        }
        else if (type == 'comment')
        {
            if (value != '' && document.getElementById('jform_study_description').value == '')
            {document.getElementById('jform_study_description').value = value;}
        }
        else if (type == 'tags')
        {
            if (value != '' && value != 0 && document.getElementById('jform_tags').value == '')
            {document.getElementById('jform_tags').value = value;}
        }
        else if (type == 'thirdparty')
        {
            if (value == 1)
            {
                if (folder == 'video_folder')
                {
                    if (document.getElementById('jform_video_folder'))
                    {document.getElementById('jform_video_folder').value = '';}
                    else if (document.getElementById('extrainputs')) 
                    {
                        document.getElementById('extrainputs').innerHTML += '<input type="hidden" id="videofolderoverride" name="videofolderoverride" value ="'+value+'" />';
                    }
                }
                else if (folder == 'vclip1_folder')
                {
                    if (document.getElementById('jform_vclip1_folder'))
                    {document.getElementById('jform_vclip1_folder').value = '';}
                    else if (document.getElementById('extrainputs')) 
                    {
                        document.getElementById('extrainputs').innerHTML += '<input type="hidden" id="vclip1folderoverride" name="vclip1folderoverride" value ="'+value+'" />';
                    }
                }
                else if (folder == 'vclip2_folder')
                {
                    if (document.getElementById('jform_vclip2_folder'))
                    {document.getElementById('jform_vclip2_folder').value = '';}
                    else if (document.getElementById('extrainputs')) 
                    {
                        document.getElementById('extrainputs').innerHTML += '<input type="hidden" id="vclip2folderoverride" name="vclip2folderoverride" value ="'+value+'" />';
                    }
                }
                else if (folder == 'vclip3_folder')
                {
                    if (document.getElementById('jform_vclip3_folder'))
                    {document.getElementById('jform_vclip3_folder').value = '';}
                    else if (document.getElementById('extrainputs')) 
                    {
                        document.getElementById('extrainputs').innerHTML += '<input type="hidden" id="vclip3folderoverride" name="vclip3folderoverride" value ="'+value+'" />';
                    }
                }
            }
        }
        else if (type == 'mediaimage')
        {
            if (document.getElementById('jform_imagelrg'))
            {
                if (value != '' && value != 0 && document.getElementById('jform_imagelrg').value == '')
                {
                    document.getElementById('jform_imagelrg').value = value;
                    if (document.getElementById('jform_image_folderlrg'))
                    {document.getElementById('jform_image_folderlrg').value = '';}
                    else if (document.getElementById('extrainputs')) 
                    {document.getElementById('extrainputs').innerHTML += '<input type="hidden" id="imagefolderoverride" name="imagefolderoverride" value ="'+value+'" />'}
                }
            }
        }
        else if (type == 'video_type')
        {
        	if (folder == 'video_folder')
        	{
            if (document.getElementById('jform_video_type') && value != '' && value != 0)
            {    
                    document.getElementById('jform_video_type').value = value;
            }
            else if (document.getElementById('extrainputs') && value != '' && value != 0) 
            {
                document.getElementById('extrainputs').innerHTML += '<input type="hidden" id="videotypeoverride" name="videotypeoverride" value ="'+value+'" />';
            }
          }
          else if (folder == 'vclip1_folder')
          {
          	if (document.getElementById('jform_vclip1_type') && value != '' && value != 0)
            {    
                    document.getElementById('jform_vclip1_type').value = value;
            }
            else if (document.getElementById('extrainputs') && value != '' && value != 0) 
            {
                document.getElementById('extrainputs').innerHTML += '<input type="hidden" id="vclip1typeoverride" name="vclip1typeoverride" value ="'+value+'" />';
            }
          }
          else if (folder == 'vclip2_folder')
          {
          	if (document.getElementById('jform_vclip2_type') && value != '' && value != 0)
            {    
                    document.getElementById('jform_vclip2_type').value = value;
            }
            else if (document.getElementById('extrainputs') && value != '' && value != 0) 
            {
                document.getElementById('extrainputs').innerHTML += '<input type="hidden" id="vclip2typeoverride" name="vclip2typeoverride" value ="'+value+'" />';
            }
          }
          else if (folder == 'vclip3_folder')
          {
          	if (document.getElementById('jform_vclip3_type') && value != '' && value != 0)
            {    
                    document.getElementById('jform_vclip3_type').value = value;
            }
            else if (document.getElementById('extrainputs') && value != '' && value != 0) 
            {
                document.getElementById('extrainputs').innerHTML += '<input type="hidden" id="vclip3typeoverride" name="vclip3typeoverride" value ="'+value+'" />';
            }
          }
        }
        return;
}

