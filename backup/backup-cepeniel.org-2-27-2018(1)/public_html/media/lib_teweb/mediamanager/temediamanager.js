/**
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

/**
 * JImageManager behavior for media component
 *
 * @package		Joomla.Extensions
 * @subpackage	Media
 * @since		1.5
 */

(function() {
var temediaManager = this.temediaManager = {
	initialize: function()
	{
		o = this._getUriObject(window.self.location.href);
		//console.log(o);
		q = new Hash(this._getQueryObject(o.query));

		// Setup image manager fields object
		this.fields			= new Object();
		this.fields.url		= document.id("f_url");

		// Setup image listing objects
        if (document.id("temediaframe"))
		{
            this.frame		= window.frames['temediaframe'];
		    this.frameurl	= this.frame.location.href;
        }
	},
    
    getMediaFolder: function()
    {
        var url     = this.frame.location.search.substring(1);
        return this.getParam('relpath', url);
    },

	setFolder: function(relpath)
	{
		var folder = document.id('view_folder').value;
        var url = this.frame.location.href; 
        var option = this.getParam('option', url);
        var media = this.getParam('media', url);
        this.setRelpath(relpath);
        this.frame.location.href = 'index.php?option='+option+'&view=temedialist&tmpl=component&folder=' + folder+'&media=' + media+'&relpath=' + relpath;
        this.uploadFoldertest(folder);
	},
    
    setRelpath: function(relpath)
    {
        if (relpath == '')
        {relpath = this.getMediaFolder();}
        document.getElementById('relpath_notice').value = relpath;
    },

	upFolder: function(option)
	{
		var currentFolder = this.getMediaFolder();

		if(currentFolder.length < 2) {
			return false;
		}

		var folders = currentFolder.split('/');
		var search = '';

		for(var i = 0; i < folders.length - 1; i++) {
			search += folders[i];
			search += '/';
		}
		// remove the trailing slash
		search = search.substring(0, search.length - 1);
		var newFolder = search;
		this.setFolder(newFolder);
	},

	populateFields: function(file)
	{
        document.id('f_folder').value = document.id('view_folder').value;
		document.id("f_url").value = file;
        if (document.id('id3data'))
        {
            this.getId3details(file, document.id('view_folder').value);
        }
	},
    
    populateFieldsupload: function(responseJSON)
    {
        document.id('f_folder').value = responseJSON['setfolder'];
        document.id('f_url').value = responseJSON['setfile'];
    },
    
    processLink: function()
    {
        document.id('f_folder').value = document.id('lt_folder').value;
        document.id('f_url').value = document.id('lt_url').value;
    },

	showMessage: function(text)
	{
		var message  = document.id('message');
		var messages = document.id('messages');

		if(message.firstChild)
			message.removeChild(message.firstChild);

		message.appendChild(document.createTextNode(text));
		messages.style.display = "block";
	},
    
    popId3table: function(responseJSON)
    {
        var popupalert = false;
        if (this.testId3tags())
        {
            if (responseJSON.title && responseJSON.title != '')
            {
                document.id('title').value = responseJSON.title;
                popupalert = true;
            }
            else {
                document.id('title').value = '';
            }
            if (responseJSON.albumid && (responseJSON.albumid != '' || responseJSON.albumid != 0) )
            {
                document.id('album').value = responseJSON.albumid;
                popupalert = true;
            }
            else {
                document.id('album').value = '';
            }
            if (responseJSON.artistid && (responseJSON.artistid != '' || responseJSON.artistid != 0))
            {
                document.id('artist').value = responseJSON.artistid;
                popupalert = true;
            }
            else {
                document.id('artist').value = '';
            }
            if (responseJSON.hrs && responseJSON.hrs != '')
            {document.id('hrs').value = responseJSON.hrs; popupalert = true;}
            else {document.id('hrs').value = '';}
            if (responseJSON.mins && responseJSON.mins != '')
            {document.id('mins').value = responseJSON.mins; popupalert = true;}
            else {document.id('mins').value = '';}
            if (responseJSON.secs && responseJSON.secs != '')
            {document.id('secs').value = responseJSON.secs; popupalert = true;}
            else {document.id('secs').value = '';}
            if (responseJSON.comment && responseJSON.comment != '')
            {
                document.id('comment').value = responseJSON.comment;
                popupalert = true;
            }
            else {
                document.id('comment').value = '';
            }
            if (responseJSON.image && responseJSON.image != '')
            {
                document.id('mediaimage').value = responseJSON.image;
                popupalert = true;
            }
            else {
                document.id('mediaimage').value = '';
            }
            if (responseJSON.tags && responseJSON.tags != '')
            {
                document.id('tags').value = responseJSON.tags;
                popupalert = true;
            }
            else {
                document.id('tags').value = '';
            }
        }
        if (responseJSON.video_type)
        {document.id('video_type').value = responseJSON.video_type;}
        else {document.id('video_type').value = '';}
        if (responseJSON.thirdparty)
        {document.id('thirdparty').value = responseJSON.thirdparty;}
        else {document.id('thirdparty').value = '';}
        if (responseJSON.id)
        {document.id('id').value = responseJSON.id; }
        if (popupalert == true)
        {this.alertId3tags();}
    },
    
    hidedetails: function(detailid)
    {
        if (document.id(detailid+"-cont"))
        {
            document.id(detailid+"-cont").value = ""; 
        }
        return;
    },
    
    testId3tags: function()
    {
        if (document.getElementById('id3data'))
        {
            return true;
        }
        return false;
    },
    
    alertId3tags: function()
    {
        alert(Joomla.JText._('LIB_TEWEB_ID3_ALERT','Information has been gathered for this media. Click on the "ID3 data" tab to view these details.'));
        return;
    },
    
    getID: function()
    {
        return document.id('id').value;
    },
    
    enterValues: function(fileid, folderid)
    {
      if (document.id('f_folder').value == -1)
      {
          var foldervalue = '';
      }
      else {var foldervalue = document.id('f_folder').value;}
      
      window.parent.teInsertFieldValue(foldervalue, folderid);

      var filevalue = document.id('f_url').value;
      
      window.parent.teInsertFieldValue(filevalue, fileid);
    },
    
    fillMainform: function(folder)
    {
        if (this.testId3tags() && typeof window.parent.teInsertId3Values == 'function')
        {
            window.parent.teInsertId3Values(document.id('title').value, 'title', folder);
            window.parent.teInsertId3Values(document.id('album').value, 'album', folder);
            window.parent.teInsertId3Values(document.id('artist').value, 'artist', folder);
            window.parent.teInsertId3Values(document.id('hrs').value, 'hrs', folder);
            window.parent.teInsertId3Values(document.id('mins').value, 'mins', folder);
            window.parent.teInsertId3Values(document.id('secs').value, 'secs', folder);
            window.parent.teInsertId3Values(document.id('comment').value, 'comment', folder);  
            window.parent.teInsertId3Values(document.id('tags').value, 'tags', folder);  
            window.parent.teInsertId3Values(document.id('mediaimage').value, 'mediaimage', folder);  
        }
        if (typeof window.parent.teInsertId3Values == 'function')
        {
            window.parent.teInsertId3Values(document.id('id').value, 'id', folder);
            window.parent.teInsertId3Values(document.id('video_type').value, 'video_type', folder); 
            window.parent.teInsertId3Values(document.id('thirdparty').value, 'thirdparty', folder);
        }
    },
    
    getId3details: function(file, folder)
    {
        var task = parent.document.getElementsByName('controller')[0].value+'.getId3';
        var obj1 = $(parent.document.adminForm).toQueryString().parseQueryString();
        var obj2 = { folder: folder, file: file, task: task, studyid: temediaManager.getID() };
        var obj3 = temediaManager.merge_options(obj1, obj2); 
        var container = new Element('div', { 'class': 'mediaid3container',
                                         'styles': { 'display': 'none'
                                                   }    });  
        var url = 'index.php';
         var XHRCheckin = new Request.HTML({
                        url: url, 
                        data: obj3,
                        method: 'post',
                        update: container,
                        onComplete: function() {    
                            temediaManager.popId3table(JSON.parse(container.innerHTML));
                        }
                        }).send();  
         
    },
    
    get3rdparty: function()
    {
        var file = document.id('video_third').value;
        var task = parent.document.getElementsByName('controller')[0].value+'.get3rdpartydata';
        document.id('3rd-error-response').innerHTML = '';
        var obj1 = $(parent.document.adminForm).toQueryString().parseQueryString();
        var obj2 = { file: file, task: task, studyid: temediaManager.getID() };
        var obj3 = temediaManager.merge_options(obj1, obj2); 
        var container = new Element('div', { 'class': 'mediaid3container',
                                         'styles': { 'display': 'none'
                                                   }    });  
        var url = 'index.php';
         var XHRCheckin = new Request.HTML({
                        url: url, 
                        data: obj3,
                        method: 'post',
                        update: container,
                        onComplete: function() {
                            var response = JSON.parse(container.innerHTML);    
                            if (!response.error)
                            {
                                document.id('3rd-error-response').innerHTML = '';
                                temediaManager.populateFieldsupload(response);
                                temediaManager.popId3table(response);
                            }
                            else {
                                document.id('3rd-error-response').innerHTML = response.error;
                            }
                        }
                        }).send();  
         
    },
    
    uploadFoldertest: function(folder)
    {
        var task = parent.document.getElementsByName('controller')[0].value+'.uploadFoldertest';
        if (folder == 0)
        {document.id("uploadform").style.display = "block";  return;}
        if (document.id('3rd-error-response'))
        {document.id('3rd-error-response').innerHTML = '';}
        var obj1 = $(parent.document.adminForm).toQueryString().parseQueryString();
        var obj2 = { folder: folder, task: task };
        var obj3 = temediaManager.merge_options(obj1, obj2); 
        var container = new Element('div', { 'class': 'mediaid3container',
                                         'styles': { 'display': 'none'
                                                   }    });  
        var url = 'index.php';
         var XHRCheckin = new Request.HTML({
                        url: url, 
                        data: obj3,
                        method: 'post',
                        update: container,
                        onComplete: function() {
                            var response = JSON.parse(container.innerHTML);    
                            if (!response.upfolder || response.upfolder == 0)
                            {
                                 document.id("uploadform").style.display = "none"; 
                            }
                            else {
                                 document.id("uploadform").style.display = "block"; 
                            }
                        }
                        }).send();     
    },
    
    deleteMedia: function(relpath, type)
    {
        var folder = document.id('view_folder').value;
        var task = parent.document.getElementsByName('controller')[0].value+'.deleteMedia';
        if (relpath != '' && folder != 0)
        {
            var obj1 = $(parent.document.adminForm).toQueryString().parseQueryString();
            var obj2 = { folder: folder, file: relpath, task: task, type: type };
            var obj3 = temediaManager.merge_options(obj1, obj2); 
            var container = new Element('div', { 'class': 'mediaid3container',
                                         'styles': { 'display': 'none'
                                                   }    });  
            var url = 'index.php';
            var XHRCheckin = new Request.HTML({
                        url: url, 
                        data: obj3,
                        method: 'post',
                        update: container,
                        onComplete: function() {
                            var response = JSON.parse(container.innerHTML);   
                            if (response.success) 
                            {alert(relpath+': '+response.success);
                            var path = temediaManager.getMediaFolder();
                            temediaManager.setFolder(path);}
                            else if (response.error) 
                            {alert(response.error);}
                            else {alert(response);}
                        }
                        }).send();  
        }
        return false;   
    },
    
    createFolder: function()
    {
        var folder = document.id('view_folder').value;
        var relpath = document.getElementById('relpath_notice').value;
        var crfolder = document.getElementById('create_folder').value;
        var task = parent.document.getElementsByName('controller')[0].value+'.createFolder';
        if (crfolder != '')
        {
            var obj1 = $(parent.document.adminForm).toQueryString().parseQueryString();
            var obj2 = { folder: folder, relpath: relpath, crfolder: crfolder, task: task };
            var obj3 = temediaManager.merge_options(obj1, obj2); 
            var container = new Element('div', { 'class': 'mediaid3container',
                                         'styles': { 'display': 'none'
                                                   }    });  
            var url = 'index.php';
            var XHRCheckin = new Request.HTML({
                        url: url, 
                        data: obj3,
                        method: 'post',
                        update: container,
                        onComplete: function() {
                            var response = JSON.parse(container.innerHTML);   
                            if (response.success) 
                            {alert(response.success);
                            var path = temediaManager.getMediaFolder();
                            temediaManager.setFolder(path);
                            document.getElementById('create_folder').value = '';}
                            else if (response.error) 
                            {alert(response.error);}
                            else {alert(response);}
                        }
                        }).send();  
        }
        return false;   
    },

	parseQuery: function(query)
	{
		var params = new Object();
		if (!query) {
			return params;
		}
		var pairs = query.split(/[;&]/);
		for ( var i = 0; i < pairs.length; i++ )
		{
			var KeyVal = pairs[i].split('=');
			if ( ! KeyVal || KeyVal.length != 2 ) {
				continue;
			}
			var key = unescape( KeyVal[0] );
			var val = unescape( KeyVal[1] ).replace(/\+ /g, ' ');
			params[key] = val;
	   }
	   return params;
	},
    
    getParam: function( name, url )
    {
        name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
        var regexS = "[\\?&]"+name+"=([^&#]*)";
        var regex = new RegExp( regexS );
        var results = regex.exec( url );
        if( results == null )
        return "";
        else
        return results[1];
    },
    
    merge_options: function (obj1,obj2) {
    var obj3 = {};
    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
    return obj3;
    },

	refreshFrame: function()
	{
		this._setFrameUrl();
	},

	_setFrameUrl: function(url)
	{
		if (url != null) {
			this.frameurl = url;
		}
		this.frame.location.href = this.frameurl;
	},

	_getQueryObject: function(q) {
		var vars = q.split(/[&;]/);
		var rs = {};
		if (vars.length) vars.each(function(val) {
			var keys = val.split('=');
			if (keys.length && keys.length == 2) rs[encodeURIComponent(keys[0])] = encodeURIComponent(keys[1]);
		});
		return rs;
	},

	_getUriObject: function(u){
		var bits = u.match(/^(?:([^:\/?#.]+):)?(?:\/\/)?(([^:\/?#]*)(?::(\d*))?)((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[\?#]|$)))*\/?)?([^?#\/]*))?(?:\?([^#]*))?(?:#(.*))?/);
		return (bits)
			? bits.associate(['uri', 'scheme', 'authority', 'domain', 'port', 'path', 'directory', 'file', 'query', 'fragment'])
			: null;
	}
};
})(document.id);

window.addEvent('domready', function(){
	temediaManager.initialize();
});

