/**
 * @package Extly.Components
 * @subpackage com_autotweet - AutoTweet posts content to social channels
 *             (Twitter, Facebook, LinkedIn, etc).
 *
 * @author Prieco S.A.
 * @copyright Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link http://www.extly.com http://support.extly.com
 */

/* jslint plusplus: true, browser: true, sloppy: true */
/* global jQuery, Request, Joomla, alert, Backbone */

define('jootool', [ 'extlycore' ],
		function(Core, Store, ItemEditorHelper, agendaTemplate) {
	"use strict";

	/* BEGIN - variables to be inserted here */

	/* END - variables to be inserted here */

	// Tabs

	/*
	jQuery('#itemEditorTabs a:first').tab('show');

	var itemEditorView = new ItemEditorView({
		el : jQuery('#itemEditorForm'),
		collection : new Agendas()
	});

	// Global agendasToLoad
	itemEditorView.loadAgenda(agendasToLoad);
	*/

});
