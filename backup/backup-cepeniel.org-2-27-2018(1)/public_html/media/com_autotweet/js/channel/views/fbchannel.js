/**
 * @package Extly.Components
 * @subpackage com_autotweet - AutoTweet posts content to social channels
 *             (Twitter, Facebook, LinkedIn, etc).
 *
 * @author Prieco S.A.
 * @copyright Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link http://www.extly.com http://support.extly.com
 */

/* jslint plusplus: true, browser: true, sloppy: true */
/* global jQuery, Request, Joomla, alert, Backbone */

var FbChannelView = Backbone.View
		.extend({

			events : {
				'change #xtformfbchannel_id' : 'onChangeChannel'
			},

			initialize : function() {
				this.options.dispatcher.on('fbapp:channelschanged',
						this.onAccessTokenChanged, this);
				this.collection.on('add', this.loadFbChannel, this);
				this.fbchannellist = '#xtformfbchannel_id';
				this.$('.group-warn').fadeOut();
			},

			onAccessTokenChanged : function onAccessTokenChanged() {
				var thisView = this, messagesview = this.options.messagesview, params = appParamsHelper
						.get(thisView);

				Core.UiHelper.listReset(thisView.$(this.fbchannellist));

				this.collection.create(this.collection.model, {
					attrs : {
						own_app : params.p_own_app,
						app_id : params.p_app_id,
						secret : params.p_secret,
						access_token : params.p_access_token,
						token : params.p_token
					},
					urlBase : (thisView.options.urlBase || ''),
					urlTest : (thisView.options.urlTest || null),

					wait : true,
					dataType:     'text',
					error : function(model, fail, xhr) {
						validationHelper.showError(messagesview,
								fail.responseText);
					}
				});
			},

			onChangeChannel : function onChangeChannel() {
				var access_token = this.getFbChannelAccessToken(),
					channel_type = this.getFbChannelType();
				this.$('#fbchannel_access_token').val(access_token);

				if (channel_type === 'Group') {
					this.$('.group-warn').fadeIn();
				} else {
					this.$('.group-warn').fadeOut();
				}
			},

			getFbChannelAccessToken : function getFbChannelAccessToken() {
				var oselected = this.$('#xtformfbchannel_id option:selected'),
					access_token = 'INVALID';
				if (oselected) {
					access_token = oselected[0].access_token
							|| oselected.attr('access_token');
				}
				return access_token;
			},

			getFbChannelType : function getFbChannelType() {
				var oselected = this.$('#xtformfbchannel_id option:selected'),
					channelType = 'INVALID';
				if (oselected) {
					channelType = oselected[0].data_type
							|| oselected.attr('data_type');
				}
				return channelType;
			},

			getFbChannelId : function getFbChannelId() {
				return this.$('#xtformfbchannel_id option:selected').val();
			},

			loadFbChannel : function loadFbChannel(message) {
				var fbchannellist = this.$(this.fbchannellist), channels;

				fbchannellist.empty();
				if (message.get('status')) {
					channels = message.get('channels');
					_.each(channels, function(channel) {
						var opt = new Option();
						opt.value = channel.id;
						opt.text = channel.type + ': ' + channel.name;
						opt.access_token = channel.access_token;
						opt.data_type = channel.type;

						fbchannellist.append(opt);
					});
					fbchannellist.trigger('liszt:updated');
					this.onChangeChannel();
				} else {
					validationHelper.showError(this.options.messagesview,
							message.get('error_message'));
				}

			}

		});
