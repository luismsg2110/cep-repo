/**
 * @package Extly.Components
 * @subpackage com_autotweet - AutoTweet posts content to social channels
 *             (Twitter, Facebook, LinkedIn, etc).
 *
 * @author Prieco S.A.
 * @copyright Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link http://www.extly.com http://support.extly.com
 */

/* jslint plusplus: true, browser: true, sloppy: true */
/* global jQuery, Request, Joomla, alert, Backbone */

var VkGroupView = Backbone.View
		.extend({

			events : {
				'click #vkgrouploadbutton' : 'ongroupsReq'
			},

			initialize : function() {
				this.collection.on('add', this.loadVkGroup, this);
				this.vkgrouplist = '#xtformvkgroup_id';
			},

			ongroupsReq : function ongroupsReq() {
				var thisView = this,
					list = thisView.$(this.vkgrouplist),
					channelId = thisView.$('#channel_id').val(),
					channelToken = thisView.$('#access_token').val(),
					token = thisView.$('#token').attr('name');

				Core.UiHelper.listReset(list);

				this.collection.create(this.collection.model, {
					attrs : {
						channel_id : channelId,
						access_token : channelToken,
						token : token
					},
					urlBase : (thisView.options.urlBase || ''),
					urlTest : (thisView.options.urlTest || null),

					wait : true,
					dataType:     'text',
					error : function(model, fail, xhr) {
						validationHelper.showError(thisView,
								fail.responseText);
					}
				});
			},

			loadVkGroup : function loadVkGroup(message) {
				var vkgrouplist = this.$(this.vkgrouplist), groups;

				vkgrouplist.empty();
				if (message.get('status')) {
					groups = message.get('groups');
					_.each(groups, function(group) {
						var opt = new Option();
						opt.value = group.id;
						opt.text = group.name;
						vkgrouplist.append(opt);
					});
					vkgrouplist.trigger('liszt:updated');
				} else {
					validationHelper.showError(this, message.get('error_message'));
				}

			}

		});