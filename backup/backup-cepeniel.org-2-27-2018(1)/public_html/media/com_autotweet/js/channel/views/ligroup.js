/**
 * @package Extly.Components
 * @subpackage com_autotweet - AutoTweet posts content to social channels
 *             (Twitter, Facebook, LinkedIn, etc).
 *
 * @author Prieco S.A.
 * @copyright Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link http://www.extly.com http://support.extly.com
 */

/* jslint plusplus: true, browser: true, sloppy: true */
/* global jQuery, Request, Joomla, alert, Backbone */

var LiGroupView = Backbone.View
		.extend({

			events : {
				'click #ligrouploadbutton' : 'onChangeChannel'
			},

			initialize : function() {
				this.collection.on('add', this.loadLiGroup, this);
				this.ligrouplist = '#xtformgroup_id';
				this.$('.group-warn').fadeOut();
			},

			onChangeChannel : function onChangeChannel() {
				var thisView = this,
					params = appParamsHelper.getLi(thisView);

				Core.UiHelper.listReset(thisView.$(this.ligrouplist));

				this.collection.create(this.collection.model, {
					attrs : {
						api_key : params.p_api_key,
						secret_key : params.p_secret_key,
						oauth_user_token : params.p_oauth_user_token,
						oauth_user_secret : params.p_oauth_user_secret,
						token : params.p_token
					},
					urlBase : (thisView.options.urlBase || ''),
					urlTest : (thisView.options.urlTest || null),

					wait : true,
					dataType:     'text',
					error : function(model, fail, xhr) {
						validationHelper.showError(this, fail.responseText);
					}
				});
			},

			loadLiGroup : function loadLiGroup(message) {
				var ligrouplist = this.$(this.ligrouplist), channels;

				ligrouplist.empty();
				if (message.get('status')) {
					channels = message.get('channels');
					_.each(channels, function(channel) {
						var opt = new Option();
						opt.value = channel.id;
						opt.text = channel.name;
						ligrouplist.append(opt);
					});
					ligrouplist.trigger('liszt:updated');
				} else {
					validationHelper.showError(this, message.get('error_message'));
				}

			}

		});
