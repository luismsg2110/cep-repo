<?php // no direct access
defined('_JEXEC') or die('Restricted access');
$i = 0;
$count_list = count($list);
?>
<div class="latestnews" style="width:100%">

	<?php foreach ($list as $item) :  ?>
		<?php $i++; ?>
		<?php if($i == $count_list) : ?>
		<div class="latestnewsitems last-item" style="width:<?php echo $item->width; ?>%">
		<?php else : ?>
		<div class="latestnewsitems" style="width:<?php echo $item->width; ?>%">
		<?php endif; ?>
			<div class="latestnewsitems-inner">
			

			

			<?php if($params->get('thumb')==1 && $item->thumb) : ?>
			<div class="latestnews-thumb second-effect">
				
				<img src="<?php echo $item->thumb; ?>" border="0" alt="<?php echo $item->title; ?>" />
				
				 <div class="mask">  
					<a  href="<?php echo $item->link; ?>" class="info" title="Detail">Full Image</a>  
				</div> 
				<div class="latestnews-shadow"></div>
			</div>
			<?php endif; ?>
			
			<?php if($params->get('showtitle')==1) : ?>
				<h4 class="latestnews-title"><a href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a></h4>
			<?php endif; ?>
			
			<?php if($params->get('showdate')==1) : ?>
				<span class="latestnewsdate"><?php echo $item->date; ?></span>
			<?php endif; ?>
			
			<p class="latestnews-text" style="margin: 5px 0;">
				<?php if($params->get('showintro')==1) echo $item->introtext; ?>
			</p>


			<?php if($params->get('readmore')==1) : ?>
				
				<a class="readmore" href="<?php echo $item->link; ?>"><span><?php echo JText::sprintf('Read more'); ?></span></a>
				
			<?php endif; ?>
			</div>
		</div>
	<?php endforeach; ?>

</div>
