<?php
/**
* @version 1.7.x
* @package ZooTemplate Project
* @email webmaster@zootemplate.com
* @copyright (c) 2008 - 2011 http://www.ZooTemplate.com. All rights reserved.
*/

$groups = array('bd'=>'Background Body','zt-userwrap5'=>'Background Userwrap5');
$value  = array();

$prefix = "galosa";

//Body Group
$value['bd']['color'] = $ztTools->getParamsValue($prefix, 'color', 'bd');
$value['bd']['image'] = array($ztTools->getParamsValue($prefix, 'image', 'bd'), array('pattern1', 'pattern2', 'pattern3', 'pattern4', 'pattern5', 'pattern6', 'pattern7', 'pattern8', 'pattern9', 'pattern10', 'pattern11', 'pattern12', 'pattern13', 'pattern14', 'pattern15', 'pattern16'));

$value['zt-userwrap5']['color'] = $ztTools->getParamsValue($prefix, 'color', 'zt-userwrap5');
$value['zt-userwrap5']['image'] = array($ztTools->getParamsValue($prefix, 'image', 'zt-userwrap5'), array('pattern1', 'pattern2', 'pattern3', 'pattern4', 'pattern5', 'pattern6', 'pattern7', 'pattern8', 'pattern9', 'pattern10', 'pattern11', 'pattern12', 'pattern13', 'pattern14', 'pattern15', 'pattern16'));


?>

<style type="text/css">

	#bd {background-color: <?php echo $ztTools->getParamsValue($prefix, 'color', 'bd'); ?>;}
	#zt-userwrap5 {background-color: <?php echo $ztTools->getParamsValue($prefix, 'color', 'zt-userwrap5'); ?>;}
</style>
